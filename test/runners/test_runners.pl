#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;
use DBD::Sybase;
use DBI;
use FindBin;
use lib "$FindBin::Bin/";
require "runners/transferto-common.pm";


# Some data is not accessible from my account, so I hard-code credentials here.
# TODO: fix this
our $db_s = 'db.hometowntelecom.com';
our $db_t = 'htt_billing_actions';
our $db_u = 'transferto';
our $db_p = 'Vah}vaich9';

our $db_name = 'TEL_DATA';

my $connect_string = "DBI:Sybase:database=".$db_name.";server=${db_s}";

my $dbh = DBI->connect($connect_string,
                        $db_u, $db_p,
                        {'RaiseError' => 1});

die "Couldn't connect: $!" unless defined $dbh;


main();


sub main
{
  my $test_name = $ARGV[0];

  my $tests =
  {
    'find_customer'       => \&test_find_customer,
    'get_queue_item_info' => \&test_get_queue_item_info,
    'lock_attempt'        => \&test_lock_attempt,
    'run_sql'             => \&test_run_sql,
    'update_queue_status' => \&test_update_queue_status,
    'user_agent_request'  => \&test_user_agent_request,
    'verify_lock_attempt' => \&test_verify_lock_attempt,
    'perform_post_request' => \&test_perform_post_request,
  };

  if ( exists $tests->{ $test_name } )
  { 
    $tests->{ $test_name }->();
  }
  else
  {
    die "Undefined test $test_name\n\n";
  }
}

sub test_perform_post_request
{
  my $url = 'https://rgalli-dev.uvnv.com/pr/internal/1/ultra/api/internal__SubmitChargeAmount';

  my $data =
  {
    'customer_id' => 123,
  };

  my ( $response ) = transferto::perform_post_request($url,$data,$data);

  print Dumper($response);
}

sub test_run_sql
{
  my $result = [];

  my $q = "SELECT * FROM $db_t";
  #my $q = "SELECT 1+1"; # OK

  print "\n$q\n";

  transferto::run_sql($dbh,$q,$result);

  print Dumper($result);
}

sub test_find_customer
{
  my $customer = transferto::find_customer($dbh,123);

  print Dumper($customer);
}

sub test_lock_attempt
{
  my ($reservation,$key) = transferto::lock_attempt($dbh,$db_t);

  print "reservation: $reservation\n";
  print "key:         $key\n";
}

sub test_verify_lock_attempt
{
  my $reservation = 'RESERVED 134608204526184';

  my %queue = transferto::verify_lock_attempt($dbh,$db_t,$reservation);

  print Dumper(\%queue);
}

sub test_update_queue_status
{
  my $uuid = 'test';
  my $status = 'new_test_status';

  transferto::update_queue_status($dbh,$db_t,$uuid,$status);
}

sub test_get_queue_item_info
{
  my $uuid = 'test';

  my %queue = transferto::get_queue_item_info($dbh,$db_t,$uuid);

  print Dumper(\%queue);
}

sub test_user_agent_request
{
  my $url = "http://not-existing-fake-url-i-hope.com/noway.php";

  my $data = {test=>123};

  my ($response,$timeout) = transferto::user_agent_request($url,$data,6);

  print Dumper([$response,$timeout]);
}

__END__

Usage:
%> perl test/runners/test_runners.pl run_sql
%> perl test/runners/test_runners.pl user_agent_request
%> perl test/runners/test_runners.pl update_queue_status
%> perl test/runners/test_runners.pl lock_attempt
%> perl test/runners/test_runners.pl find_customer
%> perl test/runners/test_runners.pl get_queue_item_info
%> perl test/runners/test_runners.pl verify_lock_attempt

