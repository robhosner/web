use strict;

use Data::Dumper;
use SOAP::Lite +trace => 'debug';

my $soap = SOAP::Lite->uri('provisioning__checkSIMCard')->proxy('https://dougmeli:Flora@rgalli3-dev.uvnv.com/ps/celluphone/1/ultra/api/provisioning__checkSIMCard.xml');

my @p = (
partner_tag => 'random',
ICCID       => '1234567890123456789',
mode        => 'x'
);

my $som = $soap->provisioning__checkSIMCard(@p);

print Dumper( $som->{'_context'}->{'_transport'}->{'_proxy'}->{'_http_response'}->{'_content'} );


__END__

my $soap = SOAP::Lite->uri('customercare__ReplaceSIMCardByMSISDN')->proxy('https://dougmeli:Flora@rgalli3-dev.uvnv.com/ps/celluphone/1/ultra/api/customercare__ReplaceSIMCardByMSISDN.xml');

my @p = (
 SOAP::Data->name('msisdn'      => '1001001000')->type('string'),
 SOAP::Data->name('partner_tag' => 'test_celluphone')->type('string'),
 SOAP::Data->name('new_ICCID'   => '1234567890123456789')->type('string'),
 #SOAP::Data->name('store_id'    => 1234)->type('string'),
 #SOAP::Data->name('user_id'     => 1234)->type('string')
 SOAP::Data->name('store_id'    => 1234),
 SOAP::Data->name('user_id'     => 1234)
);

my $som = $soap->customercare__ReplaceSIMCardByMSISDN(@p);

print Dumper( $som->{'_context'}->{'_transport'}->{'_proxy'}->{'_http_response'}->{'_content'} );

__END__

my $soap = SOAP::Lite->uri('interactivecare__GetCommandHelp')->proxy('https://dougmeli:Flora@rgalli3-dev.uvnv.com/ps/interactivecare/1/ultra/api/interactivecare__GetCommandHelp.xml');

my $p = SOAP::Data->name('msisdn' => '1001001000');

my $som = $soap->interactivecare__GetCommandHelp($p);

print Dumper( $som->{'_context'}->{'_transport'}->{'_proxy'}->{'_http_response'}->{'_content'} );

__END__

my $namespace = 'celluphone';
my $command   = 'provisioning__requestProvisionPortedCustomerAsync';

my $soap = SOAP::Lite->uri( $command )->proxy( 'https://dougmeli:Flora@rgalli3-dev.uvnv.com/ps/'.$namespace.'/1/ultra/api/'.$command.'.xml');

my @p = (
  SOAP::Data->name('ICCID' => '123456789012345678'),
  SOAP::Data->name('loadPayment' => 'NONE'),
  SOAP::Data->name('targetPlan' => 'L49'),
  SOAP::Data->name('numberToPort' => ''),
  SOAP::Data->name('portAccountNumber' => 'abcdefg'),
  SOAP::Data->name('portAccountPassword' => 'abcdefg'),
  SOAP::Data->name('portAccountZipcode' => '12345'),
  SOAP::Data->name('pinsToApply' => ''),
  SOAP::Data->name('creditCard' => '1122334455667788'),
  SOAP::Data->name('creditExpiry' => '1'),
  SOAP::Data->name('creditCVV' => '1'),
  SOAP::Data->name('creditAutoRecharge' => '1'),
  SOAP::Data->name('creditAmount' => '1'),
  SOAP::Data->name('customerZip' => '12345')->type('string'),
  SOAP::Data->name('customerAddressOne' => '1'),
  SOAP::Data->name('customerAddressTwo' => '1'),
  SOAP::Data->name('customerCity' => 'New York'),
  SOAP::Data->name('customerState' => 'NY'),
  SOAP::Data->name('customerFirstName' => 'Tester'),
  SOAP::Data->name('customerLastName' => 'Teston'),
  SOAP::Data->name('customerEMail' => 'TT@example.com'),
  SOAP::Data->name('activation_masteragent' => '12'),
  SOAP::Data->name('activation_distributor' => '13'),
  SOAP::Data->name('activation_agent' => '14'),
  SOAP::Data->name('activation_store' => '15'),
  SOAP::Data->name('activation_userid' => '16')
);

my $som = $soap->provisioning__requestProvisionPortedCustomerAsync(@p);

#print Dumper($som);

print Dumper( $som->{'_context'}->{'_transport'}->{'_proxy'}->{'_http_response'}->{'_content'} );

__END__

Perl script to test SOAP calls
