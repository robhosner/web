<?php

# Usage:
# php test/api/test_threeci.php ShowCustomerInfo             $MSISDN [GET_DATA_USAGE]           OK
# php test/api/test_threeci.php FindDealersByZip             $MSISDN $ZIPCODE  OK (it works, but we should increase the radius)
# php test/api/test_threeci.php GetCommandHelp               $MSISDN           OK
# php test/api/test_threeci.php ApplyPIN                     $MSISDN $PIN      OK
# php test/api/test_threeci.php ApplyDataRecharge            $CUSTOMER_ID     $SOC_ID
# php test/api/test_threeci.php CheckDataRechargeByMSISDN    $MSISDN
# php test/api/test_threeci.php ApplyVoiceRecharge           $CUSTOMER_ID $VOICE_SOC_ID
# php test/api/test_threeci.php CheckVoiceRechargeByMSISDN   $MSISDN
# php test/api/test_threeci.php SetLanguagebyMSISDN          $MSISDN  $LANG
# php test/api/test_threeci.php ApplyInternationalFixedPromo $MSISDN
# php test/api/test_threeci.php CalculateTaxesFees           $MSISDN $CHARGE_AMOUNT $CHECK_TAXES $CHECK_RECOVERY_FEE
# php test/api/test_threeci.php SetAutoRechargeByMSISDN      $MSISDN $AUTO_RECHARGE

include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');

$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);

$partner = 'threeci';
$base_url = find_site_url().'/pr/'.$partner.'/1/ultra/api/';

abstract class AbstractTestStrategy
{
  abstract function test();

  public function postAndPrint($url,$params) {
    global $curl_options;

    $json_result = curl_post($url,$params,$curl_options,NULL,240);
    echo "\n\n$json_result\n\n";
    print_r(json_decode($json_result));
  }
}

class Test_threeci__GetCommandHelp extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__GetCommandHelp';

    $params = array(
      'msisdn'   => $argv[2]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__FindDealersByZip extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__FindDealersByZip';

    $params = array(
      'msisdn'   => $argv[2],
      'zip_code' => $argv[3]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__SetLanguageByMSISDN extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__SetLanguagebyMSISDN';

    $params = array(
      'msisdn'             => $argv[2],
      'preferred_language' => $argv[3]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__ShowCustomerInfo extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ShowCustomerInfo';

    $params = array(
      'msisdn' => $argv[2]
    );

    if ( ! empty($argv[3]))
      $params['get_data_usage'] = TRUE;

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__ApplyPIN extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyPIN';

    $params = array(
      'msisdn'     => $argv[2],
      'pin_number' => $argv[3]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__CheckDataRechargeByMSISDN extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__CheckDataRechargeByMSISDN';

    $params = array(
      'msisdn' => $argv[2]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__CheckVoiceRechargeByMSISDN extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__CheckVoiceRechargeByMSISDN';

    $params = array(
      'msisdn' => $argv[2]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__ApplyVoiceRecharge extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyVoiceRecharge';

    $params = array(
      'customer_id'  => $argv[2],
      'voice_soc_id' => $argv[3]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__ApplyDataRecharge extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyDataRecharge';

    $params = array(
      'customer_id'    => $argv[2],
      'data_soc_id'    => $argv[3]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__ApplyInternationalFixedPromo extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__ApplyInternationalFixedPromo';

    $params = array(
      'msisdn' => $argv[2]
    );

    $this->postAndPrint($url,$params);
  }
}

class Test_threeci__CalculateTaxesFees extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__CalculateTaxesFees';

    $params = array(
      'msisdn'             => $argv[2],
      'charge_amount'      => $argv[3],
      'check_taxes'        => $argv[4],
      'check_recovery_fee' => $argv[5]
    );

    $this->postAndPrint($url,$params);    
  }
}

class Test_threeci__SetAutoRechargeByMSISDN extends AbstractTestStrategy {
  function test() {
    global $base_url;
    global $argv;

    $url = $base_url . 'interactivecare__SetAutoRechargeByMSISDN';

    $params = array(
      'msisdn'        => $argv[2],
      'auto_recharge' => $argv[3]
    );

    $this->postAndPrint($url,$params);
  }
}

# perform test #

$testClass = 'Test_'.$partner.'__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();

?>

