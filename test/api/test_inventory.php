<?php


# Usage:
# php test/api/test_inventory.php StockSIMCelluphone
# php test/api/test_inventory.php ShipSIMMaster


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/inventory/1/ultra/api/';


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_inventory__StockSIMCelluphone extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'inventory__StockSIMCelluphone';

    $params = array(
      'request_epoch' => time(),
      'startICCID'    => 800000000000000001,
      'endICCID'      => 800000000000000003,
      'updated_by'    => 12345
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_inventory__ShipSIMMaster extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'inventory__ShipSIMMaster';

    $params = array(
      'request_epoch' => time(),
      #'startICCID'    => 800000000000000001,
      #'endICCID'      => 800000000000000003,
'startICCID'    => '890126084210441030',
#'endICCID'      => '890126084210443029',
'endICCID'      => '890126084210441038',
      'updated_by'    => 54321,
      'masteragent'   => 987667890
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,100);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_inventory__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>
