<?php

# php test/api/test_celluphone.php CheckOrangeActCode $ACTCODE $MODE
# php test/api/test_celluphone.php checkZipCode $ZIP_CODE
# php test/api/test_celluphone.php checkSIMCard $ICCID
# php test/api/test_celluphone.php requestActivateShippedNewCustomerAsync $ICCID
# php test/api/test_celluphone.php verifyActivateShippedNewCustomerAsync $REQUEST_ID
# php test/api/test_celluphone.php verifyProvisionPortedCustomerAsync $REQUEST_ID
# php test/api/test_celluphone.php requestResubmitProvisionPortedCustomerAsync $NUMBERTOPORT $PORTACCTNUMBER $PORTACCTPASSWORD
# php test/api/test_celluphone.php GetPortingState $MSISDN
# php test/api/test_celluphone.php requestProvisionNewCustomerAsync $ICCID
# php test/api/test_celluphone.php verifyProvisionNewCustomerAsync $REQUEST_ID
# php test/api/test_celluphone.php requestProvisionPortedCustomerAsync $NUMBERTOPORT $PORTACCTNUMBER $PORTACCTPASSWORD
# php test/api/test_celluphone.php CancelRequestedPort $ICCID $NUMBERTOPORT
# php test/api/test_celluphone.php checkPINCard $PIN
# php test/api/test_celluphone.php checkPINCards $PIN $PIN $PIN $PIN $PIN $PIN
# php test/api/test_celluphone.php ReplaceSIMCardByMSISDN $MSISDN $NEW_ICCID $STORE_ID $USER_ID
# php test/api/test_celluphone.php ValidateMSISDN $MSISDN
# php test/api/test_celluphone.php DealerLoginAsCustomer $CUSTOMER_ID $AGENT_ID $USER_ID
# php test/api/test_celluphone.php CustomerInfoByID $CUSTOMER_ID $MODE
# php test/api/test_celluphone.php SearchCustomers $MSISDN $ICCID $NAME $EMAIL $ACTCODE

include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');

$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);

$partner = 'celluphone';
$base_url = find_site_url().'/pr/'.$partner.'/1/ultra/api/';

abstract class AbstractTestStrategy
{
  abstract function test();

  public function postAndPrint($url,$params,$curl_options)
  {
    $json_result = curl_post($url,$params,$curl_options,NULL,240);
    echo "\n\n$json_result\n\n";
    print_r(json_decode($json_result));
  }
}

class Test_celluphone__requestProvisionOrangeCustomerAsync extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__requestProvisionOrangeCustomerAsync';

    $params = array(
      'actcode'            => $argv[2],
      'customerZip'        => $argv[3],
      'activation_channel' => $argv[4],
      'activation_info'    => $argv[5],
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__CheckOrangeActCode extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__CheckOrangeActCode';

    $actcode = $argv[2];
    $mode    = ( isset($argv[3]) ? $argv[3]: null);

    $params = array(
      'actcode'  => $actcode,
      'mode'     => $mode
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__checkZipCode extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;
    
    $url = $base_url . 'provisioning__checkZipCode';

    $zipCode =  (isset($argv[2])) ? $argv[2] : NULL;

    $params = array(
      'request_epoch' => time(),
      'zip_code'      => $zipCode
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__checkSIMCard extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__checkSIMCard';

    // $test_iccid = get_test_iccid();

    $params = array(
      'request_epoch' => time(),
      'ICCID'         => $argv[2]
      #'ICCID'         => 8000000000000000011,
      #'ICCID'         => '8901260842102253880'
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__requestActivateShippedNewCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__requestActivateShippedNewCustomerAsync';

    // $test_iccid = get_test_iccid();

    $params = array(
      'ICCID'       => $argv[2],
      'zsession'    => test_login_for_zsession( $curl_options )
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__verifyActivateShippedNewCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__verifyActivateShippedNewCustomerAsync';

    $params = array(
      'request_id' => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__verifyProvisionPortedCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__verifyProvisionPortedCustomerAsync';

    $params = array(
      'request_epoch' => time(),
      'request_id'    => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__requestResubmitProvisionPortedCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__requestResubmitProvisionPortedCustomerAsync';

    #$numberToPort = '2124704574';
    $numberToPort        = $argv[2];
    $portAccountNumber   = $argv[3];
    $portAccountPassword = $argv[4];

    if ( ! $numberToPort ) { die("No numberToPort"); }

    #$test_iccid = get_test_iccid();

    $test_iccid = '8901260842102260396';

    if ( ! $test_iccid ) { die("No ICCID to test"); }

    $params = array(
      'ICCID'                  => $test_iccid,
      'numberToPort'           => $numberToPort,
      'portAccountNumber'      => $portAccountNumber,
      'portAccountPassword'    => $portAccountPassword,
      'portAccountZipcode'     => '12345',
      'activation_masteragent' => '55',
      'activation_distributor' => '44',
      'activation_agent'       => '33',
      'activation_store'       => '22',
      'activation_userid'      => '11'
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__GetPortingState extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;
   
    $url = $base_url . 'provisioning__GetPortingState';

    $params = array(
      'msisdn' => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__requestProvisionNewCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__requestProvisionNewCustomerAsync';

    $params = array(
      'ICCID'                  => $argv[2],
      'targetPlan'             => 'L49',
      'loadPayment'            => 'NONE',
      "customerFirstName"      => "ITW Promotional",
      "activation_distributor" => "301",
      "activation_agent"       => "10323",
      "activation_userid"      => "11863",
      "loadPayment"            => "PINS",
      'pinsToApply'            => '7264592833',
      "activation_masteragent" => "69",
      "customerEMail"          => "vtarasov@ultra.me",
      "customerZip"            => "48229",
      "customerLastName"       => "ITW Promotional"
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__verifyProvisionNewCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__verifyProvisionNewCustomerAsync';

    $params = array(
      'request_epoch' => time(),
      'request_id'    => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__requestProvisionPortedCustomerAsync extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__requestProvisionPortedCustomerAsync';

    $numberToPort        = $argv[2];
    $portAccountNumber   = $argv[3];
    $portAccountPassword = $argv[4];

    if ( ! $numberToPort ) { die("No numberToPort"); }

    #$test_iccid = get_test_iccid();
    $test_iccid = '';

    if ( ! $test_iccid ) { die("No ICCID to test"); }

    $base_params = array(
      'ICCID'               => $test_iccid,
      'loadPayment'         => 'PINS',
      #'loadPayment'        => 'CCARD',
      'pinsToApply'        => '7264592830',
      'targetPlan'          => 'L29',
      'numberToPort'        => $numberToPort,
#'numberToPort'        => '9172390705', '9172390659', '9174235208', '3472435064',
      'portAccountNumber'   => $portAccountNumber,
      'portAccountPassword' => $portAccountPassword,
      'customerZip'            => '12345',
      'activation_masteragent' => '52',
      'activation_distributor' => 42,
      'activation_agent'       => '32',
      'activation_store'       => 22,
      'activation_userid'      => '12'
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__CancelRequestedPort extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__CancelRequestedPort';

    $params = array(
      'ICCID'        => $argv[2],
      'numberToPort' => $argv[3]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__checkPINCard extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__checkPINCard';

    $params = array(
      'pin' => $argv[2]
#'188646081071819442'
#'123456789012345678'
#'890126084210225555'
#'800000000000000003'
#'300000003000000600'
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__checkPINCards extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'provisioning__checkPINCards';

    $params = array(
      'pin' => $argv[2]
    );

    foreach(range(3,7) as $n)
    {
      if ( isset($argv[$n]) )
      { $params['pin'.($n-1)] = $argv[$n]; }
    }

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__ReplaceSIMCardByMSISDN extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ReplaceSIMCardByMSISDN';

    $params = array(
      'msisdn' => $argv[2],
      'new_ICCID' => $argv[3],
      'store_id' => $argv[4],
      'user_id' => $argv[5]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__ValidateMSISDN extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ReplaceSIMCardByMSISDN';

    $params = array(
      'msisdn' => $argv[2]
    );
  
    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__DealerLoginAsCustomer extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__DealerLoginAsCustomer';

    $customerId = (isset($argv[2])) ? $argv[2] : 1125;
    $agentId = (isset($argv[3])) ? $argv[3] : 33;
    $userId = (isset($argv[4])) ? $argv[4] : 456;

    $params = array(
      'customer_id' => $customerId,
      'agent_id' => $agentId,
      'user_id' => $userId
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__CustomerInfoByID extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CustomerInfoByID';

    $mode = (isset($argv[3])) ? $argv[3] : 'OCS_BALANCE';

    $params = array(
      'customer_id' => $argv[2],
      'mode' => 'OCS_BALANCE',
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_celluphone__SearchCustomers extends AbstractTestStrategy
{
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__SearchCustomers';

    $params = array(
      'msisdn'  => empty($argv[2]) ? NULL : $argv[2],
      'iccid'   => empty($argv[3]) ? NULL : $argv[3],
      'name'    => empty($argv[4]) ? NULL : $argv[4],
      'email'   => empty($argv[5]) ? NULL : $argv[5],
      'actcode' => empty($argv[6]) ? NULL : $argv[6]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

function test_login_for_zsession( $curl_options , $login , $password )
{
  $url = find_site_url() . '/pr/portal/1/ultra/api/portal__Login';

  echo "url = $url\n";

  $params = array(
    'account_login'    => $login,
    'account_password' => $password
  );

  $json_result = curl_post($url,$params,$curl_options,NULL,240);

  $json_decoded = json_decode($json_result);

  dlog('',"zsession = ".$json_decoded->zsession);

  return $json_decoded->zsession;
}

$testClass = 'Test_celluphone__' . $argv[1];
print "$testClass\n\n";
$testObject = new $testClass();
$testObject->test();

?>
