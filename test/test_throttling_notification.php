<?php

// attempts to send throttling notification to provided customer_id

global $credentials;
require_once 'cosid_constants.php';

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

if ( ! isset($argv[1])) { dlog('', 'ERROR: Provide a customer_id'); exit; }
$customer_id = $argv[1];

teldata_change_db();

$customer = get_customer_from_customer_id($customer_id);

if ( ! $customer) { dlog('', 'ERROR: Customer not found'); exit; }

$notification = new \Ultra\Lib\MiddleWare\ACC\NotificationThrottlingAlert('test_' . time());

$params = array(
  'Value' => 1,
  'MSISDN' => $customer->current_mobile_number,
  'Event' => 80,
  'UserData' => array('senderId' => 'MVNEACC', 'timeStamp' => '2015-11-05T06:30:52'),
  'CounterName' => 'WPRBLK20'
);

$notification->processNotification($params);
