<?php

include_once 'db.php';
include_once 'classes/ProjectW.php';

teldata_change_db();

/*
$plan_cost = 50;

$customer = get_customer_from_customer_id( 2077 );

      // add STORED_VALUE for total plan amount
      $result = \func_courtesy_add_stored_value(
        array(
          'terminal_id'      => '0',
          'amount'           => $plan_cost,
          'reference'        => create_guid( __FUNCTION__ ),
          'source'           => 'UVIMPORT',
          'reason'           => 'UVIMPORT.MEP_SV_INCREASE',
          'customer'         => $customer,
          'detail'           => 'ProjectW::enrollMEP'
        )
      );

exit;
*/

class Test_get_dealer_code_from_univision_code
{
  function test( $argv )
  {
    $x = \get_dealer_code_from_univision_code( $argv[2] );

    print_r( $x );
  }
}

class Test_get_candidate_rows_to_send_sms
{
  function test( $argv )
  {
    $x = \get_candidate_rows_to_send_sms( $argv[2] );

    print_r( $x );
  }
}

class Test_get_candidate_rows_to_process
{
  function test( $argv )
  {
    $x = \get_candidate_rows_to_process();

    print_r( $x );
  }
}

class Test_update_file_status_paused
{     
  function test( $argv )
  {   
    $file_name = $argv[2];

    if ( \update_univision_file_status_paused( $file_name ) )
      echo "OK\n";
    else
      echo "KO\n";      
  }   
}     
      
class Test_update_file_status_ongoing
{
  function test( $argv )
  {
    $file_name = $argv[2];

    if ( \update_univision_file_status_ongoing( $file_name ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_file_status_completed
{
  function test( $argv )
  {
    $file_name = $argv[2];

    if ( \update_univision_file_status_completed( $file_name ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_file_status_aborted
{
  function test( $argv )
  {
    $file_name = $argv[2];

    if ( \update_univision_file_status_aborted( $file_name ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_invalid
{ 
  function test( $argv )
  {
    $univision_import_id = $argv[2];
    $last_error          = $argv[3];

    if ( \update_univision_import_invalid( $univision_import_id , $last_error) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_port_called
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];

    if ( \update_univision_import_port_called( $univision_import_id ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_activated_temp
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];

    if ( \update_univision_import_activated_temp( $univision_import_id ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_port_error
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];
    $last_error          = $argv[3];

    if ( \update_univision_import_port_error( $univision_import_id , $last_error ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_port_failed
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];
    $last_error          = $argv[3];

    if ( \update_univision_import_port_failed( $univision_import_id , $last_error ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_changesim_called
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];

    if ( \update_univision_import_changesim_called( $univision_import_id ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_activated
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];

    if ( \update_univision_import_activated( $univision_import_id ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_changesim_error
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];
    $last_error          = $argv[3];

    if ( \update_univision_import_changesim_error( $univision_import_id , $last_error ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_update_changesim_failed
{
  function test( $argv )
  {
    $univision_import_id = $argv[2];
    $last_error          = $argv[3];

    if ( \update_univision_import_changesim_failed( $univision_import_id , $last_error ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}

class Test_get_by_import_date_range
{
  function test( $argv )
  {
    $q = select_univision_import_by_date_query( '2016-01-13 16:39:04', '2016-01-13 16:39:05' );
    $a = mssql_fetch_all_objects( logged_mssql_query( $q ) );
    var_dump( $a );
  }
}

class Test_add
{
  function test( $argv )
  {
    $msisdn          = '';
    $iccid_full      = '';
    $temp_iccid_full = '';

    for( $i = 0 ; $i < 19 ; $i++ )
    {
      $iccid_full      .= mt_rand(0, 9);
      $temp_iccid_full .= mt_rand(0, 9);

      if ( $i < 10 )
        $msisdn .= mt_rand(0, 9);
    }

    $params = array(
      'migration_date'  => 'Jan  2 2016 11:00:00:000PM',
      'balance'         => 1010,
      'city'            => 'test city',
      'state'           => 'CA',
      'email'           => 'test email',
      'address'         => 'test address',
      'fist_name'       => 'test fist_name',
      'last_name'       => 'test last_name',
      'file_name'       => 'test file_name',
      'file_date_time'  => 'Jan  4 2016 05:25:41:847PM',
      'renewal_date'    => '02/20/2016',
      'iccid_full'      => $iccid_full,
      'temp_iccid_full' => $temp_iccid_full,
      'imei'            => '1234567890123456',
      'msisdn'          => $msisdn,
      'feature'         => 'test feature',
      'zip_code'        => '12345',
      'account_number'  => '23456',
      'account_pin'     => '34567',
      'plan_name'       => 'UV55',
      'language'        => 'EN',
      'plan_state'      => 'Active',
      'univision_code'  => '45678',
      'subs_number'     => '56789',
      'subs_id'         => '67890',
      'data_allotted'   => '222',
      'data_used'       => '333',
      'data_remaining'  => '444',
      'monthly_renewal_target' => 'UV55'
    );

    if ( \add_to_univision_import( $params ) )
      echo "OK\n";
    else
      echo "KO\n";
  }
}


$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$testObject = new $testClass();
$testObject->test( $argv );

/*

Examples:

php test/db/test_univision_import.php add
php test/db/test_univision_import.php update_invalid 8
php test/db/test_univision_import.php update_port_called 8
php test/db/test_univision_import.php update_activated_temp 8
php test/db/test_univision_import.php update_port_error 8 test_error2
php test/db/test_univision_import.php update_changesim_called 8
php test/db/test_univision_import.php update_activated 8
php test/db/test_univision_import.php update_changesim_error 8 test_error3
php test/db/test_univision_import.php update_changesim_failed 8 test_error4
php test/db/test_univision_import.php update_file_status_ongoing 'test file_name'
php test/db/test_univision_import.php update_file_status_paused 'test file_name'
php test/db/test_univision_import.php update_file_status_aborted 'test file_name'
php test/db/test_univision_import.php update_file_status_completed 'test file_name'
*/
