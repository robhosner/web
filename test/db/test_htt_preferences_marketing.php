<?php

# php test/db/test_htt_preferences_marketing.php set_marketing_settings $CUSTOMER_ID $EMAIL_OPT $SMS_OPT
# php test/db/test_htt_preferences_marketing.php get_marketing_settings $CUSTOMER_ID
# php test/db/test_htt_preferences_marketing.php htt_preferences_marketing_select_query $CUSTOMER_ID
# php test/db/test_htt_preferences_marketing.php htt_preferences_marketing_insert_query $CUSTOMER_ID
# php test/db/test_htt_preferences_marketing.php validate_marketing_settings $CUSTOMER_ID
# php test/db/test_htt_preferences_marketing.php get_sms_preference_by_customer_id $CUSTOMER_ID

include_once 'db.php';

function test_set_marketing_settings()
{
  global $argv;

  $params = array();
  $params['customer_id'] = (isset($argv[2])) ? $argv[2] : 1685;
  $params['marketing_email_option'] = (isset($argv[3])) ? $argv[3] : '0';
  $params['marketing_sms_option'] = (isset($argv[4])) ? $argv[4] : '1';

  $result = set_marketing_settings($params);
  print_r($result);
  echo "\n\n";
}

function test_get_marketing_settings()
{
  global $argv;

  $params = array();
  $params['customer_id'] = (isset($argv[2])) ? $argv[2] : 1685;

  $result = get_marketing_settings($params);
  print_r($result);
  echo "\n\n";
}

function test_htt_preferences_marketing_select_query()
{
  global $argv;

  $params = array();
  $params['customer_id'] = (isset($argv[2])) ? $argv[2] : 1685;

  echo htt_preferences_marketing_select_query($params);
  echo "\n\n";
}

function test_htt_preferences_marketing_insert_query()
{
  global $argv;

  $params = array();
  $params['customer_id'] = (isset($argv[2])) ? $argv[2] : 1685;

  echo htt_preferences_marketing_insert_query($params);
  echo "\n\n";
}

function test_validate_marketing_settings()
{
  global $argv;

  // valid
  $settings = array(
    'marketing_email_option',
    '0',
    'marketing_sms_option',
    '1'
  );
  
  print_r(validate_marketing_settings($settings)); echo "\n";

  // invalid
  $settings = array(
    'marketing_email',
    '0',
    'marketing_sms',
    '1'
  );

  print_r(validate_marketing_settings($settings)); echo "\n";

  // invalid
  $settings = array(
    'marketing_email_option',
    '0',
    'marketing_sms_option',
    '2'
  );

  print_r(validate_marketing_settings($settings)); echo "\n";
}

function test_get_sms_preference_by_customer_id()
{
  global $argv;

  $params = array();
  $customer_id = (isset($argv[2])) ? $argv[2] : 1685;

  echo "marketing_sms_option for $customer_id : ";
  echo get_sms_preference_by_customer_id($customer_id) . "\n";
}

$test = $argv[1];

if (empty($argv[1]))
{
  echo "ERROR: no test name given\n";
  return;
}

$test = 'test_' . $argv[1];
if (!is_callable($test))
{
  echo "ERROR: function '$test' does not exist\n";
  return;
}

teldata_change_db();
$test();
echo "\n";

?>
