<?php

require_once 'db.php';

/**
 * createCCRows
 *
 * A. inserts a new CC_HOLDERS row:
 * customer_id:$customerId, created_date=now, BIN:$ccBin, last_four:$last4, EXPIRES_DATE:$exp, CVV:"X", AVS:"U", ENABLED:1
 * Riz and I agreed on "X" for CVV to denote that we do not know what the response value was. "U" for AVS (Verification Unavailable).
 *
 * B. inserts new CC_HOLDER_TOKENS row:
 * CC_HOLDERS_ID (from A above), gateway: MeS, Merchangeaccount: 1, token: $token, enabled:1 
 */
function createCCRows($token, $customer_id, $bin, $last_four, $expire_date)
{
  $success = TRUE;

echo "$token $bin $last_four $expire_date\n";

  // clean up
  $token = str_replace( '"' , '' , $token);
  $token = str_replace( "'" , "" , $token);
  $bin   = str_replace( '"' , '' , $bin );
  $bin   = str_replace( "'" , "" , $bin );
  $last_four = str_replace( '"' , '' , $last_four );
  $last_four = str_replace( "'" , "" , $last_four );
  $expire_date = str_replace( '"' , '' , $expire_date );
  $expire_date = str_replace( "'" , "" , $expire_date );

echo "$token $bin $last_four $expire_date\n";

  $sql = sprintf("
  INSERT INTO ULTRA.CC_HOLDERS
(
  CUSTOMER_ID,
  BIN,
  LAST_FOUR,
  EXPIRES_DATE,
  CVV_VALIDATION,
  AVS_VALIDATION
)
VALUES
(
  %d,
  '%s',
  '%s',
  '%s',
  'X',
  'U'
)",
    $customer_id,
    $bin,
    $last_four,
    $expire_date
  );

  try
  {
    $success = run_sql_and_check($sql);

    if ( ! $success )
      throw new \Exception( 'ULTRA.CC_HOLDERS insert error - '.$customer_id );

    $sql = sprintf("
  INSERT INTO ULTRA.CC_HOLDER_TOKENS
(
  CC_HOLDERS_ID,
  GATEWAY,
  MERCHANT_ACCOUNT,
  TOKEN,
  ENABLED
)
SELECT
  CC_HOLDERS_ID,
  'MeS',
  '1  ',
  '%s',
  1
FROM  ULTRA.CC_HOLDERS
WHERE CUSTOMER_ID = %d
AND   ENABLED     = 1
      ",
      $token,
      $customer_id
    );

    $success = run_sql_and_check($sql);

    if ( ! $success )
      throw new \Exception( 'ULTRA.CC_HOLDER_TOKENS insert error - '.$customer_id );
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());

    $success = FALSE;
  }

  return $success;
}

teldata_change_db();


# sean's snippet :

foreach(file("mes_response.txt") as $line)
{
  $line = str_replace('transaction_id=', '', $line);
  $line = str_replace('&error_code=', '|', $line);
  $line = str_replace('&auth_response_text=', '|', $line);
  $line = str_replace('&eresp_customer_id=', '|', $line);
  $line = str_replace('&eresp_exp=', '|', $line);
  $line = str_replace('&eresp_last4=', '|', $line);
  $line = str_replace('&eresp_bin=', '|', $line);



  $ccArray = explode('|', $line);

  $token = $ccArray[0];
  $errorCode = $ccArray[1];
  $authResponseText = $ccArray[2];
  $customerId = $ccArray[3];
  $exp = $ccArray[4] ;
  $last4 = $ccArray[5];
  $ccBin = $ccArray[6];


  if ( $errorCode == '000' )
  {

    if ( createCCRows($token,$customerId,$ccBin,$last4,$exp) )
      dlog('','success');
    else
      dlog('','failure');
  }
}


/*

# raf's test :

list($token, $customer_id, $bin, $last_four, $expire_date) = array(
  'test_token',244,'654321','4321','1122'
);

$b = createCCRows($token, $customer_id, $bin, $last_four, $expire_date);

if ( $b ) echo "OK\n"; else echo "KO\n";

*/

?>
