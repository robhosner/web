<?php

require_once 'db.php';

teldata_change_db();

$limit = 35000;
$min   = 51;

$sql = 'SELECT top '.$limit.'
  u.CUSTOMER_ID,
  u.CURRENT_MOBILE_NUMBER,
  u.CURRENT_ICCID_FULL,
  r.CUSTOMER_ID CANCELLATION_CUSTOMER_ID,
  r.MSISDN,
  r.ICCID
-- ,
-- "x" + u.CURRENT_MOBILE_NUMBER + "x" as z,
-- "y" + u.CURRENT_ICCID_FULL    + "y" as q
FROM
  HTT_CUSTOMERS_OVERLAY_ULTRA u
LEFT OUTER JOIN
  HTT_CANCELLATION_REASONS    r
ON r.CUSTOMER_ID = u.CUSTOMER_ID
  WHERE
u.CUSTOMER_ID > '.$min.'
  AND
PLAN_STATE = "Cancelled"
  AND
(
( ( CURRENT_MOBILE_NUMBER IS NOT NULL ) AND ( CURRENT_MOBILE_NUMBER != "" ) AND ( CURRENT_MOBILE_NUMBER != " " ) )
OR
( ( CURRENT_ICCID         IS NOT NULL ) AND ( CURRENT_ICCID         != "" ) AND ( CURRENT_ICCID         != " " ) )
) ';

$data = mssql_fetch_all_objects(logged_mssql_query($sql));

foreach ( $data as $row )
  fix_htt_cancellation_reasons( $row );


function fix_htt_cancellation_reasons( $row )
{
  if ( $row->CURRENT_MOBILE_NUMBER || $row->CURRENT_ICCID_FULL )
    print_r($row);

  if ( cancellation_matches( $row ) )
    nullify_cancelled_ultra_data( $row );
  else
    fix_cancelled_ultra_data( $row );
}


function cancellation_matches( $row )
{
  return
  (
    ( $row->MSISDN && ! $row->CURRENT_MOBILE_NUMBER )
    &&
    ( $row->ICCID  == $row->CURRENT_ICCID_FULL )
  );
}


function nullify_cancelled_ultra_data( $row )
{
  dlog('',"nullify_cancelled_ultra_data ".$row->CUSTOMER_ID);

  $sql =
    'update htt_customers_overlay_ultra set current_iccid=NULL , CURRENT_ICCID_FULL=NULL , current_mobile_number=NULL where customer_id = '.
    $row->CUSTOMER_ID;

  if ( run_sql_and_check( $sql ) ) echo "SUCCESS\n"; else echo "FAILURE\n";
}


function fix_cancelled_ultra_data( $row )
{
  if ( !$row->CANCELLATION_CUSTOMER_ID )
    add_to_htt_cancellation_reasons( $row );
  else
    fix_htt_cancellation_reasons_row( $row );
}


function add_to_htt_cancellation_reasons( $row )
{
  $check = htt_cancellation_reasons_add(
    array(
      'customer_id' => $row->CUSTOMER_ID,
      'reason'      => 'MVNO-2460',
      'msisdn'      => $row->CURRENT_MOBILE_NUMBER,
      'iccid'       => $row->CURRENT_ICCID_FULL,
      'type'        => 'CANCELLED',
      'cos_id'      => '0',
      'status'      => 'Cancelled',
      'agent'       => 'fix_htt_cancellation_reasons'
    )
  );

  if ( $check ) echo "SUCCESS\n"; else echo "FAILURE\n";
}


function fix_htt_cancellation_reasons_row( $row )
{
  if ( $row->CURRENT_MOBILE_NUMBER || $row->CURRENT_ICCID_FULL )
  {
    if
    (
      ( $row->CURRENT_MOBILE_NUMBER != $row->MSISDN )
      ||
      ( $row->CURRENT_ICCID_FULL    != $row->ICCID  )
    )
      update_htt_cancellation_reasons( $row );
    else
      nullify_cancelled_ultra_data( $row );
  }
}


function update_htt_cancellation_reasons( $row )
{
  $set_clause = '';
  $and = '';

  if ( $row->CURRENT_MOBILE_NUMBER )
  {
    $set_clause .= $and.' MSISDN = "'.$row->CURRENT_MOBILE_NUMBER.'"';
    $and = ' , ';
  }

  if ( $row->CURRENT_ICCID_FULL )
  {
    $set_clause .= $and.' ICCID = "'.$row->CURRENT_ICCID_FULL.'"';
    $and = ' , ';
  }

  $sql = 'update HTT_CANCELLATION_REASONS set '.$set_clause.' where CUSTOMER_ID = '.$row->CUSTOMER_ID;

  $check = run_sql_and_check( $sql );

  if ( $check ) echo "SUCCESS\n"; else echo "FAILURE\n";
  if ( $check ) nullify_cancelled_ultra_data( $row );
}


?>
