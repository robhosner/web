<?php

// DB connection tests

include_once('db.php');

// connect to $argv[1] DB

if ( ! empty( $argv[1] ) )
{
  putenv("OVERRIDE_DB_NAME=".$argv[1]);

  $link = connect_db(
    array(
      'db_name' => $argv[1]
    )
  );

  $sql = 'select GETUTCDATE()';

  $result = mssql_fetch_all_rows(logged_mssql_query($sql));

  print_r($result);

  exit;
}

// connect to default DB

teldata_change_db();

$sql = 'select top 1 customer_id from accounts order by customer_id desc';

$result = mssql_fetch_all_rows(logged_mssql_query($sql));

print_r($result);

$result = mssql_fetch_all_flatten(logged_mssql_query($sql));

print_r($result);

// connect to prod DB

putenv("OVERRIDE_DB_NAME=ULTRA_DATA");

$link = connect_db(
    array(
      'db_name' => 'ULTRA_DATA'
    )
  );

$sql = 'select top 1 customer_id from accounts order by customer_id desc';

$result = mssql_fetch_all_rows(logged_mssql_query($sql));

print_r($result);

// connect to ULTRA_ACC

\Ultra\Lib\DB\ultra_acc_connect();

$sql = 'select top 1 * from PORTIN_QUEUE';

$result = mssql_fetch_all_rows(logged_mssql_query($sql));

print_r($result);


