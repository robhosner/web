use strict;
use warnings;

use Devel::Size qw(total_size);

my $foo = {a => [1, 2, 3],
           b => {a => [1, 3, 4]}
          };

my $total_size = total_size($foo);


print STDOUT "total_size = $total_size\n";

__END__

