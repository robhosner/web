<?php


include_once('db.php');
include_once('lib/messaging/functions.php');
include_once('classes/postageapp.inc.php');
include_once('sms.php');
include_once('web.php');
require_once 'Ultra/Lib/MQ/ControlChannel.php';
require_once 'classes/Messenger.php';
require_once 'Ultra/Messaging/Templates.php';


date_default_timezone_set("America/Los_Angeles");


$test_email = isset($argv[2]) ? $argv[2] : 'vtarasov@ultra.me';


teldata_change_db(); // connect to the DB


# Usage:

# to test PostageApp::mail
# php test/messaging/test_functions.php PostageApp_mail

# to test lib/messaging/functions.php::send_postage_app_mail
# php test/messaging/test_functions.php send_postage_app_mail

# to test sms.php::send_silverstreet_sms
# php test/messaging/test_functions.php send_silverstreet_sms

# php test/messaging/test_functions.php funcSendCustomerEmail

# php test/messaging/test_functions.php send_activation_email CUSTOMER_ID

# php test/messaging/test_functions.php enqueue_daytime_sms						OK

# php test/messaging/test_functions.php enqueue_immediate_sms $TEMPLATE

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_account_created		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_sim_shipped			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_activated			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_forgot_password		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_auto_recharge_on		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_auto_recharge_off		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_replacement_sim_ordered	OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_recharge_successful		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_purchase_intl			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_paygo_topup			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_plan_switched			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_replacement_sim_activated	OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_send_username			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_purchase_data			OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_portin_success		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_portin_fail_resubmit		OK

# php test/messaging/test_functions.php funcSendExemptCustomerEmail_ultra_portin_fail_denied		OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSTemplateNoFail

# php test/messaging/test_functions.php funcSendExemptCustomerSMSRecurringBoltOnsError

# php test/messaging/test_functions.php funcSendExemptCustomerSMSPromoRedeemInactive $CUSTOMER_ID

# php test/messaging/test_functions.php funcSendExemptCustomerSMSRenewalPlanFailChange $CUSTOMER_ID	OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSMonthlyPlanRenewal $CUSTOMER_ID	OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSProvisionedReminder $CUSTOMER_ID	OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSDataRecharge $CUSTOMER_ID

# php test/messaging/test_functions.php funcSendExemptCustomerSMSSelfCareUnavailable $MSISDN

# php test/messaging/test_functions.php funcSendExemptCustomerSMSImmediatePlanChange			OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSPortSuccess

# php test/messaging/test_functions.php funcSendExemptCustomerSMSPlanSuspendedDowngraded

# php test/messaging/test_functions.php funcSendExemptCustomerSMSPlanSuspendedUpgraded

# php test/messaging/test_functions.php funcSendExemptCustomerSMS

# php test/messaging/test_functions.php funcSendExemptCustomerEmail

# php test/messaging/test_functions.php funcSendCustomerSMS

# php test/messaging/test_functions.php funcSendExemptCustomerSMSUnsuspend $CUSTOMER_ID		OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSActivatePlanInfo $CUSTOMER_ID OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSActivateHelp				OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSActivateDataHelp $CUSTOMER_ID		OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSDisabling $CUSTOMER_ID			OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSMonthlyPlanChange $CUSTOMER_ID		OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSActivationNewPhone $CUSTOMER_ID	OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSTempPassword $CUSTOMER_ID		OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSTempPasswordDealer $CUSTOMER_ID

# php test/messaging/test_functions.php funcSendExemptCustomerSMSData95PercentUsed $CUSTOMER_ID		OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSDataThrottle100PercentUsed $CUSTOMER_ID

# php test/messaging/test_functions.php funcSendExemptCustomerSMSData100PercentUsed $CUSTOMER_ID

# php test/messaging/test_functions.php funcSendExemptCustomerSMSVoiceRecharge $CUSTOMER_ID $COST $BALANCE $MINUTES OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSBalanceAdd $CUSTOMER_ID $REASON $AMOUNT

# php test/messaging/test_functions.php funcSendExemptCustomerSMSVoiceRechargeError $MINUTES

# php test/messaging/test_functions.php funcSendCustomerSMS_internal

# show all SMS templates
# php test/messaging/test_functions.php showSMStemplates

# php test/messaging/test_functions.php three_cinteractive_send_message $MSISDN_LIST $MESSAGE

# php test/messaging/test_functions.php enqueue_immediate_email $CUSTOMER_ID		OK

# php test/messaging/test_functions.php enqueue_conditional_sms

# php test/messaging/test_functions.php enqueue_immediate_external_sms $CUSTOMER_ID $TEMPLATE

# php test/messaging/test_functions.php enqueue_once_immediate_external_sms

# php test/messaging/test_functions.php enqueue_once_immediate_email $CUSTOMER_ID	OK

# php test/messaging/test_functions.php send_sms_resolution_required $CUSTOMER_ID	OK

# php test/messaging/test_functions.php send_message_resolution_required $CUSTOMER_ID	OK

# php test/messaging/test_functions.php send_portin_success_email $CUSTOMER_ID		OK

# php test/messaging/test_functions.php send_portin_failure_email $CUSTOMER_ID		OK

# php test/messaging/test_functions.php SMS_by_language $MESSAGE_TYPE

# php test/messaging/test_functions.php test_mvne

# php test/messaging/test_functions.php sms_bypass

# php test/messaging/test_functions.php Messenger_reconcile_messages

# php test/messaging/test_functions.php Messenger_initialize_day

# php test/messaging/test_functions.php message_length

# php test/messaging/test_functions.php funcSendSMSBOGORedeemed $CUSTOMER_ID OK
 
# php test/messaging/test_functions.php funcSendSMSBOGOActivation $CUSTOMER_ID OK

# php test/messaging/test_functions.php funcSendExemptCustomerSMSIntlBoltOnSuccess $CUSTOMER_ID

abstract class AbstractTestStrategy
{
  abstract function test();
}


class test_customer_data
{
#  public $COS_ID      = 98274;
  public $COS_ID      = 98275;
  public $CUSTOMER_ID = 31;
  public $CUSTOMER    = '1000000025';
  public $ACCOUNT_ID  = '12344321';
  public $ACCOUNT     = '43211234';
  public $FIRST_NAME  = 'John';
  public $LAST_NAME   = 'Doe';
  public $COMPANY     = 'Google';
  public $ADDRESS1    = '123 Abe Ave';
  public $ADDRESS2    = '1A';
  public $CITY        = 'New York';
  public $STATE_REGION  = 'NY';
  public $COUNTRY       = 'USA';
  public $POSTAL_CODE   = '10100';
  public $LOCAL_PHONE   = '1000000025';
  public $E_MAIL        = 'rgalli@ultra.me';
  public $CC_NUMBER     = '1234';
  public $CC_EXP_DATE   = '';
  public $ACCOUNT_GROUP_ID  = '1234';
  public $CCV               = '1234';
  public $PLAN_EXPIRES = '1/2/2012';
  public $LOGIN_NAME     = 'qwerty';
  public $LOGIN_PASSWORD = 'test123';
  public $cos            = 'plan ultra';
  public $plan_expires   = 'Oct  2 2012 08:42:08:327PM';
  public $MONTHLY_RENEWAL_TARGET = 'L49';
  public $current_mobile_number  = 1001001000;
    #13474004455;
    #18082055024;
    #11001001000;
    #18082055026;

  public function __construct($customer_id = NULL)
  {
    if ($customer_id)
    {
      $customer = get_customer_from_customer_id($customer_id);
      if ($customer)
        foreach ($this as $property => $value)
          if ( ! empty($customer->$property))
            $this->$property = $customer->$property;
    }
    global $test_email;
    $this->E_MAIL = $test_email;
  }
}


class Test_L19_unlimited extends AbstractTestStrategy
{
  function test()
  {
/*
php test/messaging/test_functions.php L19_unlimited $min $max
*/
    global $argv;

    require_once 'Ultra/Lib/Util/Redis.php';

    $ultra_redis = new \Ultra\Lib\Util\Redis();

    $redis_key_prefix = 'sms/L19_unlimited/msisdn/';
    $message_type     = 'L19_unlimited';
    $messages         = array(
      'EN' => 'We upgraded the $19 plan to unlimited international talk. Your account now has unlimited talk minutes to US and 10 countries. Learn more at www.ultra.me/updates',
      'ES' => 'Hemos actualizado el plan $19 a tener Llamadas INTL Ilimitadas. Tu cuenta ahora tiene llamadas ilimitadas a EE.UU. y 10 paises. Mas info en Ultra.me/updates'
    );
    $file_name        = '/home/rgalli/19_es_unlimited.csv';
    $sleep_seconds    = 1;
    $cycles           = $argv[2];

    $sql_template = " select current_mobile_number, preferred_language, CUSTOMER_ID from htt_customers_overlay_ultra where current_mobile_number = ";

    $list = file($file_name);

    shuffle($list);

    $count = 0;

    teldata_change_db();

    foreach ( $list as $msisdn )
    {
      $msisdn = trim($msisdn);
      $msisdn =  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $msisdn);

      if ( $msisdn && ( $count < $cycles ) )
      {
        $count++;

        if ( ! $ultra_redis->get( $redis_key_prefix.$msisdn ) )
        {
          dlog('',"Test_L19_unlimited sending to $msisdn");

          $sql = $sql_template."'".$msisdn."'";

          $data = mssql_fetch_all_objects(logged_mssql_query($sql));

          dlog('',"data = %s",$data);

          if ( ! $data || ! is_array($data) || ! count($data) )
            dlog('',"could not find $msisdn in DB");
          elseif ( ! $ultra_redis->get( $redis_key_prefix.$msisdn ) )
          {
            if ( $data[0]->preferred_language == 'ES' )
            {
              $message = $messages[ $data[0]->preferred_language ];

              $ultra_redis->set( $redis_key_prefix.$msisdn , 1 , 60 * 60 * 168 ); // 1 week

              $r = funcSendCustomerSMS_internal(
                array(
                  'customer'   => $data[0],
                  'message'    => $message,
                  'client_tag' => 'client_tag'
                )
              );

              dlog('',"result = %s",$r);

              if ( $r['sent'] )
              {
                echo "success\n";
                dlog('',"success");
              }
              else
              {
                echo "failure\n";
                dlog('',"failure");
              }

              dlog('',"sleeping $sleep_seconds seconds");

              sleep($sleep_seconds);
            }
          }
        }
      }
    }
  }
}


class Test_Cuba_57_cents_per_minute extends AbstractTestStrategy
{
  function test()
  {
/*
php test/messaging/test_functions.php Cuba_57_cents_per_minute $CYCLES
*/

#'Great news! We have lowered the cost to call Cuban land and mobiles to 57 cents per minute. Talk more for less with Ultra Mobile.'
#'Buenas noticias! Hemos reducido el costo para llamar a lineas fijas y los moviles de Cuba a 57 centavos por minuto. Hable mas por menos con Ultra Mobile.'

    global $argv;

    require_once 'Ultra/Lib/Util/Redis.php';

    $ultra_redis = new \Ultra\Lib\Util\Redis();

    $redis_key_prefix = 'Cuba_57_cents_per_minute/customer_id/';
    $message_type = 'Cuba_57_cents_per_minute';
    $messages         = array(
      'EN' => 'Great news! We have lowered the cost to call Cuban land and mobiles to 57 cents per minute. Talk more for less with Ultra Mobile.',
      'ES' => 'Buenas noticias! Hemos reducido el costo para llamar a lineas fijas y los moviles de Cuba a 57 centavos por minuto. Hable mas por menos con Ultra Mobile.'
    );

    $sql_template = " select current_mobile_number, preferred_language, CUSTOMER_ID from htt_customers_overlay_ultra where current_mobile_number = ";

    $cycles           = $argv[2];

    $sleep_seconds = 2;

    $file_name = '/home/rgalli/cuba.txt';

    $list = file($file_name);

    shuffle($list);

    $count = 0;

    teldata_change_db();

    foreach ( $list as $msisdn )
    {
      $msisdn = trim($msisdn);
      $msisdn =  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $msisdn);

      if ( $msisdn && ( $count < $cycles ) )
      {
        $count++;

        if ( ! $ultra_redis->get( $redis_key_prefix.$msisdn ) )
        {
          dlog('',"Cuba_57_cents_per_minute sending to $msisdn");

          $sql = $sql_template."'".$msisdn."'";

          $data = mssql_fetch_all_objects(logged_mssql_query($sql));

          dlog('',"data = %s",$data);

          if ( ! $data || ! is_array($data) || ! count($data) )
            dlog('',"could not find $msisdn in DB");
          elseif ( ! $ultra_redis->get( $redis_key_prefix.$msisdn ) )
          {
echo "Cuba_57_cents_per_minute sending to $msisdn\n";

              $message = $messages[ $data[0]->preferred_language ];

              $ultra_redis->set( $redis_key_prefix.$msisdn , 1 , 60 * 60 * 168 ); // 1 week

              $r = funcSendCustomerSMS_internal(
                array(
                  'customer'   => $data[0],
                  'message'    => $message,
                  'client_tag' => 'client_tag'
                )
              );

              dlog('',"result = %s",$r);

              if ( $r['sent'] )
              {
                echo "success\n";
                dlog('',"success");
              }
              else
              {
                echo "failure\n";
                dlog('',"failure");
              }

              dlog('',"sleeping $sleep_seconds seconds");

              sleep($sleep_seconds);

          }

        }
      }
    }
  }
}


class Test_julyplanrefresh extends AbstractTestStrategy
{
  function test()
  {
/*
php test/messaging/test_functions.php julyplanrefresh 62 100
*/

    global $argv;

    require_once 'Ultra/Lib/Util/Redis.php';

    $ultra_redis = new \Ultra\Lib\Util\Redis();

    $SMS_templates = \Ultra\Messaging\Templates\SMS_templates( '2' );

    $redis_key_prefix = 'julyplanrefresh/customer_id/';
    $message_type = 'julyplanrefresh';

    $sleep_seconds = 2;

    dlog('',$ultra_redis->get('julyplanrefresh_count')." <- julyplanrefresh_count");

    $min = $argv[2];
    $max = $argv[3];

    $sql = "
select 
       a.current_mobile_number,preferred_language,a.CUSTOMER_ID
from   htt_customers_full a, htt_customers_overlay_ultra b
where  a.customer_id=b.customer_id
and    a.plan_state='Active'
and    preferred_language='EN'
and    a.current_mobile_number is not null
and    a.current_mobile_number != ''
and    a.CUSTOMER_ID <= $max
and    a.CUSTOMER_ID >= $min
and    a.CUSTOMER_ID != 667
union all
select 
       a.current_mobile_number,preferred_language,a.CUSTOMER_ID
from   htt_customers_full a, htt_customers_overlay_ultra b
where  a.customer_id=b.customer_id
and    a.plan_state='Suspended'
and    preferred_language='EN'
and    a.plan_expires>='01-JUL-2014'
and    a.current_mobile_number is not null
and    a.current_mobile_number != ''
and    a.CUSTOMER_ID <= $max
and    a.CUSTOMER_ID >= $min
and    a.CUSTOMER_ID != 667
";

    $data = mssql_fetch_all_objects(logged_mssql_query($sql));

    foreach( $data as $row )
    {
      dlog('',"%s",$row);

      if ( ! $ultra_redis->get( $redis_key_prefix.$row->current_mobile_number ) )
      {
        $ultra_redis->set( $redis_key_prefix.$row->current_mobile_number , 1 , 60 * 60 * 48 );

        $message = $SMS_templates[ $message_type . '__' . $row->preferred_language ];

        dlog('',"sending julyplanrefresh to ".$row->current_mobile_number);
        echo "sending julyplanrefresh to (".$row->CUSTOMER_ID.") ".$row->current_mobile_number."\n";

        $r = funcSendCustomerSMS_internal(
          array(
            'customer'   => $row,
            'message'    => $message,
            'client_tag' => 'client_tag'
          )
        );

        if ( $r['sent'] )
        {
          echo "success\n";
          dlog('',"success");
          $ultra_redis->incr('julyplanrefresh_count');
        }
        else
        {
          echo "failure\n";
          dlog('',"failure");
        }

        dlog('',"sleeping $sleep_seconds seconds");

        sleep($sleep_seconds);
        #print_r($r);
      }
    }

    exit;
  }
}

class Test_funcSendCustomerSMS_internal extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

/*
php test/messaging/test_functions.php funcSendCustomerSMS_internal 1001001000 EN 'julyplanrefresh'
*/

    $customers = get_ultra_customers_from_msisdn($argv[2],array('CUSTOMER_ID','current_mobile_number'));

    if ( !$customers || !is_array($customers) || !count($customers) )
      die('No customer found');

    $message_type = $argv[4];
    $language = $argv[3];

    $SMS_templates = \Ultra\Messaging\Templates\SMS_templates( '2' );

    $message = $SMS_templates[ $message_type . '__' . $language ];

    $r = funcSendCustomerSMS_internal(
      array(
        'customer'   => $customers[0],
        'message'    => $message,
        'client_tag' => 'client_tag'
      )
    );

    print_r($r);
  }
}


class Test_funcSendExemptCustomerSMSPromotionCredit extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    teldata_change_db(); // connect to the DB

    $customer = get_customer_from_msisdn( $argv[2] );
    if ( ! $customer )
      throw new \Exception("ERR_API_INTERNAL: MSISDN not found.");

    $r = funcSendExemptCustomerSMSPromotionCredit( array('customer'=>$customer , 'amount' => '2.50' ) );

    print_r($r);
  }
}


class Test_funcSendExemptCustomerSMSPromotionNotEligible extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    teldata_change_db(); // connect to the DB

    $customer = get_customer_from_msisdn( $argv[2] );
    if ( ! $customer )
      throw new \Exception("ERR_API_INTERNAL: MSISDN not found.");

    $r = funcSendExemptCustomerSMSPromotionNotEligible( array('customer'=>$customer) );

    print_r($r);
  }
}


class Test_funcSendExemptCustomerSMSPromotionBlock extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    teldata_change_db(); // connect to the DB
    
    $customer = get_customer_from_msisdn( $argv[2] );
    if ( ! $customer )
      throw new \Exception("ERR_API_INTERNAL: MSISDN not found.");

    $r = funcSendExemptCustomerSMSPromotionBlock( array('customer'=>$customer) );

    print_r($r);
  }
}


class Test_funcSendExemptCustomerSMSPromotionInactive extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
  
    teldata_change_db(); // connect to the DB

    $customer = get_customer_from_msisdn( $argv[2] );
    if ( ! $customer )
      throw new \Exception("ERR_API_INTERNAL: MSISDN not found.");

    $r = funcSendExemptCustomerSMSPromotionInactive( array('customer'=>$customer) );

    print_r($r);
  }
}


class Test_send_sms_resolution_required extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = send_sms_resolution_required( $argv[2] );

    print_r($r);
  }
}


class Test_enqueue_once_immediate_email extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_customer_from_customer_id($argv[2]);

    $r = enqueue_once_immediate_email(
           $argv[2],
           'ultra-forgot-password',
           array(
             'email'         => $customer->E_MAIL,
             'first_name'    => $customer->FIRST_NAME,
             'last_name'     => $customer->LAST_NAME,
             'new_password'  => 'fuppetsfromhell'
           ),
           2
    );

    print_r($r);
  }
}


class Test_enqueue_once_immediate_external_sms extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = enqueue_once_immediate_external_sms(
           $argv[2],
           'port_resolution_required',
           array(),
           2
    );

    print_r($r);
  }
}


class Test_enqueue_immediate_external_sms extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = enqueue_immediate_external_sms(
      $argv[2],
      $argv[3],
      array()
    );

    print_r($r);
  }
}


class Test_test_mvne extends AbstractTestStrategy
{
  function test()
  {
    $languages     = array('EN','ES');
    $message_types = array('activate_help','balanceadd_reload','data_block_100_percent','data_throttle_100_percent','data_95_percent','voice_notification','voice_exhausted');

    foreach ( $message_types as $message_type )
    {
        foreach ( $languages as $language )
        {
          $params  = array(
            'message_type' => $message_type,
            'gizmo_amount' => '$49.00'
          );

          $message = \Ultra\Messaging\Templates\SMS_by_language( $params , $language );

          echo "Message = $message\n";
        }
    }

    echo "\n";
  }
}


class Test_enqueue_immediate_sms extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    $test_customer = new test_customer_data;

    $caption_amount = '29.00';

    $r = enqueue_immediate_sms(
           18691,
           $argv[2], // template
           array(
            'plan_amount'       => '19',
            'old_plan_amount'   => '29',
            'new_plan_amount'   => '19',
            'active_days'       => '30',
            'renewal_date'      => 'Jan 2nd',
            'reload_amount'     => '$'.$caption_amount,
            'gizmo_amount'      => '$'.$caption_amount
           )
    );

    print_r($r);
  }
}

class Test_enqueue_conditional_sms extends AbstractTestStrategy
{
  function test()
  {

    $customer_id = 3395; // ZIP 11211, New York City

    // daytime = NULL (FALSE)
    $r = enqueue_conditional_sms(
      $customer_id,
      'voice_recharged', // priority 1
      2,
      array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => 29));
    print_r($r);

    // daytime = TRUE
    $r = enqueue_conditional_sms(
      $customer_id,
      'voice_recharged', // priority 1
      2,
      array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => 29),
      TRUE);
    print_r($r);

    // daytime = NULL (FALSE)
    $r = enqueue_conditional_sms(
      $customer_id,
      'plan_provisioned_reminder', // priority 3
      2,
      array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => 29));
    print_r($r);

    // daytime = TRUE
    $r = enqueue_conditional_sms(
      $customer_id,
      'plan_provisioned_reminder', // priority 3
      2,
      array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => 29),
      TRUE);
    print_r($r);

    // daytime = NULL (FALSE)
    $r = enqueue_conditional_sms(
      $customer_id,
      'voice_recharged', // priority 1
      6, // 6 hours delay
      array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => 29));
    print_r($r);

    // daytime = TRUE
    $r = enqueue_conditional_sms(
      $customer_id,
      'voice_recharged', // priority 1
      6, // 6 hours delay
      array(
        'message_type'  => 'plan_provisioned',
        'plan_amount'   => 29),
      TRUE);
    print_r($r);  
  }
}

class Test_funcSendExemptCustomerSMSPromoRedeemInactive extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSPromoRedeemInactive(
      array(
        'customer_id' => $test_customer->CUSTOMER_ID
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSRenewalPlanFailChange extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSRenewalPlanFailChange(
      array(
        'customer_id' => 19032
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSPlanSuspendedChanged extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSPlanSuspendedChanged(
      array(
        'customer_id' => 19021,
        'plan_cost' => 19,
        'sms_template' => 'account_suspended'
      )
    );

    print_r($r);

     $r = funcSendExemptCustomerSMSPlanSuspendedChanged(
      array(
        'customer' => $test_customer,
        'plan_cost' => 19,
        'sms_template' => 'account_suspended'
      )
    );

    print_r($r);


  }
}

class Test_funcSendExemptCustomerSMSPortSuccess extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSPortSuccess(
      array(
        'customer' => $test_customer
      )
    );

    print_r($r);
  }
}

class Test_showSMStemplates extends AbstractTestStrategy
{
  function test()
  {
    $tlist = array(
      'activate_newphone',
      'activate_portin',
      'activate_help',
      'activate_datahelp',
      'activate_planinfo',
      'activate_provision',
      'balanceadd_recharge',
      'balanceadd_reload',
      'balanceadd_resume',
      'auto_monthly_cc_renewal',
      'monthly_renewal',
      'renewal_plan_change',
      'plan_close_to_expiration',
      'plan_expires_today',
      'plan_suspended_now',
      'plan_suspended_two_days_ago',
      'renewal_plan_fail_change'
    );

    $tparams = array(
      'msisdn'        => 1001001000,
      'renewal_date'  => 'Jul 1st',
      'plan_name'     => 'Ultra $29',
      'plan_amount'   => '29',
      'plan_amount_upgrade' => '49',
      'gizmo_amount'  => '$30',
      'reload_amount' => '$29',
      'login_token'   => '0DcOFwxBsmlMZQ'
    );

    foreach($tlist as $template)
    {
      $tparams['message_type'] = $template;

      $message = \Ultra\Messaging\Templates\SMS_by_language($tparams);

      echo "Template: $template\n";
      echo "Message:  $message\n\n";
    }
  }
}


class Test_send_message_resolution_required extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = send_message_resolution_required($argv[2]);

    print_r($r);
  }
}


class Test_send_portin_success_email extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = send_portin_success_email($argv[2]);

    print_r($r);
  }
}


class Test_send_portin_failure_email extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = send_portin_failure_email($argv[2]);

    print_r($r);
  }
}


class Test_funcSendExemptCustomerSMSPlanSuspendedDowngraded extends AbstractTestStrategy
{
  function test()
  {

    $r = funcSendExemptCustomerSMSPlanSuspendedDowngraded(
      array(
        'customer_id'     => 3561,
        'plan_cost'       => 19
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSPlanSuspendedUpgraded extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSPlanSuspendedUpgraded(
      array(
        'customer'     => $test_customer
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSImmediatePlanChange extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSImmediatePlanChange(
      array(
        'customer'     => $test_customer,
        'plan_from'    => 'L29',
        'plan_to'      => 'L49'
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSActivationNewPhone extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $test_customer = get_customer_from_customer_id($argv[2]);

    $r = funcSendExemptCustomerSMSActivationNewPhone(
      array(
        'customer'     => $test_customer
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSProvisionedReminder extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSProvisionedReminder(
      array(
        "customer_id"   => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSMonthlyPlanRenewal extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSMonthlyPlanRenewal(
      array(
        "customer_id"   => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSMonthlyPlanChange extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSMonthlyPlanChange(
      array(
        "customer_id" => $argv[2],
        'plan_to'     => 'L49'
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSBalanceAdd extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = $argv[2];
    $reason      = $argv[3];
    $amount      = $argv[4];

    $caption_amount = caption_amount($amount);

    $query = make_find_ultra_customer_query_from_customer_id($customer_id);

    $customer = find_customer( $query );

    $r = funcSendExemptCustomerSMSBalanceAdd(
      array(
        'customer'      => $customer,
        'sms_template'  => ( $reason == 'ULTRA_PLAN_RECHARGE' ) ? 'balanceadd_recharge' : 'balanceadd_reload' ,
        'renewal_date'  => get_date_from_full_date( $customer->plan_expires_effective ),
        'reload_amount' => '$'.$caption_amount,
        'gizmo_amount'  => '$'.$caption_amount
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSVoiceRechargeError extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSVoiceRechargeError(
      array(
        'customer_id'    => $argv[2],
        "minutes"        => $argv[3]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSVoiceRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSVoiceRecharge(
      array(
        'customer_id'    => $argv[2],
        "cost_amount"    => $argv[3],
        "wallet_balance" => $argv[4],
        "minutes"        => $argv[5]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSData100PercentUsed extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSData100PercentUsed(
      array(
        'customer_id'   => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSDataThrottle100PercentUsed extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSDataThrottle100PercentUsed(
      array(
        'customer_id'   => $argv[2]
      )
    );

    print_r($r);
  }
}


class Test_funcSendExemptCustomerSMSData95PercentUsed extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSData95PercentUsed(
      array(
        'customer_id'   => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSTempPasswordDealer extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSTempPasswordDealer(
      array(
        'customer_id'   => $argv[2],
        'temp_password' => 'abc123'
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSTempPassword extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSTempPassword(
      array(
        'customer_id'   => $argv[2],
        'temp_password' => 'abc123'
      )
    );

    print_r($r);
  }
}

class Test_three_cinteractive_send_message extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $msisdn_list = explode(",",$argv[2]);
    $message     = $argv[3];

    print_r($msisdn_list);
    print_r($message);

    foreach($msisdn_list as $id => $n)
    {
      $r = three_cinteractive_send_message(
        array(
          'msisdn'     => trim($n),
          'message'    => $message,
          'client_tag' => 'client_tag'
        )
      );

      print_r($r);
    }
  }
}

class Test_funcSendExemptCustomerSMSDataRecharge extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSDataRecharge(
      array(
        'customer' => $test_customer,
        'data_MB'  => 50
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSUnsuspend extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMSUnsuspend(
      array(
        'customer_id' => $test_customer->CUSTOMER_ID
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSActivatePlanInfo extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_customer_from_customer_id($argv[2]);

    $r = funcSendExemptCustomerSMSActivatePlanInfo(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'cos_id'      => $customer->COS_ID
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSActivateHelp extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_customer_from_customer_id($argv[2]);

    $r = funcSendExemptCustomerSMSActivateHelp(
      array(
        'customer_id' => $customer->CUSTOMER_ID
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSJulyPlanRefresh extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

print_r($argv);

    $customers = get_ultra_customers_from_msisdn($argv[2],array('CUSTOMER_ID','current_mobile_number'));

    if ( !$customers || !is_array($customers) || !count($customers) )
      die('No customer found');

    $r = funcSendExemptCustomerSMSJulyPlanRefresh(
      array(
        'customer' => $customers[0]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSActivateDataHelp extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSActivateDataHelp(
      array(
        'customer_id' => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSSelfCareUnavailable extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSSelfCareUnavailable(
      array(
        'msisdn' => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_funcSendExemptCustomerSMSDisabling extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSDisabling(
      array(
        'customer_id' => $argv[2]
      )
    );

    print_r($r);
  }
}

class Test_sms_bypass extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $command = 'SendSMS';

    $parameters = array(
      'actionUUID'        => time(),
      'MSISDN'            => '3103097855', // Vitaly's test phone @ NY
      'preferredLanguage' => 'en',
      'qualityOfService'  => '801',
      'smsText'           => 'another test '.time(),
      'UserData'          => array()
    );

    $parameters['UserData']['channelId'] = 'ULTRA';
    $parameters['UserData']['senderId']  = 'MVNEACC';
    $parameters['UserData']['timeStamp'] = getTimeStamp();
    $parameters['UserData']['timeStamp'] = preg_replace( "/Z$/" , "" , $parameters['UserData']['timeStamp'] );

    $parameters['SubscriberSMSList']['SubscriberSMS'][] = array(
      'Language' => $parameters['preferredLanguage'],
      'MSISDN'   => $parameters['MSISDN'],
      'Text'     => ''
    );

    $controlChannelObject = new \Ultra\Lib\MQ\ControlChannel;

    $outboundControlMessage = $controlChannelObject->buildMessage(
      array(
        'actionUUID' => time(),
        'header'     => $command,
        'body'       => $parameters
      )
    );

    # create Request-Reply ACC MW Control Channels
    list($outboundSMSMWControlChannel,$inboundSMSMWControlChannel) = $controlChannelObject->createNewSMSMWControlChannels();

    if ( ! $outboundSMSMWControlChannel )
      throw new \Exception("Could not create an outbound SMS MW Control Channel");

    if ( ! $inboundSMSMWControlChannel )
      throw new \Exception("Could not create an inbound SMS MW Control Channel");

    # I am a Requestor, I want to send an *outbound* message through $outboundSMSMWControlChannel,
    # then I will synchronously wait for a reply the Replier will send through $inboundSMSMWControlChannel

    $controlUUID = $controlChannelObject->sendToControlChannel( $outboundSMSMWControlChannel , $outboundControlMessage );

    if ( ! $controlUUID )
      throw new \Exception("sendToControlChannel failed (no controlUUID)");

    $inboundReplyResult = $controlChannelObject->waitForSMSMWControlMessage( $inboundSMSMWControlChannel );

    if ( ! $inboundReplyResult )
      throw new \Exception("waitForSMSMWControlMessage failed (no inboundReplyResult)");

    dlog('',"inboundReplyResult = %s",$inboundReplyResult);

    if ( ! isset($inboundReplyResult->data_array['message']) )
      throw new \Exception("waitForSMSMWControlMessage failed (no inboundReplyMessage)");

    $inboundReplyMessage = $inboundReplyResult->data_array['message'];

    dlog('',"inboundReplyMessage = %s",$inboundReplyMessage);

    # data we obtained from the response
    $data = $controlChannelObject->extractFromMessage( $inboundReplyMessage );

    if ( ! $data )
      throw new \Exception("No data found in inbound ACC MW Control Message");

    dlog('',"data extracted from message = %s",$data);

    exit;
  }
}

class Test_send_sms_bolt_on_data_immediate extends AbstractTestStrategy
{
  function test()
  {
    $r = send_sms_bolt_on_data_immediate( 31 );

    print_r($r);

    exit;
  }
}

class Test_send_sms_bolt_on_data_recurring extends AbstractTestStrategy
{
  function test()
  {
    $r = send_sms_bolt_on_data_recurring( 31 );

    print_r($r);

    exit;
  }
}

class Test_funcSendExemptCustomerSMSTemplateNoFail extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = funcSendExemptCustomerSMSTemplateNoFail( $argv[2] , $argv[3] );

    print_r($r);

    exit;
  }
}

class Test_funcSendExemptCustomerSMSRecurringBoltOnsError extends AbstractTestStrategy
{
  function test()
  {
    $r = funcSendExemptCustomerSMSRecurringBoltOnsError(
      array(
        'customer_id' => 31,
        'customer'    => get_customer_from_customer_id( 31 ),
        'bolt_ons'    => 'UpIntl'
      )
    );

    print_r($r);

    exit;
  }
}

class Test_funcSendExemptCustomerSMS extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendExemptCustomerSMS(
      array(
        'message_type' => 'activation',
        'customer'     => $test_customer,
        'phone_number' => $test_customer->current_mobile_number
      )
    );

    print_r($r);
  }
}


class Test_funcSendExemptCustomerEmail extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $template_params = array(
      'email'      => $test_customer->E_MAIL,
      'first_name' => 'test first_name',
      'cert'       => 12345678
    );

    $r = funcSendExemptCustomerEmail(
      array(
        'customer'        => $test_customer,
        'template_params' => $template_params,
        'template_name'   => 'Restaurant',
        'postage_app_key' => POSTAGE_API_KEY_RESTAURANT
      )
    );

    print_r($r);
  }
}


class Test_funcSendCustomerSMS extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $r = funcSendCustomerSMS(
      array(
        'gizmo_amount' => '$30',
        'type'         => 'internal',
        'message_type' => 'balanceadd_reload',
        'customer'     => $test_customer
      )
    );

    print_r($r);
  }
}


class Test_enqueue_daytime_sms extends AbstractTestStrategy
{
  function test()
  {
    $customer_id  = 1932; // PT
    $message_type = 'porting'; // priority 1
    $messaging_queue_params = array();

    $r = enqueue_daytime_sms(
      $customer_id,$message_type,$messaging_queue_params
    );

    print_r($r);

    $message_type = 'plan_provisioned_reminder'; // priority 3
    $r = enqueue_daytime_sms(
      $customer_id,$message_type,$messaging_queue_params
    );

    print_r($r);
    
  }
}


class Test_enqueue_immediate_email extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer = get_customer_from_customer_id($argv[2]);

    $r = enqueue_immediate_email(
      $customer->CUSTOMER_ID,
      'ultra-forgot-password',
      array(
        'email'         => $customer->E_MAIL,
        'first_name'    => $customer->FIRST_NAME,
        'last_name'     => $customer->LAST_NAME,
        'new_password'  => 'sword-shield'
      )
    );

    print_r($r);
  }
}


class Test_SMS_by_language extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $message_type = $argv[2];

    foreach (array('EN', 'ES', 'ZH') as $language)
    {
      $r = \Ultra\Messaging\Templates\SMS_by_language(
        array(
          'message_type' => $message_type,
          'msisdn'       => '1234567890'
        ),
        $language
      );
  
      echo "$language: $r\n";
    }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_portin_fail_denied extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_portin_fail_denied(
      array(
        'customer'     => $test_customer
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_portin_fail_resubmit extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_portin_fail_resubmit(
      array(
        'customer'     => $test_customer
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_portin_success extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_portin_success(
      array(
        'customer'     => $test_customer
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_auto_recharge_off extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_auto_recharge_off(
      array(
        'customer'     => $test_customer,
        'plan_name'    => 'TEST_PLAN_NAME',
        'plan_cost'    => 'test cost',
        'renewal_date' => '11/11/1111'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_auto_recharge_on extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_auto_recharge_on(
      array(
        'customer'     => $test_customer,
        'plan_name'    => 'TEST PLAN',
        'plan_cost'    => 33,
        'renewal_date' => '11/11/11'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_forgot_password extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_forgot_password(
      array(
        'customer'     => $test_customer,
        'new_password' => 'zwordfish'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_activated extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data(2466);

    $success = funcSendExemptCustomerEmail_ultra_activated(array('customer' => $test_customer), $test_customer->COS_ID);

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_replacement_sim_ordered extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_replacement_sim_ordered(
      array(
        'customer'        => $test_customer,
        'tracking_number' => '11223344'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_recharge_successful extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_recharge_successful(
      array(
        'customer'        => $test_customer,
        'amount'          => '12.34',
        'recharge_date'   => '11/11/1111',
        'renewal_date'    => '12/12/1111'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_purchase_intl extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_purchase_intl(
      array(
        'customer'       => $test_customer,
        'amount'         => '12.55',
        'order_date'     => '11/11/1111'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_sim_shipped extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_sim_shipped(
      array(
        'customer'        => $test_customer,
        'tracking_number' => 123456
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_plan_switched extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_plan_switched(
      array(
        'customer'       => $test_customer,
        'old_plan_name'  => 'Olde planne',
        'new_plan_name'  => 'newest plan',
        'plan_cost'      => '11.22',
        'renewal_date'   => '11/11/2013'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_replacement_sim_activated extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_replacement_sim_activated(
      array(
        'customer'       => $test_customer,
        'plan_name'      => 'newest plan',
        'renewal_date'   => '11/11/2013'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_send_username extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_send_username(
      array(
        'customer'       => $test_customer,
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_purchase_data extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_purchase_data(
      array(
        'customer'       => $test_customer,
        'amount'         => '12.55',
        'order_date'     => '11/11/2222',
        'renewal_date'   => '22/22/1111'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerSMSProjectW extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $success = \funcSendExemptCustomerSMSProjectW( $argv[2] );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_paygo_topup extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_paygo_topup(
      array(
        'customer'       => $test_customer,
        'amount'         => '12.55',
        'order_date'     => '11/11/2222',
        'renewal_date'   => '22/22/1111'
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_funcSendExemptCustomerEmail_ultra_account_created extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $success = funcSendExemptCustomerEmail_ultra_account_created(
      array(
        'customer' => $test_customer
      )
    );

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_send_activation_email extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = send_activation_email( $argv[2] );

    print_r($r);

    exit;
  }
}


class Test_funcSendCustomerEmail extends AbstractTestStrategy
{
  function test()
  {
    $test_customer = new test_customer_data;

    $template_params = array(
      'email'      => $test_customer->E_MAIL,
      'first_name' => 'test first_name',
      'cert'       => 12345678
    );

    $params = array(
      'customer'        => $test_customer,
      'postage_app_key' => POSTAGE_API_KEY_RESTAURANT,
      'template_params' => $template_params,
      'template_name'   => 'Restaurant'
    );

    $success = funcSendCustomerEmail($params);

    if ( $success )
    { echo "Test OK\n"; }
    else
    { echo "Test KO\n"; }
  }
}


class Test_send_silverstreet_sms extends AbstractTestStrategy
{
  function test()
  {
    $result = send_silverstreet_sms(
      array(
        'sms_dest' => 11001001000,
        'sender'   => 'IndiaLD',
        'sms_text' => 'test 1234',
        #'service'  => 'TEST',
        'service'  => 'ILD SMS',
        'dlr'      => 0
      )
    );

    print_r($result);
  }
}


class Test_send_postage_app_mail extends AbstractTestStrategy
{
  function test()
  {
    global $test_email;
    $template_name = 'Restaurant';

    $template_params = array(
      'email'      => $test_email,
      'first_name' => 'test first_name',
      'cert'       => 12345678
    );

    $success = send_postage_app_mail(
      array(
        'postage_app_key' => POSTAGE_API_KEY_RESTAURANT,
        'template_params' => $template_params,
        'template_name'   => $template_name,
        'email'           => $test_email,
        'subject'         => NULL,
        'header'          => array()
      )
    );

    if ($success)
    {
      echo "Test OK\n";
    }
    else
    {
      echo "Test KO\n";
    }

  }
}


class Test_PostageApp_mail extends AbstractTestStrategy
{
  function test()
  {
    global $test_email;

    $template_name = 'Restaurant';

    $params = array(
      'email'      => $test_email,
      'first_name' => 'test first_name',
      'cert'       => 12345678
    );

    $result = PostageApp::mail(POSTAGE_API_KEY_RESTAURANT,
                           $test_email,
                           NULL, // subject
                           $template_name, // body
                           array(), // header
                           $params);

    print_r($result);

    if ( $result->response->status == 'ok' )
    {
      echo "Test OK\n";
    }
    else
    {
      echo "Test KO\n";
    }
  }
}


class Test_Messenger_reconcile_messages extends AbstractTestStrategy
{
  function test()
  {
    $m = new Messenger();
    $m->reconcile_messages();
  }
}

class Test_Messenger_initialize_day extends AbstractTestStrategy
{
  function test()
  {
    $m = new Messenger(array('initialize_day' => TRUE));
    $m->run();
  }
}

// test all message templates for missing parameters and maximum length
class Test_message_length extends AbstractTestStrategy
{
  function test()
  {
    $params = array(
      'plan_amount_upgrade' => '39',
      'url'             =>  'https://m.ultra.me/',
      'amount'          =>  '10',
      'msisdn'          =>  '1234567890',
      'renewal_date'    =>  '12/23',
      'plan_name'       =>  'Personal $22',
      'reload_amount'   =>  '22',
      'gizmo_amount'    =>  '33',
      'bolt_ons'        =>  '10',
      'bolt_on'         =>  '10',
      'data_MBMB'       =>  '500',
      'temp_password'   =>  'abcdfrwe',
      'old_plan_amount' =>  '19',
      'new_plan_amount' =>  '29',
      'plan_amount'     =>  '39',
      'personal'        =>  'Personal',
      'active_days'     =>  '31',
      'value'           =>  '22',
      'balance'         =>  '12.50',
      'date'            =>  '12/22',
      'wallet_balance'  =>  '12.50',
      'zero_minutes'    =>  '300',
      'stored_value'    =>  '11.45',
      'remaining'       =>  '234KB',
      'usage'           =>  '455KB');
    $templates = \Ultra\Messaging\Templates\SMS_templates();
    $i = 0;
    foreach ($templates as $template => $message)
    {
      $lang = substr($template, -2);
      $sms = \Ultra\Messaging\Templates\fill_template_values($params, $message);
      if (strstr($sms, '_'))
        die("MISSING PARAM: $template -> $sms\n");
      if (strlen($sms) > 160)
        echo "TOO LONG (" . mb_strlen($sms) . "): $template -> $sms\n";
      $i++;
    }
    echo "tested $i messages\n";

  }
}

class Test_funcSendSMSBOGORedeemed extends AbstractTestStrategy
{
  function test()
  {
    $result = funcSendSMSBOGORedeemed(array('customer_id' => 19021));
    print_r($result);
    if (count($result['errors']) == 0)
      echo 'Expected VALID SUCCESS';
    else
      echo 'Expected VALID FAILURE';
    echo PHP_EOL;

    $result = funcSendSMSBOGORedeemed(array('customer_id' => 999999999));
    print_r($result);
    if (count($result['errors']))
      echo 'Expected ERROR SUCESSS';
    else
      echo 'Expected ERROR FAILURE';
    echo PHP_EOL;
  }
}

class Test_funcSendExemptCustomerSMSIntlBoltOnSuccess extends AbstractTestStrategy
{
  function test()
  {
    $customer_id = 18670;

    $customer = get_customer_from_customer_id( $customer_id );

    \funcSendExemptCustomerSMSIntlBoltOnSuccess(
      array(
        'customer_id' => $customer_id,
        'customer'    => $customer,
        'template'    => 'immediate'
      )
    );
  }
}

class Test_funcSendSMSBOGOActivation extends AbstractTestStrategy
{
  function test()
  {
    $result = funcSendSMSBOGOActivation(array('customer_id' => 19021));
    print_r($result);
    if (count($result['errors']) == 0)
      echo 'Expected VALID SUCCESS';
    else
      echo 'Expected VALID FAILURE';
    echo PHP_EOL;

    $result = funcSendSMSBOGOActivation(array('customer_id' => 999999999));
    print_r($result);
    if (count($result['errors']))
      echo 'Expected ERROR SUCESSS';
    else
      echo 'Expected ERROR FAILURE';
    echo PHP_EOL;
  }
}

class Test_funcMultiMonthReset extends AbstractTestStrategy
{
  function test()
  {
    $languages     = array('EN','ES','ZH');
    $message_types = array('multi_month_reset','multi_month_reset_confirm');

    foreach ( $message_types as $message_type )
    {
        foreach ( $languages as $language )
        {
          $params  = array(
            'message_type' => $message_type,
            'renewal_date' => 'June 30 2015'
          );

          $message = \Ultra\Messaging\Templates\SMS_by_language( $params , $language );

          echo "Message = $message\n";
        }
    }
  }
}

# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass $test_email\n\n";

$testObject = new $testClass();

$testObject->test();


/*
php test/messaging/test_functions.php funcSendExemptCustomerSMSRenewalPlanFailChange 2609
*/

?>
