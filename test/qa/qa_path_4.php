<?php

/*
[ Neutral ] => [ Port-In Requested ]
*/

include_once('classes/Result.php');
include_once('test/api/test_api_include.php');
include_once('test/qa/test_classes.php');
include_once('web.php');


$mode = 'default';

if ( isset($argv[1]) )
{
  $mode = $argv[1];
}

$plan = 'L29';

if ( isset($argv[2]) )
{
  $allowed_plans = array("L19","L29","L39","L49","L59");

  if ( in_array( $argv[2] , $allowed_plans ) )
  {
    $plan = $argv[2];
  }
  else
  {
    die($argv[2]." is not a valid plan");
  }
}

if ( isset($argv[7]) )
{
  $allowed_languages = array("EN","ES");

  if ( ! in_array( $argv[7] , $allowed_languages ) )
  {
    die($argv[7]." is not a valid language");
  }
}

$qa_tester = new QATester($mode);

$qa_tester->data_array['targetPlan']          = $plan;
$qa_tester->data_array['numberToPort']        = $argv[3];
$qa_tester->data_array['portAccountNumber']   = $argv[4];
$qa_tester->data_array['portAccountPassword'] = $argv[5];
$qa_tester->data_array['portAccountZipcode']  = $argv[6];
$qa_tester->data_array['preferred_language']  = $argv[7];

$qa_tester->load_tests(
  array(

    /* create new web account with portal__CreateWebUser */
    new Test_portal__CreateWebUser($mode),

    /* check the state of the customer with internal__GetState */
    new Test_internal__GetState($mode),

    /* set credit card info with customercare__SetCustomerCreditCard */
    new Test_customercare__SetCustomerCreditCard($mode),

    /* transition to Prefunded with portal__PrepayPlan */
    new Test_portal__PrepayPlan($mode),

    /* attempt port */
    new Test_provisioning__requestPortFundedCustomerAsync($mode),

    /* veryfy port attempt */
    new Test_provisioning__verifyProvisionPortedCustomerAsync($mode)

  )
);

while( $qa_tester->has_next() && $qa_tester->has_no_errors() )
{
  $qa_tester->test_next();
}


/*

usage:

to use an invalid ICCID:
  php test/qa/qa_path_4.php INVALID L[2345]9 $numberToPort $portAccountNumber $portAccountPassword $portAccountZipcode [EN|ES]

to use a valid ICCID:
  php test/qa/qa_path_4.php default L[2345]9 $numberToPort $portAccountNumber $portAccountPassword $portAccountZipcode [EN|ES]

*/


?>
