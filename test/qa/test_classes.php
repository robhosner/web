<?php


include_once('db.php');
include_once('db/htt_transition_log.php');
include_once('lib/state_machine/functions.php');
include_once('test/state_machine/test_util.php');


class QATester
{
  // class for the implementation of a chain of tests

  private $index      = 0;
  public  $data_array = array(); # used for miscellaneous data
  private $errors     = array();
  private $tests      = array();
  private $mode       = 'default';

  function __construct($mode)
  {
    $this->mode = $mode;

    teldata_change_db(); // connect to the DB
  }

  public function has_next()
  {
    return ! ! ( $this->index < count($this->tests) );
  }

  public function has_no_errors()
  {
    return ! ! ( count($this->errors) == 0 );
  }

  public function test_next()
  {
    echo "\n\n[[[[ Executing test # ".( $this->index + 1 )." ]]]]\n\n";

    // execute test
    $test_result = $this->tests[$this->index]->test( $this->data_array );

    if ( $test_result->is_success() )
    {
      // validate test
      $this->tests[$this->index]->validate_test( $this->data_array );
    }

    if ( $test_result->is_failure() )
    {
      echo "\n\n[[[[ Test ".( $this->index + 1 )." failure ]]]]\n\n";

      $this->errors = $test_result->get_errors();
    }
    else
    {
      $this->data_array = array_merge( $this->data_array , $test_result->data_array );

      echo "\n\n[[[[ Test ".( $this->index + 1 )." success ]]]]\n\n";

      print_r( $this->data_array );
    }

    $this->index++;
  }

  public function load_tests($tests_array)
  {
    $this->tests = $tests_array;
  }

}

abstract class QATest
{

  // single test abstract class

  public $result;
  public $mode = 'default';

  public $curl_options = array(
    CURLOPT_USERPWD  => "dougmeli:Flora",
    CURLOPT_HTTPAUTH => CURLAUTH_BASIC
  );

  function __construct($mode)
  {
    $this->mode = $mode;

    $this->result = new Result;
  }

  function parse_test_result( $json_result )
  {
    echo "\n\n$json_result\n\n";

    $json_result_object = json_decode($json_result);

    print_r($json_result_object);

    if ( $json_result_object && is_object($json_result_object) )
    {

      if ( $json_result_object->success )
      { $this->result->succeed(); } else { $this->result->fail(); }

      if ( property_exists($json_result_object,'errors') )
      { $this->result->add_errors($json_result_object->errors); }

      if ( property_exists($json_result_object,'warnings') )
      { $this->result->add_warnings($json_result_object->warnings); }

      if ( property_exists($json_result_object,'pending') )
      { $this->result->data_array['pending'] = $json_result_object->pending; }

    }

    return $json_result_object;
  }

  // perform the test
  // must return $this->result
  abstract function test($test_params);

  // validate the test result
  abstract function validate_test($test_params);
}

class Test_internal__GetState extends QATest
{

  private $partner = 'internal';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'internal__GetState';

    $params = array(
      'customer_id' => $test_params['customer_id']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( $json_result_object && is_object($json_result_object) )
    {
      $this->result->data_array['state_valid']     = $json_result_object->state_valid;
      $this->result->data_array['plan_name']       = $json_result_object->plan_name;
      $this->result->data_array['plan_desc_name']  = $json_result_object->plan_desc_name;
      $this->result->data_array['state_name']      = $json_result_object->state_name;
      $this->result->data_array['state_desc_name'] = $json_result_object->state_desc_name;
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {
    $expected_values = array(
      'state_valid'     => '1',
      'plan_name'       => 'STANDBY',
      'plan_desc_name'  => 'Ultra SIM Standby',
      'state_name'      => 'Neutral',
      'state_desc_name' => 'Ultra SIM Standby/Neutral'
    );

    $errors = match_expected_values( $this->result->data_array , $expected_values );

    if ( count($errors) )
    {
      $this->result->add_errors( $errors );

      $this->result->fail();
    }
  }
}

class Test_internal__GetState_Suspended extends Test_internal__GetState
{
  function validate_test($test_params)
  {
    $plan = substr($test_params['targetPlan'],-2);

    $expected_values = array(
      'state_valid'     => '1',
      'plan_name'       => get_plan_name_from_short_name('L'.$plan),
      'plan_desc_name'  => 'Ultra $' . $plan,
      'state_name'      => 'Suspended',
      'state_desc_name' => 'Ultra $' . $plan . '/Suspended'
    );

    $errors = match_expected_values( $this->result->data_array , $expected_values );

    if ( count($errors) )
    {
      $this->result->add_errors( $errors );

      $this->result->fail();
    }
  }
}

class Test_internal__GetState_Active extends Test_internal__GetState
{
  function validate_test($test_params)
  {
    $plan = substr($test_params['targetPlan'],-2);

    $expected_values = array(
      'state_valid'     => '1',
      'plan_name'       => get_plan_name_from_short_name('L'.$plan),
      'plan_desc_name'  => 'Ultra $' . $plan,
      'state_name'      => 'Active',
      'state_desc_name' => 'Ultra $' . $plan . '/Active'
    );

    $errors = match_expected_values( $this->result->data_array , $expected_values );

    if ( count($errors) )
    {
      $this->result->add_errors( $errors );

      $this->result->fail();
    }
  }
}

class Test_customercare__SetCustomerCreditCard extends QATest
{
  private $partner = 'customercare';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'customercare__SetCustomerCreditCard';

    $credit_card_testing_values = credit_card_testing_values();

    $params = array(
      'customer_id'       => $test_params['customer_id'],
      'account_cc_exp'    => $credit_card_testing_values['creditExpiry'],
      'account_cc_cvv'    => $credit_card_testing_values['creditCVV'],
      'account_cc_number' => $credit_card_testing_values['creditCard'],
      'account_zipcode'   => '12345'
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( ! $json_result_object && is_object($json_result_object) )
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_customercare__AddCourtesyCash extends QATest
{
  private $partner = 'customercare';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'customercare__AddCourtesyCash';

    $plan_cost = substr($test_params['targetPlan'],-2) * 100;

    $params = array(
      'reason'                => 'Test_customercare__AddCourtesyCash',
      'agent_name'            => '12345',
      'customercare_event_id' => '12345',
      'customer_id'           => $test_params['customer_id'],
      'amount'                => $plan_cost
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( ! $json_result_object && is_object($json_result_object) )
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }
    else
    {
      # ResolvePendingTransitions twice

      echo "Trying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result_object);

      echo "\nTrying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result_object);

      echo "\n";
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_provisioning__verifyActivateShippedNewCustomerAsync extends QATest
{
  private $partner = 'celluphone';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'provisioning__verifyActivateShippedNewCustomerAsync';

    $params = array(
      'request_id' => $test_params['request_id']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

print_r( $json_result_object ); exit;

/*
    $this->result->succeed();

    $this->result->data_array['pending'] = TRUE;
*/

    while( ( $this->result->is_success() )
        && ( $this->result->data_array['pending'] )
    )
    {
      sleep(2);

      # try to resolve pending transitions
      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      if ( $json_result_object && is_object($json_result_object) )
      {

      }
      else
      {
        $this->result->fail();
        $this->result->add_error("No data from command $url");
      }

    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_provisioning__requestPortFundedCustomerAsync extends QATest
{
  function test($test_params)
  {
    $customer = get_customer_from_customer_id($test_params['customer_id']);

    $this->result->data_array['zsession'] = test_login_for_zsession( $this->curl_options , $customer->LOGIN_NAME , $customer->LOGIN_PASSWORD );

    $url = find_site_url() . '/pr/rainmaker/1/ultra/api/' . 'provisioning__requestPortFundedCustomerAsync';

    $test_iccid = get_test_iccid( ! ! ( $this->mode == 'INVALID' ) );

    $params = array(
      'zsession'            => $this->result->data_array['zsession'],
      'ICCID'               => luhnenize($test_iccid),
      'numberToPort'        => $test_params['numberToPort'],
      'portAccountNumber'   => $test_params['portAccountNumber'],
      'portAccountPassword' => $test_params['portAccountPassword'],
      'portAccountZipcode'  => $test_params['portAccountZipcode']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( $json_result_object && is_object($json_result_object) )
    {
      if ( count ( $json_result_object->errors ) )
      {
        $this->result->fail();
      }
      else
      {
        $this->result->data_array['request_id'] = $json_result_object->request_id;
      }
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {
  }
}

class Test_provisioning__requestActivateShippedNewCustomerAsync extends QATest
{
  private $partner = 'celluphone';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'provisioning__requestActivateShippedNewCustomerAsync';

    if ( is_numeric( $this->mode ) )
      $test_iccid = $this->mode;
    else
      $test_iccid = get_test_iccid( ! ! ( $this->mode == 'INVALID' ) );

    $params = array(
      'ICCID'    => luhnenize($test_iccid),
      'zsession' => $test_params['zsession']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( $json_result_object && is_object($json_result_object) )
    {
       $this->result->data_array['request_id'] = $json_result_object->request_id;
       $this->result->data_array['zsession']   = $test_params['zsession'];
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

print_r( $this->result->data_array );

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_portal__PrepayPlanMimic extends QATest
{
  private $partner = 'portal';

  function test($test_params)
  {
    $return = func_courtesy_add_balance(
      array(
        'reason'      => 1,
        'terminal_id' => 2,
        'reference'   => 3,
        'amount'      => 5900,
        'customer'    => get_customer_from_customer_id( $test_params['customer_id'] )
      )
    );

    // TODO: error handling

    $check = run_sql_and_check("update customers set POSTAL_CODE = '12345' , CC_POSTAL_CODE = '12345' where customer_id = ".$test_params['customer_id']);

    if ( !$check ) { die('DB error'); }

    $context = array(
      'transition_id' => '',
      'customer_id'   => $test_params['customer_id']
    );

    $max_path_depth   = 1;
    $state_name       = 'Pre-Funded';
    $transition_name  = 'Pre-Fund SIM '.get_plan_name_from_short_name($test_params['targetPlan']);
    $dry_run          = FALSE;
    $resolve_now      = TRUE;

    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    dlog('','change_state => '.json_encode($result_status));

    if ( $result_status['success'] == 1 )
    {
      $this->result->succeed();
      dlog('','Test_portal__PrepayPlanMimic success');

      $customer = get_customer_from_customer_id($test_params['customer_id']);

      $this->result->data_array['zsession'] = test_login_for_zsession( $this->curl_options , $customer->LOGIN_NAME , $customer->LOGIN_PASSWORD );
    }
    else
    {
      $this->result->fail();
      $this->result->add_errors( $result_status['errors'] );
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }

}

class Test_portal__PrepayPlan extends QATest
{
  private $partner = 'portal';

  function test($test_params)
  {
    $customer = get_customer_from_customer_id($test_params['customer_id']);

    dlog('',"customer = %s",$customer);

    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'portal__PrepayPlan';

    $this->result->data_array['zsession'] = test_login_for_zsession( $this->curl_options , $customer->LOGIN_NAME , $customer->LOGIN_PASSWORD );

    echo 'zsession = '.$this->result->data_array['zsession']."\n";

    $params = array(
      'chargeAmount' => ( substr($test_params['targetPlan'],-2) ) * 100, # L[2345]9 => [2345]900
      'targetPlan'   => $test_params['targetPlan'],
      'zsession'     => $this->result->data_array['zsession']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( $json_result_object && is_object($json_result_object) )
    {
      $this->result->data_array['accountBalance'] = $json_result_object->accountBalance;
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_customer_suspend extends QATest
{
  function test($test_params)
  {
    $context = array(
      'customer_id' => $test_params['customer_id']
    );

    $customer = get_customer_from_customer_id( $test_params['customer_id'] );

    $result = suspend_account($customer,$context);

    if ( count($result['errors']) )
    {
      $this->result->fail();
      $this->result->add_errors( $result['errors'] );
    }
    else
    {
      # ResolvePendingTransitions twice

      echo "Trying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result_object);

      echo "\nTrying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result_object);

      echo "\n";

      $this->result->succeed();
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_customer_activate extends QATest
{
  function test($test_params)
  {
    $context = array(
      'customer_id' => $test_params['customer_id']
    );

    $customer = get_customer_from_customer_id( $test_params['customer_id'] );

    $state = internal_func_get_state(
      array(
        'states'  => NULL,
        'context' => $context
      )
    );

    $result = reactivate_suspended_account($state,$customer,$context);

    if ( $result['errors'] && count( $result['errors'] ) )
    {
      $this->result->fail();
      $this->result->add_errors( $result['errors'] );
    }
    else
    {
      # ResolvePendingTransitions twice

      echo "Trying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result_object);

      echo "\nTrying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result_object);

      echo "\n";

      $this->result->succeed();
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_provisioning__verifyProvisionPortedCustomerAsync extends QATest
{
  function test($test_params)
  {
    $url = find_site_url() . '/pr/rainmaker/1/ultra/api/' . 'provisioning__verifyProvisionPortedCustomerAsync';

    $params = array(
      'request_id' => $test_params['request_id']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    while ( $json_result_object
            &&
            is_object($json_result_object)
            &&
            $json_result_object->pending
    ) {
      # ResolvePendingTransitions
      echo "Result is pending, trying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result);

      echo "\n";

      $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

      $json_result_object = $this->parse_test_result( $json_result );
    }

    if ( $json_result_object && is_object($json_result_object) )
    {
print_r( $json_result_object );
      if ( $json_result_object->success )
      {
        $this->result->succeed();
      }
      else
      { $this->result->fail(); }
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_provisioning__verifyProvisionNewCustomerAsync extends QATest
{
  private $partner = 'celluphone';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'provisioning__verifyProvisionNewCustomerAsync';

    $params = array(
      'request_id' => $test_params['request_id']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

/*
    if ( ! $json_result_object->success )
    {
      $this->result->fail();
      return $this->result;
    }
*/

    while ( $json_result_object
            &&
            is_object($json_result_object)
            &&
            $json_result_object->pending
    ) {
      # ResolvePendingTransitions
      echo "Result is pending, trying to resolve pending transitions ...\n";

      $json_result_object = $this->parse_test_result( test_resolve_pending_transitions( $this->curl_options ) );

      print_r($json_result);

      echo "\n";

      $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

      $json_result_object = $this->parse_test_result( $json_result );
    }

    if ( $json_result_object && is_object($json_result_object) )
    {
      if ( $json_result_object->success )
      {
        $this->result->succeed();

        // retrieve customer_id from $test_params['request_id']
        $this->result->data_array['customer_id'] = get_customer_id_from_transition_uuid( $test_params['request_id'] );
      }
      else
      { $this->result->fail(); }
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_provisioning__requestProvisionNewCustomerAsync extends QATest
{
  private $partner = 'celluphone';

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'provisioning__requestProvisionNewCustomerAsync';

    if ( isset( $test_params['test_iccid'] ) && $test_params['test_iccid'] )
    {
      $test_iccid = $test_params['test_iccid'];
    }
    else
    {
      $test_iccid = ( $this->mode == 'INVALID' )
                    ?
                    get_test_iccid('USED') // a used SIM ( requests will succeed, but state machine actions will eventually - pusposely - fail )
                    :
                    get_test_iccid(FALSE) // a new valid SIM
                    ;
    }

    $loadPayment = 'NONE';
    $customerZip = '12345';

    # ( 'NONE'|'CCARD'|'PINS' )
    if ( isset( $test_params['loadPayment'] ) )
      $loadPayment = $test_params['loadPayment'];

    if ( isset( $test_params['customerZip'] ) && $test_params['customerZip'] )
      $customerZip = $test_params['customerZip'];

    $params = array(
      'ICCID'       => luhnenize( $test_iccid ),
      'loadPayment' => $loadPayment,
      'targetPlan'  => $test_params['targetPlan'],
      'pinsToApply' => '',
      'customerZip' => $customerZip,
      'activation_masteragent' => '9999',
      'activation_distributor' => '9998',
      'activation_agent'       => '9997',
      'activation_store'       => '9996',
      'activation_userid'      => '9995',
      'preferred_language'     => $test_params['preferred_language']
    );

    $credit_card_testing_values = credit_card_testing_values();

    $cc_params = array(
      'creditCard'         => $credit_card_testing_values['creditCard'],
      'creditExpiry'       => $credit_card_testing_values['creditExpiry'],
      'creditCVV'          => $credit_card_testing_values['creditCVV'],
      'creditAutoRecharge' => FALSE,
      'creditAmount'       => '6900',
      'customerAddressOne' => 'add 1 '.time(),
      'customerCity'       => 'New York',
      'customerState'      => 'NY',
    );

    $params = array_merge( $params , $cc_params );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( $json_result_object && is_object($json_result_object) )
    {
       $this->result->data_array['request_id'] = $json_result_object->request_id;
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {

  }
}

class Test_portal__CreateWebUser extends QATest
{
  private $partner = 'portal';

  private function rnd_login()
  {
    $login = '';

    $character_range = range(65,90);

    foreach(range(0,19) as $i)
    {
      $random_pick = mt_rand(1,count($character_range));
      $login .= chr($character_range[$random_pick-1]);
    }

    return $login;
  }

  function test($test_params)
  {
    $url = find_site_url() . '/pr/' . $this->partner.'/1/ultra/api/' . 'portal__CreateWebUser';

    $login = $this->rnd_login();

    $params = array(
      'mock'                 => 'allow_duplicate_email',
      'account_login'        => $login,
      'account_first_name'   => $login,
      'account_last_name'    => $login,
      'account_password'     => time(),
      'account_number_email' => 'rgalli@ultra.me',
      'account_country'      => 'US',
      'preferred_language'   => $test_params['preferred_language']
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    $json_result_object = $this->parse_test_result( $json_result );

    if ( $json_result_object && is_object($json_result_object) )
    {
      $this->result->data_array['customer_id'] = $json_result_object->customer_id;
    }
    else
    {
      $this->result->fail();
      $this->result->add_error("No data from command $url");
    }

    return $this->result;
  }

  function validate_test($test_params)
  {
    // TODO: any validation for the customer data in the DB?
  }
}

function test_login_for_zsession( $curl_options , $login , $password )
{
  $url = find_site_url() . '/pr/portal/1/ultra/api/portal__Login';

  $params = array(
    'account_login'    => $login,
    'account_password' => $password
  );

  $json_result = curl_post($url,$params,$curl_options,NULL,240);

  $json_decoded = json_decode($json_result);

  dlog('',"zsession = ".$json_decoded->zsession);

  return $json_decoded->zsession;
}

?>
