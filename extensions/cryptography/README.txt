This is a a PHP extension 'cryptography' for access to low level cryptographic functions provided by openssh library

WARNING: the following instructions are for Red Hat/CentOS, different Linux distros have different paths and package names

compilation (if necessary)
  install or update openssl and openssl-devel packages: the latter will install required source headers
  install or update php-devel package: this will install required tool phpize
  create a directory (in any location) to build the extension and place the required sources into it
    config.m4, cryptography.h, cryptography.c, cryptography.ini, cryptography.php
  cd into the directory and initialize phpize (this will create a large number of files and directories)
    $ phpize
    $ ./configure
  compile and build the extension (this will create the extension file modules/cryptography.so)
    $ make
  follow installation instructions

installation (if already complied)
  install or update openssl package via your preferred package manager; required version is 1.0.1e or higher
  descend into the extension build directory created during complication steps (above)
  copy cryptography extension file into PHP
    cp modules/cryptography.so /usr/lib64/php/modules
      note: on different distributions this default PHP extension directory can be identified with command
        php-config | grep extension-dir
  enable extension by copying its ini file
    cp cryptography.ini /etc/php.d/
  check that extension loaded successfully by testing its functionality
    php cryptography.php
  restart apache

