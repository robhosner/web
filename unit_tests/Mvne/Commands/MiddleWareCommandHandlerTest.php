<?php

use Ultra\Exceptions\MiddleWareException;
use Ultra\Mvne\Commands\MiddleWareCommandHandler;

class MiddleWareCommandHandlerTest extends PHPUnit_Framework_TestCase
{
  public function testMiddleWareResultFails()
  {
    $result = new Result();
    $result->fail();

    $this->setExpectedException(MiddleWareException::class);

    $commandHandler = new MiddleWareCommandHandler();
    $commandHandler->handleMiddleWareResultCheck($result);
  }

  public function testMiddleWareReturnsInvalidResultCode()
  {
    $result = new Result();
    $result->succeed();
    $result->add_data_array(['ResultCode' => 500, 'errors' => ['What an error']]);

    $this->setExpectedException(MiddleWareException::class);

    $commandHandler = new MiddleWareCommandHandler();
    $commandHandler->handleMiddleWareResultCheck($result);
  }

  public function testMiddleWareReturnsEmptyBody()
  {
    $result = new Result();
    $result->succeed();
    $result->add_data_array(['ResultCode' => 100, 'body' => '', 'success' => false]);

    $this->setExpectedException(MiddleWareException::class);

    $commandHandler = new MiddleWareCommandHandler();
    $commandHandler->handleMiddleWareResultCheck($result);
  }

  public function testMiddleWareResultReturnsTrue()
  {
    $result = new Result();
    $result->succeed();
    $result->add_data_array(['ResultCode' => 100, 'body' => 'body', 'success' => true]);

    $commandHandler = new MiddleWareCommandHandler();
    $commandHandler->handleMiddleWareResultCheck($result);
  }
}
