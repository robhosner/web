<?php
putenv("UNIT_TESTING=1");

require_once 'classes/Result.php';
require_once 'UltraLogger.php';
require_once 'OverrideHandler.php';

// Autoload files contained within a specific namespace
spl_autoload_register(function($class)
{
  // project-specific namespace prefix
  $prefix = 'Ultra\\';
  $baseDir = 'Ultra/';

  // does the class use the namespace prefix?
  $len = strlen($prefix);
  if (strncmp($prefix, $class, $len) !== 0)
  {
    // no, move to the next registered autoloader
    return;
  }

  // get the relative class name
  $relativeClass = substr($class, $len);
  $file = $baseDir . str_replace('\\', '/', $relativeClass) . '.php';

  if (file_exists($file))
  {
    require_once $file;
  }
});