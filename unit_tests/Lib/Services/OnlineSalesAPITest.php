<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\OnlineSalesAPI;
use Ultra\Utilities\Common;

class OnlineSalesAPITest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var OnlineSalesAPI
   */
  private $class;
  private $config;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getOnlineSalesAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $utils = $this->getMock(Common::class);
    $utils->expects($this->any())
      ->method('luhnenize')
      ->will($this->returnValue(1));

    $this->class = $this->getMockBuilder(OnlineSalesAPI::class)
      ->setConstructorArgs([
        $this->config,
        $utils
      ])
      ->setMethods(['get', 'post', 'put', 'delete'])
      ->getMock();
  }

  public function testMissingConfiguration()
  {
    $this->setExpectedException(\Exception::class);
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getOnlineSalesAPIConfig')
      ->will($this->returnValue([]));
    new OnlineSalesAPI($this->config);
  }

  public function testGetOrderByOrderIDFails()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue(['code' => 400]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertFalse($result->is_success());
  }

  public function testGetOrderByOrderIDMissingOrderItems()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{}'
      ]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing order field items', $result->get_errors());
  }

  public function testGetOrderByOrderIDMissingWallet()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"items": [123]}'
      ]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing order field wallet cost', $result->get_errors());
  }

  public function testGetOrderByOrderIDMissingCredit()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"items": [123], "cost": {"wallet":{"totalAmount":123}}}'
      ]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing order field credit card cost', $result->get_errors());
  }

  public function testGetOrderByOrderIDInvalidOrder()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"items": [123], "cost": {"wallet":{"totalAmount":123}, "creditCard": {"totalAmount": 123}}, "status": "pending"}'
      ]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertFalse($result->is_success());
    $this->assertContains('Invalid order status of pending', $result->get_errors());
  }

  public function testGetOrderByOrderIDParserError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"items": [123], "cost": {"wallet":{"tota}'
      ]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse RetailerOrders response', $result->get_errors());
  }

  public function testGetOrderByOrderIDSuccess()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"items": [123], "cost": {"wallet":{"totalAmount":123}, "creditCard": {"totalAmount": 123}}, "status": "created"}'
      ]));

    $result = $this->class->getOrderByOrderID(1, 3);
    $this->assertTrue($result->is_success());
  }

  public function testGetRetailerOrdersFails()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 500,
      ]));

    $result = $this->class->getRetailerOrders(1, 3);
    $this->assertFalse($result->is_success());
  }

  public function testGetRetailerOrdersParseError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => ''
      ]));

    $result = $this->class->getRetailerOrders(1, 3);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse RetailerOrders response', $result->get_errors());
  }

  public function testGetRetailerOrdersSucceeds()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{}'
      ]));

    $result = $this->class->getRetailerOrders(1, 3);
    $this->assertTrue($result->is_success());
  }

  public function testMarkOrderPaidFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
      ]));

    $result = $this->class->markOrderPaid(1, 1, 1);
    $this->assertFalse($result->is_success());
  }

  public function testMarkOrderPaidParseError()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => ''
      ]));

    $result = $this->class->markOrderPaid(1, 1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('Failed to parse RetailerOrders response', $result->get_errors());
  }

  public function testMarkOrderPaidSucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{}'
      ]));

    $result = $this->class->markOrderPaid(1, 1, 1);
    $this->assertTrue($result->is_success());
  }

  public function testFulfillOrderItemFails()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
      ]));

    $result = $this->class->fulfillOrderItem(1, 1);
    $this->assertFalse($result->is_success());
  }

  public function testFulfillOrderItemSucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
      ]));

    $result = $this->class->fulfillOrderItem(1, 1);
    $this->assertTrue($result->is_success());
  }

  public function testFulfillOrderItemCurlError()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'curl_errno' => 123
      ]));

    $result = $this->class->fulfillOrderItem(1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('OnlineOrders API curl failed - 123', $result->get_errors());
  }

  public function testFulfillOrderItemResponseContainsError()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": ["errrrrr"]}',
      ]));

    $result = $this->class->fulfillOrderItem(1, 1);
    $this->assertFalse($result->is_success());
    $this->assertContains('errrrrr', $result->get_errors());
  }

  public function testGetOrderSucceeds()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{
          "cc_exp_month": 12,
          "cc_exp_year": 20,
          "bill_postcode": 12345,
          "cc_bin": 1234,
          "cc_last4": 4321,
          "mes_token": "token",
          "bill_firstname": "meh",
          "bill_lastname": "hem"
        }'
      ]));

    $result = $this->class->getOrder(123456789012345678);

    $order = new \stdClass();
    $order->postal_code = 12345;
    $order->bin = 1234;
    $order->last_four = 4321;
    $order->expires_date = '1220';
    $order->cvv_validation = 'M';
    $order->avs_validation = 'Y';
    $order->gateway = 'MeS';
    $order->merchant_account = 1;
    $order->token = 'token';
    $order->customer_ip = null;
    $order->first_name = 'meh';
    $order->last_name = 'hem';
    $order->address1 = null;
    $order->address2 = null;
    $order->city = null;
    $order->state = null;
    $order->auto_enroll = false;

    $this->assertTrue($result->is_success());
    $this->assertEquals($order, $result->data_array['order']);
  }

  public function testGetOrderInvalidIccid()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 400,
        'body' => ''
      ]));

    $result = $this->class->getOrder(1234567890123456789);
    $this->assertFalse($result->is_success());
  }

  public function testGetOrderOrderNotFound()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 404,
        'body' => ''
      ]));

    $result = $this->class->getOrder(1234567890123456789);
    $this->assertFalse($result->is_success());
  }

  public function testGetOrderCurlError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 409,
        'body' => '',
        'curl_errno' => 12,
      ]));

    $result = $this->class->getOrder(1234567890123456789);
    $this->assertFalse($result->is_success());
    $this->assertContains('OnlineSalesAPI curl call failed with error number 12', $result->get_errors());
  }

  public function testGetOrderServerError()
  {
    $this->class->expects($this->any())
      ->method('get')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '',
      ]));

    $result = $this->class->getOrder(1234567890123456789);
    $this->assertFalse($result->is_success());
    $this->assertContains('OnlineSalesAPI call failed with response code 500', $result->get_errors());
  }
}
