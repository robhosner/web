<?php

use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Orders\Repositories\Mssql\OrderRepository;
use Ultra\Lib\Services\OnlineSalesAPI;
use Ultra\Payments\Payment;
use Ultra\Configuration\Configuration;

class PaymentsTests extends PHPUnit_Framework_TestCase
{
  /**
   * @var Payment
   */
  private $payment;

  public function setUp()
  {
    $this->payment = $this->getMock(Payment::class);
    $this->configuration = $this->getMock(Configuration::class);

    $onlineSalesAPIConfig = [
      'host'      => 'web-qa-08:3100',
      'basepath'  => '/v1'
    ];
    $this->configuration->expects($this->any())
      ->method('getOnlineSalesAPIConfig')
      ->will($this->returnValue($onlineSalesAPIConfig));
    $this->orderAPI = $this->getMock(OnlineSalesAPI::class, [], [$this->configuration]);
  }

  /**
   * @test
   */
  public function online_auto_enroll()
  {
    $cc_holders_identity = 7;
    $customer_id         = 31;
    $iccid_full          = '1234567891234567891';

    $order = new stdClass();
    $order->ICCID_FULL       = '1234567890';
    $order->postal_code      = '34743';
    $order->bin              = '123456';
    $order->last_four        = '9876';
    $order->expires_date     = '12/16';
    $order->cvv_validation   = '100';
    $order->avs_validation   = '200';
    $order->gateway          = 'TEST';
    $order->merchant_account = '1';
    $order->token            = 'test_token';
    $order->customer_ip      = '127.0.0.1';
    $order->first_name       = 'testFirstName';
    $order->last_name        = 'testLastName';
    $order->address1         = 'test address 1';
    $order->address2         = 'test address 2';
    $order->city             = 'testCity';
    $order->state            = 'CA';
    $order->auto_enroll      = 0;

    $getOrderByIccidInputs = [
      'postal_code',
      'bin',
      'last_four',
      'expires_date',
      'cvv_validation',
      'avs_validation',
      'gateway',
      'merchant_account',
      'token',
      'customer_ip',
      'first_name',
      'last_name',
      'address1',
      'address2',
      'city',
      'state',
      'auto_enroll'
    ];

    $getUltraCCHolderByDetailsInputs = [
      'customer_id'  => $customer_id,
      'bin'          => $order->bin,
      'last_four'    => $order->last_four,
      'expires_date' => $order->expires_date
    ];

    $addToUltraCCHoldersInputs = [
      'customer_id'     => $customer_id,
      'bin'             => $order->bin,
      'last_four'       => $order->last_four,
      'expires_date'    => $order->expires_date,
      'cvv_validation'  => $order->cvv_validation,
      'avs_validation'  => $order->avs_validation
    ];

    $addToUltraCCHolderTokensInputs = [
      'cc_holders_id'    => $cc_holders_identity,
      'gateway'          => $order->gateway,
      'merchant_account' => $order->merchant_account,
      'token'            => $order->token
    ];

    $updateCustomerByCustomerIdInputs = [
      'cc_name'         => $order->first_name . ' ' . $order->last_name,
      'cc_address1'     => $order->address1,
      'cc_address2'     => $order->address2,
      'cc_city'         => $order->city,
      'cc_state_region' => $order->state,
      'cc_postal_code'  => $order->postal_code,
      'cc_exp_date'     => $order->expires_date
    ];

    $this->order = $order;

    /*
     * tests
     */

    // test no order success
    {
      $customerRepo   = $this->getMock(CustomerRepository::class);
      $creditCardRepo = $this->getMock(CreditCardRepository::class);

      $this->orderAPI->expects($this->any())
        ->method('getOrder')
        ->with($iccid_full, $getOrderByIccidInputs)
        ->will($this->returnValue(false));

      $repos = [
        'order_api'        => $this->orderAPI,
        'customer_repo'    => $customerRepo,
        'credit_card_repo' => $creditCardRepo
      ];

      $result = new Result();
      $result->succeed();

      $this->payment->expects($this->at(0))
        ->method('onlineAutoEnroll')
        ->with($customer_id, $iccid_full, $repos)
        ->will($this->returnValue($result));

      $result = $this->payment->onlineAutoEnroll($customer_id, $iccid_full, $repos);
      $this->assertTrue($result->is_success());
    }

    // already registered credit card
    {
      $customerRepo   = $this->getMock(CustomerRepository::class);
      $creditCardRepo = $this->getMock(CreditCardRepository::class);

      $this->orderAPI->expects($this->any())
        ->method('getorder')
        ->with($iccid_full, $getOrderByIccidInputs)
        ->will($this->returnValue($order));

      $creditCardRepo->expects($this->any())
        ->method('getUltraCCHolderByDetails')
        ->with($getUltraCCHolderByDetailsInputs)
        ->will($this->returnValue(['customer_id' => $customer_id]));

      
      $customerRepo->expects($this->any())
        ->method('updateCustomerByCustomerId')
        ->with($customer_id, $updateCustomerByCustomerIdInputs)
        ->will($this->returnValue(true));

      $repos = [
        'order_api'        => $this->orderAPI,
        'customer_repo'    => $customerRepo,
        'credit_card_repo' => $creditCardRepo
      ];

      $result = new Result();
      $result->succeed();

      $this->payment->expects($this->at(0))
        ->method('onlineAutoEnroll')
        ->with($customer_id, $iccid_full, $repos)
        ->will($this->returnValue($result));

      $result = $this->payment->onlineAutoEnroll($customer_id, $iccid_full, $repos);
      $this->assertTrue($result->is_success());
    }

    // test error on add to ultra cc holders
    {
      $customerRepo   = $this->getMock(CustomerRepository::class);
      $creditCardRepo = $this->getMock(CreditCardRepository::class);

      $this->orderAPI->expects($this->any())
        ->method('getOrder')
        ->with($iccid_full, $getOrderByIccidInputs)
        ->will($this->returnValue($order));

      $creditCardRepo->expects($this->any())
        ->method('getUltraCCHolderByDetails')
        ->with($getUltraCCHolderByDetailsInputs)
        ->will($this->returnValue(null));

      $creditCardRepo->expects($this->any())
        ->method('addToUltraCCHolders')
        ->with($addToUltraCCHoldersInputs)
        ->will($this->returnValue(false));

      $repos = [
        'order_api'        => $this->orderAPI,
        'customer_repo'    => $customerRepo,
        'credit_card_repo' => $creditCardRepo
      ];

      $result = new Result();

      $this->payment->expects($this->at(0))
        ->method('onlineAutoEnroll')
        ->with($customer_id, $iccid_full, $repos)
        ->will($this->returnValue($result));

      $result = $this->payment->onlineAutoEnroll($customer_id, $iccid_full, $repos);
      $this->assertTrue( ! $result->is_success());
    }

    // test error on add to ultra cc holder tokens
    {
      $customerRepo   = $this->getMock(CustomerRepository::class);
      $creditCardRepo = $this->getMock(CreditCardRepository::class);

      $this->orderAPI->expects($this->any())
        ->method('getOrder')
        ->with($iccid_full, $getOrderByIccidInputs)
        ->will($this->returnValue($order));

      $creditCardRepo->expects($this->any())
        ->method('getUltraCCHolderByDetails')
        ->with($getUltraCCHolderByDetailsInputs)
        ->will($this->returnValue(null));

      $creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolders')
      ->with($addToUltraCCHoldersInputs)
      ->will($this->returnValue($cc_holders_identity));

      $creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolderTokens')
      ->with($addToUltraCCHolderTokensInputs)
      ->will($this->returnValue(false));

      $repos = [
        'order_api'        => $this->orderAPI,
        'customer_repo'    => $customerRepo,
        'credit_card_repo' => $creditCardRepo
      ];

      $result = new Result();

      $this->payment->expects($this->at(0))
        ->method('onlineAutoEnroll')
        ->with($customer_id, $iccid_full, $repos)
        ->will($this->returnValue($result));

      $result = $this->payment->onlineAutoEnroll($customer_id, $iccid_full, $repos);
      $this->assertTrue( ! $result->is_success());
    }

    // test error on update customer
    {
      $customerRepo   = $this->getMock(CustomerRepository::class);
      $creditCardRepo = $this->getMock(CreditCardRepository::class);

      $this->orderAPI->expects($this->any())
        ->method('getOrder')
        ->with($iccid_full, $getOrderByIccidInputs)
        ->will($this->returnValue($order));

      $creditCardRepo->expects($this->any())
        ->method('getUltraCCHolderByDetails')
        ->with($getUltraCCHolderByDetailsInputs)
        ->will($this->returnValue(null));

      $creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolders')
      ->with($addToUltraCCHoldersInputs)
      ->will($this->returnValue($cc_holders_identity));

      $creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolderTokens')
      ->with($addToUltraCCHolderTokensInputs)
      ->will($this->returnValue(true));

      $customerRepo->expects($this->any())
        ->method('updateCustomerByCustomerId')
        ->with($customer_id, $updateCustomerByCustomerIdInputs)
        ->will($this->returnValue(false));

      $repos = [
        'order_repo'       => $this->orderAPI,
        'customer_repo'    => $customerRepo,
        'credit_card_repo' => $creditCardRepo
      ];

      $result = new Result();

      $this->payment->expects($this->at(0))
        ->method('onlineAutoEnroll')
        ->with($customer_id, $iccid_full, $repos)
        ->will($this->returnValue($result));

      $result = $this->payment->onlineAutoEnroll($customer_id, $iccid_full, $repos);
      $this->assertTrue( ! $result->is_success());
    }

    // test success
    {
      $customerRepo   = $this->getMock(CustomerRepository::class);
      $creditCardRepo = $this->getMock(CreditCardRepository::class);

      $this->orderAPI->expects($this->any())
        ->method('getOrder')
        ->with($iccid_full, $getOrderByIccidInputs)
        ->will($this->returnValue($order));

      $creditCardRepo->expects($this->any())
        ->method('getUltraCCHolderByDetails')
        ->with($getUltraCCHolderByDetailsInputs)
        ->will($this->returnValue(null));

      // test again for addToUltraCCHolders
      $creditCardRepo->expects($this->any())
        ->method('addToUltraCCHolders')
        ->with($addToUltraCCHoldersInputs)
        ->will($this->returnValue($cc_holders_identity));

      $creditCardRepo->expects($this->any())
        ->method('addToUltraCCHolderTokens')
        ->with($addToUltraCCHolderTokensInputs)
        ->will($this->returnValue(true));

      $customerRepo->expects($this->any())
        ->method('updateCustomerByCustomerId')
        ->with($customer_id, $updateCustomerByCustomerIdInputs)
        ->will($this->returnValue(true));

      $repos = [
        'order_api'        => $this->orderAPI,
        'customer_repo'    => $customerRepo,
        'credit_card_repo' => $creditCardRepo
      ];

      $result = new Result();
      $result->succeed();

      $this->payment->expects($this->at(0))
        ->method('onlineAutoEnroll')
        ->with($customer_id, $iccid_full, $repos)
        ->will($this->returnValue($result));

      // success
      $result = $this->payment->onlineAutoEnroll($customer_id, $iccid_full, $repos);
      $this->assertTrue($result->is_success());
    }
  }

  /**
   * @test
   */
  public function func_get_orange_plan_from_sim_data()
  {
    // D
    $simData = new stdClass();
    $simData->BRAND_ID = 1;
    $simData->DURATION = 1;
    $simData->STORED_VALUE = 39;

    $this->payment->expects($this->at(0))
      ->method('funcGetOrangePlanFromSimData')
      ->with($simData)
      ->will($this->returnValue('D39'));

    $plan = $this->payment->funcGetOrangePlanFromSimData($simData);
    $this->assertEquals('D39', $plan);

    // L
    $simData = new stdClass();
    $simData->BRAND_ID = 1;
    $simData->DURATION = 1;
    $simData->STORED_VALUE = 19;

    $this->payment = $this->getMock(Payment::class);
    $this->payment->expects($this->any())
      ->method('funcGetOrangePlanFromSimData')
      ->with($simData)
      ->will($this->returnValue('L19'));

    $plan = $this->payment->funcGetOrangePlanFromSimData($simData);
    $this->assertEquals('L19', $plan);

    // UV
    $simData = new stdClass();
    $simData->BRAND_ID = 2;
    $simData->DURATION = 1;
    $simData->STORED_VALUE = 20;

    $this->payment = $this->getMock(Payment::class);
    $this->payment->expects($this->any())
      ->method('funcGetOrangePlanFromSimData')
      ->with($simData)
      ->will($this->returnValue('UV20'));

    $plan = $this->payment->funcGetOrangePlanFromSimData($simData);
    $this->assertEquals('UV20', $plan);

    //
    $simData = new stdClass();
    $simData->BRAND_ID = 2;
    $simData->DURATION = 1;
    $simData->STORED_VALUE = 20;

    // overrides everything else , uses get_orange_plan_map
    $simData->OFFER_ID = 11;

    $this->payment = $this->getMock(Payment::class);
    $this->payment->expects($this->any())
      ->method('funcGetOrangePlanFromSimData')
      ->with($simData)
      ->will($this->returnValue('S29'));

    $plan = $this->payment->funcGetOrangePlanFromSimData($simData);
    $this->assertEquals('S29', $plan);

    // duration > 0 , assume multi-month
    $simData = new stdClass();
    $simData->BRAND_ID = 2;
    $simData->DURATION = 2;
    $simData->STORED_VALUE = 20;

    $this->payment = $this->getMock(Payment::class);
    $this->payment->expects($this->any())
      ->method('funcGetOrangePlanFromSimData')
      ->with($simData)
      ->will($this->returnValue('M20'));

    $plan = $this->payment->funcGetOrangePlanFromSimData($simData);
    $this->assertEquals('M20', $plan);
  }
}