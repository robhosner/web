<?php

use Ultra\Customers\Customer;
use Ultra\Lib\Api\Partner\Portal\ClaimCreditCardByActCode;

use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\ApiErrors;
use Ultra\Orders\Repositories\Mssql\OrderRepository;
use Ultra\Sims\Repositories\Mssql\SimRepository;

class ClaimCreditCardByActCodeTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var ClaimCreditCardByActCode
   */
  private $api;

  /**
   * @var SimRepository
   */
  private $simRepo;

  /**
   * @var OrderRepository
   */
  private $orderRepo;

  /**
   * @var CreditCardRepository
   */
  private $creditCardRepo;

  /**
   * @var CustomerRepository
   */
  private $customerRepo;

  private $session;

  public function setUp()
  {
    $this->simRepo        = $this->getMock(SimRepository::class);
    $this->orderRepo      = $this->getMock(OrderRepository::class);
    $this->creditCardRepo = $this->getMock(CreditCardRepository::class);
    $this->customerRepo   = $this->getMock(CustomerRepository::class);
    $this->session        = $this->getMock(Session::class);

    $this->getUltraCCHolderByDetailsInputs = [
      'customer_id'  => 31,
      'bin'          => '123456',
      'last_four'    => '9876',
      'expires_date' => '12/16'
    ];
    
    $this->api = $this->getMockBuilder(ClaimCreditCardByActCode::class)
      ->setConstructorArgs([
        $this->simRepo,
        $this->orderRepo,
        $this->creditCardRepo,
        $this->customerRepo,
        $this->session
      ])
      ->setMethods(['getCustomerIdFromSession'])
      ->getMock();

    $this->api->result = new Result();
    $this->api->defaultValues = [];

    $this->api->expects($this->any())
      ->method('getCustomerIdFromSession')
      ->will($this->returnValue(31));

    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testGetCustomerIdFromSessionReturnsCustomerId()
  {
    $this->simRepo        = $this->getMock(SimRepository::class);
    $this->orderRepo      = $this->getMock(OrderRepository::class);
    $this->creditCardRepo = $this->getMock(CreditCardRepository::class);
    $this->customerRepo   = $this->getMock(CustomerRepository::class);
    $this->session        = $this->getMock(Session::class);

    $this->session->customer_id = 0;
    $this->api = new ClaimCreditCardByActCode(
      $this->simRepo,
      $this->orderRepo,
      $this->creditCardRepo,
      $this->customerRepo,
      $this->session
    );

    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues(['customer_id' => 31, 'actcode' => 1234, 'postal_code' => 345678, 'auto_enroll' => 1]);

    $_COOKIE['zsession'] = 1234;
    $customer = new Customer(['customer_id' => 31]);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with($_COOKIE['zsession'])
      ->will($this->returnValue($customer));

    $customerId = $this->api->getCustomerIdFromSession();
    $this->assertEquals(31, $customerId);
  }
  
  public function testMissingParameters()
  {
    // missing actcode
    $this->api->setInputValues(['customer_id' => '31', 'actcode' => '', 'postal_code' => '', 'auto_enroll' => '']);
    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('MP0001', $result->data_array['error_codes']);

    // missing postal_code
    $this->api->setInputValues(['customer_id' => '31', 'actcode' => 'adsfasdfasd', 'postal_code' => '', 'auto_enroll' => '']);
    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('MP0001', $result->data_array['error_codes']);

    // missing auto_enroll
    $this->api->setInputValues(['customer_id' => '', 'actcode' => 'adsfasdfasd', 'postal_code' => '34743', 'auto_enroll' => '']);
    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testInvalidActCode()
  {
    $customer_id = 31;
    $actcode     = 'adsfasdfasd';
    $postal_code = '34743';
    $auto_enroll = 1;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue(false));

    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);
    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testNoOrdersFoundForIccid()
  {
    $customer_id = 31;
    $actcode     = '1234';
    $postal_code = '34743';
    $auto_enroll = 1;

    $sim = new stdClass();
    $sim->ICCID_FULL = 1234567890;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      )
      ->will($this->returnValue(false));

    // $this->api->setCreditCardRepository($this->creditCardRepo);

    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);

    $result = $this->api->portal__ClaimCreditCardByActCode();

    // print_r($result);

    $this->assertContains('IC0001', $result->data_array['error_codes']);
  }

  public function testAlreadyClaimed()
  {
    $customer_id = 31;
    $actcode     = '1234';
    $postal_code = '34743';
    $auto_enroll = 1;

    // last modified id by DB
    $cc_holders_identity = 7;

    $sim = new stdClass();
    $sim->ICCID_FULL  = 1234567890;
    $sim->CUSTOMER_ID = 31;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $order = new stdClass();
    $order->ICCID_FULL       = '1234567890';
    $order->postal_code      = '34743';
    $order->bin              = '123456';
    $order->last_four        = '9876';
    $order->expires_date     = '12/16';
    $order->cvv_validation   = '100';
    $order->avs_validation   = '200';
    $order->gateway          = 'TEST';
    $order->merchant_account = '1';
    $order->token            = 'test_token';
    $order->customer_ip      = '127.0.0.1';
    $order->first_name       = 'testFirstName';
    $order->last_name        = 'testLastName';
    $order->address1         = 'test address 1';
    $order->address2         = 'test address 2';
    $order->city             = 'testCity';
    $order->state            = 'CA';

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      )
      ->will($this->returnValue($order));

    $this->creditCardRepo->expects($this->any())
      ->method('getUltraCCHolderByDetails')
      ->with($this->getUltraCCHolderByDetailsInputs)
      ->will($this->returnValue(['customer_id' => $customer_id]));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with($customer_id, [
          'cc_name'         => $order->first_name . ' ' . $order->last_name,
          'cc_address1'     => $order->address1,
          'cc_address2'     => $order->address2,
          'cc_city'         => $order->city,
          'cc_state_region' => $order->state,
          'cc_postal_code'  => $order->postal_code,
          'cc_exp_date'     => $order->expires_date
        ])
      ->will($this->returnValue(1));
    
    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);

    $result = $this->api->portal__ClaimCreditCardByActCode();

    $warnings = $result->get_warnings();
    $this->assertEquals(1, count($warnings));
  }

  public function testFailureInsertingCCHolders()
  {
    $customer_id = 31;
    $actcode     = '1234';
    $postal_code = '34743';
    $auto_enroll = 1;

    // last modified id by DB
    $cc_holders_identity = 7;

    $sim = new stdClass();
    $sim->ICCID_FULL  = 1234567890;
    $sim->CUSTOMER_ID = 31;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $order = new stdClass();
    $order->ICCID_FULL       = '1234567890';
    $order->postal_code      = '34743';
    $order->bin              = '123456';
    $order->last_four        = '9876';
    $order->expires_date     = '12/16';
    $order->cvv_validation   = '100';
    $order->avs_validation   = '200';
    $order->gateway          = 'TEST';
    $order->merchant_account = '1';
    $order->token            = 'test_token';
    $order->customer_ip      = '127.0.0.1';
    $order->first_name       = 'testFirstName';
    $order->last_name        = 'testLastName';
    $order->address1         = 'test address 1';
    $order->address2         = 'test address 2';
    $order->city             = 'testCity';
    $order->state            = 'CA';

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      )
      ->will($this->returnValue($order));

    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolders')
      ->with(
        [
          'customer_id'     => $customer_id,
          'bin'             => $order->bin,
          'last_four'       => $order->last_four,
          'expires_date'    => $order->expires_date,
          'cvv_validation'  => $order->cvv_validation,
          'avs_validation'  => $order->avs_validation
        ]
      )
      ->will($this->returnValue(FALSE));
    
    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);

    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testFailureInsertingCCHolderTokens()
  {
    $customer_id = 31;
    $actcode     = '1234';
    $postal_code = '34743';
    $auto_enroll = 1;

    // last modified id by DB
    $cc_holders_identity = 7;

    $sim = new stdClass();
    $sim->ICCID_FULL = 1234567890;
    $sim->CUSTOMER_ID  = 31;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $order = new stdClass();
    $order->ICCID_FULL       = '1234567890';
    $order->postal_code      = '34743';
    $order->bin              = '123456';
    $order->last_four        = '9876';
    $order->expires_date     = '12/16';
    $order->cvv_validation   = '100';
    $order->avs_validation   = '200';
    $order->gateway          = 'TEST';
    $order->merchant_account = '1';
    $order->token            = 'test_token';
    $order->customer_ip      = '127.0.0.1';
    $order->first_name       = 'testFirstName';
    $order->last_name        = 'testLastName';
    $order->address1         = 'test address 1';
    $order->address2         = 'test address 2';
    $order->city             = 'testCity';
    $order->state            = 'CA';

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      )
      ->will($this->returnValue($order));

    // test again for addToUltraCCHolders
    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolders')
      ->with(
        [
          'customer_id'     => $customer_id,
          'bin'             => $order->bin,
          'last_four'       => $order->last_four,
          'expires_date'    => $order->expires_date,
          'cvv_validation'  => $order->cvv_validation,
          'avs_validation'  => $order->avs_validation
        ]
      )
      ->will($this->returnValue($cc_holders_identity));

    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolderTokens')
      ->with(
        [
          'cc_holders_id'    => $cc_holders_identity,
          'gateway'          => $order->gateway,
          'merchant_account' => $order->merchant_account,
          'token'            => $order->token
        ]
      )
      ->will($this->returnValue(FALSE));
    
    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);

    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testFailureUpdatingCustomer()
  {
    $customer_id = 31;
    $actcode     = '1234';
    $postal_code = '34743';
    $auto_enroll = 0;

    // last modified id by DB
    $cc_holders_identity = 7;

    $sim = new stdClass();
    $sim->ICCID_FULL = 1234567890;
    $sim->CUSTOMER_ID  = 31;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $order = new stdClass();
    $order->ICCID_FULL       = '1234567890';
    $order->postal_code      = '34743';
    $order->bin              = '123456';
    $order->last_four        = '9876';
    $order->expires_date     = '12/16';
    $order->cvv_validation   = '100';
    $order->avs_validation   = '200';
    $order->gateway          = 'TEST';
    $order->merchant_account = '1';
    $order->token            = 'test_token';
    $order->customer_ip      = '127.0.0.1';
    $order->first_name       = 'testFirstName';
    $order->last_name        = 'testLastName';
    $order->address1         = 'test address 1';
    $order->address2         = 'test address 2';
    $order->city             = 'testCity';
    $order->state            = 'CA';

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      )
      ->will($this->returnValue($order));

    // test again for addToUltraCCHolders
    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolders')
      ->with(
        [
          'customer_id'     => $customer_id,
          'bin'             => $order->bin,
          'last_four'       => $order->last_four,
          'expires_date'    => $order->expires_date,
          'cvv_validation'  => $order->cvv_validation,
          'avs_validation'  => $order->avs_validation
        ]
      )
      ->will($this->returnValue($cc_holders_identity));

    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolderTokens')
      ->with(
        [
          'cc_holders_id'    => $cc_holders_identity,
          'gateway'          => $order->gateway,
          'merchant_account' => $order->merchant_account,
          'token'            => $order->token
        ]
      )
      ->will($this->returnValue(1));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with($customer_id, [
          'cc_name'         => $order->first_name . ' ' . $order->last_name,
          'cc_address1'     => $order->address1,
          'cc_address2'     => $order->address2,
          'cc_city'         => $order->city,
          'cc_state_region' => $order->state,
          'cc_postal_code'  => $order->postal_code,
          'cc_exp_date'     => $order->expires_date
        ])
      ->will($this->returnValue(0));
    
    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);

    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $customer_id = 31;
    $actcode     = '1234';
    $postal_code = '34743';
    $auto_enroll = 0;

    // last modified id by DB
    $cc_holders_identity = 7;

    $sim = new stdClass();
    $sim->ICCID_FULL = 1234567890;
    $sim->CUSTOMER_ID  = 31;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $order = new stdClass();
    $order->ICCID_FULL       = '1234567890';
    $order->postal_code      = '34743';
    $order->bin              = '123456';
    $order->last_four        = '9876';
    $order->expires_date     = '12/16';
    $order->cvv_validation   = '100';
    $order->avs_validation   = '200';
    $order->gateway          = 'TEST';
    $order->merchant_account = '1';
    $order->token            = 'test_token';
    $order->customer_ip      = '127.0.0.1';
    $order->first_name       = 'testFirstName';
    $order->last_name        = 'testLastName';
    $order->address1         = 'test address 1';
    $order->address2         = 'test address 2';
    $order->city             = 'testCity';
    $order->state            = 'CA';

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      )
      ->will($this->returnValue($order));

    // test again for addToUltraCCHolders
    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolders')
      ->with(
        [
          'customer_id'     => $customer_id,
          'bin'             => $order->bin,
          'last_four'       => $order->last_four,
          'expires_date'    => $order->expires_date,
          'cvv_validation'  => $order->cvv_validation,
          'avs_validation'  => $order->avs_validation
        ]
      )
      ->will($this->returnValue($cc_holders_identity));

    $this->creditCardRepo->expects($this->any())
      ->method('addToUltraCCHolderTokens')
      ->with(
        [
          'cc_holders_id'    => $cc_holders_identity,
          'gateway'          => $order->gateway,
          'merchant_account' => $order->merchant_account,
          'token'            => $order->token
        ]
      )
      ->will($this->returnValue(1));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with($customer_id, [
          'cc_name'         => $order->first_name . ' ' . $order->last_name,
          'cc_address1'     => $order->address1,
          'cc_address2'     => $order->address2,
          'cc_city'         => $order->city,
          'cc_state_region' => $order->state,
          'cc_postal_code'  => $order->postal_code,
          'cc_exp_date'     => $order->expires_date
        ])
      ->will($this->returnValue(1));

    $this->api->setInputValues(['customer_id' => $customer_id, 'actcode' => $actcode, 'postal_code' => $postal_code, 'auto_enroll' => $auto_enroll]);

    $result = $this->api->portal__ClaimCreditCardByActCode();

    $this->assertEmpty($result->data_array['error_codes']);
  }
}