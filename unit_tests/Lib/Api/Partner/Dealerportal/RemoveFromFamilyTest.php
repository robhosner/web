<?php
namespace unit_tests;

use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Dealerportal\RemoveFromFamily;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;
use Ultra\Mvne\Adapter;

class RemoveFromFamilyTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var RemoveFromFamily
   */
  private $api;
  private $familyApi;
  private $adapter;
  private $accountsRepo;
  private $config;
  private $customer_id = 123;
  private $sharedData;

  public function setUp()
  {
    $this->config = $this->getMockBuilder(Configuration::class)
      ->setConstructorArgs([])
      ->setMethods(['getFamilyAPIConfig', 'getPlanFromCosId', 'getSharedDataConfig'])
      ->getMock();

    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->config->expects($this->any())
      ->method('getSharedDataConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->familyApi = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([$this->config])
      ->setMethods(['getFamilyByCustomerID', 'removeFamilyMember', 'createCustomerFamily'])
      ->getMock();

    $this->sharedData = $this->getMockBuilder(SharedData::class)
      ->setConstructorArgs([$this->config])
      ->setMethods(['getBucketIDByCustomerID', 'getBucketByBucketID'])
      ->getMock();

    $this->adapter = $this->getMockBuilder(Adapter::class)
      ->setConstructorArgs([
        $this->getMock(Control::class),
        $this->getMock(CustomerRepository::class),
        $this->getMock(Configuration::class)
      ])
      ->setMethods(['mvneMakeitsoRemoveFromBan'])
      ->getMock();

    $this->accountsRepo = $this->getMock(AccountsRepository::class);

    $this->api = $this->getMockBuilder(RemoveFromFamily::class)
      ->setConstructorArgs([
        $this->familyApi,
        $this->adapter,
        $this->accountsRepo,
        $this->config,
        $this->sharedData
      ])
      ->setMethods(['getValidUltraSessionData'])
      ->getMock();

    $this->api->result          = new \Result();
    $this->api->defaultValues   = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $this->api->setInputValues([
      'security_token'      => 'ST-903CB0815D9B8278-DA7FFFAD9E68189F',
      'customer_id'         => $this->customer_id,
    ]);
  }

  public function testInvalidSession()
  {
    $this->api->expects($this->any())
      ->method('getValidUltraSessionData')
      ->will($this->returnValue([[], 103, 'Error']));

    $getValidUltraSessionDataReflection = new \ReflectionMethod($this->api, 'getValidUltraSessionData');
    $getValidUltraSessionDataReflection->setAccessible(true);

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('103', $result->data_array['error_codes']);
  }

  public function testNoCustomerAccountIsFound()
  {
    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue(null));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testGetFamilyByCustomerIDFails()
  {
    $account = new \stdClass();
    $account->cos_id = 1;

    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $result = new \Result();
    $result->fail();

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($result));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testGetFamilyByCustomerIDReturnsNothing()
  {
    $account = new \stdClass();
    $account->cos_id = 1;

    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $result = new \Result();
    $result->succeed();

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($result));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('ND0001', $result->data_array['error_codes']);
    $this->assertEquals('ERR_API_INTERNAL: Unable to remove customer 123 from family.', $result->get_errors()[0]);
  }

  public function testCustomerDoesNotHaveABanAndFails()
  {
    $account = new \stdClass();
    $account->cos_id = 1;

    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['id' => 1]);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->with(123)
      ->will($this->returnValue($result));

    $this->config->expects($this->any())
      ->method('getPlanFromCosId')
      ->with($account->cos_id)
      ->will($this->returnValue(1));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['bucket_id' => 1]);

    $this->sharedData->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->with($this->customer_id)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->add_data_array(['ban' => 0]);
    $result->succeed();

    $this->sharedData->expects($this->any())
      ->method('getBucketByBucketID')
      ->with(1)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->fail();

    $this->familyApi->expects($this->any())
      ->method('removeFamilyMember')
      ->with(1, 123)
      ->will($this->returnValue($result));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('Customer 123 does not have a ban.', $result->get_warnings());
  }

  public function testFailedToRemoveFamily()
  {
    $account = new \stdClass();
    $account->cos_id = 1;

    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['id' => 1]);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->with(123)
      ->will($this->returnValue($result));

    $this->config->expects($this->any())
      ->method('getPlanFromCosId')
      ->with($account->cos_id)
      ->will($this->returnValue(1));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['bucket_id' => 1]);

    $this->sharedData->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->with($this->customer_id)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['ban' => 1]);

    $this->sharedData->expects($this->any())
      ->method('getBucketByBucketID')
      ->with(1)
      ->will($this->returnValue($result));

    $result = ['success' => true];

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoRemoveFromBan')
      ->with($this->customer_id, 1, null, 1)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->fail();

    $this->familyApi->expects($this->any())
      ->method('removeFamilyMember')
      ->with(1, 123)
      ->will($this->returnValue($result));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testAddingMisoFails()
  {
    $account = new \stdClass();
    $account->cos_id = 1;

    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['id' => 1]);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->with($this->customer_id)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['bucket_id' => 1]);

    $this->sharedData->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->with($this->customer_id)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['ban' => 1]);

    $this->sharedData->expects($this->any())
      ->method('getBucketByBucketID')
      ->with(1)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();

    $this->familyApi->expects($this->any())
      ->method('removeFamilyMember')
      ->with(1, $this->customer_id)
      ->will($this->returnValue($result));

    $this->config->expects($this->any())
      ->method('getPlanFromCosId')
      ->with($account->cos_id)
      ->will($this->returnValue(1));

    $result = ['success' => false, 'errors' => ['boo']];

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoRemoveFromBan')
      ->with($this->customer_id, 1, null, 1)
      ->will($this->returnValue($result));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testRemoveCustomerFromFamilySucceeds()
  {
    $account = new \stdClass();
    $account->cos_id = 1;

    $this->accountsRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->will($this->returnValue($account));

    $result = new \Result();
    $result->succeed();
    $result->add_data_array(['id' => 1]);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->with($this->customer_id)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->fail();

    $this->sharedData->expects($this->any())
      ->method('getBucketIDByCustomerID')
      ->with($this->customer_id)
      ->will($this->returnValue($result));

    $result = new \Result();
    $result->succeed();

    $this->familyApi->expects($this->any())
      ->method('removeFamilyMember')
      ->with(1, $this->customer_id)
      ->will($this->returnValue($result));

    $this->config->expects($this->any())
      ->method('getPlanFromCosId')
      ->with($account->cos_id)
      ->will($this->returnValue(1));

    $result = ['success' => true, 'errors' => []];

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoRemoveFromBan')
      ->with($this->customer_id, 1, null, 1)
      ->will($this->returnValue($result));

    $this->familyApi->expects($this->any())
      ->method('createCustomerFamily')
      ->will($this->returnValue(new \Result(null, true)));

    $result = $this->api->dealerportal__RemoveFromFamily();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}
