<?php

class Provisioning
{
  /**
   * verifyPortInEligibility
   * uses middleware to check port in eligibility
   *
   * @param  $msisdn
   * @return Result
   */
  public static function verifyPortInEligibility($msisdn)
  {
    $result = new Result();

    try
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $mwResult = $mwControl->mwPortInEligibility(
        array(
          'actionUUID' => getNewActionUUID(rand(0, getrandmax()) . time()),
          'msisdn'     => $msisdn
        )
      );

      if ( $mwResult->get_errors() )
      {
        $errors = $mwResult->get_errors();
        throw new Exception($errors[0] . '|EL0001');
      }
      else if ( ! $mwResult->get_data_key('eligible') )
      {
        $error = "ERR_API_INVALID_ARGUMENTS: $msisdn is not eligible to port in - " . $mwResult->get_data_key('failure_reason');
        throw new Exception($error . '|EL0001');
      }

      $result->succeed();
    }
    catch (Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * verifyICCIDGoodToActivate
   * uses middleware to check if ICCID is good to activate
   *
   * @param  $iccidFull
   * @param  $redis will instantiate if null
   * @return Result
   */
  public static function verifyICCIDGoodToActivate($iccidFull, $redis = NULL)
  {
    $result = new Result();

    try
    {
      if ( ! $redis)
        $redis = new \Ultra\Lib\Util\Redis;

      // verify that SIM can be activated on MVNE
      if ( $redis->get( 'iccid/good_to_activate/' . $iccidFull ) )
        $redis->del( 'iccid/good_to_activate/' . $iccidFull );
      else // check on MVNE
      {
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        $mwResult = $mwControl->mwCanActivate(array(
          'iccid'      => $iccidFull,
          'actionUUID' => getNewActionUUID('portal ' . time())
        ));

        if ($mwResult->is_failure() || ! isset($mwResult->data_array['available']))
          throw new Exception('ERR_API_INTERNAL: the ICCID cannot be activated|VV0066');
      }

      $result->succeed();
    }
    catch (Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * verifyNumberNotInSystem
   * verifies that msisdn is not already in our system
   *
   * @param  $msisdn
   * @return Result
   */
  public static function verifyNumberNotInSystem($msisdn)
  {
    $result = new Result();

    try
    {
      if ( $customer = get_customer_from_msisdn($msisdn, 'u.CUSTOMER_ID') )
      {
        // get current state of $msisdn
        $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

        if ( ! $state )
          throw new Exception("ERR_API_INTERNAL: customer (" . $customer->CUSTOMER_ID . ") state could not be determined|UN0001");

        if ( ( $state['state'] != 'Port-In Denied' ) && ( $state['state'] != 'Cancelled' ) )
        {
          if ( $state['state'] == "Port-In Requested" )
            throw new Exception("ERR_API_INVALID_ARGUMENTS: $msisdn is still being ported into our system.|PO0005");

          if ( $state['state'] == "Provisioned" )
            throw new Exception("ERR_API_INVALID_ARGUMENTS: $msisdn is ported to our system and is awaiting payment.|PO0006");

          if ( $state['state'] == "Active" )
            throw new Exception("ERR_API_INVALID_ARGUMENTS: $msisdn is ported to our system and working.|PO0007");

          throw new Exception("ERR_API_INVALID_ARGUMENTS: $msisdn is already in our system|PO0008");
        }
        else
        {
          # There is a customer associated with $msisdn which is in 'Port-In Denied' or 'Cancelled',
          # but HTT_CUSTOMERS_OVERLAY_ULTRA.current_mobile_number is set.
          # Before proceeding with the port attempt we have to nullify the previous entry in HTT_CUSTOMERS_OVERLAY_ULTRA.current_mobile_number

          \logit("nullify previous entry with MSISDN = $msisdn");

          $updateCustomerQuery = htt_customers_overlay_ultra_update_query(
            array(
              'customer_id'           => $customer->CUSTOMER_ID,
              'current_mobile_number' => 'NULL'
            )
          );

          if ( ! run_sql_and_check($updateCustomerQuery) )
            throw new Exception('ERR_API_INTERNAL: DB error - could not update htt_customers_overlay_ultra|DB0001');
        }
      }

      $result->succeed();
    }
    catch (Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }

  /**
   * configureBoltOns
   * takes input params from API [bolt_ons]
   * returns Result data_array with [bolt_ons_cost,bolt_on_configuration]
   *
   * @param  $msisdn
   * @return Result
   */
  public static function configureBoltOns($bolt_ons)
  {
    $result = new Result();

    $bolt_ons_cost = 0;
    $bolt_on_configuration = array();

    try
    {
      if (is_array($bolt_ons))
      {
        foreach( $bolt_ons as $bolt_on_id )
        {
          if ($bolt_on_id = trim($bolt_on_id))
          {
            $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id );

            if ( ! $boltOnInfo )
            {
              throw new Exception('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid|VV0101');
            }

            dlog('',"%s : boltOnInfo = %s",$bolt_on_id,$boltOnInfo);

            $bolt_on_configuration[ $boltOnInfo['option_attribute'] ] = $boltOnInfo['cost'];
            $bolt_ons_cost += $boltOnInfo['cost'];
          }
        }
      }

      $result->succeed();
    }
    catch (Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    $result->add_to_data_array('bolt_on_configuration', $bolt_on_configuration);
    $result->add_to_data_array('bolt_ons_cost', $bolt_ons_cost);

    \logit('new_bolt_on_configuration = ' . json_encode($bolt_on_configuration));

    return $result;
  }

  /**
   * intraBrandPortOperations
   * soft clones and cancels previous customer
   * performs state transition unique to intra brand port-ins on new customer
   *
   * @param Integer old customer ID (porting out)
   * @param Object HTT_INVENTORY_SIM row to create a new customer with (porting in)
   * @return Result
   */
  public static function intraBrandPortOperations($customer_id, $sim)
  {
    $result = new Result();

    $newCustomer = null;

    $result_status = array();
    $result_status['success'] = false;

    try
    {
      $referenceUID = create_guid( __FUNCTION__ );

      $customer = get_customer_from_customer_id($customer_id);

      \logit('CUSTOMER: ' . json_encode($customer));

      // clear balance of customer we're going to cancel
      $clearBalanceResult = accounts_clear_balance($customer->CUSTOMER_ID);
      if (count($clearBalanceResult['errors']))
        \logError($clearBalanceResult['errors'][0]);

      $newCustomer = clone_customer($customer->CUSTOMER_ID, $sim);
      if ( ! $newCustomer)
        throw new Exception('ERR_API_INTERNAL: failure cloning customer for intra port|DB0001');

      // performs State Transition to cancel prev customer id
      $result_status = change_state_cancel_intra_port($customer, array('customer_id' => $customer_id));

      if ( $result_status['success'] != 1 )
        throw new Exception('ERR_API_INTERNAL: state transition error (2)|SM0001');

      if ( ! self::insertBillingHistoryForIntraPort($customer, 'CANCELLATION', $referenceUID))
        throw new Exception('ERR_API_INTERNAL: failure inserting into HTT_BILLING_HISTORY (1)|DB0001');

      if ( ! self::insertBillingHistoryForIntraPort($newCustomer, 'CREATION', $referenceUID))
        throw new Exception('ERR_API_INTERNAL: failure inserting into HTT_BILLING_HISTORY (2)|DB0001');

      // update customer notes with intra-brand port-in info
      $check = run_sql_and_check(htt_customers_overlay_ultra_update_query(array(
        'customer_id' => $newCustomer->CUSTOMER_ID,
        'notes'       => sprintf('intra-brand port-in %d to %d', $customer->BRAND_ID, $sim->BRAND_ID)
      )));

      if ( ! $check)
        throw new Exception('ERR_API_INTERNAL: failure updating HTT_CUSTOMERS_OVERLAY_ULTRA.NOTES|DB0001');

      $result->succeed();
    }
    catch (Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    $result->add_to_data_array('customer', $newCustomer);
    $result->add_to_data_array('result_status', $result_status);

    return $result;
  }

  /**
   * updateActivationHistoryForMsisdn
   * updates activation history for MSISDN
   * flushes ultra activation history catch for customer_id
   *
   * @param  $customer_id
   * @param  $msisdn
   * @return null
   */
  public static function updateActivationHistoryForMsisdn($customer_id, $msisdn)
  {
    $sql = ultra_activation_history_update_query(array(
      'customer_id' => $customer_id,
      'msisdn'      => $msisdn
    ));
      
    if ( ! is_mssql_successful(logged_mssql_query($sql)))
      \logError('ERROR: failed to update ULTRA.HTT_ACTIVATION_HISTORY');

    flush_ultra_activation_history_cache_by_customer_id($customer_id);
  }

  /**
   * insertBillingHistoryForIntraPort
   * inserts a row into HTT_BILLING_HISTORY for INTRAPORT operations
   *
   * @param  Object $customer
   * @param  String $type ex. CANCELLATION || CREATION
   * @param  String $referenceUID
   * @return null
   */
  public static function insertBillingHistoryForIntraPort($customer, $type, $referenceUID)
  {
    return run_sql_and_check_result(htt_billing_history_insert_query(
      array(
        'customer_id'            => $customer->CUSTOMER_ID,
        'date'                   => 'now',
        'source'                 => 'INTRAPORT',
        'result'                 => 'COMPLETE',
        'entry_type'             => 'INTRAPORT',
        'is_commissionable'      => 0,
        'stored_value_change'    => 0,
        'balance_change'         => 0,
        'package_balance_change' => 0,
        'charge_amount'          => 0,
        'cos_id'                 => $customer->cos_id,
        'detail'                 => 'intraBrandPortOperations',
        'description'            => 'INTRAPORT.' . $type,
        'reference'              => $referenceUID,
        'reference_source'       => get_reference_source('INTRAPORT'),
        'terminal_id'            => '0',
        'commissionable_charge_amount' => 0
      )
    ));
  }

  /**
   * validate PINS / pinsToApply
   * @param String $pinsToApply
   * @return Result 
   */
  public static function validatePinsToApply($pinsToApply)
  {
    $result = new \Result();

    try
    {
      if ( ! $pinsToApply || $pinsToApply == '' )
        throw new Exception('ERR_API_INVALID_ARGUMENTS: Missing pinsToApply, required for PINS Payments');

      if ( isset($pinsToApply) && $pinsToApply != '' )
      {
        list($errors, $validation) = validate_pins_to_apply($pinsToApply);

        foreach ($errors as $error)
          $result->add_error($error);

        $result->add_to_data_array('validation', $validation);

        if ( ! $result->has_errors())
          $result->succeed();
      }
    }
    catch (Exception $e)
    {
      $result->add_error($e->getMessage());
    }

    return $result;
  }
}
