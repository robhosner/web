<?php

require_once 'classes/UltraAcc.php';

/**
 * Generic class for DB ULTRA_ACC data access
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class UltraAcc
{
  /**
   * Class constructor
   */
  public function __construct( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);
  }

  public function dbConnect()
  {
    // connect to ULTRA_ACC DB
    \Ultra\Lib\DB\ultra_acc_connect();
  }

  /**
   * setParameters
   *
   * Set parameters as object members
   *
   * @return NULL
   */
  protected function setParameters($params)
  {
    $parametersNames = $this->getParametersNames();

    foreach ( $parametersNames as $paramName )
      if ( isset( $params[ $paramName ] ) )
        $this->{$paramName} = $params[ $paramName ];
  }

  /**
   * getParameters
   *
   * Get parameters from object members
   *
   * @return array
   */
  protected function getParameters()
  {
    $parametersNames = $this->getParametersNames();

    $params = array();

    foreach ( $parametersNames as $paramName )
      if ( property_exists($this, $paramName ) )
        $params[ $paramName ] = $this->{$paramName};

    return $params;
  }

  /**
   * getParametersNames
   *
   * List of object members names, which correspond to columns of a DB table in ULTRA_ACC
   *
   * @return array
   */
  protected function getParametersNames()
  {
    return array();
  }

  protected function loadMembers( $data )
  {
    $parametersNames = $this->getParametersNames();

    foreach ( $parametersNames as $paramName )
    {
      $columnName = strtoupper($paramName);

      if ( isset($data->$columnName) )
        $this->{$paramName} = $data->$columnName;
    }
  }
}

?>
