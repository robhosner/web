<?php


include_once('db/htt_messaging_queue.php');
include_once('lib/messaging/functions.php');
require_once 'Ultra/Lib/Util/Redis/Messaging.php';


/**
 * Ultra Message class
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/How+it+works+-+Messaging
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra Messaging
 */
class Message
{
  // we may need an abstract method that saves to HTT_MESSAGING_QUEUE , HTT_MESSAGING_QUEUE_PARAM

  public $data_array = array();
  public $redis_messaging;

  /**
   * Class constructor
   */
  public function __construct($data)
  {
    \logInfo(json_encode($data));

    if ( is_object($data) )
      $this->data_array = get_object_vars($data);
    elseif( is_array($data) )
      $this->data_array = $data;
    else
      \logError("new Message : invalid data");

    if ( isset($this->data_array['CUSTOMER_ID']) && ( ! isset($this->data_array['customer']) ) )
      $this->data_array['customer'] = get_customer_from_customer_id($this->data_array['CUSTOMER_ID']);

    // we need a \Ultra\Lib\Util\Redis\Messaging object
    $this->redis_messaging = new \Ultra\Lib\Util\Redis\Messaging();
  }

  /**
   * skip_dispatch
   *
   * The conditional message should not be sent because the conditions are changed (For example: the customer reactivated the account).
   * The message is purposedly not delivered. HTT_MESSAGING_QUEUE.STATUS is set to 'SKIPPED'
   *
   * @return Result object
   */
  public function skip_dispatch()
  {
    $htt_messaging_queue_update_query = htt_messaging_queue_update_query(
      array(
        'htt_messaging_queue_id' => $this->data_array['HTT_MESSAGING_QUEUE_ID'],
        'status'                 => 'SKIPPED',
        'attempt_log'            => $this->data_array['ATTEMPT_LOG'].' ['.gmdate("Y-m-d H:i:s").' '.getmypid().' skipped ]',
        'next_attempt_datetime'  => 'NULL'
      )
    );

    if ( empty( $this->redis_messaging ) )
      // we need a \Ultra\Lib\Util\Redis\Messaging object
      $this->redis_messaging = new \Ultra\Lib\Util\Redis\Messaging();

    // remove message Id from queue
    $this->redis_messaging->freeMessageId( $this->data_array['HTT_MESSAGING_QUEUE_ID'] , extrapolate_message_priority( $this->data_array['REASON'] ) );

    \logDebug("skip_dispatch sql = $htt_messaging_queue_update_query");

    return run_sql_and_check_result($htt_messaging_queue_update_query);
  }

  /**
   * dispatch
   *
   * Send the message with the appropriate method (HTT_MESSAGING_QUEUE.TYPE)
   *
   * @return Result object
   */
  public function dispatch()
  {
    \logDebug('dispatch TYPE = '.$this->data_array['TYPE']);

    $result = NULL;

    if ( $this->lock_message_for_dispatch() )
    {

      switch ($this->data_array['TYPE'])
      {
      case 'SMS_INTERNAL':
        $result = $this->dispatch_sms_internal();
        $result = $this->unlock_message_after_dispatch($result);
        break;
      case 'SMS_EXTERNAL':
        $result = $this->dispatch_sms_external();
        $result = $this->unlock_message_after_dispatch($result);
        break;
      case 'EMAIL_POSTAGEAPP':
        $result = $this->dispatch_email_postageapp();
        $result = $this->unlock_message_after_dispatch($result);
        break;
      /* add more delivery methods here */
      default:
        \logError("Invalid HTT_MESSAGING_QUEUE.TYPE : ".$this->data_array['TYPE']);
        $result = make_error_Result("Invalid HTT_MESSAGING_QUEUE.TYPE : ".$this->data_array['TYPE']);
      }

    }

    return $result;
  }

  /**
   * dispatch_email_postageapp
   *
   * Send email using postageapp
   *
   * @return Result object
   */
  private function dispatch_email_postageapp()
  {
    $result = make_error_Result("dispatch_email_postageapp : case not handled");

    if ( isset($this->data_array['PARAMS']['template_name']) )
    {
      $success = funcSendCustomerEmail(
        array(
          'template_name'   => $this->data_array['PARAMS']['template_name'],
          'postage_app_key' => POSTAGE_API_KEY_ULTRA,
          'customer'        => $this->data_array['customer'],
          'template_params' => $this->data_array['PARAMS']
        )
      );

      if ( $success )
        $result = make_ok_Result(NULL);
      else
      {
        \logError('dispatch_email_postageapp : error after funcSendCustomerEmail');

        $result = make_error_Result("dispatch_email_postageapp : error after funcSendCustomerEmail");
      }
    }
    else
      \logError("dispatch_email_postageapp : case not handled ".json_encode($this));

    return $result;
  }

  /**
   * dispatch_sms_internal
   *
   * Send an SMS using 3c
   *
   * @return Result object
   */
  private function dispatch_sms_internal()
  {
    $result = make_error_Result("dispatch_sms_internal : case not handled");

    // SMS message is split in 2 parts from the caller
    if ( isset($this->data_array['PARAMS']['message1']) )
    {
      $this->data_array['PARAMS']['message'] = $this->data_array['PARAMS']['message1'];

      if ( isset($this->data_array['PARAMS']['message2']) )
        $this->data_array['PARAMS']['message'] .= $this->data_array['PARAMS']['message2'];
    }

    if ( isset($this->data_array['PARAMS']['message']) )
    {
      $return = funcSendCustomerSMS_internal(
        array(
          'customer'   => $this->data_array['customer'],
          'message'    => $this->data_array['PARAMS']['message'],
          'client_tag' => $this->data_array['HTT_MESSAGING_QUEUE_ID']
        )
      );

      if ( $return['sent'] )
        $result = make_ok_Result(NULL);
      else
      {
        $result = make_error_Result("dispatch_sms_internal : error after funcSendCustomerSMS_internal");
        $result->data_array['ResultCode'] = $return['ResultCode'];
      }
    }
    elseif ( isset($this->data_array['PARAMS']['message_type']) )
    {
      // Example: {"message_type":"data_block_100_percent"}

      $sms_params = $this->data_array['PARAMS'];

      $sms_params['customer'] = $this->data_array['customer'];

      $sms_params['client_tag'] = $this->data_array['HTT_MESSAGING_QUEUE_ID'];

      $return = funcSendExemptCustomerSMS( $sms_params );

      if ( $return['sent'] )
        $result = make_ok_Result(NULL);
      else
      {
        $result = make_error_Result("dispatch_sms_internal : error after funcSendExemptCustomerSMS");
        $result->data_array['ResultCode'] = $return['ResultCode'];
      }
    }
    else
      \logError("dispatch_sms_internal : case not handled ".json_encode($this));

    return $result;
  }

  /**
   * dispatch_sms_external
   *
   * Send SMS using Silverstreet
   *
   * @return Result object
   */
  private function dispatch_sms_external()
  {
    $result = make_error_Result("dispatch_sms_external : case not handled");

    if ( isset($this->data_array['PARAMS']['message']) )
    {
      $return = funcSendCustomerSMS_external(
        array(
          'customer' => $this->data_array['customer'],
          'message'  => $this->data_array['PARAMS']['message'],
          'params'   => $this->data_array['PARAMS']
        )
      );

      if ( $return['sent'] )
        $result = make_ok_Result(NULL);
      else
      {
        \logError(json_encode($return['errors']));

        $result = make_error_Result("dispatch_sms_external : error after funcSendCustomerSMS_external");
      }
    }
    elseif ( isset($this->data_array['PARAMS']['message_type']) )
    {
      $return = funcSendCustomerSMS_external(
        array(
          'customer' => $this->data_array['customer'],
          'message'  => \Ultra\Messaging\Templates\SMS_by_language(
            array('message_type' => $this->data_array['PARAMS']['message_type']),
            $this->data_array['customer']->preferred_language,
            $this->data_array['customer']->BRAND_ID
          ),
          'params'  => $this->data_array['PARAMS']
        )
      );

      if ( $return['sent'] )
        $result = make_ok_Result(NULL);
      else
      {
        \logError(json_encode($return['errors']));

        $result = make_error_Result("dispatch_sms_external : error after funcSendCustomerSMS_external");
      }
    }
    else
      \logError("dispatch_sms_external : case not handled ".json_encode($this));

    return $result;
  }

  /**
   * lock_message_for_dispatch
   *
   * Sets HTT_MESSAGING_QUEUE.STATUS to 'SENDING '.getmypid()
   *
   * @return boolean - success or failure
   */
  private function lock_message_for_dispatch()
  {
    $htt_messaging_queue_update_query = htt_messaging_queue_update_query(
      array(
        'htt_messaging_queue_id' => $this->data_array['HTT_MESSAGING_QUEUE_ID'],
        'status'                 => 'SENDING '.getmypid()
      )
    );

    $return = run_sql_and_check_result($htt_messaging_queue_update_query);

    return $return->is_success();
  }

  /**
   * unlock_message_after_dispatch
   *
   * Update HTT_MESSAGING_QUEUE columns with the result of the delivery attempt.
   *
   * @return Result object
   */
  private function unlock_message_after_dispatch($dispatch_result)
  {
    $htt_messaging_queue_update_query = '';

    // we need a \Ultra\Lib\Util\Redis\Messaging object
    $this->redis_messaging = new \Ultra\Lib\Util\Redis\Messaging();

    // remove message Id from queue
    $this->redis_messaging->freeMessageId( $this->data_array['HTT_MESSAGING_QUEUE_ID'] , extrapolate_message_priority( $this->data_array['REASON'] ) );

    if ( $dispatch_result->is_success() )
    {
      \logDebug("unlock_message_after_dispatch with status = SENT");

      $htt_messaging_queue_update_query = htt_messaging_queue_update_query(
        array(
          'htt_messaging_queue_id' => $this->data_array['HTT_MESSAGING_QUEUE_ID'],
          'status'                 => 'SENT',
          'attempt_count'          => '+1',
          'attempt_log'            => $this->data_array['ATTEMPT_LOG'].' ['.gmdate("Y-m-d H:i:s").' '.getmypid().' success ]',
          'next_attempt_datetime'  => 'NULL'
        )
      );
    }
    else
    {
      \logError("unlock_message_after_dispatch with status = ERROR");

      $next_attempt_datetime = NULL;
      $retry_seconds         = NULL;

      if (in_array($dispatch_result->data_array['ResultCode'], array(333, 224, 306)))
        list( $next_attempt_datetime , $retry_seconds ) = $this->get_next_attempt_datetime_after_error();

      $htt_messaging_queue_update_query = htt_messaging_queue_update_query(
        array(
          'htt_messaging_queue_id' => $this->data_array['HTT_MESSAGING_QUEUE_ID'],
          'status'                 => 'ERROR',
          'attempt_count'          => '+1',
          'attempt_log'            => $this->data_array['ATTEMPT_LOG'].' ['.gmdate("Y-m-d H:i:s").' '.getmypid()." error ({$dispatch_result->data_array['ResultCode']}) ]",
          'next_attempt_datetime'  => $next_attempt_datetime
        )
      );

      // re-add to Redis sorted set with new timestamp if needed
      if ( $retry_seconds )
        $this->redis_messaging->addMessageId( $this->data_array['HTT_MESSAGING_QUEUE_ID'] , extrapolate_message_priority( $this->data_array['REASON'] ) , time() + $retry_seconds );
    }

    \logDebug("unlock_message_after_dispatch sql = $htt_messaging_queue_update_query");

    return run_sql_and_check_result($htt_messaging_queue_update_query);
  }

  /**
   * get_next_attempt_datetime_after_error
   *
   * Returns the date of the next retry
   *
   * @return string
   */
  private function get_next_attempt_datetime_after_error()
  {
    /*
    logic for retries after error : +1 +3 +3
    email - depends on the response
    if it's a bad email then never
    */

    // TODO: we may not want to retry if too late in the day, or we may want to retry the next day in the morning (this will fail for conditional messages)

    $delay_times = \Ultra\UltraConfig\getMessengerRunnerDelayTimes();

    $seconds = isset( $delay_times[ $this->data_array['ATTEMPT_COUNT'] ] )
      ? $delay_times[ $this->data_array['ATTEMPT_COUNT'] ]
      : NULL;

    $next_attempt_datetime = ($seconds) ? sprintf("DATEADD(ss,%d,getutcdate())", $seconds) : 'NULL';

    return array( $next_attempt_datetime , $seconds );
  }

}

/**
 * get_next_message_to_send
 *
 * Retrieves next message to be sent
 *
 * @return object of class Message
 */
function get_next_message_to_send( $redis_messaging = NULL )
{
  $message = NULL;

  if ( ! $redis_messaging )
    $redis_messaging = new \Ultra\Lib\Util\Redis\Messaging();

  $next_htt_messaging_queue_id = $redis_messaging->getNextMessageId();

  if ( ! $next_htt_messaging_queue_id )
  {
    \logDebug('No new HTT_MESSAGING_QUEUE_ID obtained');

    return NULL;
  }

  try
  {
    // load data from HTT_MESSAGING_QUEUE
    $htt_messaging_queue_select_query = htt_messaging_queue_select_query(
      array(
        'htt_messaging_queue_id' => $next_htt_messaging_queue_id
      )
    );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_messaging_queue_select_query));

    if ( ! ( $query_result && is_array($query_result) && count($query_result) ) )
    {
      // remove $next_htt_messaging_queue_id from queue

      $redis_messaging->freeMessageId( $next_htt_messaging_queue_id , 0 );

      throw new Exception("HTT_MESSAGING_QUEUE_ID $next_htt_messaging_queue_id not found in DB");
    }

    $message_data = $query_result[0];

    // sanity check: STATUS should be 'TO_SEND' or 'ERROR'
    if ( ( $message_data->STATUS != 'TO_SEND' ) && ( $message_data->STATUS != 'ERROR' ) )
    {
      // remove message Id from queue
      $redis_messaging->freeMessageId( $next_htt_messaging_queue_id , extrapolate_message_priority( $message_data->REASON ) );
      throw new Exception("HTT_MESSAGING_QUEUE_ID $next_htt_messaging_queue_id already sent!");
    }

    // load data from HTT_MESSAGING_QUEUE_PARAM
    $htt_messaging_queue_param_select_query = htt_messaging_queue_param_select_query(
      array(
        'htt_messaging_queue_id' => $next_htt_messaging_queue_id
      )
    );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_messaging_queue_param_select_query));

    if ( ( is_array($query_result) ) && count($query_result) > 0 )
    {
      $message_data->PARAMS = array();

      foreach( $query_result as $i => $row_result )
      {
        $message_data->PARAMS[ $row_result->PARAM ] = $row_result->VALUE;
      }
    }

    $message = new Message($message_data);
  }
  catch(\Exception $e)
  {
    \logDebug( $e->getMessage() );
  }

  return $message;
}

