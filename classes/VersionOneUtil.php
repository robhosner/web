<?php

class VersionOneUtil
{
  private $testing = FALSE;

  public function testing($b = NULL)
  {
    if ($b) return $this->testing = FALSE;
    else    return $this->testing;
  }
}