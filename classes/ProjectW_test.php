<?php

require_once 'db.php';
require_once 'classes/ProjectW.php';
include_once('Ultra/Lib/MiddleWare/Adapter/Control.php');
include_once('Ultra/tests/SoapLog.php');
require_once 'Ultra/Lib/DB/Customer.php';


class Test_run
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );
    $object->run();
  }
}

class Test_runSMSAfterMigrationaDay
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $method = new ReflectionMethod('ProjectW', 'runSMSAfterMigrationaDay');
    $method->setAccessible(true);

    $method->invoke(
      $object,
      1,
      1
    );
  }
}

class Test_createCustomer
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'createCustomer');
    $method->setAccessible(true);

    $success = $method->invoke(
      $object,
      (array) $object->getCustomerData()
    );
  }
}

class Test_addBoltOnMinutes
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'addBoltOnMinutes');
    $method->setAccessible(true);

    $method->invoke(
      $object,
      10
    );
  }
}

class Test_validateImportFile
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $validationResult = $object->validateImportFile( $argv[2] );

    print_r( $validationResult );
  }
}

class Test_addBillingHistoryActivationRow
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'addBillingHistoryActivationRow');
    $method->setAccessible(true);

    $method->invoke(
      $object
    );
  }
}

class Test_addBillingHistoryBalanceRow
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'addBillingHistoryBalanceRow');
    $method->setAccessible(true);

    $method->invoke(
      $object
    );
  }
}

class Test_checkResources
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );
    $object->checkResources();
  }
}

class Test_enrollMEP
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $sql = "DELETE FROM ULTRA.CUSTOMER_OPTIONS WHERE CUSTOMER_ID = (select customer_id from ULTRA.UNIVISION_IMPORT where UNIVISION_IMPORT_ID = $univision_import_id)";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "ULTRA.CUSTOMER_OPTIONS update failed\n";
      exit;
    }

    $object->loadTaskByID( $univision_import_id );

    $success = $object->enrollMEP();
  }
}

class Test_addBoltOnsAfterActivation
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'addBoltOnsAfterActivation');
    $method->setAccessible(true);

    $method->invoke(
      $object
    );
  }
}

class Test_getPlanCost
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $cost = $object->getPlanCost([
      'PLAN_NAME' => 'UV50'
    ]);

    echo "cost = $cost\n";
  }
}

class Test_getNextTaskData
{       
  function test( $argv )
  {   
    $object = new \ProjectW( NULL , TRUE );

    $method = new ReflectionMethod('ProjectW', 'getNextTaskData');
    $method->setAccessible(true);

    $method->invoke(
      $object
    );
  }
}

class Test_changeSIM
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );
    $object->setCustomerData(
      array(
        'customer_id'       => 1,
        'msisdn'            => '1001001000',
        'old_iccid'         => '1001001000100100100',
        'new_iccid'         => '2001001000100100100'
      )
    );

    $method = new ReflectionMethod('ProjectW', 'changeSIM');
    $method->setAccessible(true);

    $method->invoke(
      $object
    );
  }
}

class Test_portInCustomer
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );
    $object->setCustomerData(
      array(
        'customer_id'       => 1,
        'msisdn'            => '1001001000',
        'iccid'             => '1001001000100100100',
        'zipcode'           => '12345',
        'wholesalePlan'     => '111',
        'ultra_plan_name'   => 'TWENTY_NINE',
        'preferredLanguage' => 'en'
      )
    );

    $method = new ReflectionMethod('ProjectW', 'portInCustomer');
    $method->setAccessible(true);

    $method->invoke(
      $object
    );
  }
}

class Test_processNextTaskActivateSubscriber
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'processNextTaskActivateSubscriber');
    $method->setAccessible(true);
    
    $method->invoke(
      $object
    );
  }
}

class Test_addMinutesOnActivation
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];
    $cos_id              = $argv[3];

    $object->loadTaskByID( $univision_import_id );

    $method = new ReflectionMethod('ProjectW', 'addMinutesOnActivation');
    $method->setAccessible(true);

    $method->invoke(
      $object,
      $cos_id
    );
  }
}

class Test_deleteCustomerIfRetryPort
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $method = new ReflectionMethod('ProjectW', 'deleteCustomerIfRetryPort');
    $method->setAccessible(true);

    $customerData = [
      'UNIVISION_IMPORT_ID' => 123456,
      'CUSTOMER_ID'         => 123456,
      'MSISDN'              => 1234512345
    ];

    $method->invoke(
      $object,
      $customerData
    );

    $method->invoke(
      $object,
      []
    );
  }
}

class Test_addBalanceOnActivation
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $method = new ReflectionMethod('ProjectW', 'addBalanceOnActivation');
    $method->setAccessible(true);

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );

    $method->invoke(
      $object
    );
  }
}

class Test_loadTaskByID
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    $object->loadTaskByID( $univision_import_id );
  }
}

class Test_updateOverlayAfterActivation
{ 
  function test( $argv )
  { 
    $object = new \ProjectW( NULL , TRUE );
    
    $univision_import_id = $argv[2];
    
    if ( ! $object->loadTaskByID( $univision_import_id ) )
    {
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $object->updateOverlayAfterActivation();
  }
}

class Test_recordPortFailed
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    // mock ULTRA.UNIVISION_IMPORT data for our test
    $sql = "UPDATE ULTRA.UNIVISION_IMPORT SET IMPORT_HISTORY = '' , STATUS = 'PORT_CALLED' WHERE UNIVISION_IMPORT_ID = $univision_import_id";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "univision_import_id $univision_import_id update failed\n";
      exit;
    }

    if ( ! $object->loadTaskByID( $univision_import_id ) )
    { 
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $error = 'brain error';

    $object->recordPortFailed( $error );
  }
}

class Test_recordChangeSIMFailed
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    // mock ULTRA.UNIVISION_IMPORT data for our test
    $sql = "UPDATE ULTRA.UNIVISION_IMPORT SET IMPORT_HISTORY = '' , STATUS = 'CHANGESIM_CALLED' WHERE UNIVISION_IMPORT_ID = $univision_import_id";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "univision_import_id $univision_import_id update failed\n";
      exit;
    }

    if ( ! $object->loadTaskByID( $univision_import_id ) )
    {
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $error = 'brain error';

    $object->recordChangeSIMFailed( $error );
  }
}

class Test_recoverChangeSIMError
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    if ( ! $object->loadTaskByID( $univision_import_id ) )
    {
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $customerData = $object->getCustomerData();

    echo "{$customerData->UNIVISION_IMPORT_ID}\n";

    echo "{$customerData->MSISDN}\n";

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $data = array(
      'actionUUID' => 'test',
      'iccid'      => NULL,
      'msisdn'     => $customerData->MSISDN
    );

    $result = $mwControl->mwQuerySubscriber( $data );

    if ( ! empty( $result->data_array['ResultCode'] ) )
    {
      echo "ResultCode = {$result->data_array['ResultCode']}\n";
    }

    if ( ! empty( $result->data_array['ResultMsg'] ) )
    {
      echo "ResultMsg = {$result->data_array['ResultMsg']}\n";
    }

    if ( isset($result->data_array['body']) )
      if ( $result->data_array['body']->SubscriberStatus == 'Active' )
      {
        echo "ChangeSIM was successful, proceed with activation\n";

        // ChangeSIM was successful, proceed with activation

        $sql = "UPDATE ULTRA.UNIVISION_IMPORT SET IMPORT_HISTORY = '' , STATUS = 'CHANGESIM_CALLED' WHERE UNIVISION_IMPORT_ID = $univision_import_id";

        $success = is_mssql_successful(logged_mssql_query($sql));

        if ( ! $success  )
        { 
          echo "univision_import_id $univision_import_id update failed\n";
          exit;
        }

        $object->recordActivated();
      }
  }
}

class Test_recordActivated
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    // mock ULTRA.UNIVISION_IMPORT data for our test
    $sql = "UPDATE ULTRA.UNIVISION_IMPORT SET IMPORT_HISTORY = '' , STATUS = 'CHANGESIM_CALLED' WHERE UNIVISION_IMPORT_ID = $univision_import_id";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "univision_import_id $univision_import_id update failed\n";
      exit;
    }

    $sql = "DELETE FROM dbo.HTT_ACTIVATION_LOG WHERE ACTIVATED_CUSTOMER_ID = (select customer_id from ULTRA.UNIVISION_IMPORT where UNIVISION_IMPORT_ID = $univision_import_id)";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "dbo.HTT_ACTIVATION_LOG delete failed\n";
      #exit;
    }

    $sql = "DELETE FROM dbo.HTT_ACTIVATION_LOG WHERE ICCID_FULL = (select ICCID_FULL from ULTRA.UNIVISION_IMPORT where UNIVISION_IMPORT_ID = $univision_import_id)";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "ULTRA.CUSTOMER_OPTIONS delete failed\n";
     # exit;
    }

/*
    $sql = "DELETE FROM ULTRA.CUSTOMER_OPTIONS WHERE CUSTOMER_ID = (select customer_id from ULTRA.UNIVISION_IMPORT where UNIVISION_IMPORT_ID = $univision_import_id)";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "ULTRA.CUSTOMER_OPTIONS delete failed\n";
      exit;
    }
*/

    $sql = "DELETE FROM HTT_ACTIVATION_LOG WHERE ICCID_NUMBER = '1001110101010110021' or iccid_full = '1001110101010110021' or activated_customer_id = 19938";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $object->loadTaskByID( $univision_import_id ) )
    {
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $object->recordActivated();
  }
}

class Test_processNextTaskChangeSIM
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    if ( ! $object->loadTaskByID( $univision_import_id ) )
    {
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $method = new ReflectionMethod('ProjectW', 'processNextTaskChangeSIM');
    $method->setAccessible(true);
    
    $method->invoke(
      $object
    );
  }
}

class Test_recordActivatedTemp
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );

    $univision_import_id = $argv[2];

    // mock ULTRA.UNIVISION_IMPORT data for our test
    $sql = "UPDATE ULTRA.UNIVISION_IMPORT SET IMPORT_HISTORY = '' , STATUS = 'PORT_CALLED' WHERE UNIVISION_IMPORT_ID = $univision_import_id";

    $success = is_mssql_successful(logged_mssql_query($sql));

    if ( ! $success  )
    {
      echo "univision_import_id $univision_import_id update failed\n";
      exit;
    }

    if ( ! $object->loadTaskByID( $univision_import_id ) )
    {
      echo "univision_import_id $univision_import_id not found\n";
      exit;
    }

    $object->recordActivatedTemp();
  }
}

class Test_checkMWResources
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );
    $object->checkMWResources();
  }
}

class Test_checkSettings
{
  function test( $argv )
  {
    $object = new \ProjectW( NULL , TRUE );
    $object->checkSettings();
  }
}

teldata_change_db();

$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$testObject = new $testClass();
$testObject->test( $argv );

/*

Import file:
php classes/ProjectW/Parser_test.php import test/ProjectW/Parser/ ProjectW-2016-01-27-1.csv ''

Validation after import:
php classes/ProjectW_test.php validateImportFile ProjectW-2016-01-27-1.csv

Set to ONGOING
curl -i 'https://rgalli3-dev.uvnv.com/pr/projectw/2/ultra/api/projectw__SetStatusOngoing' -d 'filename=ProjectW-2016-01-27-1.csv'

*/
