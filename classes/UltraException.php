<?php

// Exception subclass with $error_code

class UltraException extends Exception
{
  protected $error_code;

  // Redefine the exception so message isn't optional
  public function __construct( $message, $code=0, Exception $previous=NULL, $error_code='' )
  {
    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);

    $this->error_code = $error_code;
  }

  // custom string representation of object
  public function __toString()
  {
    return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }

  public function get_error_code()
  {
    return $this->error_code;
  }
}

