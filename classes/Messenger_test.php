<?php

require_once 'db.php';
require_once 'classes/Messenger.php';

$the_messenger = new Messenger( array('max_iterations' => 1) );

teldata_change_db();

$customer_id = 18960;

$r = $the_messenger->should_skip_renewal_message( $customer_id, 19 );
echo 'should skip renewal message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$r = $the_messenger->should_skip_renewal_message( $customer_id, 49 );
echo 'should skip renewal message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$customer_id = 31;

$r = $the_messenger->should_skip_promo_message( $customer_id , 'plan_expires_today' );
echo 'should skip promo message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$r = $the_messenger->should_skip_promo_message( $customer_id , 'test' );
echo 'should skip promo message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$customer_id = 18667;

$r = $the_messenger->should_skip_promo_message( $customer_id , 'plan_expires_today' );
echo 'should skip promo message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$r = $the_messenger->should_skip_promo_message( $customer_id , 'test' );
echo 'should skip promo message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$customer_id = 32;

$r = $the_messenger->should_skip_promo_message( $customer_id , 'plan_expires_today' );
echo 'should skip promo message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

$r = $the_messenger->should_skip_promo_message( $customer_id , 'test' );
echo 'should skip promo message: '; if ( $r ) { echo "Y\n"; } else { echo "N\n"; }

