<?php

/**
 * $planConfig = PlanConfig::Instance();
 * $planConfig->getLongNames(1);
 */

define('CONFIG_DIR', '');

class PlanConfig
{
  private $usingLocalConfig = false;
  private $feature_config = [];
  private $soc_config     = [];
  private $plan_config    = [];
  private $demo_lines  = [
    1 => [ 'A39'  ],
    2 => [ 'UVS45' ]
  ];

  /**
   * this returns instance of singleton
   * $planConfig = PlanConfig::Instance();
   * @return PlanConfig
   */
  public static function Instance()
  {
    static $inst = null;
    if ($inst === null)
      $inst = new PlanConfig();
    return $inst;
  }

  //
  private function __construct()
  {
    // defaulting to local storage for testing
    $this->setConfigFromLocalStorage();
    return;

    // for testing, should be replaced by setConfigFromCfengine
    if (getenv("UNIT_TESTING")) {
      $this->setConfigFromLocalStorage();
    } else {
      $this->setConfigFromCfengine();
    }
  }

  public function get_soc_config()
  {
    return $this->soc_config;
  }

  public function get_plan_config()
  {
    return $this->plan_config;
  }

  public function get_feature_config()
  {
    return $this->feature_config;
  }

  /**
   *
   */
  public function getAccSocDefinition($plan_soc_name)
  {
    $definition = ( ! $this->usingLocalConfig)
      ? trim(\Ultra\UltraConfig\find_config("amdocs/definition/plan/$plan_soc_name"))
      : trim($this->feature_config->{"amdocs/definition/plan/$plan_soc_name"});

    return explode(
      ' ',
      preg_replace('/\s+/', ' ', $definition),
      count(\Ultra\MvneConfig\getAccSocsDescriptorList())
    );
  }

  /**
   *
   */
  public function getAccSocsDefinitionsList()
  {
    if ( ! $this->usingLocalConfig)
      return explode(' ', \Ultra\UltraConfig\find_config('amdocs/definition/planlist'));

    return explode(' ', $this->feature_config->{'amdocs/definition/planlist'});
  }

  /**
   * this defines legacy constants
   * @return null
   */
  public function defineConstants()
  {
    // mint is left blank intentionally
    // MINT is in the long_name
    $brands = ['ULTRA','UNIVISION',''];

    foreach ($this->plan_config as $plan => $fields)
    {
      $long_name = $fields['long_name'];

      // this is a hack for these plans
      if (in_array($long_name, ['UVTWENTY','UVTHIRTY','UVTHIRTYFIVE','UVFORTYFIVE','UVFIFTY','UVFIFTYFIVE']))
        $long_name = str_replace('UV', '', $long_name);

      $brandName = isset($fields['brand_id']) ? "_{$brands[$fields['brand_id'] - 1]}_" : '_ULTRA_';
      if ($brandName == '__') $brandName = '_';

      // COSID_ULTRA_NINETEEN
      define("COSID{$brandName}{$long_name}", $plan);

      // COSID_ULTRA_NINETEEN_COST
      if (isset($fields['cost']))
        define("COSID{$brandName}{$long_name}_COST", $fields['cost']);

      // COSID_ULTRA_NINETEEN_NAME
      if (isset($fields['name']))
        define("COSID{$brandName}{$long_name}_NAME", $fields['name']);

      // COSID_MINT_ONE_MONTH_S_MONTHS
      if (isset($fields['months']))
        define("COSID{$brandName}{$long_name}_MONTHS", $fields['months']);
    }
  }

  /**
   * generates configuration members from local JSON files
   * soc_config.json
   * plan_config.json
   * @return null
   */
  public function setConfigFromLocalStorage()
  {
    $this->usingLocalConfig = true;

    $this->plan_config = [];
    $this->soc_config  = (object)[];

    $this->soc_config      = json_decode(file_get_contents(CONFIG_DIR . 'soc_config.json'));
    $this->feature_config  = json_decode(file_get_contents(CONFIG_DIR . 'feature_config.json'));

    $plan_config = json_decode(file_get_contents(CONFIG_DIR . 'plan_config.json'));
    $temp = array();

    foreach ($plan_config as $key => $val)
    {
      $key_e = explode('/', $key);
      // [ $temp[plan_name][field] = value , ... ]
      $temp[$key_e[2]][$key_e[3]] = $val;
    }

    foreach ($temp as $plan => $fields)
    {
      foreach ($fields as $field => $val)
        $this->plan_config[$fields['cos_id']][$field] = $val;
      $this->plan_config[$fields['cos_id']]['long_name'] = $plan;
    }
  }

  /**
   * generates configuration members from cfengine
   * @return null
   */
  public function setConfigFromCfengine()
  {
    global $credentials;

    $plan_config = [];

    $this->plan_config = [];
    $this->soc_config  = (object)[];

    $cfEngineEnv = (\Ultra\UltraConfig\isDevEnvironment() || \Ultra\UltraConfig\isQAEnvironment())
      ? 'ultra_develop_tel_api_accounts'
      : 'ultra_data_ultra_api';

    foreach ($credentials[$cfEngineEnv] as $key => $val)
    {
      if (strpos($key, 'amdocs/plans/Ultra') === 0)
        $this->soc_config->$key = $val;

      // plan_config will be parsed after
      if (strpos($key, 'plans/Ultra') === 0)
        $plan_config[$key] = $val;
    }

    foreach ($plan_config as $key => $val)
    {
      $key_e = explode('/', $key);
      // $temp[plan_name][field] = value
      $temp[$key_e[2]][$key_e[3]] = $val;
    }

    foreach ($temp as $plan => $fields)
    {
      foreach ($fields as $field => $val)
        $this->plan_config[$fields['cos_id']][$field] = $val;

      // long_name is not part of JSON configuration
      $this->plan_config[$fields['cos_id']]['long_name'] = $plan;
    }
  }

  /**
   * returns the orange offer_id map
   * @return [ offer_id => [ cos_id, months ] ]
   */
  public function getOrangePlanMap()
  {
    $data = array();
    foreach ($this->plan_config as $cos_id => $fields)
    {
      if ( ! isset($fields['offer_id']))
        continue;

      // "8|3,9|6,10|12"
      if ($fields['aka'] == 'M29')
      {
        $e = explode(',', $fields['offer_id']);
        foreach ($e as $f)
        {
          list ($offer_id, $months) = explode('|', $f);
          $data[$offer_id] = [$fields['cos_id'], $months];
        }
      }
      else
        $data[$fields['offer_id']] = [$fields['cos_id'], 1];
    }
    return $data;
  }

  /**
   * @param string $plan long_name
   * @return soc configuration for $plan
   */
  public function find_config($plan)
  {
    if ($plan == 'MULTI_TWENTY_NINE') $plan = 'TWENTY_NINE';
    return $this->soc_config->{"amdocs/plans/Ultra/$plan"};
  }

  public function getPlanConfigItem($cos_id, $key)
  {
    return $this->plan_config[$cos_id][$key];
  }

  /**
   * @param string $plan long_name
   * @return int
   */
  public function zeroUsedMinutes($plan)
  {
    $map = $this->cosIdPlanMap();
    return isset($this->plan_config[$map[$plan]]['zero_used_minutes']) ? $this->plan_config[$map[$plan]]['zero_used_minutes'] : 1000;
  }

  /**
   * @param string $plan long_name
   * @return plan configuration for $plan
   */
  public function getPlanConfig($plan)
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      if ($val['long_name'] == $plan)
        foreach ($val as $k => $v)
          $data[$k] = $v;
    return $data;
  }

  /**
   * returns plan configuration arrays keyed by long_name
   * @return [ long_name => [ plan conifguration array ] ]
   */
  public function ultraPlans()
  {
    $data = array();
    foreach ($this->plan_config as $k => $v)
      $data[$v['long_name']] = $v;
    return $data;
  }

  /**
   * returns external payments product configuration keyed by cos_id
   * @return [ cos_id => [ name => , cost => , subproduct_id => short_name ] ]
   */
  public function partnerFaceEpayAllowedCosIds()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[$key] = [
        'name'          => $val['name'],
        'cost'          => $val['cost'],
        'subproduct_id' => isset($val['aka']) ? $val['aka'] : ''
      ];
    return $data;
  }

  /**
   * returns array of plan description keyed by cos_id
   * @return [ cos_id => name ]
   */
  public function cosIdPlanDescription()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[$key] = $val['name'];
    return $data;
  }

  /**
   * return array of cos_ids keyed by short_name and long_name
   * @return [ long_name => cos_id , short_name => cos_id ]
   */
  public function cosIdPlanMap()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
    {
      $data[$val['long_name']] = $key;
      $data[$val['aka']]  = $key;
    }
    return $data;
  }

  /**
   * returns array of long_name keyed by short_name
   * @return [ short_name => long_name ]
   */
  public function planStringsMap()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
    {
      $data[$val['aka']] = $val['long_name'];
      $data[$val['long_name']] = $val['long_name'];
    }
    return $data;
  }

  /**
   * returns array of short_name
   * @return [ short_name ]
   */
  public function getUltraPlansShort()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[] = $val['aka'];
    return $data;
  }

  /**
   * returns array of long_name
   * @return [ long_name ]
   */
  public function getUltraPlansLongDefinitions()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[] = $val['long_name'];
    return $data;
  }

  /**
   * returns array of name keyed by cos_id
   * @return [ cos_id => name ]
   */
  public function getPlanNameMap()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[$key] = $val['name'];
    return $data;
  }

  /**
   * returns array of cost keyed by cos_id
   * @return [ cos_id => cost ]
   */
  public function getPlanCostMap()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[$key] = $val['cost'];
    return $data;
  }

  /**
   * returns array of MINT(3) months keyed by cos_id
   * @return [ cos_id => months ]
   */
  public function planMonthsMap()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
    {
      if (isset($val['brand_id']) && $val['brand_id'] == 3)
        $data[$key] = $val['months'];
      elseif (isset($val['months']))
        $data[$key] = $val['months'];
    }
    return $data;
  }

  /**
   * returns configuration map for rodeo transition generator
   * @param int $brand_id
   * @return [ long_name => [ cost / 100 , short_name ] ]
   */
  public function getRodeoMap($brand_id)
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      if (isset($val['brand_id']) && $val['brand_id'] == $brand_id)
        $data[$val['long_name']] = [$val['cost'] / 100, $val['aka']];
    return $data;
  }

  /**
   * returns plan group configuration for rodeo transition generator
   * @param int $brand_id
   * @return []
   */
  public function getPlanGroups($brand_id)
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
    {
      if (isset($val['brand_id']) && $val['brand_id'] == $brand_id)
      {
        if (isset($val['active']) && (int)$val['active'] == 1)
          $data['provision'][] = $val['aka'];
        $data['all'][] = $val['aka'];
      }
    }

    if (isset($this->demo_lines[$brand_id]))
      $data['dealer_demo'] = $this->demo_lines[$brand_id];

    return $data;
  }

  /**
   * returns array of active long_name by $brand_id
   * @param int brand_id
   * @return [ long_name ]
   */
  public function getLongNames($brand_id)
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      if (isset($val['brand_id']) && $val['brand_id'] == $brand_id)
        if (isset($val['active']) && (int)$val['active'] == 1)
          $data[] = $val['long_name'];
    return $data;
  }

  /**
   * returns array of active long_name
   * @return [ long_name ]
   */
  public function getActiveLongNames()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      if ((int)$val['active'] == 1)
        $data[] = $val['long_name'];
    return $data;
  }

  /**
   * returns array of active short_name
   * @return [ short_name ]
   */
  public function getActivePlansAka()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      if ((int)$val['active'] == 1)
        $data[] = $val['aka'];
    return $data;
  }

  /**
   * return array of short_name
   * @return [ short_name ]
   */
  public function getAllPlansAka()
  {
    $data = array();
    foreach ($this->plan_config as $key => $val)
      $data[] = $val['aka'];
    return $data;
  }

  // TODO
  // only for M29
  // cosid_constants.php monthly_plan_map
  public function monthlyPlanMap() {}
}