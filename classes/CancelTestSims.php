<?php

require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';

class CancelTestSims
{
  public $cancellationReason = 'Activation';
  public $planEffectiveLimit;
  public $accMiddleware;
  public $whiteListSims = array();

  public function __construct()
  {
    $nDays = \Ultra\UltraConfig\getMintTestSimTTL();
    $this->planEffectiveLimit = strtotime("-$nDays days", time());

    $this->accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

    $this->whiteListSims = \Ultra\UltraConfig\getMintTestSimWhiteList();
  }

  /**
   * main program
   */
  public function main()
  {
    $testSimRanges = \Ultra\UltraConfig\getMintTestSimRanges();

    // $testSimRanges       = [ ['8901260842116062762','8901260842116062763'] ];
    // $this->whiteListSims = [ ['8901260842116062762','8901260842116062762'] ];

    $simsToCancel = [];
    foreach ($testSimRanges as $range)
    {
      for ($iccid = $range[0]; $iccid <= $range[1]; $iccid++)
      {
        if ($this->isWhiteListed($iccid))
          continue;

        $dataBody = $this->querySubscriber($iccid);
        if ($dataBody && $this->shouldCancel($dataBody))
          $simsToCancel[] = $iccid;
      }
    }

    // print_r($simsToCancel);

    if (count($simsToCancel))
      $this->cancelSimsApi($simsToCancel);
  }

  /**
   * checks if ICCID is in white listed sim range
   *
   * @param  String
   * @return Boolean
   */
  public function isWhiteListed($iccid)
  {
    foreach ($this->whiteListSims as $range)
    {
      if ($iccid >= $range[0] && $iccid <= $range[1])
        return TRUE;
    }

    return FALSE;
  }

  /**
   * determine if test SIM should be cancelled
   *
   * @param  Array
   * @return Boolean
   */
  public function shouldCancel($dataBody)
  {
    $planEffectiveDate = strtotime($dataBody['PlanEffectiveDate']);
    return ($dataBody['SubscriberStatus'] != 'Inactive' && $planEffectiveDate < $this->planEffectiveLimit);
  }

  /**
   * passes iccids to internal__CancelCustomersWithinTestRange
   *
   * @param  Array
   * @return NULL
   */
  public function cancelSimsApi($iccids)
  {
    foreach ($iccids as &$iccid)
      $iccid = "$iccid:{$this->cancellationReason}";

    $params = [ 'customers' => implode(',', $iccids) ];

    list( $internal_domain , $internal_user , $internal_password ) = \Ultra\UltraConfig\ultra_api_internal_info();

    $url = "https://$internal_domain/pr/internal/2/ultra/api/internal__CancelCustomersWithinTestRange";

    $curlOptions = [
      CURLOPT_USERPWD  => "$internal_user:$internal_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC
    ];

    $result = curl_post($url, $params, $curlOptions, NULL, 120);
    \logit('reponse = ' . json_encode($result));

    return json_decode($result);
  }

  /**
   * run QuerySubscriber, return NULL or message data
   *
   * @param  String
   * @return Array
   */
  public function querySubscriber($iccid)
  {
    $dataBody = NULL;

    $querySubscriberResult = $this->accMiddleware->processControlCommand(
      [
        'command'    => 'QuerySubscriber',
        'parameters' => [
          'iccid' => $iccid
        ],
        'actionUUID' => getNewActionUUID('Cancel Runner ' . time())
      ]
    );

    // error, do not cancel
    if ( ! $querySubscriberResult->is_success())
    {
      $error = ( $querySubscriberResult->has_errors() )
        ? 'errors ' . json_encode($querySubscriberResult->get_errors())
        : 'generic error';
    
      \logError($error);
    }
    else
    {
      $dataBody = $this->accMiddleware->controlObject->extractDataBodyFromMessage(
        $querySubscriberResult->data_array['message']
      );
    }

    // timeout, do not cancel
    if ( $querySubscriberResult->is_timeout() )
      \logError('timeout');

    return $dataBody;
  }
}
