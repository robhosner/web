<?php

require_once 'classes/PHPUnitBase.php';
require_once 'db.php';
require_once 'classes/IntraBrand.php';

class IntraBrandTest extends PHPUnitBase
{
  public function test__isUVImportPortByMSISDN()
  {
    teldata_change_db();

    $result = IntraBrand::isUVImportPortByMSISDN('7146753662');
    print_r($result);
    $this->assertFalse($result);
  }

  public function test__isIntraBrandPortByMSISDNAndSIM()
  {
    teldata_change_db();

    $sim = get_htt_inventory_sim_from_iccid('8901260892107435683');
    $result = IntraBrand::isIntraBrandPortByMSISDNAndSIM('9173536004', $sim);
    $this->assertTrue($result);

    $sim = get_htt_inventory_sim_from_iccid('8901260842107734452');
    $result = IntraBrand::isIntraBrandPortByMSISDNAndSIM('9173536004', $sim);
    $this->assertFalse($result);
  }

  public function test__isIntraBrandPortByReferenceSource()
  {
    teldata_change_db();

    $customer_id = 20129;
    $result = IntraBrand::isIntraBrandPortByReferenceSource($customer_id);
    $this->assertTrue($result);

    $customer_id = 19493;
    $result = IntraBrand::isIntraBrandPortByReferenceSource($customer_id);
    $this->assertFalse($result);
  }
}