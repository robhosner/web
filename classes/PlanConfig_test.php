<?php

global $credentials;
require_once 'core.php';
require_once 'cosid_constants.php';
require_once 'db.php';
require_once 'Ultra/UltraConfig.php';
require_once 'Ultra/MvneConfig.php';

class PlanConfigTest extends PHPUnit_Framework_testCase
{
  protected $backupGlobals = FALSE;

  public function test_CompareConfigs()
  {
    return;

    $planConfig = \PlanConfig::Instance();
    $localPlanConfig = $planConfig->get_plan_config();
    $localSocConfig  = $planConfig->get_soc_config();

    $planConfig->setConfigFromCfengine();

    $remotePlanConfig = $planConfig->get_plan_config();
    $remoteSocConfig  = $planConfig->get_soc_config();

    foreach ($localPlanConfig as $key => $val)
    {
      if ( ! isset($remotePlanConfig[$key]))
        echo "$key does not exist in remote plan config" . "\n";
      else if ($remotePlanConfig[$key] != $val)
      {
        $val = json_encode($val);
        echo "$key $val does not match remote plan config" . "\n";
      }
    }

    foreach ($localSocConfig as $key => $val)
    {
      if ( ! isset($remoteSocConfig->$key))
        echo "$key does not exist in remote soc config" . "\n";
      else if ($remoteSocConfig->$key != $val)
      {
        $val = json_encode($val);
        echo "$key $val does not match remote soc config" . "\n";
      }
    }
  }

  public function test__getOrangePlanMap()
  {
    $planConfig = \PlanConfig::Instance();
    $orangePlanMap = $planConfig->getOrangePlanMap();

    print_r($orangePlanMap);

    $this->assertTrue(true);
  }

  public function test__find_config()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->find_config('NINETEEN'));

    $this->assertTrue(true);
  }

  public function test__zeroUsedMinutes()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->zeroUsedMinutes('NINETEEN'));

    $this->assertTrue(true);
  }

  public function test__getPlanConfig()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getPlanConfig('NINETEEN'));

    $this->assertTrue(true);
  }

  public function test__ultraPlans()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->ultraPlans());

    $this->assertTrue(true);
  }

  public function test__partnerFaceEpayAllowedCosIds()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->partnerFaceEpayAllowedCosIds());

    $this->assertTrue(true);
  }

  public function test__cosIdPlanDescription()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->cosIdPlanDescription());

    $this->assertTrue(true);
  }

  public function test__cosIdPlanMap()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->cosIdPlanMap());

    $this->assertTrue(true);
  }

  public function test__planStringsMap()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->planStringsMap());

    $this->assertTrue(true);
  }

  public function test__getUltraPlansShort()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getUltraPlansShort());

    $this->assertTrue(true);
  }

  public function test__getUltraPlansLongDefinitions()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getUltraPlansLongDefinitions());

    $this->assertTrue(true);
  }

  public function test__getPlanNameMap()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getPlanNameMap());

    $this->assertTrue(true);
  }

  public function test__getPlanCostMap()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getPlanCostMap());

    $this->assertTrue(true);
  }

  public function test__mintMonthsMap()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->planMonthsMap());

    $this->assertTrue(true);
  }

  public function test__getRodeoMap()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getRodeoMap(1));

    $this->assertTrue(true);
  }

  public function test__getPlanGroups()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getPlanGroups(1));

    $this->assertTrue(true);
  }

  public function test__getLongNames()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getLongNames(1));

    $this->assertTrue(true);
  }

  public function test__getActiveLongNames()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getActiveLongNames());

    $this->assertTrue(true);
  }

  public function test__getActivePlansAka()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getActivePlansAka());

    $this->assertTrue(true);
  }

  public function test__getAllPlansAka()
  {
    $planConfig = \PlanConfig::Instance();

    print_r($planConfig->getAllPlansAka());

    $this->assertTrue(true);
  }

  /**
   */
  public function test__Rodeo()
  {
  }
}


/*
$planConfig = \PlanConfig::Instance();

$rodeo = new Rodeo();
$rodeo->setMap($planConfig->getRodeoMap(2));
$rodeo->loadFile('Ultra/Lib/StateMachine/' . 'parsed_univision.json');
$rodeo->setPlanGroups($planConfig->getPlanGroups(2));
$rodeo->parseFile();
*/