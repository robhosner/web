<?php
namespace Ultra\Taxes;

use Ultra\Configuration\Configuration;
use Ultra\Payments\TaxCalculator\SureTax;

class TaxesAndFeesCalculator
{
  private $brand;
  private $cosId;
  private $product;
  private $amount;
  private $zipCode;
  private $retryCount = 0;
  private $activationZipCode;

  /**
   * @var SureTax
   */
  private $sureTax;

  /**
   * @var TransactionTypeCodeConfig
   */
  private $transactionTypeCodeConfig;

  /**
   * @var Configuration
   */
  private $configuration;

  public function __construct(
    SureTax $sureTax,
    TransactionTypeCodeConfig $transactionTypeCodeConfig,
    Configuration $configuration
  )
  {
    $this->sureTax = $sureTax;
    $this->transactionTypeCodeConfig = $transactionTypeCodeConfig->toArray();
    $this->configuration = $configuration;
  }

  public function calculateTaxesAndFees($brand, $cosId, $product, $amount, $zipCode, $activationZipCode = null)
  {
    if (empty($brand) || empty($cosId) || empty($product) || empty($amount) || empty($zipCode)) {
      $error = 'missing one or more parameters $brand, $cosId, $product, $amount, $zipCode';
      \logError($error);
      return make_error_Result($error);
    }

    $this->cosId = $cosId;
    $this->product = $product;
    $this->amount = $amount;
    $this->zipCode = $zipCode;
    $this->activationZipCode = !empty($activationZipCode) ?: $zipCode;

    if ($brand == 1 || $brand == 'ULTRA') {
      $this->brand = 'ULTRA';
      return $this->getUltraTaxesAndFees();
    } else if ($brand == 3 || $brand == 'MINT') {
      $this->brand = 'MINT';
      return $this->getMintTaxesAndFees();
    } else {
      $error = "Invalid brand $brand";
      \logError($error);
      return make_error_Result($error);
    }
  }

  private function getUltraTaxesAndFees()
  {
    if (!empty($this->transactionTypeCodeConfig[$this->brand][$this->product])) {
      return $this->calculateTaxesFees($this->transactionTypeCodeConfig[$this->brand][$this->product]);
    } else {
      return $this->noTransactionTypeCodeError();
    }
  }

  private function getMintTaxesAndFees()
  {
    if (!empty($this->transactionTypeCodeConfig[$this->brand][$this->product])) {
      return $this->calculateTaxesFees($this->transactionTypeCodeConfig[$this->brand][$this->product]);
    } else {
      return $this->noTransactionTypeCodeError();
    }
  }

  private function noTransactionTypeCodeError()
  {
    $error = "No TransactionTypeCode found for brand: $this->brand, product: $this->product.";
    \logError($error);
    return make_error_Result($error);
  }

  private function calculateTaxesFees($transactionTypeCode, $transactionType = null)
  {
    // default result array
    $result_data = [
      'zip_code' => $this->zipCode,
      'mts_tax' => 0,
      'sales_tax' => 0,
      'sales_tax_rule' => 'SureTax API error',
      'sales_tax_percentage' => 0,
      'recovery_fee' => 0,
      'recovery_fee_rule' => 0,
      'recovery_fee_basis' => 0
    ];

    $recoveryFeeMultiplier = 1;
    if (strpos($transactionType, 'FLEX_RECHARGE_') == 0) {
      $meta = explode('_', $transactionType);
      if (isset($meta[2]) && $meta[2] > 0) {
        $recoveryFeeMultiplier = $meta[2];
      }
    }

    $fee = $this->getRecoveryFee($this->amount, $this->cosId);

    // get recovery fee and et our result data array
    $result_data['recovery_fee']       = $fee['recovery_fee'] * $recoveryFeeMultiplier;
    $result_data['recovery_fee_basis'] = $fee['recovery_basis'];
    $result_data['recovery_fee_rule']  = $fee['recovery_rule'];

    $salesTaxRule = "SureTax Rule $transactionTypeCode";
    $this->amount += $result_data['recovery_fee'];

    // validate input information, call SureTax
    $salesTax = $this->callSureTax($this->amount, $this->zipCode, $transactionTypeCode);

    // gracefully handle SureTax error
    if (count($errors = $salesTax->get_errors()))
      return make_ok_Result($result_data, NULL, $errors[0]);

    // if suretax returned 0 tax, exit with default result
    if ( $salesTax->data_array['TotalTax'] == "0.00" )
      return make_ok_Result($result_data, NULL, 'SureTax API returned 0 taxes');

    $result_data['zip_code'] = $salesTax->data_array['zip_code'];

    // this is our sales tax parsed by our parsing rule
    $parsedSalesTax = $this->parseTaxResultJan2017($salesTax);
    $parsedSalesTax['sales_tax_rule'] = $salesTaxRule;

    $result_data['mts_tax']              = $parsedSalesTax['mts_tax'];
    $result_data['sales_tax']            = $parsedSalesTax['sales_tax'];
    $result_data['sales_tax_percentage'] = $parsedSalesTax['sales_tax_percentage'];
    $result_data['sales_tax_rule']       = $parsedSalesTax['sales_tax_rule'];

    \logit(sprintf("calculateTaxesFees mts_tax      = %s",$result_data['mts_tax']));
    \logit(sprintf("calculateTaxesFees sales_tax    = %s",$result_data['sales_tax']));
    \logit(sprintf("calculateTaxesFees recovery_fee = %s",$result_data['recovery_fee'])); # in cents

    // everything was OK
    return make_ok_Result($result_data);
  }

  /**
   * suretax parsing for January 2017
   * in this rule, we are calculating each GroupList[i] with no TaxTypeCode filer
   * @param $salesTax
   * @return array
   */
  private function parseTaxResultJan2017($salesTax)
  {
    $result = [
      'mts_tax' => 0,
      'sales_tax' => 0,
      'sales_tax_percentage' => 0,
    ];

    $isCaliforniaZip = ($this->zipCode > 90001 && $this->zipCode < 96162) ? true : false;

    foreach ($salesTax->data_array['GroupList'] as $groupList) {
      foreach ($groupList->TaxList as $taxEntry) {
        // if is MTS tax
        if ($isCaliforniaZip) {
          $result['mts_tax'] += ($taxEntry->TaxAmount * 100);
        } else {
          $result['sales_tax'] += ($taxEntry->TaxAmount * 100);
        }
      }
    }

    $result['mts_tax']   = floor($result['mts_tax']);
    $result['sales_tax'] = floor($result['sales_tax']);
    $result['sales_tax_percentage'] = ($result['sales_tax'] + $result['mts_tax']) / $this->amount * 100;

    return $result;
  }

  /**
   * calculate recovery fee based on $amount
   * @param  int $amount
   * @param $cosId
   * @return array {'recovery_fee': int, 'recovery_basis': string, 'recovery_rule', string}
   */
  public function getRecoveryFee($amount, $cosId)
  {
    $fee = [
      'recovery_fee' => null,
      'recovery_basis' => null,
      'recovery_rule' => 'CC Recovery Rule v1'
    ];

    if (!$amount) {
      $fee['recovery_fee']   = 0;
      $fee['recovery_basis'] = '0';
    } elseif ($amount > 1000) {
      $fee['recovery_fee']   = 100;
      $fee['recovery_basis'] = 'Charge > $10';
    } else {
      $fee['recovery_fee']   = 50;
      $fee['recovery_basis'] = 'Charge <= $10';
    }

    if ($this->configuration->getBrandIDFromCOSID($cosId) == 1
      && $this->configuration->planMonthsByCosID($cosId) != null
      && $amount >= 5700
    ) {
      // Ultra multi month plans have $3 recovery fee, the assumption is this is for purchasing a plan and not a bolton
      // have to change $amount if the cheapest multi month price changes :(
      $fee['recovery_fee']   = 300;
      $fee['recovery_basis'] = 'Multi month plan';
    }

    if ($this->configuration->isMintPlan($cosId)) {
      $totalMonths = $this->configuration->mintMonthsByCosId($cosId);

      // 1 month: $1, 3 month: $1.75, 6 month: $2.50, 12 month: $4
      if ($totalMonths == 1) {
        $fee['recovery_fee']   = 100;
        $fee['recovery_basis'] = '1 Month Plan';
      } elseif ($totalMonths == 3) {
        $fee['recovery_fee']   = 175;
        $fee['recovery_basis'] = '3 Month Plan';
      } elseif ($totalMonths == 6) {
        $fee['recovery_fee']   = 250;
        $fee['recovery_basis'] = '6 Month Plan';
      } elseif ($totalMonths == 12) {
        $fee['recovery_fee']   = 400;
        $fee['recovery_basis'] = '12 Month Plan';
      } else {
        $fee['recovery_fee']   = 0;
        $fee['recovery_basis'] = '0';
      }

      if ($this->product == 'ADD_BALANCE') {
        $fee['recovery_fee'] = 0;
      }
    }

    return $fee;
  }

  public function getSalesTax($amount, $billingZipCode, $transactionTypeCode)
  {
    return $this->callSureTax($amount, $billingZipCode, $transactionTypeCode);
  }

  /**
   * calls SureTax API, handles API result errors
   * @param $amount
   * @param $billingZipCode
   * @param $transactionTypeCode
   * @param null $returnFileCode
   * @return object
   */
  private function callSureTax($amount, $billingZipCode, $transactionTypeCode, $returnFileCode = null)
  {
    $sureTaxResult = $this->sureTax->performCall([
      'amount'                => ($amount / 100),
      'zip_code'              => trim($billingZipCode),
      'transaction_type_code' => $transactionTypeCode,
      'return_file_code'      => $returnFileCode
    ]);

    try {
      if ($sureTaxResult->is_failure()) {
        dlog('', "ESCALATION IMMEDIATE SureTax API error : call failed : %s", $sureTaxResult->get_errors());
        throw new \Exception( 'SureTax API error : call failed' );
      }

      if (!isset($sureTaxResult->data_array['TotalTax'])) {
        dlog('', "ESCALATION IMMEDIATE SureTax API error : call did not return meaningful data");
        throw new \Exception( 'SureTax API error : call did not return meaningful data' );
      }

      // there is an error message
      if (isset($sureTaxResult->data_array['ItemMessages'])
        && is_array($sureTaxResult->data_array['ItemMessages'])
        && count($sureTaxResult->data_array['ItemMessages'])
        && is_object($sureTaxResult->data_array['ItemMessages'][0])
        && property_exists($sureTaxResult->data_array['ItemMessages'][0], 'Message')
      ) {
        if (
          $this->retryCount == 0
          && property_exists($sureTaxResult->data_array['ItemMessages'][0], 'ResponseCode')
          && $sureTaxResult->data_array['ItemMessages'][0]->ResponseCode == 9152
        ) {
          $this->retryCount++;
          dlog('', "Retry $this->retryCount " . __FUNCTION__ . " with customer's activation zip code $this->activationZipCode");
          return $this->callSureTax($amount, $this->activationZipCode, $transactionTypeCode, $returnFileCode);
        } else {
          dlog('', "ESCALATION IMMEDIATE SureTax API error : message = %s", $sureTaxResult->data_array['ItemMessages'][0]->Message);
          throw new \Exception('SureTax API error : message = ' . $sureTaxResult->data_array['ItemMessages'][0]->Message);
        }
      }

      // there is no grouplist to parse
      if (!isset($sureTaxResult->data_array['GroupList'])
        || !is_array($sureTaxResult->data_array['GroupList'])
        || !count($sureTaxResult->data_array['GroupList'])
        || !property_exists($sureTaxResult->data_array['GroupList'][0], 'TaxList')
        || !is_array($sureTaxResult->data_array['GroupList'][0]->TaxList)
        || !count($sureTaxResult->data_array['GroupList'][0]->TaxList)
      ) {
        dlog('', "ESCALATION IMMEDIATE SureTax API error : API response invalid - %s", $sureTaxResult->data_array);
        throw new \Exception('SureTax API error : API response invalid');
      }
    } catch(\Exception $e) {
      return make_error_Result($e->getMessage());
    }

    return $sureTaxResult;
  }
}
