<?php
namespace Ultra\Customers\Repositories\Mssql;

use DateTime;
use Ultra\Customers\Customer;

require_once __DIR__ . '/../../Interfaces/CustomerRepository.php';
require_once 'Ultra/Customers/Customer.php';

/**
 * Class CustomerRepository
 * @package Ultra\Customers\Repositories\Mssql
 */
class CustomerRepository implements \Ultra\Customers\Interfaces\CustomerRepository
{
  /**
   * Get customer by customer id.
   *
   * @param $customerId
   * @param array $selectFields
   * @param bool $transform
   * @return object
   */
  public function getCustomerById($customerId, array $selectFields, $transform = false)
  {
    $selectFields[] = 'CUSTOMER_ID';
    $selectFields = array_unique($selectFields);
    $customer = get_ultra_customer_from_customer_id($customerId, $selectFields);

    if ($transform)
    {
      return new Customer((array) $customer);
    }

    return $customer;
  }

  /**
   * Get customer from msisdn
   *
   * @param $msisdn
   * @param array $selectFields
   * @param bool $transform
   * @return object
   */
  public function getCustomerFromMsisdn($msisdn, array $selectFields = [], $transform = false)
  {
    if (!empty($selectFields)) {
      $selectFields = implode(", ", $selectFields);
    } else {
      $selectFields = '';
    }

    $customer = get_customer_from_msisdn($msisdn, $selectFields);
    return $transform ? new Customer((array) $customer) : $customer;
  }

  /**
   * Get a list of customers from msisdns
   *
   * @param array $msisdns
   * @param array $selectFields
   * @return \Array|bool
   */
  public function getCustomersFromMsisdns(array $msisdns, array $selectFields)
  {
    if ($key = array_search('customer_id', $selectFields)) {
      $selectFields[$key] = 'c.customer_id';
    }

    $selectFields[0] = 'c.customer_id';

    if ($key = array_search('cos_id', $selectFields)) {
      $selectFields[$key] = 'a.cos_id';
    }

    $msisdns = array_map(function($msisdns) {
      return "'" . mssql_escape($msisdns) . "'";
    }, $msisdns);
    
    $selectFields = implode(", ", $selectFields);

    $query = sprintf("
      SELECT $selectFields
      FROM dbo.htt_customers_overlay_ultra AS u WITH (NOLOCK)
      JOIN dbo.customers  AS c  WITH (NOLOCK) ON u.customer_id = c.customer_id
      JOIN dbo.accounts   AS a  WITH (NOLOCK) ON c.customer_id = a.customer_id
      JOIN dbo.parent_cos AS pc WITH (NOLOCK) ON a.cos_id = pc.cos_id
      WHERE u.current_mobile_number IN (%s)
      ", implode(",", $msisdns)
    );
    
    $customers = mssql_fetch_all_objects(logged_mssql_query($query));

    return count($customers) ? $customers : false;
  }

  /**
   * Get customer from iccid
   * 
   * @param $iccid
   * @param array $selectFields
   * @return object
   */
  public function getCustomerFromIccid($iccid, array $selectFields)
  {
    return get_customer_from_iccid($iccid, $selectFields);
  }

  /**
   * Get a list of customers from iccids
   *
   * @param array $iccids
   * @param array $selectFields
   * @return \Array|bool
   */
  public function getCustomersFromIccids(array $iccids, array $selectFields)
  {
    if ($key = array_search('customer_id', $selectFields)) {
      $selectFields[$key] = 'c.customer_id';
    }

    $selectFields[0] = 'c.customer_id';

    if ($key = array_search('cos_id', $selectFields)) {
      $selectFields[$key] = 'a.cos_id';
    }

    $iccids = array_map(function($iccids) {
      return "'" . mssql_escape($iccids) . "'";
    }, $iccids);

    $iccids = implode(",", $iccids);

    $selectFields = implode(", ", $selectFields);

    $query = sprintf("
      SELECT $selectFields
      FROM  htt_customers_overlay_ultra u WITH (NOLOCK)
      INNER JOIN customers  c  WITH (NOLOCK) ON c.CUSTOMER_ID = u.CUSTOMER_ID
      INNER JOIN accounts   a  WITH (NOLOCK) ON a.CUSTOMER_ID = u.CUSTOMER_ID
      INNER JOIN parent_cos pc WITH (NOLOCK) on pc.COS_ID     = a.COS_ID
      AND   ( u.ACTIVATION_ICCID      IN (%s)
           OR u.current_iccid         IN (%s)
           OR u.ACTIVATION_ICCID_FULL IN (%s)
           OR u.CURRENT_ICCID_FULL    IN (%s) )
      ",  $iccids,
          $iccids,
          $iccids,
          $iccids
    );

    $customers = mssql_fetch_all_objects(logged_mssql_query($query));

    return count($customers) ? $customers : false;
  }

  /**
   * Update customers table with update fields by customer id
   *
   * @param  int   $customer_id
   * @param  array $fieldsToUpdate key/value
   * @return bool  was sql successful?
   */
  public function updateCustomerByCustomerId($customer_id, array $fieldsToUpdate)
  {
    $fieldsToUpdate['customer_id'] = $customer_id;
    $sql = customers_update_query($fieldsToUpdate);

    return is_mssql_successful(logged_mssql_query($sql));
  }

  /**
   * Cancel customer
   * 
   * @param $customer
   * @param $customerId
   * @return array
   */
  public function cancelCustomer($customer, $customerId)
  {
    return change_customer_state($customer, ['customer_id' => $customerId], 'Cancelled', 'STANDBY');
  }

  /**
   * Ensure to cancel customer.
   * 
   * @param $msisdn
   * @param $iccid
   * @param null $transitionUuid
   * @param null $actionUuid
   * @param null $customerId
   * @return object
   */
  public function ensureCancelCustomer($msisdn, $iccid, $transitionUuid = null, $actionUuid = null, $customerId = null)
  {
    return mvneEnsureCancel($msisdn, $iccid, $transitionUuid, $actionUuid, $customerId);

  }

  /**
   * Add cancellation reason
   * 
   * @param array $fields
   * @return object
   */
  public function addCancellationReason(array $fields)
  {
    return htt_cancellation_reasons_add($fields);
  }

  /**
   * Update auto-recharge
   *
   * @param $customerId
   * @param $enrollAutoRecharge
   */
  public function updateAutoRecharge($customerId, $enrollAutoRecharge)
  {
    \Ultra\Lib\DB\Customer\updateAutoRecharge($customerId, $enrollAutoRecharge);
  }

  /**
   * @param $customerId
   * @param array $selectFields
   * @return Customer
   */
  public function getCombinedCustomerByCustomerId($customerId, array $selectFields = ['*'])
  {
    $selectFields[] = 'u.CUSTOMER_ID';
    $selectFields = array_unique($selectFields);
    return new Customer((array) \get_customer_from_customer_id($customerId, $selectFields));
  }

  /**
   * @param $customerId
   * @param array $selectFields
   * @return Customer
   */
  public function getCustomerFromCustomersTable($customerId, array $selectFields = [])
  {
    $selectFields[] = 'CUSTOMER_ID';
    $selectFields = array_unique($selectFields);
    return new Customer((array) \customers_get_customer_by_customer_id($customerId, $selectFields));
  }

  /**
   * @param $zsession
   * @param string $method
   * @return Customer
   */
  public function getCustomerFromSession($zsession, $method = 'get_customer_from_customer')
  {
    return new Customer((array) \getCustomerFromSession($zsession, $method));
  }

  /**
   * @param $username
   * @return Customer
   */
  public function getCustomerFromUsername($username)
  {
    return new Customer((array) find_first(make_find_ultra_customer_query_anycosid(-1, $username)));
  }

  public function addCustomerContactPhone($customerId, $contactPhone)
  {
    $sql = sprintf("INSERT INTO HTT_CUSTOMER_CONTACT_PHONE (CUSTOMER_ID, CONTACT_PHONE) VALUES (%d, %d)", $customerId, $contactPhone);
    return is_mssql_successful(logged_mssql_query($sql));
  }

  public function getFlexMessagingCustomers(DateTime $renewDate)
  {
    $date = $renewDate->format('Y-m-d');
    return mssql_fetch_all_objects(logged_mssql_query("EXEC dbo.GetFlexMessagingCustomers @Plan_Expires = '$date'"));
  }

  public function getCustomerTimeZone($customerId)
  {
    return \get_customer_time_zone($customerId);
  }
}
