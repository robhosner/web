<?php
namespace Ultra\Plans;

use Ultra\Hydrator;

/**
 * Class Plan
 * @package Ultra\Plans
 */
final class Plan
{
  use Hydrator;

  /**
   * @var
   */
  public $planId;

  /**
   * @var
   */
  public $boltOnTypes;

  /**
   * @var
   */
  public $boltOns;

  /**
   * @var
   */
  public $brandId;

  /**
   * @var
   */
  public $cosId;

  /**
   * @var
   */
  public $cost;

  /**
   * @var
   */
  public $cycleData;

  /**
   * @var
   */
  public $intlMinutes;

  /**
   * @var
   */
  public $months;

  /**
   * @var
   */
  public $name;

  /**
   * @var
   */
  public $offerId;

  /**
   * @var
   */
  public $talkMinutes;

  /**
   * @var
   */
  public $unlimitedIntlMinutes;

  /**
   * @var
   */
  public $wholesale;

  /**
   * @var
   */
  public $zeroUsedMinutes;

  /**
   * @var
   */
  public $dataRecharge;

  /**
   * @var
   */
  public $monthly;

  /**
   * @var
   */
  public $unlimited;

  /**
   * @var
   */
  public $dataLte;

  /**
   * @var
   */
  public $data3G;

  /**
   * @var
   */
  public $roamingCredit;

  /**
   * @var
   */
  public $unlimitedLTE;

  /**
   * @var
   */
  public $unlimited3G;

  /**
   * @var
   */
  public $bonusMonths;

  /**
   * @var
   */
  public $active;

  /**
   * Plan constructor.
   * @param array $data
   */
  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
}
