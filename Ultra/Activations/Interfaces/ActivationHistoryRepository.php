<?php
namespace Ultra\Activations\Interfaces;

/**
 * Interface ActivationHistoryRepository
 * @package Ultra\Activations\Interfaces
 */
interface ActivationHistoryRepository
{
  /**
   * @param $customer_id
   */
  public function getHistory($customer_id);

  /**
   * @param $customer_id
   * @param $provider_name
   * @param $load_amount in dollars
   */
  public function logFunding($customer_id, $provider_name, $load_amount);
}