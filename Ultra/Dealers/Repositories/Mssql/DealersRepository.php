<?php
namespace Ultra\Dealers\Repositories\Mssql;

require_once __DIR__ . '/../../Interfaces/DealersRepository.php';

/**
 * Class DealersRepository
 * @package Ultra\Dealers\Repositories\Mssql
 */
class DealersRepository implements \Ultra\Dealers\Interfaces\DealersRepository
{
  /**
   * get dealer record tblDealerSite by dealer code (Dealercd)
   *
   * @param  string $code
   * @return array dealer data
   */
  public function getDealerFromCode($code)
  {
    $code = trim($code);
    if (empty($code))
    {
      dlog('', 'ERROR: empty parameter code');
      return NULL;
    }

    $db  = \Ultra\UltraConfig\celluphoneDb();
    $sql = sprintf("SELECT TOP 1 * FROM $db..tblDealerSite WITH (NOLOCK) WHERE Dealercd = '%s'", $code);
    $dealer = mssql_fetch_all_objects(logged_mssql_query($sql));

    return (count($dealer)) ? $dealer[0] : NULL;
  }

  /**
   * Get dealer locations.
   * 
   * @param array $parameters
   * @return mixed
   */
  public function getLocations(array $parameters)
  {
    return mssql_fetch_all_objects(logged_mssql_query(htt_ultra_locations_select_query($parameters)));
  }

  /**
   * Get all relevant dealer_ids from the parent-child relationship.
   *
   * @param $dealer
   * @return mixed
   */
  public function getParentDealerChildren($dealer)
  {
    return get_parent_dealer_children($dealer);
  }
  
  /**
   * Get all relevant dealer_ids from the parent-child relationship.
   *
   * @param $master
   * @return mixed
   */
  public function getDealersFromMaster($master)
  {
    return get_dealers_from_master($master);
  }
}
