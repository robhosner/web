<?php
namespace Ultra\Utilities;

class SessionUtilities
{
  /**
   * @var
   */
  private $redis;

  /**
   * @var
   */
  private $session;

  public function checkApiAbuseByIdentifier($api, $id, $max, $increment = true, $reset = false, $skipInDev = true)
  {
    return \checkApiAbuseByIdentifier($api, $id, $max, $this->getRedis(), $increment, $reset, $skipInDev);
  }

  public function resetCounter($api, $id)
  {
    return \checkApiAbuseByIdentifier($api, $id, null, $this->getRedis(), false, true);
  }

  /**
   * @return \Ultra\Lib\Util\Redis
   */
  private function getRedis()
  {
    return !empty($this->redis) ? $this->redis : new \Ultra\Lib\Util\Redis();
  }

  /**
   * @param $msisdn
   * @param $token
   * @return \Session
   */
  public function getSession($msisdn = null, $token = null)
  {
    return !empty($this->session) ? $this->session : new \Session($msisdn, $token);
  }

  public function checkIfUserIsBlockedFromApi($api, $username, $max)
  {
    return \checkIfUserIsBlockedFromApi($api, $username, $max, $this->getRedis());
  }

  public function isIpAddressInArray($ip, $list)
  {
    return isIpAddressInArray($ip, $list);
  }

  public function getClientIp()
  {
    return \Session::getClientIp();
  }

  public function encryptToken($msisdn)
  {
    return \Session::encryptToken($msisdn);
  }
}
