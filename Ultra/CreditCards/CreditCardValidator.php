<?php
namespace Ultra\CreditCards;

use Ultra\Configuration\Configuration;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\DB\Merchants\MerchantInterface;
use Ultra\Lib\DB\Merchants\Tokenizer;
use Ultra\Utilities\Validator;

/**
 * Class CreditCardValidator
 * @package Ultra\CreditCards
 */
class CreditCardValidator
{
  /**
   * @var MerchantInterface
   */
  private $merchant;

  /**
   * @var Validator
   */
  private $validator;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var Tokenizer
   */
  private $tokenizer;

  /**
   * @var \Result
   */
  private $result;

  /**
   * CreditCard constructor.
   * @param MerchantInterface $merchant
   * @param Validator $validator
   * @param Configuration $configuration
   * @param Tokenizer $tokenizer
   */
  public function __construct(
    MerchantInterface $merchant,
    Validator $validator,
    Configuration $configuration,
    Tokenizer $tokenizer
  )
  {
    $this->merchant = $merchant;
    $this->validator = $validator;
    $this->configuration = $configuration;
    $this->tokenizer = $tokenizer;
  }

  /**
   * @param array $creditCardInfo
   * @return CreditCard
   * @throws InvalidObjectCreationException
   */
  public function validate(array $creditCardInfo)
  {
    $creditCard = new CreditCard($creditCardInfo);

    if (!empty($creditCard->cvv)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorCVV('cvv', $creditCard->cvv);
    }

    if (!empty($creditCard->cc_name)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorName('cc_name', $creditCard->cc_name);
    }

    if (!empty($creditCard->cc_postal_code)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorZipcode('cc_postal_code', $creditCard->cc_postal_code, 'US');
    }

    list($errors[], $errorCodes[]) = $this->validator->validatorDateformat('expires_date', $creditCard->expires_date, 'mmyy');
    list($errors[], $errorCodes[]) = $this->validator->validatorMinstrlen('token', $creditCard->token, 32);
    list($errors[], $errorCodes[]) = $this->validator->validatorMinstrlen('bin', $creditCard->bin, 6);
    list($errors[], $errorCodes[]) = $this->validator->validatorMaxstrlen('bin', $creditCard->bin, 6);
    list($errors[], $errorCodes[]) = $this->validator->validatorMaxstrlen('last_four', $creditCard->last_four, 4);
    list($errors[], $errorCodes[]) = $this->validator->validatorMaxstrlen('last_four', $creditCard->last_four, 4);

    if (!empty($creditCard->cc_address1)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorAddress('cc_address1', $creditCard->cc_address1);
    }

    if (!empty($creditCard->cc_address2)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorAddress('cc_address2', $creditCard->cc_address1);
    }

    if (!empty($creditCard->cc_city)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorCity('cc_city', $creditCard->cc_city);
    }

    if (!empty($creditCard->cc_country)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorCountry('cc_country', $creditCard->cc_country);
    }

    if (!empty($creditCard->cc_state_region)) {
      list($errors[], $errorCodes[]) = $this->validator->validatorStringformat('cc_state_region', $creditCard->cc_state_region, 'alphanumeric_punctuation');
    }

    $errors = array_values(array_filter($errors));
    $errorCodes = array_values(array_filter($errorCodes));

    if (!empty($errors[0])) {
      throw new InvalidObjectCreationException($errors[0], $errorCodes[0]);
    }

    $address = $creditCard->cc_address1;

    if (!empty($creditCard->cc_address2)) {
      $address .= ' ' . $creditCard->cc_address2;
    }

    $creditCardInfo = (array) $creditCard;
    $creditCardInfo['address'] = $address;
    $creditCardInfo['zipcode'] = $creditCard->cc_postal_code;

    if (!$this->configuration->ccTransactionsEnabled()) {
      throw new InvalidObjectCreationException('Credit Card transactions have been disabled temporarily', 'CC0004');
    } else {
      if ($this->tokenizer->alwaysFailAccount($creditCardInfo)) {
        throw new InvalidObjectCreationException('Always Fail Account match', 'CC0001');
      } elseif ($this->tokenizer->alwaysSucceedAccount($creditCardInfo)) {

      } else {
        $this->result = $this->merchant->verify($creditCardInfo);

        if ($this->result->is_failure()) {
          $errors = $this->result->get_errors();

          if (!empty($errors['user_errors'])) {
            $error = $errors['user_errors']['EN'];
            $errors = $errors['user_errors'];
          } else {
            $error = implode(',', $errors);
            $errors = [$error];
          }

          $error = 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed - (' . $error . ')';
          throw new InvalidObjectCreationException($error, 'CC0001', null, $errors);
        } else {
          $creditCard->cvv_validation = $this->result->data_array['cvv_validation'];
          $creditCard->avs_validation = $this->result->data_array['avs_validation'];

          // apply any overrides
          if (!empty($this->result->data_array['overrides']) && is_array($this->result->data_array['overrides'])) {
            foreach ($this->result->data_array['overrides'] as $key=>$val) {
              $creditCard->{$key} = $val;
            }
          }
        }
      }
    }

    return $creditCard;
  }
}
