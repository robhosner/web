<?php
namespace Ultra\CreditCards;

use Ultra\Customers\Customer;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Hydrator;

/**
 * Class CreditCard
 * @package Ultra\CreditCards
 */
class CreditCard
{
  use Hydrator;

  /**
   * @var
   */
  public $customer_id;

  /**
   * @var
   */
  public $brand_id;

  /**
   * @var
   */
  public $cvv;

  /**
   * @var
   */
  public $expires_date;

  /**
   * @var
   */
  public $cc_address1;

  /**
   * @var
   */
  public $cc_address2;

  /**
   * @var
   */
  public $cc_postal_code;

  /**
   * @var
   */
  public $cc_name;

  /**
   * @var
   */
  public $cc_city;

  /**
   * @var
   */
  public $cc_country;

  /**
   * @var
   */
  public $cc_state_region;

  /**
   * @var
   */
  public $token;

  /**
   * @var
   */
  public $bin;

  /**
   * @var
   */
  public $last_four;

  /**
   * @var
   */
  public $cvv_validation;

  /**
   * @var
   */
  public $avs_validation;

  /**
   * @var
   */
  public $processor;

  /**
   * CreditCard constructor.
   * @param array $creditCardInfo
   * @throws InvalidObjectCreationException
   */
  public function __construct(array $creditCardInfo)
  {
    $this->hydrate($creditCardInfo, true);

    if (empty($this->customer_id) || empty($this->token) || empty($this->bin) || empty($this->last_four) || empty($this->expires_date)) {
      throw new InvalidObjectCreationException(__CLASS__ . ' Missing required parameters.', 'MP0001');
    }
  }

  /**
   * @param array $creditCardInfo
   * @param CreditCardValidator $validator
   * @return CreditCard
   */
  public static function verify(array $creditCardInfo, CreditCardValidator $validator)
  {
    return $validator->validate($creditCardInfo);
  }

  /**
   * @param Customer $customer
   * @param $chargeAmount
   * @param $description
   * @param $reason
   * @param $session
   * @return \Result
   */
  public function initAddBalanceFromTokenizedCreditCard(Customer $customer, $chargeAmount, $detail, $description, $reason, $session, $disableFraudCheck=false, $isCommissionable = 1)
  {
    return \func_add_balance_by_tokenized_cc([
      'customer'      => $customer,
      'charge_amount' => $chargeAmount,
      'detail'        => $detail,
      'description'   => $description,
      'reason'        => $reason,
      'session'       => $session
    ], $disableFraudCheck, $isCommissionable);
  }

  public function disableCreditCard()
  {
    return \Ultra\Lib\DB\Setter\Customer\disable_cc_info(['customer_id' => $this->customer_id]);
  }
}
