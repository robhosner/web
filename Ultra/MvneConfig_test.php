<?php

require_once 'db.php';
require_once 'Ultra/MvneConfig.php';

$x = Ultra\MvneConfig\getMintDataSocFeatureID(1);
echo "$x\n";

$x = Ultra\MvneConfig\getMintDataSocFeatureID(1024);
echo "$x\n";

$x = Ultra\MvneConfig\getMintDataSocFeatureID(3);
echo "$x\n";

$x = Ultra\MvneConfig\getMintDataSocFeatureID(3072);
echo "$x\n";

exit;

$customer      = new stdClass();
$customer->BRAND_ID = 1;
$data_recharge = [
  'MB' => 1024
];

$x = Ultra\MvneConfig\getDataAddOnForMakeitsoUpgradePlan( $customer , $data_recharge );

echo "$x\n";

$customer->BRAND_ID = 3;
$data_recharge = [
  'MB' => 1024
];

$x = Ultra\MvneConfig\getDataAddOnForMakeitsoUpgradePlan( $customer , $data_recharge );

echo "$x\n";

exit;

$x = Ultra\MvneConfig\isMintRetailPlanSOC( 0 );

print_r($x);

exit;

test_getAccSocsUltraPlanConfig();

$retailPlans = Ultra\MvneConfig\getRetailPlans();

print_r( $retailPlans );

foreach( $retailPlans as $retailPlan )
  echo (Ultra\MvneConfig\isRetailPlanSOC( $retailPlan ) ? 'true' : 'false' )."\n";

echo (Ultra\MvneConfig\isRetailPlanSOC( 'dummy' ) ? 'true' : 'false' )."\n";

$wholesalePlans = Ultra\MvneConfig\getWholesalePlans();

print_r( $wholesalePlans );

foreach( $wholesalePlans as $wholesalePlan )
  echo (Ultra\MvneConfig\isWholesalePlanSOC( $wholesalePlan ) ? 'true' : 'false' )."\n";

echo (Ultra\MvneConfig\isWholesalePlanSOC( 'dummy' ) ? 'true' : 'false' )."\n";

$networkSocs = Ultra\MvneConfig\getNetworkSocs();

print_r( $networkSocs );

foreach( $networkSocs as $networkSoc )
  echo (Ultra\MvneConfig\isNetworkSoc( $networkSoc ) ? 'true' : 'false' )."\n";

echo (Ultra\MvneConfig\isNetworkSoc( 'dummy' ) ? 'true' : 'false' )."\n";

$x = Ultra\MvneConfig\getAccSocsDefinitions();

print_r($x);

$x = Ultra\MvneConfig\getAccSocDefinition('w_primary');

print_r($x);

$x = Ultra\MvneConfig\getAccSocDefinition('w_secondary');

print_r($x);

$x = Ultra\MvneConfig\getACCWholesaleValue('W-PRIMARY');

echo "$x\n";

$x = Ultra\MvneConfig\getACCWholesaleValue('W-SECONDARY');

echo "$x\n";

$x = Ultra\MvneConfig\getACCWholesaleValue('W-UV-PRIMARY');

echo "$x\n";

$x = Ultra\MvneConfig\getACCWholesaleValue('W-UV-SECONDARY');

echo "$x\n";

$x = Ultra\MvneConfig\getACCWholesaleValue('W-MINT');

echo "$x\n";

exit;

$x = Ultra\MvneConfig\getACCWholesaleValueByPlanName('NINETEEN');

echo "$x\n";

$x = Ultra\MvneConfig\getACCWholesaleValueByPlanName('FIFTY_NINE');

echo "$x\n";

$plans = [
'NINETEEN',
'TWENTY_FOUR',
'TWENTY_NINE',
'THIRTY_FOUR',
'THIRTY_NINE',
'FORTY_FOUR',
'FORTY_NINE',
'FIFTY_NINE',
'UVTWENTY',
'UVTHIRTY',
'UVTHIRTYFIVE',
'UVFORTYFIVE',
'UVFIFTY',
'UVFIFTYFIVE'
];

foreach( $plans as $plan )
{
  $x = Ultra\MvneConfig\getAccSocsPlanConfig( $plan );

  echo "$plan:\n";
  print_r( $x );
}



function test_getAccSocsUltraPlanConfig()
{
  $config = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(PLAN_UV30);
  assert($config['b_voice'] == 8250);
  $config = \Ultra\MvneConfig\getAccSocsUltraPlanConfig(PLAN_UV30, NULL, BRAND_OPTION_SUBPLAN_A);
  assert($config['b_voice'] == 1000);
}
