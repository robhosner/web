<?php

namespace Ultra\Lib\BoltOn;

require_once 'classes/Flex.php';
require_once 'Ultra/Lib/Services/FamilyAPI.php';

/**
 * addBoltOnsRecurring
 *
 * Add Recurring Bolt Ons if balance is sufficient
 *
 * @return array
 */
function addBoltOnsRecurring( $customer_id , $balance, $plan )
{
  // get Bolt Ons info

  $bolt_ons = get_bolt_ons_info_from_customer_options( $customer_id );

  dlog('',"customer_id = %s , bolt_ons = %s",$customer_id,$bolt_ons);

  if ( ! $bolt_ons || ! is_array( $bolt_ons ) || ! count( $bolt_ons ) )
  {
    dlog('',"Nothing to do");

    return array( 'success' => TRUE, 'errors' => array() );
  }

  // compute cost - cumulative

  $total_cost = 0;

  foreach( $bolt_ons as $bolt_on )
    $total_cost += $bolt_on['cost'];

  // check if the customer has enough funds

  if ( $balance < $total_cost )
  {
    dlog('',"Not enough money");

    $and = '';
    $caption_bolt_ons = '';

    // record failures in ULTRA.BOLTON_TRACKER
    foreach( $bolt_ons as $bolt_on_info )
    {
      dlog('',"Recording ".$bolt_on_info['description']." failure");

      // record failure in ULTRA.BOLTON_TRACKER
      $result = trackBoltOn( $customer_id , 'MONTHLY' , 'ISF' , $bolt_on_info['sku'] , $bolt_on_info['product'] , 'MONTHLY' );

      // non-fatal error
      if ( $result->is_failure() )
        dlog('',"Warning : INSERT INTO ULTRA.BOLTON_TRACKER failed");

      $caption_bolt_ons .= $and . $bolt_on_info['ui_product_name'];

      $and = ' and ';
    }

    dlog('',"caption_bolt_ons = $caption_bolt_ons");

    // send message to the customer: not enough funds for recurring bolt ons
    \funcSendExemptCustomerSMSRecurringBoltOnsNoFunds(
      array(
        'customer_id' => $customer_id,
        'customer'    => get_customer_from_customer_id($customer_id),
        'bolt_ons'    => $caption_bolt_ons
      )
    );

    return array( 'success' => TRUE, 'errors' => array() );
  }

  foreach( $bolt_ons as $bolt_on_info )
  {
    dlog('',"Adding ".$bolt_on_info['description']);

    $add_bolt_on_function = 'Ultra\Lib\BoltOn\add'.$bolt_on_info['product'].'BoltOnRecurring';

    dlog('',"invoking $add_bolt_on_function");

    $error      = NULL;
    $error_code = '';

    if ( function_exists( $add_bolt_on_function ) )
    {
      list( $error , $error_code ) = $add_bolt_on_function( $customer_id , $bolt_on_info , 'MONTHLY' , __FUNCTION__, $plan );

      if ( $error || $error_code )
      {
        dlog('',"$error_code - $error");

        dlog('',"Recording ".$bolt_on_info['description']." failure");

        // record failure in ULTRA.BOLTON_TRACKER
        $result = trackBoltOn( $customer_id , 'MONTHLY' , 'ERROR' , $bolt_on_info['sku'] , $bolt_on_info['product'] , 'MONTHLY' );

        // non-fatal error
        if ( $result->is_failure() )
          dlog('',"Warning : INSERT INTO ULTRA.BOLTON_TRACKER failed");

        if ($bolt_on_info['product'] == 'SHAREDDATA' || $bolt_on_info['product'] == 'SHAREDILD') {
          enqueue_immediate_sms($customer_id, 'flex_boltons_fail_recurring', []);
        }
      }
    }
    else
      dlog('',"Product ".$bolt_on_info['product']." does not exist");
  }

  return array( 'success' => TRUE, 'errors' => array() );
}

/**
 * addIDDCABoltOnRecurring
 *
 * Add INTL minutes as consequence of a plan renewal - IDDCA = International Direct Dial Call Anywhere Credit
 * Increase packaged_balance1 ( 600 min = 1 cent )
 *
 * @return array
 */
function addIDDCABoltOnRecurring( $customer_id , $bolt_on_info , $source , $detail, $plan )
{
  $customer = get_customer_from_customer_id( $customer_id );

  if ( ! $customer )
    return array('ERR_API_INVALID_ARGUMENTS: customer does not exist', '------');

  // execute the following DB operations in a DB transaction
  if ( ! start_mssql_transaction() )
    return array('ERR_API_INTERNAL: DB write error (0)', 'DB0001');

  $result = func_add_ild_minutes(
    array(
      'customer'       => $customer,
      'amount'         => $bolt_on_info['cost'],
      'reason'         => $detail,                        // HTT_BILLING_HISTORY.DETAIL
      'description'    => $bolt_on_info['description'],   // HTT_BILLING_HISTORY.DESCRIPTION
      'source'         => $source,                        // HTT_BILLING_HISTORY.SOURCE
      'reference'      => create_guid( __FUNCTION__ ),
      'balance_change' => $bolt_on_info['cost'],
      'bonus'          => .25
    )
  );

  dlog('',"func_add_ild_minutes result = %s",$result);

  if ( ! $result['success'] )
  {
    rollback_mssql_transaction();

    // send message to the customer: Recurring Bolt On UpIntl failed
    \funcSendExemptCustomerSMSRecurringBoltOnsError(
      array(
        'customer_id' => $customer_id,
        'customer'    => $customer,
        'bolt_ons'    => 'UpIntl'
      )
    );

    return array('ERR_API_INTERNAL: DB write error (1)', 'DB0001');
  }

  if ( ! commit_mssql_transaction() )
  {
    // send message to the customer: Recurring Bolt On UpIntl failed
    \funcSendExemptCustomerSMSRecurringBoltOnsError(
      array(
        'customer_id' => $customer_id,
        'customer'    => $customer,
        'bolt_ons'    => 'UpIntl'
      )
    );

    return array('ERR_API_INTERNAL: DB write error (2)', 'DB0001');
  }

  // send message to the customer: Recurring Bolt On UpIntl succeeded
  \funcSendExemptCustomerSMSIntlBoltOnSuccess(
    array(
      'customer_id' => $customer_id,
      'customer'    => $customer,
      'template'    => 'recurring'
    )
  );

  // add a new row into ULTRA.BOLTON_TRACKER
  $result = trackBoltOn( $customer_id , $source , 'DONE' , $bolt_on_info['sku'] , $bolt_on_info['product'] , 'MONTHLY' );

  // non-fatal error
  if ( $result->is_failure() )
    dlog('',"Warning : INSERT INTO ULTRA.BOLTON_TRACKER failed");

  // everything went well
  return NULL;
}

/**
 * addBoltOnImmediate
 *
 * Immediated Bolt On request
 * We do not record failures in ULTRA.BOLTON_TRACKER
 *
 * @return array
 */
function addBoltOnImmediate( $customer_id , $bolt_on_info , $source , $detail, $customer=NULL )
{
  if ( ! $customer_id || ! is_numeric($customer_id) )
    return array('ERR_API_INTERNAL: invalid customer_id', '------');

  if ( ! $bolt_on_info || ! is_array($bolt_on_info) )
    return array('ERR_API_INTERNAL: invalid parameters', '------');

  $add_bolt_on_function = 'Ultra\Lib\BoltOn\add'.$bolt_on_info['product'].'BoltOnImmediate';

  dlog('',"invoking $add_bolt_on_function");

  if ( ! function_exists( $add_bolt_on_function ) )
    return array('ERR_API_INTERNAL: product '.$bolt_on_info['product'].' does not exist', '------');

  return $add_bolt_on_function( $customer_id , $bolt_on_info , $source , $detail, $customer );
}

/**
 * addIDDCABoltOnImmediate
 *
 * Add INTL minutes immediately - IDDCA = International Direct Dial Call Anywhere Credit
 * Increase packaged_balance1 ( 600 min = 1 cent )
 *
 * @return array
 */
function addIDDCABoltOnImmediate( $customer_id , $bolt_on_info , $source , $detail, $customer=NULL )
{
  if ( ! $customer )
    $customer = get_customer_from_customer_id( $customer_id );

  if ( ! $customer )
    return array('ERR_API_INVALID_ARGUMENTS: customer does not exist', '------');

  // check if the customer has enough money in his BALANCE
  if ( $customer->BALANCE < $bolt_on_info['cost'] )
    return array('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

  // execute the following DB operations in a DB transaction
  if ( ! start_mssql_transaction() )
    return array('ERR_API_INTERNAL: DB write error (0)', 'DB0001');

  $result = func_add_ild_minutes(
    array(
      'customer'       => $customer,
      'amount'         => $bolt_on_info['cost'],
      'reason'         => $detail,                        // HTT_BILLING_HISTORY.DETAIL
      'description'    => $bolt_on_info['description'],   // HTT_BILLING_HISTORY.DESCRIPTION
      'source'         => $source,                        // HTT_BILLING_HISTORY.SOURCE
      'reference'      => create_guid( __FUNCTION__ ),
      'balance_change' => $bolt_on_info['cost'],
      'bonus'          => .25,
      'commissionable' => 1,
    )
  );

  dlog('',"func_add_ild_minutes result = %s",$result);

  if ( ! $result['success'] )
  {
    rollback_mssql_transaction();

    return array('ERR_API_INTERNAL: DB write error (1)', 'DB0001');
  }

  if ( ! commit_mssql_transaction() )
    return array('ERR_API_INTERNAL: DB write error (2)', 'DB0001');

  // add a new row into ULTRA.BOLTON_TRACKER
  $result = trackBoltOn( $customer_id , $source , 'DONE' , $bolt_on_info['sku'] , $bolt_on_info['product'] , 'IMMEDIATE' );

  // non-fatal error
  if ( $result->is_failure() )
    dlog('',"Warning : INSERT INTO ULTRA.BOLTON_TRACKER failed");

  // send SMS message to notify user UPINTL Bolt On was successful
  \funcSendExemptCustomerSMSIntlBoltOnSuccess(
    array(
      'customer_id' => $customer_id,
      'customer'    => $customer,
      'template'    => 'immediate'
    )
  );

  // success!
  return array(NULL,NULL);
}

/**
 * addSHAREDDATABoltOnImmediate
 *
 * Adds a new transition for an immediate SHARED DATA Bolt On request
 *
 * @return array
 */
function addSHAREDDATABoltOnImmediate( $customer_id , $bolt_on_info , $source , $detail, $customer )
{
  dlog('',"customer_id = $customer_id");

  // the will create a BAN if one does not exist
  // this will be required for the UpdatePlanAndFeatures call
  // if it's not successful, we can avoid creating the transition
  // that will create the MISO that will create the command invocation
  $familyApi = new \Ultra\Lib\Services\FamilyAPI();
  $familyResult = $familyApi->applyFamilyBoltOnsByCustomerID($customer_id, [$bolt_on_info['id']], 1);
  if ( ! $familyResult->is_success())
    return ['Error pulling Family ID for customer', 'SM0001'];

  // check for cost when sending SMS
  $infoArr = explode('_', $bolt_on_info['id']);
  $withSMS = $infoArr[2] > 0;

  $result = ($source == 'ORDER')
    ? addDATABoltOnOrder( $customer_id , $bolt_on_info , $source , $detail, $customer , 'immediate' , null , $withSMS )
    : addDATABoltOn( $customer_id , $bolt_on_info , $source , $detail, $customer , 'immediate' , null, $withSMS );
  if (!empty($result[0])) {
    return $result;
  }

  // add any existing family members to BAN
  $parts = explode('|', $bolt_on_info['upgrade_plan_soc']);
  $flex = (new \Ultra\Container\AppContainer())->make(\Ultra\Plans\Flex::class);
  $misoResult = $flex->queueFamilyAddToBanMISOs($customer_id, $parts[1]);
  if (!$misoResult->is_success()) {
    \dlog('', 'ESCALATION FLEX Failed adding family to BAN where customer_id = ' . $customer_id);
  }

  return $result;
}

/**
 * addSHAREDDATABoltOnRecurring
 *
 * Adds a new transition for an Recurring SHARED DATA Bolt On request
 *
 * @return array
 */
function addSHAREDDATABoltOnRecurring( $customer_id , $bolt_on_info , $source , $detail, $plan )
{
  dlog('',"customer_id = $customer_id");

  if ( ! $customer)
    $customer = get_account_from_customer_id($customer_id, ['BALANCE', 'CUSTOMER_ID']);

  // ensure subscriber has enough money in his BALANCE
  if ($customer->BALANCE < $bolt_on_info['cost'])
    return array('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

  // the will create a BAN if one does not exist
  // this will be required for the UpdatePlanAndFeatures call
  // if it's not successful, we can avoid creating the transition
  // that will create the MISO that will create the command invocation
  $familyApi = new \Ultra\Lib\Services\FamilyAPI();
  $familyResult = $familyApi->applyFamilyBoltOnsByCustomerID($customer_id, [$bolt_on_info['id']], 1);
  if ( ! $familyResult->is_success())
    return ['Error pulling Family ID for customer', 'SM0001'];

  // check for cost when sending SMS
  $infoArr = explode('_', $bolt_on_info['id']);
  $withSMS = $infoArr[2] > 0;

  $result = addDATABoltOn( $customer_id , $bolt_on_info , $source , $detail, $customer , 'recurring' , null , $withSMS );
  if (!empty($result[0])) {
    return $result;
  }

  // add any existing family members to BAN
  $parts = explode('|', $bolt_on_info['upgrade_plan_soc']);
  $flex = (new \Ultra\Container\AppContainer())->make(\Ultra\Plans\Flex::class);
  $misoResult = $flex->queueFamilyAddToBanMISOs($customer_id, $parts[1]);
  if (!$misoResult->is_success()) {
    \dlog('', 'ESCALATION FLEX Failed adding family to BAN where customer_id = ' . $customer_id);
  }

  return $result;
}

/**
 * addSHAREDILDBoltOnImmediate
 *
 * Adds a new transition for an immediate SHARED ILD Bolt On request
 *
 * @return array
 */
function addSHAREDILDBoltOnImmediate( $customer_id , $bolt_on_info , $source , $detail, $customer )
{
  dlog('',"customer_id = $customer_id");

  $familyApi = new \Ultra\Lib\Services\FamilyAPI();
  $result = $familyApi->applyFamilyBoltOnsByCustomerID($customer_id, [$bolt_on_info['id']], 1);
  if ( ! $result->is_success())
    return ['Error adding Family ID for customer', 'SM0001'];

  \funcSendExemptCustomerSMSIntlBoltOnSuccess(
    array(
      'customer_id' => $customer_id,
      'customer'    => $customer,
      'template'    => 'immediate',
      'boltOn'      => $bolt_on_info
     )
  );

  // record bolt on
  $result = trackBoltOn($customer_id, $source, 'DONE', $bolt_on_info['sku'], $bolt_on_info['product'], 'immediate');
  if ($result->is_failure())
    dlog('', 'WARNING: INSERT INTO ULTRA.BOLTON_TRACKER failed');

  return [ null, null ];
}

/**
 * addSHAREDILDBoltOnRecurring
 *
 * Adds a new transition for an Recurring SHARED ILD Bolt On request
 *
 * @return array
 */
function addSHAREDILDBoltOnRecurring( $customer_id , $bolt_on_info , $source , $detail, $plan )
{
  dlog('',"customer_id = $customer_id");

  if ( ! $customer)
    $customer = get_account_from_customer_id($customer_id, ['BALANCE', 'CUSTOMER_ID']);

  // ensure subscriber has enough money in his BALANCE
  if ($customer->BALANCE < $bolt_on_info['cost'])
    return array('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

  $familyApi = new \Ultra\Lib\Services\FamilyAPI();
  $result = $familyApi->applyFamilyBoltOnsByCustomerID($customer_id, [$bolt_on_info['id']], 1);
  if ( ! $result->is_success())
    return ['Error adding Family ID for customer', 'SM0001'];

  // debit the wallet accordingly
  $result = func_spend_from_balance(
    array(
     'customer_id'      => $customer->CUSTOMER_ID,
     'amount'           => ($bolt_on_info['cost']),
     'detail'           => 'addSHAREDILDBoltOnRecurring',
     'reason'           => 'SHAREDILD Purchase',
     'reference'        => 'SHAREDILD_PAYMENT',
     'source'           => 'SPEND',
     'commissionable'   => 1,
     'reference_source' => create_guid('PHPAPI')));

  if (count($result['errors']))
    return array("ERR_API_INTERNAL: {$result['errors']}", 'VO0001');

  \funcSendExemptCustomerSMSIntlBoltOnSuccess(
    array(
      'customer_id' => $customer_id,
      'customer'    => $customer,
      'template'    => 'recurring',
      'boltOn'      => $bolt_on_info
    )
  );

  // record bolt on
  $result = trackBoltOn($customer_id, $source, 'DONE', $bolt_on_info['sku'], $bolt_on_info['product'], 'MONTHLY');
  if ($result->is_failure())
    dlog('', 'WARNING: INSERT INTO ULTRA.BOLTON_TRACKER failed');

  return [ null, null ];
}

/**
 * addDATABoltOnImmediate
 *
 * Adds a new transition for an immediate DATA Bolt On request
 *
 * @return array
 */
function addDATABoltOnImmediate( $customer_id , $bolt_on_info , $source , $detail, $customer )
{
  dlog('',"customer_id = $customer_id");

  return addDATABoltOn( $customer_id , $bolt_on_info , $source , $detail, $customer , 'immediate' );
}

/**
 * addMDATABoltOnImmediate
 *
 * Adds a new transition for an immediate MDATA Bolt On request
 *
 * @return array
 */
function addMDATABoltOnImmediate( $customer_id , $bolt_on_info , $source , $detail, $customer )
{
  dlog('',"customer_id = $customer_id");

  return addDATABoltOn( $customer_id , $bolt_on_info , $source , $detail, $customer , 'immediate' );
}

/**
 * addDATABoltOnRecurring
 *
 * Adds a new transition for a recurring DATA Bolt On addition
 *
 * @return array
 */
function addDATABoltOnRecurring( $customer_id , $bolt_on_info , $source , $detail, $plan )
{
  dlog('',"customer_id = $customer_id");

  return addDATABoltOn( $customer_id , $bolt_on_info , $source , $detail, NULL , 'recurring', $plan );
}

/**
 * addDATABoltOn
 *
 * $type specifies 'recurring' or 'immediate'
 *
 * @return array
 */
function addDATABoltOn( $customer_id , $bolt_on_info , $source , $detail, $customer , $type, $plan = NULL , $withSMS = true)
{
  $errorMessage = NULL;
  $errorCode = NULL;

  try
  {
    // initialize the context for the sequence of actions we will generate
    $context            = array('customer_id' => $customer_id );
    $action_seq         = 0;
    $action_transaction = NULL; // no TRANSITION_UUID yet

    // add action to validate balance
    $result_status = append_make_funcall_action(
      'assert_balance',
      $context,
      $action_transaction,
      $action_seq,
      ( $bolt_on_info['cost'] * 100 ),
      NULL,
      NULL,
      NULL,
      __FUNCTION__
    );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (1)', 'SM0001')));

    // enqueue to the TRANSITION_UUID created with the previous append_make_funcall_action
    $context['transition_id'] = $result_status['transitions'][0];

    // cumulative MISOs are delayed by 10 minutes, so we should be safe
    $action_seq++;
    $result_status =
      ( $type == 'recurring' )
      ?
        append_make_funcall_action(
          'mvneAddRecurringUpData',
          $context,
          $action_transaction,
          $action_seq,
          $bolt_on_info['upgrade_plan_soc'], // example: 'A-DATA-BLK|500'
          $bolt_on_info['sku'],
          $plan
        )
      :
        append_make_funcall_action(
          'mvneMakeitsoUpgradePlan',
          $context,
          $action_transaction,
          $action_seq,
          $bolt_on_info['upgrade_plan_soc'] // example: 'A-DATA-BLK|500'
        );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (2)', 'SM0001')));

    // debit the wallet; include name of data recharge in the HTT_BILLING_LOG entry
    $action_seq++;
    $result_status = append_make_funcall_action(
      'spend_from_balance',
      $context,
      $action_transaction,
      $action_seq,
      $bolt_on_info['cost'],                // amount in dollars
      $detail,                              // detail
      $bolt_on_info['description'],         // description
      create_guid( __FUNCTION__ ),          // reference
      'SPEND',                               // source
      $type
    );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (3)', 'SM0001')));

    if ($withSMS) {
      // send SMS to customer
      $action_seq++;
      $result_status = append_make_funcall_action(
        'send_sms_bolt_on_data_'.$type,
        $context,
        $action_transaction,
        $action_seq,
        $bolt_on_info
      );
    }
  
    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (4)', 'SM0001')));

    // track Data Socs to htt_customers_overlay_ultra
    $action_seq++;
    $result_status = append_make_funcall_action(
      'record_customer_soc',
      $context,
      $action_transaction,
      $action_seq,
      $bolt_on_info['description']
    );
  
    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (5)', 'SM0001')));

    // track DATA Bolt On in DB table ULTRA.BOLTON_TRACKER
    $action_seq++;
    $result_status = append_make_funcall_action(
      'record_'.$type.'_bolt_on',
      $context,
      $action_transaction,
      $action_seq,
      $source,                 // ULTRA.BOLTON_TRACKER.SOURCE
      $bolt_on_info['sku'],    // ULTRA.BOLTON_TRACKER.SKU
      // $bolt_on_info['ui_product_name'].'('.$bolt_on_info['cost'].')', // TODO: MVNO-2766 pending exact value to store from MVNO-2766
      $bolt_on_info['product'] // ULTRA.BOLTON_TRACKER.PRODUCT
    );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (6)', 'SM0001')));
  }

  catch(\Exception $e)
  {
    list($errorMessage, $errorCode) = json_decode($e->getMessage());
    if ( ! empty($context['transition_id']))
      abort_transition($context['transition_id']);
  }

  if ( ! empty($context['transition_id']))
    unreserve_transition_uuid_by_pid($context['transition_id']);
  return array($errorMessage, $errorCode);
}

/**
 * addDATABoltOnNoBalance
 *
 * $type specifies 'recurring' or 'immediate'
 *
 * @return array
 */
function addDATABoltOnOrder( $customer_id , $bolt_on_info , $source , $detail, $customer , $type, $plan = NULL , $withSMS = true )
{
  $errorMessage = NULL;
  $errorCode = NULL;

  try
  {
    // initialize the context for the sequence of actions we will generate
    $context            = array('customer_id' => $customer_id );
    $action_seq         = 0;
    $action_transaction = NULL; // no TRANSITION_UUID yet

    // cumulative MISOs are delayed by 10 minutes, so we should be safe
    $result_status = append_make_funcall_action(
      'mvneMakeitsoUpgradePlan',
      $context,
      $action_transaction,
      $action_seq,
      $bolt_on_info['upgrade_plan_soc'], // example: 'A-DATA-BLK|500'
      NULL,
      NULL,
      NULL,
      __FUNCTION__
    );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (2)', 'SM0001')));

    // enqueue to the TRANSITION_UUID created with the previous append_make_funcall_action
    $context['transition_id'] = $result_status['transitions'][0];

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (3)', 'SM0001')));

    if ($withSMS) {
      // send SMS to customer
      $action_seq++;
      $result_status = append_make_funcall_action(
          'send_sms_bolt_on_data_'.$type,
          $context,
          $action_transaction,
          $action_seq,
          $bolt_on_info
      );
    }

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (4)', 'SM0001')));

    // track Data Socs to htt_customers_overlay_ultra
    $action_seq++;
    $result_status = append_make_funcall_action(
      'record_customer_soc',
      $context,
      $action_transaction,
      $action_seq,
      $bolt_on_info['description']
    );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (5)', 'SM0001')));

    // track DATA Bolt On in DB table ULTRA.BOLTON_TRACKER
    $action_seq++;
    $result_status = append_make_funcall_action(
      'record_'.$type.'_bolt_on',
      $context,
      $action_transaction,
      $action_seq,
      $source,                 // ULTRA.BOLTON_TRACKER.SOURCE
      $bolt_on_info['sku'],    // ULTRA.BOLTON_TRACKER.SKU
      // $bolt_on_info['ui_product_name'].'('.$bolt_on_info['cost'].')', // TODO: MVNO-2766 pending exact value to store from MVNO-2766
      $bolt_on_info['product'] // ULTRA.BOLTON_TRACKER.PRODUCT
    );

    if ( ! $result_status['success'] )
      throw new \Exception(json_encode(array('ERR_API_INTERNAL: unexpected error (6)', 'SM0001')));
  }

  catch(\Exception $e)
  {
    list($errorMessage, $errorCode) = json_decode($e->getMessage());
    if ( ! empty($context['transition_id']))
      abort_transition($context['transition_id']);
  }

  if ( ! empty($context['transition_id']))
    unreserve_transition_uuid_by_pid($context['transition_id']);
  return array($errorMessage, $errorCode);
}

/**
 * addROAMBoltOnImmediate
 * add roaming credit
 */
function addROAMBoltOnImmediate($customer_id, $bolt_on_info, $source, $detail, $customer)
{
  dlog('', '%s, %s, %s, %s', $customer_id, $bolt_on_info, $source, $detail);

  // ensure we have a valid subscruber
  if ( ! $customer)
    if ( ! $customer = get_customer_from_customer_id($customer_id))
      return array('ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031');

  // ensure subsriber is active
  if ($customer->plan_state !== STATE_ACTIVE)
    return array('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

  // ensure subscriber has enough money in his BALANCE
  if ($source != 'ORDER' && $customer->BALANCE < $bolt_on_info['cost'])
    return array('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

  // attempt to set voice SOC
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  $result = $mwControl->mwMakeitsoUpgradePlan(
    array(
      'actionUUID'         => getNewActionUUID(__FUNCTION__ . time()),
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'customer_id'        => $customer->CUSTOMER_ID,
      'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
      'ultra_plan'         => get_plan_from_cos_id($customer->COS_ID),
      'preferred_language' => $customer->preferred_language,
      'option'             => $bolt_on_info['upgrade_plan_soc']));
  teldata_change_db();
  if ($result->is_failure())
    return array('ERR_API_INTERNAL: MW error', 'MW0001');

  // debit the wallet accordingly
  if ($source != 'ORDER') {
    $result = func_spend_from_balance(
      array(
        'customer_id'      => $customer->CUSTOMER_ID,
        'amount'           => ($bolt_on_info['cost']),
        'detail'           => 'addROAMBoltOnImmediate',
        'reason'           => 'ROAM Purchase',
        'reference'        => 'ROAM_PAYMENT',
        'source'           => 'SPEND',
        'commissionable'   => 1,
        'reference_source' => create_guid('PHPAPI')));
    if (count($result['errors']))
      return array("ERR_API_INTERNAL: {$result['errors']}", 'VO0001');
  }

  // record bolt on
  $result = trackBoltOn($customer_id, $source, 'DONE', $bolt_on_info['sku'], $bolt_on_info['product'], 'IMMEDIATE');
  if ($result->is_failure())
    dlog('', 'WARNING: INSERT INTO ULTRA.BOLTON_TRACKER failed');

/*
  // TODO add roaming confirmation SMS
  // send confirmation via SMS
  funcSendExemptCustomerSMSVoiceRecharge(array(
    'customer'       => $customer,
    'cost_amount'    => $bolt_on_info['cost'],
    'wallet_balance' => $customer->BALANCE - $bolt_on_info['cost'],
    'minutes'        => $bolt_on_info['get_value']));
*/

  // success
  return array(NULL, NULL);
}

/**
 * addVoiceBoltOnImmediate
 * add nationwide (domestic) minutes
 * @see DATAQ-113
 */
function addVOICEBoltOnImmediate($customer_id, $bolt_on_info, $source, $detail, $customer)
{
  dlog('', '%s, %s, %s, %s', $customer_id, $bolt_on_info, $source, $detail);

  // ensure we have a valid subscruber
  if ( ! $customer)
    if ( ! $customer = get_customer_from_customer_id($customer_id))
      return array('ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031');

  // ensure subsriber is active
  if ($customer->plan_state !== STATE_ACTIVE)
    return array('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

  // ensure subscriber has enough money in his BALANCE
  if ($customer->BALANCE < $bolt_on_info['cost'])
    return array('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

  // attempt to set voice SOC
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  $result = $mwControl->mwMakeitsoUpgradePlan(
    array(
      'actionUUID'         => getNewActionUUID(__FUNCTION__ . time()),
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'customer_id'        => $customer->CUSTOMER_ID,
      'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
      'ultra_plan'         => get_plan_from_cos_id($customer->COS_ID),
      'preferred_language' => $customer->preferred_language,
      'option'             => $bolt_on_info['upgrade_plan_soc']));
  teldata_change_db();
  if ($result->is_failure())
    return array('ERR_API_INTERNAL: MW error', 'MW0001');

  // debit the wallet accordingly
  $result = func_spend_from_balance(
    array(
     'customer_id'      => $customer->CUSTOMER_ID,
     'amount'           => ($bolt_on_info['cost']),
     'detail'           => 'addVOICEBoltOnImmediate',
     'reason'           => 'VOICE Purchase',
     'reference'        => 'VOICE_PAYMENT',
     'source'           => 'SPEND',
     'commissionable'   => 1,
     'reference_source' => create_guid('PHPAPI')));
  if (count($result['errors']))
    return array("ERR_API_INTERNAL: {$result['errors']}", 'VO0001');

  // record bolt on
  $result = trackBoltOn($customer_id, $source, 'DONE', $bolt_on_info['sku'], $bolt_on_info['product'], 'IMMEDIATE');
  if ($result->is_failure())
    dlog('', 'WARNING: INSERT INTO ULTRA.BOLTON_TRACKER failed');

  // send confirmation via SMS
  funcSendExemptCustomerSMSVoiceRecharge(array(
    'customer'       => $customer,
    'cost_amount'    => $bolt_on_info['cost'],
    'wallet_balance' => $customer->BALANCE - $bolt_on_info['cost'],
    'minutes'        => $bolt_on_info['get_value']));

  // success
  return array(NULL, NULL);
}

/**
 * addGLOBEBoltOnImmediate
 * add UpGlobe bolt on
 */
function addGLOBEBoltOnImmediate($customer_id, $bolt_on_info, $source, $detail, $customer, $type = 'IMMEDIATE')
{
  // get subscriber info
  if (!is_object($customer)
    || !property_exists($customer, 'current_mobile_number')
    || !property_exists($customer, 'BALANCE')
  ) {
    $customer = get_customer_from_customer_id($customer_id, ['BALANCE', 'current_mobile_number']);
    if ( !$customer )
      return ['ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031'];
  }

  // validate info and check conditions
  if (empty($customer->current_mobile_number) || strlen($customer->current_mobile_number) != 10)
    return ['ERR_API_INVALID_ARGUMENTS: invalid current_mobile_number', 'MV0001'];
  
  if ($customer->BALANCE < $bolt_on_info['cost'])
    return [$bolt_on_info['cost'], 'VV0103'];

  // check existing bolt on
  $existing = ani_format_select_upglobe($customer->current_mobile_number);
  if (count($existing))
    return ['ERR_API_INTERNAL: Bolt on already exists', 'VV0234', 'UpGlobe bolt on already added'];

  // add 3 ANI_FORMAT records with varients of current_mobile_number
  if ( !start_mssql_transaction() )
    return ['ERR_API_INTERNAL: DB write error (0)', 'DB0001'];

  $aniPrefixArr = [
    $customer->current_mobile_number,
    '1' . $customer->current_mobile_number,
    '+1' . $customer->current_mobile_number
  ];
  foreach ($aniPrefixArr as $aniPrefix)
  {
    $result = ani_format_insert([
      'ani_prefix'  => $aniPrefix,
      'origin'      => '6309',
      'description' => 'UpGlobe'
    ]);

    if ( !$result )
    {
      rollback_mssql_transaction();
      return ['ERR_API_INTERNAL: DB write error (1)', 'DB0001'];
    }
  }

  // deduct from balance
  $result = func_spend_from_balance(
    array(
     'customer_id'      => $customer_id,
     'amount'           => ($bolt_on_info['cost']),
     'detail'           => 'addGLOBEBoltOn' . ucfirst(strtolower($type)),
     'reason'           => 'GLOBE Purchase',
     'reference'        => 'GLOBE_PAYMENT',
     'source'           => 'SPEND',
     'commissionable'   => 1,
     'reference_source' => create_guid('PHPAPI')
    )
  );
  if (count($result['errors']))
  {
    rollback_mssql_transaction();
    return ["ERR_API_INTERNAL: {$result['errors']}", 'DB0001'];
  }

  // set packaged_balance3
  $sql = accounts_update_query([
    'customer_id'       => $customer_id,
    'packaged_balance3' => \Ultra\UltraConfig\getUpGlobeMOU()
  ]);
  if ( !run_sql_and_check($sql) )
  {
    rollback_mssql_transaction();
    return ["ERR_API_INTERNAL: Failed to set packaged_balance3", 'DB0001'];
  }

  if ( !commit_mssql_transaction() )
    return ['ERR_API_INTERNAL: DB write error (2)', 'DB0001'];

  // record bolt on
  $result = trackBoltOn($customer_id, $source, 'DONE', $bolt_on_info['sku'], $bolt_on_info['product'], $type);
  if ($result->is_failure())
    dlog('', 'WARNING: INSERT INTO ULTRA.BOLTON_TRACKER failed');

  // success
  return [null, null];
}

/**
 * addGLOBEBoltOnRecurring
 * add UpGlobe bolt on recurring, same as immediate
 */
function addGLOBEBoltOnRecurring($customer_id, $bolt_on_info, $source, $detail, $plan)
{
  dlog('', '%s, %s, %s, %s', $customer_id, $bolt_on_info, $source, $detail);

  return addGLOBEBoltOnImmediate($customer_id, $bolt_on_info, $source, $detail, null, 'MONTHLY');
}

/**
 * validateImmediateBoltOn
 * check if given immediate bolt on(s) can be applied to the given plan
 * @param Int plan COS_ID
 * @param String or Array bolt on ID
 * @result Boolean TRUE on success, FALSE otherwise
 */
function validateImmediateBoltOn($cos_id, $bolt_on_id)
{
  return validateBoltOn($cos_id, $bolt_on_id, 'immediate');
}

/**
 * validateRecurringBoltOn
 * check if given recurring bolt on(s) can be applied to the given plan
 * @param Int plan COS_ID
 * @param String or Array bolt on ID
 * @result Boolean TRUE on success, FALSE otherwise
 */
function validateRecurringBoltOn($cos_id, $bolt_on_id)
{
  return validateBoltOn($cos_id, $bolt_on_id, 'recurring');
}

/**
 * validateBoltOn
 * internal function used by validateImmediateBoltOn and validateRecurringBoltOn
 * @param Int plan COS_ID
 * @param String or Array bolt on ID
 * @param String bolt on type
 * @result Boolean TRUE on success, FALSE otherwise
 */
function validateBoltOn($cos_id, $bolt_on_id, $bolt_on_type)
{
  // validate COS_ID and get full name
  if ( ! $plan  = get_plan_name_from_short_name(get_plan_from_cos_id($cos_id)))
  {
    dlog('', "ERROR: invalid COS_ID $cos_id");
    return FALSE;
  }

  // validate bolt on type
  $planConfig = \PlanConfig::Instance();
  $types = $planConfig->getPlanConfigItem($cos_id, 'bolt_on_types');
  if (strpos($types, $bolt_on_type) === FALSE)
  {
    dlog('', "ERROR: $bolt_on_type bolt on is not valid for plan $plan");
    return FALSE;
  }

  // validate each bolt on
  $bolt_on_ids = (is_array($bolt_on_id)) ? $bolt_on_id : array($bolt_on_id);
  $available   = \Ultra\UltraConfig\getBoltOnsByPlan($plan);
  foreach ($bolt_on_ids as $id)
    if ( ! empty($id))
      if ( ! in_array($id, $available))
      {
        dlog('', "ERROR: bolt on $id is not valid for plan $plan");
        return FALSE;
      }

  return TRUE;
}

/**
 * verifyMintDataAddOnThreshold
 *
 * Returns NULL if
 *  - the customer brand is not MINT
 *    or
 *  - the customer brand is MINT and he is allowed to purchase Mint Add On Data.
 * Returns an error otherwise.
 *
 * @result String - error
 */
function verifyMintDataAddOnThreshold( $customer , $type = NULL, $actionUUID=NULL )
{
  $error = NULL;

  // check brand
  if ( ! \Ultra\Lib\Util\validateMintBrandId($customer->BRAND_ID) )
    return NULL;

  $redis = new \Ultra\Lib\Util\Redis();

  if ( empty($actionUUID) )
    $actionUUID = getNewUUID('MW');

  list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown, $mvneError ) = mvneGet4gLTE( $actionUUID , $customer->current_mobile_number , $customer->CUSTOMER_ID , $redis );

  \logDebug("remaining = $remaining ; usage = $usage ; mintAddOnRemaining = $mintAddOnRemaining ; mintAddOnUsage = $mintAddOnUsage ; mvneError = $mvneError");

  if ( ! empty( $mvneError ) )
    return $mvneError;

  // no Mint Add On Data
  if ( empty($breakDown[$type]) )
    return NULL;

  $mintDataAddOnUsagePercentage = empty($breakDown[$type])
    ? 0
    : ceil( ( $breakDown[$type]["Mint $type Used"] * 100 ) / $breakDown[$type]["Mint $type Total"] );

  // this is a percentage
  $mindDataAddonThreshold = \Ultra\UltraConfig\getMindDataAddonThreshold();

  \logDebug("mindDataAddonThreshold = $mindDataAddonThreshold ; mintDataAddOnUsagePercentage = $mintDataAddOnUsagePercentage");

  if ( $mintDataAddOnUsagePercentage < ( 100 - $mindDataAddonThreshold ) )
    $error = 'ERR_API_INTERNAL: cannot process data recharge. Minimal usage not met.';

  return $error;
}

