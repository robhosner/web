<?php

require_once 'classes/PHPUnitBase.php';
require_once 'Ultra/Lib/DB/Cache.php';

class CacheTest extends PHPUnitBase
{
  public function test__Cache__selectRow()
  {
    $cache = new \Ultra\Lib\DB\Cache;

    // invalid table
    $result = $cache->selectRow('JWEKWLKERW', 'ERROR_CODE', 'VV0036');
    $this->assertEmpty($result);

    // lower case table
    $result = $cache->selectRow('ultra.user_error_messages', 'ERROR_CODE', 'VV0036');
    $this->assertEmpty($result);

    // invalid primary key
    $result = $cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODES', 'VV0036');
    $this->assertEmpty($result);

    // missing primary key value
    $result = $cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', '');
    $this->assertEmpty($result);

    // table without extended schema definition
    $result = $cache->selectRow('HTT_COVERAGE_INFO', 'ZIPCODE', '11211');
    $this->assertEmpty($result);

    // table without primary key
    $result = $cache->selectRow('HTT_COVERAGE_INFO', 'ZIPCODE', '11211');
    $this->assertEmpty($result);

    // PK value does not exist
    $result = $cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', 'VV11100');
    $this->assertEmpty($result);

    // valid result with lock
    $result = $cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', 'VV0036');
    print_r($result);
    $this->assertNotEmpty($result);
    $this->assertTrue(is_object($result));

    // valid result without lock
    $result = $cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', 'VV0037', SECONDS_IN_HOUR, FALSE);
    print_r($result);
    $this->assertNotEmpty($result);
    $this->assertTrue(is_object($result));

    // get 100 results in succession
    for ($i = 0; $i < 99; $i++)
    {
      $result = $cache->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', 'VV00' . $i, SECONDS_IN_HOUR, FALSE);
      print_r($result);
    }

  }


  public function test__Cache__connect()
  {

    // test invalid host
    $cache = new \Ultra\Lib\DB\Cache(NULL, 'gloomyside', 'ultra_data');
    $data = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', 12345);
    $this->assertEmpty($data);
    $this->assertNotEmpty($err = $cache->getLastError());
    echo "ERROR: $err\n";
    unset($cache);

    // test invalid database
    $cache = new \Ultra\Lib\DB\Cache(NULL, 'sunnyside', 'ultra_datas');
    $data = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', 12345);
    $this->assertEmpty($data);
    $this->assertNotEmpty($err = $cache->getLastError());
    echo "$err\n";
    unset($cache);

    // test database without permissions
    $cache = new \Ultra\Lib\DB\Cache(NULL, 'sunnyside', 'ext_reports');
    $data = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', 12345);
    $this->assertEmpty($data);
    $this->assertNotEmpty($err = $cache->getLastError());
    echo "$err\n";
    unset($cache);

    // successfull test with default database and host
    $cache = new \Ultra\Lib\DB\Cache();
    $id = 18003;
    $result = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', $id);
    print_r($result);
    $this->assertNotEmpty($result);
    $this->assertTrue(is_object($result));
    $this->assertEquals($id, $result->CUSTOMER_ID);
    unset($cache);

    // test specific host and database
    $cache = new \Ultra\Lib\DB\Cache(NULL, 'SUNNYSIDE', 'ULTRA_ACC');
    $this->assertEmpty($cache->getLastError());
    for ($i = 278652000; $i < 278652090; $i++)
    {
      $result = $cache->selectRow('SOAP_LOG', 'SOAP_LOG_ID', $i);
      print_r($result);
      $this->assertNotEmpty($result);
      $this->assertTrue(is_object($result));
      $this->assertEquals($i, $result->SOAP_LOG_ID);
    }
    unset($cache);

  }


  public function test__Cache__selectError()
  {
    $cache = new \Ultra\Lib\DB\Cache;

    // error codes does not exist
    $result = $cache->selectUserError('ZZZ');
    print_r($result);
    $this->assertNotEmpty($result);
    $this->assertEquals($result->ERROR_CODE, 'DB0001');

    // existing error code
    $result = $cache->selectUserError('VV0036');
    print_r($result);
    $this->assertNotEmpty($result);
    $this->assertEquals($result->ERROR_CODE, 'VV0036');

  }

}
