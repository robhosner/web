<?php

namespace Ultra\Lib\DB;
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'db/ultra_session.php';

/**
 * Caching access layer for CELLUPHONE database
 */
class Celluphone
{
  // redis constants
  const DEALER_RECORD_KEY   = 'celluphone/cache/dealer/record/';
  const DEALER_ID_KEY       = 'celluphone/cache/dealer/id/';
  const DEALER_TTL          = 21600; // 6 hours

  private $redis;

  public function __construct()
  {
    $this->redis = new \Ultra\Lib\Util\Redis;
  }
  
  /**
   * getDealerInfoFromCache
   * given either dealer code or ID return dealer info record
   * @param String dealer code
   * @param Integet readler ID
   * @return Object dealer (properties: dealerId, dealerCode, dealerName, distributorId, distributorName, masterId, masterName) or NULL on faiure
   */
  public function getDealerInfoFromCache($code = NULL, $id = NULL)
  {
    // init and check parameters
    $record = NULL;
    if ( ! $code && ! $id)
    {
      dlog('', 'ERROR: blank parameters dealer code %s and id %d', $code, $id);
      return $record;
    }

    // get ID if only code is given
    if ( ! $id)
      $id = $this->redis->get(self::DEALER_ID_KEY . $code);

    // get from cache
    if ($id)
      if ($record = $this->redis->get(self::DEALER_RECORD_KEY . $id))
        $record = json_decode($record);

    // get from DB and cache it
    if ( ! $record)
    {
      if ($record = getDealerInfoFromDb($code, $id))
      {
        try
        {
          foreach ($record as $key => $value)
            $record->$key = utf8_encode($value);

          $json = json_encode($record);
          $this->redis->set(self::DEALER_RECORD_KEY . $record->dealerId, $json, self::DEALER_TTL);
          $this->redis->set(self::DEALER_ID_KEY . $record->dealerCode, $record->dealerId, self::DEALER_TTL);
        }
        catch (\Exception $x)
        {
          dlog('', 'json_encode failed on record %s with error %s', print_r($record, TRUE), $x->getMessage());
        }
      }
      else
        dlog('', "no dealer found for dealer code $code or ID $id");
    }

    return $record;
  }

}

?>
