<?php

namespace Ultra\Lib\DB\MongoDB;

require_once 'classes/Outcome.php';
require_once 'Ultra/Lib/DB/MongoDB.php';

class Base extends \Outcome
{
  protected static $dbObject;
  protected static $dbObjectEvents;
  private static $schemaMap = array();
  private static $reverseSchemaMap = array();

  /**
   * getDB
   *
   * Return the appropriate Mongo DB connection, given the $instance requested
   *
   * @return object of class \MongoDB
   */
  public function getDB( $instance=NULL )
  {
    if ( empty( $instance ) )
    {
      if ( empty( self::$dbObject ) )
      {
        \logDebug( 'Connecting to default Mongo DB instance' );

        self::$dbObject = \Ultra\Lib\DB\selectMongoDB(
          array(
            'db_name' => self::getDBName()
          )
        );
      }

      return self::$dbObject;
    }
    elseif ( $instance === 'Events' )
    {
      // connect to MongoLab (mongolab.com)
      if ( empty( self::$dbObjectEvents ) )
      {
        \logDebug( 'Connecting to MongoLab' );

        self::$dbObjectEvents = \Ultra\Lib\DB\selectMongoDBEvents( array() );
      }

      return self::$dbObjectEvents;
    }
    else
    {
      \logFatal( "Instance $instance not handled" );
      return NULL;
    }
  }

  /**
   * getDBName
   *
   * Returns the name of the Mongo DB
   *
   * @return string
   * @throws \Exception
   */
  public static function getDBName()
  {
    $defaultMongoDBCredentials = \Ultra\UltraConfig\getDefaultMongoDBCredentials();

    if ( empty( $defaultMongoDBCredentials['db_name'] ) )
      return NULL;

    return $defaultMongoDBCredentials['db_name'];
  }

  /**
   * getCollectionObject
   *
   * Returns a collection object given a collection name
   */
  public function getCollectionObject( $collectionName )
  {
    if ( empty( $collectionName ) )
      return NULL;

    $instance = NULL;

    // check if we should use MongoLab (mongolab.com)
    if ( ( $collectionName == 'Events' )
      && \Ultra\UltraConfig\isProductionEnvironment()
    )
    {
      \logInfo( 'Connecting to MongoLab for Event Tracking Production Instance' );

      $instance = 'Events';
    }

    $db = $this->getDB( $instance );

    if ( $db )
      return $db->$collectionName;

    return NULL;
  }

  /**
    * getSchemaMap
    *
    * Returns the schema array for a collection (long names => short name)
    *
    * @return array
    */
  public static function getSchemaMap( $collectionName )
  {
    if ( empty( self::$schemaMap ) )
    {
      self::$schemaMap = mapping();
    }

    if ( !empty( self::$schemaMap[$collectionName] ) )
    {
      return self::$schemaMap[$collectionName];
    }
    else
    {
      \logError( 'Missing schema definition for collection ' . $collectionName );
      return null;
    }
  }

  /**
    * getReverseSchemaMap
    *
    * Returns the schema array for a collection (short names => long name)
    *
    * @return array
    */
  public static function getReverseSchemaMap( $collectionName )
  {
    if ( empty( self::$reverseSchemaMap[$collectionName] ) )
    {
      self::$reverseSchemaMap[$collectionName] = reverseMapping( $collectionName );
    }

    if ( !empty( self::$reverseSchemaMap[$collectionName] ) )
    {
      return self::$reverseSchemaMap[$collectionName];
    }
    else
    {
      \logError( 'Missing schema definition for collection ' . $collectionName );
      return null;
    }
  }
}


/**
  * castDataFields
  *
  * Reads the schema and casts values accordingly. Uses castValue function for casting.
  *
  * @return array
  */
function castDataFields( $collectionName, array $data )
{
  // TODO: there may be more castable operators
  $castableOperators = array( '$eq', '$ne', '$lt', '$lte', '$gt', '$gte' );

  $mapping = Base::getSchemaMap( $collectionName );
  $reverseMapping = Base::getReverseSchemaMap( $collectionName );

  foreach ( $data as $key => &$val )
  {
    // handle short and long names
    if ( !empty( $mapping[$key] ) )
    {
      $definition = $mapping[$key];
    }
    elseif ( !empty( $reverseMapping[$key] ) )
    {
      $definition = $reverseMapping[$key];
    }
    else
    {
      $definition = null;
    }

    if ( is_array( $val ) )
    {
      if ( empty( $definition ) )
      {
        $data[$key] = castDataFields( $collectionName, $val );
      }
      else
      {
        // handle castable operators
        foreach ( $data[$key] as $k => $v )
        {
          if ( in_array( $k, $castableOperators ) )
          {
            $data[$key][$k] = castValue( $definition[1], $v, $key );
          }
          else
          {
            $data[$key][$k] = $v;
          }
        }
      }
    }
    elseif ( empty( $definition ) )
    {
      if ( substr( $key, 0, 1 ) != '$' && $key != '_id' )
      {
        \logError( 'Missing definition for field ' . $key . ' in ' . $collectionName );
      }
      $data[$key] = $val;
    }
    else
    {
      if ( empty( $definition[1] ) )
      {
        \logError( 'Definition for field ' . $key . ' in ' . $collectionName . ' is missing data type' );
        $data[$key] = $val;
      }
      else
      {
        $data[$key] = castValue( $definition[1], $val, $key );
      }
    }
  }

  return $data;
}

/**
  * castValue
  *
  * Given a data type, returns the casted value
  *
  * @return array
  */
function castValue( $type, $value, $key )
{
  if ( is_array( $value ) )
  {
    \logError( $key . ' - trying to cast array' );
    return $value;
  }
  if ( $value === null || $value === '' )
  {
    // null and empty string always null
    return null;
  }
  else
  {
    if ( $type == 'int' || $type == 'timestamp' )
    {
      if ( !ctype_digit( (string)$value ) )
        \logError( $key . ' - ' . $value . ' is not numeric ( type = '.$value.')' );
      
      return (int)$value;
    }
    elseif ( $type == 'boolean' )
    {
      // cast true/false strings
      if ( $value == 'true' )
        return true;
      elseif ( $value == 'false' )
        return false;

      return (bool)$value;
    }
    elseif ( $type == 'object' )
    {
      // for object types (MongoId, MongoBinData, ...)
      return $value;
    }
    else
    {
      // default is strval cast
      return strval( $value );
    }
  }
}

/**
 * toShortName
 *
 * Provides the short param name
 *
 * @return string
 */
function toShortName( $collectionName , $longName )
{
  $mapping = mapping();

  if ( empty( $mapping[ $collectionName ] ) || empty( $mapping[ $collectionName ][ $longName ] ) )
    return '';

  return $mapping[ $collectionName ][ $longName ][0];
}

/**
 * toShortNamesArray
 *
 * Provides short param names
 *
 * @return array
 */
function toShortNamesArray( $collectionName , $data )
{
  $mappedData = array();

  foreach( $data as $datum )
    $mappedData[] = toShortNames( $collectionName , $datum );

  return $mappedData;
}

/**
 * toShortNames
 *
 * Provides short param names
 *
 * @return array
 */
function toShortNames( $collectionName , $data )
{
  $mapping = mapping();

  if ( empty( $mapping[ $collectionName ] ) )
    return array();

  $mappedData = array();

  foreach( $data as $longName => $value )
    if ( in_array( $longName, array( '_id', '$or', '$and' ) ) )
      $mappedData[ $longName ] = $value;
    elseif ( in_array( $longName, array( '$set', '$setOnInsert', '$inc', '$unset' ) ) )
    {
      $mappedData[ $longName ] = toShortNames( $collectionName, $value );
    }
    elseif ( ! empty($mapping[ $collectionName ][ $longName ][0])
        && ( !isset($mapping[ $collectionName ][ $longName ][1]) || $mapping[ $collectionName ][ $longName ][1] != 'array') )
      $mappedData[ $mapping[ $collectionName ][ $longName ][0] ] = $value;
    elseif ( is_array($value) && isset( $mapping[ $collectionName ][ $longName ][1] ) && $mapping[ $collectionName ][ $longName ][1] == 'array'
        && !empty( $mapping[ $collectionName ][ $longName ][2] ) && is_array( $mapping[ $collectionName ][ $longName ][2] ) )
    {
      // array of items
      $shortField = $mapping[ $collectionName ][ $longName ][0];
      $mappedData[$shortField] = array();
      $itemMap = $mapping[ $collectionName ][ $longName ][2];
      foreach ( $value as $item )
      {
        $itemArr = array();
        foreach ( $item as $key => $val )
          if ( isset( $itemMap[$key] ) && !empty( $itemMap[$key][0] ) )
            $itemArr[ $itemMap[$key][0] ] = $val;

        if ( !empty( $itemArr ) )
          $mappedData[$shortField][] = $itemArr;
      }
    }
    else
    {
      // additional depth, example: 'social.facebook' becomes 'social'=>'fb'
      $parts = explode('.',$longName);
      if ( ! empty($mapping[ $collectionName ]) )
      if ( ! empty($mapping[ $collectionName ][ $parts[0] ]) )
      if ( ! empty($mapping[ $collectionName ][ $parts[0] ][ $parts[1] ]) )
      if ( ! empty($mapping[ $collectionName ][ $parts[0] ][ $parts[1] ][0]) )
        $mappedData[ $parts[0] ][ $mapping[ $collectionName ][ $parts[0] ][ $parts[1] ][0] ] = $value;
    }

  return $mappedData;
}

/**
 * toLongNames
 *
 * Provides long param names
 *
 * @return array
 */
function toLongNames( $collectionName , $data )
{
  $mapping = reverseMapping( $collectionName );

  if ( empty( $mapping ) )
    return array();

  $mappedData = array();

  $n = count($data);

  for ( $i=0 ; $i < $n; $i++ )
    foreach( $data[ $i ] as $shortName => $value )
    {
      if ( !isset( $mapping[ $shortName ] ) && $shortName != '_id' )
      {
        \logError( 'Skipping... missing schema definition for ' . $shortName );
      }
      elseif ( isset($mapping[ $shortName ][0]) && !is_array( $value ) )
        $mappedData[ $i ][ $mapping[ $shortName ][0] ] = $value;
      elseif ( $shortName == '_id' )
        $mappedData[ $i ][ $shortName ] = $value;
      elseif ( is_array($value) )
      {
        if (\isAssociative( $value ) )
        {
          // additional depth
          $mappedData[ $i ][ $shortName ] = array();

          foreach( $value as $shortParam => $shortValue )
            if ( is_array($value) )
              $mappedData[ $i ][ $shortName ][ $mapping[ $shortName ][ $shortParam ][0] ] = $value[ $shortParam ];
            else
              $mappedData[ $i ][ $shortName ][ $mapping[ $shortName ][ $shortParam ][0] ] = $value->$shortParam;
        }
        elseif ( empty($mapping[ $shortName ][2]) )
        {
          if ( !empty( $mapping[ $shortName ][1] ) && $mapping[ $shortName ][1] == 'list' )
            $mappedData[ $i ][ $shortName ] = $value;
          else
            \logFatal('data format invalid (expecting fieldMap) : '.json_encode($mapping[ $shortName ]));
        }
        else
        {
          // array of items
          $longFieldName = $mapping[ $shortName ][0];
          $fieldMap = $mapping[ $shortName ][2];
          $mappedData[ $i ][ $longFieldName ] = array();

          // array of items
          foreach ( $value as $item )
          {
            $itemArr = array();

            if ( is_array($item) )
            {
              foreach ( $item as $key => $val )
              {
                if ( isset( $fieldMap[$key] ) )
                  $itemArr[ $fieldMap[$key][0] ] = $val;
              }

              if ( !empty( $itemArr ) )
                $mappedData[ $i ][ $longFieldName ][] = $itemArr;
            }
            else
              \logFatal('data format invalid (expecting array) : '.json_encode($item));
          }
        }
      }
    }

  return $mappedData;
}

/**
 * reverseMapping
 *
 * Returns an array as follows:
 *   $shortName => Array
 *       (
 *           [0] => $longName
 *           [1] => $dataType
 *           [2] => $length
 *       )
 * For field of type array, $length is array of definitions for item fields
 *
 * @return array
 */
function reverseMapping( $collectionName )
{
  $mapping = mapping();

  $reverseMapping = array();

  foreach( $mapping[ $collectionName ] as $longName => $info )
  {
    if ( isset($info[0]) && ( !isset($info[1]) || $info[1] != 'array') )
    {
      $reverseMapping[ $info[0] ] = $info;
      $reverseMapping[ $info[0] ][0] = $longName;
    }
    elseif ( isset($info[1]) && $info[1] == 'array' && isset($info[2]) && is_array($info[2]) )
    {
      // array of items
      $reverseMapping[ $info[0] ] = array(
        $longName,
        'array',
        array()
      );
      foreach ( $info[2] as $itemLongName => $itemInfo )
      {
        $reverseMapping[ $info[0] ][2][ $itemInfo[0] ] = array(
          $itemLongName,
          $itemInfo[1]
        );
      }
    }
    else
    {
      // additional depth
      foreach( $info as $key => $value )
      {
        $reverseMapping[ $longName ][ $value[0] ] = $value;
        $reverseMapping[ $longName ][ $value[0] ][0] = $key;
      }
    }
  }

  return $reverseMapping;
}

/**
 * isList
 *
 * Returns TRUE if $shortName is defined as 'list'
 *
 * @return boolean
 */
function isList( $collectionName , $shortName )
{
  $isList = FALSE;

  $reverseMapping = reverseMapping( $collectionName );

  if ( ! empty( $reverseMapping[ $shortName ] ) )
    $isList = ! ! ( $reverseMapping[ $shortName ][1] == 'list' );

  return $isList;
}

/**
 * mapping
 *
 * MongoDB documents definition and mapping
 * Example:
 *  'login_name'        => array( 'us' , 'varchar' , 20 ),
 *   - long param name  is 'login_name'
 *   - short param name is 'us'
 *   - param type       is 'varchar'
 *   - max lenght       is 20
 *
 * @return array
 */
function mapping()
{
  $configNamespace = \Ultra\UltraConfig\getConfigNamespace();

  $schema = '\\' . $configNamespace . '\Lib\DB\MongoDB\schema';

  return $schema();
}

