use strict;
use warnings;


use Data::Dumper;
use JSON::XS;
use Test::More;


BEGIN { use_ok('Ultra::Lib::MQ::EndPoint'); }


# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/EndPoint_test.log';

# instantiate our JSON coder/encoder
my $json_coder  = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

# instantiate Ultra::Lib::MQ::EndPoint
my $mq_endpoint = Ultra::Lib::MQ::EndPoint->new( JSON_CODER => $json_coder );

ok( ref $mq_endpoint eq 'Ultra::Lib::MQ::EndPoint' , 'Ultra::Lib::MQ::EndPoint new' );


## Message Tests ##

my $messageTTLSeconds = $mq_endpoint->getMessageTTLSeconds();

ok( $messageTTLSeconds , 'getMessageTTLSeconds' );

my $data = { header => 'a_command' , 'body' => ['some'=>'data'] };

my $aNewTestMessage = $mq_endpoint->buildMessage( $data );

my $newData = $mq_endpoint->extractFromMessage( $aNewTestMessage );

=head
$VAR1 = {
          'body' => [
                      'some',
                      'data'
                    ],
          '_timestamp' => 1461010850,
          '_createdBy' => 'Ultra/Lib/MQ/EndPoint_test.pl',
          'header' => 'a_command'
        };
=cut

ok( ref $newData eq 'HASH' , 'extractFromMessage 1' );
ok( $newData->{header} eq 'a_command' , 'extractFromMessage 2' );
ok( ref $newData->{body} eq 'ARRAY' , 'extractFromMessage 3' );


## Control Channel Tests ##


my $aControlUUID = $mq_endpoint->_getNewControlUUID();

ok( length($aControlUUID) == 38 , 'ControlUUID lenght' );

ok( $aControlUUID =~ /^\{C/ , 'ControlUUID structure' );

my $aMessage = 'This is a message from EndPoint_test.pl';


## Notification Channel Tests ##


my $aNotificationUUID = $mq_endpoint->_getNewNotificationUUID();

ok( length($aNotificationUUID) == 38 , 'NotificationUUID lenght' );

ok( $aNotificationUUID =~ /^\{N/ , 'NotificationUUID structure' );

# reset the ACC MW notification channel
$mq_endpoint->_redis()->del( $mq_endpoint->notificationChannelACCMW() );

my $alphaMessage = 'alpha';
my $betaMessage  = 'beta';
my $gammaMessage = 'gamma';

$mq_endpoint->enqueueNotificationChannel($alphaMessage);
$mq_endpoint->enqueueNotificationChannel($betaMessage);
$mq_endpoint->enqueueNotificationChannel($gammaMessage);

ok( $alphaMessage eq $mq_endpoint->peekNotificationChannel() , 'peekNotificationChannel '.$alphaMessage );

ok( $alphaMessage eq $mq_endpoint->dequeueNotificationChannel() , 'dequeueNotificationChannel '.$alphaMessage );

ok( $betaMessage eq $mq_endpoint->peekNotificationChannel() , 'peekNotificationChannel '.$betaMessage );

ok( $betaMessage eq $mq_endpoint->dequeueNotificationChannel() , 'dequeueNotificationChannel '.$betaMessage );

ok( $gammaMessage eq $mq_endpoint->peekNotificationChannel() , 'peekNotificationChannel '.$gammaMessage );

ok( $gammaMessage eq $mq_endpoint->dequeueNotificationChannel() , 'dequeueNotificationChannel '.$gammaMessage );


done_testing();


__END__


Test script for Ultra::Lib::MQ::EndPoint


Usage:
perl Ultra/Lib/MQ/EndPoint_test.pl

