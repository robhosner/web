<?php

# Usage:
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwActivateSubscriber   $ICCID $ZIPCODE $PLAN $WHOLESALEPLAN $LANG $ACTION_UUID $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwCanActivate          $ICCID   $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwCancelDeviceLocation $ACTION_UUID $MSISDN $ICCID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwCancelPortIn         $ACTION_UUID $MSISDN
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwChangeIMEI           $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwChangeMSISDN         $ACTION_UUID $MSISDN $ICCID $ZIPCODE $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwChangeSIM            $ACTION_UUID $MSISDN $OLD_ICCID $NEW_ICCID $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwCheckBalance         $MSISDN  $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwDeactivateSubscriber $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoAddMintData  $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $DATAADDON
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoDeactivate   $ACTION_UUID $CUSTOMER_ID $MSISDN $ICCID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoReactivate   $ACTION_UUID $CUSTOMER_ID $MSISDN $ICCID $PLAN $WHOLESALE_PLAN $LANG
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoRenewPlan    $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoRenewMintPlan $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoSuspend      $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoUpgradePlan  $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $OPTION
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwGetMVNEDetails       $ACTION_UUID $MSISDN $ICCID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwGetNetworkDetails    $ICCID   $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwGetNGPList           $ZIPCODE $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwPortIn               $CUSTOMER_ID $MSISDN $ICCID $ACTION_UUID $ZIPCODE $PLAN $WHOLESALEPLAN $LANG $ACCOUNT $PASSWD
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwPortInEligibility    $MSISDN  $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwQueryStatus          $ACTION_UUID $MSISDN
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwQuerySubscriber       $MSISDN $ICCID $ACTION_UUID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwQuerySubscriberByICCIDList $ICCID_LIST
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwReactivateSubscriber  $ACTION_UUID $MSISDN $ICCID $WHOLESALEPLAN $PLAN $LANG $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwRenewPlan             $ACTION_UUID $MSISDN $ICCID $WHOLESALEPLAN $PLAN $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwRenewPlanRaw          $ACTION_UUID $MSISDN $ICCID $WHOLESALEPLAN $PLAN $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwRestoreSubscriber     $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwSendSMS               $ACTION_UUID $MSISDN $LANG  $TEXT
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwSendSMSAsynch         $ACTION_UUID $MSISDN $LANG  $TEXT
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwSuspendSubscriber     $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures   $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $OPTION
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeaturesRaw $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $B_VOICE $B_SMS
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePortIn            $ACTION_UUID $MSISDN $ACCOUNT $PASSWD $ZIPCODE
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePortInExtended    $ACTION_UUID $MSISDN $ACCOUNT $PASSWD $ZIPCODE $FIRSTNAME $LASTNAME
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdateSubscriberFeature $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $FEATURE
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdateWholesalePlan     $ACTION_UUID $MSISDN $WHOLESALEPLAN $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php get_iccids_from_msisdns $MSISDN_LIST
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwQuerySubscriberByCustomerId $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwSuspendSubscriberByCustomerId $CUSTOMER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php add_voice_by_customer_id      $CUSTOMER_ID $MINUTES
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdateSubscriberAddress $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $ADDRESS

# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoRenewEndMintPlan $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoRenewMidMintPlan $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoSuspendMintPlan  $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID $WHOLESALEPLAN $PLAN $LANG $PLAN_TRACKER_ID
# php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwRenewMintPlan            $ACTION_UUID $MSISDN $ICCID $CUSTOMER_ID

include_once('db.php');
include_once('Ultra/Lib/MiddleWare/Adapter/Control.php');
include_once('Ultra/tests/SoapLog.php');
require_once 'Ultra/Lib/DB/Customer.php';


$mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;


abstract class AbstractTestStrategy
{
  abstract function test( $argv , $mwControl );
}


class Test_add_voice_by_customer_id extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    if ( ! $argv[2] || ! $argv[3] )
      die("invalid or missing parameters");

    teldata_change_db();

    $customer = get_customer_from_customer_id( $argv[2] );

    if ( !$customer )
    {
      $e = "Cannot find customer ".$argv[2];
      dlog('',"$e");
      echo "$e\n";
      die($e);
    }

    $result = $mwControl->mwMakeitsoUpgradePlan(
      array(
        'actionUUID'         => getNewActionUUID('add_voice_by_customer_id ' . time()),
        'msisdn'             => $customer->current_mobile_number,
        'iccid'              => $customer->CURRENT_ICCID_FULL,
        'customer_id'        => $customer->CUSTOMER_ID,
        'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan( $customer->CUSTOMER_ID ),
        'ultra_plan'         => get_plan_from_cos_id( $customer->COS_ID ), # L[12345]9,
        'preferred_language' => $customer->preferred_language,
        'option'             => 'B-VOICE|' . $argv[3]
      )
    );

    if ( $result->is_success() )
      dlog('',"mwMakeitsoUpgradePlan success");
    else
      dlog('',"mwMakeitsoUpgradePlan failure");

    exit;
  }
}


class Test_mwUpdatePortIn extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwUpdatePortIn(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'port_account'       => $argv[4],
        'port_password'      => $argv[5],
        'zipcode'            => $argv[6]
      )
    );
  }
}


class Test_mwUpdatePortInExtended extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwUpdatePortIn(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'port_account'       => $argv[4],
        'port_password'      => $argv[5],
        'zipcode'            => $argv[6],
        'first_name'         => $argv[7],
        'last_name'          => $argv[8],
        'address_1'          => '123 Main Street',
        'city'               => 'Atlanta',
        'state'              => 'GA'
      )
    );
  }
}


class Test_mwQueryStatus extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwQueryStatus(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3]
      )
    );
  }
}


class Test_mwCancelDeviceLocation extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwCancelDeviceLocation(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'iccid'              => $argv[4]
      )
    );
  }
}


class Test_mwMakeitsoAddMintData extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwMakeitsoAddMintData([
      'actionUUID'         => $actionUUID,
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'customer_id'        => $argv[5],
      'wholesale_plan'     => $argv[6],
      'ultra_plan'         => $argv[7],
      'preferred_language' => $argv[8],
      'data_add_on'        => $argv[9]
    ]);
  }
}


class Test_mwMakeitsoSuspend extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwMakeitsoSuspend(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8]
      )
    );
  }
}


class Test_mwMakeitsoUpgradePlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwMakeitsoUpgradePlan(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8],
        'option'             => $argv[9]
      )
    );
  }
}


class Test_mwMakeitsoDeactivate extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwMakeitsoDeactivate(
      array(
        'actionUUID'         => $argv[2],
        'customer_id'        => $argv[3],
        'msisdn'             => $argv[4],
        'iccid'              => $argv[5]
      )
    );
  }
}


class Test_mwMakeitsoReactivate extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwMakeitsoReactivate(
      array(
        'actionUUID'         => $argv[2],
        'customer_id'        => $argv[3],
        'msisdn'             => $argv[4],
        'iccid'              => $argv[5],
        'ultra_plan'         => $argv[6],
        'wholesale_plan'     => $argv[7],
        'preferred_language' => $argv[8]
      )
    );
  }
}


class Test_mwMakeitsoRenewPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwMakeitsoRenewPlan(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8]
      )
    );
  }
}


class Test_mwCancelPortIn extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwCancelPortIn(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn
      )
    );
  }
}


class Test_mwChangeIMEI extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwChangeIMEI(
      array(
      )
    );
  }
}


class Test_mwChangeMSISDN extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwChangeMSISDN(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'iccid'              => $argv[4],
        'zipcode'            => $argv[5],
        'customer_id'        => $argv[6]
      )
    );
  }
}


class Test_mwChangeSIM extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwChangeSIM(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'old_iccid'          => $argv[4],
        'new_iccid'          => $argv[5],
        'customer_id'        => $argv[6]
      )
    );
  }
}


class Test_mwGetNetworkDetails extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $r = $mwControl->mwGetNetworkDetails(
      array(
        'iccid'      => $argv[2],
        'actionUUID' => $argv[3]
      )
    );

    if ( isset($r->data_array['body']) )
    {
      print_r($r->data_array['body']); exit;
    }

    return $r;
  }
}


class Test_mwCanActivate extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwCanActivate(
      array(
        'iccid'      => $argv[2],
        'actionUUID' => $argv[3]
      )
    );
  }
}


class Test_mwRenewPlanRaw extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;

/*
    $features = array(
      array(
        'FeatureID'    => 12808,
        'FeatureValue' => '',
        'Action'       => 'RESET',
        'autoRenew'    => 'false'
      )
    );
*/

/*
[{"FeatureID":12808,"FeatureValue":"","Action":"RESET","autoRenew":"false"},{"Action":"RESET","autoRenew":"false","FeatureID":"12701","FeatureValue":"8250"}]
*/

    #echo json_encode( $features )."\n";

    print_r($features);

    $features = json_decode('[{"FeatureID":12808,"FeatureValue":"","Action":"RESET","autoRenew":"false"},{"Action":"RESET","autoRenew":"false","FeatureID":"12701","FeatureValue":"8250"}]');

    print_r($features);

    $result = $mwControl->mwRenewPlanRaw(
      array(
        'actionUUID'          => $actionUUID,
        'msisdn'              => $msisdn,
        'iccid'               => $iccid,
        'wholesale_plan'      => $argv[5],
        'ultra_plan'          => $argv[6],
        'customer_id'         => $argv[7],
        'addOnFeatureListRaw' => array(
          'AddOnFeature' => $features
        )
      )
    );

    return $result;
  }
}


class Test_mwRenewPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;

    $result = $mwControl->mwRenewPlan(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'wholesale_plan'     => $argv[5],
        'ultra_plan'         => $argv[6],
        'customer_id'        => $argv[7]
      )
    );

    return $result;
  }
}


class Test_mwReactivateSubscriber extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $timestamp  = time();

    $result = $mwControl->mwReactivateSubscriber(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'wholesale_plan'     => $argv[5],
        'ultra_plan'         => $argv[6],
        'preferred_language' => $argv[7],
        'customer_id'        => $argv[8]
      )
    );

    displaySoapRequests(
      array(
        'fromtimestamp' => $timestamp,
        'msisdn'        => $msisdn,
        'iccid'         => $iccid,
        'command'       => array( 'ReactivateSubscriber' )
      )
    );

    return $result;
  }
}


class Test_mwUpdateSubscriberFeature extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $timestamp  = time();

    $result = $mwControl->mwUpdateSubscriberFeature(
      array(
        'actionUUID'  => $actionUUID,
        'msisdn'      => $msisdn,
        'iccid'       => $iccid,
        'customer_id' => $argv[5],
        'feature'     => $argv[6]
      )
    );

    displaySoapRequests(
      array(
        'fromtimestamp' => $timestamp,
        'msisdn'        => $msisdn,
        'iccid'         => $iccid,
        'command'       => array( 'UpdatePlanAndFeatures','UpdateSubscriberFeature','QuerySubscriber' )
      )
    );

    return $result;
  }
}


class Test_mwRestoreSubscriber extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $timestamp  = time();

    $result = $mwControl->mwRestoreSubscriber(
      array(
        'actionUUID'  => $actionUUID,
        'msisdn'      => $msisdn,
        'iccid'       => $iccid,
        'customer_id' => $argv[5]
      )
    );

    displaySoapRequests(
      array(
        'fromtimestamp' => $timestamp,
        'msisdn'        => $msisdn,
        'iccid'         => $iccid,
        'command'       => array( 'RestoreSubscriber' )
      )
    );

    return $result;
  }
}


class Test_mwUpdateWholesalePlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    teldata_change_db();

    return $mwControl->mwUpdateWholesalePlan(
      array(
        'actionUUID'     => $argv[2],
        'msisdn'         => $argv[3],
        'wholesale_plan' => $argv[4],
        'customer_id'    => $argv[5]
      )
    );
  }
}


class Test_mwSendSMSAsynch extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwSendSMSAsynch(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'preferred_language' => $argv[4],
        'sms_text'           => $argv[5]
      )
    );
  }
}


class Test_mwSendSMS extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwSendSMS(
      array(
        'actionUUID'         => $argv[2],
        'msisdn'             => $argv[3],
        'preferred_language' => $argv[4],
        'sms_text'           => $argv[5]
      )
    );
  }
}


class Test_mwUpdatePlanAndFeatures extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwUpdatePlanAndFeatures(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8],
        'option'             => $argv[9]
      )
    );
  }
}

class Test_mwUpdatePlanAndFeaturesRaw extends AbstractTestStrategy
{
  function test( $argv, $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwUpdatePlanAndFeaturesRaw(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8],
        'raw_b_voice'        => $argv[9],
        'raw_b_sms'          => $argv[10]
      )
    );
  }
}

class Test_mwSuspendSubscriber extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    $timestamp  = time();

    $result = $mwControl->mwSuspendSubscriber(
      array(
        'actionUUID'  => $actionUUID,
        'msisdn'      => $msisdn,
        'iccid'       => $iccid,
        'customer_id' => $argv[5]
      )
    );

    displaySoapRequests(
      array(
        'fromtimestamp' => $timestamp,
        'msisdn'        => $msisdn,
        'iccid'         => $iccid,
        'command'       => array( 'SuspendSubscriber' )
      )
    );

    return $result;
  }
}


class Test_mwDeactivateSubscriber extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwDeactivateSubscriber(
      array(
        'actionUUID'  => $argv[2],
        'msisdn'      => $argv[3],
        'iccid'       => $argv[4],
        'customer_id' => $argv[5]
      )
    );
  }
}


class Test_mwUpdateSubscriberAddress extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID  = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn      = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid       = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $customer_id = $argv[5];
    $address     = $argv[6];

    $result = $mwControl->mwUpdateSubscriberAddress(
      array(
        'actionUUID'  => $actionUUID,
        'customer_id' => $customer_id,
        'msisdn'      => $msisdn,
        'iccid'       => $iccid,
        'E911Address' => array(
          'addressLine1' => $address,
          'addressLine2' => $address,
          'City'         => 'San Diego',
          'State'        => 'CA',
          'Zip'          => '10124'
        )
      )
    );

    print_r(  $result );

    return $result;
  }
}


class Test_mwGetMVNEDetails extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $timestamp  = time();

    $result = $mwControl->mwGetMVNEDetails(
      array(
        'actionUUID' => $actionUUID,
        'msisdn'     => $msisdn,
        'iccid'      => $iccid
      )
    );

    // displaySoapRequestsByActionUUID( $actionUUID );

    displaySoapRequests(
      array(
        'fromtimestamp' => $timestamp,
        'msisdn'        => $msisdn,
        'iccid'         => $iccid,
        'command'       => array( 'GetNetworkDetails' , 'CheckBalance' )
      )
    );

    return $result;
  }
}


class Test_mwGetNGPList extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwGetNGPList(
      array(
        'actionUUID' => $argv[3],
        'zipcode'    => $argv[2]
      )
    );
  }
}


class Test_mwPortIn extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    teldata_change_db();

    return $mwControl->mwPortIn(
      array(
        'customer_id'        => $argv[2],
        'msisdn'             => $argv[3],
        'iccid'              => $argv[4],
        'actionUUID'         => $argv[5],
        'zipcode'            => $argv[6],
        'ultra_plan'         => $argv[7],
        'wholesale_plan'     => $argv[8],
        'preferred_language' => $argv[9],
        'port_account'       => $argv[10],
        'port_password'      => $argv[11]
      )
    );
  }
}


class Test_mwPortInEligibility extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    return $mwControl->mwPortInEligibility(
      array(
        'actionUUID' => $argv[3],
        'msisdn'     => $argv[2]
      )
    );
  }
}


class Test_mwCheckBalance extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $r = $mwControl->mwCheckBalance(
      array(
        'actionUUID' => $argv[3],
        'msisdn'     => $argv[2]
      )
    );

    if ( isset($r->data_array['body']) )
    {
      print_r($r->data_array['body']); exit;
    }

    return $r;
  }
}


class Test_mwSuspendSubscriberByCustomerId extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    teldata_change_db();

    $customer = get_ultra_customer_from_customer_id( $argv[2] , array('CUSTOMER_ID','current_mobile_number','current_iccid_full') );

    print_r( $customer );

    $result = $mwControl->mwSuspendSubscriber(
      array(
        'actionUUID'  => time().__FUNCTION__,
        'msisdn'      => $customer->current_mobile_number,
        'iccid'       => $customer->current_iccid_full,
        'customer_id' => $argv[2]
      )
    );

    return $result;
  }
}


class Test_mwQuerySubscriberByCustomerId extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    teldata_change_db();

    $customer = get_ultra_customer_from_customer_id( $argv[2] , array('CUSTOMER_ID','current_mobile_number','current_iccid_full') );

    print_r( $customer );

    $data = array(
      'actionUUID' => time().__FUNCTION__,
      'iccid'      => $customer->current_iccid_full,
      'msisdn'     => $customer->current_mobile_number
    );

    print_r( $data );

    $result = $mwControl->mwQuerySubscriber( $data );

    echo "\n";

    if ( isset($result->data_array['body']) )
    {
      if ( is_object($result->data_array['body']) && is_object($result->data_array['body']->AddOnFeatureInfoList) )
        foreach( $result->data_array['body']->AddOnFeatureInfoList->AddOnFeatureInfo as $soc )
          echo $soc->FeatureID . sprintf("\t %16.s \t", $soc->UltraServiceName ) . $soc->UltraDescription . "\n";

      echo "\nSubscriberStatus : ".$result->data_array['body']->SubscriberStatus."\n";

      if ( property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) )
        echo "\nCurrentAsyncService : ".$result->data_array['body']->CurrentAsyncService."\n";

      if ( property_exists( $result->data_array['body'] , 'SIM' ) )
        echo "\nSIM : ".$result->data_array['body']->SIM."\n";

      // load WHOLESALE_PLAN_ID from DB
      $wholesale_plan = \Ultra\Lib\DB\Customer\getWholesalePlan( $argv[2] );

      echo "wholesale_plan = $wholesale_plan_id\n";
    }

    echo "\n";

    return $result;
  }
}


class Test_mwQuerySubscriberByICCIDList extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $list       = $argv[2];
    $actionUUID = 'test_'.time();

    $iccid_list = explode(' ',$list);

    foreach( $iccid_list as $iccid )
    {
      $data = array(
        'actionUUID' => $actionUUID,
        'iccid'      => $iccid,
      );

      $result = $mwControl->mwQuerySubscriber( $data );

      if ( $result->is_success()
        && isset($result->data_array['success'])
        && $result->data_array['success']
        && isset($result->data_array['body'])
        && ( property_exists( $result->data_array['body'] , 'ResultCode' ) )
        && ( property_exists( $result->data_array['body'] , 'ResultMsg'  ) )
        && ( $result->data_array['body']->ResultCode == '100' )
        && ( $result->data_array['body']->ResultMsg  == 'Success' )
      )
      {
        echo "$iccid SubscriberStatus = ".$result->data_array['body']->SubscriberStatus."\n";
      }
      else
      {
        echo "mwQuerySubscriber failed for $iccid\n";
      }
    }

    exit;
  }
}


class Test_mwQuerySubscriber extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $data = array(
      'actionUUID' => $argv[4],
      'iccid'      => $argv[3],
      'msisdn'     => $argv[2]
    );

    print_r( $data );

    $result = $mwControl->mwQuerySubscriber( $data );

    echo "\n";

    dlog('',"result      = %s",$result);
    dlog('',"result data = %s",$result->data_array);

    if ( ! empty( $result->data_array['ResultCode'] ) )
      dlog('',"ResultCode  = %s",$result->data_array['ResultCode']);

    if ( ! empty( $result->data_array['ResultMsg'] ) )
      dlog('',"ResultMsg   = %s",$result->data_array['ResultMsg']);

    if ( isset($result->data_array['body']) )
    {
      if ( is_object($result->data_array['body']) && is_object($result->data_array['body']->AddOnFeatureInfoList) )
        foreach( $result->data_array['body']->AddOnFeatureInfoList->AddOnFeatureInfo as $soc )
          echo $soc->FeatureID . sprintf("\t %16.s \t", $soc->UltraServiceName ) . $soc->UltraDescription . "\n";

      echo "\nSubscriberStatus : ".$result->data_array['body']->SubscriberStatus."\n";

      if ( property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) )
        echo "\nCurrentAsyncService : ".$result->data_array['body']->CurrentAsyncService."\n";

      if ( property_exists( $result->data_array['body'] , 'SIM' ) )
        echo "\nSIM : ".$result->data_array['body']->SIM."\n";
    }

    echo "\n";

    return $result;
  }
}


class Test_mwActivateSubscriber extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    teldata_change_db();
    return $mwControl->mwActivateSubscriber(
      array(
        'iccid'              => $argv[2],
        'zipcode'            => $argv[3],
        'ultra_plan'         => $argv[4],
        'wholesale_plan'     => $argv[5],
        'preferred_language' => $argv[6],
        'actionUUID'         => $argv[7],
        'customer_id'        => $argv[8]
      )
    );
  }
}


class Test_mwMakeitsoRenewEndMintPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    $plan_tracker_id = ( isset($argv[9])  && $argv[9]  ) ? $argv[9]  : NULL ;
    $subplan         = ( isset($argv[10]) && $argv[10] ) ? $argv[10] : NULL ;

    return $mwControl->mwMakeitsoRenewEndMintPlan(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8],
        'plan_tracker_id'    => $plan_tracker_id,
        'subplan'            => $subplan
      )
    );
  }
}


class Test_mwMakeitsoRenewMidMintPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    $plan_tracker_id = ( isset($argv[9])  && $argv[9]  ) ? $argv[9]  : NULL ;
    $subplan         = ( isset($argv[10]) && $argv[10] ) ? $argv[10] : NULL ;

    return $mwControl->mwMakeitsoRenewMidMintPlan(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8],
        'plan_tracker_id'    => $plan_tracker_id,
        'subplan'            => $subplan
      )
    );
  }
}


class Test_mwMakeitsoRenewMintPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $plan_tracker_id = ( isset($argv[9])  && $argv[9]  ) ? $argv[9]  : NULL ;

    return $mwControl->mwMakeitsoRenewMintPlan([
      'actionUUID'         => $actionUUID,
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'customer_id'        => $argv[5],
      'wholesale_plan'     => $argv[6],
      'ultra_plan'         => $argv[7],
      'preferred_language' => $argv[8],
      'plan_tracker_id'    => $plan_tracker_id
    ]);
  }
}


class Test_mwMakeitsoSuspendMintPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());

    return $mwControl->mwMakeitsoSuspendMintPlan(
      array(
        'actionUUID'         => $actionUUID,
        'msisdn'             => $msisdn,
        'iccid'              => $iccid,
        'customer_id'        => $argv[5],
        'wholesale_plan'     => $argv[6],
        'ultra_plan'         => $argv[7],
        'preferred_language' => $argv[8],
        'plan_tracker_id'    => $argv[9]
      )
    );
  }
}


class Test_mwRenewMintPlan extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $actionUUID = ( isset($argv[2]) && $argv[2] ) ? $argv[2]                   : getNewActionUUID('test ' . time());
    $msisdn     = ( isset($argv[3]) && $argv[3] ) ? normalize_msisdn($argv[3]) : NULL ;
    $iccid      = ( isset($argv[4]) && $argv[4] ) ? luhnenize($argv[4])        : NULL ;
    $timestamp  = time();

    $result = $mwControl->mwRenewMintPlan(
      array(
        'actionUUID'  => $actionUUID,
        'msisdn'      => $msisdn,
        'iccid'       => $iccid,
        'customer_id' => $argv[5]
      )
    );

    displaySoapRequests(
      array(
        'fromtimestamp' => $timestamp,
        'msisdn'        => $msisdn,
        'iccid'         => $iccid,
        'command'       => array( 'RestoreSubscriber' )
      )
    );

    return $result;
  }
}


# given a comma or space separated list of msisdns, it returns the ICCIDs associated to them
class Test_get_iccids_from_msisdns extends AbstractTestStrategy
{
  function test( $argv , $mwControl )
  {
    $msisdns = $argv[2];

    $list = explode(',',$msisdns);

    print_r($list);

    $data = array(
      'actionUUID' => time(),
      'iccid'      => ''
    );

    $output = '';

    foreach( $list as $row )
    {
      $list1 = explode(' ',$row);

      foreach( $list1 as $msisdn )
      {
        $data['msisdn'] = $msisdn;

        $iccid = '';

        $result = $mwControl->mwQuerySubscriber( $data );

        if ( $result->is_success() 
          && isset( $result->data_array['body'] )
          && property_exists( $result->data_array['body'] , 'SIM' )
          && $result->data_array['body']->SIM
        )
          $iccid = $result->data_array['body']->SIM;

        $output .= $msisdn.','.$iccid."\n";
      }
    }

    echo "\n$output";

    exit;
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$result = $testObject->test( $argv , $mwControl );

print_r( $result->get_errors() );
print_r( $result->to_string() );
echo "\n";


/*
Examples:

php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwActivateSubscriber 1901260842107741111 39574 L19 W-PRIMARY en test 8
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwCanActivate 8901260842107742315 testaac1
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwDeactivateSubscriber deact_live_476296 2284379356 8901260842107740269 476296
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwGetMVNEDetails 'aaa' 1001001002 ''
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwGetNGPList 10128 kjadhsfvgkjashvgfkjqasgdvfjhas
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwQuerySubscriber 6199060903 sjdhfskjh
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwReactivateSubscriber react_live_B_476296 2284379356 8901260842107740269 W-PRIMARY L39 EN 476296
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwRestoreSubscriber adfadfadf_476296 2284379356 8901260842107740269 476296
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwSendSMS zsfkgh6 1001001000 ES 'hello there'
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwSuspendSubscriber susp_test_live_476296 2284379356 8901260842107740269 476296
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoSuspend mktsotestac1 476296 L39 W-PRIMARY EN 2284379378 8901260842107740269
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP1 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN test_option
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwPortIn 1 1001001000 1001001000100100100 ACTION_UUID 12345 L19 W-PRIMARY en ACCOUNT PASSWD

Switch to Voicemail English:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP2 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN V-ENGLISH
Switch to Voicemail Spanish:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP3 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN V-SPANISH
Increment or Add B-VOICE:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP4 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'B-VOICE|9500'
Increment or Add B-SMS:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP6 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'B-SMS|13750'
Increment or Add A-VOICE-DR:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP7 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'A-VOICE-DR|123'
Increment or Add A-SMS-DR:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateP8 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'A-SMS-DR|456'
Change plan:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdatecp01 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'CHANGE|L49'
Add data:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwUpdatePlanAndFeatures mwUpdateADDda01 2284379378 8901260842107740269 476296 W-PRIMARY L39 EN 'A-DATA-BLK-DR|1'

mwMakeitsoUpgradePlan - Add data:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoUpgradePlan mup00000001 2284379378 8901260842107740269 476296 W-PRIMARY L59 EN 'A-DATA-BLK-DR|1'

mwMakeitsoSuspend:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoSuspend sspp001 2284379378 8901260842107740269 476296 W-PRIMARY L59 EN

mwRestoreSubscriber:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwRestoreSubscriber res001 2284379378 8901260842107740269 476296

mwMakeitsoReactivate:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoReactivate VYT_TEST 3647 3127149962 8901260962113607409 L19 W-SECONDARY EN

mwMakeitsoRenewPlan:
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoRenewPlan rnp0001 2284379378 8901260842107740269 476296 W-PRIMARY L59 EN

mwMakeitsoAddMintData
php Ultra/Lib/MiddleWare/Adapter/Control_test.php mwMakeitsoAddMintData testAPI-879-3 5186129768 8901260963163667913 19 W-MINT M01S EN 'A-DATA-MINT|1'
*/

