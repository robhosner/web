<?php


namespace Ultra\Lib\MiddleWare\ACC;


require_once 'db/htt_data_event_log.php';
require_once 'classes/CommandInvocation.php';
require_once 'classes/Result.php';
require_once 'lib/messaging/functions.php';
require_once 'Ultra/Lib/MiddleWare/Adapter/Control.php';
require_once 'Ultra/Lib/MQ/EndPoint.php';
require_once 'Ultra/Lib/CustomerNotification.php';
require_once 'Ultra/Lib/Services/UserDeviceAPI.php';
require_once 'Ultra/Configuration/Configuration.php';



/**
 * Main class for ACC Notifications Middleware Logic
 *
 * Notification* classes contain ACC specific logic.
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class Notification
{
  private $actionUUID;
  private $command;

  function __destruct()
  {
  }

  /**
   * \Ultra\Lib\MiddleWare\ACC\Notification constructor
   */
  public function __construct()
  {
  }

  /**
   * processNotification
   *
   * Process an inbound notification at MiddleWare level
   *
   * @return object of class \Result
   */
  public function processNotification($params)
  {
    dlog('',"params = %s",$params);

    $this->actionUUID = $params['actionUUID'];
    $this->command    = $params['command'];
    $parameters       = $params['parameters'];

    $parameters = $this->mapParameters($parameters);

    $notificationClass = '\Ultra\Lib\MiddleWare\ACC\Notification'.$this->command;

    $result = new \Result();

    if ( class_exists( $notificationClass ) )
    {
      dlog('',"processNotification class = $notificationClass");

      # Factory
      $notificationObject = new $notificationClass( $this->actionUUID );

      $result = $notificationObject->processNotification( (array) $parameters );
    }
    else
    {
      \logWarn( __CLASS__ . " notification ".$params['command']." not handled" );

      $result->add_error( __CLASS__ . " notification ".$params['command']." not handled" );
    }

    return $result;
  }

  /**
   * divertNotification
   *
   * redirect notifications to another endpoint
   *
   * @param  String $wsdl
   * @param  String $location endpoint
   * @param  String $command
   * @param  Array  $params
   * @return Object Result
   */
  public static function divertNotification($wsdl, $location, $command, $params)
  {
    $result = new \Result();

    if (empty($wsdl) || empty($location) || empty($command) || empty($params))
    {
      $result->add_error('Some required parameters missing or empty');
      return $result;
    }

    \logDebug("Using WSDL: $wsdl , LOCATION: $location , COMMAND: $command , PARAMS: " . json_encode($params));

    $soap = array(
      'location' => $location,
      'stream_context' => stream_context_create(array(
        'ssl' => array(
          'verify_peer'       => FALSE,
          'verify_peer_name'  => FALSE,
          'allow_self_signed' => TRUE
        ))
      )
    );

    $client = new \SoapClient($wsdl, $soap);
    $capabilities = $client->__getFunctions();

    if ( ! $params = prepareSoapParameters($command, array('parameters' => $params), $capabilities))
    {
      $result->add_error('unable to prepare SOAP parameters');
      return $result;
    }

    \logDebug('PREPARED PARAMS: ' . json_encode($params));

    $params = self::cleanEmptyArrays($params);

    \logDebug('CLEAN PARAMS: ' . json_encode($params));

    // make soap call, get response
    $response = $client->__soapCall($command, $params);
    if ( ! empty($response))
    {
      \logDebug('RESPONSE: ' . json_encode($response));
      $result->add_data_array((array)$response);
      $result->succeed();
    }

    return $result;
  }

  /**
   * cleanEmptyArrays
   *
   * replaces empty arrays in array with empty strings
   *
   * @param  array
   * @return array
   */
  public static function cleanEmptyArrays(&$data)
  {
    foreach ($data as &$entry)
    {
      if (is_array($entry))
      {
        if ( ! count($entry))
          $entry = '';
        else
          self::cleanEmptyArrays($entry);
      }
    }

    return $data;
  }

  /**
   * mapParameters
   *
   * Mapping / translation layer
   *
   * @return array
   */
  private function mapParameters($parameters)
  {
    dlog('',"mapParameters input = %s",$parameters);

    if ( is_object($parameters) )
      $parameters = (array)$parameters;

    // PROD-1687: check and normalize MSISDN if needed
    $length = strlen($parameters['MSISDN']);
    if ($length == 11 || $length == 15) // internationalized regular or fake (orange SMS activation notification) MSISDN
    {
      $msisdn = substr($parameters['MSISDN'], 1);
      dlog('', "corrected internationalized MSISDN {$parameters['MSISDN']} -> $msisdn");
      $parameters['MSISDN'] = $msisdn;
    }
    elseif ($length != 10 && $length != 14)
      dlog('', "ERROR: invalid MSISDN {$parameters['MSISDN']} length $length");

    return $parameters;
  }
}

interface NotificationInterface
{
  public function processNotification($parameters);
}

class NotificationBase
{
  protected $actionUUID;
  protected $result;

  function __destruct()
  {
  }

  /**
   * \Ultra\Lib\MiddleWare\ACC\NotificationBase constructor
   */
  public function __construct($actionUUID)
  {
    $this->actionUUID = $actionUUID;
    $this->endPoint   = new \Ultra\Lib\MQ\EndPoint;
    $this->result     = new \Result();
  }

  /**
   * enqueueNotification
   *
   * Enqueue $message in the ULTRA MW Notification Channel
   *
   * @return object of class \Result
   */
  protected function enqueueNotification($message)
  {
    dlog('',"%s",$message);

    // enqueue $message in the ULTRA MW Notification Channel
    $notificationUUID = $this->endPoint->enqueueNotificationChannel($message);

    if ( $notificationUUID )
      $this->result->succeed();
    else
    {
      $this->result->add_error( __CLASS__ . " enqueueNotificationChannel failed" );
      $this->result->fail();
    }

    return $this->result;
  }

  /**
   * sendSMS
   *
   * Send an SMS using messaging__SendSMS
   * Example:
   *   curl -i 'https://mw-aspider-prod.ultra.me/pr/messaging/2/ultra/api/messaging__SendSMS'
   *        -u 'ultra_mw_prod:f879d47151f35dec!'
   *        -d 'access_token=f36b06e7Z2333497bfP2f29dX8N694e5&sms_text=a&msisdn=3238080808'
   *
   * @return NULL
   */
  protected function sendSMS( $msisdn , $message )
  {
    dlog('',"%s",$message);

    if ( \Ultra\UltraConfig\isDevelopmentDB() )
    {
      dlog('',"This feature is disabled in development");

      return NULL;
    }

    list( $internal_domain , $internal_user , $internal_password ) = \Ultra\UltraConfig\ultra_api_internal_info();

    dlog('',"internal_domain = $internal_domain , internal_user = $internal_user");

    $curl_options = array(
      CURLOPT_USERPWD  => $internal_user.':'.$internal_password,
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );

    $url = 'https://'.$internal_domain.'/pr/messaging/2/ultra/api/messaging__SendSMS';

    $params = array(
      'access_token' => 'f36b06e7Z2333497bfP2f29dX8N694e5', // TODO: configure this in ht.cf
      'sms_text'     => $message,
      'msisdn'       => $msisdn,
      'partner_tag'  => __CLASS__
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    dlog('',"json_result = %s",$json_result);

    return NULL;
  }
}


/**
 * Main class for ACC inbound NotificationReceived API
 *
 * If ShortCode = 6700, treat at SMS.
 * If ShortCode = nil , treat as USSD.
 * USSD => NOOP
 * SMS  => Any SMS message received should be passed to an internal API method which will return a message to the subscriber.
 */
class NotificationNotificationReceived extends NotificationBase implements NotificationInterface
{
  public function processNotification($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'NotificationReceived';

    $errors = $this->validateNotificationReceivedParams( $parameters , $command );

    if ( count($errors) )
      return \make_error_Result( $errors );

    // exit on SG123
    if (isset($parameters['serviceGrade']) && $parameters['serviceGrade'] == 'SG123')
    {
      // sometimes this notification contains IMSI instead of ICCID... ignore those
      if (false && strlen($parameters['smsText']) < 18) {
        \logit('skipping IMSI SG123 notification');
      } else {
        $redis    = new \Ultra\Lib\Util\Redis();
        $redisKey = "apnsettings/{$parameters['MSISDN']}/{$parameters['smsText']}";
        $redisVal = $redis->get($redisKey);

        if ($redisVal)
        {
          $redis->incr($redisKey);
          $redisVal += 1;
        }
        else
        {
          $redis->set($redisKey, 1, 60*60*24);
          $redisVal = 1;
        }

        if ($redisVal > 2)
        {
          \logError("{$paramters['MSISDN']} has received the maximum amount of apn updates in 24 hours");
          return \make_ok_Result();
        }

        // save info on user device API
        // smsText = ICCID, messageType = IMEI
        $userDeviceAPI = new \Ultra\Lib\Services\UserDeviceAPI(new \Ultra\Configuration\Configuration());
        $userDeviceResult = $userDeviceAPI->setDeviceInfo(
          $parameters['MSISDN'],
          $parameters['smsText'],
          $parameters['messageType']
        );
        if (!$userDeviceResult->is_success()) {
          \logError('Failed to save user device information on userdevice-api');
        } else if (!empty($userDeviceResult->data_array['new_device'])) {
          // send APN SMS
          $userDeviceResult = $userDeviceAPI->sendApnSettings($parameters['MSISDN']);
          if (!$userDeviceResult->is_success()) {
            \logError('Failed to send APN SMS');
          }
        }

        // $parameters['smsText'] = 'Setup';
      }
      
      // \logit('serviceGrade is SG123, exiting before CustomerNotification');
      return \make_ok_Result();
    }


    return ( $parameters['shortCode'] )
      ? $this->processNotificationSMS($parameters)
      : $this->processNotificationUSSD($parameters);

/*
{
   "shortCode":"6700",
   "MSISDN":"2172206891",
   "smsText":"3rd text",
   "serviceGrade":"SG801",
   "messageType":"ASCII",
   "UserData":{
      "senderId":"MVNEACC",
      "timeStamp":"2014-02-07T08:24:25.902"
   }
}
*/
  }

  private function processNotificationSMS($parameters)
  {
    // AMDOCS-276 - [6700] reply with "Customer Self Care is currently unavailable. Please call 611 for any questions."

/*
    // enqueue an inbound message, the adapter level will perform the appropriate action
    $message = $this->endPoint->buildMessage(
      array(
        'actionUUID' => $this->actionUUID,
        'header'     => 'NotificationReceived',
        'body'       => array(
          'success'    => TRUE,
          'errors'     => array(),
          'shortCode'  => '6700',
          'msisdn'     => $parameters['MSISDN']
        )
      )
    );

    // send up to Ultra/Adapter layer
    return $this->enqueueNotification($message);
*/

    // PJW-61: handle SMS notifications to 6700 via CustomerNotification class
    $handler = new \Ultra\Lib\CustomerNotification;
    $outcome = $handler->execute($parameters);

    // log result but there is little we can do about errors at this level
    $msg = "SMS notification from {$parameters['MSISDN']} with text '{$parameters['smsText']}'";
    if ($outcome->is_success())
      logInfo("successfully handled $msg");
    else
      logError("failed to handle $msg, error " . json_encode($outcome->get_errors()));

    return \make_ok_Result();
  }

  private function processNotificationUSSD($parameters)
  {
    return \make_error_Result("$command does NOOP for USSD");
  }

  private function validateNotificationReceivedParams( $parameters , $command )
  {
    $errors = array();

    // not really an error ...
    if ( !isset($parameters['UserData']) || !$parameters['UserData'] )
      dlog('','Warning: UserData not defined in '.$command.' callback');

    if ( !isset($parameters['MSISDN']) || !$parameters['MSISDN'] )
    {
      $errors[] = 'Missing MSISDN in '.$command.' callback';
    }
    else
    {
      if ( ! in_array(strlen($parameters['MSISDN']), array(10, 14)) || ! preg_match('/^[0-9]*$/', $parameters['MSISDN']))
        $errors[] = 'Invalid MSISDN '.$parameters['MSISDN'].' in '.$command.' callback';
    }

    if ( !isset($parameters['smsText']) || !$parameters['smsText'] )
      $errors[] = 'Missing smsText in '.$command.' callback';

    if ( isset($parameters['ShortCode']) && $parameters['ShortCode'] && ( $parameters['ShortCode'] != '6700' ) )
      $errors[] = 'Invalid ShortCode '.$parameters['ShortCode'].' in '.$command.' callback';

    return $errors;
  }
}


# PortOutDeactivation
class NotificationPortOutDeactivation extends NotificationBase implements NotificationInterface
{
  public function processNotification($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'PortOutDeactivation';

    $errors = $this->validatePortOutDeactivationParams( $parameters );

    if ( count($errors) )
      return \make_error_Result( $errors );

    #TODO: 

    return \make_error_Result("$command callback not implemented yet");
  }

  private function validatePortOutDeactivationParams($parameters)
  {
    $errors = array();

    return $errors;
  }
}


# PortOutRequest
class NotificationPortOutRequest extends NotificationBase implements NotificationInterface
{
  public function processNotification($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'PortOutRequest';

    $errors = $this->validatePortOutRequestParams( $parameters , $command );

    if ( count($errors) )
      return \make_error_Result( $errors );

    #TODO: 

    return \make_error_Result("$command callback not implemented yet");
  }

  private function validatePortOutRequestParams( $parameters , $command )
  {
    $errors = array();

    // not really an error ...
    if ( !isset($parameters['UserData']) || !$parameters['UserData'] )
      dlog('','Warning: UserData not defined in '.$command.' callback');

    if ( !isset($parameters['MSISDN']) || !$parameters['MSISDN'] )
      $errors[] = 'Missing MSISDN in '.$command.' callback';

    if ( !preg_match("/^[0-9]{10,11}$/", $parameters['MSISDN']))
      $errors[] = 'Invalid MSISDN '.$parameters['MSISDN'].' in '.$command.' callback';

    if ( !isset($parameters['accountNumber']) || !$parameters['accountNumber'] )
      $errors[] = 'Missing accountNumber in '.$command.' callback';

    return $errors;
  }
}


# ProvisioningStatus
#   Asynch notification from
#   - * migration *           <ProvisioningType>Activation</ProvisioningType>       TransactionIDAnswered starts with MIG - AMDOCS-417
#   - ActivateSubscriber      <ProvisioningType>Activation</ProvisioningType>       ( regular activation and Port Complete )
#   - ActivateSubscriber      <ProvisioningType>PortStatusUpdate</ProvisioningType> ( Port Concurrence )
#   - ActivateSubscriber      <ProvisioningType>Reactivation</ProvisioningType>     ( port in activation from TMO or another TMO MVNO )
#   - ChangeMSISDN            <ProvisioningType>ChangeMSISDN</ProvisioningType>
#   - ChangeSIM               <ProvisioningType>UpdateSIM</ProvisioningType>
#   - DeactivateSubscriber    <ProvisioningType>Deactivation</ProvisioningType>
#   - ReactivateSubscriber    <ProvisioningType>Reactivation</ProvisioningType>
#   - RenewPlan               <ProvisioningType>ChangeFeatures</ProvisioningType>
#   - ResendOTA               <ProvisioningType>ResendOTA</ProvisioningType>
#   - ResetVoiceMailPassword  <ProvisioningType>ResetVMPassword</ProvisioningType>
#   - RestoreSubscriber       <ProvisioningType>Restore</ProvisioningType>
#   - SuspendSubscriber       <ProvisioningType>Suspend</ProvisioningType>
#   - UpdatePlanAndFeatures   <ProvisioningType>ChangeFeatures</ProvisioningType>
#   - UpdateSubScriberAddress <ProvisioningType>UpdateAddress</ProvisioningType>
#   - UpdateWholesalePlan     <ProvisioningType>ChangeFeatures</ProvisioningType>
#   - ManageCap               <ProvisioningType>ManageCap</ProvisioningType>
class NotificationProvisioningStatus extends NotificationBase implements NotificationInterface
{
  public function processNotification($parameters)
  {
    dlog('',"%s",$parameters);

    // error handling / sanity checks
    $errors = $this->validateProvisioningStatusParams( $parameters );

    if ( count($errors) )
      return \make_error_Result( $errors );

    $methodName = 'processNotification'.$parameters['ProvisioningType'];

    if ( ! method_exists( $this, $methodName) )
      return \make_error_Result("ProvisioningStatus callback not implemented for ProvisioningType = ".$parameters['ProvisioningType']);

    // PortStatusUpdate does not need a CommandInvocation object
    if ( $parameters['ProvisioningType'] == 'PortStatusUpdate' )
      return $this->$methodName($parameters);

    \teldata_change_db();

    // verify that there is a customer associated with the given MSISDN
    $customers = \get_ultra_customers_from_msisdn($parameters['MSISDN'],array('customer_id'));

    if ( !$customers || !is_array($customers) || !count($customers) )
      $parameters['customer_id'] = '';
    elseif( count($customers) > 1 )
    {
      dlog('','ESCALATION IMMEDIATE MSISDN '.$parameters['MSISDN'].' is associated with more than one Ultra customer' );

      return \make_error_Result( 'MSISDN '.$parameters['MSISDN'].' is associated with more than one Ultra customer' );
    }
    else
      $parameters['customer_id'] = $customers[0]->customer_id;

    if ( !$parameters['customer_id'] )
    {
      // cancelled customers have their MSISDN stored in HTT_CANCELLATION_REASONS

      $customer_ids = \get_cancellation_customer_ids_by_msisdn( $parameters['MSISDN'] );

      if ( $customer_ids && is_array( $customer_ids ) && count( $customer_ids ) )
        if ( count( $customer_ids ) == 1 )
        {
          dlog('','MSISDN '.$parameters['MSISDN'].' is associated with Cancelled customer id '.$customer_ids[0] );

          $parameters['customer_id'] = $customer_ids[0];
        }
        else
          dlog('','ESCALATION IMMEDIATE MSISDN '.$parameters['MSISDN'].' is associated with more than one cancelled customer' );
    }

    $commandInvocationObject = new \CommandInvocation();

    $result = NULL;

    if ( $parameters['customer_id'] )
      // we found a customer id associated with $parameters['MSISDN']
      dlog('','MSISDN = '.$parameters['MSISDN'].' ; CUSTOMER_ID = '.$parameters['customer_id']);
    elseif( $parameters['ProvisioningType'] != 'ChangeMSISDN' )
    {
      // we could not find a customer id associated with $parameters['MSISDN'] from HTT_CUSTOMERS_OVERLAY_ULTRA,
      // we now attempt to get customer_id from COMMAND_INVOCATIONS using CorrelationID ( CORRELATION_ID )
      if ( isset( $parameters['CorrelationID'] ) && $parameters['CorrelationID'] )
      {
        dlog('','CorrelationID = '.$parameters['CorrelationID']);

        $result = $commandInvocationObject->loadOpenByCorrelationID( $parameters['CorrelationID'] );

        if ( $result->is_failure() )
        {
          dlog('',"loadOpenByCorrelationID failed for CorrelationID ".$parameters['CorrelationID']);

          return $result;
        }
        elseif ( $result->is_pending() )
        {
          dlog('',"found pending command invocation for CorrelationID ".$parameters['CorrelationID']);

          $parameters['customer_id'] = ( property_exists( $commandInvocationObject , 'customer_id' ) ) ? $commandInvocationObject->customer_id : NULL ;
        }
        else
        {
          dlog('',"result = %s",$result);
          dlog('',"commandInvocationObject = %s",$commandInvocationObject);

          return \make_error_Result(
            'MSISDN '.$parameters['MSISDN'].' is not associated with an Ultra customer ; '.
            'CorrelationID '.$parameters['CorrelationID']. ' is not associated with an incomplete command invocation'
          );
        }

        // inconsistency: we have a row in COMMAND_INVOCATIONS without CUSTOMER_ID
        if ( ! $parameters['customer_id'] )
          return \make_error_Result( 'CorrelationID '.$parameters['CorrelationID']. ' is not associated with an Ultra customer' );
      }
      else
      {
        dlog('',"no CorrelationID available");

        return \make_error_Result( 'MSISDN '.$parameters['MSISDN'].' is not associated with an Ultra customer' );
      }
    }

    // notifications are sometimes faster than our DB
    sleep( 2 );

    if ( $parameters['ProvisioningType'] == 'ChangeMSISDN' )
    {
      // we have to retrieve the customer_id from the originating ChangeMSISDN call

      $result = $commandInvocationObject->loadOpenByCommand( 'ChangeMSISDN' , $parameters['TransactionIDAnswered'] );

      $parameters['customer_id'] = ( property_exists( $commandInvocationObject , 'customer_id' ) ) ? $commandInvocationObject->customer_id : NULL ;

      dlog('','MSISDN = '.$parameters['MSISDN'].' ; CUSTOMER_ID = '.$parameters['customer_id']);
    }
    elseif( is_null($result) )
      // we now attempt to load COMMAND_INVOCATIONS data using customer_id
      $result = $commandInvocationObject->loadOpenByCustomerId( $parameters['customer_id'] );

# TODO: should 'command' be checked?
# TODO: should 'CORRELATION_ID' be consistent?

    dlog('',"CommandInvocation::load result = %s ; commandInvocationObject = %s",$result,$commandInvocationObject);

    if ( $result->is_failure() )
      return $result;
    elseif ( $result->is_pending() )
    {
      // it may be a completed port
      if (in_array($parameters['ProvisioningType'], array('Activation', 'Reactivation')))
      {
        // load the latest port attempt for the given msisdn (if any)
        $portInQueue = new \PortInQueue();

        $loadPortInQueueResult = $portInQueue->loadByMsisdn( $parameters['MSISDN'] );

        dlog('',"portInQueue = %s",$portInQueue);

        // is there an incomplete port attempt for the given msisdn?
        if ( $loadPortInQueueResult->is_success() && $portInQueue->portin_queue_id && ( !property_exists( $portInQueue , 'completed_date_time' ) || !$portInQueue->completed_date_time ) )
        {
          // mark row in PORTIN_QUEUE as COMPLETED
          $portInQueue->endAsSuccess();

          // enqueue an inbound message, the adapter level will perform the state transition [ Port-In Requested ] => [ Active ]
          $message = $this->endPoint->buildMessage(
            array(
              'actionUUID' => $this->actionUUID,
              'header'     => 'PortStatusUpdate',
              'body'       => array(
                'success'       => TRUE,
                'errors'        => array(),
                'customer_id'   => $parameters['customer_id'],
                'msisdn'        => $parameters['MSISDN'],
                'soap_log_id'   => $parameters['soap_log_id']
              )
            )
          );

          // send up to Ultra/Adapter layer
          $enqueueResult = $this->enqueueNotification($message);

          // PROD-2279: treat port in Reactivation from TMO or another MVNO as a regular port in Activation
          // overwride method since processNotificationReactivation is a callback for ReactivateSubscriber ACC API
          $methodName = 'processNotificationActivation';

          // close Command Invocation
          return $this->$methodName($parameters,$commandInvocationObject);
        }
        else
        {
          dlog('',"There are no incomplete port attempt for MSISDN ".$parameters['MSISDN']);

          return $this->$methodName($parameters,$commandInvocationObject);
        }
      }
      else
        return $this->$methodName($parameters,$commandInvocationObject);
    }
    else
      return \make_error_Result( 'MSISDN '.$parameters['MSISDN'].' is not associated with an OPEN Command Invocation' );
  }

  /**
   * validateProvisioningStatusParams
   *
   * @return array
   */
  private function validateProvisioningStatusParams($parameters)
  {
/*
            <s:element name="UserData" minOccurs="1" nillable="false" maxOccurs="1" type="tns:UserDataObj" />
            <s:element name="MSISDN" minOccurs="1" nillable="false" maxOccurs="1" type="s:string" />
            <s:element name="TransactionIDAnswered" minOccurs="1" nillable="false" maxOccurs="1" type="s:string" />
            <s:element name="ProvisioningType" minOccurs="1" nillable="false" maxOccurs="1" type="s:string" />
            <s:element name="ProvisioningStatus" minOccurs="1" nillable="false" maxOccurs="1" type="s:boolean" />
*/
    $errors = array();

    // not really an error ...
    if ( empty($parameters['UserData']) || !$parameters['UserData'] )
      dlog('','Warning: UserData not defined in ProvisioningStatus callback');

    if ( empty($parameters['MSISDN']) || !$parameters['MSISDN'] )
      $errors[] = 'Missing MSISDN in ProvisioningStatus callback';
    elseif ( !preg_match("/^[0-9]{10,11}$/", $parameters['MSISDN']))
      $errors[] = 'Invalid MSISDN '.$parameters['MSISDN'].' in ProvisioningStatus callback';

    if ( !isset($parameters['TransactionIDAnswered']) || !$parameters['TransactionIDAnswered'] )
      $errors[] = 'Missing TransactionIDAnswered in ProvisioningStatus callback';

    if ( !isset($parameters['ProvisioningType']) || !$parameters['ProvisioningType'] )
      $errors[] = 'Missing ProvisioningType in ProvisioningStatus callback';

    if ( !isset($parameters['ProvisioningStatus']) || !$parameters['ProvisioningStatus'] )
      $errors[] = 'Missing ProvisioningStatus in ProvisioningStatus callback';

    return $errors;
  }

  /**
   * processNotificationPortStatusUpdate
   *
   * Asynch response for Amdocs command ActivateSubscriber (Port)
   *
   * @return object of class \Result
   */
  private function processNotificationPortStatusUpdate($parameters)
  {
    dlog('',"%s",$parameters);

    $portInQueue = new \PortInQueue();

    $result = $portInQueue->loadByMsisdn( $parameters['MSISDN'] );

    if ( $result->is_failure() )
    {
      dlog('','ERROR ESCALATION ALERT PORT : We could not find a port in attempt for msisdn '.$parameters['MSISDN']);
      return \make_error_Result('We could not find a port in attempt for msisdn '.$parameters['MSISDN']);
    }
    else
    {
      dlog('',"%s",$portInQueue);

      if ( empty( $parameters['StatusDescription'] ) )
      {
        dlog('',"ERROR ESCALATION ALERT DAILY - missing StatusDescription in PortStatusUpdate notification");

        $parameters['StatusDescription'] = '';
      }

      // connect to MW DB
      \Ultra\Lib\DB\ultra_acc_connect();

      if ( $parameters['StatusDescription'] == 'PORT_RESOLUTION_REQUIRED' ) 
        // PROD-827 : send a SMS to the customer if additional info are needed
        $this->sendResolutionRequiredSMS( $parameters['MSISDN'] );

      $updateParams = array(
        'provstatus_description' => $parameters['StatusDescription']
      );

      if ( isset($parameters['PortErrorCode']) && $parameters['PortErrorCode'] )
        $updateParams['provstatus_error_code'] = $parameters['PortErrorCode'];

      if ( isset($parameters['PortErrorDesc']) && $parameters['PortErrorDesc'] )
        $updateParams['provstatus_error_msg'] = $parameters['PortErrorDesc'];

      return $portInQueue->updateOnProvisioningStatus( $updateParams );
/*
ERROR: ProvisioningType is PortStatusUpdate
      <MSISDN>8037154849</MSISDN>
      <TransactionIDAnswered>API-ULTRA-76170003304</TransactionIDAnswered>
      <ProvisioningType>PortStatusUpdate</ProvisioningType>
      <ProvisioningStatus>true</ProvisioningStatus>
      <StatusDescription>PORT_RESOLUTION_REQUIRED</StatusDescription>
      <CorrelationID></CorrelationID>
      <PortErrorCode>8E</PortErrorCode>
      <PortErrorDesc>First name required or incorrect</PortErrorDesc>

CONCURRENCE: ProvisioningType is PortStatusUpdate
      <MSISDN>8037154849</MSISDN>
      <TransactionIDAnswered></TransactionIDAnswered>
      <ProvisioningType>PortStatusUpdate</ProvisioningType>
      <ProvisioningStatus>true</ProvisioningStatus>
      <StatusDescription>PORT_CONCURRENCE</StatusDescription>
      <CorrelationID></CorrelationID>
      <PortErrorCode></PortErrorCode>
      <PortErrorDesc></PortErrorDesc>
*/
    }
  }

  /**
   * sendResolutionRequiredSMS
   *
   * Send this message only once per 3 days per msisdn
   *
   * @return NULL
   */
  private function sendResolutionRequiredSMS( $msisdn )
  {
    \teldata_change_db();

    // get $customer_id
    // PROD-827: at this time subsriber exists in HTT_CUSTOMERS_OVERLAY_ULTRA only
    $customer_id = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'customer_id', NULL, 'HTT_CUSTOMERS_OVERLAY_ULTRA');

    if ( ! $customer_id )
    {
      dlog('',"No customer id associated with msisdn $msisdn");

      \Ultra\Lib\DB\ultra_acc_connect();

      return NULL;
    }

    // get HTT_ACTIVATION_HISTORY.DEALER
    $dealer = \Ultra\Lib\DB\Getter\getScalar('ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID', $customer_id, 'DEALER');

    $umact_list = array(34,23,28);

    $reason = ( in_array( $dealer , $umact_list ) ) ? 'port_resolution_required_umact' : 'port_resolution_required' ;

    // send SMS to customer to current mobile provider
    $result = enqueue_once_immediate_external_sms(
      $customer_id,
      $reason,
      array(),
      (24*3)
    );

    if ( $result->is_success() )
      dlog('',"$customer_id - $reason - enqueue_once_immediate_external_sms OK");
    else
      dlog('',"$customer_id - $reason - enqueue_once_immediate_external_sms ERRORS: ".json_encode($return->get_errors()));

    // check to send to contact phone
    $contactPhone = \get_customer_contact_phone($customer_id);
    if (!empty($contactPhone)) {
      $result = enqueue_immediate_external_sms(
        $customer_id,
        $reason,
        ['_to_msisdn' => $contactPhone]
      );

      if ( $result->is_success() )
        dlog('',"$customer_id - $reason - enqueue_immediate_external_sms OK");
      else
        dlog('',"$customer_id - $reason - enqueue_immediate_external_sms ERRORS: ".json_encode($return->get_errors()));
    }

    \Ultra\Lib\DB\ultra_acc_connect();

    return NULL;
  }

  /**
   * processNotificationActivation
   *
   * Asynch response for Amdocs command ActivateSubscriber (not Port)
   * I guess this is a success : <ProvisioningStatus>true</ProvisioningStatus>
   *
   * @return object of class \Result
   */
  private function processNotificationActivation($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationActivationSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationActivationFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationActivationSuccess
   *
   * Successful Asynch response for Amdocs command ActivateSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationActivationSuccess($parameters,$commandInvocationObject)
  {
    // handle PortInMvne1To2 case
    if ( $parameters['customer_id'] 
      && $commandInvocationObject->customer_id
      && ( $commandInvocationObject->command == 'PortInMvne1To2' )
      && ( $commandInvocationObject->status  == 'OPEN' )
    )
      $this->sendSMS(
        $parameters['MSISDN'],
        'Your new SIM has different settings than your old SIM. Please SMS HELP to 6700 instead of 7770. Your voicemail will also need to be reconfigured.' 
      );

    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationActivationFailure
   *
   * Failed Asynch response for Amdocs command ActivateSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationActivationFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationChangeFeatures
   *
   * Asynch response for Amdocs commands RenewPlan, UpdatePlanAndFeatures, UpdateWholesalePlan
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationChangeFeatures($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationChangeFeaturesSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationChangeFeaturesFailure($parameters,$commandInvocationObject);
  }

  private function processNotificationChangeFeaturesSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  private function processNotificationChangeFeaturesFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationManageCap
   *
   * Asynch response for Amdocs commands ManageCap
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationManageCap($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationManageCapSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationManageCapFailure($parameters,$commandInvocationObject);
  }

  private function processNotificationManageCapSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  private function processNotificationManageCapFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationChangeMSISDN
   *
   * Asynch response for Amdocs command ChangeMSISDN
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationChangeMSISDN($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationChangeMSISDNSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationChangeMSISDNFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationChangeMSISDNSuccess
   *
   * Successful Asynch response for Amdocs command ChangeMSISDN
   *
   * @return object of class \Result
   */
  private function processNotificationChangeMSISDNSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationChangeMSISDNFailure
   *
   * Failed Asynch response for Amdocs command ChangeMSISDN
   *
   * @return object of class \Result
   */
  private function processNotificationChangeMSISDNFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationDeactivation
   *
   * Asynch response for Amdocs command DeactivateSubscriber
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationDeactivation($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationDeactivationSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationDeactivationFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationDeactivationSuccess
   *
   * Successful Asynch response for Amdocs command DeactivateSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationDeactivationSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationDeactivationFailure
   *
   * Failed Asynch response for Amdocs command DeactivateSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationDeactivationFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationReactivation
   *
   * Asynch response for Amdocs command ReactivateSubscriber
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationReactivation($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationReactivationSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationReactivationFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationReactivationSuccess
   *
   * Successful Asynch response for Amdocs command ReactivateSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationReactivationSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationReactivationFailure
   *
   * Failed Asynch response for Amdocs command ReactivateSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationReactivationFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  private function processNotificationResendOTA($parameters)
  {
    dlog('',"%s",$parameters);

    return \make_error_Result("ProvisioningStatus ResendOTA callback not implemented yet");
  }

  private function processNotificationResetVMPassword($parameters)
  {
    dlog('',"%s",$parameters);

    return \make_error_Result("ProvisioningStatus ResetVMPassword callback not implemented yet");
  }

  /**
   * processNotificationRestore
   *
   * Asynch response for Amdocs command RestoreSubscriber
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationRestore($parameters,$commandInvocationObject)
  {
    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationRestoreSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationRestoreFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationRestoreSuccess
   *
   * Successful Asynch response for Amdocs command RestoreSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationRestoreSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationRestoreFailure
   *
   * Failed Asynch response for Amdocs command RestoreSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationRestoreFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationSuspend
   *
   * Asynch response for Amdocs command SuspendSubscriber
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationSuspend($parameters,$commandInvocationObject)
  {
    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationSuspendSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationSuspendFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationSuspendSuccess
   *
   * Successful Asynch response for Amdocs command SuspendSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationSuspendSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationSuspendFailure
   *
   * Failed Asynch response for Amdocs command SuspendSubscriber
   *
   * @return object of class \Result
   */
  private function processNotificationSuspendFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /**
   * processNotificationUpdateSIM
   *
   * Asynch response for Amdocs command ChangeSIM
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationUpdateSIM($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationUpdateSIMSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationUpdateSIMFailure($parameters,$commandInvocationObject);
  }

  /**
   * processNotificationUpdateSIMSuccess
   *
   * @return object of class \Result
   */
  private function processNotificationUpdateSIMSuccess($parameters,$commandInvocationObject)
  {
    // we have to close successfully the Command Invocation (C3)
    return $commandInvocationObject->endAsSuccess();
  }

  /**
   * processNotificationUpdateSIMFailure
   *
   * Failed Asynch response for Amdocs command ChangeSIM
   *
   * @return object of class \Result
   */
  private function processNotificationUpdateSIMFailure($parameters,$commandInvocationObject)
  {
    // we have to update with error the Command Invocation (M2)
    return $commandInvocationObject->failOnAsync(
      array(
        'error_code_and_message' => $parameters['PortErrorCode'] . ' - ' . $parameters['PortErrorDesc']
      )
    );
  }

  /*
   * processNotificationUpdateAddress
   *
   * Asynch response for Amdocs commands UpdateAddress
   * I assume <ProvisioningStatus>true</ProvisioningStatus> means success
   *
   * @return object of class \Result
   */
  private function processNotificationUpdateAddress($parameters,$commandInvocationObject)
  {
    dlog('',"%s",$parameters);

    if ( $parameters['ProvisioningStatus'] )
      return $this->processNotificationChangeFeaturesSuccess($parameters,$commandInvocationObject);
    else
      return $this->processNotificationChangeFeaturesFailure($parameters,$commandInvocationObject);
  }
}


# ThrottlingAlert
class NotificationThrottlingAlert extends NotificationBase implements NotificationInterface
{
  public function processNotification($parameters)
  {
    dlog('',"%s",$parameters);

    $errors = $this->validateThrottlingAlertParams( $parameters );

    if ( count( $errors ) )
      return \make_error_Result( $errors );

    // get subscriber from MSISDN
    teldata_change_db();
    if ( ! $customer = get_ultra_customer_from_msisdn($parameters['MSISDN']))
      return \make_error_Result("MSISDN {$parameters['MSISDN']} is not associated with an Ultra customer");

    // get plan from subscriber
    $planData = \get_customer_plan($customer->customer_id);
    if ( ! $planData || ! is_array($planData) || empty($planData['plan']))
      return \make_error_Result("Cannot retrieve customer plan for MSISDN {$parameters['MSISDN']}");

    // get current DATA botlons
    $boltOns = getCurrentTrackedBoltOns($customer->customer_id, NULL,'DATA');

    // dont send flex customer notifications for b-data-block or if flex customer and notification doesnt contain BAN
    if (
      (\Ultra\Lib\Flex::isFlexPlan($planData['plan']) && $parameters['CounterName'] == 'Base Data Usage') ||
      (\Ultra\Lib\Flex::isFlexPlan($planData['plan']) && (stripos($parameters['CounterName'], "BAN") === false))
    ) {
      return \make_ok_Result();
    }

    // map SOC if necessary and log event
    if ($parameters['CounterName'] == 'Base Data Usage')
      if ($baseSocName = \Ultra\MvneConfig\getPlanBaseDataSoc($planData['plan']))
        $parameters['CounterName'] = $baseSocName;
    $this->logDataEvent($parameters, $customer->customer_id);

    // send message to the customer according to the associated event and data bucket
    if (\Ultra\Lib\Util\validateMintBrandId($customer->BRAND_ID))
    {
      return $this->processNotificationThrottlingAlertMessagesMint(
        $customer,
        $planData['plan'],
        $parameters['Event'],
        $parameters['CounterName'],
        $parameters['Value'],
        $boltOns
      );
    }
    else
    {
      $method = \Ultra\Lib\Flex::isFlexPlan($planData['plan'])
        ? 'processNotificationThrottlingAlertMessagesFlex'
        : 'processNotificationThrottlingAlertMessages';

      $method = (\Ultra\UltraConfig\isABPlan($planData['cos_id']))
        ? 'processNotificationThrottlingAlertMessagesABPlans'
        : $method;

      return $this->{$method}(
        $customer,
        $planData['plan'],
        $parameters['Event'],
        $parameters['CounterName'],
        $parameters['Value'],
        $boltOns,
        $planData['cos_id']
      );
    }
  }

  private function processNotificationThrottlingAlertMessagesABPlans(
    $customer,
    $plan,
    $event,
    $counterName,
    $counterValue,
    $boltOns,
    $cos_id
  ) {
    $smsTemplate = NULL;

    // process Active subscribers for events 30 (95% of data) and 80 (100% of data) only
    if ($customer->plan_state != STATE_ACTIVE || ($event != 30 && $event != 80))
      return \make_ok_Result();

    if ( ! in_array($counterName, ['WPRBLK20','WPRBLK33S','WPRBLK35S','WPRBLK39S']))
      return \make_ok_Result();

    $checkBalance = new \Ultra\Lib\CheckBalance();
    $checkBalance->byCustomerId($customer->customer_id);

    if ( ! $checkBalance)
    {
      $error = 'MVNE failure while checking balance';
      \logError($error);
      return make_error_Result($error);
    }

    if ($event == 30)
    {
      if (in_array($counterName, ['WPRBLK20','WPRBLK33S','WPRBLK35S'])) {
        if (\Ultra\UltraConfig\isUnlimitedABPlan($cos_id)) {
          $smsTemplate = $cos_id == '100004' ? 'abplans_4G_95_percent_limited' : 'abplans_4G_95_percent_unlimited';
        } else {
          $smsTemplate = 'abplans_4G_95_percent_limited';
        }
      } else {
        $smsTemplate = 'abplans_3G_95_percent_limited';
      }
    }


    if ($event == 80)
    {
      if (in_array($counterName, ['WPRBLK20','WPRBLK33S','WPRBLK35S'])) {
        if (\Ultra\UltraConfig\isUnlimitedABPlan($cos_id)) {
          $smsTemplate = $cos_id == '100004' ? 'abplans_4G_100_percent_limited' : 'abplans_4G_100_percent_unlimited';
        } else {
          $smsTemplate = 'abplans_4G_100_percent_limited';
        }
      } else {
        $smsTemplate = 'abplans_3G_100_percent_limited';
      }
    }

    // we ignore errors at this point
    if ($smsTemplate)
      funcSendExemptCustomerSMSTemplateNoFail(
        $customer->customer_id,
        $smsTemplate,
        null,
        [ 'login_token' => \Session::encryptToken($customer->current_mobile_number, \Session::TOKEN_V3, array(\Session::SYSTEM_TOKEN)) ]
      );
    else
      logInfo("alert ignored");

    return \make_ok_Result();
  }

  private function processNotificationThrottlingAlertMessagesFlex($customer, $plan, $event, $counterName, $counterValue, $boltOns, $cos_id)
  {
    $smsTemplate = NULL;

    // process Active subscribers for events 30 (95% of data) and 80 (100% of data) only
    if ($customer->plan_state != STATE_ACTIVE || ($event != 30 && $event != 80))
      return \make_ok_Result();

    $redis = new \Ultra\Lib\Util\Redis();
    list(
      $remaining,
      $usage,
      $mintAddOnRemaining,
      $mintAddOnUsage,
      $breakDown,
      $mvneError
    ) = mvneGet4gLTE($this->actionUUID, $customer->current_mobile_number, $customer->customer_id, $redis);

    if ($mvneError)
    {
      \logError($mvneError);
      return make_error_Result($mvneError);
    }

    $flexInfo = \Ultra\Lib\Flex::getInfoByCustomerId($customer->customer_id);
    if ( ! $flexInfo)
      return make_error_Result('FLEX ESCALATION ERROR retrieving customer info');

    $isFamily = ( $flexInfo['memberCount'] > 1 );

    if ($event == 30)
    {
      $smsTemplate = ($isFamily)
        ? 'flex_family_data_95_percent_no_bolt'
        : 'flex_data_95_percent_no_bolt';
    }

    if ($event == 80)
    {
      $smsTemplate = ($isFamily)
        ? 'flex_family_data_100_percent_no_bolt'
        : 'flex_data_100_percent_no_bolt';
    }

    // we ignore errors at this point
    if ($smsTemplate)
      funcSendExemptCustomerSMSTemplateNoFail($customer->customer_id, $smsTemplate);
    else
      logInfo("alert ignored");

    return \make_ok_Result();
  }

  /**
   * processNotificationThrottlingAlertMessagesMint
   *
   * Send message to the MINT customer according to the associated event and data bucket
   *
   * @return object of class \Result
   */
  private function processNotificationThrottlingAlertMessagesMint($customer, $plan, $event, $counterName, $counterValue, $boltOns, $cos_id)
  {
    $smsTemplate = NULL;

    // process Active subscribers for events 30 (95% of data) and 80 (100% of data) only
    if ($customer->plan_state != STATE_ACTIVE || ($event != 30 && $event != 80))
      return \make_ok_Result();

    $redis = new \Ultra\Lib\Util\Redis();
    list(
      $remaining,
      $usage,
      $mintAddOnRemaining,
      $mintAddOnUsage,
      $breakDown,
      $mvneError
    ) = mvneGet4gLTE($this->actionUUID, $customer->current_mobile_number, $customer->customer_id, $redis);

    if ($mvneError)
    {
      \logError($mvneError);
      return make_error_Result($mvneError);
    }

    // notification received on plan data, have no add-on data
    if ($counterName == 'WPRBLK30' && ! $mintAddOnRemaining)
    {
      $smsTemplate = ($event == 30) ? 'data_throttle_95_percent' : 'data_throttle_100_percent';
    }
    elseif ($counterName == 'WPRBLK20' || $counterName == 'WPRBLK25')
    {
      // notification received on add-on data, plan data remains
      if ($remaining)
      {
        // suppress 95% add-on data notification

        if ($event == 80)
          $smsTemplate = 'data_100_plan_next';
      }
      // notification received on add-on data, no remaining plan data
      else
        $smsTemplate = ($event == 30) ? 'data_additional_95_percent' : 'data_additional_100_percent';
    }

    // we ignore errors at this point
    if ($smsTemplate)
      funcSendExemptCustomerSMSTemplateNoFail($customer->customer_id, $smsTemplate);
    else
      logInfo("alert ignored");

    return \make_ok_Result();
  }

  /**
   * processNotificationThrottlingAlertMessages
   *
   * Send message to the customer according to the associated event and data bucket
   *
   * @return object of class \Result
   */
  private function processNotificationThrottlingAlertMessages($customer, $plan, $event, $counterName, $counterValue, $boltOns, $cos_id)
  {
    $smsTemplate = NULL;

    // process Active subscribers for events 30 (95% of data) and 80 (100% of data) only
    // we ignore event 
    if ($customer->plan_state == STATE_ACTIVE && ($event == 30 || $event == 80))
    {
      // is data blocked or throttled?
      $limited = \Ultra\MvneConfig\isLimitedDataPlan($plan);
      logInfo("customer ID {$customer->customer_id}, plan $plan, data " . (($limited) ? 'limited' : 'unlimited') . ', boltons ' . (count($boltOns) ? 'present' : 'absent'));

      // plan data is used up first
      if (in_array($counterName, ['WPRBLK20','WPRBLK33S','WPRBLK35S']))
      {
        if (count($boltOns)) // bolt on data is available next
        {
          $smsTemplate = $event == 80 ? 'data_100_bolt_on_next' : 'data_95_bolt_on_next';
        }
        else // no further data available
        {
          if ($limited)
            $smsTemplate = $event == 30 ? 'data_95_percent' : 'data_100_percent';
          else
            $smsTemplate = $event == 30 ? 'data_throttle_95_percent' : 'data_throttle_100_percent';
        }
      }

      // bolt on data is used up after plan data
      elseif (in_array($counterName, ['WPRBLK30','WPRBLK34S','WPRBLK36S']))
      {
        if ($limited)
          $smsTemplate = $event == 30 ? 'data_95_percent' : 'data_100_percent';
        else // throttled data
          $smsTemplate = $event == 30 ? 'data_throttle_95_percent' : 'data_throttle_100_percent';
      }

      // 256k_v2 soc
      else if (in_array($counterName, ['WPRBLK39S']))
        $smsTemplate = $event == 30 ? 'data_3g_95_percent' : 'data_3g_100_percent';

      // we ignore errors at this point
      if ($smsTemplate)
        funcSendExemptCustomerSMSTemplateNoFail($customer->customer_id, $smsTemplate, null, [ 'login_token' => \Session::encryptToken($customer->current_mobile_number, \Session::TOKEN_V3, array(\Session::SYSTEM_TOKEN)) ] );
      else
        logInfo("alert ignored");
    }

    return \make_ok_Result();
  }

  /**
   * blockMessage
   *
   * @return object of class \Result
   */
  private function blockMessage( $redis , $customer_id )
  {
    \funcSendExemptCustomerSMSData100PercentUsed(
      array( 'customer_id' => $customer_id )
    );

    return \make_ok_Result();
  }

  /**
   * throttleMessage
   *
   * @return object of class \Result
   */
  private function throttleMessage( $redis , $customer_id )
  {
    if ( ! \get_data_recharge_notification_delay( $redis , $customer_id ) ) // no recent data recharge
    {
      \funcSendExemptCustomerSMSDataThrottle100PercentUsed(
        array( 'customer_id' => $customer_id )
      );

      // block duplicate messages for 10 minutes
      \set_data_recharge_notification_delay( $redis , $customer_id , 600 );
    }

    return \make_ok_Result();
  }

  /**
   * logDataEvent
   *
   * @return NULL
   */
  private function logDataEvent($parameters, $customer_id)
  {
    // check required parameters
    if (empty($parameters['CounterName']))
      \logError('Missing CounterName');
    else
    {
      // per API-41 we record SOC name as given by ACC along with the event's value
      $event_status = \log_data_event(
        array(
          'action'      => 'Event ' . $parameters['Event'],
          'customer_id' => $customer_id,
          'soc'         => $parameters['CounterName'],
          'value'       => $parameters['Value'])); // per 'ACC MVNE Partner Facing APIs V 6.1.0' Value is KB
    }

    return NULL; // ignore $event_status
  }

  /**
   * validateThrottlingAlertParams
   *
   * @return array
   */
  private function validateThrottlingAlertParams($parameters)
  {
    $errors = array();

    if ( empty( $parameters['Event'] ) )
      $errors[] = 'Missing Event in ThrottlingAlert callback';
    elseif ( !in_array( $parameters['Event'] , array(20,30,50,80) ) )
      $errors[] = 'Event '.$parameters['Event'].' not handled';

    if ( empty( $parameters['CounterName'] ) )
      $errors[] = 'Missing CounterName in ThrottlingAlert callback';

    if ( empty( $parameters['MSISDN'] ) )
      $errors[] = 'Missing MSISDN in ThrottlingAlert callback';
    elseif ( ! is_numeric( $parameters['MSISDN'] ) )
      $errors[] = 'Invalid MSISDN in ThrottlingAlert callback';

    return $errors;
  }
}

?>
