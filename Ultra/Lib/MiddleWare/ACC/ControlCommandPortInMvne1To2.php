<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * PortInMvne1To2 control command implementation
 */
class ControlCommandPortInMvne1To2 extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'PortInMvne1To2';
    $acc_api = 'ActivateSubscriber';

    $customer_id          = $parameters['customer_id'];
    $msisdn               = $parameters['MSISDN'];
    $zipCode              = NULL;
    $donorAccountNumber   = NULL;
    $donorAccountPassword = NULL;

    if ( isset($parameters['zipCode']) )
      $zipCode              = $parameters['zipCode'];

    if ( isset($parameters['donorAccountNumber']) ) 
      $donorAccountNumber   = $parameters['donorAccountNumber'];

    if ( isset($parameters['donorAccountPassword']) )
      $donorAccountPassword = $parameters['donorAccountPassword'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $acc_api , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addPortInParameters($parameters);

    unset( $parameters['ICCID'] );
    unset( $parameters['MSISDN'] );
    unset( $parameters['wholesalePlan'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['customer_id'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $acc_api , $parameters );

    $result = $this->interactWithChannel( $acc_api , $outboundControlMessage );

    dlog('',"interactWithChannel result = %s",$result);
    dlog('',"interactWithChannel result errors = %s",$result->get_errors());

/*
    if ( ! $result->has_errors() )
      $this->initiatePortIn($parameters,$customer_id,$acc_api,$msisdn,$zipCode,$donorAccountNumber,$donorAccountPassword,$result->data_array);
*/

    return $result;
  }

  private function addPortInParameters($parameters)
  {
    $parameters['numberGroup'] = '';
    $parameters['ReturnURL']   = '';

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'MSISDN',
      'serialNumber'    => $parameters['MSISDN']
    );

    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addPoolAccInd($parameters);
    $parameters = $this->addResourceData($parameters);
    $parameters = $this->addAutoRenew($parameters);
    $parameters = $this->addPlanDataOnActivation($parameters);
    $parameters = $this->addContactData($parameters);

    $parameters['PortInData']['specificationValueName'] = 'PORTTYPE';
    $parameters['PortInData']['specificationValue']     = 'PI'; # regular, i.e. no porting. Otherwise 'PI'

    $parameters['PortInData']['firstName'] = 'Prepaid';
    $parameters['PortInData']['lastName']  = 'Customer';
    $parameters['PortInData']['fullName']  = '';
    $parameters['PortInData']['PortInAddressList']['PortInAddressInfo'][] = array(
      'addressLine1' => '123 Main Street',
      'addressLine2' => '',
      'State'        => 'GA',
      'Zip'          => empty($parameters['zipCode']) ? '30319' : $parameters['zipCode'],
      'City'         => 'Atlanta',
      'Country'      => 'US'
    );

    if ( !isset($parameters['donorAccountNumber']) || ( $parameters['donorAccountNumber'] == '' ) )
      $parameters['PortInData']['donorAccountNumber']   = '12345';
    else
    {
      $parameters['PortInData']['donorAccountNumber'] = $parameters['donorAccountNumber'];
      unset( $parameters['donorAccountNumber'] );
    }

    if ( !isset($parameters['donorAccountPassword']) || ( $parameters['donorAccountPassword'] == '' ) )
      $parameters['PortInData']['donorAccountPassword'] = '1234';
    else
    {
      $parameters['PortInData']['donorAccountPassword'] = $parameters['donorAccountPassword'];
      unset( $parameters['donorAccountPassword'] );
    }

    return $parameters;
  }

}

?>
