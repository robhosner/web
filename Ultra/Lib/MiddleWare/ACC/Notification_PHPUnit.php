<?php

global $credentials;
require_once 'cosid_constants.php';

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

include 'classes/PHPUnitBase.php';

class NotificationTest extends PHPUnitBase
{
  public function setUp()
  {
    teldata_change_db();

    parent::setUp();
  }

  public function test__divertNotification()
  {
    $wsdl = str_replace('production', 'development', \Ultra\UltraConfig\acc_notification_wsdl());
    $wsdl = 'runners/amdocs/' . $wsdl;

    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification();
    $result = $notification->divertNotification(
      $wsdl,
      \Ultra\UltraConfig\getDevMiddlewareEndpoint(),
      'ThrottlingAlert',
      array(
        'UserData'    => array("senderId" => "MVNEACC", "timeStamp" => "2015-12-09T10:20:51"),
        'MSISDN'      => '5555559606',
        'Event'       => 80,
        'Value'       => '990000',
        'CounterName' => 'WPRBLK20'
      )
    );

    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals(100, $result->data_array['ResultCode']);
  }

  public function test__NotificationReceived()
  {
    $command = 'NotificationReceived';

    $params = array(
      'actionUUID' => time() . __function__,
      'command'    => $command,
      'parameters' => array()
    );

    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification();
/*
    $result = $notification->processNotification($params);
    print_r($result);

    $errors = $result->get_errors();
    $this->assertEquals(2, count($errors));
*/
    $params['parameters'] = array(
      'UserData'  => array
      (
        'senderId'      => 'MVNEACC',
        'timeStamp'     => date('Y-m-d\Th:i:0') // 2015-12-18T13:34:16
      ),
      'MSISDN'          => '3476455407',
      'smsText'         => 'test',
      'messageType'     => 'ASCII',
      'shortCode'       => '6700',
      'serviceGrade'    => 'SG801'
    );

    $result = $notification->processNotification($params);
    print_r($result);
    $this->assertEquals(TRUE, $result->is_success());

  }

  public function test__ProvisioningStatus()
  {
    $command = 'ProvisioningStatus';

    $params = array(
      'actionUUID' => time() . __function__,
      'command'    => $command,
      'parameters'  => array()
    );

    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification();
    $result = $notification->processNotification($params);
    print_r($result);
    $errors = $result->get_errors();
    $this->assertEquals(4, count($errors));

    $params['parameters'] = array(
      'UserData'              => array(),
      'MSISDN'                => '3473205862',
      'TransactionIDAnswered' => 'test',
      'ProvisioningType'      => 'Reactivation',
      'ProvisioningStatus'    => TRUE
    );

    $result = $notification->processNotification($params);

    print_r($result);

    $this->assertEquals(TRUE, $result->is_success());
  }

  public function test__ThrottlingAlert()
  {
    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification();
    $params = array(
      'actionUUID' => time() . __function__,
      'command'    => 'ThrottlingAlert',
      'parameters' => array());

    $params['parameters'] = array(
      'Event'       => 80, // 30
      'CounterName' => 'WPRBLK30', // 'Base Data Usage', 'WPRBLK20'
      'MSISDN'      => '6572898032',
      'Value'       => rand(100000, 999999));
    $result = $notification->processNotification($params);
    print_r($result);
    $this->assertEquals(TRUE, $result->is_success());

    $params = array(
      'actionUUID' => time() . __function__,
      'command'    => 'ThrottlingAlert',
      'parameters' => array());

    $params['parameters'] = array(
      'Event'       => 80, // 30
      'CounterName' => 'WPRBLK30', // 'Base Data Usage', 'WPRBLK20'
      'MSISDN'      => '7143606195',
      'Value'       => rand(100000, 999999));
    $result = $notification->processNotification($params);
    print_r($result);
    $this->assertEquals(TRUE, $result->is_success());
/*
    // L24 event 30
    $params['parameters'] = array(
      'Event'       => 30,
      'CounterName' => 'Base Data Usage',
      'MSISDN'      => '7202364186',
      'Value'       => rand(100000, 999999));

    $result = $notification->processNotification($params);
    print_r($result);
    $this->assertEquals(TRUE, $result->is_success());

    // L24 event 80
    $params['parameters'] = array(
      'Event'       => 80,
      'CounterName' => 'Base Data Usage',
      'MSISDN'      => '7142449303',
      'Value'       => 841328);

    $result = $notification->processNotification($params);
    print_r($result);
    $this->assertEquals(TRUE, $result->is_success()); */
  }

  public function test__PortOutDeactivation()
  {
    $command = 'PortOutDeac';

    $subParams = array(
    );

    $params = array(
      'actionUUID' => time() . __function__,
      'command'    => $command,
      'parameters'  => $subParams
    );

    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification();
    $result = $notification->processNotification($params);

    print_r($result);

    // callback not implemented
  }

  public function test__PortOutRequest()
  {
    $command = 'ThrottlingAlert';

    $subParams = array(
    );

    $params = array(
      'actionUUID' => time() . __function__,
      'command'    => $command,
      'parameters'  => $subParams
    );

    $notification = new \Ultra\Lib\MiddleWare\ACC\Notification();
    $result = $notification->processNotification($params);

    print_r($result);

    // callback not implemented
  }
}

?>
