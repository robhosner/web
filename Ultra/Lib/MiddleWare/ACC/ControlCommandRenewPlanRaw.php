<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * RenewPlanRaw control command implementation
 *
 * Does the exact same thing as RenewPlan, but the AddOnFeatureList node is custom ( $params['addOnFeatureListRaw'] )
 */
class ControlCommandRenewPlanRaw extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'RenewPlan';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addResourceData($parameters);

    $parameters['PlanData']['wholesalePlan'] = $parameters['wholesalePlan'];

    $parameters = $this->addRetailPlanIDFromUltraPlan( $parameters );

    $parameters = $this->addAddOnFeatureListRaw( $parameters );

    unset( $parameters['CorrelationID'] );
    unset( $parameters['ICCID'] );
    unset( $parameters['customer_id'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['wholesalePlan'] );
    unset( $parameters['addOnFeatureListRaw'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }

  private function addAddOnFeatureListRaw( $parameters )
  {
    if ( isset($parameters['addOnFeatureListRaw']) && $parameters['addOnFeatureListRaw'] )
      $parameters['PlanData']['AddOnFeatureList'] = $parameters['addOnFeatureListRaw'];

    return $parameters;
  }
}

?>
