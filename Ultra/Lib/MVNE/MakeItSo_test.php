<?php

require_once 'db.php';
require_once 'Ultra/Lib/MVNE/MakeItSo.php';


$m = new \Ultra\Lib\MVNE\MakeItSo();

#$m->verifyChangePlan(NULL,'L59');

#exit;


abstract class AbstractTestStrategy
{
  abstract function test( $argv , $m );
}

class Test_MakeItSo_verifyVoiceMailSoc extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'verifyVoiceMailSoc');
    $method->setAccessible(true);

    $querySubscriberResultData = array(
      "ResultCode"            => "100",
      "wholesalePlan"         => "32",
      "IMSI"                  => "xxxxxxxxxxxxxxx6537",
      "ResultMsg"             => "Success",
      "IMEI"                  => "xxxxxxxxxxxxxxx5872",
      "QuerySubscriberResult" => "1",
      "E911Address"           => NULL,
      "serviceTransactionId"  => "API-ULTRA-213721533108",
      "PlanEffectiveDate"     => "2014-11-22T16:54:09",
      "MSISDN"                => "7609122367",
      "AutoRenewalIndicator"  => "false",
      "SIM"                   => "8901260962113865379",
      "PlanId"                => "12006",
      "SubscriberStatus"      => "Active",
      "PlanExpirationDate"    => "2014-12-28T00:00:00",
      "AddOnFeatureInfoList"  => NULL
    );

    // invoke verifyVoiceMailSoc to get FALSE
    $r = $method->invoke(
      $m,
      $querySubscriberResultData,
      'v_english'
    );

    echo ( $r ? 'TRUE' : 'FALSE' )."\n";

    $feature = new \stdClass();
    $feature->FeatureID = '12802';

    $querySubscriberResultData['AddOnFeatureInfoList'] = new \stdClass();
    $querySubscriberResultData['AddOnFeatureInfoList']->AddOnFeatureInfo = array( $feature );

    // invoke verifyVoiceMailSoc to get TRUE
    $r = $method->invoke(
      $m,
      $querySubscriberResultData,
      'v_english'
    );

    echo ( $r ? 'TRUE' : 'FALSE' )."\n";
  }
}

class Test_MakeItSo_getNext extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    return $m->getNext();
  }
}

class Test_MakeItSo_makeitso_housekeeping extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    return \Ultra\Lib\MVNE\MakeItSo\makeitso_housekeeping();
  }
}

class Test_MakeItSo_processNext extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( isset($argv[2]) && is_numeric($argv[2]) )
      return $m->processNext( $argv[2] );

    return $m->processNext();
  }
}

class Test_MakeItSo_enqueueRenewPlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $action_uuid        = $argv[2];
    $customer_id        = $argv[3];
    $msisdn             = $argv[4];
    $iccid              = $argv[5];
    $ultra_plan         = $argv[6];
    $wholesale_plan     = $argv[7];
    $preferred_language = $argv[8];
    $plan_tracker_id    = $argv[9];
    $subplan            = $argv[10];

    return $m->enqueueRenewPlan( $action_uuid , $customer_id , $msisdn , $iccid , $ultra_plan , $wholesale_plan , $preferred_language, $plan_tracker_id, $subplan );
  }
}

class Test_MakeItSo_enqueueSuspend extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $action_uuid        = $argv[2];
    $customer_id        = $argv[3];
    $msisdn             = $argv[4];
    $iccid              = $argv[5];
    $ultra_plan         = $argv[6];
    $wholesale_plan     = $argv[7];
    $preferred_language = $argv[8];
    $plan_tracker_id    = $argv[9];

    return $m->enqueueSuspend( $action_uuid , $customer_id , $msisdn , $iccid , $ultra_plan , $wholesale_plan , $preferred_language, $plan_tracker_id );
  }
}

class Test_MakeItSo_enqueueUpgradePlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $action_uuid        = $argv[2];
    $customer_id        = $argv[3];
    $msisdn             = $argv[4];
    $iccid              = $argv[5];
    $current_plan       = $argv[6];
    $wholesale_plan     = $argv[7];
    $preferred_language = $argv[8];
    $option             = $argv[9];

    return $m->enqueueUpgradePlan( $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language , $option );
  }
}

class Test_MakeItSo_enqueueReactivate extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $action_uuid        = $argv[2];
    $customer_id        = $argv[3];
    $msisdn             = $argv[4];
    $iccid              = $argv[5];
    $ultra_plan         = $argv[6];
    $wholesale_plan     = $argv[7];
    $preferred_language = $argv[8];

    return $m->enqueueReactivate($action_uuid , $customer_id , $msisdn , $iccid, $ultra_plan, $wholesale_plan, $preferred_language);
  }
}

class Test_MakeItSo_delay extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( ! $m->getNext() ) { die ("getNext failed!\n"); }

    return $m->delay('test');
  }
}

class Test_MakeItSo_timeout extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( ! $m->getNext() ) { die ("getNext failed!\n"); }

    return $m->timeout();
  }
}

class Test_MakeItSo_processDeactivate extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    if ( ! $m->getNext( $makeitso_queue_id ) ) { die ("getNext failed!\n"); }

    if ( $m->makeitso_type != 'Deactivate' ) { die ("Type is not Deactivate!\n"); }

    return $m->process();
  }
}

class Test_MakeItSo_processSuspend extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    if ( ! $m->getNext( $makeitso_queue_id ) ) { die ("getNext failed!\n"); }

    if ( $m->makeitso_type != 'Suspend' ) { die ("Type is not Suspend!\n"); }

    return $m->process();
  }
}

class Test_MakeItSo_exists_open_task extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $m->dbConnect();

    $exists = \Ultra\Lib\MVNE\exists_open_task( $argv[2] );

    echo ( $exists ? 'It does' : 'It does not' )." exist\n";

    exit;
  }
}

class Test_MakeItSo_unblock_invocation_and_retry extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    \Ultra\Lib\MVNE\MakeItSo\unblock_invocation_and_retry( $argv[2] );

    exit;
  }
}

class Test_MakeItSo_exists_makeitso_paid_event extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    \Ultra\Lib\DB\ultra_acc_connect();

    if ( \Ultra\Lib\MVNE\MakeItSo\exists_makeitso_paid_event( $argv[2] ) )
      echo "YES\n";
    else
      echo "NO\n";

    exit;
  }
}

class Test_MakeItSo_adjust_and_retry extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    \Ultra\Lib\MVNE\MakeItSo\adjust_and_retry( $argv[2] );

    exit;
  }
}

class Test_MakeItSo_retry_list extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id list is required for this test\n"); }

    $makeitso_queue_id_list = $argv[2];

    $list_1 = explode(" ", $makeitso_queue_id_list);

    foreach( $list_1 as $id_1 )
    {
      $list_2 = explode(",", $id_1);

      foreach( $list_2 as $id_2 )
      {
        echo "retrying $id_2\n";

        $result = $m->retry( $id_2 );

        if ( $result->is_success() ) { echo "$id_2 OK\n"; } else { echo "$id_2 KO\n"; }
      }
    }
  }
}

class Test_MakeItSo_retry extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    $result = $m->retry( $makeitso_queue_id );

    print_r($result);
  }
}

class Test_MakeItSo_redo extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    $result = $m->redo( $makeitso_queue_id );

    print_r($result);
  }
}

class Test_MakeItSo_processUpgradePlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    if ( ! $m->getNext( $makeitso_queue_id ) ) { die ("getNext failed!\n"); }

    if ( $m->makeitso_type != 'UpgradePlan' ) { die ("Type is not UpgradePlan!\n"); }

    return $m->process();
  }
}

class Test_MakeItSo_processRenewPlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    if ( ! $m->getNext( $makeitso_queue_id ) ) { die ("getNext failed!\n"); }

    if ( $m->makeitso_type != 'RenewPlan' ) { die ("Type is not RenewPlan!\n"); }

    return $m->process();
  }
}

class Test_MakeItSo_processReactivate extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("makeitso_queue_id is required for this test\n"); }

    $makeitso_queue_id = $argv[2];

    if ( ! $m->getNext( $makeitso_queue_id ) ) { die ("getNext failed!\n"); }

    if ( $m->makeitso_type != 'Reactivate' ) { die ("Type is not RenewPlan!\n"); }

    return $m->process();
  }
}

class Test_MakeItSo_Reflection_processRenewEndMintPlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewEndMintPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $m->makeitso_options  = $makeitso_options;
    $m->makeitso_queue_id = 1;
    $m->attempt_count     = 1;

    // invoke private method processRenewEndMintPlan
    $r = $method->invoke(
      $m
    );

    print_r($r);
  }
}

class Test_MakeItSo_Reflection_processRenewMidMintPlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    teldata_change_db();

    // load a brand 3 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 3 and CUSTOMER_ID is not null');
    $iccid_sim_3 = $res->ICCID_FULL;

    echo "iccid_sim_3 = $iccid_sim_3\n";

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewMidMintPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = $iccid_sim_3;
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $m->makeitso_options  = $makeitso_options;
    $m->makeitso_queue_id = 1;
    $m->attempt_count     = 1;
    $m->customer_id       = 1;
    $m->created_days_ago  = 1;
    $m->attempt_history   = '';

    // invoke private method processRenewMidMintPlan
    $r = $method->invoke(
      $m
    );

    print_r($r);
  }
}

class Test_MakeItSo_Reflection_processRenewMintPlan extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewMintPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $m->makeitso_options  = $makeitso_options;
    $m->makeitso_queue_id = 1;
    $m->attempt_count     = 1;

    // invoke private method processRenewMintPlan
    $r = $method->invoke(
      $m
    );

    print_r($r);
  }
}

class Test_MakeItSo_delayDueToMWQueueSize extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    \Ultra\Lib\MVNE\MakeItSo\delayDueToMWQueueSize();
  }
}

class Test_MakeItSo_withdrawByCustomerId extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    if ( !isset($argv[2]) || !$argv[2] ) { die ("customer_id is required for this test\n"); }
    if ( !isset($argv[3]) || !$argv[3] ) { die ("type is required for this test\n"); }

    $m->dbConnect();

    return $m->withdrawByCustomerId( $argv[2] , $argv[3] );
  }
}

class Test_MakeItSo_compute_defer_seconds extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $attempt_history =
'PENDING|2014-02-27 13:56:30;DELAY(Command Invocation pending)|2014-02-27 13:56:30;DELAY(Command Invocation pending)|2014-02-27 13:56:30;DELAY(Command Invocation pending)|2014-02-27 13:36:16;DELAY(Invoked ReactivateSubscriber)|2014-02-27 13:11:03;DELAY(Invoked SuspendSubscriber)|2014-02-26 19:00:26;DELAY(Cannot perform QuerySubscriber)|2014-02-26 19:00:51;FAILED(QuerySubscriber - 200 : MSISDN is not numeric or not 10 digits long)|2014-02-26 19:01:41';

    $s = \Ultra\Lib\MVNE\MakeItSo\compute_defer_seconds( $attempt_history );

    echo "compute_defer_seconds = $s\n";
  }
}

class Test_MakeItSo_parseHistory extends AbstractTestStrategy
{
  function test( $argv , $m )
  {
    $attempt_history =
'PENDING|2014-02-27 13:56:30;DELAY(Command Invocation pending)|2014-02-27 13:56:30;DELAY(Command Invocation pending)|2014-02-27 13:56:30;DELAY(Command Invocation pending)|2014-02-27 13:36:16;DELAY(Invoked ReactivateSubscriber)|2014-02-27 13:11:03;DELAY(Invoked SuspendSubscriber)|2014-02-26 19:00:26;DELAY(Cannot perform QuerySubscriber)|2014-02-26 19:00:51;FAILED(QuerySubscriber - 200 : MSISDN is not numeric or not 10 digits long)|2014-02-26 19:01:41';

    echo "$attempt_history\n\n";

    list ( $parsedHistory , $latest_sequence_episode , $latest_sequence_length ) = \Ultra\Lib\MVNE\MakeItSo\parseHistory( $attempt_history );

    print_r($parsedHistory);

    echo "\nlatest_sequence_episode = $latest_sequence_episode ; $latest_sequence_length\n\n";
  }
}

class Test_MakeItSo_querySubscriber extends AbstractTestStrategy
{
  function test($argv, $m)
  {
    if ( ! empty($argv[2]))
      $m->makeitso_options->msisdn = $argv[2];
    elseif ( ! empty($rgv[3]))
      $m->makeitso_options->iccid = $argv[2];
    else
      die ("ERROR: either MSISDN or ICCID is required\n");
    $m->action_uuid = 'TEST' . time();
    $control =  new \Ultra\Lib\MiddleWare\ACC\Control;

    $result = $m->querySubscriber($control);
    echo "RESULT: " . ($result === FALSE ? 'FALSE' : print_r($result, TRUE));
  }
}


# perform test #


$testClass = 'Test_MakeItSo_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$r = $testObject->test( $argv , $m );

print_r($r);


/*
Usage:

php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan   $ACTION_UUID $CUSTOMER_ID $MSISDN $ICCID $CURRENT_PLAN $WHOLESALEPLAN $LANG $PLAN_TRACKER_ID $SUBPLAN
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueSuspend     $ACTION_UUID $CUSTOMER_ID $MSISDN $ICCID $CURRENT_PLAN $WHOLESALEPLAN $LANG $PLAN_TRACKER_ID
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan $ACTION_UUID $CUSTOMER_ID $MSISDN $ICCID $CURRENT_PLAN $WHOLESALEPLAN $LANG $OPTION
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueReactivate  $ACTION_UUID $CUSTOMER_ID $MSISDN $ICCID $ULTRA_PLAN $WHOLESALEPLAN $LANG
php Ultra/Lib/MVNE/MakeItSo_test.php getNext
php Ultra/Lib/MVNE/MakeItSo_test.php processNext        $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php processSuspend     $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php processDeactivate  $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php processUpgradePlan $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php processRenewPlan   $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php processReactivate  $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php Reflection_processRenewEndMintPlan
php Ultra/Lib/MVNE/MakeItSo_test.php Reflection_processRenewMidMintPlan
php Ultra/Lib/MVNE/MakeItSo_test.php Reflection_processRenewMintPlan
php Ultra/Lib/MVNE/MakeItSo_test.php delay
php Ultra/Lib/MVNE/MakeItSo_test.php timeout
php Ultra/Lib/MVNE/MakeItSo_test.php parseHistory
php Ultra/Lib/MVNE/MakeItSo_test.php withdrawByCustomerId $CUSTOMER_ID
php Ultra/Lib/MVNE/MakeItSo_test.php compute_defer_seconds
php Ultra/Lib/MVNE/MakeItSo_test.php unblock_invocation_and_retry $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php adjust_and_retry   $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php retry              $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php redo               $MAKEITSO_QUEUE_ID
php Ultra/Lib/MVNE/MakeItSo_test.php retry_list         $LIST_OF_IDs
php Ultra/Lib/MVNE/MakeItSo_test.php exists_open_task   $CUSTOMER_ID
php Ultra/Lib/MVNE/MakeItSo_test.php makeitso_housekeeping
php Ultra/Lib/MVNE/MakeItSo_test.php delayDueToMWQueueSize
php Ultra/Lib/MVNE/MakeItSo_test.php querySubscriber    $MSISDN $ICCID

Examples:
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan aaaaaSTANDBY 476296 2284379378 8901260842107740269 STANDBY W-PRIMARY EN
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan eeeeeeeeeL39 476296 2284379378 8901260842107740269 L39 W-PRIMARY EN
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan eeeeeeeeeL49 476296 2284379378 8901260842107740269 L49 W-PRIMARY EN
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan eeeeeeeeeL59 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0012 476296 2284379378 8901260842107740269 L49 W-PRIMARY EN V-SPANISH
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0013 476296 2284379378 8901260842107740269 L49 W-PRIMARY EN V-ENGLISH
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0011 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN 'B-VOICE|20'
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0011 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN 'B-SMS|20'
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0022 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN 'A-VOICE-DR|20'
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0022 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN 'A-SMS-DR|40'
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0033 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN 'A-DATA-BLK|1'
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testbb3333 476296 2284379378 8901260842107740269 L59 W-PRIMARY EN 'A-DATA-BLK|3'
php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan testaa0014 476296 2284379378 8901260842107740269 L49 W-PRIMARY EN 'CHANGE|L19'

DATAQ-22, DATAQ-29
  downgrade L39 -> L19 and update wholesale plan
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan VYT_TEST_01 3373 7202510654 8901260962103789159 L39 W-PRIMARY EN 'CHANGE|L19'
    php Ultra/Lib/MVNE/MakeItSo_test.php processUpgradePlan 1696
  update from L19 -> L39 and update wholesale plan
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan VYT_TEST_01 18960 5189865644 8901260842107739485 L19 W-SECONDARY EN 'CHANGE|L39'
    php Ultra/Lib/MVNE/MakeItSo_test.php processUpgradePlan 1694
  renew plan L49 -> L19 and update wholesale plan
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan VYT_TEST_02 18857 6094018424 8901260842107310667 L19 W-PRIMARY EN
    php Ultra/Lib/MVNE/MakeItSo_test.php processRenewPlan 1698
  renew plan L19 -> L59 and update wholesale plan
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan VYT_TEST_02 18715 5189866600 8901260842107739410 L59 W-SECONDARY EN
    php Ultra/Lib/MVNE/MakeItSo_test.php processRenewPlan 1699
  renew L19 -> L19 on W-PRIMARY
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan VYT_TEST_301 18847 6092235709 8901260842107310527 L19 W-PRIMARY EN
    php Ultra/Lib/MVNE/MakeItSo_test.php processRenewPlan 1743
  renew L19 -> L19 W-PRIMARY -> W-SECONDARY
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan VYT_TEST_302 18850 6093251624 890126084210731057 L19 W-SECONDARY EN
    php Ultra/Lib/MVNE/MakeItSo_test.php processRenewPlan 1746
  renew L19 -> L49 W-SECONDARY -> W-PRIMARY
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueRenewPlan VYT_TEST_303 18850 6093251624 8901260842107310576 L49 W-PRIMARY EN
    php Ultra/Lib/MVNE/MakeItSo_test.php processRenewPlan 1749
  UpgradePlan L19 -> L39 W-SECONDARY -> W-PRIMARY
    php Ultra/Lib/MVNE/MakeItSo_test.php enqueueUpgradePlan VYT_TEST_304 18690 6094010836 8901260842116131096 L19 W-PRIMARY EN 'CHANGE|L39'
    php Ultra/Lib/MVNE/MakeItSo_test.php processUpgradePlan 1751
*/

