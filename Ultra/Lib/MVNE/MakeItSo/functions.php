<?php

namespace Ultra\Lib\MVNE\MakeItSo;

/**
 * checkCommandInvocationCompleted
 *
 * check for completed command invocation
 * works reliably only for unique calls in a makeitso task
 *
 * @return boolean
 */
function checkCommandInvocationCompleted($makeitsoActionUUID, $command)
{
  \Ultra\Lib\DB\ultra_acc_connect();

  return ( count(\get_ultra_acc_command_invocations([
    'ACTION_UUID' => $makeitsoActionUUID,
    'COMMAND'     => $command,
    'STATUS'      => 'COMPLETED'
  ])));
}

/**
 * checkForLimitedPlanSoc
 *
 * checks if addOnFeatureInfo contains r_limited SOC
 *
 * @return boolean
 */
function checkForLimitedPlanSoc($addOnFeatureInfo)
{
  $result = FALSE;

  list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

  foreach( $addOnFeatureInfo as $addOnFeature )
  {
    if ($ultraSocsByPlanId[ $addOnFeature->FeatureID ] == 'r_limited' )
      $result = TRUE;
  }

  return $result;
}

/**
 * checkForLimitedPlanId
 *
 * checks if PlanId contains r_limited SOC
 *
 * @return boolean
 */
function checkForLimitedPlanId($planId)
{
  $result = FALSE;

  if ($planId == 12007)
    $result = TRUE;

  return $result;
}

/**
 * planCheck reflects the SOCs of the right plan
 *
 * checks if addOnFeatureInfo reflects the SOCs of the right plan
 *
 * @return boolean
 */
function planCheck($addOnFeatureInfo, $customer_id, $ultra_plan, $subplan)
{
  // verify that $statusData reflects the SOCs of the right plan ( $this->makeitso_options->ultra_plan )
  list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

  // SOCs in the plan we have to renew
  $subplan = empty($subplan) ? NULL : $subplan;
  $accSocsPlanConfigNewPlan = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($ultra_plan, $customer_id, $subplan);

  // adjust throttle socs
  $throttling = new \Throttling($ultra_plan);
  $throttleSpeed = $throttling->detectThrottleSpeedFromAddOnFeatureInfo($addOnFeatureInfo);
  $throttling->setCurrentThrottleSpeed($throttleSpeed);
  $throttling->adjustPlanConfigForThrottle($accSocsPlanConfigNewPlan);

  dlog('',"ultra_plan               = %s", $ultra_plan);
  dlog('',"accSocsPlanConfigNewPlan = %s", $accSocsPlanConfigNewPlan);
  dlog('',"AddOnFeatureInfo         = %s", $addOnFeatureInfo);

  // if TRUE, the SOCS are correctly set up for the plan we have to renew
  $planCheck = TRUE;

  if ( $ultra_plan == 'STANDBY' )
  {
    // with plan 'STANDBY', we only allow Retail ID and Wholesale ID

    // loop through SOCs already set
    foreach( $addOnFeatureInfo as $addOnFeature )
    {
      if ( ! \Ultra\MvneConfig\isWholesalePlanSOC(  $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
        && ! \Ultra\MvneConfig\isRetailPlanSOC(     $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
        && ! \Ultra\MvneConfig\isVoicemailSOC(      $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
        && ! \Ultra\UltraConfig\isRoamingSoc(       $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
        && ! \Ultra\UltraConfig\isRoamingWalletSoc( $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
        && ! \Ultra\UltraConfig\isWifiCallingSoc(   $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
      )
        $planCheck = FALSE;
    }
  }
  else // not STANDBY
  {
    // loop through SOCs in the new plan
    foreach( $accSocsPlanConfigNewPlan as $ultraSocName => $featureValue )
    {
      $socCheck = FALSE;

      // loop through SOCs already set
      foreach( $addOnFeatureInfo as $addOnFeature )
      {
        if ( $ultraSocName == $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
          $socCheck = TRUE;
      }

      if ( ! $socCheck )
      {
        dlog('',"SOC $ultraSocName is missing");
        $planCheck = FALSE;
      }
    }
  }

  return $planCheck;
}

/**
 * statusDataContainsUltraRetailPlan
 *
 * Checks if result of query subscriber contains R-Limited Feature ID
 *
 * @return boolean
 */
function statusDataContainsUltraRetailPlan($statusData)
{
  foreach ($statusData->AddOnFeatureInfoList->AddOnFeatureInfo as $addOnFeatureInfo)
    if ( in_array($addOnFeatureInfo->FeatureID,[RETAIL_PLAN_LIMITED,RETAIL_PLAN_UNLIMITED]) )
      return TRUE;

  return FALSE;
}

function statusDataContainsSMSVoice($statusData)
{
  foreach ($statusData->AddOnFeatureInfoList->AddOnFeatureInfo as $addOnFeatureInfo)
    if ( in_array($addOnFeatureInfo->FeatureID,[B_VOICE_FEATURE_ID,B_SMS_FEATURE_ID]) )
      return TRUE;

  return FALSE;
}

/**
 * exists_makeitso_paid_event
 *
 * Checks if a Paid Event has been already associated with the given makeitso_queue_id
 *
 * @return boolean
 */
function exists_makeitso_paid_event( $makeitso_queue_id )
{
  $makeitso_data = \get_soap_paid_event_by_makeitso_queue_id( $makeitso_queue_id );

  return ! ! ( ! empty( $makeitso_data ) && is_object( $makeitso_data ) && $makeitso_data->SOAP_LOG_ID );
}

/**
 * compute_defer_seconds
 *
 * How many seconds from now until the next attempt
 *
 * @return integer
 */
function compute_defer_seconds( $attempt_history )
{
  list ( $parsedHistory , $latest_sequence_episode , $latest_sequence_length ) = \Ultra\Lib\MVNE\MakeItSo\parseHistory( $attempt_history );

  if ( ( $latest_sequence_episode == 'BiphasicInvoked' ) && ( $latest_sequence_length < 5 ) )
    // The past delay was "First Async command successful"
    return 120;
  if ( $latest_sequence_episode == 'BiphasicUnavailable' )
  {
    // The past 1-4 delays were "Async command unavailable"
    if ( ( $latest_sequence_length >= 1  ) && ( $latest_sequence_length < 5  ) )
      return 120;
    // The past 5-9 delays were "Async command unavailable"
    if ( ( $latest_sequence_length >= 5  ) && ( $latest_sequence_length < 10 ) )
      return 600;
    // The past 10-19 delays were "Async command unavailable"
    if ( ( $latest_sequence_length >= 10 ) && ( $latest_sequence_length < 20 ) )
      return 7200;
  }
  elseif ( $latest_sequence_episode == 'QuerySubscriberError' )
  {
    // The past 1-4 delays were "Cannot Query Details"
    if ( ( $latest_sequence_length >= 1  ) && ( $latest_sequence_length < 5  ) )
      return 120;
    // The past 5-9 delays were "Cannot Query Details"
    if ( ( $latest_sequence_length >= 5  ) && ( $latest_sequence_length < 10 ) )
      return 600;
    // The past 10-19 delays were "Cannot Query Details"
    if ( ( $latest_sequence_length >= 10 ) && ( $latest_sequence_length < 20 ) )
      return 3600;
  }
  if ( $latest_sequence_episode == 'BiphasicPending' )
  {
    // The past delay was "Command Invocation Blocked"
    if ( $latest_sequence_length == 1 )
      return 120;
    // The past 2-4 delays were "Command Invocation Blocked"
    if ( ( $latest_sequence_length >= 2  ) && ( $latest_sequence_length < 5  ) )
      return 180;
    // The past 5-9 delays were "Command Invocation Blocked"
    if ( ( $latest_sequence_length >= 5  ) && ( $latest_sequence_length < 10 ) )
      return 600;
    // The past 10-19 delays were "Command Invocation Blocked"
    if ( ( $latest_sequence_length >= 10 ) && ( $latest_sequence_length < 20 ) )
      return 3600;
    // The past 20-29 delays were "Command Invocation Blocked"
    if ( ( $latest_sequence_length >= 20 ) && ( $latest_sequence_length < 30 ) )
      return 7200;
    // The past 30-39 delays were "Command Invocation Blocked"
    if ( ( $latest_sequence_length >= 30 ) && ( $latest_sequence_length < 40 ) )
      return 10800;
  }

  // default
  return 60;
}

/**
 * parseHistory
 *
 * ATTEMPT_HISTORY data mining
 *
 * @return array
 */
function parseHistory( $attempt_history )
{
  // raw episode count
  $parsedHistory = array(
    'QuerySubscriberError' => 0,
    'BiphasicUnavailable'  => 0,
    'BiphasicPending'      => 0,
    'BiphasicInvoked'      => 0
  );

  $latest_sequence_length  = 0;
  $latest_sequence_episode = '';

  $episodes = explode("|", $attempt_history );

  foreach( $episodes as $episode )
    if ( preg_match("/DELAY\(Cannot perform QuerySubscriber\)/", $episode, $matches) )
    {
      $parsedHistory['QuerySubscriberError']++;

      if ( $latest_sequence_episode == 'QuerySubscriberError' )
        $latest_sequence_length++;
      else
      {
        $latest_sequence_length  = 1;
        $latest_sequence_episode = 'QuerySubscriberError';
      }
    }
    elseif ( preg_match("/DELAY\(Command Invocation pending\)/", $episode, $matches) )
    {
      $parsedHistory['BiphasicPending']++;

      if ( $latest_sequence_episode == 'BiphasicPending' )
        $latest_sequence_length++;
      else
      {
        $latest_sequence_length  = 1;
        $latest_sequence_episode = 'BiphasicPending';
      }
    }
    elseif ( preg_match("/DELAY\(Invoked \w+\)/", $episode, $matches) )
    {
      $parsedHistory['BiphasicInvoked']++;

      if ( $latest_sequence_episode == 'BiphasicInvoked' )
        $latest_sequence_length++;
      else
      {
        $latest_sequence_length  = 1;
        $latest_sequence_episode = 'BiphasicInvoked';
      }
    }
    elseif ( preg_match("/DELAY\(Biphasic command unavailable\)/", $episode, $matches) )
    {
      $parsedHistory['BiphasicUnavailable']++;

      if ( $latest_sequence_episode == 'BiphasicUnavailable' )
        $latest_sequence_length++;
      else
      {
        $latest_sequence_length  = 1;
        $latest_sequence_episode = 'BiphasicUnavailable';
      }
    }

  return array( $parsedHistory , $latest_sequence_episode , $latest_sequence_length );
}

/**
 * adjust_and_retry
 *
 * Load a FAILED MISO, adjust MAKEITSO_OPTIONS and retry.
 * This is useful if the customer changed MSISDN or ICCID.
 */
function adjust_and_retry( $makeitso_queue_id )
{
  $success = FALSE;

  \Ultra\Lib\DB\ultra_acc_connect();

  $data = get_makeitso(
    array(
      'makeitso_queue_id' => $makeitso_queue_id
    )
  );

  dlog('',"data = %s",$data);

  if ( $data && is_array($data) && count($data) )
  {
    $options = json_decode( $data[0]->MAKEITSO_OPTIONS );

    if ( $options && is_object($options) )
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($data[0]->CUSTOMER_ID , array('CURRENT_ICCID_FULL','current_mobile_number','plan_state') );

      if ( $customer && ( $customer->plan_state != 'Cancelled' ) )
      {
        dlog('',"customer = %s",$customer);

        foreach( (array) $options as $name => $value )
          dlog('',"name = $name ; value = $value");

        // sanity checks
        if (
          $customer->CURRENT_ICCID_FULL
          &&
          ( $customer->CURRENT_ICCID_FULL != '' )
          &&
          ( $customer->CURRENT_ICCID_FULL != ' ' )
          &&
          $customer->current_mobile_number
          &&
          ( $customer->current_mobile_number != '' )
          &&
          ( $customer->current_mobile_number != ' ' )
          &&
          ( $customer->CURRENT_ICCID_FULL != $options->iccid ) || ( $customer->current_mobile_number != $options->msisdn )
        )
        {
          $options->iccid  = $customer->CURRENT_ICCID_FULL;
          $options->msisdn = $customer->current_mobile_number;

          dlog('',"new options = %s",$options);

          \Ultra\Lib\DB\ultra_acc_connect();

          // update MAKEITSO_QUEUE

          $success = update_makeitso(
            array(
              'makeitso_queue_id' => $makeitso_queue_id,
              'makeitso_options'  => json_encode( $options ),
              'created_date_time' => 'NOW'
            )
          );

          if ( $success )
          {
            $object = new \Ultra\Lib\MVNE\MakeItSo();

            $result = $object->retry( $makeitso_queue_id );

            $success = $result->is_success();
          }
        }
      }
    }
  }

  if ( $success )
    dlog('',"success");
}

/**
 * makeitso_housekeeping
 *
 * Detect and fix stuck MISO items
 *
 * @return Result object
 */
function makeitso_housekeeping()
{
  \Ultra\Lib\DB\ultra_acc_connect();

  // get all MAKEITSO_QUEUE rows which should have been processed more than 1 hour ago
  $data = \get_stuck_makeitso( 1 );

  if ( $data && is_array( $data ) && count( $data ) )
  {
    $redis_makeitso = new \Ultra\Lib\Util\Redis\MakeItSo();

    foreach( $data as $row )
    {
      dlog('',"MAKEITSO_QUEUE_ID ".$row->MAKEITSO_QUEUE_ID." is stuck, it should have been processed ".$row->MINUTES_AGO." minutes ago" );

      $redis_makeitso->add( time() , $row->MAKEITSO_QUEUE_ID );
    }
  }

  return \make_ok_Result();
}

/**
 * unblock_invocation_and_retry
 *
 * Load a FAILED MISO, unblock COMMAND INVOCATION and retry.
 * This is useful if the COMMAND INVOCATION is blocked
 */
function unblock_invocation_and_retry( $makeitso_queue_id )
{
  $success = FALSE;

  \Ultra\Lib\DB\ultra_acc_connect();

  $data = get_makeitso(
    array(
      'makeitso_queue_id' => $makeitso_queue_id
    )
  );

  dlog('',"data = %s",$data);

  if ( $data && is_array($data) && count($data) )
  {
    $options = json_decode( $data[0]->MAKEITSO_OPTIONS );

    if ( $options && is_object($options) )
    {
      $options = (array) $options;

      foreach( $options as $name => $value )
        dlog('',"name = $name ; value = $value");

      if ( isset($options['msisdn']) )
      {
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        $result = $mwControl->mwQuerySubscriber(
          array(
            'actionUUID' => time(),
            'msisdn'     => $options['msisdn']
          )
        );

        dlog('',"result = %s",$result);

        if ( $result->is_success()
          && isset($result->data_array['success'])
          && $result->data_array['success']
          && isset($result->data_array['body'])
          && ( property_exists( $result->data_array['body'] , 'ResultCode' ) )
          && ( property_exists( $result->data_array['body'] , 'ResultMsg'  ) )
          && ( $result->data_array['body']->ResultCode == '100' )
          && ( $result->data_array['body']->ResultMsg  == 'Success' )
        )
        {
          dlog('',"body = %s",$result->data_array['body']);

          if ( ( ! property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) ) || ( ! $result->data_array['body']->CurrentAsyncService ) )
          {
            // ready to unblock COMMAND INVOCATION

            $command_invocations = get_ultra_acc_command_invocations(
              array(
                'customer_id' => $data[0]->CUSTOMER_ID,
              ),
              NULL,
              'COMMAND_INVOCATIONS_ID desc'
            );

            dlog('',"command_invocations = %s",$command_invocations);

            $command_invocations_id     = NULL;
            $command_invocations_status = NULL;

            foreach( $command_invocations as $row )
              if ( ( $row->STATUS == 'INITIATED' ) || ( $row->STATUS == 'OPEN' ) )
              {
                dlog('',"COMMAND_INVOCATIONS_ID : ".$row->COMMAND_INVOCATIONS_ID);
                dlog('',"STATUS                 : ".$row->STATUS);

                $command_invocations_id     = $row->COMMAND_INVOCATIONS_ID;
                $command_invocations_status = $row->STATUS;
              }

            if ( $command_invocations_id )
            {
              $commandInvocation = new \CommandInvocation();

              $result = $commandInvocation->loadPending(
                array(
                  'customer_id' => $data[0]->CUSTOMER_ID
                )
              );

              if ( $result->is_success() )
              {
                dlog('',"commandInvocation = %s",$commandInvocation);

                // close the pending command invocation

                $result = $commandInvocation->escalationSuccess(
                  array(
                    'from_status'            => $command_invocations_status,
                    'last_status_updated_by' => 'raf (unblock_invocation_and_retry)'
                  )
                );

                if ( $result->is_success() )
                {
                  // retry MISO

                  $object = new \Ultra\Lib\MVNE\MakeItSo();

                  $result = $object->retry( $makeitso_queue_id );

                  $success = $result->is_success();
                }
                else
                  dlog('',"escalationSuccess failed");
              }
              else
                dlog('',"We could not load the pending command invocations for customer id ".$data[0]->CUSTOMER_ID);
            }
            else
              dlog('',"We could not find pending command invocations for customer id ".$data[0]->CUSTOMER_ID);
          }
          else
            dlog('',"QuerySubscriber returned CurrentAsyncService = ".$result->data_array['body']->CurrentAsyncService);
        }
        else
          dlog('',"QuerySubscriber failed");
      }
      else
        dlog('',"msisdn not found");
    }
    else
      dlog('',"options not found");
  }
  else
    dlog('',"makeitso_queue_id $makeitso_queue_id not found");

  if ( $success )
    dlog('',"success");
}

/**
 * delayDueToMWQueueSize
 *
 * Delay execution due to large number of pending elements in MW queue(s)
 * ULTRA_MW/OUTBOUND/SYNCH
 * ACC_MW/OUTBOUND/SYNCH
 *
 * @return boolean
 */
function delayDueToMWQueueSize( $redis=NULL )
{
  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis;

  $max_mw_queue_size = 90;

  $mw_queue_size_redis_key = 'ultra/mw_queue/outbound/size';

  $mw_queue_size = $redis->get( $mw_queue_size_redis_key );

  \logDebug("mw_queue_size = $mw_queue_size");

  // the result of this method is cached because we don't want to access Redis an excessive amount of times
  if ( is_null($mw_queue_size) )
  {
    $ttl = 60;

    $controlChannel = new \Ultra\Lib\MQ\ControlChannel;

    $messageQueue = $controlChannel->outboundACCMWControlChannel();

    $members_1 = $redis->smembers( $messageQueue );

    dlog('',"count = ".count($members_1)." members = %s",$members_1);

    $messageQueue = $controlChannel->outboundUltraMWControlChannel();

    $members_2 = $redis->smembers( $messageQueue );

    dlog('',"count = ".count($members_2)." members = %s",$members_2);

    $mw_queue_size = max( count($members_1) , count($members_2) );

    if ( $mw_queue_size )
      $redis->set( $mw_queue_size_redis_key , $mw_queue_size , $ttl );
    else
      $redis->set( $mw_queue_size_redis_key , "0"            , $ttl );
  }

  if( $mw_queue_size > $max_mw_queue_size )
  {
    \logWarn("DELAY DUE TO MW QUEUES SIZE = $mw_queue_size");

    return TRUE;
  }

  return FALSE;
}

