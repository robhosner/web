<?php

# php Ultra/Lib/StateMachine/Action/functions_test.php noopAction
# php Ultra/Lib/StateMachine/Action/functions_test.php testAction
# php Ultra/Lib/StateMachine/Action/functions_test.php assertBalanceAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php emptyMinutesAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php expireSimCardAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php expireMSISDNAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php disableAccountAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php deactivateUltraMSISDNAction $CUSTOMER_ID $TRANSITION_UUID
# php Ultra/Lib/StateMachine/Action/functions_test.php deleteAccountAliasesByMSISDNAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php deleteAccountAliasesByAccountIdAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php setActivationDateTimeIfUnsetAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php setPlanStartedAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php setGrossAddDateAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php logCancellationTransitionInActivationHistoryAction $CUSTOMER_ID $TRANSTION_UUID
# php Ultra/Lib/StateMachine/Action/functions_test.php nullifySimAndMsisdnAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php logTransitionInActivationHistoryAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php logNewActivationInActivationHistoryAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php saveGrossAddDateAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php logPlanStartedInActivationHistoryAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php mvneProvisionAction $CUSTOMER_ID $TRANSITION_UUID $ACTION_UUID
# php Ultra/Lib/StateMachine/Action/functions_test.php mvneActivateAction $CUSTOMER_ID $TRANSITION_UUID $ACTION_UUID
# php Ultra/Lib/StateMachine/Action/functions_test.php activeMaybeSetActivationDateTimeAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php activePlanStartedXAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php renewalActivePlanExpiresAction $CUSTOMER_ID
# php Ultra/Lib/StateMachine/Action/functions_test.php mvneensureSuspendAction $CUSTOMER_ID $TRANSACTION_UUID $ACTION_UUID
# php Ultra/Lib/StateMachine/Action/functions_test.php mvneensureCancelAction $CUSTOMER_ID $TRANSACTION_UUID $ACTION_UUID
# php Ultra/Lib/StateMachine/Action/functions_test.php sleepAction $seconds

require_once 'db.php';
require_once 'classes/Outcome.php';
require_once 'Ultra/Lib/StateMachine/Action/functions.php';

function test__noopAction()
{
  $outcome = \Ultra\Lib\StateMachine\Action\noopAction(array(), NULL, NULL);
  print_r($outcome);
}

function test__testAction()
{
  $outcome = \Ultra\Lib\StateMachine\Action\testAction(array(), NULL, NULL);
  print_r($outcome);
}

function test__assertBalanceAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];
  $customerData = array('customer_id' => $customer_id);

  $params = array('amount' => 19);

  $outcome = \Ultra\Lib\StateMachine\Action\assertBalanceAction($customerData, NULL, NULL, $params);
  print_r($outcome);
}

function test__emptyMinutesAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];
  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\emptyMinutesAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__expireSimCardAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array('current_iccid_full'));

  $outcome = \Ultra\Lib\StateMachine\Action\expireSimCardAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__expireMSISDNAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number'));

  $outcome = \Ultra\Lib\StateMachine\Action\expireMSISDNAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__disableAccountAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\disableAccountAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__deactivateUltraMSISDNAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];
  $transition_uuid = $argv[3];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number'));

  $outcome = \Ultra\Lib\StateMachine\Action\deactivateUltraMSISDNAction($customerData, $transition_uuid, NULL);
  print_r($outcome);
}

function test__deleteAccountAliasesByMSISDNAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number'));

  $outcome = \Ultra\Lib\StateMachine\Action\deleteAccountAliasesByMSISDNAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__deleteAccountAliasesByAccountIdAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array('current_mobile_number'));

  $outcome = \Ultra\Lib\StateMachine\Action\deleteAccountAliasesByAccountIdAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__setActivationDateTimeIfUnsetAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\setActivationDateTimeIfUnsetAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__setPlanStartedAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);
  $params = array('plan_started' => date('Y-m-d', time()));

  $outcome = \Ultra\Lib\StateMachine\Action\setPlanStartedAction($customerData, NULL, NULL, $params);
  print_r($outcome);
}

function test__setGrossAddDateAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\setGrossAddDateAction($customerData, NULL, NULL);
  print_r($outcome);
}

function test__logCancellationTransitionInActivationHistoryAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];
  $transition_uuid = $argv[3];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\logCancellationTransitionInActivationHistoryAction(
    get_customer_from_customer_id($customer_id),
    $transition_uuid,
    NULL
  );

  print_r($outcome);
}

function test__nullifySimAndMsisdnAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\nullifySimAndMsisdnAction($customerData, NULL, NULL);

  print_r($outcome);
}

function test__logTransitionInActivationHistoryAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\logTransitionInActivationHistoryAction($customerData, NULL, NULL);

  print_r($outcome);
}

function test__logNewActivationInActivationHistoryAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\logNewActivationInActivationHistoryAction($customerData, NULL, NULL);

  print_r($outcome);
}

function test__saveGrossAddDateAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\saveGrossAddDateAction($customerData, NULL, NULL);

  print_r($outcome);
}

function test__logPlanStartedInActivationHistoryAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\logPlanStartedInActivationHistoryAction($customerData, NULL, NULL);

  print_r($outcome);
}

function test__mvneProvisionAction()
{
  teldata_change_db();

  global $argv;

  $customer_id     = $argv[2];
  $transition_uuid = $argv[3];
  $action_uuid     = $argv[4];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array(
    'current_iccid_full', 'postal_code', 'preferred_language', 'customer_id'
  ));

  

  $params = array('plan' => 'L19');

  $outcome = \Ultra\Lib\StateMachine\Action\mvneProvisionAction($customerData, $transition_uuid, $action_uuid, $params);

  print_r($outcome);
}

function test__mvneActivateAction()
{
  teldata_change_db();

  global $argv;

  $customer_id     = $argv[2];
  $transition_uuid = $argv[3];
  $action_uuid     = $argv[4];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array(
    'current_iccid_full', 'postal_code', 'preferred_language', 'customer_id'
  ));

  $params = array('plan' => 'L19');

  $outcome = \Ultra\Lib\StateMachine\Action\mvneActivateAction($customerData, $transition_uuid, $action_uuid, $params);

  print_r($outcome);
}

function test__activeMaybeSetActivationDateTimeAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);

  $outcome = \Ultra\Lib\StateMachine\Action\activeMaybeSetActivationDateTimeAction($customerData, NULL, NULL);

  print_r($outcome);
}

function test__activePlanStartedXAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);
  $params = array('plan_started' => date('Y-m-d', time()));

  $outcome = \Ultra\Lib\StateMachine\Action\activePlanStartedXAction($customerData, NULL, NULL, $params);

  print_r($outcome);
}

function test__renewalActivePlanExpiresAction()
{
  teldata_change_db();

  global $argv;

  $customer_id = $argv[2];

  $customerData = array('customer_id' => $customer_id);
  $params = array('plan_started' => date('Y-m-d', time()));

  $outcome = \Ultra\Lib\StateMachine\Action\renewalActivePlanExpiresAction($customerData, NULL, NULL, $params);

  print_r($outcome);
}

function test__mvneEnsureSuspendAction()
{
  teldata_change_db();

  global $argv;

  $customer_id     = $argv[2];
  $transition_uuid = $argv[3];
  $action_uuid     = $argv[4];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array(
    'customer_id',
    'current_mobile_number',
    'current_iccid_full'
  ));

  $outcome = \Ultra\Lib\StateMachine\Action\mvneEnsureSuspendAction(
    $customerData, $transition_uuid, $action_uuid);

  print_r($outcome);
}

function test__mvneEnsureCancelAction()
{
  teldata_change_db();

  global $argv;

  $customer_id     = $argv[2];
  $transition_uuid = $argv[3];
  $action_uuid     = $argv[4];

  $customerData = (array) get_ultra_customer_from_customer_id($customer_id, array(
    'customer_id',
    'current_mobile_number',
    'current_iccid_full'
  ));

  $outcome = \Ultra\Lib\StateMachine\Action\mvneEnsureCancelAction(
    $customerData, $transition_uuid, $action_uuid);

  print_r($outcome);
}

function test__sleepAction()
{
  global $argv;
  $seconds = $argv[2];

  $outcome = new \Outcome;

  sleep($seconds);

  $outcome->succeed();

  return $outcome;
}

// test
$function = 'test__' . $argv[1];
$function();
