#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use JSON::XS;
use Ultra::Lib::Amdocs::Base;
use Ultra::Lib::Amdocs::Control::Prober;
use Ultra::Lib::Amdocs::Notification;
use Ultra::Lib::Amdocs::Notification::Prober;
use Ultra::Lib::Aspider::Prober;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::WSDL11;

my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

my $config = Ultra::Lib::Util::Config->new();

my $controlProber = Ultra::Lib::Amdocs::Control::Prober->new(
  JSON_CODER  => $json_coder,
  LOG_ENABLED => 1,
  WSDL_FILE   => 'runners/amdocs/'.$config->find_credential('amdocs/control/soap/wsdl')
);

my $command    = 'CancelDeviceLocation';

my $parameters =
{
  'MSISDN'   => '1001001000',
  'SIM'      => '1001001000100100100',
  'UserData' =>
  {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2014-08-27T08:29:01',
  },
};

my $requestResult = $controlProber->probeControlCommand( $command , $parameters );

print Dumper( $requestResult );

__END__

test for Ultra::Lib::Amdocs::Control::Prober

