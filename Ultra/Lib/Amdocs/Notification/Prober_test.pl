use strict;
use warnings;

use Data::Dumper;
use JSON::XS;
use Ultra::Lib::Amdocs::Notification::Prober;
use Ultra::Lib::Util::Config;

my $MVNO_WSDL = 'runners/amdocs/TMobile_MVNO_3_15_development.wsdl';

my $params =
{
  'notification' => 'ProvisioningStatus',
  'data'         =>
  {
    "PortErrorCode" => "",
    "UserData"      => 
    {
      "senderId"  => "ACC",
      "timeStamp" => "2014-07-11T14:24:08"
    },
    "ProvisioningType"      => "ChangeFeatures",
    "MSISDN"                => "3234989330",
    "TransactionIDAnswered" => "API-ULTRA-148092413058",
    "StatusDescription"     => "",
    "CorrelationID"         => "1405113811-UpdatePlanAndFeatures-{AX-E825BE038A68A535-1543A5500EFFF6A6}",
    "ProvisioningStatus"    => 1,
    "PortErrorDesc"         => ""
  }
};

my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

my $config = Ultra::Lib::Util::Config->new();

my $notificationProber = Ultra::Lib::Amdocs::Notification::Prober->new(
  JSON_CODER  => $json_coder,
  LOG_ENABLED => 1,
  WSDL_FILE   => $MVNO_WSDL
);

my $probeResult = $notificationProber->probeNotification( $params->{'notification'} , $params->{'data'} );

print STDOUT Dumper($probeResult);

__END__

