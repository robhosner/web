package Ultra::Lib::Amdocs::Base;


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::DB::SoapLog;
use Ultra::Lib::Util::Miscellaneous;


use base qw(Ultra::Lib::Util::XML);


# SOAP_LOG.TYPE_ID
# SOAP_LOG_ARCHIVE.TYPE_ID
use constant TYPE_ID_CONTROL_OUTBOUND      => 1;
use constant TYPE_ID_CONTROL_INBOUND       => 2;


=head1 NAME

Ultra::Lib::Amdocs::Base

=head1 Perl module for generic methods under Ultra::Lib::Amdocs namespace

=head1 SYNOPSIS

  use Ultra::Lib::Amdocs::Base;

  my $amdocsBaseObject = Ultra::Lib::Amdocs::Base->new(
    JSON_CODER => $json_coder,
    CONFIG     => $config,
  );

  # store SOAP XML into DB.
  my $success = $amdocsBaseObject->storeSOAPXML({
    data_xml         => $request->content,
    type_id          => $type_id,
    method           => $command,
    acc_session_uuid => $uuid,
  });

  my $responseData = $amdocsBaseObject->extractResponseData( $response->content );

=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ LOG_ENABLED } = 1;

  $this->SUPER::_initialize($data);
}


=head4 defaultValues

  Default values to be shown in the prober for Amdocs SOAP calls

=cut
sub defaultValues
{
  my ($this) = @_;

  #TODO: those default values should be accessible by PHP code as well

  return
  {
    # Quality of service, valid values IMMEDIATE, COURTESY. COURTESY means SMS will not be sent middle of night according to NPA
    'qualityOfService'                 => [ 'IMMEDIATE' , 'COURTESY' ],
    'preferredLanguage'                => [ 'es' , 'en' ],
    'ContactData :: preferredLanguage' => [ 'es-US' , 'en-US' ],
    'SuspendReason'                    => [ 'LOST' , 'STOLEN' , 'INVOLUNTARY' ],
    'RestoreReason'                    => [ 'LOST' , 'STOLEN' , 'INVOLUNTARY' ],
    'autoRenew'                        => [ 'false' , 'true' ],
    'poolAccInd'                       => 'false',
    'donorBillingSystem'               => 'PREPAID',
    #'PlanData :: AddOnFeatureList :: AddOnFeature :: ARRAYOF :: autoRenew' => 'false',
    'PlanData :: wholesalePlan'        => [ 32 , 31 ],
    'PlanData :: retailPlanID'         => [ 12001,12002,12003,12004,12005,12707,12708,12901,12801,12802,12803,12804,12805,12806,12807 ],
    'UserData :: channelId' => 'ULTRA',
    'UserData :: senderId'  => 'MVNEACC',
    'UserData :: timeStamp' => Ultra::Lib::Util::Miscellaneous::getTimeStamp(),
    'OriginatorData :: accountSubType' => 'ULTRA',
    'OriginatorData :: accountType'    => 'MVNE',
    'AmountData :: specificationValueName' => 'INITIAL_BALANCE', # what else?
    'PortInData :: specificationValueName' => 'PORTTYPE',
    'PortInData :: specificationValue'     => [ 'PI' , 'RE' ],
  };
}


=head4 extractConfiguration

  $wsdl must be a XML::Compile::WSDL11 object.
  This method uses XML::Compile::WSDL11::explain to get detailed info about all commands defined in the given WSDL.
  Output format:
    $configuration->{ $command }->{ $direction }->{ $id }->{ type }
    $configuration->{ $command }->{ $direction }->{ $id }->{ required }
    $configuration->{ $command }->{ $direction }->{ $id }->{ example }

=cut
sub extractConfiguration
{
  my ($this,$wsdl,$configuration,$display) = @_;

  if ( ! defined $display ) { $display = 0; }

  my $commands = [ sort keys %$configuration ] ;

  my $defaultValues = $this->defaultValues();

  # loop through commands
  while (my $command = shift( @$commands ) )
  {
    # include both request and response
    for my $direction('INPUT','OUTPUT')
    {
      # XML::Compile::WSDL11::explain provide mixed code and test we have to parse :-/
      my $explain = $wsdl->explain($command, PERL => $direction, recurse => 1);

      print "$explain\n" if $display;

      $explain =~ s/([\{\}])/\n$1/g;

      $explain =~ s/[\n\r\s]+\[/\[/mg;

      my $explainRows = [ split(/[\n\r]/,$explain) ];

      my $type     = '';
      my $required = '';
      my $name     = '';
      my $node     = '';
      my $example  = '';

      my $node_exit = 0;

      my $explainRow = shift( @$explainRows);

      # loop though explan text rows
      while ( defined $explainRow )
      {
        chomp $explainRow;

        if ( $explainRow =~ /, }/ )
        { # end of nested content
          $node_exit = 1;
        }
        elsif ( $explainRow =~ /\s (\S+) =>\s*\[\s*$/ )
        { # node with array content
          $node .= ' :: ' . $1 . ' :: ARRAYOF ';
        }
        elsif ( $explainRow =~ /\s (\S+) =>\s*$/ )
        { # node with nested content
          $node .= ' :: ' . $1;
        }
        elsif ( $explainRow =~ /},\s*],/ )
        { # end of nested array content
          $node =~ s/\s*\:\:\s*\S+\s*\:\:\s*ARRAYOF\s*$//;
        }
        elsif ( $explainRow =~ /}/ )
        { # end of nested content
          $node =~ s/\s*\:\:\s*\S+\s*$//;
        }
        elsif ( $explainRow =~ /\s+# is a (.+)$/ )
        { # data type
          $type = $1;
        }
        elsif ( $explainRow =~ /\s+# is (.+)$/ )
        { # required or optional
          $required = $1;
        } # {
        elsif ( $explainRow =~ /\s (\S+) => (.+[^\}\]]),/ )
        { # field name and example value
          $name    = $1;
          $example = $2;
          $example =~ s/^\"//;
          $example =~ s/\"$//;

          if ( $type )
          {
            my $id = ( $node ) ? "$node :: $name" : $name ;

            $id =~ s/^\s*\:\:\s*//;

            # add parsed info to configuration
            $configuration->{ $command }->{ $direction }->{ $id } =
            {
              type     => $type,
              required => ( $required ) ? $required : 'required' ,
              example  => $example,
              default  => undef,
            };

            if ( exists $defaultValues->{ $id } )
            {
              $configuration->{ $command }->{ $direction }->{ $id }->{ default } = $defaultValues->{ $id };
            }
          }
        }

        if ( $node_exit ) { $node = ''; }

        $node_exit = 0;

        $explainRow = shift( @$explainRows);
      }
    }
  }

  return $configuration;
}


=head4 storeSOAPXML

  Store SOAP XML into DB.
  $params:
    data_xml: the SOAP XML string to be stored
    type_id:  the type

=cut
sub storeSOAPXML
{
  my ($this,$params) = @_;

  if ( ! $this->{ DB } )
  {
    $this->log("Error : DB object missing");

    return 0;
  }

  $params->{ data_xml } = $this->extractSOAPXMLEnvelope( $params->{ data_xml } );

  my $dbSoapLog;

  #$this->log("instantiating Ultra::Lib::DB::SoapLog");

  eval
  {
    $dbSoapLog = Ultra::Lib::DB::SoapLog->new( DB => $this->{ DB } , CONFIG => $this->{ CONFIG } );
  };

  if ( $@ || ( ! $dbSoapLog ) )
  {
    $this->log("instantiation error");

    return 0;
  }

  my $sessionID = $this->{ SESSION_ID };

  if ( ( defined $params->{ sessionID } ) && $params->{ sessionID } )
  {
    $sessionID = $params->{ sessionID };
  }

  #$this->log("invoking insertSoapLog with session_id = $sessionID , command = ".$this->{ COMMAND });

  my ($success, $soapLogId) = $dbSoapLog->insertSoapLog({
    %$params,
    msisdn     => $this->{ MSISDN  },
    iccid      => $this->{ ICCID   },
    command    => $this->{ COMMAND },
    tag        => $this->{ TAG     },
    session_id => $sessionID,
  });

  if ( $dbSoapLog->hasErrors() )
  {
    $this->log("There were errors : ".Dumper( $dbSoapLog->getErrors() ));

    $this->addErrors( $dbSoapLog->getErrors() );
  }
  elsif ( ! $success )
  {
    $this->log("insertSoapLog failed");

    $this->addError("insertSoapLog failed");
  }

  # if insertSoapLog successful and is a paid event
  if ( $success && $this->{ PAID_EVENT } && ( $params->{ type_id } == 2 ) )
  {
    $success = $dbSoapLog->insertSoapPaidEvent({
      soap_log_id       => $soapLogId,
      makeitso_queue_id => $params->{ makeitso_queue_id },
      soc_name          => $params->{ soc_name }
    });

    if ( $dbSoapLog->hasErrors() )
    {
      $this->log("There were errors : ".Dumper( $dbSoapLog->getErrors() ));

      $this->addErrors( $dbSoapLog->getErrors() );
    }
    elsif ( ! $success )
    {
      $this->log("insertSoapPaidEvent failed");

      $this->addError("insertSoapPaidEvent failed");
    }
  }

  return ($soapLogId, $success);
}


=head4 configuration

  WSDL commands configuration.
  On the Perl side, we should need to know as less info as possible about those commands.
  $wsdl must be a XML::Compile::WSDL11 object.

=cut
sub configuration
{
  my ($this,$wsdl,$operation,$display) = @_;

  if ( ! defined $display ) { $display = 0; }

  my $configuration = {};

  for my $writer( sort keys % { $wsdl->{XCC_writers} } )
  {
    $writer =~ s/^.+[^\w](\w+)/$1/g;

    if ( ( $writer ne 'Fault' ) && ( $writer ne 'Envelope' ) )
    {
      $configuration->{ $writer } = undef;
    }
  }

  if ( ( defined $operation ) && $operation )
  {
    if ( exists $configuration->{ $operation } )
    {
      $configuration = { $operation => undef };
    }
    else
    {
      $configuration = {};
    }
  }

  return $this->extractConfiguration($wsdl,$configuration,$display);
}


1;


__END__


