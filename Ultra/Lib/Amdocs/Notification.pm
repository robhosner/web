package Ultra::Lib::Amdocs::Notification;


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Config;
use Ultra::Lib::Util::Miscellaneous;


use base qw(Ultra::Lib::Amdocs::Base);


=head1 NAME

Ultra::Lib::Amdocs::Notification

=head1 Amdocs Notifications Handler

=head1 SYNOPSIS

  use Ultra::Lib::Amdocs::Notification;

  my $amdocsNotificationObject = Ultra::Lib::Amdocs::Notification->new( operation => $operation );

  # this will fill the SOAP Notification response
  my $return = { parameters => $amdocsNotificationObject->callbackOperation() };

=cut


use constant RESULT_MESSAGE_SUCCESS       => "Success";
use constant RESULT_MESSAGE_UNKNOWN_ERROR => "An unknown error occurred";
use constant RESULT_CODE_SUCCESS  => 100;
use constant RESULT_CODE_ERROR    => 101;
use constant SOAP_NAMESPACE       => 'http://www.sigvalue.com/acc';
use constant SOAP_NAMESPACE_ALIAS => 'acc';

# SOAP_LOG_ARCHIVE.TYPE_ID
use constant TYPE_ID_NOTIFICATION_OUTBOUND => 4;
use constant TYPE_ID_NOTIFICATION_INBOUND  => 3;


=head4 _initialize

  Specific initialization on object instantiation.

=cut
sub _initialize
{
  my ($this, $data) = @_;

  # object members
  $data->{ LOG_ENABLED } = 1;
  $data->{ CONFIG }      ||= Ultra::Lib::Util::Config->new();

  $this->SUPER::_initialize($data);
}


=head4 operationThrottlingAlert, operationPortOutDeactivation, operationNotificationReceived, operationPortOutRequest, operationProvisioningStatus

  Those methods provide parameters for each notification response.

=cut

sub operationThrottlingAlert
{
  my ($this,$operation,$parameters) = @_;

  return $this->defaultSuccessOperation($operation);
}

sub operationPortOutDeactivation
{
  my ($this,$operation,$parameters) = @_;

  my ( $domain , $user , $pwd ) = $this->{ CONFIG }->ultraApiInternalInfo();

  # invoke internal__PortOutDeactivation
  my $url  = 'https://'.$user.':'.$pwd.'@'.$domain.'/pr/internal/2/ultra/api/internal__PortOutDeactivation';

  my $data =
  {
    MSISDN             => $parameters->{ MSISDN },
    portOutCarrierID   => $parameters->{ PortOutCarrierID },
    portOutCarrierName => $parameters->{ PortOutCarrierName },
    partner_tag        => $this->{ ACTION_UUID },
  };

  $this->log( "url = $url" );

  my ($response,$timeout) = Ultra::Lib::Util::Miscellaneous::userAgentRequest($url,$data,undef,undef);

  if ( $timeout )
  {
    $this->log( "internal__PortOutDeactivation timed out" );
  }
  else
  {
    # $response is a HTTP::Response object
    if ( ref $response )
    {
      $this->log( "response = ".$response->decoded_content );

      my $data;

      eval
      {
        $data = $this->{ JSON_CODER }->decode( $response->decoded_content );
      };

      if ( $@ )
      {
        $this->log( "error while extracting data from internal__PortOutDeactivation response : $@" );
      }
      elsif( ! $data->{ success } )
      {
        $this->log( "internal__PortOutDeactivation failed for MSISDN ".$parameters->{ MSISDN }." : ".$this->{ JSON_CODER }->encode( $data->{ errors } ) );
      }
      else
      {
        $this->log( "internal__PortOutDeactivation success" );
      }
    }
    else
    {
      $this->log( "userAgentRequest did not return a HTTP::Response object : $response" );
    }
  }

  return $this->defaultSuccessOperation($operation);
}

sub operationNotificationReceived
{
  my ($this,$operation,$parameters) = @_;

  return $this->defaultSuccessOperation($operation);
}

sub operationPortOutRequest
{
  my ($this,$operation,$parameters) = @_;

  my $operationResult = $this->defaultSuccessOperation($operation);

  my ( $isPINValid , $isAccountValid ) = $this->validatePortOutRequestMSISDN( $parameters );

  $operationResult->{"IsPINValid"}     = ( $isPINValid     ? 'true' : 'false' ) ;
  $operationResult->{"IsAccountValid"} = ( $isAccountValid ? 'true' : 'false' ) ;

  return $operationResult;
}

sub validatePortOutRequestMSISDN
{
  my ($this,$parameters) = @_;

  $this->log( $this->{ JSON_CODER }->encode( $parameters ) );

  my $isAccountValid = 0;
  my $isPINValid     = 0;

  if ( ( ref $parameters eq 'HASH' )
    && ( defined $parameters->{ MSISDN } )
    && ( $parameters->{ MSISDN } =~ /^\d{10,11}$/ )
    && ( defined $parameters->{ accountNumber } )
    && ( $parameters->{ accountNumber } =~ /^\d+$/ )
  )
  {
    my ( $domain , $user , $pwd ) = $this->{ CONFIG }->ultraApiInternalInfo();

    # invoke internal__ValidateAccountMSISDN
    my $url  = 'https://'.$user.':'.$pwd.'@'.$domain.'/pr/internal/2/ultra/api/internal__ValidateAccountMSISDN';

    my $data =
    {
      account => $parameters->{ accountNumber },
      MSISDN  => $parameters->{ MSISDN },
    };

    my ($response,$timeout) = Ultra::Lib::Util::Miscellaneous::userAgentRequest($url,$data,undef,undef);

    if ( $timeout )
    {
      $this->log( "internal__ValidateAccountMSISDN timed out" );
    }
    else
    {
      # $response is a HTTP::Response object
      if ( ref $response )
      {
        $this->log( "response = ".$response->decoded_content );

        my $data;

        eval
        {
          $data = $this->{ JSON_CODER }->decode( $response->decoded_content );
        };

        if ( $@ )
        {
          $this->log( "error while extracting data from internal__ValidateAccountMSISDN response : $@" );
        }
        elsif( ! $data->{ success } )
        {
          $this->log( "internal__ValidateAccountMSISDN failed for MSISDN ".$parameters->{ MSISDN }." : ".$this->{ JSON_CODER }->encode( $data->{ errors } ) );
        }
        else
        {
          $this->log( "internal__ValidateAccountMSISDN success ; IsPINValid = ".$data->{ IsPINValid }." ; IsAccountValid = ".$data->{ IsAccountValid } );

          ( $isPINValid , $isAccountValid ) = ( $data->{ IsPINValid } , $data->{ IsAccountValid } ) ;
        }
      }
      else
      {
        $this->log( "userAgentRequest did not return a HTTP::Response object : $response" );
      }
    }
  }

  return ( $isPINValid , $isAccountValid );
}

sub operationProvisioningStatus
{
  my ($this,$operation,$parameters) = @_;

  return $this->defaultSuccessOperation($operation);
}


=head4 defaultSuccessOperation

  Default set of success values for notification responses.

=cut
sub defaultSuccessOperation
{
  my ($this,$operation) = @_;

  return
  {
    $operation."Result"  => "1",
    "ResultCode"         => RESULT_CODE_SUCCESS,
    "ResultMsg"          => RESULT_MESSAGE_SUCCESS,
  }
}


=head4 defaultErrorOperation

  Default set of error values for notification responses.

=cut
sub defaultErrorOperation
{
  my ($this,$operation) = @_;

  return
  {
    $operation."Result"  => "0",
    "ResultCode"         => RESULT_CODE_ERROR,
    "ResultMsg"          => RESULT_MESSAGE_UNKNOWN_ERROR,
  }
}


=head4 callbackOperation

  Given the operation, it returns the appropriate parameters for the notification response.

=cut
sub callbackOperation
{
  my ($this,$operation,$parameters) = @_;

  $this->log( "callbackOperation operation = $operation , parameters = ".$this->{ JSON_CODER }->encode( $parameters ) );

  my $return = {};

  # currently we handle the following notifications.
  my $callbackOperationMapper =
  {
    'ThrottlingAlert'      => sub { return $this->operationThrottlingAlert( @_ );      },
    'PortOutDeactivation'  => sub { return $this->operationPortOutDeactivation( @_ );  },
    'NotificationReceived' => sub { return $this->operationNotificationReceived( @_ ); },
    'PortOutRequest'       => sub { return $this->operationPortOutRequest( @_ );       },
    'ProvisioningStatus'   => sub { return $this->operationProvisioningStatus( @_ );   },
  };

  # for those notifications, it's necessary to send an inbound message
  my $shouldEnqueueInbound =
  {
    'ThrottlingAlert'      => 1,
    'NotificationReceived' => 1,
    'ProvisioningStatus'   => 1,
  };

  # can we handle the given operation?
  if ( defined $callbackOperationMapper->{ $operation } )
  {
    if ( exists $shouldEnqueueInbound->{ $operation } )
    {
      # EA-78: do not send inbound message for some ProvisioningStatus (Nightly TMO Autorenew)
      if ( $this->ignoredOperation( $operation , $parameters ) )
      {
        $this->log("operation ignored");
      }
      else
      {
        # inbound message
        my $message = $this->{ mq_endpoint }->buildMessage({
          'header'     => $operation,
          'body'       => $parameters,
          'actionUUID' => $this->getActionUUID(),
        });

        # enqueue $message in the ACC MW Notification Channel
        my $notificationUUID = $this->{ mq_endpoint }->enqueueNotificationChannel($message);

        $this->log("enqueueNotificationChannel returned $notificationUUID");
      }
    }

    # returns the appropriate parameters for $operation
    $return = $callbackOperationMapper->{ $operation }->( $operation , $parameters );
  }
  else
  {
    $this->log("No callback logic implemented for operation $operation");
  }

  $this->log( "returning ".$this->{ JSON_CODER }->encode( $return ) );

  return $return;
}


=head4 ignoredOperation

  Returns a true value if the given operation should be ignored

=cut
sub ignoredOperation
{
  my ($this, $operation, $parameters) = @_;

  # EA-78
  # ignore all ProvisioningType operations with ProvisioningType = 'AutoRenew' and with TransactionIDAnswered starting with 'ATR'

  my $ignoreBoolean =
  (
    ( $operation eq 'ProvisioningStatus' )
    &&
    ( ref $parameters eq 'HASH' )
    &&
    ( defined $parameters->{ 'ProvisioningType' } )
    &&
    ( $parameters->{ 'ProvisioningType' } eq 'AutoRenew' )
    &&
    ( defined $parameters->{ 'TransactionIDAnswered' } )
    &&
    ( $parameters->{ 'TransactionIDAnswered' } =~ /^ATR/ )
  );

  if ( $ignoreBoolean )
  {
    $this->log("ignored $operation with ProvisioningType = ".$parameters->{ 'ProvisioningType' }." and TransactionIDAnswered = ".$parameters->{ 'TransactionIDAnswered' });
  }

  return $ignoreBoolean;
}


=head4 getActionUUID

  Returns the appropriate actionUUID

=cut
sub getActionUUID
{
  my ($this) = @_;

  if ( ! $this->{ ACTION_UUID } )
  {
    if ( $this->{ mq_endpoint } )
    {
      $this->{ ACTION_UUID } = $this->{ mq_endpoint }->_getNewNotificationActionUUID();
    }
    else
    {
      # it should be provided during instantiation
      $this->{ ACTION_UUID } = 'TODO';
    }
  }

  return $this->{ ACTION_UUID };
}


=head4 getSOAPProxy

  SOAP Proxy Url

=cut
sub getSOAPProxy
{
  my ($this) = @_;

  return $this->{ CONFIG }->find_credential('amdocs/notifications/soap/proxy');
}


1;


__END__


'ResultMsg' should be "success" in case of success. The ResultMsg expected from the MVNO is documented in chapter 5.
'ResultCode' should be 100 in case of success. This is documented in “Response Message” sections per partner exposed API in Chapter 5 of the PF API doc.
             What range of "ResultCode" failure values should we provide? [ACC] Each expected error code from the partner (and passed on as-is to ESP) is documented per API in Chapter 5. If EIT/ESP expects different/additional error codes from the MVNOs, please let us know, and the document can be updated.
'ThrottlingAlertResult' should be 1 in case of success.
'PortOutDeactivationResult' should be 1 in case of success.
'NotificationReceivedResult' should be 1 in case of success.
'PortOutRequestResult' should be 1 in case of success.
'ProvisioningStatusResult' should be 1 in case of success.


