<?php

require_once 'db.php';
require_once 'Ultra/Lib/DemoLine.php';

class DemoLineTest extends PHPUnit_Framework_TestCase
{
  protected function setUp()
  {
    teldata_change_db();
  }


  function test_createDemoLine()
  {
    $activations = 20;

    // invalid dealer test
    $result  = \Ultra\Lib\DemoLine\createDemoLine(NULL, DEMO_DEALER_EPP, $activations);
    $this->assertFalse($result);

    // invalid status test
    $result  = \Ultra\Lib\DemoLine\createDemoLine(123, 48, $activations);
    $this->assertFalse($result);

    // successful test
    $result  = \Ultra\Lib\DemoLine\createDemoLine(12345, DEMO_DEALER_EPP, $activations);
    $this->assertTrue($result);
  }


  function test_updateDemoLineStatus()
  {
    // test invalid demo line
    $result = \Ultra\Lib\DemoLine\updateDemoLineStatus(NULL, DEMO_SMS_WARNING, 5);
    $this->assertFalse($result);

    // test invalid status
    $result = \Ultra\Lib\DemoLine\updateDemoLineStatus(123, 'i', 5);
    $this->assertFalse($result);

    // successful test
    $result = \Ultra\Lib\DemoLine\updateDemoLineStatus(10, DEMO_STATUS_ACTIVE, 13226);
    $this->assertTrue($result);
  }


  function test_sendDemoLineSms()
  {
    // test invalid line object
    $line = 1;
    $result = \Ultra\Lib\DemoLine\sendDemoLineSms($line, DEMO_SMS_WARNING);
    $this->assertFalse($result);

    // test line without CUSTOMER_ID
    $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', 1, NULL, array('DEMO_LINE_ID' => 7056));
    $this->assertNotNull($sql);
    list($line) = mssql_fetch_all_objects(logged_mssql_query($sql));
    $this->assertNotEmpty($line);
    $this->assertNull($line->CUSTOMER_ID);
    $result = \Ultra\Lib\DemoLine\sendDemoLineSms($line, DEMO_SMS_WARNING);
    $this->assertFalse($result);

    // test cancelled customer_id demo line (should not happen unless DB malfunction)
    $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', 1, NULL, array('DEMO_LINE_ID' => 7062));
    $this->assertNotNull($sql);
    list($line) = mssql_fetch_all_objects(logged_mssql_query($sql));
    $this->assertNotEmpty($line);
    $this->assertNotEmpty($line->CUSTOMER_ID);
    $status = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $line->CUSTOMER_ID, 'PLAN_STATE');
    $this->assertEquals(STATE_CANCELLED, $status);
    $result = \Ultra\Lib\DemoLine\sendDemoLineSms($line, DEMO_SMS_WARNING);
    $this->assertFalse($result);

    // test invalid SMS template
    $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', 1, NULL, array('DEMO_LINE_ID' => 7053));
    $this->assertNotNull($sql);
    list($line) = mssql_fetch_all_objects(logged_mssql_query($sql));
    $this->assertNotEmpty($line);
    $this->assertNotEmpty($line->CUSTOMER_ID);
    $result = \Ultra\Lib\DemoLine\sendDemoLineSms($line, 'string');
    $this->assertFalse($result);

    // successful test for each template
    foreach (array(DEMO_SMS_WARNING, DEMO_SMS_REMINDER, DEMO_SMS_CANCELLED) as $status)
    {
      $result = \Ultra\Lib\DemoLine\sendDemoLineSms($line, $status);
      $this->assertTrue($result);
    }
  }


  function test_findDealerById()
  {
    // test invalid parameters
    $result = \Ultra\Lib\DemoLine\findDealerById(NULL, NULL);
    $this->assertNull($result);
    $result = \Ultra\Lib\DemoLine\findDealerById(123, NULL);
    $this->assertNull($result);
    $result = \Ultra\Lib\DemoLine\findDealerById(123, 'abc');
    $this->assertNull($result);
    $result = \Ultra\Lib\DemoLine\findDealerById(123, array());
    $this->assertNull($result);

    // test dealer not present
    $dealers = \Ultra\Lib\DemoLine\getEligibleDealers(array('month', -1));
    $this->assertNotEmpty($dealers);
    $this->assertGreaterThan(0, count($dealers));
    $result = \Ultra\Lib\DemoLine\findDealerById(99999999, $dealers);
    $this->assertNull($result);

    // successful test
    $result = \Ultra\Lib\DemoLine\findDealerById($dealers[count($dealers) - 1]->id, $dealers);
    $this->assertNotEmpty($result);
    $this->assertEquals($dealers[count($dealers) - 1]->id, $result->id);
  }


  function test_getActiveLines()
  {
    // function does not accept any parameters, therefore we can only test its result
    $result = \Ultra\Lib\DemoLine\getActiveLines();
    $this->assertNotEmpty($result);
    $this->assertGreaterThan(1, count($result));
    foreach ($result as $line)
      $this->assertContains($line->STATUS, array(DEMO_STATUS_ACTIVE, DEMO_STATUS_WARNING));
  }


  function test_getEligibleDealers()
  {
    // function takes no parameters, therefore we can only test its result
    print_r($result = \Ultra\Lib\DemoLine\getEligibleDealers('month'));
    $this->assertNotEmpty($result);
    $this->assertGreaterThan(0, count($result));
    foreach ($result as $dealer)
    {
      $this->assertNotEmpty($dealer->id);
      $this->assertNotEmpty($dealer->code);
      $this->assertContains($dealer->type, array(DEMO_DEALER_EPP, DEMO_DEALER_1_1));
      $this->assertContains($dealer->eligible, array(1, 2));
    }

    // function takes no parameters, therefore we can only test its result
    print_r($result = \Ultra\Lib\DemoLine\getEligibleDealers('daily'));
    $this->assertNotEmpty($result);
    $this->assertGreaterThan(0, count($result));
    foreach ($result as $dealer)
    {
      $this->assertNotEmpty($dealer->id);
      $this->assertNotEmpty($dealer->code);
      $this->assertContains($dealer->type, array(DEMO_DEALER_EPP, DEMO_DEALER_1_1));
      $this->assertContains($dealer->eligible, array(1, 2));
    }
  }


  function test_dailyEligibilityCheck()
  {
    // check invalid parameter
    $result = \Ultra\Lib\DemoLine\dailyEligibilityCheck('string');
    $this->assertFalse($result);

    // successful test
    $dealers = \Ultra\Lib\DemoLine\getEligibleDealers('daily');
    $result = \Ultra\Lib\DemoLine\dailyEligibilityCheck($dealers);
    $this->assertTrue($result);
  }


  function test_startOfMonthCheck()
  {
    // check invalid parameter
    $result = \Ultra\Lib\DemoLine\startOfMonthCheck('string', 'string');
    $this->assertFalse($result);
    
    // successful test
    $dealers = \Ultra\Lib\DemoLine\getEligibleDealers('month');
    $lines = \Ultra\Lib\DemoLine\getActiveLines();
    $result = \Ultra\Lib\DemoLine\startOfMonthCheck($dealers, $lines);
    $this->assertTrue($result);
  }


  function test_middleOfMonthCheck()
  {
    // check invalid parameter
    $result = \Ultra\Lib\DemoLine\middleOfMonthCheck('string', 'string');
    $this->assertFalse($result);

    // successful test
    $dealers = \Ultra\Lib\DemoLine\getEligibleDealers('daily');
    $lines = \Ultra\Lib\DemoLine\getActiveLines();
    $result = \Ultra\Lib\DemoLine\middleOfMonthCheck($dealers, $lines);
    $this->assertTrue($result);
  }

}

