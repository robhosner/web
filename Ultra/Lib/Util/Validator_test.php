<?php

include_once 'db.php';
include_once 'Ultra/Lib/Util/Validator.php';

#$address = array('street' => '25 5 Av', 'suite' => '23B', 'city' => 'New York', 'state' => 'NY');
#$result = \Ultra\Lib\Util\validateStreetAddress((object)$address);
#print_r($result);

teldata_change_db();

$result = \Ultra\Lib\Util\validatorIccid('test_iccid','1','brand_id_1');
print_r($result);

exit;

list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID','1234','VALID');
echo "( $error , $error_code )\n";

exit;

list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode','00000','in_coverage');
echo "( $error , $error_code )\n";

exit;

$x = \Ultra\Lib\Util\Validator::validate(1,2,3,4);
print_r($x);

exit;

$x = \Ultra\Lib\Util\validatorCountryCode('a');
print_r($x.'a');

$x = \Ultra\Lib\Util\validatorCountryCode('IT');
print_r($x.'IT');

exit;

$test_array = array( 123 , 1111.1111 , .11 , -12 , +12 , 'a' , -1111.1111 , +1111.1111 );

foreach ( $test_array as $value )
{
  list( $error , $error_code ) = \Ultra\Lib\Util\validateTypeReal( 'test' , $value );

  echo "error      = $error\n";
  echo "error_code = $error_code\n";
}

// VYT @ 14-07-15
$result = \Ultra\Lib\Util\validatorMinvalue('name', 0, 1);
echo 'validatorMinvalue: ' . print_r($result, TRUE);
$result = \Ultra\Lib\Util\validatorMinvalue('name', 1, 1);
echo 'validatorMinvalue: ' . print_r($result, TRUE);

// MVNO-3086 - Add validation for country codes
$result = \Ultra\Lib\Util\validatorCountryCode('US');
echo "success = " . (($result == true) ? 'true' : 'false') . "\n";

$result = \Ultra\Lib\Util\validatorCountryCode('USA');
echo "fail on length = " . (($result == true) ? 'true' : 'false') . "\n";

$result = \Ultra\Lib\Util\validatorCountryCode();
echo "fail not supplied = " . (($result == true) ? 'true' : 'false') . "\n";

$result = \Ultra\Lib\Util\validatorCountryCode('ZZ');
echo "fail not found = " . (($result == true) ? 'true' : 'false') . "\n";

// MVNO-3086 - Add validation for countries
$result = \Ultra\Lib\Util\validatorCountryName('United States');
echo "success = " . (($result == true) ? 'true' : 'false') . "\n";

$result = \Ultra\Lib\Util\validatorCountryName('US');
echo "fail on length = " . (($result == true) ? 'true' : 'false') . "\n";

$result = \Ultra\Lib\Util\validatorCountryName();
echo "fail not supplied = " . (($result == true) ? 'true' : 'false') . "\n";


$result = \Ultra\Lib\Util\validatorCountryName('Avalon');
echo "fail not found = " . (($result == true) ? 'true' : 'false') . "\n";

