<?php

namespace Ultra\Lib\Util\Redis;

include_once('Ultra/Lib/Util/Redis.php');

/**
 * Redis Transition class
 *
 * Redis data structures used in Ultra\Lib\Util\Redis\Transition and Ultra\Lib\Util\Redis\Action :
 *  "ultra/sortedset/transitions/$ENVIRONMENT/$PRIORITY"   : Transitions sorted set ( score = time ) 
 *  "ultra/sortedset/transitions/actions/$TRANSITION_UUID" : Actions sorted set ( score = ACTION_SEQ )
 *  "ultra/transition/$TRANSITION_UUID"                    : Transition object uniquely identified by $TRANSITION_UUID
 *  "ultra/action/$ACTION_UUID"                            : Action object uniquely identified by $ACTION_UUID
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Transition
{
  private $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) || !is_object($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * getObject
   *
   * Returns the Transition Object
   *
   * @return array (?)
   */
  public function getObject( $t_uuid )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    return $this->redis->hgetall( 'ultra/transition/'.$t_uuid );
  }

  /**
   * addObject
   *
   * Adds a new Transition Object to the Redis Hash 
   *
   * @return boolean - FALSE in case of issues ; TRUE otherwise
   */
  public function addObject( $t_uuid , $priority , $customer_id , $from_cos_id , $from_plan_state , $to_cos_id , $to_plan_state , $trans_label , $created, $delay = 0, $env = NULL )
  {
    // There are as many sorted sets as ( environments X priorities )
    // The ``score`` is time()
    // In this $t_uuid includes '{' and '}' for easier retrieval

    if (empty($env))
      $env = get_htt_env();

    $zaddResult  = $this->redis->zadd( 'ultra/sortedset/transitions/'.$env.'/'.$priority , time() + $delay, $t_uuid );

    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    // Redis hash by $t_uuid
    $hmsetResult = $this->redis->hmset( 'ultra/transition/'.$t_uuid ,
      array(
        'customer_id'      => $customer_id,
        'from_cos_id'      => $from_cos_id,
        'from_plan_state'  => $from_plan_state,
        'to_cos_id'        => $to_cos_id,
        'to_plan_state'    => $to_plan_state,
        'htt_environment'  => get_htt_env(),
        'transition_label' => $trans_label,
        'priority'         => $priority,
        'created'          => $created
      )
    );

    // add by $customer_id only for transitions from one state to another
    if ( $to_plan_state
      && $from_plan_state
      && ( $to_plan_state != '' )
      && ( $from_plan_state != '' )
    )
    {
      $ttl = 60 * 60 * 24 ;

      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/transition_uuid' , $t_uuid          , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/from_cos_id'     , $from_cos_id     , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/to_cos_id'       , $to_cos_id       , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/from_plan_state' , $from_plan_state , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/to_plan_state'   , $to_plan_state   , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/htt_environment' , get_htt_env()    , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/transition_label', $trans_label     , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/priority'        , $priority        , $ttl );
      #$this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/created'         , $created         , $ttl );
      $this->redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/status'          , 'OPEN'           , $ttl );
    }

    return ( ! ! ( $zaddResult && $hmsetResult ) );
  }

  /**
   * getListByEnvironmentAndPriority
   *
   * Returns the sorted set (by time) identified by $environment and $priority
   *
   * @return array
   */
  public function getListByEnvironmentAndPriority( $environment , $priority )
  {
    $listByEnvironmentAndPriority = $this->redis->zrevrangebyscore( 'ultra/sortedset/transitions/'.$environment.'/'.$priority , '+inf' , '-inf' , TRUE );

    return ( $listByEnvironmentAndPriority )
           ?
           array_reverse( $listByEnvironmentAndPriority )
           :
           array()
           ;
  }

  /**
   * deleteObject
   *
   * Delete the Transition Object and its action sorted set from Redis
   */
  public function deleteObject( $t_uuid )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    $del1_result = $this->redis->del( 'ultra/transition/'.$t_uuid );
    $del2_result = $this->redis->del( 'ultra/sortedset/transitions/actions/'.$t_uuid );

    return ! ! ( $del1_result && $del2_result );
  }

  /**
   * setObjectAttribute
   *
   * Sets or Updates a Transition Object attribute to a value
   */
  public function setObjectAttribute( $t_uuid , $field , $value )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    return $this->redis->hset( 'ultra/transition/'.$t_uuid , $field , $value );
  }

  /**
   * getObjectAttribute
   *
   * Returns the value of a Transition Object attribute
   */
  public function getObjectAttribute( $t_uuid , $field )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    return $this->redis->hget( 'ultra/transition/'.$t_uuid , $field );
  }

  /**
   * getObjectAsArray
   *
   * Returns the Transition Object as an array
   */
  public function getObjectAsArray( $t_uuid )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $t_uuid );

    return $this->redis->hgetall( 'ultra/transition/'.$t_uuid );
  }

}

?>
