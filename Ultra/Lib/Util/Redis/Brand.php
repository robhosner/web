<?php

namespace Ultra\Lib\Util\Redis;

const REDIS_ICCID_BRAND_ID      = 'ultra/iccid/brand_id/';
const REDIS_ICCID_BRAND_ID_TTL  = 600;
const REDIS_MSISDN_BRAND_ID     = 'ultra/msisdn/brand_id/';
const REDIS_MSISDN_BRAND_ID_TTL = 600;

/**
 * setBrandSIM
 *
 * Cache $brand_id associated with $iccid_full
 *
 * @return NULL
 */
function setBrandSIM( $iccid_full , $brand_id )
{
  $redis = new \Ultra\Lib\Util\Redis;

  $redis->set( REDIS_ICCID_BRAND_ID . $iccid_full , $brand_id , REDIS_ICCID_BRAND_ID_TTL );

  return NULL;
}

/**
 * getBrandSIM
 *
 * Gets $brand_id from $iccid_full if cached with setBrandSIM
 *
 * @return integer
 */
function getBrandSIM( $iccid_full )
{
  $redis = new \Ultra\Lib\Util\Redis;

  return $redis->get( REDIS_ICCID_BRAND_ID . $iccid_full );
}

/**
 * setBrandMSISDN
 *
 * Cache $brand_id associated with $msisdn
 *
 * @return NULL
 */
function setBrandMSISDN( $msisdn , $brand_id )
{
  $redis = new \Ultra\Lib\Util\Redis;

  $redis->set( REDIS_MSISDN_BRAND_ID . $msisdn , $brand_id , REDIS_MSISDN_BRAND_ID_TTL );

  return NULL;
}

/**
 * getBrandMSISDN
 *
 * Gets $brand_id from $msisdn if cached with setBrandMSISDN
 *
 * @return integer
 */
function getBrandMSISDN( $msisdn )
{
  $redis = new \Ultra\Lib\Util\Redis;

  return $redis->get( REDIS_MSISDN_BRAND_ID . $msisdn );
}

