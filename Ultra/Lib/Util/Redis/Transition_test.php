<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/Transition.php';

$redis_transition = new \Ultra\Lib\Util\Redis\Transition();

#$x = $redis_transition->getObjectAsArray( '{TX-F8297CE98793A7C6-D5375B0BC48F8105}' );

echo $redis_transition->addObject(
  '{TX-2222222222222222-3333333333333333}',
  1,
  31,
  0,
  'Active',
  0,
  'Active',
  'redis_transition'
)."\n";

echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'customer_id' )."\n";
echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'from_cos_id' )."\n";
echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'from_plan_state' )."\n";
echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'htt_environment' )."\n";
echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'transition_label' )."\n";
echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'priority' )."\n";

echo $redis_transition->setObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'priority' , 5 )."\n";

echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'priority' )."\n";

echo $redis_transition->deleteObject( '{TX-2222222222222222-3333333333333333}' )."\n";

echo $redis_transition->getObjectAttribute( '{TX-2222222222222222-3333333333333333}' , 'priority' )."\n";

?>
