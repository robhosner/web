<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Soap/SendSMSPayload.php';
require_once 'Ultra/Lib/Util/Soap/SendSMSResponse.php';

/**
 * AmdocsSoapClient is a wrapper around PHP's built in SoapClient. You must
 * generate a class for payload and response for each call to the client that
 * matches the structure of the WSDL.
 * Usage examples:
 *  $amdocs = new AmdocsSoapClient();
 *
 *  (SendSMSResponse) $response = $amdocs->call('SendSMS', (SendSMS) $payload);
 *  (QuerySubscriberResponse) $response = $amdocs->call('QuerySubscriber', (QuerySubscriber) $payload);
 *
 * @throws SoapException
 * @author bwalters@ultra.me
 */
class AmdocsSoapClient
{
  private $wsdl;
  private $web_service  = false;
  private $methods      = false;
  private $payload      = false;
  private $command      = false;
  private $tag          = false;
  private $options      = array(
    'cache_wsdl'    => WSDL_CACHE_NONE,
    'trace'         => 1,
    'exceptions'    => true
  );

  const TIMEOUT_SECONDS = 30;

  /**
   * Constructor method. Options passed to the constructor
   * will override default options and are passed to the
   * SoapClient constructor.
   *
   * @param array $options
   * @author bwalters@ultra.me
   */
  public function __construct($options = false, $tag)
  {
    list( $certificateFilename , $certificatePassword ) = \Ultra\UltraConfig\acc_certificate_info();

    if ( empty( $certificateFilename ) )
      \logFatal( 'ERROR ESCALATION ALERT DAILY - we could not retrieve acc certificate file name' );
    else
      $this->options['local_cert']  = $certificateFilename;

    if ( empty( $certificatePassword ) )
      \logFatal( 'ERROR ESCALATION ALERT DAILY - we could not retrieve acc certificate password' );
    else
      $this->options['passphrase']  = $certificatePassword;

    // with the introduction of PHP 5.6 , this is necessary
    $this->options['stream_context'] = stream_context_create(
      array(
        'ssl' => array(
          'allow_self_signed' => true,
          'verify_peer_name'  => false,
        )
      )
    );

    $this->tag  = $tag;
    $this->wsdl = 'runners/amdocs/'.\Ultra\UltraConfig\acc_control_wsdl();

    if ($options)
      foreach ($options as $option => $value)
        $this->options[$option] = $value;

    $this->web_service  = new SoapClient($this->wsdl, $this->options);
    $this->methods      = $this->web_service->__getFunctions();

    return $this;
  }

  /**
   * getMethods
   *
   * Return an array of method definitions from the WSDL.
   *
   * @return array
   * @author bwalters@ultra.me
   */
  public function getMethods()
  {
    return $this->methods;
  }

  /**
   * call
   *
   * Call a command from the WSDL.
   *
   * @param string $command
   * @param class $payload
   * @return class
   * @throws SoapException
   * @author bwalters@ultra.me
   */
  public function call($command, $payload)
  {
    $this->payload = $payload;
    $this->command = $command;
    $this->responseTime = 0;

    $found = false;
    foreach ($this->methods as $soap_method)
    {
      if (strpos($soap_method, "{$command}("))
      {
        $found = true;
        break;
      }
    }

    if (!$found) throw new \Exception("Method {$command} does not exist");

    $callResult = NULL;

    try
    {
      // set timeout
      $currentTimeout = ini_get('default_socket_timeout');
      ini_set('default_socket_timeout', self::TIMEOUT_SECONDS);

      list($startTime, $sec) = explode(" ", microtime());

      $startTime = (float) $startTime + (float) $sec;
      $milliseconds = extract_milliseconds_from_microtime($startTime);
      $this->requestTime = get_utc_date_formatted_for_mssql($startTime) . $milliseconds;

      // actual SOAP call
      $callResult = $this->web_service->{$command}($payload);
    }
    catch (\Exception $e)
    {
      dlog('',"Error: %s",$e);

      ini_set('default_socket_timeout', $currentTimeout);
    }

    ini_set('default_socket_timeout', $currentTimeout);

    list($endTime, $sec) = explode(" ", microtime());
    $endTime = (float) $endTime + (float) $sec;
    $milliseconds = extract_milliseconds_from_microtime($endTime);
    $this->responseTime =  get_utc_date_formatted_for_mssql($endTime) . $milliseconds;

    $duration = substr( ( $endTime - $startTime ) , 0 , 5 );

    if ( empty($callResult) )
      $callResult = new \stdClass();

    // provide important feedback
    dlog('',"ACC Sync %s call duration = %s",$command,$duration);
    // dlog('',"request headers = %s", $this->getLastRequestHeaders());
    dlog('',"callResult = %s", $callResult);

    $this->connectToDatabase();
    $this->logRequest();
    $this->logResponse();

    if ( ( $endTime - $startTime ) > self::TIMEOUT_SECONDS )
    {
      dlog('','ACC Sync %s call timeout after %s seconds',$command,$duration);

      $callResult->isTimeout = TRUE;
    }

    return $callResult;
  }

  /**
   * Get the last request's headers.
   *
   * @return string
   * @author bwalters@ultra.me
   */
  public function getLastRequestHeaders()
  {
    return $this->web_service->__getLastRequestHeaders();
  }

  /**
   * getLastRequest
   *
   * Get the last request's body.
   *
   * @return string
   * @author bwalters@ultra.me
   */
  public function getLastRequest()
  {
    return $this->web_service->__getLastRequest();
  }

  /**
   * getLastResponseHeaders
   *
   * Get the last response's headers.
   *
   * @return string
   * @author bwalters@ultra.me
   */
  public function getLastResponseHeaders()
  {
    return $this->web_service->__getLastResponseHeaders();
  }

  /**
   * getLastResponse
   *
   * Get the last response's body.
   *
   * @return string
   * @author bwalters@ultra.me
   */
  public function getLastResponse()
  {
    return $this->web_service->__getLastResponse();
  }

  protected function connectToDatabase()
  {
    $connection = \Ultra\Lib\DB\ultra_acc_connect();
  }

  /**
   * logRequest
   *
   * Logs requests to the soap_log table
   * @author bwalters@ultra.me
   */
  protected function logRequest()
  {
    $request  = $this->getLastRequest();

    $query    = soap_log_insert_query(array(
      'soap_date'   => $this->requestTime,
      'data_xml'    => str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $this->getLastRequest()), // TODO: escape unicode ( see MVNO-2928 )
      'type_id'     => 1,
      'msisdn'      => $this->getKey('msisdn', $request),
      'iccid'       => $this->getKey('iccid', $request),
      'tag'         => $this->tag,
      'session_id'  => $this->getKey('serviceTransactionId', $request),
      'command'     => $this->command,
      'env'         => get_htt_env(),
      'result_code' => NULL
    ));


    if (!is_mssql_successful(logged_mssql_query($query)))
    {
      dlog("", "ERR_API_INTERNAL: DB ERROR - Could not insert into SOAP_LOG");
    }
  }

  /**
   * logResponse
   *
   * Logs responses to the soap_log table.
   * @author bwalters@ultra.me
   */
  protected function logResponse()
  {
    $request  = $this->getLastRequest();
    $response = $this->getLastResponse();

    if ( ! $response )
    {
// TODO: riz wants to store something here ?
// http://wiki.hometowntelecom.com:8090/pages/viewpage.action?title=How+It+Works+%3A+SOAP_LOG+in+ULTRA_ACC&spaceKey=SPEC

      dlog('',"skip due to empty response");

      return NULL;
    }

    $query    = soap_log_insert_query(array(
      'soap_date'   => $this->responseTime,
      'data_xml'    => str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $response),
      'type_id'     => 2,
      'msisdn'      => $this->getKey('msisdn', $request),
      'iccid'       => $this->getKey('iccid', $request),
      'tag'         => $this->tag,
      'session_id'  => $this->getKey('serviceTransactionId', $response),
      'command'     => $this->command,
      'env'         => get_htt_env(),
      'result_code' => $this->getKey('ResultCode', $response)
    ));

    if (!is_mssql_successful(logged_mssql_query($query)))
    {
      dlog("", "ERR_API_INTERNAL: DB ERROR - Could not insert into SOAP_LOG");
    }
  }

  /**
   * getKey
   */
  public function getKey($needle, $xml)
  {
    $needle = strtolower($needle);
    $parser = xml_parser_create();
    xml_parse_into_struct($parser, $xml, $haystack);
    xml_parser_free($parser);

    foreach ($haystack as $item)
    {
      if (isset($item['tag']) && isset($item['value']))
      {
        $tag = strtolower($item['tag']);
        if (strpos($tag, $needle) !== FALSE)
        {
          return $item['value'];
        }
      }
    }

    return NULL;
  }

  /**
   * getKeyRecursive
   *
   * Will return a given key from an xml
   * element or false on failure.
   *
   * @return string|false
   * @author bwalters@utltra.me
   */
  public function getKeyRecursive($needle, $haystack)
  {
    foreach ($haystack as $key => $value)
    {
      $needle = strtolower($needle);
      $key = strtolower($key);

      if ($needle === $key)
      {
        return (string) $value;
      }
      else
      {
        $returned = $this->getKeyRecursive($needle, $value);
        if ($returned !== FALSE)
        {
          return (string) $returned;
        }
      }
    }

    return false;
  }
}

