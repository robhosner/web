<?php

include_once('db.php');
include_once('lib/provisioning/functions.php');
include_once('Ultra/Lib/Util/Settings.php');

teldata_change_db();

$settings = new \Ultra\Lib\Util\Settings;

$all    = $settings->checkUltraSettings('ultra/activations/enabled');
$portal = $settings->checkUltraSettings('ultra/activations/dealer_portal/enabled');

echo 'ALL    : '.$all."\n";
echo 'PORTAL : '.$portal."\n";

if ( activations_enabled( TRUE  ) ) { echo "DP  ACT ON\n"; } else { echo "DP  ACT OFF\n"; }
if ( activations_enabled( FALSE ) ) { echo "ALL ACT ON\n"; } else { echo "ALL ACT OFF\n"; }

exit;

$value = $settings->checkUltraSettings('amdocs/autofail');

echo "value = $value\n";

$value = $settings->checkUltraSettings('test_failure');

echo "value = $value\n";

$settings->forceDBRefreshSettings('test');

$value = $settings->checkUltraSettings('test');

echo "value = $value\n";

?>
