use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::Util::Config;
use Ultra::Lib::Util::Redis;


my $config = Ultra::Lib::Util::Config->new();

my $ultraRedis = Ultra::Lib::Util::Redis->new( CONFIG => $config );

my $info = $ultraRedis->monitorInfo();

print Dumper($info);

print $ultraRedis->{ REDIS_OBJECT }->smembers('ULTRA_MW/OUTBOUND/SYNCH');

exit;

print "\nget : ".$ultraRedis->{ REDIS_OBJECT }->get('setnx_test_Perl')."\n";

$ultraRedis->{ REDIS_OBJECT }->setnx('setnx_test_Perl',200);

$ultraRedis->{ REDIS_OBJECT }->expire('setnx_test_Perl',200);

print "\nget : ".$ultraRedis->{ REDIS_OBJECT }->get('setnx_test_Perl')."\n";

$ultraRedis->{ REDIS_OBJECT }->setnx('setnx_test_Perl',300);

print "\nget : ".$ultraRedis->{ REDIS_OBJECT }->get('setnx_test_Perl')."\n";

__END__

