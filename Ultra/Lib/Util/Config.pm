package Ultra::Lib::Util::Config;


use strict;
use warnings;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


use lib "$ENV{HTT_CONFIGROOT}";


# Do not blame me
our %e_config;
our %cred_config;


use JSON::XS;


=head1 NAME

Ultra::Lib::Util::Config

Ultra Configuration Tokens

=head1 SYNOPSIS

  my $config = Ultra::Lib::Util::Config->new();

  my $token = $config->find_credential('some/configuration/token');

=cut


=head4 credentials

  The whole credential hash.
  I did not design this.

=cut
sub credentials
{
  my ($this) = @_;

  if ( ! $this->{ CREDENTIALS } )
  {
    my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

    $this->{ CREDENTIALS } = {};

    my $credentials = $this->envConfigValue('credentials');

    for my $credential_name(split(/ /,$credentials))
    {
      my $credential_dir = $ENV{HTT_CONFIGROOT}.'/'.$credential_name;

      if ( -e "$credential_dir/cred.pm" )
      {
        # load Pel Module which re-declares %cred_config
        require "$credential_dir/cred.pm";

        $this->{ CREDENTIALS }->{$credential_name} = { %cred_config } unless $this->{ CREDENTIALS }->{$credential_name};
      }

      # load JSON file
      my $jsonFile = $credential_dir . '/cred.json';

      my $status = open my $fh, '<', $jsonFile;

      if ( $status )
      {
        my $jsonContent = do { local $/; <$fh> };

        my $decodedContent = $json_coder->decode( $jsonContent );

        if ( ref $decodedContent eq 'HASH' )
        {
          # load credentials
          while ( my ($key, $value) = each %$decodedContent )
          {
            $this->{ CREDENTIALS }->{$credential_name}->{ $key } = $value;
          }
        }
        else
        {
          print STDERR "$jsonFile has no data\n";
        }

        close $fh;
      }
      else
      {
        print STDERR "Cannot open file $jsonFile for reading\n";
      }
    }
  }

  return $this->{ CREDENTIALS };
}


=head4 envConfigValue

  Returns the value for the given environment token

=cut
sub envConfigValue
{
  my ($this,$token) = @_;

  unless ( keys %e_config )
  {
    # load main configuration tokens from environment.json
    my $jsonFile = $ENV{HTT_CONFIGROOT} . '/environment.json';

    my $status = open my $fh, '<', $jsonFile;

    if ( $status )
    {
      my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

      # slurp-load in 1 line
      my $jsonContent = do { local $/; <$fh> };

      my $decodedContent = $json_coder->decode( $jsonContent );

      if ( ref $decodedContent ne 'HASH' )
      {
        print STDERR "environment.json content is invalid";
      }
      else
      {
        # dereference
        %e_config = %$decodedContent;
      }
    }
    else
    {
      print STDERR "Cannot open file $jsonFile for reading\n";
    }
  }

  return $e_config{ $token };
}


=head4 find_credential

  Returns the value for the given credential key

=cut
sub find_credential
{
  my ($this,$sKey) = @_;

  my $return = '';

  my $credentials = $this->credentials();

  for my $cred_name( keys %$credentials )
  {
    for my $key( keys % { $credentials->{$cred_name} } )
    {
      if ( $key eq $sKey )
      {
        $return = $credentials->{$cred_name}->{$key};
      }
    }
  }

  warn "** could not find credential token $sKey **" unless $return;

  return $return;
}


=head4 find_credential_from_file

  The value for the given credential key is a file name.
  This method returns the content of that file.

=cut
sub find_credential_from_file
{
  my ($this,$sKey) = @_;

  my $fileName = $this->find_credential($sKey);

  my $value = '';

  if ( ! $fileName )
  {
    warn "** could not retrieve value for credential token $sKey **";
  }
  elsif ( ! -f $fileName )
  {
    warn "** file $fileName does not exist or it's not accessible **";
  }
  elsif ( ! -r $fileName )
  {
    warn "** could not read file $fileName **";
  }
  elsif ( open CFH , $fileName )
  {
    $value = <CFH>;

    chomp $value;

    close CFH;
  }
  else
  {
    warn "** could not retrieve value for credential token $sKey (cannot open file $fileName) **";
  }

  return $value;
}


=head4 getDBName

  Returns the default DB name

=cut
sub getDBName
{
  my ($this) = @_;

  return $this->find_credential('db/name');
}


=head4 isDevelopmentDB

  Returns true if the default DB name is DEV

=cut
sub isDevelopmentDB
{
  my ($this) = @_;

  return ( $this->getDBName() eq 'ULTRA_DEVELOP_TEL' );
}


=head4 ultraApiInternalInfo

  Returns domain and credential for internal API calls

=cut
sub ultraApiInternalInfo
{
  my ($this) = @_;

  return (
    $this->find_credential('ultra/api/internal/domain'),
    $this->find_credential('ultra/api/internal/user'),
    $this->find_credential('ultra/api/internal/password')
  );
}


=head4 soapLogRedoLog

  Redo log for SOAP_LOG

=cut
sub soapLogRedoLog
{
  my ($this) = @_;

  return $this->find_credential('amdocs/redo_log');
}

1;

__END__


Example:
export HTT_CONFIGROOT=/home/ht/config/rgalli4_dev/
export HTT_ENV=rgalli4_dev

