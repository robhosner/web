<?php

namespace Ultra\Lib;

require_once 'classes/Rodeo.php';

require_once 'classes/UltraException.php';
require_once 'classes/Outcome.php';
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/StateMachine/Strategy.php';
require_once 'lib/util-common.php';

/**
 * Object Oriented version of State Machine
 *
 * You may find some (old) useful info under http://wiki.hometowntelecom.com:8090/display/SPEC/How+it+works+-+Transition+Log
 * Data storage and state machine configuration are completely abstracted from this class.
 * Data storage is handled by the object contained in $this->dataEngine ( dependency injection pattern )
 * State Machine configuration is contained in $this->configFile
 * Multiple State Machine configurations can co-exist in the same environment
 * Actions are defined in $NAMESPACE/Lib/StateMachine/Action/functions.php
 * Requirement validation functions are in $NAMESPACE/Lib/StateMachine/Requirement/functions.php
 *
 *
 * How to use this class:
 *   Initially it's necessary to instantiate the Data Engine.
 *   For Ultra it will be MSSQL:
 *
 * $dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;
 *
 *   We also need a Redis connection:
 *
 * $redis = new \Ultra\Lib\Util\Redis();
 *
 *   We are ready to instantiate the State Machine object:
 *
 * $sm = new \Ultra\Lib\StateMachine( $dataEngineRedisObject , NULL , NULL , $redis );
 *
 *   The default location of the configuration file can be overridden (good for testing):
 *
 * $sm = new \Ultra\Lib\StateMachine( $dataEngineRedisObject , $configDirectory , $configFile , $redis );
 *
 *   We need customer data now.
 *   The full list of acceptable customer fields is in the docs for method setCustomerData.
 *
 * $sm->setCustomerData(
 *   array(
 *     'customer_id' => 31,
 *     'cos_id'      => 123,
 *     'plan_state'  => 'Neutral',
 *     ...
 *   )
 * );
 *
 *   We can select the transition we want to execute:
 *
 * $sm->selectTransitionByLabel( $label , $state );
 *
 *   We generate the state transition data:
 *
 * $sm->generateTransitionData();
 *
 *   Let's check if that worked ...
 *
 * if ( $sm->is_failure() ) { ... something went wrong ... }
 *
 *   If that worked, we can run the transition:
 *
 * $sm->runTransition();
 *
 *   Let's check if that worked ...
 *
 * if ( $sm->is_failure() ) { ... something went wrong ... }
 *
 *   Important: $maxTransitionRunTime should be set to 0 if $dataEngine is not MSSQL
 *
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project State Machine Complete Refactoring
 */
class StateMachine extends \Outcome
{
  private $redis = NULL;

  protected $configDirectory;       // directory path to state machine configuration file
  protected $configFile;            // configuration file name
  protected $configuration;         // complete state machine definition
  protected $forwardStar;           // array $state => definition of state transitions
  protected $currentTransition;     // definition of current state transition
  protected $currentActionData;     // data associated with current action
  protected $dataEngine;            // container for DB dependency injection
  protected $customerData;          // customer's data
  protected $nameSpace;             // namespace for Actions and Requirements
  protected $strategy;

  protected $customConfiguration = FALSE;

  public $maxTransitionRunTime = 120; // for how many seconds we should continue running actions for the same transition

  const DEFAULT_CONFIG_DIRECTORY = 'Ultra/Lib/StateMachine/';
  const DEFAULT_CONFIG_FILE      = 'ultra.json';
  const TRANS_RES_CUSTOMER_ID_PREFIX = 'transition/resolving/customer_id/';
  const TRANS_RES_UUID_PREFIX        = 'transition/resolving/uuid/';

  /**
   * Class constructor
   *
   * @param \Ultra\Lib\StateMachine\DataEngineInterface $dataEngine
   * @param string $configDirectory
   * @param string $configFile
   * @param \Ultra\Lib\Util\Redis $redis
   * @param string $nameSpace
   */
  public function __construct( \Ultra\Lib\StateMachine\DataEngineInterface $dataEngine , $configDirectory=NULL , $configFile=NULL , \Ultra\Lib\Util\Redis $redis=NULL , $nameSpace='Ultra', \Ultra\Lib\Strategy $strategy=null )
  {
    // configuration file location
    $this->configDirectory = empty( $configDirectory ) ? self::DEFAULT_CONFIG_DIRECTORY : $configDirectory ;
    $this->configFile      = empty( $configFile      ) ? self::DEFAULT_CONFIG_FILE      : $configFile      ;

    // Data Engine object must be provided
    $this->dataEngine = $dataEngine;

    // propagate redis connection if possible
    $this->redis = empty( $redis ) ? new \Ultra\Lib\Util\Redis() : $redis ;

    // set namespace for Actions and Requirements
    $this->nameSpace = $nameSpace;

    if ( !empty( $strategy ) )
    {
      $this->strategy = $strategy;
    }
    else
    {
      // default to namespace strategy
      $strategy = '\\' . $this->nameSpace.'\Lib\StateMachine\\' . $this->nameSpace . 'Strategy';
      
      require_once $this->nameSpace . '/Lib/StateMachine/' . $this->nameSpace . 'Strategy.php';
      $this->strategy = new $strategy();
    }

    $this->succeed();
  }

  /**
   * getForwardStar
   *
   * Loads transitions which depart from $state
   *
   * @param string $state
   * @return boolean
   */
  public function getForwardStar( $state )
  {
    $this->forwardStar[ $state ] = array();

    if ( ! $this->customConfiguration)
      $this->loadConfiguration();

    if ( $this->is_success() )
    {
      // dlog('',"configuration = %s",$this->configuration);

      // loop through all transitions
      foreach( $this->configuration->transitions as $transition )
        if ( $transition->from_plan_state == $state )
          $this->forwardStar[ $state ][] = $transition;

      if ( count( $this->forwardStar[ $state ] ) )
        $this->succeed();
      else
        $this->add_errors_and_code( 'ERROR: there are no transitions associated with state ' . $state , 'IN0001' );
    }

    return $this->is_success();
  }

  /**
   * runNextAction
   *
   * Execute next action
   *  - get next action data from data engine
   *  - verify that the action type can be handled
   *  - invoke action executor
   *  - record action result using data engine
   *
   * @return NULL
   */
  public function runNextAction()
  {
    $this->currentActionData = $this->dataEngine->getNextActionData();

    if ( empty( $this->currentActionData ) )
      $this->unset_pending();
    else
    {
      // run action
      dlog('',"running next action : %s",$this->currentActionData);

      // sets 'pending_since' for the current action
      $this->dataEngine->recordActionPending();

      // function which will execute the action
      $actionExecutor = 'executeAction'.ucfirst( strtolower( $this->currentActionData['action_type'] ) );

      dlog('',"actionExecutor : %s",$actionExecutor);

      // sanity check
      if ( is_callable( array ( $this , $actionExecutor ) ) )
        $this->$actionExecutor();
      else
        $this->add_errors_and_code( 'ERROR: action type '.$this->currentActionData['action_type'].' is not handled.' , 'IN0001' );

      // record action result
      $error = $this->dataEngine->recordActionOutcome( $this );

      if ( $error )
        // this is pretty bad, because it's a fatal error at data engine level
        dlog('',"recordActionOutcome FAILED : %s",$error);

      if ( $this->is_failure() )
      {
        dlog('',"action %s FAILED : %s",$this->currentActionData['action_uuid'],$this->get_errors());

        // do not execute more actions after this one
        $this->unset_pending();
      }
      else
      {
        dlog('',"action %s SUCCEEDED",$this->currentActionData['action_uuid']);

        // execute more actions after this one
        $this->set_pending();
      }
    }

    return NULL;
  }

  /**
   * executeActionDefault
   *
   * Execute a single action of type DEFAULT
   * DEFAULT actions are simple PHP functions defined in $this->nameSpace/Lib/StateMachine/Action/functions.php
   *
   * @return NULL
   */
  protected function executeActionDefault()
  {
    \logDebug("action_name = ".$this->currentActionData['action_name']);

    // action parameters are included as a '?' separated list after action name in the state machine JSON configuration
    $actionComponents = explode('?',$this->currentActionData['action_name']);

    // PHP function which will be called
    $function = $this->strategy->getActionFunction( array_shift( $actionComponents ) );

    $params = array();
    foreach ($actionComponents as $component)
    {
      $keyval = explode(':', $component);
      $params[$keyval[0]] = $keyval[1];
    }

    \logDebug("function = $function");
    \logDebug("params   = ".\toKeyValString($params));

    // sanity check
    if ( is_callable( $function ) )
      // generic action PHP function
      $actionOutcome = $function( $this->customerData , $this->dataEngine->currentTransitionData , $this->currentActionData['action_uuid'] , $params );
    else
      $actionOutcome = make_error_Outcome( 'ERROR: DEFAULT action ' . $this->currentActionData['action_name'] . ' is not defined.' , 'IN00001' );

    dlog('',"actionOutcome = %s",$actionOutcome);

    // update customer's data if there were changes
    $this->updateCustomerDataFromActionOutcome( $actionOutcome );

    $this->merge( $actionOutcome );

    return NULL;
  }

  /**
   * updateCustomerDataFromActionOutcome
   *
   * Handle customer data side effects
   *
   * @return NULL
   */
  protected function updateCustomerDataFromActionOutcome( $actionOutcome )
  {
    if ( ! empty( $actionOutcome->data_array['customerData'] ) )
      $this->customerData = $actionOutcome->data_array['customerData'];

    return NULL;
  }

  /**
   * runTransition
   *
   * Run the current transition instance.
   * generateTransitionData or loadTransitionData must have been invoked before this method
   *
   * @return boolean
   */
  public function runTransition($reserve = TRUE)
  {
    $this->succeed();

    // check prerequisites before running the transition
    $this->checkPrerequisites();
    if ( ! $this->is_success())
      return FALSE;

    dlog('',"%s",$this->dataEngine->currentTransitionData);

    $start = intval(microtime(TRUE) * 1000);

    // ULTRA reserves transitions and customers using a different PID format than PRIMO
    // $reserve allows the StateMachine to skip over this reservation check as it would
    // conflict with existing reservation logic in certain DB functions
    if ($reserve)
    {
      // reserve customer_id
      if ( ! $this->reserveCustomer() )
        return TRUE;

      // reserve transition_uuid
      if ( ! $this->reserveTransition() )
        return TRUE;
    }

    $this->set_pending();

    $stillPending = FALSE;

    // loop through all actions in the current transition
    while ( $this->is_pending() )
    {
      $this->runNextAction();

      // check transition time limit
      if ( ! empty($this->maxTransitionRunTime) && is_numeric($this->maxTransitionRunTime) )
      {
        dlog('',"elapsed time = %03d ms", ( intval(microtime(TRUE) * 1000) ) - $start );

        if ( ( $this->maxTransitionRunTime * 1000 ) <= ( ( intval(microtime(TRUE) * 1000) ) - $start ) )
        {
          $stillPending = TRUE;

          $this->unset_pending();
        }
      }
    }

    $end = intval(microtime(TRUE) * 1000);

    if ( ! $stillPending )
    {
      $error = '';

      if ( $this->is_failure() )
        $error = $this->abortTransition();
      else
        $error = $this->closeTransition();

      if ( $error )
        // this is pretty bad, because it's a fatal error at data engine level
        dlog('',"transition update FAILED : %s",$error);
      elseif( ! $this->is_failure() )
      {
        // update plan_state to new state if needed
        $error = $this->updatePlanStateAfterTransition();

        if ( $error )
          // this is pretty bad, because it's a fatal error at data engine level
          dlog('',"plan state update FAILED : %s",$error);
      }
    }
    else
      \logDebug("transition still pending or aborted");

    \logDebug("freeing customer and transition");

    $this->freeCustomer();
    $this->freeTransition();

    $end = intval(microtime(TRUE) * 1000);

    dlog('',"Execution time = %03d ms", $end - $start );

    return $this->is_success();
  }

  /**
   * reserveCustomer
   *
   * Attempts to reserve a Customer Id using Redis
   *
   * @return boolean - TRUE if success , FALSE if failure
   */
  protected function reserveCustomer()
  {
    $reserved = FALSE;

    $myPID = gethostname() . getmypid() . rand(1, 9999);

    dlog('','Process Id '.$myPID.' attempts to reserve customer_id = "'.$this->dataEngine->currentTransitionData['customer_id'].'"');

    if ( empty( $this->dataEngine->currentTransitionData['customer_id'] ) )
      return FALSE;

    $redisKey = self::TRANS_RES_CUSTOMER_ID_PREFIX.$this->dataEngine->currentTransitionData['customer_id'];

    $value = $this->redis->get($redisKey);

    if ( ! $value )
    {
      $this->redis->setnx( $redisKey , $myPID , 1*60 ); # ttl is 15 minutes

      $value = $this->redis->get($redisKey);

      $reserved = ! ! ( $value == $myPID );
    }

    return $reserved;
  }

  /**
   * reserveTransition
   *
   * Attempts to reserve a Transition UUID using Redis
   *
   * @return boolean - TRUE if success , FALSE if failure
   */
  protected function reserveTransition()
  {
    $reserved = FALSE;

    $myPID = gethostname() . getmypid() . rand(1, 9999);

    dlog('','Process Id '.$myPID.' attempts to reserve transition_uuid = "'.$this->dataEngine->currentTransitionData['transition_uuid'].'"');

    if ( empty( $this->dataEngine->currentTransitionData['transition_uuid'] ) )
      return FALSE;

    $t_uuid = preg_replace( '/[\{\}]/' , '' , $this->dataEngine->currentTransitionData['transition_uuid'] );

    $redisKey = self::TRANS_RES_UUID_PREFIX.$t_uuid;

    $value = $this->redis->get($redisKey);

    if ( ! $value )
    {
      $this->redis->setnx( $redisKey , $myPID , 15*60 ); # ttl is 15 minutes

      $value = $this->redis->get($redisKey);

      $reserved = ! ! ( $value == $myPID );
    }

    return $reserved;
  }

  /**
   * freeCustomer
   *
   * Attempts to unreserve a Customer Id using Redis
   *
   * @return boolean - TRUE if success , FALSE if failure
   */
  protected function freeCustomer()
  {
    dlog('','Process Id '.getmypid().' attempts to free customer_id = "'.$this->dataEngine->currentTransitionData['customer_id'].'"');

    if ( empty( $this->dataEngine->currentTransitionData['customer_id'] ) )
      return FALSE;

    $redisKey = self::TRANS_RES_CUSTOMER_ID_PREFIX.$this->dataEngine->currentTransitionData['customer_id'];

    return $this->redis->del( $redisKey );

    return true;
  }

  /**
   * freeTransition
   *
   * Attempts to unreserve a Transition UUID using Redis
   *
   * @return boolean - TRUE if success , FALSE if failure
   */
  protected function freeTransition()
  {
    dlog('','Process Id '.getmypid().' attempts to free transition_uuid = "'.$this->dataEngine->currentTransitionData['transition_uuid'].'"');

    if ( empty( $this->dataEngine->currentTransitionData['transition_uuid'] ) )
      return FALSE;

    $t_uuid = preg_replace( '/[\{\}]/' , '' , $this->dataEngine->currentTransitionData['transition_uuid'] );

    $redisKey = self::TRANS_RES_UUID_PREFIX.$t_uuid;

    return $this->redis->del( $redisKey );
  }

  /**
   * checkPrerequisites
   *
   * Verifies that the customer data is OK with this particular state transition requirements
   *
   * @return NULL
   */
  public function checkPrerequisites()
  {
    $this->succeed();

    if ( ! empty( $this->currentTransition['requirements'] ) )
      foreach ( $this->currentTransition['requirements'] as $requirement )
        if ( $this->is_success() )
          $this->checkPrerequisite( $requirement );

    return NULL;
  }

  /**
   * checkPrerequisite
   *
   * Verifies that the customer data is OK with this particular state transition requirement
   * Requirements are defined in PHP functions from $this->nameSpace/Lib/StateMachine/Requirement/functions.php
   *
   * @return NULL
   */
  public function checkPrerequisite( $requirement )
  {
    // requirement parameters are included as a '?' separated list after requirement name in the state machine JSON configuration
    $requirementComponents = explode('?',$requirement);

    // PHP function which will validate the requirement
    $function = $this->strategy->getRequirementFunction( array_shift( $requirementComponents ) );

    $params = array();
    foreach ($requirementComponents as $component)
    {
      $keyval = explode(':', $component);
      $params[$keyval[0]] = $keyval[1];
    }

    // sanity check
    if ( is_callable( $function ) )
      $requirementOutcome = $function( $this->customerData , $params );
    else
      $requirementOutcome = make_error_Outcome( 'ERROR: requirement ' . $requirement . ' is not defined.' , 'IN0001' );

    $this->merge( $requirementOutcome );

    return NULL;
  }

  /**
   * updatePlanStateAfterTransition
   *
   * Update plan state to arrival plan state
   *
   * @return string
   */
  public function updatePlanStateAfterTransition()
  {
    \logDebug('updatePlanStateAfterTransition');
    \logDebug('from_plan_state = '.$this->dataEngine->currentTransitionData['from_plan_state']);
    \logDebug('to_plan_state   = '.$this->dataEngine->currentTransitionData['to_plan_state']);

    $error = '';

    if (
         ($this->dataEngine->currentTransitionData['from_plan_state'] != $this->dataEngine->currentTransitionData['to_plan_state'])
      || ($this->dataEngine->currentTransitionData['from_cos_id']     != $this->dataEngine->currentTransitionData['to_cos_id'])
    )
    {
      $error = $this->dataEngine->updatePlanState( $this->dataEngine->currentTransitionData['to_plan_state'] , $this->customerData['customer_id'] );
    }

    return $error;
  }

  /**
   * closeTransition
   *
   * Closes the current transition
   *
   * @return string
   */
  public function closeTransition()
  {
    \logDebug('closeTransition');

    $this->succeed();

    return $this->dataEngine->closeTransition();
  }

  /**
   * abortTransition
   *
   * Aborts the current transition
   *
   * @return string
   */
  public function abortTransition()
  {
    \logDebug('abortTransition');

    // $this->succeed();

    return $this->dataEngine->abortTransitionAndActions();
  }

  public function injectConfiguration($configuration)
  {
    $this->configuration = $configuration;
    $this->customConfiguration = TRUE;
  }

  /**
   * loadConfiguration
   *
   * Import State machine Configuration from $this->configFile
   *
   * @return boolean
   */
  public function loadConfiguration()
  {
    $fileName = $this->configDirectory . $this->configFile ;

    try
    {
      // sanity check
      if ( ! file_exists( $fileName ) )
        throw new \UltraException( 'ERROR: Cannot access configuration file ' . $fileName , 0 , NULL , 'IN0001' );

      // load the whole content of $fileName
      // $fileContent = file_get_contents( $fileName );

      // sanity check
      // if ( empty( $fileContent ) )
      //   throw new \UltraException( 'ERROR: Configuration file ' . $fileName . ' is empty' , 0 , NULL , 'IN0001' );

      // decode JSON configuration
      // $fileContent = json_decode( $fileContent );

      $planConfig = \PlanConfig::Instance();
      $rodeo = new \Rodeo();
      $rodeo->setMap($planConfig->getRodeoMap($this->brandId));
      $rodeo->loadFile(self::DEFAULT_CONFIG_DIRECTORY . 'parsed_' . $this->strategy->getBrandConfig( $this->brandId ));
      $rodeo->setPlanGroups($planConfig->getPlanGroups($this->brandId));
      $rodeo->parseFile();
      $fileContent = json_decode($rodeo->getJSON());

      // sanity check
      if ( empty( $fileContent ) )
        throw new \UltraException( 'ERROR: Configuration file ' . $fileName . ' is malformed' , 0 , NULL , 'IN0001' );

      $this->configuration = $fileContent;
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', $e->getMessage());

      if ( is_a( $e , 'UltraException' ) )
        $this->add_errors_and_code( $e->getMessage() , $e->get_error_code() );
      else
        $this->add_errors_and_code( $e->getMessage() , 'IN0001' );
    }

    return $this->is_success();
  }

  /**
   * find transition in configuration when transition label is unknown
   * similar to selectTransitionByLabel
   *
   * @param  String $from_state state customer is coming from
   * @param  String $to_state   state customer is going to
   * @param  String $from_plan  plan customer is coming from
   * @return Bool
   */
  public function selectTransitionByStatesAndPlan($from_state, $to_state, $from_plan = null, $to_plan = null)
  {
    \logit("$from_state $to_state $from_plan $to_plan");
    $this->currentTransition = NULL;

    // sanity check
    if ( empty( $this->forwardStar[ $from_state ] ) )
    {
      $this->add_errors_and_code( 'ERROR: there are no transitions associated with state ' . $from_state , 'IN0001' );
      return FALSE;
    }

    // loop through transitions departing from $state
    foreach( $this->forwardStar[ $from_state ] as $transition )
    {
      if ($transition->to_plan_state !== $to_state)
        continue;

      if ( count( $transition->transition_labels ) )
      {
        foreach( $transition->transition_labels as $info )
        {
          if ( 
            ($from_plan && $to_plan && $info->from_plan == $from_plan && $info->to_plan == $to_plan)
            || ($from_plan && !$to_plan   && $info->from_plan == $from_plan)
            || ($to_plan   && !$from_plan && $info->to_plan   == $to_plan  )
          )
          {
            // make sure there are no duplicate labels!
            if ( ! empty($this->currentTransition) )
              $this->add_errors_and_code( 'ERROR: there was more than one transition found' , 'IN0001' );
            else
            {
              // build transition named $label
              $this->currentTransition = ( array ) $transition;

              unset( $this->currentTransition['transition_labels'] );

              $this->currentTransition = array_merge ( $this->currentTransition , ( array ) $info );

              if ( empty( $this->currentTransition['requirements'] ) )
                $this->currentTransition['requirements'] = array();
            }

            // put together requirements
            if ( ! empty( $transition->requirements ) )
              $this->currentTransition['requirements'] = array_merge( $this->currentTransition['requirements'] , $transition->requirements );
          }
        }
      }
      else
        dlog('','WARNING: there are no labels');
    }

    if ( empty( $this->currentTransition ) )
      $this->add_errors_and_code( 'ERROR: no ' . $from_state .' transitions found for customer' , 'IN0001' );

    if ( $this->is_success() )
      $this->forwardStar = array();

    dlog('',"currentTransition = %s",$this->currentTransition);

    return $this->is_success();
  }

  /**
   * selectTransitionByLabel
   *
   * Select the transition to be run ( identified by $label ) and cleans up $this->forwardStar if successful.
   * Method getForwardStar must be invoked before this method.
   * The output of this method can be accessed by getCurrentTransition.
   *
   * @return boolean
   */
  public function selectTransitionByLabel( $label , $state, $from_plan = null )
  {
    $this->currentTransition = NULL;

    // sanity check
    if ( empty( $this->forwardStar[ $state ] ) )
    {
      $this->add_errors_and_code( 'ERROR: there are no transitions associated with state ' . $state , 'IN0001' );
      return FALSE;
    }

    // loop through transitions departing from $state
    foreach( $this->forwardStar[ $state ] as $transition )
    {
      if ( count( $transition->transition_labels ) )
      {
        foreach( $transition->transition_labels as $info )
        {
          if (
            $info->transition_label == $label
            && (($from_plan == null) || ($info->from_plan == $from_plan))
          )
          {
            // make sure there are no duplicate labels!
            if ( ! empty($this->currentTransition) )
              $this->add_errors_and_code( 'ERROR: there are more than one transition named ' . $label , 'IN0001' );
            else
            {
              // build transition named $label
              $this->currentTransition = ( array ) $transition;

              unset( $this->currentTransition['transition_labels'] );

              $this->currentTransition = array_merge ( $this->currentTransition , ( array ) $info );

              if ( empty( $this->currentTransition['requirements'] ) )
                $this->currentTransition['requirements'] = array();
            }

            // put together requirements
            if ( ! empty( $transition->requirements ) )
              $this->currentTransition['requirements'] = array_merge( $this->currentTransition['requirements'] , $transition->requirements );
          }
        }
      }
      else
        dlog('','WARNING: there are no labels');
    }

    if ( empty( $this->currentTransition ) )
      $this->add_errors_and_code( 'ERROR: no ' . $state .' transitions found named ' . $label , 'IN0001' );

    if ( $this->is_success() )
      $this->forwardStar = array();

    dlog('',"currentTransition = %s",$this->currentTransition);

    return $this->is_success();
  }

  /**
   * getCurrentTransition
   *
   * Get the configuration of the transition which has been selected with method selectTransitionByLabel.
   * Method selectTransitionByLabel must be invoked before this method.
   *
   * @return array
   */
  public function getCurrentTransition()
  {
    return $this->currentTransition;
  }

  /**
   * getCurrentTransitionData
   *
   * Get the transition data which has been generated with method generateTransitionData
   * Method generateTransitionData must be invoked before this method.
   *
   * @return array
   */
  public function getCurrentTransitionData()
  {
    return $this->dataEngine->currentTransitionData;
  }

  /**
   * setCustomerData
   *
   * Required fields:
   *  - customer_id           ( required )
   *  - cos_id                ( required )
   *  - plan_state            ( required )
   * Most important fields which may be or may not be available (it's important that those values should be retrieved only once and kept in synch within the Data Engine):
   *  - activation_iccid_full
   *  - current_iccid_full
   *  - current_mobile_number
   *  - stored_value
   *  - balance
   *  - packaged_balance1
   *  - packaged_balance2
   *  - mvne
   *  - customer_source
   *  - preferred_language
   *  - plan_started
   *  - plan_expires (date)
   *  - plan_expired (boolean)
   *  - monthly_cc_renewal
   *  - tos_accepted
   *  - monthly_renewal_target
   *  - monthly_renewal_target_requested
   *  - cancel_requested
   *  - easypay_activated
   *  - account
   *  - login_name
   *  - address1
   *  - address2
   *  - city
   *  - state_or_region
   *  - postal_code
   *  - country
   *  - first_name
   *  - last_name
   * Note: this replaces 'context' and 'customer' in previous implementation
   *
   * @return boolean
   */
  public function setCustomerData( $params )
  {
    // validate basic parameters
    if ( empty( $params['customer_id'] ) )
      $this->add_errors_and_code( 'ERROR: customer_id is missing (b)' , 'MP0020' );

    elseif ( !array_key_exists( 'cos_id', $params ) )
      $this->add_errors_and_code( 'ERROR: cos_id is missing (b)' , 'IN0001' );

    elseif ( empty( $params['plan_state'] ) )
      $this->add_errors_and_code( 'ERROR: plan_state is missing (b)' , 'MP0022' );

    // set configuration file by brand
    $brandID = !empty( $params['BRAND_ID'] ) ? $params['BRAND_ID'] : 1;
    $this->brandId    = $brandID;
    $this->configFile = $this->strategy->getBrandConfig( $brandID );

    if ( $this->is_success() )
    {
      $this->customerData = array();
      foreach ($params as $key => $val)
        $this->customerData[strtolower($key)] = $val;

      $this->getForwardStar( $params['plan_state'] );
    }

    return $this->is_success();
  }

  /**
   * loadTransitionData
   *
   * Load data previously generated with generateTransitionData and saved using $this->dataEngine
   * If $transition_uuid is not provided, load the next transition to be resolved.
   *
   * @return boolean
   */
  public function loadTransitionData( $transition_uuid=NULL )
  {
    $outcome = empty( $transition_uuid )
      ? $this->dataEngine->loadNextTransitionAndActions()
      : $this->dataEngine->loadTransitionAndActionsByUUID( $transition_uuid );

    $this->merge( $outcome );

    return $this->is_success();
  }

  /**
   * generateTransitionData
   *
   * Generate data to execute the given transition for the customer.
   * Methods setCustomerData and selectTransitionByLabel must be invoked before this method.
   * The output of this method can be accessed by getCurrentTransitionData.
   * Manipulates $this->dataEngine->currentTransitionData
   *
   * @return boolean
   */
  public function generateTransitionData($context = NULL)
  {
    // sanity check
    if ( empty( $this->currentTransition ) )
    {
      $this->add_errors_and_code( 'ERROR: no transition data available' , 'IN0001' );

      return FALSE;
    }

    // prerequisites are checked at creation time
    $this->checkPrerequisites();
    if ( ! $this->is_success())
      return FALSE;

    $this->dataEngine->currentTransitionData = array(
      'transition_uuid'  => \getNewTransitionUUID('transition ' . time()),
      'customer_id'      => $this->customerData['customer_id'],
      'status'           => STATUS_OPEN,
      'from_cos_id'      => $this->strategy->getCOSIDByPlanName( $this->currentTransition['from_plan'] ), //\get_cos_id_from_plan( $this->currentTransition['from_plan'] ),
      'to_cos_id'        => $this->strategy->getCOSIDByPlanName( $this->currentTransition['to_plan'] ), //\get_cos_id_from_plan( $this->currentTransition['to_plan']   ),
      'from_plan_state'  => $this->currentTransition['from_plan_state'],
      'to_plan_state'    => $this->currentTransition['to_plan_state'],
      'actions'          => empty( $this->currentTransition['actions'] ) ? array() : $this->currentTransition['actions'] ,
      'transition_label' => $this->currentTransition['transition_label'],
      'priority'         => $this->currentTransition['priority'],
      'htt_environment'  => \Ultra\UltraConfig\getEnvironment(),
      'created'          => time(),
      'closed'           => NULL,
      'context'          => $context
    );

    // add departing and arrival actions

    $this->appendArrivingAndDepartingActions();

    $this->dataEngine->storeTransitionAndActions($context);

    $this->dataEngine->resetCurrentActionData();

    /* moved this before storing in DB
    // prerequisites are checked at creation time
    $this->checkPrerequisites();
    */

    return $this->is_success();
  }

  /**
   * Generate action data without storing to database
   * TODO refactor, functionality already exists
   */
  public function generateActionData($abortedAction = null)
  {
    $this->dataEngine->currentTransitionData['actions'] = empty( $this->currentTransition['actions'] )
      ? array()
      : $this->currentTransition['actions'];

    // find aborted action
    $startFrom = 0;
    if ($abortedAction)
      $startFrom = array_search($abortedAction, $this->dataEngine->currentTransitionData['actions']);

    $this->dataEngine->currentTransitionData['actions'] = array_slice($this->dataEngine->currentTransitionData['actions'], $startFrom);

    $this->appendArrivingAndDepartingActions();

    $this->dataEngine->applyActionDetails();

    $this->dataEngine->resetCurrentActionData();

    return $this->is_success();
  }

  public function appendArrivingAndDepartingActions()
  {
    $this->dataEngine->currentTransitionData['actions'] = $this->addDepartingActions(
      $this->dataEngine->currentTransitionData['actions'],
      $this->currentTransition['from_plan_state']
    );

    $this->dataEngine->currentTransitionData['actions'] = $this->addArrivalActions(
      $this->dataEngine->currentTransitionData['actions'],
      $this->currentTransition['to_plan_state']
    );
  }

  /**
   * addDepartingActions
   *
   * Prepend departing actions from state $plan_state at the beginning of $actions
   *
   * @param array $actions
   * @param string $plan_state
   * @return NULL
   */
  protected function addDepartingActions( array $actions , $plan_state )
  {
    if ( ! empty( $this->configuration->states->$plan_state->depart ) )
      $actions = array_merge( $this->configuration->states->$plan_state->depart , $actions );

    return $actions;
  }

  /**
   * addArrivalActions
   *
   * Append arrival actions from state $plan_state at the end of $actions
   *
   * @param array $actions
   * @param string $plan_state
   * @return NULL
   */
  protected function addArrivalActions( array $actions , $plan_state )
  {
    if ( ! empty( $this->configuration->states->$plan_state->arrival ) )
      $actions = array_merge( $actions , $this->configuration->states->$plan_state->arrival );

    return $actions;
  }
}

/*
TODO:
 - continue transaction execution after being paused
 - config file content sanity checks
 - recover transition with aborted action
*/

