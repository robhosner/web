<?php

require_once 'classes/PHPUnitBase.php';
require_once 'PartnerBase.php';

use \Ultra\Lib\Util\Redis;
use \Ultra\Lib\Api\PartnerBase;


class PartnerBaseTest extends PHPUnitBase
{
  private $intUsername;
  private $stringUsername;
  private $partnerBase;
  private $redis;
  private $testMsisdnKey;
  private $testUsernameKey;

  private $apiName;

  public function __construct()
  {
    $this->intUsername = '9174023697';
    $this->stringUsername = 'mm6dev';
  }

  public function setUp()
  {
    parent::setUp();

    $this->apiName = 'test_api_block_user';
    $this->redis = new Redis();

    $this->testMsisdnKey = "command/count/$this->apiName/$this->intUsername";
    $this->redis->set($this->testMsisdnKey, 7, 3600);

    $this->testUsernameKey = "command/count/$this->apiName/$this->stringUsername";
    $this->redis->set($this->testUsernameKey, 7, 3600);

    $this->partnerBase = new PartnerBase(array(), 1);
    teldata_change_db();
  }

  public function tearDown()
  {
    $this->redis->del($this->testMsisdnKey);
    $this->redis->del($this->testUsernameKey);
  }

  public function testRemoveBlockedUserByMsisdn()
  {
    $result = $this->partnerBase->removeBlockedUserByMsisdn($this->intUsername, $this->apiName, '', 123456789);
    $this->assertTrue($result);

    $result = $this->partnerBase->removeBlockedUserByMsisdn('THIS IS NOT A USERNAME', $this->apiName);
    $this->assertFalse($result);
  }

  public function testRemoveBlockedUserByUsername()
  {
    $result = $this->partnerBase->removeBlockedUserByUsername($this->stringUsername, $this->apiName, 'Cameron', 123456789);
    $this->assertTrue($result);

    $result = $this->partnerBase->removeBlockedUserByUsername('THIS IS NOT A USERNAME', $this->apiName);
    $this->assertFalse($result);
  }

  public function testRemoveBlockedUser()
  {
    $userName = 'fakename';
    $this->redis->set("command/count/$this->apiName/$userName", 7, 3600);

    $result = $this->partnerBase->removeBlockedUser($userName, $this->apiName, 'Cameron');
    $this->assertTrue($result);

    $result = $this->partnerBase->removeBlockedUser('THIS IS NOT A USERNAME', $this->apiName);
    $this->assertFalse($result);

    $userName = '7777777777';
    $this->redis->set("command/count/$this->apiName/$userName", 7, 3600);

    $result = $this->partnerBase->removeBlockedUser($userName, $this->apiName);
    $this->assertTrue($result);

    $userName = '127.0.0.1';
    $this->redis->set("command/count/$this->apiName/$userName", 7, 3600);

    $result = $this->partnerBase->removeBlockedUser($userName, $this->apiName);
    $this->assertTrue($result);
  }
}