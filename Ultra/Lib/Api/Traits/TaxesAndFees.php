<?php

namespace Ultra\Lib\Api\Traits;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Taxes\TaxesAndFeesCalculator;

trait TaxesAndFees
{
  protected abstract function errException($error, $error_code, $user_error = false);
  protected abstract function addToOutput($field, $value);

  public function calculateTaxesAndFees(
    CustomerRepository $customerRepository,
    TaxesAndFeesCalculator $taxesAndFeesCalculator,
    Configuration $configuration,
    $customerId,
    $zip,
    $chargeAmount,
    $productType
  ) {
    $customer = $customerRepository->getCombinedCustomerByCustomerId(
      $customerId,
      ['u.customer_id', 'u.BRAND_ID', 'MONTHLY_RENEWAL_TARGET', 'a.COS_ID', 'CC_POSTAL_CODE', 'POSTAL_CODE']
    );

    if (!$customer) {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
    }

    // get zip code
    if (!$zip || !is_numeric($zip) || (strlen($zip) != 5)) {
      $zip = $customer->cc_postal_code;
    }

    if (!$zip || !is_numeric($zip) || (strlen($zip) != 5)) {
      $zip = $customer->postal_code;
    }

    if (!$zip || !is_numeric($zip) || (strlen($zip) != 5)) {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: No zip code found for this customer', 'MP0007');
    }

    $activationZipCode = !empty($customer->postal_code) ?: null;

    if ($chargeAmount >= 5700 && !empty($customer->monthly_renewal_target)) {
      $cosId = $configuration->getCosIdFromPlan($customer->monthly_renewal_target);

      if ($configuration->isBPlan($cosId)) {
        $customer->cos_id = $cosId;
      }
    }

    $result = $taxesAndFeesCalculator->calculateTaxesAndFees($customer->brand_id, $customer->cos_id, $productType, $chargeAmount, $zip, $activationZipCode);

    if ($result->is_failure()) {
      $errors = $result->get_errors();
      return $this->errException('ERR_API_INVALID_ARGUMENTS: ' . $errors[0], 'MP0007');
    }

    return $result;
  }
}
