<?php
namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Sims\Inventory;

class SendFreeReplacementSim extends CustomerCare
{
  /**
   * @var Inventory
   */
  private $inventory;

  public function __construct(Inventory $inventory)
  {
    $this->inventory = $inventory;
  }

  public function customercare__SendFreeReplacementSim()
  {
    list ($customer_id) = $this->getInputValues();

    teldata_change_db();

    try {
      $result = $this->inventory->shipSim($customer_id, __FUNCTION__);

      if (!$result['success']) {
        return $this->errException("ERR_API_INTERNAL: {$result['error']}", $result['code']);
      }

      $this->succeed();
    } catch(\Exception $e) {
      dlog('' , 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}
