<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class PromoEnroll extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__CheckBalances
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__PromoEnroll()
  {
    list ($customer_id, $plan, $reference_id, $months) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, ['plan_state']);
      if ( ! $customer)
        $this->errException('ERR_API_INTERNAL: customer does not exist', 'VV0031');

      $result = enroll_billing_mrc_promo_plan($customer_id, $plan, $reference_id, $months);

      if ( ! $result->is_success())
        $this->errException('ERR_API_INTERNAL: failure inserting into customer options', 'DB0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
