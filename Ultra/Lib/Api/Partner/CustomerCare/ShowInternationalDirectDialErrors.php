<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class ShowInternationalDirectDialErrors extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__ShowInternationalDirectDialErrors
   *
   * Gives the customer increased $ that can be used for anything; but will not be commissioned.
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__ShowInternationalDirectDialErrors()
  {
    list ($customer_id) = $this->getInputValues();

    $history = array();

    try
    {
      teldata_change_db();

      $customer = get_account_from_customer_id( $customer_id , array('account') );

      if ( $customer )
      {
        $call_session_log_data = call_session_log_data($customer->account);

        if ( $call_session_log_data && is_array($call_session_log_data) && count($call_session_log_data) )
        {
          foreach($call_session_log_data as $id => $data)
          {
            $history[] = implode(";", array( $data->DATE_TIME , $data->CALL_SESSION_CODE , $data->ERROR , $data->DATE_TIME_PST , $data->DIALED_NUMBER ) );
          }
        }
      }
      else
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      $this->addToOutput('history', $history);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
