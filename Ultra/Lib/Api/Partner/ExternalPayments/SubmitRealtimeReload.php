<?php

namespace Ultra\Lib\Api\Partner\ExternalPayments;

require_once 'classes/Flex.php';

use Ultra\Lib\Api\Partner\ExternalPayments;
use Ultra\Payments\Payment;

require_once 'Ultra/Lib/Api/Partner/ExternalPayments.php';

class SubmitRealtimeReload extends \Ultra\Lib\Api\Partner\ExternalPayments
{
  /**
   * @var Payment
   */
  public $payment;

  /**
   * externalpayments__SubmitRealtimeReload
   * allows external payment processors to apply credit to a subscriber's account
   * @return object Result
   */
  public function externalpayments__SubmitRealtimeReload()
  {
    // initialize
    list(
      $request_epoch,
      $phone_number,
      $dealer_code,
      $store_zipcode,
      $store_zipcode_extra4,
      $clerk_id,
      $terminal_id,
      $product_id,
      $subproduct_id,
      $upc,
      $load_amount,
      $provider_trans_id,
      $provider_name
    ) = $this->getInputValues();

    foreach (array(
      'phone_number',
      'customer_balance',
      'ultra_payment_trans_id',
      'load_amount',
      'provider_trans_id'
    ) as $value)
    {
      $this->addToOutput($value, NULL);
    }

    $fraud = NULL;

    try
    {
      $this->init(new Payment());

      // parameter checking and initialization
      $this->checkTimeSync($request_epoch);

      teldata_change_db();
      
      $dealer     = $this->verifyDealer($dealer_code, FALSE); // API-236: allow inactive dealer to recharge
      $subscriber = $this->verifySubscriber($phone_number);

      if (\Ultra\Lib\Flex::isFlexPlan($subscriber->cos_id))
        $this->errException('This command does not support Ultra Flex subscribers. Please use the Ultra Flex products in your terminal.', 'VV0006');

      $this->validateProvider($provider_name);
      $this->validateCommisionCanBeFactored($subscriber->plan_state, $subscriber->current_iccid);

      // build fraud data
      $fraud = array(
        'provider'      => $provider_name,
        'store_id'      => $dealer->DealerSiteID,
        'clerk_id'      => $clerk_id,
        'terminal_id'   => $terminal_id,
        'load_amount'   => $load_amount,
        'subproduct_id' => $subproduct_id
      );

      $this->verifyUPC($provider_name, $upc, $load_amount, $subscriber);

      // check product and subproduct and convert if activating
      $subproduct_id = $this->checkProduct($subscriber, $product_id, $subproduct_id, $load_amount);

      // insert a new row into HTT_BILLING_ACTIONS for processing by the billing runner
      $ultra_payment_trans_id = create_guid(__FUNCTION__);

      if ( ! \Ultra\Lib\Billing\addBillingActionData(
        $subscriber,
        $ultra_payment_trans_id,
        $provider_trans_id,
        $product_id,
        $subproduct_id,
        $load_amount,
        $upc,
        $provider_name
      ))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001');

      $this->storeTrackingParamsInRedis(
        $provider_trans_id,
        $dealer->DealerSiteID,
        $store_zipcode,
        $store_zipcode_extra4,
        $clerk_id,
        $terminal_id,
        $provider_name,
        __FUNCTION__
      );

      // update ULTRA.HTT_ACTIVATION_HISTORY if necessary
      if ( ! in_array($subscriber->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED, STATE_CANCELLED)))
        $this->logFundingInActivationHistory($subscriber->CUSTOMER_ID, $provider_name, $load_amount);

      // fill in return values
      $this->addToOutput('phone_number', $phone_number);
      $this->addToOutput('customer_balance', ($subscriber->stored_value + $subscriber->BALANCE) * 100 + $load_amount);
      $this->addToOutput('load_amount', $load_amount);
      $this->addToOutput('ultra_payment_trans_id', $ultra_payment_trans_id);
      $this->addToOutput('provider_trans_id', $provider_trans_id);

      fraud_event($subscriber, 'externalpayments', 'SubmitRealtimeReload', 'success', $fraud);
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
      if ($fraud)
        fraud_event($subscriber, 'externalpayments', 'SubmitRealtimeReload', 'error', $fraud);
    }

    return $this->result;
  }

  /**
   * validates provided provider_name
   * @param  string $provider_name
   * @return null
   */
  public function validateProvider($provider_name)
  {
    // PROD-1855: validate that commission can be factored
    // VYT @ 2015-08-12: this API is used by T-Cetra only since ePay is using v1 equivalent; update this condition when adding new payment providers
    if ( ! in_array($provider_name, array('TCETRA')))
    {
      $this->errException(
        'ERR_API_INVALID_ARGUMENTS: Unknown validation in API configuration',
        'VV0019', 
        "Cannot handle payment provider $provider_name"
      );
    }
  }

  /**
   * @param string $plan_state
   * @param string $current_iccid
   */
  public function validateCommisionCanBeFactored($plan_state, $current_iccid)
  {
    // reject activation payment applied to SIMs with alternative masters
    if ( ! in_array($plan_state, array(STATE_ACTIVE, STATE_SUSPENDED, STATE_PRE_FUNDED)))
    {
      if ( ! $sim = $this->simRepo->getSimFromIccid($current_iccid))
        $this->errException('ERR_API_INVALID_ARGUMENTS: cannot find sim for subscriber', 'IC0001');

      if ( $error = $this->payment->validateFactoredCommission($sim->INVENTORY_MASTERAGENT))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Customer SIM is not eligible for instant spiff', 'EL0004', $error);
    }  
  }

  /**
   * verify UPC, amount, and mapped destination
   * @param  string $provider_name
   * @param  string $upc
   * @param  int    $load_amount in cents
   * @param  object $subscriber
   * @return null
   */
  public function verifyUPC($provider_name, $upc, $load_amount, $subscriber)
  {
    // verify UPC
    if ( ! $this->payment->verifyProviderSku($provider_name, $upc, $subscriber))
      $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU', 'VV0006');

    // verify UPC amount
    if ( ! $destination = $this->payment->verifyProviderSkuAmount($provider_name, $upc, $load_amount))
      $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid amount', 'VV0115');

    // verify mapped destination
    if ( ! $destination = $this->payment->verifyAmountDestination($subscriber, $destination, $load_amount))
      $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid amount', 'VV0115');

    dlog('', 'mapped $%.2f into %s for customer %d', $load_amount / 100, $destination, $subscriber->customer_id);
  }

  /**
   * stores tracking parameters in redis
   * @param  string $provider_trans_id
   * @param  int    $store_id DealerSiteId
   * @param  string $store_zipcode
   * @param  string $store_zipcode_extra4
   * @param  int    $clerk_id
   * @param  int    $terminal_id
   * @param  string $source
   * @param  string $detail
   * @param  Redis  $redis default to null
   * @return null
   */
  public function storeTrackingParamsInRedis(
    $provider_trans_id,
    $store_id,
    $store_zipcode,
    $store_zipcode_extra4,
    $clerk_id,
    $terminal_id,
    $source,
    $detail,
    $redis = null)
  {
    if ( ! $redis && ! $redis = $this->getRedis())
      $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002');

    $key = "externalpayments/$provider_trans_id";
    $ttl = 60 * 30;

    $redis->set("$key/store_id",             $store_id,             $ttl);
    $redis->set("$key/store_zipcode",        $store_zipcode,        $ttl);
    $redis->set("$key/store_zipcode_extra4", $store_zipcode_extra4, $ttl);
    $redis->set("$key/clerk_id",             $clerk_id,             $ttl);
    $redis->set("$key/terminal_id",          $terminal_id,          $ttl);
    $redis->set("$key/source",               $source,               $ttl);
    $redis->set("$key/detail",               $detail,               $ttl);
  }

  /**
   * @param  int    $customer_id
   * @param  string $provider_name
   * @param  int    $load_amount in cents
   * @return null
   */
  public function logFundingInActivationHistory($customer_id, $provider_name, $load_amount)
  {
    $activation_history = $this->activationHistoryRepo->getHistory($customer_id);

    if (
      $activation_history
      && ! in_array($activation_history->FINAL_STATE, array(STATE_CANCELLED, FINAL_STATE_COMPLETE))
      && ! $activation_history->FUNDING_SOURCE
    )
    {
      $this->activationHistoryRepo->logFunding($customer_id, $provider_name, $load_amount / 100);
    }
  }

  public function init(Payment $payment)
  {
    $this->payment = $payment;
  }
}

