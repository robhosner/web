<?php

require_once 'classes/PHPUnitBase.php';


/**
 * tests for 'externalpayments' partner APIs v2
 */
class ExternalPaymentsTest extends PHPUnitBase
{

  /**
   * test__externalpayments__CheckMobileBalance
   */
  public function test__externalpayments__CheckMobileBalance()
  {
    // API setup
    list($test, $partner, $api) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'version'   => 2,
      'partner'   => $partner,
      'debug'     => TRUE));

    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);

    // test invalid epoch
    $params = array(
      'request_epoch'   => time() - 181,
      'phone_number'    => '5555555555',
      'store_zipcode'   => '99999',
      'dealer_code'     => 'ABC',
      'clerk_id'        => 100,
      'terminal_id'     => 32);
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid UNIX Epoch value", $result->errors);
    $this->assertTrue( ! $result->success);

    // test invalid dealer code
    $params = array(
      'request_epoch'   => time(),
      'phone_number'    => '9292589471',
      'store_zipcode'   => '99999',
      'dealer_code'     => 'ABC123',
      'clerk_id'        => 100,
      'terminal_id'     => 32);
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: No celluphone dealer info found", $result->errors);
    $this->assertTrue( ! $result->success);

    // test disabled dealer code
    $params = array(
      'request_epoch'   => time(),
      'phone_number'    => '9292589471',
      'store_zipcode'   => '99999',
      'dealer_code'     => 'C20099',
      'clerk_id'        => 100,
      'terminal_id'     => 32);
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Dealer is not active", $result->errors);
    $this->assertTrue( ! $result->success);

    // test invalid MSISDN
    $params = array(
      'request_epoch'   => time(),
      'phone_number'    => '5555555555',
      'store_zipcode'   => '99999',
      'dealer_code'     => 'abmn64',
      'clerk_id'        => 100,
      'terminal_id'     => 32);
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: The customer does not exist", $result->errors);
    $this->assertTrue( ! $result->success);

    // test cancelled subscriber
    $params = array(
      'request_epoch'   => time(),
      'phone_number'    => '3474356492',
      'store_zipcode'   => '99999',
      'dealer_code'     => 'abmn64',
      'clerk_id'        => 100,
      'terminal_id'     => 32);
    $result = $this->callApi($params);
    print_r($result);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command", $result->errors);
    $this->assertTrue( ! $result->success);

    // successfull test
    $params = array(
      'request_epoch'   => time(),
      'phone_number'    => '9292589471',
      'store_zipcode'   => '99999',
      'dealer_code'     => 'abmn64',
      'clerk_id'        => 100,
      'terminal_id'     => 32,
      'partner_tag'     => 'epay');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);

  }


}

