<?php

namespace Ultra\Lib\Api\Partner\ProjectW;

require_once 'Ultra/Lib/Api/Partner/ProjectW.php';

class ImportFile extends \Ultra\Lib\Api\Partner\ProjectW
{
  /**
   * projectw__ImportFile
   *
   * Validates and imports a file into the UNIVISION_IMPORT table
   *
   * @param 
   * @return Result object
   */
  public function projectw__ImportFile()
  {
    list( $filename ) = $this->getInputValues();

    try
    {
      $fullPath = self::PROJECTW_IMPORT_FILE_PATH . $filename;
      \logInfo( 'Full path to file: ' . $fullPath );
      if ( ! file_exists( $fullPath ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: filename $filename does not exist", 'PW0001' );

      teldata_change_db();

      // determine IMPORT_FILE_DATE_TIME
      if ( time() < strtotime( 'today 8am' ) )
      {
        $importDate = date('Y-m-d H:i:s',  strtotime( 'yesterday 08:00:00' ) );
      }
      else
      {
        $importDate = date('Y-m-d H:i:s',  strtotime( 'today 08:00:00' ) );
      }

      // import file
      $parser = new \ProjectW\Parser();
      $outcome = $parser->import( self::PROJECTW_IMPORT_FILE_PATH, $filename, null, $importDate );
      if ( !$outcome->is_success() )
      {
        list( $errors, $errorCodes, $userErrors ) = $outcome->get_errors_and_code();
        for ( $i = 0; $i < count( $errors ); $i++ )
          $this->addError( $errors[0], $errorCodes[0], $userErrors[0] );
      }
      else
        $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
