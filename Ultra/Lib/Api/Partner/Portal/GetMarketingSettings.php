<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class GetMarketingSettings extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__GetMarketingSettings
   *
   * Retrieves Marketing Settings for a logged in customer.
   *
   * @param string zsession
   * @return Result object
   */
  public function portal__GetMarketingSettings()
  {
    list ($zsession) = $this->getInputValues();

    $marketing_settings = array();

    try
    {
      teldata_change_db();

      $data_zsession = get_customer_from_zsession($zsession);

      if ( count($data_zsession['errors']) == 0 )
      {
        $customer = $data_zsession['customer'];

        $this->setOutputLanguage($customer->preferred_language);

        $result = get_marketing_settings( array( 'customer_id' => $customer->CUSTOMER_ID ) );

        $settings = array(
          'marketing_email_option',
          'marketing_sms_option'
        );

        foreach( $settings as $id => $field )
        {
          $marketing_settings[] = $field;
          $marketing_settings[] = $result['marketing_settings'][$field];
        }

        $voice_option = get_voice_preference($customer->CUSTOMER_ID);
        $marketing_settings[] = 'marketing_voice_option';
        $marketing_settings[] = ($voice_option) ? 0 : 1;
      }
      else
      {
        $this->errException($data_zsession['errors'][0], 'SE0002');
      }

      $this->addToOutput('marketing_settings', $marketing_settings);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>