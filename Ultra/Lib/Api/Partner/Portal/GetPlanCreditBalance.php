<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use LineCredits;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class GetPlanCreditBalance extends \Ultra\Lib\Api\Partner\Portal
{
  private $session;
  private $lineCredits;

  public function __construct(Session $session, LineCredits $lineCredits)
  {
    $this->session = $session;
    $this->lineCredits = $lineCredits;
  }

  public function portal__GetPlanCreditBalance()
  {
    try
    {
      if (!$this->session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');

      $this->addToOutput('credits', $this->lineCredits->getBalance($this->session->customer_id));

      $this->succeed();
    }
    catch (\Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
