<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class GetCustomerPreferences extends \Ultra\Lib\Api\Partner\Portal
{

  /**
   * portal__GetCustomerPreferences
   * return customer preferences which subscriber may change frequently
   * @see MOBIII-54
   * @return Result object
   */
  public function portal__GetCustomerPreferences()
  {
    $this->addToOutput('preferred_language', null);
    $this->addToOutput('auto_recharge',      null);
    $this->addToOutput('marketing_sms',      null);
    $this->addToOutput('marketing_email',    null);
    $this->addToOutput('3rd_party_voice',    null);
    $this->addToOutput('3rd_party_sms',      null);
    $this->addToOutput('3rd_party_email',    null);
    $this->addToOutput('marketing_voice',    null);

    try
    {
      // verify session
      $session = new \Session();
      if ( ! $customer_id = $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');

      // get subscriber with minimum needed fields
      teldata_change_db();
      if ( ! $customer = get_ultra_customer_from_customer_id($customer_id, array('preferred_language', 'monthly_cc_renewal')))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
      $this->setOutputLanguage($customer->preferred_language);

      // collect output
      $this->addToOutput('preferred_language', $customer->preferred_language);
      $this->addToOutput('auto_recharge', $customer->monthly_cc_renewal);

      // get marketing preferences
      $result = get_marketing_settings(array('customer_id' => $customer_id));
      if (count($result['errors']))
        $this->addWarning($result['errors'][0]);
      else
      {
        $this->addToOutput('marketing_sms', $result['marketing_settings']['marketing_sms_option']);
        $this->addToOutput('marketing_email', $result['marketing_settings']['marketing_email_option']);
      }

      $options = get_ultra_customer_options_by_customer_id_assoc($customer_id);

      foreach ($options as $attr => $val)
      {
        if (strpos($attr, 'OPT_OUT.3RDPARTY_VOICE'))
          $this->addToOutput('3rd_party_voice', !$val);

        if (strpos($attr, 'OPT_OUT.3RDPARTY_SMS'))
          $this->addToOutput('3rd_party_sms', !$val);

        if (strpos($attr, 'OPT_OUT.3RDPARTY_EMAIL'))
          $this->addToOutput('3rd_party_email', !$val);

        if ($attr == 'PREFERENCES.OPT_OUT.VOICE')
          $this->addToOutput('marketing_voice', !$val);
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>