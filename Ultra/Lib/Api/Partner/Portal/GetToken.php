<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Lib\Api\Partner\Portal;
use Ultra\Utilities\SessionUtilities;

class GetToken extends Portal
{
  /**
   * @var SessionUtilities
   */
  private $sessionUtilities;

  /**
   * KillSession constructor.
   * @param SessionUtilities $sessionUtilities
   */
  public function __construct(SessionUtilities $sessionUtilities)
  {
    $this->sessionUtilities = $sessionUtilities;
  }

  /**
   * portal__GetToken
   * create new session based on HTTP TMO enrichment headers (does not support SSL) and return pre-authorization token to be used for portal__Login
   * @return object Result with string 'token'
   * @see MOB-39
   */
  public function portal__GetToken()
  {
    // initialize
    $token = null;

    try {
      // get session pre-authorization token
      $token = $this->sessionUtilities->getSession()->token;

      if (!$token) {
        return $this->errException('ERR_API_INTERNAL: could not generate a security token.', 'SE0001', 'System malfunction, please try again later.');
      }

      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('token', $token);
    return $this->result;
  }
}

