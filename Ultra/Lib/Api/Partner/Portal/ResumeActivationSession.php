<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class ResumeActivationSession extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__ResumeActivationSession
   * create a zsession for a user returning mid-way to a SIM activation
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Self-Activation+APIs#Self-ActivationAPIs-portal__ResumeActivationSession
   * @return object Result
   */
  public function portal__ResumeActivationSession()
  {
    // init
    list($iccid, $actcode) = $this->getInputValues();

    $this->addToOutput('MSISDN',   '');
    $this->addToOutput('zsession', '');

    try
    {
      teldata_change_db();

      // either $iccid or $actcode required
      if (empty($iccid) && empty($actcode))
        $this->errException('ERR_API_INVALID_ARGUMENTS: either ICCID or actcode is required', 'IN0002');

      // get customer
      if ($iccid)
        $customer = get_customer_from_iccid($iccid);
      else
        $customer = get_customer_from_actcode($actcode);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      $this->setOutputLanguage($customer->preferred_language);

      $this->addToOutput('MSISDN',   $customer->current_mobile_number);

      // check customer state
      $state = get_customer_state($customer);

      if (! in_array($state['state']['state'], array('Neutral', 'Provisioned', 'Port-In Requested', 'Port-In Denied')))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer state', 'IN0001');

      // create zsession cookie
      $_REQUEST['zsession'] = session_save($customer);
      $verified = verify_session($customer->CUSTOMER);

      if (! $verified)
        $this->errException('ERR_API_INTERNAL: failed to create user session', 'SE0001');

      setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER['SERVER_NAME']);

      $session = new \Session();

      if (!$session->confirm($customer->CUSTOMER_ID))
      {
        throw new \Exception('ERR_API_INTERNAL: A problem occurred when trying to create a session.');
      }

      $this->addToOutput('zsession', $verified[2]);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}

?>