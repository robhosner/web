<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Lib\Api\Traits\CCHandler;
use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCardValidator;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\SaveMethodFailedException;
use Ultra\Exceptions\UnhandledCCProcessorException;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\Portal;

class SaveCreditCard extends Portal
{
  use CCHandler;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var CreditCardRepository
   */
  private $cardRepository;

  /**
   * SaveCreditCard constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param CreditCardRepository $cardRepository
   */
  public function __construct(
    Session $session,
    CustomerRepository $customerRepository,
    Configuration $configuration,
    CreditCardRepository $cardRepository
  )
  {
    $this->session = $session;
    $this->customerRepository = $customerRepository;
    $this->cardRepository = $cardRepository;
    $this->configuration = $configuration;
  }

  public function portal__SaveCreditCard()
  {
    $params = $this->getNamedInputValues();

    try {
      // verify session
      if (!$customer_id = $this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }
      
      $params['customer_id'] = $customer_id;

      $this->validateAndSaveToken(
        $this->configuration,
        $this->customerRepository,
        $this->cardRepository,
        $params
      );

      $this->succeed();

    } catch (InvalidObjectCreationException $e) {
      if (is_object($this->customer) && !empty($e->getCustomUserErrors()[$this->customer->preferred_language])) {
        $this->addError($e->getMessage(), $e->code(), $e->getCustomUserErrors()[$this->customer->preferred_language]);
      } elseif (!empty($e->getCustomUserErrors()[0])) {
        $this->addError($e->getMessage(), $e->code(), $e->getCustomUserErrors()[0]);
      } else {
        $this->addError($e->getMessage(), $e->code());
      }
    } catch (SaveMethodFailedException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), 'DB0001');
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
