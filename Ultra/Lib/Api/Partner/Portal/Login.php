<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Customers\StateActions as CustomerState;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Utilities\SessionUtilities;
use Ultra\Utilities\Common as Utilities;

class Login extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var SessionUtilities
   */
  private $sessionUtilities;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var CustomerState
   */
  private $customerState;

  /**
   * @var Utilities
   */
  private $utilities;

  /**
   * Login constructor.
   * @param Session $session
   * @param SessionUtilities $sessionUtilities
   * @param Configuration $configuration
   * @param CustomerRepository $customerRepository
   * @param CustomerState $customerState
   * @param Utilities $utilities
   */
  public function __construct(
    Session $session,
    SessionUtilities $sessionUtilities,
    Configuration $configuration,
    CustomerRepository $customerRepository,
    CustomerState $customerState,
    Utilities $utilities
  )
  {
    $this->session = $session;
    $this->sessionUtilities = $sessionUtilities;
    $this->configuration = $configuration;
    $this->customerRepository = $customerRepository;
    $this->customerState = $customerState;
    $this->utilities = $utilities;
  }

  /**
   * portal__Login
   * login subscriber by either username/MSISDN and password or pre-authorization token and set cookie 'session'
   * @see MOB-32
   * @param string user: either username or MSISDN
   * @param string password: subscriber's password
   * @param string token: previosly created pre-authorization token
   * @param string error_language: Defaults to EN, must be EN, ES or ZH
   * @return object Result
   */
  public function portal__Login()
  {
    // initialize
    list ($username, $password, $token, $error_language) = $this->getInputValues();

    try {
      teldata_change_db();
      $customer = null;

      $this->setOutputLanguage($error_language);

      // API-477: do not subject to throttling by IP address if the call comes from CRM
      $ip = $this->sessionUtilities->getClientIp();
      $crmIps = $this->configuration->getCrmIpAddresses();
      if (empty($crmIps)) {
        return $this->errException('ERR_API_INTERNAL: Internal configuration error', 'IN0004');
      }

      if (!$this->sessionUtilities->isIpAddressInArray($ip, $crmIps)) {
        if ($this->sessionUtilities->checkIfUserIsBlockedFromApi(__FUNCTION__, empty($username) ? $ip : $username, self::MAX_FAILED_LOGINS)) {
          return $this->errException('ERR_API_INTERNAL: This command has been currently disabled. Please try again later.', 'AP0002');
        }
      }

      // authenticate by token
      if (!empty($token)) {
        // verify token
        if (!$this->session->verifyToken($token)) {
          if ($msisdn = $this->session->getTokenMsisdn($token)) {
            dlog('', 'token %s expired for MSISDN %s', $token, $msisdn);
            return $this->errException('ERR_API_INTERNAL: Login session expired.', 'SE0003');
          } else {
            return $this->errException('ERR_API_INTERNAL: No login session found for the given security_token.', 'SE0010');
          }
        }

        // get subscriber by MSISDN
        if (!$msisdn = $this->session->msisdn) {
          return $this->errException('ERR_API_INTERNAL: Invalid login information.', 'SE0010');
        }

        $customer = $this->getUltraCustomerFromMsisdn($msisdn);
      } elseif (!empty($username) && !empty($password)) { // authenticate by user/password
        $customer_password = null;

        if (is_numeric($username)) {
          $customer = $this->getUltraCustomerFromMsisdn($username);

          if ($customer) {
            $customer_details = $this->customerRepository->getCustomerFromCustomersTable($customer->CUSTOMER_ID, ['LOGIN_PASSWORD']);
            $customer_password = $customer_details->login_password;
          }
        } else {
          $customer = $this->customerRepository->getCustomerFromUsername($username);
          if ($customer) $customer_password = $customer->login_password;
        }

        if (!$customer || ($password !== $customer_password && !$this->utilities->authenticatePasswordHS($customer_password, $password))) {
          $this->sessionUtilities->checkApiAbuseByIdentifier(__FUNCTION__, $username, self::MAX_FAILED_LOGINS); // record failed attempt
          return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid login information.', 'SE0011');
        }
      } elseif ($this->session->customer_id) { // authenticate by previously created cookie session: unusual scenario but possible
        $customer = $this->customerRepository->getCustomerById($this->session->customer_id, ['BRAND_ID']);
      } else {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing.', 'MP0027');
      }

      // check login results
      if (!$customer) {
        $this->sessionUtilities->checkApiAbuseByIdentifier(__FUNCTION__, $username, self::MAX_FAILED_LOGINS); // record failed attempt
        return $this->errException('ERR_API_INTERNAL: The customer does not exist', 'VV0108');
      }

      $customer = (object) array_change_key_case((array) $customer, CASE_UPPER);

      // API-502: validate if customer brand is allowed in this environment
      if (!$this->configuration->isBrandAllowedByAPIPartner($customer->BRAND_ID, $this->partner)) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: The API is not available for this brand', 'FA0004');
      }

      // check subscriber status
      $state = $this->customerState->getStateFromCustomerId($customer->CUSTOMER_ID);
      if ($state) {
        if ($state['state'] == STATE_CANCELLED) {
          return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command.', 'IN0003');
        } elseif ($state['state'] == STATE_NEUTRAL) {
          return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command.', 'VV0052');
        } elseif ($state['state'] == STATE_PROVISIONED) {
          return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command.', 'VV0069');
        }
      } else {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: Customer state is inconsistent', 'VV0071');
      }

      // successful login: mark session as valid
      if (!$this->session->confirm($customer->CUSTOMER_ID)) {
        return $this->errException('ERR_API_INTERNAL: Generic internal error', 'MP0027');
      }

      $this->succeed();
    } catch(MissingRequiredParametersException $e) {
      $this->addError($e->getMessage(), $e->code());
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
