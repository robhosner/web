<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class GetTransactionHistory extends \Ultra\Lib\Api\Partner\Portal
{

  /**
   * portal__GetTransactionHistory
   * return subscriber's transaction history for a given time period
   * @see MOBIII-24
   * @return Result object
   */
  public function portal__GetTransactionHistory()
  {
    list ($start_epoch, $end_epoch, $recent) = $this->getInputValues();
    $this->addToOutput('transaction_history', array());
    $this->addToOutput('record_count', 0);
    $transaction_history = array();
    $record_count = 0;

    try
    {
      // verify session
      $session = new \Session();
      if ( ! $customer_id = $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');

      // get subscriber's transaction history
      teldata_change_db();

      if ( ! $start_epoch && ! $end_epoch && ! $recent)
        $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing.', 'MP0001', 'Internal system error.');

      $result = get_billing_transaction_history(array(
        'customer_id' => $customer_id,
        'start_epoch' => $start_epoch,
        'end_epoch'   => $end_epoch,
        'recent'      => $recent));

      $history = array();

      $i = 0;
      foreach ($result['billing_transaction_history'] as $key => $value)
      {
        if ($value->result == 'COMPLETE')
          $history[] = $result['billing_transaction_history'][$i];
        $i++;
      }

      $this->addToOutput('transaction_history', $history);
      $this->addToOutput('record_count', count($history));

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>