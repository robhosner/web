<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;
use Ultra\Lib\Api\Traits\FlexHandler;

class SetDataLimit extends \Ultra\Lib\Api\Partner\Portal
{
  use FlexHandler;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var SharedData
   */
  private $sharedData;

  /**
   * @var FamilyAPI
   */
  private $familyAPI;

  /**
   * SetDataLimit constructor.
   * @param Session $session
   */
  public function __construct(FamilyAPI $familyAPI, Session $session, SharedData $sharedData)
  {
    $this->session = $session;
    $this->familyAPI = $familyAPI;
    $this->sharedData = $sharedData;
  }

  public function portal__SetDataLimit()
  {
    list ($customer_id, $percent) = $this->getInputValues();

    try {
      // verify session
      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }

      // use customer ID in session if null
      $customer_id = empty($customer_id) ? $this->session->customer_id : $customer_id;

      // check privileges
      if ($customer_id != $this->session->customer_id) {
        $familyResult = $this->familyAPI->getFamilyByCustomerID($customer_id);
        if (!$familyResult->is_success()) {
          $this->errException('Failed to retrieve family', 'IN0002');
        }

        if ($this->session->customer_id != $familyResult->data_array['parentCustomerId']) {
          $this->errException('Insufficient privileges', 'IN0002');
        }
      }

      $this->setDataLimit($this->sharedData, $customer_id, $percent);

      $this->succeed();
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
