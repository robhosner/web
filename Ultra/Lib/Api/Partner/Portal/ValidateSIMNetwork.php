<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\MiddleWare\Adapter\Control;

class ValidateSIMNetwork extends Portal
{
  /**
   * @var Control
   */
  private $mwControl;
  public $actionId;

  /**
   * ValidateSIMNetwork constructor.
   * @param Control $mwControl
   */
  public function __construct(Control $mwControl)
  {
    $this->mwControl = $mwControl;
    $this->actionId = getNewActionUUID(__FUNCTION__ . time());
  }

  /**
   * portal__ValidateSIMNetwork
   *
   * check ICCID validity on the MVNE2 network
   *
   * @param string ICCID
   * @return Result object
   */
  public function portal__ValidateSIMNetwork()
  {
    // init
    list($ICCID) = $this->getInputValues();
    $this->addToOutput('valid_network', null);

    try {
      // check ICCID on MVNE
      $result = $this->mwControl->mwCanActivate(['actionUUID' => $this->actionId, 'iccid'  => $ICCID]);

      // check result
      if ($result->is_failure()) {
        if ($result->is_timeout()) {
          return $this->errException('ERR_API_INTERNAL: MW timeout', 'MW0002');
        } else {
          return $this->errException('ERR_API_INTERNAL: ' . ($result->has_errors() ? implode('; ', $result->get_errors()) : 'MW error'), 'MW0001');
        }
      } else {
        if (!isset($result->data_array['available'])) {
          return $this->errException('ERR_API_INTERNAL: MW did not return SIM availability', 'MW0001');
        }

        $this->addToOutput('valid_network', !!$result->data_array['available']);
      }

      $this->succeed();
    } catch(\Exception $e) {
      dlog('', 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }
}
