<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class VerifyPortInStatus extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__VerifyPortInStatus
   *
   * to be invoked after portal__RequestProvisionPortedCustomer in order to get the complete response
   *
   * @return Result object
   */
  public function portal__VerifyPortInStatus()
  {
    // init
    list ($request_id) = $this->getInputValues();
    $this->addToOutput('port_pending', NULL);
    $this->addToOutput('phone_number', NULL);
    $this->addToOutput('port_success', NULL);
    $this->addToOutput('port_status', NULL);
    $this->addToOutput('port_resolution', array());

    try
    {
      // get subscriber from zsession cookie
      teldata_change_db();
      if (empty($_COOKIE['zsession']))
        $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
      $zsession = $_COOKIE['zsession'];
      if (! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $this->setOutputLanguage($customer->preferred_language);

      // get transition and check result
      $transition_result = provision_check_transition($request_id);
      $errors = append_transition_failure_reason($transition_result['errors'], $request_id);
      if ($errors && is_array($errors) && count($errors))
      {
        $this->addToOutput('port_pending', FALSE);
        $this->addToOutput('port_success', FALSE);
        $this->addToOutput('port_status', 'ERROR');
        $this->addToOutput('port_resolution', $errors);
        $this->errException('ERR_API_INTERNAL: provisioning errors - ' . implode('; ', $errors), 'PR0001');
      }
      else
      {
        // get data from PORTIN_QUEUE
        $portInQueue = new \PortInQueue();
        $loadByCustomerIdResult = $portInQueue->loadByCustomerId( $transition_result['port_attempt_customer_id'] );
        if ( $loadByCustomerIdResult->is_success() )
        {
          $this->addToOutput('phone_number',$portInQueue->msisdn);
          list(
            $port_success,
            $port_pending,
            $port_status,
            $port_resolution
          ) = interpret_port_status( $portInQueue, $transition_result );

          $this->addToOutput('port_success',    $port_success);
          $this->addToOutput('port_pending',    $port_pending);
          $this->addToOutput('port_status',     $port_status);

          if ( $port_resolution )
            $this->addToOutput('port_resolution', array($port_resolution));
        }
        else
        {
          // the port request is not yet recorded in PORTIN_QUEUE
          $this->addToOutput('port_success', FALSE);
          $this->addToOutput('port_pending', TRUE);
          $this->addToOutput('port_status', 'initialized');
        }

        teldata_change_db();
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , 'ERROR: ' . $e->getMessage () );
    }

    return $this->result;
  }
}

