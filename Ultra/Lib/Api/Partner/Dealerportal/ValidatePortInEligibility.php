<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidatePortInEligibility extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ValidatePortInEligibility
   *
   * Is a MSISDN eligible for PortIn? Validates on input.
   *
   * @return Result object
   */
  public function dealerportal__ValidatePortInEligibility ()
  {
    list ( $security_token , $msisdn, $iccid, $brand ) = $this->getInputValues();

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // check for intra-brand port
      $customer = get_customer_from_msisdn($msisdn, 'u.BRAND_ID,plan_state');
      if ($customer && $brand)
      {
        // then may be intra-brand port
        $this->validateIntraPort($customer, \Ultra\BrandConfig\getBrandFromShortName($brand));
      }
      else
        $this->validateNormalPort($msisdn);
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

  /**
   * validateNormalPort
   *
   * checks if brands are different and customer in correct state
   * else deny due to MSISDN existing in system
   * adjusts Result objects for class
   *
   * @param  $customer [plan_state, BRAND_ID]
   * @param  $requestBrand brand configuration for input brand
   * @return void
   */
  private function validateIntraPort($customer, $requestBrand)
  {
    if ($customer->BRAND_ID != $requestBrand['id'])
    {
      if ( ! in_array($customer->plan_state, array(STATE_ACTIVE,STATE_SUSPENDED)))
        $this->errException('ERR_API_INTERNAL: port-In denied due to customer state', 'EL0001');
      else
        $this->succeed();
    }
    else
      $this->errException('ERR_API_INTERNAL: port-In denied due to msisdn already in our system', 'PO0008');
  }

  /**
   * validateNormalPort
   *
   * normal port in attempt through MW
   * returns void but throws exception
   * adjusts Result objects for class
   *
   * @param  $msisdn to validate
   * @return void
   */
  private function validateNormalPort($msisdn)
  {
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
    $mwResult = $mwControl->mwPortInEligibility(array(
      'actionUUID' => getNewActionUUID('dealerportal ' . time()),
      'msisdn'     => $msisdn));

    // warnings are not fatal
    $warnings = $mwResult->get_warnings();
    if (count($warnings))
      $this->addWarnings($warnings);

    // handle errors
    $errors = $mwResult->get_errors();
    if (count($errors))
    {
      $this->addErrors($errors);
      $this->errException('ERR_API_INTERNAL: MW error', 'MW0001');
    }

    // determine eligibility
    $data = $mwResult->get_data_array();
    if ($mwResult->is_success() && isset($data['eligible']) && $data['eligible'])
      $this->succeed();
    else
    {
      if (isset($data['failure_reason']))
        $this->addWarning('Reason: ' . $data['failure_reason']);
      $this->errException('ERR_API_INVALID_ARGUMENTS: number is not eligible to port in', 'EL0001');
    }
  }
}