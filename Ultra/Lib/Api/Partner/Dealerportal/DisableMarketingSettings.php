<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class DisableMarketingSettings extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__DisableMarketingSettings
   *
   * disable Marketing Settings for a customer
   *
   * @param: customer_id
   * @param: marketing_settings is array of values only ('marketing_sms_option', '1', marketing_email_option, '0')
   * @return Result object
   * @author: VYT, 14-01-20
   */
  public function dealerportal__DisableMarketingSettings()
  {
    list ( $security_token , $customer_id , $marketing_settings ) = $this->getInputValues();

    try
    {
      // check input
      if (! isset($marketing_settings) || ! count($marketing_settings))
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing marketing_settings value', 'VV0100');

      // retrieve and validate session
      teldata_change_db();
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, NULL, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // validate marketing settings: a bit akward since we pack everything into a flat array
      $validation = array();
      $i = 0;
      foreach ($marketing_settings as $setting)
      {
        if ($setting == 'marketing_voice_option')
        {
          unset($marketing_settings[$i]);
          $result = opt_out_voice_preference($customer_id);
          if (!$result->is_success())
            $this->errException('ERR_API_INTERNAL: Error writing to ULTRA.CUSTOMER_OPTIONS', 'DB0001');
        }
        else
        {
          $validation[] = $setting;
          $validation[] = 0;
        }

        $i++;
      }

      if (count($marketing_settings))
      {
        $errors = validate_marketing_settings($validation);
        if ($errors && count($errors))
          $this->errException($errors[0] , 'VV0100');

        // pack parameters and update settings
        $params = array('customer_id' => $customer_id);
        foreach ($marketing_settings as $setting)
          $params[$setting] = 0;
        $errors = set_marketing_settings($params);
        if (count($errors))
          $this->errException($errors[0] , 'DB0001');
      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}