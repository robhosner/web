<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class ListReportBusinesses extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListReportBusinesses
   * returns a list of businesses eligible for reporting
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @see DEAL-36
   * @return Result object
   */
  public function dealerportal__ListReportBusinesses()
  {
    // initialization
    list($security_token) = $this->getInputValues(); // security_token
    $business_code = NULL;
    $record_count = 0;
    $businesses = array();

    try
    {
      // verify session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // Master may only query itself
      if (in_array($session_data['role'], array(self::ROLE_NAME_MASTER_ALL, self::ROLE_NAME_MASTER_LIMITED, self::ROLE_NAME_MASTER_EMPLOYEE)))
        $business_code = $session_data['business_code'];

      // get eligible businesses
      list($businesses, $error) = \Ultra\Lib\DB\DealerPortal\listReportBusinesses($business_code);
      if ($error)
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('businesses', $businesses);
    $this->addToOutput('record_count', count($businesses));
    return $this->result;
  }
}

