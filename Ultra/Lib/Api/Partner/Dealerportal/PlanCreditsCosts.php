<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';
require_once 'classes/LineCredits.php';

class PlanCreditsCosts extends \Ultra\Lib\Api\Partner\Dealerportal
{
  public function __construct()
  {
    parent::__construct();
  }

  public function dealerportal__PlanCreditsCosts()
  {
    list ($security_token, $customerId, $amount) = $this->getInputValues();

    try
    {
      // validate dealer portal session
      list ($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code) $this->errException($error, $error_code);

      // connects to line credit database
      $lineCredits = new \LineCredits();

      // do line credit transfer
      $costs = $lineCredits->getCosts();
      if (empty($costs))
        $this->errException('ERR_API_INTERNAL: error getting costs from database', 'DB0001');

      $output = [];
      foreach ($costs as $cost)
        $output[$cost->CreditsPurchased] = $cost->CreditsTotalCost;

      $this->addToOutput('costs', $output);
      $this->succeed();
    }
    catch (\Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
