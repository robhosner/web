<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Dealerportal;

/**
 * Class GetE911SubscriberAddress
 * @package Ultra\Lib\Api\Partner\Portal
 */
class GetE911SubscriberAddress extends Dealerportal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * GetE911SubscriberAddress constructor.
   * @param CustomerRepository $customerRepository
   */
  public function __construct(CustomerRepository $customerRepository)
  {
    $this->customerRepository = $customerRepository;
    parent::__construct();
  }

/**
   * dealerportal__GetE911SubscriberAddress
   *
   * Retrieve customer E911 Subscriber Address
   *
   * @param  Integer $customer_id
   * @return Result object
   */
  public function dealerportal__GetE911SubscriberAddress()
  {
    list($security_token, $customer_id) = $this->getInputValues();

    try {
      // retrieve and validate session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);

      if ($error_code) {
        return $this->errException($error, $error_code);
      }

      $this->customer = $this->customerRepository->getCustomerById($customer_id, [], true);

      $this->addToOutput('address', $this->customer->getAccAddressE911());

      // success
      $this->succeed();
    } catch (MissingRequiredParametersException $e) {
      $this->addError($e->getMessage(), 'VV0031');
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
