<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ClearCreditCard extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ClearCreditCard
   *
   * remove customer credit card info
   *
   * @param: customer_id
   * @return Result object
   * @author: VYT, 14-01-27, adopted from v1
   */
  public function dealerportal__ClearCreditCard()
  {
    // init
    list ( $security_token , $customer_id ) = $this->getInputValues();

    try
    {
      // retrieve and validate session
      teldata_change_db(); // connect to the DB
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // check if customer is valid
      $customer = get_customer_from_customer_id($customer_id);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // prepare query
      $query = customers_update_query(
        array(
          'SET_NULL_CREDIT_CARD_DATA' => TRUE,
          'customer_id' => $customer_id));

      // execute
      if ( ! run_sql_and_check($query) )
        $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');

      // done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}