<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class GetRechargeDetailByDealerMaster extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetRechargeDetailByDealerMaster
   * report period recharge stats for each dealer under the authenticated master agent for the given month
   * @see SMR-16
   */
  public function dealerportal__GetRechargeDetailByDealerMaster()
  {
    // init
    list ($security_token, $recharge_period) = $this->getInputValues();
    $details_by_plan = array();
    $record_count = 0;

    try
    {
      // retrieve and validate session from $security_token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);
      $master = $session_data['masteragent'];

      // get cached results if available
      $recharge_period = sprintf('%d/1/%d', substr($recharge_period, 0, 2), 2000 + substr($recharge_period, -2));
      $redis_key = __FUNCTION__ . "/$master/$recharge_period";
      $redis = new \Ultra\Lib\Util\Redis;
      if ($cache = $redis->get($redis_key))
      {
        $details_by_plan = json_decode($cache);
      }
      else // retrieve from DB
      {
        // get all dealer groups for this master
        $groups = get_dealer_groups_from_master($master);
        if ( ! count($groups))
          $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');

        // get period recharge rates for each group
        foreach ($groups as $group)
        {
          $activations = get_summary_dealer_recharge_rate_by_dealers($group->children, $recharge_period);
          if (count($activations))
          {
            $detail = clone $activations[0];
            $detail->dealer_code = $group->dealer_code;
            $detail->business_name = utf8_encode($group->business);
            $details_by_plan[] = $detail;
          }
        }

        // cache result for 30 minutes
        $redis->set($redis_key, json_encode($details_by_plan), 30 * 60);
      }
      $record_count = count($details_by_plan);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('details_by_plan', $details_by_plan);
    $this->addToOutput('record_count', $record_count);
    return $this->result;
  }
}