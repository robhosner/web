<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class CheckBalances extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckBalances
   *
   * @param int customer_id
   * @return object Result
   */
  public function dealerportal__CheckBalances()
  {
    list ($customer_id) = $this->getInputValues();

    $balances = [];

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, ['current_mobile_number']);
      if ( ! $customer || ! $customer->current_mobile_number)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      $cb = new \Ultra\Lib\CheckBalance();
      $balances = $cb->byMSISDN($customer->current_mobile_number);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('balances', $balances);

    return $this->result;
  }
}
