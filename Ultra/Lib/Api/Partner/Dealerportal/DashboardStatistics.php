<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class DashboardStatistics extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__DashboardStatistics
   *
   * return Dealer Portal Dashboard statistics
   *
   * @author VYT, 14-05-07
   */
  public function dealerportal__DashboardStatistics()
  {
    // init
    list ($security_token) = $this->getInputValues();
    $this->addToOutput('customers', array());
    $this->addToOutput('activations', array());
    $this->addToOutput('second_month_recharge_rate', array());
    $this->addToOutput('activations_by_month', array());

    try
    {
      teldata_change_db();

      // retrieve and validate security token
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);
      $dealer = $session_data['dealer'];

      // get all-time customer count by plan
      $redis = new \Ultra\Lib\Util\Redis();
      $key = parent::DASHBOARD_CUSTOMERS_KEY . $dealer;
      if ($cache = $redis->get($key))
      {
        $customers = json_decode($cache);
      }
      else
      {
        $customers = get_ultra_dealer_customers_by_plan($session_data['role'] == self::ROLE_NAME_ADMIN ? NULL : $dealer);
        if (! $customers || ! count($customers))
          $this->errException('ERR_API_INTERNAL: DB error (1)', 'DB0001');
        $redis->set($key, json_encode($customers), parent::DASHBOARD_CUSTOMERS_TTL);
      }
      $this->addToOutput('customers', $customers);

      // get recent activations by plan
      $key = parent::DASHBOARD_ACTIVATIONS_KEY . $dealer;
      if ($cache = $redis->get($key))
      {
        $activations = json_decode($cache);
      }
      else
      {
        $activations = get_ultra_dealer_activations_by_plan($session_data['role'] == self::ROLE_NAME_ADMIN ? NULL : $dealer);
        if (! $activations || ! count($activations))
          $this->errException('ERR_API_INTERNAL: DB error (2)', 'DB0001');
        $redis->set($key, json_encode($activations), parent::DASHBOARD_ACTIVATIONS_TTL);
      }
      $this->addToOutput('activations', $activations);

      // get dealer's children and merge into single array
      $key = __FUNCTION__ . "/$dealer/dealers";
      if ($cache = $redis->get($key))
      {
        $dealers = json_decode($cache);
      }
      else
      {
        $dealers = get_parent_dealer_children($dealer);
        $dealers[] = $dealer;
        $redis->set($key, json_encode($dealers), parent::DASHBOARD_RECHARGE_TTL);
      }

      // retrieve data from ULTRA.SUMMARY_DEALER_RECHARGE_RATE
      $key = __FUNCTION__ . "/$dealer/recharge";
      if ($cache = $redis->get($key))
      {
        $recharge = json_decode($cache);
      }
      else
      {
        $recharge = get_second_month_recharge_rate_by_dealer($dealers);
        if ($recharge && count($recharge))
          $redis->set($key, json_encode($recharge), parent::DASHBOARD_RECHARGE_TTL);
      }

      if ( $recharge )
        $this->addToOutput( 'second_month_recharge_rate' , $recharge );

      \Ultra\Lib\DB\dealer_portal_connect();
      $activations_by_month = get_ultra_dealer_activations_by_month_stored_proc($dealer, 2);
      teldata_change_db();

      if (count($activations_by_month))
      {
        for ($i = 0; $i < count($activations_by_month); $i++)
        {
          $activations_by_month[$i]->month_start = strtotime($activations_by_month[$i]->month_start);
        }

        $this->addToOutput('activations_by_month', $activations_by_month);
      }

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }
}