<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;
use Ultra\Mvne\Adapter;

class RemoveFromFamily extends Dealerportal
{
  use FlexHandler;

  /**
   * @var FamilyAPI
   */
  private $familyAPI;

  /**
   * @var Adapter
   */
  private $adapter;

  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var SharedData
   */
  private $sharedData;

  /**
   * RemoveFromFamily constructor.
   * @param FamilyAPI $familyAPI
   * @param Adapter $adapter
   * @param AccountsRepository $accountsRepository
   * @param Configuration $configuration
   * @param SharedData $sharedData
   */
  public function __construct(
    FamilyAPI $familyAPI,
    Adapter $adapter,
    AccountsRepository $accountsRepository,
    Configuration $configuration,
    SharedData $sharedData
  ) {
    $this->familyAPI = $familyAPI;
    $this->adapter = $adapter;
    $this->accountsRepository = $accountsRepository;
    $this->configuration = $configuration;
    $this->sharedData = $sharedData;

    parent::__construct();
  }

  public function dealerportal__RemoveFromFamily()
  {
    list($security_token, $customerId) = $this->getInputValues();

    try {
      teldata_change_db();

      // validate dealer portal session
      list ($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code) $this->errException($error, $error_code);

      $this->removeFromFamily($customerId, $this->accountsRepository, $this->familyAPI, $this->sharedData, $this->adapter, $this->configuration);
      $this->succeed();
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
