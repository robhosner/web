<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

use Ultra\Lib\Util\Redis as RedisUtils;

class ChargeCustomerPINs extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ChargeCustomerPINs
   *
   * Tests and charges up to 6 pin cards; puts the balance into the account; then tries to activate the customer if necessary.
   *
   * - fails API call if there are more than 2500 calls in an hour
   * - fails API call if customer status is not one of the following: Active, Provisioned, Suspended, Port-In Requested, Neutral
   * - validate PINS
   * - fails API call if ( stored_value + balance + new_load ) is greater than $200
   * - if there are no transitions in progress for this customer, attempts to transition from {Provisioned|Suspended} to Active
   *
   * @return Result object
   */
  public function dealerportal__ChargeCustomerPINs ()
  {
    list ( $security_token , $customer_id , $pin_list , $destination ) = $this->getInputValues();

    if ( ( ! $destination ) || ( $destination == '' ) )
      $destination="WALLET";

    $this->addToOutput('pins_total_amount','');
    $this->addToOutput('customer_activated',FALSE);

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      if ( ! count( $pin_list ) || ( count( $pin_list ) > 6 ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The command requires from one to six PINs." , 'PI0010' );

      $redis = new \Ultra\Lib\Util\Redis;

      // block if there are more than 2500 calls in an hour
      RedisUtils\redis_increment_command_count_by_hour( $redis , "dealerportal__ChargeCustomerPINs" );

      // block if there are more than 2500 calls in an hour
      if ( RedisUtils\redis_get_command_count_by_hour( $redis , "dealerportal__ChargeCustomerPINs" ) > 2500 )
        $this->errException( "ERR_API_INTERNAL: this command has been currently disabled. Please try again later." , 'AP0001' );

      $customer = get_customer_from_customer_id($customer_id);

      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      // get customer state
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( ! $state )
        $this->errException( "ERR_API_INTERNAL: Could not load current state for customer" , 'UN0001' );

      // state must be one of the following: Active, Provisioned, Suspended, Port-In Requested, Neutral.
      if ( ( $state['state'] != 'Suspended' )
        && ( $state['state'] != 'Neutral' )
        && ( $state['state'] != 'Active' )
        && ( $state['state'] != 'Provisioned' )
        && ( $state['state'] != 'Port-In Requested' ) )
      {
        $this->errException( "ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command" , 'IN0001' );
      }

      $validate_pin_result = func_validate_pin_cards(
        array(
          'pin_list' => $pin_list
        )
      );

      dlog('',"validate_pin_result = %s",$validate_pin_result);

      if ( $validate_pin_result["at_least_one_customer_used"] )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: One or more PINs are already used" , 'PI0007' );

      if ( $validate_pin_result["at_least_one_not_found"] )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: Could not find one or more PINs" , 'PI0008' );

      // PIN status should be AT_MASTER
      if ( $validate_pin_result["at_least_one_at_foundry"] )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: One or more PINs are cannot be used" , 'PI0009' );

      $pins_total_amount = 0;

      foreach($validate_pin_result["values"] as $value)
      {
        $pins_total_amount += $value;
      }

      // stored_value+Balance+new_load must not be greater than $200
      dlog('',"total_value = ".( $pins_total_amount + $customer->BALANCE + $customer->stored_value ) );

      if ( ( $pins_total_amount + $customer->BALANCE + $customer->stored_value ) > 200 )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet. Please use up some of your current balance before adding more." , 'CH0001' );

// TODO: dealer should be recorded into HTT_BILLING_HISTORY
      $store_id = '0';

      $return = func_apply_pin_cards(
        array(
          "destination" => $destination,
          "pin_list"    => $pin_list,
          "customer"    => $customer,
          "source"      => 'WEBPIN',
          "reference"   => create_guid('dealerportal'),
          "reason"      => __FUNCTION__,
          "entry_type"  => 'LOAD',
          "store_id"    => $store_id,
          'validated'   => $validate_pin_result
        )
      );

      if ( count($return['errors']) )
        $this->errException( "ERR_API_INTERNAL: An unexpected database error occurred while applying PIN cards" , 'DB0001' );

      if ( $return['activated'] )
        $this->addToOutput('customer_activated',TRUE);

      $this->addToOutput('pins_total_amount',$pins_total_amount);

      // update ULTRA.HTT_ACTIVATION_HISTORY if necessary

      $activation_history = get_ultra_activation_history( $customer->CUSTOMER_ID );

      if ( $activation_history
        && ( $activation_history->FINAL_STATE != FINAL_STATE_COMPLETE )
        && ( $activation_history->FINAL_STATE != 'Suspended' )
        && ( $activation_history->FINAL_STATE != 'Cancelled' )
      )
        log_funding_in_activation_history( $customer->CUSTOMER_ID , 'PIN' , $pins_total_amount );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}