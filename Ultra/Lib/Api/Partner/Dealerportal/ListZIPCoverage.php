<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ListZIPCoverage extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListZIPCoverage
   *
   * Provides the coverage levels of Ultra Service in the specified Zipcode
   *
   * @return Result object
   */
  public function dealerportal__ListZIPCoverage ()
  {
    list ( $security_token , $zipcode ) = $this->getInputValues();

    $quality = '';

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $redis = new \Ultra\Lib\Util\Redis;

      // check Redis for an already computed value within 3 days. Return that value if it exists.
      $quality = \Ultra\Lib\Util\Redis\get_coverage_info_quality($zipcode, $redis);

      if ( ! isset($quality) || $quality == '' )
      {
        // Redis does not contain a value for the given zip code, we must query the DB

        // query htt_coverage_info for the given zip code
        $query = htt_coverage_info_select_query( array('zip_code' => $zipcode) );

        $coverage_data = \Ultra\Lib\DB\fetch_objects($query);

        $quality = 0;

        if ( $coverage_data && is_array($coverage_data) && count($coverage_data) )
          $quality = $coverage_data[0]->quality;

        // save the result in Redis.
        \Ultra\Lib\Util\Redis\set_coverage_info_quality($zipcode, $quality, 3*60*60*24, $redis);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $this->addToOutput('quality',$quality);

    return $this->result;
  }
}