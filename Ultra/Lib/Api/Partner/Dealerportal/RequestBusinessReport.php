<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class RequestBusinessReport extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__RequestBusinessReport
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @see DEAL-36
   * @return Result object
   */
  public function dealerportal__RequestBusinessReport()
  {
    // initialization
    $params = $this->getNamedInputValues(); // security_token, business_type, business_code, request_type, period_id, notify, inactive, start_date, end_date

    try
    {
      // verify session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      $params = $this->overwriteIdentityParameters($params, $session_data, 'MVNO');

      $params['user_id'] = $session_data['user_id'];
      if ($error = \Ultra\Lib\DB\DealerPortal\requestBusinessReport($params))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
