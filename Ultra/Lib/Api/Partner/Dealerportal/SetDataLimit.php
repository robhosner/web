<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

use Ultra\Lib\Services\SharedData;
use Ultra\Lib\Api\Traits\FlexHandler;

/**
 * Class SetDataLimit
 * @param SharedData $sharedData
 */
class SetDataLimit extends \Ultra\Lib\Api\Partner\Dealerportal
{
  use FlexHandler;

  /**
   * @var SharedData
   */
  private $sharedData;

  public function __construct(
      SharedData $sharedData
  ) {
    $this->sharedData = $sharedData;
    parent::__construct();
  }

  public function dealerportal__SetDataLimit()
  {
    list ($security_token, $customerId, $percent) = $this->getInputValues();

    try
    {
      // validate dealer portal session
      list ($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code) $this->errException($error, $error_code);

      $this->setDataLimit($this->sharedData, $customerId, $percent);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
