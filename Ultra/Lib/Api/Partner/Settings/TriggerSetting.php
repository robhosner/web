<?php

namespace Ultra\Lib\Api\Partner\Settings;

require_once 'Ultra/Lib/Api/Partner/Settings.php';

class TriggerSetting extends \Ultra\Lib\Api\Partner\Settings
{
  /**
   * settings__TriggerSetting
   *
   * Value switches getter/setter.
   * If $setting_value is passed, overwrite the content.
   *
   * @return Result object
   */
  public function settings__TriggerSetting ()
  {
    list ( $username , $setting_field , $setting_value ) = $this->getInputValues();

    $error_code = '';

    $this->addToOutput('previous_value','');
    $this->addToOutput('new_value'     ,'');
    $this->addToOutput('ttl_seconds'   ,'');

    try
    {
      teldata_change_db();

      // TODO: $username check + auditing
      // TODO: any IP check?
      $error_code = 'SM0001';
      $settings = new \Ultra\Lib\Util\Settings;

      // retrieve from DB
      $error_code = 'ND0001';
      list($value,$ttl) = $settings->checkUltraSettingsFromDB( $setting_field );

      if ( $value == '' )
        throw new \Exception("ERR_API_INTERNAL: data not found.");

      $this->addToOutput('previous_value', $value );
      $this->addToOutput('new_value'     , $value );
      $this->addToOutput('ttl_seconds'   , $ttl   );

      if ( ( isset($setting_value) ) && ( $setting_value != '' ) )
      {
        dlog('',"SETTINGS MODIFIED : FIELD = $setting_field ; OLD VALUE = $value ; NEW VALUE = $setting_value ; USERNAME = $username");

        // set at DB level
        $error_code = 'DB0001';
        $success = assignUltraSettings( $setting_field , $setting_value , $ttl );

        if ( ! $success )
          throw new \Exception("ERR_API_INTERNAL: DB write error.");

        $this->addToOutput('new_value',$setting_value);

        // removes token from Redis so that the next access will be done at DB level
        $settings->forceDBRefreshSettings( $setting_field );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

