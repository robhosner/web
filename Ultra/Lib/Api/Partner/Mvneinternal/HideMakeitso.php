<?php

namespace Ultra\Lib\Api\Partner\Mvneinternal;

require_once 'Ultra/Lib/Api/Partner/Mvneinternal.php';

class HideMakeitso extends \Ultra\Lib\Api\Partner\Mvneinternal
{
  
  /**
   * mvneinternal__HideMakeitso
   *
   * hides a makeitso entry
   *
   * @return Result object
   */
  public function mvneinternal__HideMakeitso()
  {
    // init
    list ($makeitso_queue_id) = $this->getInputValues();

    try
    {
      // get MISO data
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');

      if (! hide_makeitso(array('makeitso_queue_id' => $makeitso_queue_id)))
        $this->errException('ERR_API_INTERNAL: failed to update to database', 'DB0001');

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage ());
    }
    return $this->result;
  }
}
