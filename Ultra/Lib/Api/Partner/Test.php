<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Test partner class
 */
class Test extends \Ultra\Lib\Api\PartnerBase
{
  /**
   * test__ValidateICCIDBrand
   */
  public function test__ValidateICCIDBrand()
  {
    // init
    list ($iccid) = $this->getInputValues();

    $this->succeed();

    // get SIM info
    $sim = \get_htt_inventory_sim_from_iccid($iccid);

    if ( $sim )
    {
      // validate SIM brand
      \logit( "API partner = {$this->partner} ; BRAND_ID = {$sim->BRAND_ID}" );

      if ( ! \Ultra\UltraConfig\isBrandAllowedByAPIPartner( $sim->BRAND_ID , $this->partner ) )
        $this->addError( 'Sim brand not allowed' , 'IC0003' );
    }
    else
      $this->addError( 'ERR_API_INVALID_ARGUMENTS: invalid SIM card' , 'VV0065');

    return $this->result;
  }
}

