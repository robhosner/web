<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'db.php';
require_once 'Ultra/Lib/StateMachine.php';
#require_once 'Primo/Lib/StateMachine/DataEngineMongoDB.php';
require_once 'Ultra/Lib/StateMachine/DataEngineMSSQL.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRAM.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRedis.php';

# Ultra specific actions and requirements
require_once 'Ultra/Lib/StateMachine/Action/functions.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RecoverAbortedTransition extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RecoverAbortedTransition
   *
   * Recovers an ABORTED transition by transition_uuid
   *
   * @param string transition_uuid
   * @return Result object
   */
  public function internal__RecoverAbortedTransition()
  {
    list ($transition_uuid) = $this->getInputValues();

    $abortedTransitions = [];

    try
    {
      teldata_change_db();

      $redis = new \Ultra\Lib\Util\Redis();
      $dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;

      $sm = new \Ultra\Lib\StateMachine( $dataEngineMSSQLObject , NULL , NULL , $redis );

      $sm->loadTransitionData($transition_uuid);
      $transition = $sm->getCurrentTransitionData();

      if ( ! $transition)
        $this->errException("Transition $transition_uuid does not exist", 'IN0001');

      if ($transition['status'] != 'ABORTED')
        $this->errException("Transition $transition_uuid is not ABORTED", 'IN0001');

      $sql    = "SELECT TOP 1 * FROM HTT_ACTION_LOG with (nolock) WHERE TRANSITION_UUID = '$transition_uuid'";
      $action = find_first($sql);

      if ( ! $action)
        $this->errException("Transition $transition_uuid has no aborted action", 'IN0001');

      $customer = (array)get_customer_from_customer_id($transition['customer_id']);
      $sm->setCustomerData($customer);

      // set transition configuration
      $result = $sm->selectTransitionByLabel(
        $transition['transition_label'],
        $customer['plan_state'],
        get_plan_name_from_short_name(get_plan_from_cos_id($transition['from_cos_id']))
      );

      if ( ! $result )
        $this->errException("Could not find transition configuration for $transition_uuid", 'IN0001');

      $sm->generateActionData($action->ACTION_NAME);

      if ( ! $sm->runTransition($reserve = FALSE) )
        $this->errException("Transition $transition_uuid recover failed", 'IN0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
