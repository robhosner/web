<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Orders\Interfaces\OrderRepository;

/**
 * Class AddVertexOnlineOrder
 * @package Ultra\Lib\Api\Partner\Internal
 */
class AddVertexOnlineOrder extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * @var OrderRepository
   */
  private $orderRepository;

  /**
   * AddVertexOnlineOrder constructor.
   * @param OrderRepository $orderRepository
   */
  public function __construct(OrderRepository $orderRepository)
  {
    $this->orderRepository = $orderRepository;
  }

  /**
   * internal__AddVertexOnlineOrder
   *
   * Cron runner API
   *
   * @param string mode
   * @return Result object
   */
  public function internal__AddVertexOnlineOrder()
  {
    try
    {
      $input = array_combine([
        'ordernum',
        'iccid',
        'imei',
        'SKU',
        'first_name',
        'last_name',
        'email',
        'address1',
        'address2',
        'city',
        'state',
        'postal_code',
        'token',
        'bin',
        'last_four',
        'expires_date',
        'gateway',
        'merchant_account',
        'avs_validation',
        'cvv_validation',
        'customer_ip',
        'preferred_language',
        'auto_enroll',
        'track_trace',
      ], $this->getInputValues());

      $this->validateCreditCardExpirationDate($input['expires_date']);
      
      if (!$this->orderRepository->addOrder($input))
      {
        return $this->errException('ERR_API_INTERNAL: Error adding an order to ULTRA.ONLINE_ORDERS', 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Validate a credit cards expiration date.
   *
   * @param $expirationDate
   * @return NULL
   * @throws \Exception
   */
  private function validateCreditCardExpirationDate($expirationDate)
  {
    // make sure the first two digits are a valid month and last two are not in the past
    if (
      !(substr($expirationDate, 0, 2) > 0 && substr($expirationDate, 0, 2) < 13) ||
      !(substr($expirationDate, -2) > (date('y') - 1))
    )
    {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: expires_date is invalid', 'VV0007');
    }
  }
}
