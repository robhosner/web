<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetAllAbortedTransitions extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetAllAbortedTransitions
   *
   * Get all aborted transition for a given date
   *
   * @param string transition_date
   * @param int customer_id
   * @return Result object
   */
  public function internal__GetAllAbortedTransitions()
  {
    list ($transition_date, $customer_id) = $this->getInputValues();

    $transitions_active_to_suspended = array();
    $transitions_active_to_active    = array();
    $transitions_suspended_to_active = array();
    $transitions_neutral_to_promounused = array();
    $transitions_promounusedto_active   = array();
    $transitions_neutral_to_provisioned = array();

    try
    {
      if ( ( ! $transition_date ) || ( $transition_date == '' ) )
        $transition_date = date("m-d-Y");

      teldata_change_db();

      $result_active_to_suspended    = get_aborted_state_transitions( $transition_date , STATE_ACTIVE       , STATE_SUSPENDED    , $customer_id );
      $result_active_to_active       = get_aborted_state_transitions( $transition_date , STATE_ACTIVE       , STATE_ACTIVE       , $customer_id );
      $result_suspended_to_active    = get_aborted_state_transitions( $transition_date , STATE_SUSPENDED    , STATE_ACTIVE       , $customer_id );
      $result_neutral_to_promounused = get_aborted_state_transitions( $transition_date , STATE_NEUTRAL      , STATE_PROMO_UNUSED , $customer_id );
      $result_promounusedto_active   = get_aborted_state_transitions( $transition_date , STATE_PROMO_UNUSED , STATE_ACTIVE       , $customer_id );
      $result_neutral_to_provisioned = get_aborted_state_transitions( $transition_date , STATE_NEUTRAL      , STATE_PROVISIONED  , $customer_id );
      $result_neutral_to_active      = get_aborted_state_transitions( $transition_date , STATE_NEUTRAL      , STATE_ACTIVE       , $customer_id );

      if (count($result_active_to_suspended['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_active_to_suspended['errors'][0], 'IN0002');

      if (count($result_active_to_active['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_active_to_active['errors'][0], 'IN0002');

      if (count($result_suspended_to_active['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_suspended_to_active['errors'][0], 'IN0002');

      if (count($result_neutral_to_promounused['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_neutral_to_promounused['errors'][0], 'IN0002');

      if (count($result_promounusedto_active['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_promounusedto_active['errors'][0], 'IN0002');

      if (count($result_neutral_to_provisioned['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_neutral_to_provisioned['errors'][0], 'IN0002');

      if (count($result_neutral_to_active['errors']))
        $this->errException('ERR_API_INTERNAL: ' . $result_neutral_to_active['errors'][0], 'IN0002');

      $transitions_active_to_suspended    = $this->parse_aborted_state_transitions( $result_active_to_suspended['state_transitions'] );
      $transitions_active_to_active       = $this->parse_aborted_state_transitions( $result_active_to_active['state_transitions'] );
      $transitions_suspended_to_active    = $this->parse_aborted_state_transitions( $result_suspended_to_active['state_transitions'] );
      $transitions_neutral_to_promounused = $this->parse_aborted_state_transitions( $result_neutral_to_promounused['state_transitions'] );
      $transitions_promounusedto_active   = $this->parse_aborted_state_transitions( $result_promounusedto_active['state_transitions'] );
      $transitions_neutral_to_provisioned = $this->parse_aborted_state_transitions( $result_neutral_to_provisioned['state_transitions'] );
      $transitions_neutral_to_active      = $this->parse_aborted_state_transitions( $result_neutral_to_active['state_transitions'] );

      $this->addArrayToOutput(array(
        "transitions_active_to_suspended"    => $transitions_active_to_suspended,
        "transitions_active_to_active"       => $transitions_active_to_active,
        "transitions_suspended_to_active"    => $transitions_suspended_to_active,
        "transitions_neutral_to_promounused" => $transitions_neutral_to_promounused,
        "transitions_promounusedto_active"   => $transitions_promounusedto_active,
        "transitions_neutral_to_provisioned" => $transitions_neutral_to_provisioned,
        "transitions_neutral_to_active"      => $transitions_neutral_to_active
      ));

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  public function parse_aborted_state_transitions($state_transitions_data)
  {
    $state_transitions = array();

    if ( $state_transitions_data && is_array($state_transitions_data) && count($state_transitions_data) )
    {
      $data_transition = array();

      foreach( $state_transitions_data as $n => $data )
      {
        $data_transition[ $data->TRANSITION_UUID ]['TRANSITION_LABEL'] = $data->TRANSITION_LABEL;
        $data_transition[ $data->TRANSITION_UUID ]['CREATED'] = get_date_from_full_date( $data->CREATED );
        $data_transition[ $data->TRANSITION_UUID ]['CUSTOMER_ID'] = $data->CUSTOMER_ID;

        $state_transition_errors = '';

        if ( isset( $data->ACTION_RESULT ) && ( $data->ACTION_RESULT != '' ) )
        {
          dlog('',"action data = %s",json_encode( $data ) );

          $action_result = json_decode( $data->ACTION_RESULT );

          dlog('',"action result = %s",$action_result);

          if ( $action_result && is_array($action_result) && count($action_result) && ! empty($action_result[0]->errors))
          {
            dlog('', $action_result[0]->errors );

            $state_transition_errors = implode( " ; " , $action_result[0]->errors );

            dlog('', 'state_transition_errors = '.$state_transition_errors );
          }
          elseif( $data->ACTION_RESULT )
          {
          # $data->ACTION_RESULT may not contain well-formed JSON content
            if ( preg_match("/\"errors[^\w]+\[([^\]]+)\]/", $data->ACTION_RESULT, $matches ) )
            {
              dlog('',"action result matches = %s",$matches);

              if ( $matches && is_array($matches) && count($matches) )
                $state_transition_errors = $matches;
            }
          }
        }

        if ( $state_transition_errors )
          $data_transition[ $data->TRANSITION_UUID ]['errors'] = $state_transition_errors;
      }

      foreach( $data_transition as $id => $data )
      {
        dlog('',"data = ".json_encode( $data ));

        $state_transitions[] = $id;
        $state_transitions[] = $data['TRANSITION_LABEL'];
        $state_transitions[] = $data['CREATED'];
        $state_transitions[] = $data['CUSTOMER_ID'];
        $state_transitions[] = ( isset( $data['errors'] ) ) ? $data['errors'] : '' ;
      }
    }

    return $state_transitions;
  }
}

?>
