<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetIntakeData extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetIntakeData
   * Gets all intake records from a JSON plain text file
   * @see MVNO-3191
   * @author bwalters@ultra.me
   */
  public function internal__GetIntakeData()
  {
    try
    {
      // initialize
      $results = $this->getJSONIntakeData();

      $this->addToOutput('results', $results);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

