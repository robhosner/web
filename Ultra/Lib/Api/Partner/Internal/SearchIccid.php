<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class SearchIccid extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__SearchIccid
   * seach for given SIM and return its details; used by internal tools
   * @see MVNO-2932
   * @param string ICCID
   * @author VYT, 2014-12-02
   */
  public function internal__SearchIccid()
  {
    // initialize
    list ($iccid) = $this->getInputValues();
    $iccid = luhnenize($iccid);

    $this->addArrayToOutput(array(
      'iccid_full' => NULL,
      'inventory_dealer_code' => NULL,
      'inventory_dealer_name' => NULL,
      'inventory_distributor_code' => NULL,
      'inventory_distributor_name' => NULL,
      'inventory_master_code' => NULL,
      'inventory_master_name' => NULL,
      'customer_id' => NULL,
      'msisdn' => NULL,
      'plan_state' => NULL,
      'stored_value' => NULL,
      'first_name' => NULL,
      'last_name' => NULL,
      'plan_name' => NULL,
      'gross_add_epoch' => NULL,
      'plan_cost' => NULL,
      'full_cost' => NULL,
      'bolt_ons' => NULL,    
      'activation_epoch' => NULL,
      'activation_dealer_code' => NULL,
      'activation_dealer_name' => NULL,
      'activation_distributor_code' => NULL,
      'activation_distributor_name' => NULL,
      'activation_master_code' => NULL,
      'activation_master_name' => NULL,
      'commissions_paid' => FALSE,
      'override_dealer_code' => NULL,
      'override_dealer_name' => NULL,
      'override_distributor_code' => NULL,
      'override_distributor_name' => NULL,
      'override_master_code' => NULL,
      'override_master_name' => NULL,
      'override_user' => NULL,
      'override_epoch' => NULL,
      'override_notes' => NULL,
      'brand' => NULL
    ));

    try
    {
      // get SIM inventory record
      teldata_change_db();
      if ( ! $sql = htt_inventory_sim_join_ultra_celluphone_channel_query($iccid))
        $this->errException('ERR_API_INTERNAL: an unexpected database error has occurred (1)', 'DB0001');
      $inventory = mssql_fetch_all_objects(logged_mssql_query($sql));
      if (count($inventory) != 1)
        $this->errException('ERR_API_INTERNAL: the given ICCID does not exist in our database', 'DB0001');

      $this->addArrayToOutput(array(
        'iccid_full'                 => $iccid,
        'inventory_dealer_code'      => $inventory[0]->dealer_code,
        'inventory_dealer_name'      => $inventory[0]->dealer_name,
        'inventory_distributor_code' => $inventory[0]->dist_code,
        'inventory_distributor_name' => $inventory[0]->dist_name,
        'inventory_master_code'      => $inventory[0]->master_code,
        'inventory_master_name'      => $inventory[0]->master_name
      ));

      // get subscriber record      
      $customer = get_customer_from_iccid($iccid);
      if ($customer && count($customer) == 1)
      {
        $this->addArrayToOutput(array(
          'customer_id'     => $customer->customer_id,
          'msisdn'          => $customer->current_mobile_number,
          'plan_state'      => $customer->plan_state,
          'stored_value'    => $customer->stored_value * 100,
          'first_name'      => $customer->FIRST_NAME,
          'last_name'       => $customer->LAST_NAME,
          'gross_add_epoch' => date_to_epoch($customer->GROSS_ADD_DATE)
        ));

        // get plan info and costs
        $this->addToOutput('plan_name', get_plan_from_cos_id($customer->COS_ID));
        $plan_cost = get_plan_cost_by_cos_id($customer->COS_ID) * 100;
        $this->addToOutput('plan_cost', $plan_cost);
        $full_cost = $plan_cost;
        $bolt_ons = get_bolt_ons_info_from_customer_options($customer->CUSTOMER_ID);
        if (count($bolt_ons))
        {
          $this->addToOutput('bolt_ons', $bolt_ons);
          foreach ($bolt_ons as $bolt_on)
            $full_cost += $bolt_on['cost'] * 100;
        }
        $this->addToOutput('full_cost', $full_cost);
      }

      // check BRAND_ID from HTT_CUSTOMERS_OVERLAY_ULTRA && HTT_INVENTORY_SIM
      if (! empty($customer->BRAND_ID) && ! empty($inventory[0]->BRAND_ID) && $customer->BRAND_ID == $inventory[0]->BRAND_ID)
        $this->addToOutput('brand', \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID));
      else
        \logWarn('BRAND_ID for customer does not match assigned ICCID');

      // get activation log
      $activation = mssql_fetch_all_objects(logged_mssql_query(htt_activation_log_select_by_iccid_full_query($iccid)));
      if ($activation && count($activation) == 1)
      {
        $this->addToOutput('activation_epoch', $activation[0]->activation_epoch);
        $this->addToOutput('activation_dealer_code', $activation[0]->dealer_code);
        $this->addToOutput('activation_dealer_name', $activation[0]->dealer_name);
        if ($activation[0]->dist_id && $activation[0]->dist_id != $activation[0]->master_id)
        {
          $this->addToOutput('activation_distributor_code', $activation[0]->dist_code);
          $this->addToOutput('activation_distributor_name', $activation[0]->dist_name);
        }
        $this->addToOutput('activation_master_code', $activation[0]->master_code);
        $this->addToOutput('activation_master_name', $activation[0]->master_name);

        // commissions are paid if today is past the 7th of the second month after the activation date
        $payday = new \DateTime();
        $payday->setTimestamp($activation[0]->activation_epoch);
        $payday->add(new \DateInterval('P2M'));
        $today = new \DateTime('now');
        if (($today->format('Y') == $payday->format('Y') && $today->format('n') == $payday->format('n') && $today->format('j') > 7)
          || ($today->format('Y') == $payday->format('Y') && $today->format('n') > $payday->format('n'))
          || ($today->format('Y') > $payday->format('Y')))
          $this->addToOutput('commissions_paid', TRUE);
      }

      // get override record
      $override = get_activation_log_override($iccid);
      if ($override && count($override) == 1)
      {
        $this->addArrayToOutput(array(
          'override_dealer_code'      => $override[0]->new_dealer_code,
          'override_dealer_name'      => $override[0]->new_dealer_name,
          'override_distributor_code' => $override[0]->new_distributor_code,
          'override_distributor_name' => $override[0]->new_distributor_name,
          'override_master_code'      => $override[0]->new_masteragent_code,
          'override_master_name'      => $override[0]->new_masteragent_name,
          'override_user'             => $override[0]->CREATED_BY,
          'override_epoch'            => date_to_epoch($override[0]->CREATED_DATE_TIME),
          'override_notes'            => $override[0]->NOTES
        ));
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
