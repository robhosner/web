<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetActCodeFromICCID extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetActCodeFromICCID
   *
   * @param  string iccid
   * @return object Result
   */
  public function internal__GetActCodeFromICCID()
  {
    list ($iccid) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $sim = get_htt_inventory_sim_from_iccid($iccid);
      if (is_null($sim))
        $this->errException('ERR_API_INTERNAL: ICCID not found.', 'IC0001');

      $this->addToOutput('actcode', $sim->ACT_CODE);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
