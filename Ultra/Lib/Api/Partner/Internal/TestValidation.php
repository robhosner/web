<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class TestValidation extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__TestValidation
   *
   * Test Ultra API Validation.
   *
   * @return Result object
   */
  public function internal__TestValidation ()
  {
    $this->succeed ();

    $this->result->data_array['inputs'] = $this->getInputValues();

    return $this->result;
  }
}

?>
