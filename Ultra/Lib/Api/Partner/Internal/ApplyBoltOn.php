<?php

namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Accounts\Interfaces\AccountsRepository;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ApplyBoltOn extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ApplyBoltOn
   *
   * Adds an immediate bolt on to a customer without charge
   *
   * @return Result object
   */
  public function internal__ApplyBoltOn()
  {
    list ($customer_id, $bolt_on_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      // get customer info
      $account  = get_account_from_customer_id( $customer_id, array('CUSTOMER_ID','COS_ID','BALANCE') );
      $customer = get_ultra_customer_from_customer_id( $customer_id, array('BRAND_ID','current_mobile_number') );
      $customer = (object)array_merge((array)$account, (array)$customer);

      if (empty($customer))
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // validate that the customer is Active
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);
      if ($state['state'] != STATE_ACTIVE)
        $this->errException('Customer state is not Active.', 'IN0001');

      // validate bolt on
      if ( !\Ultra\Lib\BoltOn\validateBoltOn($account->COS_ID, $bolt_on_id, 'immediate') )
        $this->errException('Unable to add bolt on', 'VV0105', 'Invalid brand or plan');
      
      // add bolt on with 0 cost
      $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo($bolt_on_id);
      $boltOnInfo['cost'] = 0;

      list( $error , $error_code, $user_error ) = \Ultra\Lib\BoltOn\addBoltOnImmediate($customer_id, $boltOnInfo, 'SPEND', __FUNCTION__, NULL);
      if ( $error )
        $this->errException( $error , $error_code, !empty($user_error) ? $user_error : $error );

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
