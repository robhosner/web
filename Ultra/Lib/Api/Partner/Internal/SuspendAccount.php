<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class SuspendAccount extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__SuspendAccount
   *
   * Suspend an Ultra customer with a state transition to 'Suspended'
   * 
   * @param int customer_id
   * @return object Result
   */
  public function internal__SuspendAccount()
  {
    list ($customer_id, $reason) = $this->getInputValues();

    try
    {
      teldata_change_db();

      if (checkApiAbuse(__FUNCTION__, 10))
        $this->errException('ERR_API_INTERNAL: command disabled; please try again later', 'AP0001');

      $customer = get_customer_from_customer_id($customer_id);

      if ($customer)
      {
        if (in_array($customer->BRAND_ID, [3]))
          $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

        if ($customer->plan_state == STATE_ACTIVE)
        {
          $result = suspend_account($customer, array('customer_id' => $customer->customer_id));
          if (count($result['errors']))
            $this->errException($result['errors'][0], 'IN0002');
        }
        else
          $this->errException('ERR_API_INTERNAL: Invalid customer state for this command', 'IN0001');
      }
      else
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

