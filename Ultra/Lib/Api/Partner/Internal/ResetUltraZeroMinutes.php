<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ResetUltraZeroMinutes extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ResetUltraZeroMinutes
   *
   * resets zero_minutes to 0
   * invokes func_reset_zero_minutes
   *
   * @return Result object
   */
  public function internal__ResetUltraZeroMinutes()
  {
    // initialize
    list ($customer_id) = $this->getInputValues();
   
    try
    {
      teldata_change_db();
    
      $account = get_account_from_customer_id($customer_id, array('COS_ID'));
    
      if (!$account)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer_id', 'NS0001');
      
      $result = func_reset_zero_minutes($customer_id, $account->COS_ID);
      
      if (count($result['errors']) > 0)
      {
        $this->errException('ERR_API_INTERNAL: ' . $result['errors'][0], 'DB0001');
      }
      
      if (!$result['success'])
      {
        $this->errException('ERR_API_INTERNAL: Failed resetting zero_minutes', 'DB0001');
      }
      
      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }
    
    return $this->result;
  }
}

?>
