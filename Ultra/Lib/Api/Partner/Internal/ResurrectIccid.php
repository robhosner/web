<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ResurrectIccid extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ResurrectIccid
   *
   * Unblocks an inactive ICCID so that it can be used for subsequent activations.
   *
   * @return Result object
   */
  public function internal__ResurrectIccid ()
  {
    list ( $iccid, $msisdn ) = $this->getInputValues();

    $error_code = '';

    try
    {
      teldata_change_db();

      $result = resurrect_iccid( $iccid, $msisdn );

      if ( $result->is_failure() )
      {
        $error_code = 'IN0001';
        throw new \Exception("Error - ".implode(' ; ',$result->get_errors()));
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>
