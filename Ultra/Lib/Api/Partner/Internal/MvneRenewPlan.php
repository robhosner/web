<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class MvneRenewPlan extends \Ultra\Lib\Api\Partner\Internal
{
 /**
   * internal__MvneRenewPlan
   * renew subsriber's current plan on MVNE
   * @see PROD-1500
   * @return Result object
   * @author: VYT, 2015-05-18
   */
  public function internal__MvneRenewPlan()
  {
    // initialize
    list ($msisdn, $iccid) = $this->getInputValues();

    try
    {
      dlog('', 'authenticated user: %s', empty($_SERVER['PHP_AUTH_USER']) ? 'NULL' : $_SERVER['PHP_AUTH_USER']);

      // either ICCID or MSISDN is required
      if (empty($msisdn) && empty($iccid))
        $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing', 'MP0001');

      // get subsriber info
      \teldata_change_db();
      $clause = empty($msisdn) ? array('CURRENT_ICCID_FULL' => \luhnenize($iccid)) : array('CURRENT_MOBILE_NUMBER' => \normalize_msisdn($msisdn));
      $attributes = array('CUSTOMER_ID', 'PLAN_STATE', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'BRAND_ID');
      $query = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 2, $attributes, $clause, NULL, NULL, TRUE);
      $customers = \mssql_fetch_all_objects(logged_mssql_query($query));
      if (count($customers) != 1)
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');

      // verify subscriber info
      $customer = $customers[0];

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      if ($customer->PLAN_STATE != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
      if (empty($customer->CURRENT_MOBILE_NUMBER) || empty($customer->CURRENT_ICCID_FULL))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001');

      // get COS_ID and Ultra Plan
      if ( ! $cos_id =  \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer->CUSTOMER_ID, 'COS_ID'))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105', 'Invalid COS_ID');
      if ( ! $plan = \get_monthly_plan_from_cos_id($cos_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105', 'Invalid Ultra Plan');

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $mwControl->mwRenewPlan(
        array(
          'actionUUID'         => $this->getRequestId(),
          'customer_id'        => $customer->CUSTOMER_ID,
          'msisdn'             => $customer->CURRENT_MOBILE_NUMBER,
          'iccid'              => $customer->CURRENT_ICCID_FULL,
          'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
          'ultra_plan'         => $plan));

      // check MW results
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');
      if ( ! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100')
        $this->errException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');
      if ( ! $result->data_array['success'] || empty($result->data_array['body']))
        $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');
      $this->succeed();
    }
    catch(\Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>