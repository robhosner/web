<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'db.php';
require_once 'Ultra/Lib/StateMachine.php';
#require_once 'Primo/Lib/StateMachine/DataEngineMongoDB.php';
require_once 'Ultra/Lib/StateMachine/DataEngineMSSQL.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRAM.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRedis.php';

# Ultra specific actions and requirements
require_once 'Ultra/Lib/StateMachine/Action/functions.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RecoverStuckTransition extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RecoverStuckTransition
   *
   * Recovers a Stuck transition by transition_uuid
   *
   * @param string transition_uuid
   * @return Result object
   */
  public function internal__RecoverStuckTransition()
  {
    list ($transition_uuids) = $this->getInputValues();

    $failed  = [];
    $aborted = [];
    $closed  = [];

    try
    {
      teldata_change_db();

      $redis = new \Ultra\Lib\Util\Redis();
      $dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;

      $sm = new \Ultra\Lib\StateMachine( $dataEngineMSSQLObject , NULL , NULL , $redis );


      $transition_uuids = explode(',', $transition_uuids);
      foreach ($transition_uuids as $transition_uuid)
      {
        $sm->loadTransitionData($transition_uuid);
        $transition = $sm->getCurrentTransitionData();

        if ( ! $transition)
        {
          $failed[] = "Transition $transition_uuid does not exist";
          continue;
        }

        if ($transition['status'] != 'OPEN')
        {
          $failed[] = "Transition $transition_uuid is not OPEN";
          continue;
        }

        $customer = (array)get_customer_from_customer_id($transition['customer_id']);
        $sm->setCustomerData($customer);

        // set transition configuration
        $result = $sm->selectTransitionByLabel(
          $transition['transition_label'],
          $customer['plan_state'],
          get_plan_name_from_short_name(get_plan_from_cos_id($transition['from_cos_id']))
        );

        if ( ! $result )
        {
          $sql = "UPDATE HTT_TRANSITION_LOG SET STATUS = 'ABORTED' WHERE TRANSITION_UUID = '$transition_uuid'";
          logged_mssql_query($sql);
          $aborted[] = $transition_uuid;
          continue;
        }

        $sm->generateActionData();

        if ( ! $sm->runTransition($reserve = FALSE) )
          $failed[] = "Transition $transition_uuid recover failed";
        else
          $closed[] = $transition_uuid;
      }

      $this->addToOutput('failed',  $failed);
      $this->addToOutput('aborted', $aborted);
      $this->addToOutput('closed',  $closed);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
