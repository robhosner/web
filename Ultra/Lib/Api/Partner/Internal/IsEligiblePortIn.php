<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class IsEligiblePortIn extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__IsEligiblePortIn
   *
   * Gets all actions belonging to a transition
   *
   * @param string customer_id
   * @param string msisdn
   * @return Result object
   */
  public function internal__IsEligiblePortIn()
  {
    list($customer_id, $msisdn) = $this->getInputValues();

    try
    {
      if ( $customer_id != -1 )
      {
        if ( is_numeric( $customer_id ) )
        {
          $customer = find_customer( make_find_ultra_customer_query_from_customer_id($customer_id) );

          if ( ! $customer )
            $this->errException('ERR_API_INVALID_ARGUMENTS: customer_id should be -1 or the identifier of an existing customer', 'NS0002');
        }
        else
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer_id should be -1 or the numeric identifier of an existing customer', 'NS0002');
      }

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $mwResult = $mwControl->mwPortInEligibility(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn'     => $msisdn
        ),
        TRUE
      );

      if ($errors = $mwResult->get_errors())
          $this->errException($errors[0], 'MW0001');

      $eligible = ($mwResult->is_success() && $mwResult->get_data_key('eligible')) ? 1 : 0;

      $this->addToOutput('eligible', $eligible);
      $this->addToOutput('msisdn', $msisdn);

      $this->addWarnings($mwResult->get_warnings());

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
