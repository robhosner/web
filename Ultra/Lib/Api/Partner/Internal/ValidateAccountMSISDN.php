<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ValidateAccountMSISDN extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ValidateAccountMSISDN
   *
   * Validates that ACCOUNTS.ACCOUNT is associated with HTT_CUSTOMERS_OVERLAY_ULTRA.current_mobile_number.
   * Validates plan_state as well : Active or Suspended.
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MW+:+HandlePortOutRequest
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/ACC%3A+QueryStatus
   * @return Result object
   */
  public function internal__ValidateAccountMSISDN()
  {
    list ($account, $msisdn) = $this->getInputValues();
    $accountValid = TRUE;
    $pinValid = TRUE;

    try
    {
      // get customer by MSISDN
      $msisdn = normalize_msisdn($msisdn);
      teldata_change_db();
      if ($customer = get_customer_from_msisdn($msisdn, 'a.ACCOUNT, u.plan_state'))
      {
        // validate account
        if ($customer->ACCOUNT != $account)
          $accountValid = FALSE;

        // validate PIN
        if (in_array($customer->plan_state, array(STATE_PROVISIONED, STATE_PROMO_UNUSED)))
          $pinValid = FALSE;
      }

      // according to specs the API always succeeds, even if customer was not found
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('IsPINValid', $pinValid);
    $this->addToOutput('IsAccountValid',$accountValid);

    return $this->result;
  }
}

