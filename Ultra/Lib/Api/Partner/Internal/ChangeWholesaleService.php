<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ChangeWholesaleService extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ChangeWholesaleService
   *
   * Updates the wholesale customer's plan.
   *
   * @return Result object
   */
  public function internal__ChangeWholesaleService ()
  {
    list ( $customer_id , $msisdn, $price_plan ) = $this->getInputValues();

    $error_code = '';
    $this->addToOutput('customer_id', NULL);
    $this->addToOutput('MSISDN', NULL);
    $this->addToOutput('ICCID', NULL);

    try
    {
      dlog('', 'authenticated user: %s', empty($_SERVER['PHP_AUTH_USER']) ? 'NULL' : $_SERVER['PHP_AUTH_USER']);

      teldata_change_db();

      $customer = null;

      $select_fields = array('CUSTOMER_ID', 'current_mobile_number', 'CURRENT_ICCID_FULL', 'current_iccid', 'plan_state', 'MVNE', 'BRAND_ID');

      if ( ! empty($customer_id))
        $customer = get_ultra_customer_from_customer_id($customer_id, $select_fields);
      elseif ( ! empty($msisdn))
      {
        $customers = get_ultra_customers_from_msisdn(normalize_msisdn($msisdn, FALSE), $select_fields);
        if (count($customers)) $customer = $customers[0];
      }
      else
        $this->errException('ERR_API_INVALID_ARGUMENTS: either customer ID or MSISDN is required', 'IN0002');

      if ( ! $customer )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
      $this->addToOutput('MSISDN', $customer->current_mobile_number);
      $this->addToOutput('ICCID', empty($customer->CURRENT_ICCID_FULL) ? $customer->current_iccid : $customer->CURRENT_ICCID_FULL);

      // PROD-1337: confirm that subscriber does not have any pending MISO
      if (! $connection = \Ultra\Lib\DB\ultra_acc_connect())
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $open_miso = get_makeitso(array('CUSTOMER_ID' => $customer->CUSTOMER_ID, 'STATUS' => 'PENDING'), 1, 'MAKEITSO_QUEUE_ID');
      if (count($open_miso))
        $this->errException("ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command", 'IN0001', "Subscriber {$customer->CUSTOMER_ID} has pending MISO {$open_miso[0]->MAKEITSO_QUEUE_ID}");
      teldata_change_db();

      if ($customer->plan_state != 'Active')
        $this->errException("ERR_API_INVALID_ARGUMENTS: customer state is {$customer->plan_state}", 'IN0001');

      dlog('', "user {$_SERVER['PHP_AUTH_USER']} updating subscriber {$customer->CUSTOMER_ID} to plan $price_plan on MVNE {$customer->MVNE}");

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwUpdateWholesalePlan([
        'actionUUID'     => getNewActionUUID('internal ' . time()),
        'msisdn'         => $customer->current_mobile_number,
        'wholesale_plan' => \Ultra\UltraConfig\getWholesalePlanNameFromId( $price_plan ),
        'customer_id'    => $customer->CUSTOMER_ID
      ]);

        if ($result->is_failure() || empty($result->data_array['success']))
        {
          dlog('', 'MW result: %s', $result);

          // PROD-371: do not fail if plan is already set on MVNE: error 288 is 'The given Wholesale Plan is the same...'
          if (! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] == 288)
            $this->addWarning('MVNE plan is already set');
          else
          {
            if (empty($result->data_array['ResultMsg']))
              $error = 'ERR_API_INTERNAL: failed to update subscriber, MW error ' . $result->data_array['ResultCode'];
            else
              $error = 'ERR_API_INTERNAL: ' . $result->data_array['ResultMsg'];
            $this->errException($error, 'MW0001');
          }
        }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>
