<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class UpdatePromoBroadcast extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__UpdatePromoBroadcast
   * update status of the given broadcast SMS campaign:
   *   if status is given then update it
   *   if data_chunk is given then append it to the existing CSV fle (if chunk_index = 0 then over-write it)
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+Framework
   * @return Result object
   * @author: VYT, 2014-09-22
   */
  public function internal__UpdatePromoBroadcast()
  {
    // initialize
    list ($campaign_id, $status, $data_chunk, $chunk_size, $chunk_index) = $this->getInputValues();

    try
    {
      // verify that this campaign exists
      teldata_change_db();
      $result = get_ultra_promo_broadcast_campaign(array('promo_broadcast_campaign_id' => $campaign_id));
      if ($result->is_failure() || count($result->data_array) != 1)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid campaign', 'ND0001');
      if ($result->data_array[0]->STATUS == $status)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid status', 'VV0036', "campaign id $campaign_id already has status $status");

      if ( $status == 'READY' )
      {
        // initialize campaign
        $broadcast = new \PromoBroadcast();

        $result = $broadcast->loadCampaign(
          array( 'promo_broadcast_campaign_id' => $campaign_id )
        );

        if ( $result->is_failure() )
        {
          dlog('',"errors = %s",$result->get_errors());

          $this->errException('ERR_API_INTERNAL: failed to load campaign', 'DB0001');
        }

        $result = $broadcast->initializeCampaign();

        if ( $result->is_failure() )
        {
          dlog('',"errors = %s",$result->get_errors());

          $this->errException('ERR_API_INTERNAL: failed to initialize campaign', 'DB0001');
        }
      }
      elseif ($status)
      {
        // update campaign status
        $result = update_ultra_promo_broadcast_campaign(
          array(
            'promo_broadcast_campaign_id' => $campaign_id,
            'status'                      => $status
          )
        );

        if ($result->is_failure())
          $this->errException('ERR_API_INTERNAL: failed to update campaign', 'DB0001');
      }
      elseif ($data_chunk && $chunk_size)
      {
        // check data chunk: we may receive an extra new lines for each CR (FormData appends it), so have to ignore possible size increase
        if ($chunk_size > strlen($data_chunk))
          $this->errException('API_ERR_PARAMETER: corrupt data file, please retry', 'VV0033');

        // save data chunk
        $file = null;

        $destination = PROMO_BROADCAST_WORK_DIRECTORY . $campaign_id;
        $name = 'uploaded_data.csv';

        if ($chunk_index == 0)  // create/overwrite file
        {
          // create new file and path if missing and save data chunk into it
          if ( ! file_exists($destination))
          {
            $oldmask = umask(0);
            if ( ! mkdir($destination, 0776, TRUE))
              $this->errException('ERR_API_INTERNAL: failed to create destination directory', 'ND0001');
            umask($oldmask);
          }
          if ( ! $file = fopen("$destination/$name", 'w'))
              $this->errException('ERR_API_INTERNAL: failed to create destination file', 'ND0001');
          $count = fwrite($file, $data_chunk, $chunk_size);
          if ($count != $chunk_size)
            $this->errException('ERR_API_INTERNAL: failed to save data to file', 'ND0001');

          fclose($file);
        }
        else // append to existing file
        {
          // verify that destination file already exists and append data chunk to it
          if ( ! file_exists("$destination/$name"))
            $this->errException('ERR_API_INTERNAL: data chunk out of order', 'ND0001');
          if ( ! $file = fopen("$destination/$name", 'a'))
            $this->errException('ERR_API_INTERNAL: failed to open destination file', 'ND0001');
          $count = fwrite($file, $data_chunk, $chunk_size);
          if ($count != $chunk_size)
            $this->errException('ERR_API_INTERNAL: failed to append data to file', 'ND0001');

          fclose($file);
        }

        $filename = "$destination/$name";
        if (file_exists($filename))
        {
          $line_count = count(file($filename));

          if ( ! $line_count )
          {
            // update campaign status
            $result = update_ultra_promo_broadcast_campaign(
              array(
                'promo_broadcast_campaign_id' => $campaign_id,
                'status'                      => 'ERROR'
              )
            );

            if ($result->is_failure())
              $this->errException('ERR_API_INTERNAL: failed to update campaign status', 'DB0001');
          }
          else
          {
            // update campaign volume count
            $result = update_ultra_promo_broadcast_campaign(
              array(
                'promo_broadcast_campaign_id' => $campaign_id,
                'volume_count'                => $line_count
              )
            );

            if ($result->is_failure())
              $this->errException('ERR_API_INTERNAL: failed to update campaign volume count', 'DB0001');
          }
        }
      }
      else
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing parameters', 'VV0036', 'one or more required parameters are missing');

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>
