<?php

namespace Ultra\Lib\Api\Partner;

use Ultra\Activations\Interfaces\ActivationHistoryRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Dealers\Interfaces\DealersRepository;
use Ultra\Payments\Payment;
use Ultra\Sims\Interfaces\SimRepository;

if (!getenv("UNIT_TESTING"))
{
  require_once 'Ultra/Lib/Api/PartnerBase.php';
  require_once 'Ultra/Lib/Util/Redis/Billing.php';
  require_once 'Ultra/Lib/Billing/functions.php';
}

/**
 * External Payments partner class
 * APIs for external payment processors
 * @author VYT, 2015
 * @project T-Cetra integartion
 * @see https://issues.hometowntelecom.com:8443/browse/TCET-1
 */
class ExternalPayments extends \Ultra\Lib\Api\PartnerBase
{
  protected $activationHistoryRepo;
  protected $dealersRepo;
  protected $customerRepo;
  protected $simRepo;

  // constants
  const MAX_EPOCH_DRIFT = 180; // 3 min

  /**
   * @var Payment
   */
  private $payment;

  /**
   * @var Configuration
   */
  private $configuration;

  public function __construct(
    ActivationHistoryRepository $activationHistoryRepo,
    CustomerRepository $customerRepo,
    DealersRepository $dealersRepo,
    SimRepository $simRepo,
    Payment $payment,
    Configuration $configuration
  )
  {
    $this->activationHistoryRepo = $activationHistoryRepo;
    $this->dealersRepo           = $dealersRepo;
    $this->customerRepo          = $customerRepo;
    $this->simRepo               = $simRepo;
    $this->payment = $payment;
    $this->configuration = $configuration;
  }
  
  /**
   * checkTimeSync
   *
   * enforce max time drift between systems since these are financial transactions
   *
   * @throws Exception
   */
  protected function checkTimeSync($request_epoch)
  {
    if (abs(time() - $request_epoch) > self::MAX_EPOCH_DRIFT)
    {
      dlog('', 'time difference between local and remote systems: %d sec', abs(time() - $request_epoch));
      $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid UNIX Epoch value', 'VV0002');
    }
  }


  /**
   * verifySubscriber
   *
   * get subscriber object by MSISDN and verify its state
   * @return Object customer
   * @throws Exception
   */
  protected function verifySubscriber($msisdn)
  {
    // get subscriber and verify MSISDN
    if ( ! $subscriber = $this->customerRepo->getCustomerFromMsisdn($msisdn))
      $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');

    // verify state
    if ( ! in_array($subscriber->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED, STATE_PROVISIONED, STATE_PORT_IN_REQUESTED)))
      $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

    return $subscriber;
  }


  /**
   * verifyDealer
   *
   * retrieve dealer info by dealer code
   * @param String dealer code
   * @param Boolean TRUE to ensure dealer is active, FALSE skip check
   * @return Object dealer
   * @throws Exception
   */
  protected function verifyDealer($code, $checkActive = TRUE)
  {
    if ( ! $dealer = $this->dealersRepo->getDealerFromCode($code))
      $this->errException('ERR_API_INVALID_ARGUMENTS: No celluphone dealer info found', 'ND0003');

    if ($checkActive && ! $dealer->ActiveFlag)
      $this->errException('ERR_API_INVALID_ARGUMENTS: Dealer is not active', 'VV0004');

    return $dealer;
  }


  /**
   * verifyProduct
   *
   * logic for checking the integrity of product_id, subproduct_id and load_amount
   * @return String subproduct
   * @throws Exception
   */
  protected function checkProduct($subscriber, $product, $subproduct, $amount)
  {
    // check max account balance
    if ($this->payment->checkExcessBalance($subscriber, $amount))
      $this->errException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet', 'CH0001');

    // if applying to MONTHLY then load amount must cover plan cost (with bolt ons or without) 
    if ($product == 'ULTRA_PLAN_RECHARGE')
    {
      // subproduct is required
      if (empty($subproduct))
        $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing', 'MP0001', 'Missing required subproduct_id');

      $subproduct_numeric = preg_replace("/[^0-9,.]/", "", $subproduct);

      // subproduct must match amount
      if ($subproduct_numeric * 100 != $amount)
      {
        dlog('', 'subproduct amount %s does not match charge amount %s', $subproduct_numeric * 100, $amount);
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid product or subproduct', 'VV0005');
      }

      // verify that amount covers minium recharge amount without bolt ons
      $cost = $this->configuration->getPlanCostByCosId(
        $subscriber->MONTHLY_RENEWAL_TARGET ?
          $this->configuration->getCosIdFromPlan($subscriber->MONTHLY_RENEWAL_TARGET) :
          $subscriber->COS_ID
      );
      if ($amount < ($cost - $subscriber->stored_value - $subscriber->BALANCE) * 100)
      {
        dlog('', 'insufficient recharge amount: load $%.2f < (cost $%.2f - stored $%.2f - balance $%.2f)', $amount / 100, $cost, $subscriber->stored_value, $subscriber->BALANCE); 
        $this->errException('ERR_API_INVALID_ARGUMENTS: The given plan does not match the given amount', 'VV0089');
      }

      // convert subproduct from L19-L59 to PQ19-PQ59 if activating
      if (in_array($subscriber->plan_state, array(STATE_NEUTRAL, STATE_PORT_IN_REQUESTED, STATE_PROVISIONED)))
        $subproduct = 'PQ' . $subproduct_numeric;
    }

    return $subproduct;
  }


  /**
   * cancelTransaction
   *
   * update OPEN transaction in HTT_BILLING_ACTIONS in order to prevent it from processing and return it
   * @return Object transaction
   * @throws Exception
   */
  protected function cancelTransaction($subscriber, $provider_trans_id, $load_amount, $cancel_type)
  {
    // update pending transaction
    $query = \Ultra\Lib\DB\makeUpdateQuery(
      'HTT_BILLING_ACTIONS',
      array('STATUS' => $cancel_type),
      array(
        'STATUS'          => 'OPEN',
        'CUSTOMER_ID'     => $subscriber->CUSTOMER_ID,
        'TRANSACTION_ID'  => $provider_trans_id,
        'AMOUNT'          => $load_amount));
    if ( ! is_mssql_successful(logged_mssql_query($query)))
      $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001');

    // check if transaction exists and was updated
    $query = \Ultra\Lib\DB\makeSelectQuery(
      'HTT_BILLING_ACTIONS',
      1,
      array('STATUS', 'UUID'),
      array(
        'CUSTOMER_ID'     => $subscriber->CUSTOMER_ID,
        'TRANSACTION_ID'  => $provider_trans_id,
        'AMOUNT'          => $load_amount));
    $transaction = mssql_fetch_all_objects(logged_mssql_query($query));
    if ( ! is_array($transaction) || ! count($transaction))
      $this->errException('ERR_API_INVALID_ARGUMENTS: Transaction does not exist', 'VV0003');

    if ($transaction[0]->STATUS != $cancel_type)
      $this->errException('ERR_API_INTERNAL: Cannot void transaction', 'DB0002');

    return $transaction[0];
  }
}

