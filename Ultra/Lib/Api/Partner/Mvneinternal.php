<?php

namespace Ultra\Lib\Api\Partner;

require_once 'classes/IntraBrand.php';

/**
 * Mvneinternal Partner class
 *
 * See PROD-123
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class Mvneinternal extends \Ultra\Lib\Api\PartnerBase
{

  /**
   * mvneinternal__UpdatePortIn
   *
   * Invokes UpdatePortIn on the network
   *
   * @return Result object
   */
  public function mvneinternal__UpdatePortIn ()
  {
    list ( $msisdn , $port_account_number , $port_account_password , $zipcode,
           $first_name, $last_name, $address_1, $address_2, $city, $state
      ) = $this->getInputValues();

    try
    {
      $msisdn = normalize_msisdn_10( $msisdn );

      teldata_change_db();

      // check for intra-brand port
      $customer = get_customer_from_msisdn($msisdn, 'u.CUSTOMER_ID');
      if ($customer && \IntraBrand::isIntraBrandPortByReferenceSource($customer->CUSTOMER_ID))
      {
        // then is intra-brand port
        $this->errException('ERR_API_INTERNAL: intra-port requests cannot be updated', 'VV0065');
      }

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $input = array(
        'actionUUID'         => $this->getRequestId(),
        'msisdn'             => $msisdn,
        'port_account'       => $port_account_number,
        'port_password'      => $port_account_password,
        'zipcode'            => $zipcode
      );

      if ( ! empty($first_name)) $input['first_name'] = $first_name;
      if ( ! empty($last_name))  $input['last_name']  = $last_name;
      if ( ! empty($address_1))  $input['address_1']  = $address_1;
      if ( ! empty($address_2))  $input['address_2']  = $address_2;
      if ( ! empty($city))       $input['city']       = $city;
      if ( ! empty($state))      $input['state']      = $state;

      $mwResult = $mwControl->mwUpdatePortIn($input);

      // connect back to the default DB
      teldata_change_db();

      if ( $mwResult->is_failure() )
      {
        $errors = $mwResult->get_errors();

        $this->errException('ERR_API_INTERNAL: MW error - '.$errors[0], 'MW0001');
      }
      else
      {
        dlog('', "result data = %s",$mwResult->data_array);

        if ( ! $mwResult->data_array['success'] )
        {
          if ( ( ! empty($mwResult->data_array['errors']) ) && is_array( $mwResult->data_array['errors'] ) && count( $mwResult->data_array['errors'] ) )
            $this->errException('ERR_API_INTERNAL: MW error - '.$mwResult->data_array['errors'][0], 'MW0001');
          else
            $this->errException('ERR_API_INTERNAL: MW error'                                      , 'MW0001');
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }

  /**
   * mvneinternal__AddVoiceMinutes
   *
   * add subscriber voice minutes
   * @see MVNO-2427
   * @return Result object
   */
  public function mvneinternal__AddVoiceMinutes()
  {
    // init
    list ($iccid, $msisdn, $voice_minutes, $reason, $user) = $this->getInputValues();
    if (empty($user))
      $user = $_SERVER['REMOTE_USER'];

    try
    {
      // verify parameters
      if ( ! $iccid && ! $msisdn)
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN', 'NS0001');

      // get customer and fill in missing parameters
      teldata_change_db();
      if ($iccid)
      {
        $iccid = luhnenize($iccid);
        $customer = get_customer_from_iccid($iccid);
        if ( ! $customer)
          $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
        if ( ! $customer->current_mobile_number)
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no phone number', 'MV0001');
        $msisdn = $customer->current_mobile_number;
      }
      else // $msisdn
      {
        $msisdn = normalize_msisdn($msisdn, FALSE);
        $customer = get_customer_from_msisdn($msisdn);
        if ( ! $customer)
          $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
        if ( ! $customer->current_iccid && ! $customer->CURRENT_ICCID_FULL)
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no SIM card', 'IC0001');
        $iccid = $customer->CURRENT_ICCID_FULL ? $customer->CURRENT_ICCID_FULL : luhnenize($customer->current_iccid);
      }

      // verify state
      if ($customer->plan_state != 'Active')
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer state', 'IN0001');

      // verify MVNE
      if ($customer->MVNE != parent::MVNE2)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer MVNE', 'VV0016');

      // call MW
      dlog('', "INFO: sub: {$customer->CUSTOMER_ID}, iccid: $iccid, msisdn: $msisdn, voice: $voice_minutes, reason: $reason, user: $user");
      $middleware = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $middleware->mwMakeitsoUpgradePlan(array(
        'actionUUID'          => $this->getRequestId(),
        'msisdn'              => $msisdn,
        'iccid'               => $iccid,
        'customer_id'         => $customer->CUSTOMER_ID,
        'wholesale_plan'      => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
        'ultra_plan'          => get_plan_from_cos_id($customer->COS_ID),
        'preferred_language'  => $customer->preferred_language,
        'option'              => "B-VOICE|$voice_minutes"));

      teldata_change_db();

      // check result
      if ($result->is_failure() )
        $this->errException('ERR_API_INTERNAL: failed to add voice minites - ' . $result->get_errors(), 'MW0001');

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }


  /**
   * mvneinternal__NetworkRenewPlanRaw
   *
   * Invokes RenewPlan without involving SOCs.
   * $addOnFeatureList is ignored if $voice_minutes or $sms_messages are provided.
   *
   * @return Result object
   */
  public function mvneinternal__NetworkRenewPlanRaw ()
  {
    list ( $customer_id , $addOnFeatureList , $voice_minutes , $sms_messages ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id( $customer_id, array('current_mobile_number', 'CURRENT_ICCID_FULL') );
      $account = get_account_from_customer_id( $customer_id, array('COS_ID') );

      if ( ! $customer )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: invalid customer_id' , 'NS0001' );

      if ( ! $customer->current_mobile_number )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer has no MSISDN' , 'NS0001' );

      if ( ! $customer->CURRENT_ICCID_FULL )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer has no ICCID' , 'NS0001' );

      if( ! $account->COS_ID )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: account has no COS_ID' , 'NS0001' );
      else
        $customer->COS_ID = $account->COS_ID;

      // validate $voice_minutes according to customer's plan

      $planName = get_monthly_plan_from_cos_id( $customer->COS_ID );

      $unlimited_intl_minutes = \Ultra\UltraConfig\getUltraPlanConfigurationItem(get_plan_name_from_short_name($planName),'unlimited_intl_minutes');

      dlog('',"planName = $planName ; unlimited_intl_minutes = $unlimited_intl_minutes");

      if ( $voice_minutes > $unlimited_intl_minutes )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: voice_minutes value is too big' , 'NS0001' );

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      #$addOnFeatureList = '[{"FeatureID":12808,"FeatureValue":"","Action":"RESET","autoRenew":"false"},{"FeatureID":12701,"FeatureValue":"8250","Action":"RESET","autoRenew":"false"}]';

      $addOnFeatureListRaw = NULL;

      dlog('',"addOnFeatureList = %s",$addOnFeatureList);

      if ( ( !is_null($voice_minutes) && ( $voice_minutes != '' ) )
        || ( !is_null($sms_messages)  && ( $sms_messages  != '' ) )
      )
      {
        // $addOnFeatureList is ignored if $voice_minutes or $sms_messages are provided

        // 0 is a valid value

        $addOnFeature = array();

        if ( !is_null($voice_minutes) && ( $voice_minutes != '' ) )
          // 12701 Base Voice Minutes B-VOICE
          $addOnFeature[] = array(
            'FeatureID'    => '12701',
            'FeatureValue' => $voice_minutes,
            'Action'       => 'RESET',
            'autoRenew'    => 'false'
          );

        if ( !is_null($sms_messages) && ( $sms_messages != '' ) )
          // 12702 Base SMS Messages B-SMS
          $addOnFeature[] = array(
            'FeatureID'    => '12702',
            'FeatureValue' => $sms_messages,
            'Action'       => 'RESET',
            'autoRenew'    => 'false'
          );

        $addOnFeatureListRaw = array(
          'AddOnFeature' => $addOnFeature
        );
      }
      elseif ( $addOnFeatureList )
        $addOnFeatureListRaw = array(
          'AddOnFeature' => json_decode( $addOnFeatureList )
        );

      dlog('',"addOnFeatureListRaw = %s",$addOnFeatureListRaw);

      $result = $mwControl->mwRenewPlanRaw(
        array(
          'actionUUID'          => $this->getRequestId(),
          'msisdn'              => $customer->current_mobile_number,
          'iccid'               => $customer->CURRENT_ICCID_FULL,
          'wholesale_plan'      => \Ultra\Lib\DB\Customer\getWholesalePlan($customer_id),
          'ultra_plan'          => get_plan_from_cos_id($customer->COS_ID),
          'customer_id'         => $customer_id,
          'addOnFeatureListRaw' => $addOnFeatureListRaw
        )
      );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $this->errException( $errors[0] , 'MW0001' );
      }

      dlog('', "mwRenewPlanRaw result data = %s",$result->data_array);

      if ( ! $result->data_array['success'] )
        $this->errException( $result->data_array['errors'][0] , 'MW0001' );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }


  /**
   * mvneinternal__MakeItSoRenewPlan
   *
   * Initiates a MakeItSo RenewPlan
   *
   * @return Result object
   */
  public function mvneinternal__MakeItSoRenewPlan ()
  {
    list ( $customer_id ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id( $customer_id, array('BRAND_ID') );
      if (!$customer)
        $this->errException('no customer found', 'VV0031', 'no customer found');

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      $plan_data = get_customer_plan( $customer_id );

      if ( ! $plan_data )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: invalid customer_id' , 'NS0001' );

      $return = mvneMakeitsoRenewPlan( $customer_id , $plan_data['plan'] );

      if ( ! $return['success'] )
        $this->errException( $return['errors'][0] , 'NS0001' );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }


  /**
   * mvneinternal__AddData
   *
   * add subscriber data
   * @see MVNO-2427
   * @return Result object
   */
  public function mvneinternal__AddData()
  {
    // init
    list ($iccid, $msisdn, $data_option, $reason, $user) = $this->getInputValues();
    if (empty($user))
      $user = $_SERVER['REMOTE_USER'];

    try
    {
      // verify parameters
      if ( ! $iccid && ! $msisdn)
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN', 'NS0001');

      // get customer and fill in missing parameters
      teldata_change_db();
      if ($iccid)
      {
        $iccid = luhnenize($iccid);
        $customer = get_customer_from_iccid($iccid);
        if ( ! $customer)
          $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
        if ( ! $customer->current_mobile_number)
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no phone number', 'MV0001');
        $msisdn = $customer->current_mobile_number;
      }
      else // $msisdn
      {
        $msisdn = normalize_msisdn($msisdn, FALSE);
        $customer = get_customer_from_msisdn($msisdn);
        if ( ! $customer)
          $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
        if ( ! $customer->current_iccid && ! $customer->CURRENT_ICCID_FULL)
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer has no SIM card', 'IC0001');
        $iccid = $customer->CURRENT_ICCID_FULL ? $customer->CURRENT_ICCID_FULL : luhnenize($customer->current_iccid);
      }

      // verify state
      if ($customer->plan_state != 'Active')
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer state', 'IN0001');

      // verify MVNE
      if ($customer->MVNE != parent::MVNE2)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer MVNE', 'VV0016');

      // call MW
      dlog('', "INFO: sub: {$customer->CUSTOMER_ID}, iccid: $iccid, msisdn: $msisdn, data: $data_option, reason: $reason, user: $user");
      $middleware = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $middleware->mwMakeitsoUpgradePlan(array(
        'actionUUID'          => $this->getRequestId(),
        'msisdn'              => $msisdn,
        'iccid'               => $iccid,
        'customer_id'         => $customer->CUSTOMER_ID,
        'wholesale_plan'      => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
        'ultra_plan'          => get_plan_from_cos_id($customer->COS_ID),
        'preferred_language'  => $customer->preferred_language,
        'option'              => $data_option));
      teldata_change_db();

      // check result
      if ($result->is_failure() )
        $this->errException('ERR_API_INTERNAL: failed to add voice minites - ' . $result->get_errors(), 'MW0001');

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }


  /**
   * mvneinternal__RetryMakeItSo
   *
   * Retry a MakeItSo task ; Fix stuck Command invocations for the customer.
   *
   * @return Result object
   */
  public function mvneinternal__RetryMakeItSo ()
  {
    list ( $makeitso_queue_id , $reason , $user ) = $this->getInputValues();

    if ( !$user )
      $user = $_SERVER['PHP_AUTH_USER'];

    if ( !$user )
      $user = apache_getenv('REMOTE_USER');

    try
    {
      // load MakeItSo task

      \Ultra\Lib\DB\ultra_acc_connect();

      $data = get_makeitso(
        array(
          'makeitso_queue_id' => $makeitso_queue_id
        )
      );

      if (!( $data && is_array($data) && count($data) ))
        $this->errException(
          'no MakeItSo found',
          'MI0031',
          'no MakeItSo found'
        );

      dlog('',"data = %s",$data);

      // check STATUS = FAILED

      if ( $data[0]->STATUS != 'FAILED' )
        $this->errException(
          'MakeItSo is not in status FAILED',
          'MI0032',
          'MakeItSo is not in status FAILED'
        );

      // fix stuck Command invocations for the customer

      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id( $data[0]->CUSTOMER_ID , array('current_mobile_number','CURRENT_ICCID_FULL', 'BRAND_ID') );

      // if ($customer && in_array($customer->BRAND_ID, [3]))
      //   $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      if ( $customer && ( $customer->current_mobile_number || $customer->CURRENT_ICCID_FULL ) )
      {
        // load pending Command Invocation for this customer

        $commandInvocation = new \CommandInvocation();

        $result = $commandInvocation->loadPending(
          array(
            'customer_id' => $data[0]->CUSTOMER_ID
          )
        );

        if ( $result->is_success()
          && $result->is_pending()
          && ( $commandInvocation->started_hours_ago > 1 )
        )
        {
          // there is a Command invocation pending since more than 1 hour

          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $data = array(
            'actionUUID' => $this->getRequestId(),
            'iccid'      => $customer->CURRENT_ICCID_FULL,
            'msisdn'     => $customer->current_mobile_number
          );

          $result = $mwControl->mwQuerySubscriber( $data );

          // give up if there is a stuck asynch command
          if ( $result->is_success()
            && isset($result->data_array['body'])
            && ( property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) )
            && $result->data_array['body']->CurrentAsyncService
          )
            $this->errException(
              "ERR_API_INTERNAL: there is a stuck asynch command (".$result->data_array['body']->CurrentAsyncService."). Please escalate",
              'MI0031',
              "ERR_API_INTERNAL: there is a stuck asynch command (".$result->data_array['body']->CurrentAsyncService."). Please escalate"
            );

          // give up if QuerySubscriber fails
          if ( $result->is_failure() )
            $this->errException(
              "ERR_API_INTERNAL: QuerySubscriber failed",
              'MI0031',
              "ERR_API_INTERNAL: QuerySubscriber failed"
            );

          // fix pending Command invocation

          $result = $commandInvocation->escalationSuccess(
            array(
              'from_status'            => $commandInvocation->status,
              'last_status_updated_by' => $user
            )
          );

          if ( $result->is_failure() )
            $this->errException(
              "ERR_API_INTERNAL: Command invocation escalation Success failed",
              'MI0031',
              "ERR_API_INTERNAL: Command invocation escalation Success failed"
            );
        }
      }

      // retry MakeItSo

      $makeItSo = new \Ultra\Lib\MVNE\MakeItSo();

      $result = $makeItSo->retry( $makeitso_queue_id );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $this->errException(
          "ERR_API_INTERNAL: ".$errors[0],
          'MI0031',
          "ERR_API_INTERNAL: ".$errors[0]
        );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

  /**
   * mvneinternal__RedoMakeItSo
   *
   * Retry a failed MakeItSo task after adjusting its parameters.
   *
   * @return Result object
   */
  public function mvneinternal__RedoMakeItSo ()
  {
    list ( $makeitso_queue_id , $reason , $user ) = $this->getInputValues();

    if ( !$user )
      $user = $_SERVER['PHP_AUTH_USER'];

    if ( !$user )
      $user = apache_getenv('REMOTE_USER');

    try
    {
      // load MakeItSo task

      \Ultra\Lib\DB\ultra_acc_connect();

      $data = get_makeitso(
        array(
          'makeitso_queue_id' => $makeitso_queue_id
        )
      );

      if (!( $data && is_array($data) && count($data) ))
        $this->errException(
          'no MakeItSo found',
          'MI0031',
          'no MakeItSo found'
        );

      dlog('',"data = %s",$data);

      // check STATUS = FAILED

      if ( $data[0]->STATUS != 'FAILED' )
        $this->errException(
          'MakeItSo is not in status FAILED',
          'MI0032',
          'MakeItSo is not in status FAILED'
        );

      // fix stuck Command invocations for the customer

      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id( $data[0]->CUSTOMER_ID , array('current_mobile_number','CURRENT_ICCID_FULL') );

      if ( $customer && ( $customer->current_mobile_number || $customer->CURRENT_ICCID_FULL ) )
      {
        // load pending Command Invocation for this customer

        $commandInvocation = new \CommandInvocation();

        $result = $commandInvocation->loadPending(
          array(
            'customer_id' => $data[0]->CUSTOMER_ID
          )
        );

        if ( $result->is_success()
          && $result->is_pending()
          && ( $commandInvocation->started_hours_ago > 1 )
        )
        {
          // there is a Command invocation pending since more than 1 hour

          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $data = array(
            'actionUUID' => $this->getRequestId(),
            'iccid'      => $customer->CURRENT_ICCID_FULL,
            'msisdn'     => $customer->current_mobile_number
          );

          $result = $mwControl->mwQuerySubscriber( $data );

          // give up if there is a stuck asynch command
          if ( $result->is_success()
            && isset($result->data_array['body'])
            && ( property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) )
            && $result->data_array['body']->CurrentAsyncService
          )
            $this->errException(
              "ERR_API_INTERNAL: there is a stuck asynch command (".$result->data_array['body']->CurrentAsyncService."). Please escalate",
              'MI0031',
              "ERR_API_INTERNAL: there is a stuck asynch command (".$result->data_array['body']->CurrentAsyncService."). Please escalate"
            );

          // give up if QuerySubscriber fails
          if ( $result->is_failure() )
            $this->errException(
              "ERR_API_INTERNAL: QuerySubscriber failed",
              'MI0031',
              "ERR_API_INTERNAL: QuerySubscriber failed"
            );

          // fix pending Command invocation

          $result = $commandInvocation->escalationSuccess(
            array(
              'from_status'            => $commandInvocation->status,
              'last_status_updated_by' => $user
            )
          );

          if ( $result->is_failure() )
            $this->errException(
              "ERR_API_INTERNAL: Command invocation escalation Success failed",
              'MI0031',
              "ERR_API_INTERNAL: Command invocation escalation Success failed"
            );
        }
      }

      // redo MakeItSo

      $makeItSo = new \Ultra\Lib\MVNE\MakeItSo();

      $result = $makeItSo->redo( $makeitso_queue_id );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $this->errException(
          "ERR_API_INTERNAL: ".$errors[0],
          'MI0031',
          "ERR_API_INTERNAL: ".$errors[0]
        );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

  /**
   * mvneinternal__NetworkRestore
   *
   * Restore a suspended customer on the network using mwRestoreSubscriber
   *
   * @return Result object
   */
  public function mvneinternal__NetworkRestore ()
  {
    list ( $ICCID , $MSISDN , $customer_id , $reason , $user ) = $this->getInputValues();

    $error_code = '';

    $MSISDN = normalize_msisdn_10( $MSISDN );

    try
    {
      if ( !$customer_id )
      {
        teldata_change_db();

        $customers = get_ultra_customers_from_msisdn($MSISDN,array('customer_id', 'BRAND_ID'));

        if ( ( ! $customers ) || ( ! is_array( $customers ) ) || ( ! count( $customers ) ) )
          $this->errException(
            'no customer found',
            'VV0031' ,
            'no customer found'
          );

        // brand validation if customer_id was not passed
        // if (in_array($customers[0]->BRAND_ID, [3]))
        //   $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

        $customer_id = $customers[0]->customer_id;
      }
      else
      {
        // brand validation if customer_id was passed
        $customer = get_ultra_customer_from_customer_id( $customer_id, array('BRAND_ID') );
        if (!$customer)
          $this->errException('no customer found', 'VV0031', 'no customer found');

        if (in_array($customer->BRAND_ID, [3]))
          $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );
      }

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwRestoreSubscriber(
        array(
          'actionUUID'  => $this->getRequestId(),
          'msisdn'      => $MSISDN,
          'iccid'       => luhnenize( $ICCID ),
          'customer_id' => $customer_id
        )
      );

      // did mwRestoreSubscriber fail?
      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $error_code = 'MW0001';
        throw new \Exception("ERR_API_INTERNAL: ".$errors[0]);
      }

      // did RestoreSubscriber fail?
      if ( ! $result->data_array['success'] )
      {
        $error_code = 'MW0001';
        throw new \Exception("ERR_API_INTERNAL: ".$result->data_array['errors'][0]);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
        
  /**
   * mvneinternal__NetworkSuspend
   *
   * Suspend a customer on the network using mwSuspendSubscriber
   *
   * @return Result object
   */
  public function mvneinternal__NetworkSuspend ()
  {
    list ( $ICCID , $MSISDN , $customer_id , $reason , $user ) = $this->getInputValues();

    $error_code = '';

    $MSISDN = normalize_msisdn_10( $MSISDN );

    try
    {
      if ( !$customer_id )
      {
        teldata_change_db();

        $customers = get_ultra_customers_from_msisdn($MSISDN,array('customer_id'));

        if ( ( ! $customers ) || ( ! is_array( $customers ) ) || ( ! count( $customers ) ) )
          $this->errException(
            'no customer found',
            'VV0031' ,
            'no customer found'
          );

        // brand validation if customer_id was not passed
        if (in_array($customers[0]->BRAND_ID, [3]))
          $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

        $customer_id = $customers[0]->customer_id;
      }
      else
      {
        // brand validation if customer_id was passed
        $customer = get_ultra_customer_from_customer_id( $customer_id, array('BRAND_ID') );
        if (!$customer)
          $this->errException('no customer found', 'VV0031', 'no customer found');

        if (in_array($customer->BRAND_ID, [3]))
          $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );
      }

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $querySubResult = $mwControl->mwQuerySubscriber(
        array(
          'actionUUID' => $this->getRequestId(),
          'iccid'      => $ICCID,
          'msisdn'     => $MSISDN
        )
      );

      if ($querySubResult->is_failure())
      {
          $errors = $querySubResult->get_errors();
          $this->errException("ERR_API_INTERNAL: " . $errors[0], 'MW0001');
      }

      if (isset($querySubResult->data_array['ResultCode']) && $querySubResult->data_array['ResultCode'] != 100)
        $this->errException($querySubResult->data_array['ResultMsg'], 'MW0001');

      if (!isset($querySubResult->data_array['body']->SubscriberStatus))
      {
        $this->errException('ERR_API_INTERNAL: Could not load current state for customer.', 'UN0001');
      }

      if ($querySubResult->data_array['body']->SubscriberStatus == 'Suspended')
      {
        $this->addWarning('ICCID already suspended');
      }
      else
      {
        if ($querySubResult->data_array['body']->SubscriberStatus != 'Active')
        {
          $this->errException('ERR_API_INTERNAL: SubscriberStatus is not Active.', 'MW0001');
        }

        $result = $mwControl->mwSuspendSubscriber(
          array(
            'actionUUID'  => $this->getRequestId(),
            'msisdn'      => $MSISDN,
            'iccid'       => luhnenize( $ICCID ),
            'customer_id' => $customer_id
          )
        );

        // did mwSuspendSubscriber fail?
        if ( $result->is_failure() )
        {
          $errors = $result->get_errors();

          $this->errException("ERR_API_INTERNAL: ".$errors[0], 'MW0001');
        }

        // did SuspendSubscriber fail?
        if ( ! $result->data_array['success'] )
        {
          $this->errException("ERR_API_INTERNAL: ".$result->data_array['errors'][0], 'MW0001');
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

}

