<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Interactive Care partner class
 *
 * @author VYT 2014-05
 * @project Ultra Self Care
 */
class InteractiveCare extends \Ultra\Lib\Api\PartnerBase
{

  /**
   * interactivecare__RequestProvisionOrangeBySMS
   *
   * Orange SIM activation from SMS using a fake MSISDN
   * @see https://issues.hometowntelecom.com:8443/browse/OSVS-4
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+Interactive+Care#Commands:InteractiveCare-interactivecare::AttemptSMSActivationOrange
   *
   * @return Result object
   */
  public function interactivecare__RequestProvisionOrangeBySMS ()
  {
    list ( $fake_msisdn , $zipcode ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      // convert fake_msisdn to an "act code"

      $derived  = tmo_dummymsisdn( $fake_msisdn );
      $sim_data = get_htt_inventory_sim_from_imsi(str_replace('derived_', '', $derived['imsi']));

      if ( empty($sim_data[0]->ICCID_NUMBER) )
      {
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: Unable to find SIM Data from derived IMSI' , 'ND0001' );
      }

      $sim_data = $sim_data[0];
      dlog('',"sim_data = %s", $sim_data);

      // SIM validation

      if ( $sim_data->PRODUCT_TYPE != 'ORANGE' )
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: Incorrect ICCID Product type.' , 'IC0002' );

      if ( ! $sim_data->ACT_CODE || ! $sim_data->STORED_VALUE )
        $this->errException( 'ERR_API_INTERNAL: the SIM is invalid.' , 'VV0013' );

      if ( ! $sim_data->SIM_HOT )
        $this->errException( 'ERR_API_INTERNAL: the SIM cannot be activated.' , 'VV0012' );

      if ( $sim_data->SIM_ACTIVATED || $sim_data->CUSTOMER_ID )
        $this->errException( 'ERR_API_INTERNAL: the SIM is already used.' , 'VV0014' );

      $redis = new \Ultra\Lib\Util\Redis;

      // Activate/Can
      if ( $redis->get( 'iccid/good_to_activate/' . $sim_data->ICCID_FULL ) )
        $redis->del( 'iccid/good_to_activate/' . $sim_data->ICCID_FULL );
      else
      {
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        $mwResult = $mwControl->mwCanActivate(
          array(
            'actionUUID' => $this->getRequestId(),
            'iccid'      => $sim_data->ICCID_FULL
          )
        );

        if ($mwResult->is_failure() || $mwResult->is_timeout())
          $this->errException( 'ERR_API_INTERNAL: Middleware error.' , 'MW0001' );

        if ( ! isset($mwResult->data_array['available']))
          $this->errException( 'ERR_API_INTERNAL: the SIM cannot be activated.' , 'VV0012' );

        teldata_change_db();
      }

      // determine zip code

      if ( $zipcode )
      {
        list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'US');

        if ( $error )
          $zipcode = NULL;
        else
        {
          list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');

          if ( $error )
            $zipcode = NULL;
        }
      }

      if ( ! $zipcode )
        // if none provided, or invalid zip is provided
        $zipcode = $this->get_zipcode_for_RequestProvisionOrangeBySMS();

      dlog('',"zipcode = %s , iccid = %s",$zipcode,$sim_data->ICCID_FULL);

      // Orange SIM activation tracking parameters
      $activation_masteragent = 54;
      $activation_store       = 28;
      $activation_userid      = 67;
      $customer_source        = get_customer_source($activation_masteragent, $activation_store, $activation_userid);

      // create ULTRA user in DB
      $params = array(
        'dealer'                => $activation_store,
        'masteragent'           => $sim_data->INVENTORY_MASTERAGENT,
        'distributor'           => $sim_data->INVENTORY_DISTRIBUTOR,
        'userid'                => $activation_userid,
        'preferred_language'    => 'EN',
        'cos_id'                => get_cos_id_from_plan('STANDBY'),
        'postal_code'           => $zipcode,
        'country'               => 'USA',
        'plan_state'            => 'Neutral',
        'plan_started'          => 'NULL',
        'plan_expires'          => 'NULL',
        'customer_source'       => $customer_source,
        'current_iccid'         => $sim_data->ICCID_NUMBER,
        'current_iccid_full'    => $sim_data->ICCID_FULL,
        // 'notes'                 => "CH: $activation_channel; INFO: $activation_info" // TODO: Riz?
      );

      $result = create_ultra_customer_db_transaction($params);

      if ( empty( $result['customer'] ) )
        $this->errException('ERR_API_INTERNAL: failed to create customer', 'DB0001');

      $customer = $result['customer'];

      // fund the customer from the SIM
      $funcResult = func_add_stored_value_from_orange_sim(
        array(
          'amount'   => $sim_data->STORED_VALUE,
          'customer' => $customer,
          'session'  => $sim_data->ACT_CODE,
          'cos_id'   => get_cos_id_from_plan( func_get_orange_plan_from_sim_data($sim_data) ),
          'detail'   => __FUNCTION__
        )
      );

      if ( ! $funcResult->is_success() )
        $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');

      // log event in ULTRA.HTT_ACTIVATION_HISTORY
      log_funding_in_activation_history( $customer->CUSTOMER_ID , 'PREPAID' , $sim_data->STORED_VALUE );

      // set activation attribution information to be stored into HTT_ACTIVATION_LOG at activation time
      set_redis_provisioning_values($customer->CUSTOMER_ID, $activation_masteragent, NULL, $sim_data->INVENTORY_DISTRIBUTOR, $activation_store, $activation_userid, $redis);

      // prepare state transition parameters
      $context          = array('customer_id' => $customer->CUSTOMER_ID );
      $resolve_now      = FALSE;
      $max_path_depth   = 1;
      $dry_run          = TRUE;
      $plan             = get_plan_name_from_short_name( func_get_orange_plan_from_sim_data($sim_data) );
      $transition_name  = 'Orange Sim Activation '.$plan ;

      // Tests State Transition 'Neutral' => 'Active'

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

      dlog('',"%s",$result_status);

      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException('ERR_API_INTERNAL: state transition error', 'SM0001');
      }

      // State Transition is allowed (pre-requisites are met)

      $dry_run = FALSE;

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

      dlog('',"%s",$result_status);

      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException('ERR_API_INTERNAL: state transition error', 'SM0001');
      }

      reserve_iccid($sim_data->ICCID_NUMBER);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $fraudStatus = $this->isSuccess() ? 'success' : 'error';
    $data = array('source' => json_encode($this->getInputValues()));
    $customer = (isset($customer) && is_object($customer)) ? $customer : null;

    fraud_event($customer, 'interactivecare', 'RequestProvisionOrangeBySMS', $fraudStatus, $data);

    return $this->result;
  }

  function get_zipcode_for_RequestProvisionOrangeBySMS()
  {
    $list = array(48201, 48206, 48210, 48215, 48221, 48279, 48288, 48277, 48264, 48235);

    return $list[ array_rand($list) ];
  }

  /**
   * interactivecare__CheckCallCreditOptions
   *
   * return possible purchase options for buying Additional Call Anywhere Credit
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+Interactive+Care
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Additional+Call+Anywhere+Credit+Project
   * @return Result object
   */
  public function interactivecare__CheckCallCreditOptions()
  {
    // initialize
    list ($msisdn) = $this->getInputValues();
    $msisdn = normalize_msisdn($msisdn);
    $customer_active = 0;
    $call_credit_options = NULL;
    $wallet_balance = NULL;
    $message = NULL;
    $language = 'EN';

    try
    {
      // get subscriber object
      teldata_change_db();
      if (! $customer = get_customer_from_msisdn($msisdn))
      {
        $message = 'Ultra Mobile subscriber not found.';
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }
      $language = $customer->preferred_language;
      $plan_cost = get_plan_costs_by_customer_id($customer->CUSTOMER_ID, $customer->COS_ID); 

      // confirm customer status
      if ($customer->plan_state != 'Active')
      {
        // generate suspended message
        $date = get_date_from_full_date($customer->latest_plan_date);
        $date = substr($date, 0, strlen($date) - 5);
        $message = \Ultra\Messaging\Templates\SMS_by_language(
          array(
            'message_type'  => 'account_suspended',
            'date'          => $date,
            'value'         => $plan_cost['plan'] - $customer->BALANCE,
            'plan_name'     => $plan_cost['name']),
          $customer->preferred_language,
          $customer->BRAND_ID
        );

        $this->errException('ERR_API_INVALID_ARGUMENTS: customer must be Active', 'IN0001');
      }

      // get customer info
      $customer_active = 1;
      $wallet_balance = $customer->BALANCE * 100;

      // get call credit options
      $call_credit_options = \Ultra\UltraConfig\call_anywhere_additional_credit_options();
      if ( ! $call_credit_options || ! is_array( $call_credit_options ) )
        $this->errException('ERR_API_INTERNAL: data not found' , 'ND0001');

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $this->addToOutput('call_credit_options', $call_credit_options);
    $this->addToOutput('customer_active', $customer_active);
    $this->addToOutput('wallet_balance', $wallet_balance);
    $this->addToOutput('message', $message);
    $this->addToOutput('language', $language);
    return $this->result;
  }


  /**
   * interactivecare__ApplyCallCredit
   *
   * debit the wallet and add money to Call Anywhere Credit
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+Interactive+Care
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Additional+Call+Anywhere+Credit+Project
   * @return Result object
   */
  public function interactivecare__ApplyCallCredit()
  {
    // initialize
    list ($msisdn, $charge_amount) = $this->getInputValues();
    $msisdn = normalize_msisdn($msisdn);

    try
    {
      // get subscriber object
      teldata_change_db();
      if (! $customer = get_customer_from_msisdn($msisdn))
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // confirm customer status and get info
      if ($customer->plan_state != 'Active')
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer must be Active', 'IN0001');

      // get call anywhere credits
      $call_anywhere_additional_credit_options = \Ultra\UltraConfig\call_anywhere_additional_credit_options();
      if ( ! $call_anywhere_additional_credit_options || ! is_array( $call_anywhere_additional_credit_options ) )
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );

      // locate options that corresponds to the charge amount
      $credit_option = NULL;
      foreach( $call_anywhere_additional_credit_options as $option )
        if ( $option->cost == ( $charge_amount / 100 ) )
          $credit_option = $option;

      // check if the amount matches with the allowed credit options
      if ( ! $credit_option )
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid amount', 'VV0115');

      // check if the customer has enough money in his BALANCE
      if ( $customer->BALANCE < ( $charge_amount / 100 ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: insufficient funds', 'VV0103');

      // execute the following DB operations in a DB transaction
      if ( ! start_mssql_transaction() )
        $this->errException('ERR_API_INTERNAL: DB write error (0)', 'DB0001');

      $result = func_add_ild_minutes(
        array(
          'customer'  => $customer,
          'amount'    => ( $charge_amount / 100 ),
          'reason'    => __FUNCTION__,
          'source'    => 'INTERACTIVECARE',
          'reference' => $this->getRequestId(),
          'balance_change' => ( $charge_amount / 100 ),
          'bonus'     => $credit_option->bonus_percent));

      if ( ! $result['success'] )
      {
        rollback_mssql_transaction();
        $this->errException('ERR_API_INTERNAL: DB write error (1)', 'DB0001');
      }

      if ( ! commit_mssql_transaction() )
        $this->errException('ERR_API_INTERNAL: DB write error (2)', 'DB0001');

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }


  /**
   * interactivecare__TriggerPromoBroadcastShortCode
   *
   * Triggers actions associated to a Promo Campaign Shortcode
   * WARNING: care must be taken to return end-user friendly error messages which are displayed to subscribers
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+Interactive+Care
   * @return Result object
   */
  public function interactivecare__TriggerPromoBroadcastShortCode()
  {
    // initialize
    list ($msisdn, $shortcode, $sms_text) = $this->getInputValues();
    $msisdn = normalize_msisdn($msisdn);
    $shortcode = trim(strtoupper($shortcode), ". \t\n\r\0\x0B"); // aka keyword

    try
    {
      teldata_change_db();

      // get subscriber object
      if ( ! $customer = get_customer_from_msisdn($msisdn) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031', 'This number does not belong to an Ultra Mobile subscriber.');

      // verify subscriber status
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: failed to load current customer state', 'UN0001', 'Cannot determine subscriber status.');
      if ( ! in_array($state['state'], array(STATE_ACTIVE, STATE_PROVISIONED, STATE_SUSPENDED)))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001', 'Subscriber status is invalid.');

      $result = \Ultra\Lib\PromoCampaign\triggerShortCode( $shortcode , $customer );

      if ( $result->is_failure() )
        $this->errException(
          $result->data_array['error'],
          $result->data_array['error_code'],
          $result->data_array['user_error']
        );

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', $e->getMessage());
    }

    $this->addApiErrorNode();
    return $this->result;
  }

}

