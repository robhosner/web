{
  "meta": { "version": "2" },
  "security_meta": { "RBAC": "customercare group" },
  "commands":
  {
    "customercare-rainmaker.json": "include",
    "customercare-interactivecare-rainmaker.json": "include",
    "customercare-internal-rainmaker.json": "include",

    "customercare__GetBoltOnHistoryByCustomerId":
    {
      "desc": "retrive bolt on history of the given customer",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "bolt_on_history", "type": "string[]", "desc": "array of objects from ULTRA.BOLTON_TRACKER" }
      ]
    },

    "customercare__AddCourtesyCash":
    {
      "desc": "Gives the customer increased $ that can be used for anything; but will not be commissioned.",
      "parameters":
      [
       {
          "name": "reason",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4 },
          "desc": "Reason of the credit"
        },
        {
          "name": "agent_name",
          "type": "string",
          "required": true,
          "validation": { },
          "desc": "Agent making the request"
        },
        {
          "name": "customercare_event_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Identifier of the CS interaction"
        },
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": {},
          "desc": "Unique customer DB identifier"
        },
        {
          "name": "amount",
          "type": "integer",
          "required": true,
          "validation": { "min_value": 1 , "max_value": 9900 },
          "desc": "Dollars amount (in cents) to be accredited to the customer account"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__ShowInternationalDirectDialErrors":
    {
      "desc": "Returns debugging information on International Direct Dial attempts",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": {},
          "desc": "Unique customer DB identifier"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "history", "type": "string[]",  "desc": "International Direct Dial attempts history." }
      ]
    },

    "customercare__CustomerDirectSMS":
    {
      "desc": "Send an SMS to the phone number of the customer.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": {},
          "desc": "Unique customer DB identifier"
        },
        {
          "name": "message",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 132 },
          "desc": "Message to send to customer"
        },
        {
          "name": "agent",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Agent making the request"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__FlushStoredBalance":
    {
      "desc": "Set stored balance to zero.",
      "parameters":
      [
        {
          "name": "reason",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4 },
          "desc": "Reason of the credit"
        },
        {
          "name": "agent_name",
          "type": "string",
          "required": true,
          "validation": { },
          "desc": "Agent making the request"
        },
        {
          "name": "customercare_event_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Identifier of the CS interaction"
        },
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Unique customer DB identifier"
        }
      ],

      "returns":
      [
          "common-returns.json"
      ]
    },

    "customercare__CancelDeviceLocation":
    {
      "desc": "Invokes a canceldevicelocation on the network.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": {},
          "desc": "Unique customer DB identifier."
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__GetBillingHistory":
    {
      "desc": "Returns billing history for a given customer.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Unique customer DB identifier"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "billing_history", "type": "string[]", "desc": "Billing history"}
      ]
    },

    "customercare__CancelPortIn":
    {
      "desc": "Cancels a Port Request. (then does an API QueryPortIn to send the customer to Port-In Denied.).",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Unique customer DB identifier"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },


    "customercare__SearchCustomers":
    {
      "desc": "Provides basic search interface for customers.",
      "parameters":
      [
        {
          "name": "msisdn",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 10, "max_strlen": 11 },
          "desc": "Phone number"
        },
        {
          "name": "iccid",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 18, "max_strlen": 19 },
          "desc": "ICCID"
        },
        {
          "name": "name",
          "type": "string",
          "required": false,
          "validation": { },
          "desc": "First or Last name"
        },
        {
          "name": "email",
          "type": "string",
          "required": false,
          "validation": { },
          "desc": "Email address"
        },
        {
          "name": "actcode",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 11, "max_strlen": 11 },
          "desc": "Customer's Actcode"
        }
      ],

      "returns":
      [
        "common-returns.json",

        { "name": "customers",   "type": "string",   "desc": "JSON-encoded list of customers." }
      ]
    },

    "customercare__SetCustomerCreditCard":
    {
      "desc": "Updates account Credit Card to a user",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Unique customer DB identifier."
        },
        {
          "name": "account_cc_exp",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 4 },
          "desc": "Credit Card Expiration Date - format is MMYY."
        },
        {
          "name": "account_cc_cvv",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "Customer CVV."
        },
        {
          "name": "account_cc_number",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 15, "max_strlen": 16 },
          "desc": "Customer Number."
        },
        {
          "name": "account_zipcode",
          "type": "string",
          "required": true,
          "validation": { "zipcode": "US" },
          "desc": "Customer Billing Zip Code."
        }
      ],
      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__UnblockPortalLogin":
    {
      "desc": "Unblock a user who has tried logging into portal__Login too many times and has been blocked.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "string",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "agent_name",
          "type": "string",
          "required": false,
          "validation": { },
          "desc": "Agent making the request"
        },
        {
          "name": "customercare_event_id",
          "type": "integer",
          "required": false,
          "validation": { },
          "desc": "Identifier of the CS interaction"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__CheckBalances":
    {
      "desc": "Returns balances from CheckBalance.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "string",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "balances", "type": "string[]", "desc": "balances from CheckBalance." }
      ]
    },

    "customercare__ValidateGBASim":
    {
      "desc": "Returns whether customer_id has valid GBA sim.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "string",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "valid", "type": "string[]", "desc": "is valid GBA sim" }
      ]
    },

    "customercare__GetE911SubscriberAddress":
    {
      "desc": "Retrieve customer E911 Subscriber Address",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "address", "type": "string", "desc": "E911 address" }
      ]
    },

    "customercare__UpdateWifiCalling":
    {
      "desc": "Update Wifi Calling setting.",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "address1",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 3, "max_strlen": 80, "stringformat": "alphanumeric_spaces_punctuation_octothorpe" },
          "desc": "Customer billing address"
        },
        {
          "name": "address2",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 40, "stringformat": "alphanumeric_spaces_punctuation_octothorpe" },
          "desc": "Customer billing address part 2"
        },
        {
          "name": "city",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 2, "max_strlen": 25, "stringformat": "alphanumeric_punctuation" },
          "desc": "Customer billing city"
        },
        {
          "name": "state",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 2, "max_strlen": 3, "stringformat": "alphanumeric" },
          "desc": "Customer state or region"
        },
        {
          "name": "zipcode",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 2, "max_strlen": 20, "stringformat": "numeric" },
          "desc": "Customer billing postal code"
        },
        {
          "name": "enable_wifi_calling",
          "type": "boolean",
          "required": true,
          "default": "0",
          "validation": { },
          "desc": ""
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__UpdatePortInformation":
    {
      "desc": "Used to resume an already failed port. Funding and plan already exists for the customer; the only information that is necessary is a resubmission of the phone number.",
      "parameters":
      [
        {
          "name": "numberToPort",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 10, "min_strlen": 10 },
          "desc": "Phone number to light up on our system."
        },
        {
          "name": "portAccountNumber",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 , "regexp" : "^[A-Za-z0-9]+$" },
          "desc": "The account number of the account to be ported."
        },
        {
          "name": "portAccountPassword",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 15 , "regexp" : "^[A-Za-z0-9]+$" },
          "desc": "The account password of the account to be ported."
        },
        {
          "name": "portAccountZipcode",
          "type": "string",
          "required": false,
          "validation": { "zipcode": "US" },
          "desc": "The original/billing zip code of the account to be ported."
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__PromoEnroll":
    {
      "desc": "Retrieve customer E911 Subscriber Address",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "plan",
          "type": "string",
          "required": true,
          "validation": { "active_plan": 1 },
          "desc": "short plan name"
        },
        {
          "name": "reference_id",
          "type": "string",
          "required": true,
          "validation": { },
          "desc": "reference id to the promo"
        },
        {
          "name": "months",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "months to give to customer"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__EligibleChangeNumber":
    {
      "desc": "Whether subsriber is eligible for MSISDN replacement",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "eligible", "type": "boolean", "desc": "if eligible for number change" }
      ]
    },

    "customercare__FlexInfo":
    {
      "desc": "return flex info",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__ForceJoinFamily":
    {
      "desc": "forces child_customer_id to join parent_customer_id family",
      "parameters":
      [
        {
          "name": "parent_customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "parent Customer ID"
        },
        {
          "name": "child_customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "child Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__RunFlexSetup":
    {
      "desc": "run flex miso",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__AddPlanCredits":
    {
      "desc": "add plan credits to customer_id account",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "amount",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__SetCustomerCCToken":
    {
      "desc": "Set credit card fields for a customer after appropriate validation",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "cc_name",
          "type": "string",
          "required": false,
          "validation": { },
          "desc": "Customer billing name"
        },
        {
          "name": "cc_address1",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 3, "max_strlen": 80 },
          "desc": "Customer billing address"
        },
        {
          "name": "cc_address2",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 40 },
          "desc": "Customer billing address part 2"
        },
        {
          "name": "cc_city",
          "type": "string",
          "required":false,
          "validation": { "min_strlen": 2, "max_strlen": 25, "stringformat": "alphanumeric_punctuation" },
          "desc": "Customer billing city"
        },
        {
          "name": "cc_country",
          "type": "string",
          "required": false,
          "validation": { "matches": [ "US" ] },
          "desc": "Customer billing country"
        },
        {
          "name": "cc_state_or_region",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 2, "max_strlen": 40, "stringformat": "alphanumeric_spaces_punctuation" },
          "desc": "Customer billing state or region"
        },
        {
          "name": "cc_postal_code",
          "type": "integer",
          "required": true,
          "validation": { "min_strlen": 5, "max_strlen": 5 },
          "desc": "Customer billing postal code"
        },
        {
          "name": "account_cc_exp",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 4 },
          "desc": "Credit Card Expiration Date - format is MMYY."
        },
        {
          "name": "account_cc_cvv",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "Customer CVV."
        },
        {
          "name": "token",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 1, "max_strlen": 100 },
          "desc": "Credit Card token."
        },
        {
          "name": "bin",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 6, "max_strlen": 6 , "stringformat": "numeric" },
          "desc": "First six digits of credit card."
        },
        {
          "name": "last_four",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 4 , "stringformat": "numeric" },
          "desc": "Last four digits of credit card."
        },
        {
          "name": "processor",
          "type": "string",
          "required": false,
          "validation": {},
          "desc": "Credit card processor"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__VerifyPortInStatus":
    {
      "desc": "to be invoked after portal__RequestProvisionPortedCustomer in order to get the complete response",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "port_pending",       "type": "boolean",  "desc": "true if the request has not been completed yet" },
        { "name": "phone_number",       "type": "string",   "desc": "phone number of the activated customer" },
        { "name": "port_success",       "type": "boolean",  "desc": "true if the porting has been completed without errors" },
        { "name": "port_status",        "type": "string",   "desc": "current porting status" },
        { "name": "port_resolution",    "type": "string[]", "desc": "MSISDN: R-codes" }
      ]
    },

    "customercare__GetCustomerPreferences":
    {
      "desc": "return subscriber's personal preferences",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        }
      ],

      "returns":
      [
        "common-returns.json",
        { "name": "preferred_language",   "type": "string",   "desc": "preferred communication language" },
        { "name": "auto_recharge",        "type": "boolean",  "desc": "is monthly credit card auto renewal enabled?" },
        { "name": "marketing_sms",        "type": "boolean",  "desc": "are promotional SMS notifications enabled?" },
        { "name": "marketing_email",      "type": "boolean",  "desc": "are promotional e-mail notifications enabled?" },
        { "name": "marketing_voice",      "type": "boolean",  "desc": "are promotional phone call notifications enabled?" },
        { "name": "party_voice",          "type": "boolean",  "desc": "are 3rd party promotional phone call notifications enabled?" },
        { "name": "party_sms",            "type": "boolean",  "desc": "are 3rd party promotional SMS notifications enabled?" },
        { "name": "party_email",          "type": "boolean",  "desc": "are 3rd party promotional e-mail notifications enabled?" }
      ]
    },


    "customercare__SetCustomerPreferences":
    {
      "desc": "update subscriber's personal preferences",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "preferred_language",
          "type": "string",
          "required": false,
          "validation": { "matches": [ "EN", "ES", "ZH" ] },
          "desc": "preferred communication language"
        },
        {
          "name": "auto_recharge",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables monthly credit card auto renewal"
        },
        {
          "name": "marketing_sms",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables SMS promotional notifications"
        },
        {
          "name": "marketing_email",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables e-mail promotional notifications"
        },
        {
          "name": "marketing_voice",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables phone call promotional notifications"
        },
        {
          "name": "party_sms",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables 3rd parting marketing"
        },
        {
          "name": "party_email",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables 3rd parting marketing"
        },
        {
          "name": "party_voice",
          "type": "boolean",
          "required": false,
          "validation": { },
          "desc": "enables or disables 3rd parting marketing"
        },
        {
          "name": "tag",
          "type": "string",
          "required": false,
          "validation": { "matches": [ "CUSTOMER_CARE", "IVR" ] },
          "desc": ""
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    },

    "customercare__CalculateTaxesAndFees":
    {
      "desc": "Determine the Sales Tax and Regulatory fees that would be charged for a potential Credit Card purchase, provided a purchase amount.",
      "parameters":
      [
        {
          "name": "msisdn",
          "type": "integer",
          "required": true,
          "validation": { "min_strlen": 10, "max_strlen": 11 },
          "desc": "Customer's phone number"
        },
        {
          "name": "charge_amount",
          "type": "integer",
          "required": true,
          "validation": { "min_value": 1 },
          "desc": "The amount of the potential credit card purchase, represented in cents."
        },
        {
          "name": "zip",
          "type": "string",
          "required": false,
          "validation": { "zipcode": "US" },
          "desc": "If provided, calculate based on this value. Otherwise use the customer's billing zip code."
        },
        {
          "name": "product_type",
          "type": "string",
          "required": true,
          "validation": { "matches": ["PLAN", "DATA", "IDDCA", "ROAM", "GLOBE", "ADD_BALANCE", "LINE_CREDIT", "REPLACE_SIM"] },
          "desc": "product type"
        }
      ],

      "returns":
      [
        "common-returns.json",

        { "name": "sales_tax",               "type": "integer", "desc": "Sales tax in cents"    },
        { "name": "recovery_fee",            "type": "integer", "desc": "Recovery fee in cents" }
      ]
    },

    "customercare__ChargeCreditCard":
    {
      "desc": "customercare__ChargeCreditCard",
      "parameters":
      [
        {
          "name": "customer_id",
          "type": "integer",
          "required": true,
          "validation": { },
          "desc": "Customer ID"
        },
        {
          "name": "charge_amount",
          "type": "integer",
          "required": true,
          "validation": { "min_value": 1 , "max_value": 50000 },
          "desc": "charge amount in cents"
        },
        {
          "name": "destination",
          "type": "string",
          "required": true,
          "validation": { "matches": [ "WALLET","MONTHLY" ] },
          "desc": "where to deposit charged funds"
        },
        {
          "name": "product_type",
          "type": "string",
          "required": true,
          "validation": { "matches": ["PLAN", "DATA", "IDDCA", "ROAM", "GLOBE", "ADD_BALANCE", "LINE_CREDIT", "REPLACE_SIM"] },
          "desc": "product type"
        }
      ],

      "returns":
      [
        "common-returns.json"
      ]
    }
  }
}
