<?php
namespace Ultra\Lib\Services;

use \Ultra\Configuration\Configuration;
use Ultra\Utilities\Common as CommonUtilities;

class OnlineSalesAPI extends RestAPI
{
  private $configObj;
  private $config;

  /**
   * @var CommonUtilities
   */
  private $utilities;

  public function __construct(Configuration $configObj=null, CommonUtilities $utilities = null)
  {
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();
    $this->utilities = !empty($utilities) ? $utilities : new CommonUtilities();

    $this->config = $this->configObj->getOnlineSalesAPIConfig();
    if (empty($this->config['host']) || empty($this->config['basepath'])) {
      throw new \Exception('Missing OnlineSalesAPI configuration');
    }

    $curlConfig = [ CURLOPT_TIMEOUT => 30 ];

    parent::__construct($this->config['host'], $this->config['basepath'], $curlConfig, 3);
  }

  public function getOrderByOrderID($orderID, $zipCode)
  {
    $response = $this->get('retailer/orders/' . $orderID . '/?zipCode=' . $zipCode);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      $result = new \Result();
      $order = json_decode($response['body'], true);

      if (is_array($order)) {
        // validate response fields
        if (empty($order['items'])) {
          $result->add_error('Missing order field items');
        } else if (empty($order['cost']['wallet']['totalAmount'])) {
          $result->add_error('Missing order field wallet cost');
        } else if (empty($order['cost']['creditCard']['totalAmount'])) {
          $result->add_error('Missing order field credit card cost');
        } else if (empty($order['status']) || $order['status'] != 'created') {
          $result->add_error('Invalid order status of ' . $order['status']);
        } else {
          $result->data_array = $order;
          $result->succeed();
        }
      } else {
        $result->add_error('Failed to parse RetailerOrders response');
      }

      return $result;
    }
  }

  public function getRetailerOrders($customerId, $status)
  {
    $response = $this->get(
      'retailer/orders/customer/' . $customerId,
      [ 'status' => $status ]
    );

    if ($response['code'] != 200)
      return $this->getErrorResult($response);
    else
    {
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if (is_array($responseArr)){
        $result->data_array = $responseArr;
        $result->succeed();
      } else {
        $result->add_error('Failed to parse RetailerOrders response');
      }

      return $result;
    }
  }

  public function markOrderPaid($orderId, $zipCode, $amount)
  {
    $response = $this->post(
      'retailer/orders/' . $orderId,
      [
        'timestamp'             => time(),
        'storeZipcode'          => $zipCode,
        'phoneNumber'           => '2133334444',
        'providerTransactionId' => 'txID',
        'providerName'          => 'ultra',
        'providerTerminalId'    => '1',
        'totalAmount'           => $amount
      ]
    );

    if ($response['code'] != 200)
      return $this->getErrorResult($response);
    else
    {
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if (is_array($responseArr)){
        $result->data_array = $responseArr;
        $result->succeed();
      } else {
        $result->add_error('Failed to parse RetailerOrders response');
      }

      return $result;
    }
  }

  public function cancelOrder($orderId)
  {
    $response = $this->delete('retailer/orders/' . $orderId);

    if ($response['code'] != 200)
      return $this->getErrorResult($response);
    else
    {
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if (is_array($responseArr)){
        $result->data_array = $responseArr;
        $result->succeed();
      } else {
        $result->add_error('Failed to parse Online Sales DELETE response');
      }

      return $result;
    }
  }

  public function fulfillOrderItem($orderId, $itemId)
  {
    $response = $this->post("retailer/orders/$orderId/items/$itemId");

    if ($response['code'] != 200)
      return $this->getErrorResult($response);

    $result = new \Result();
    $result->succeed();
    return $result;
  }

  public function getOrder($iccid)
  {
    $result = new \Result();

    // append F to ICCID if need
    if (strlen($iccid) == 18) {
      $iccid = $this->utilities->luhnenize($iccid);
    }
    if (strlen($iccid) == 19) {
      $iccid = $iccid . 'F';
    }

    // call API
    $response = $this->get('/order/' . $iccid);
    $responseData = json_decode($response['body'], true);

    // handle response
    if ($response['code'] == 200) {
      // map response data
      $order = json_decode($response['body'], true);

      // add leading 0s to cc expiration, if needed
      $ccExpMonth = strlen($order['cc_exp_month']) == 1 ? '0' . $order['cc_exp_month'] : $order['cc_exp_month'];
      $ccExp = $ccExpMonth . $order['cc_exp_year'];

      $result->data_array['order'] = (object)[
        'postal_code'       => $order['bill_postcode'],
        'bin'               => $order['cc_bin'],
        'last_four'         => $order['cc_last4'],
        'expires_date'      => $ccExp,
        'cvv_validation'    => 'M',
        'avs_validation'    => 'Y',
        'gateway'           => 'MeS',
        'merchant_account'  => 1,
        'token'             => $order['mes_token'],
        'customer_ip'       => null,
        'first_name'        => $order['bill_firstname'],
        'last_name'         => $order['bill_lastname'],
        'address1'          => null,
        'address2'          => null,
        'city'              => null,
        'state'             => null,
        'auto_enroll'       => array_key_exists('auto_enroll', $order) ? (bool)$order['auto_enroll'] : false
      ];

      $result->succeed();
    } else {
      if ($response['code'] == 400) {
        $result->add_error('Invalid ICCID');
      } else if ($response['code'] == 404) {
        $result->add_error('Order not found');
      } else {
        // check for curl error
        if (!empty($response['curl_errno'])) {
          $result->add_error('OnlineSalesAPI curl call failed with error number ' . $response['curl_errno']);
        } else {
          $result->add_error('OnlineSalesAPI call failed with response code ' . $response['code']);
        }
      }
    }

    return $result;
  }

  private function getErrorResult($response)
  {
    $result = new \Result();

    // check for curl errors
    if (!empty($response['curl_errno'])) {
      $result->add_error('OnlineOrders API curl failed - ' . $response['curl_errno']);
      return $result;
    }

    // add errors from API response
    if (!empty($response['body'])) {
      $responseArr = json_decode($response['body'], true);
      if (is_array($responseArr) && !empty($responseArr['errors'])) {
        foreach ($responseArr['errors'] as $error) {
          $result->add_error($error);
        }
      }
    }

    return $result;
  }
}
