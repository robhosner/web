<?php

namespace Ultra\Lib\CustomerNotification;

/**
 * WorkflowInterface
 * class interface used by all workflows
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/3ci+Replacement
 * @author VYT, 2015
 */

interface WorkflowInterface
{
  /**
   * run
   * execute a workflow
   * @param Object Context instance
   * @return String CustomerNotifiaction error code or NULL on success
   */
  public function run($context);
}
