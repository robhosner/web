<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Language Decide workflow action
 */
class LanguageDecide extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * validate subscriber response and decide if language change is needed
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // decide on next action based in user response
    $context->resume();
    $keyword = $context->getKeyword(0);
    switch ($keyword)
    {
      case '1':
        return $this->jump($context, 'EN');

      case '2':
        return $this->jump($context, 'ES');

      case '3':
        return $this->jump($context, 'ZH');

      default: // invalid input: repeat previous action
        $context->setAction('Query');
        logInfo("invalid response $keyword, repeating action Query");
        return NULL;
    }
  }


  /**
   * jump
   * validate change and prepare paramaters for jump to immediate language change action
   * @param Object Context instance
   */
  private function jump($context, $language)
  {
    // check if same language requested
    if ($context->language == $language)
    {
      // terminate workflow
      $context->terminate();

      // inform that language is aready set
      if ( ! $message = \Ultra\Messaging\Templates\SMS_by_language(array('message_type' => 'language_notify'), $context->language))
        return 'IN0004';
      return $this->sendSms($context, $message);
    }

    // prepare for jump to Change
    $context->setLanguage($language);
    $context->setAction('Change');
    return NULL;
  }

}
