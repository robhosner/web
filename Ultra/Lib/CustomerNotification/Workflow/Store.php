<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/StoreQuery.php';

/**
 * workflow for SMS commands STORE, DEALER
 * query and return subscriber account balance details
 * request -> response only, implements a single default method
 */
class Store extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Query');
  protected $workflow = __CLASS__; // due to late static binding
}


// this workflow can also be called by the following additional keywords
class_alias(__NAMESPACE__ . '\\Store', __NAMESPACE__ . '\\dealer'); // we actually do not have any subscriber usage of this keyword
