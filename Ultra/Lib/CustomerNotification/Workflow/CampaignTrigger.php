<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Campaign Trigger workflow action
 */
class CampaignTrigger extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * call Ultra v2 API interactivecare__TriggerPromoBroadcastShortCode
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    // prepare API config
     $config = array(
      'command'   => 'interactivecare__TriggerPromoBroadcastShortCode',
      'version'   => 2);

    // prepare API parameters
    $params = array(
      'msisdn'          => $context->msisdn,
      'shortcode'       => $context->getKeyword(0, TRUE),
      'sms_text'        => implode(' ', $context->message)
    );

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // handle response
    if ( ! $result->success)
      return empty($result->error_codes) ? 'IN0005' : $result->error_codes[0];
    return NULL;
  }
}
