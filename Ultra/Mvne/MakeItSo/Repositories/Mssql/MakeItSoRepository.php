<?php
namespace Ultra\Mvne\MakeItSo\Repositories\Mssql;

use Ultra\Mvne\MakeItSo\Interfaces\MakeItSoRepositoryInterface;

/**
 * Class MakeItSoRepository
 * @package Ultra\Mvne\MakeItSo\Repositories\Mssql
 */
class MakeItSoRepository implements MakeItSoRepositoryInterface
{
  /**
   * @param $customerId
   * @return \object[]
   */
  public function getPendingMakeItSoByCustomerId($customerId)
  {
    return \get_pending_makeitso_by_customer_id($customerId);
  }
}