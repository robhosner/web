<?php
namespace Ultra\Mvne;

/**
 * Class UpgradePlanBase
 * @package Ultra\Mvne
 */
abstract class UpgradePlanBase
{
  /**
   * @var
   */
  public $actionUUID;

  /**
   * @var
   */
  public $msisdn;

  /**
   * @var
   */
  public $iccid;

  /**
   * @var
   */
  public $customer_id;

  /**
   * @var
   */
  public $wholesale_plan;

  /**
   * @var
   */
  public $ultra_plan;

  /**
   * @var
   */
  public $preferred_language;
}
