<?php

require_once 'PHPUnit/Framework/TestCase.php';
require_once 'db.php';
require_once 'lib/payments/functions.php';

/**
 * test class for lib/payments/functions.php
 */
class PaymentsTest extends PHPUnit_Framework_TestCase
{
  const TEST_CUSTOMER_ID = 31; # a test customer id

  const TEST_ORANGE_ICCID_19 = '1234567890123456769';

  /**
   * Prepares the environment before running a test.
   */
  protected function setUp()
  {
    parent::setUp ();

    teldata_change_db();
  }

  public function test_func_add_stored_value_from_orange_sim ()
  {
    $customer = get_customer_from_customer_id( self::TEST_CUSTOMER_ID );

    $this->assertTrue( ! ! $customer );

    $sql = htt_inventory_sim_select_query(
      array(
        "iccid_full" => self::TEST_ORANGE_ICCID_19
      )
    );

    $sim_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    $this->assertTrue( $sim_data && is_array( $sim_data ) && count($sim_data) && $sim_data[0]->STORED_VALUE );

    $result = func_add_stored_value_from_orange_sim(
      array(
        'customer' => $customer,
        'amount'   => $sim_data[0]->STORED_VALUE,
        'session'  => 'actcode',
      )
    );

    print_r($result);
  }
}

?>
