<?php

require_once 'PHPUnit/Framework/TestCase.php';
require_once 'db.php';
include_once 'lib/transitions.php';

/**
 * test transitions related functions
 */
class TransitionsTest extends PHPUnit_Framework_TestCase
{
  /**
   * Prepares the environment before running a test.
   */
  protected function setUp ()
  {
    teldata_change_db();
  }

  public function test_a ()
  {
    $transition_uuid = time().getmypid();

    echo "transition_uuid = $transition_uuid\n";

    $this->assertTrue(  ! ! reserve_transition_uuid_by_pid($transition_uuid) ); // reserved successfully
    $this->assertFalse( ! ! reserve_transition_uuid_by_pid($transition_uuid) ); // cannot reserve twice
    $this->assertTrue(  ! ! unreserve_transition_uuid_by_pid($transition_uuid) ); // unreserve
  }
}

?>
