<?php

require_once('web.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/TestConstants.php');


/**
 * tests for 'mvneinternal' partner APIs (V2.0)
 */
class PortalTest extends PHPUnit_Framework_TestCase
{

  public function setUp()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url() . '/ultra_api.php?bath=rest&format=JSON&partner_tag=portal&version=2';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
      CURLOPT_SSL_VERIFYPEER => FALSE);
  }

  /**
   * test__mvneinternal_mvneinternal__AddVoiceMinutes
   */
  public function test__mvneinternal__AddVoiceMinutes()
  {
    $url = $this->base_url . '&command=' . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url , $this);
    $defaultTester->runDefaultTests();

    // test missing parameters
    $params = array();
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $missing = 'API_ERR_PARAMETER: Missing mvneinternal__AddVoiceMinutes REST parameter';
    $this->assertContains("$missing voice_minutes!", $decoded_response->errors);
    $this->assertContains("$missing reason!", $decoded_response->errors);

    // test missing ICCID and MSISDN
    $params['voice_minutes'] = 125;
    $params['reason'] = 'PHPUnit test';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN", $decoded_response->errors);

    // avialable ICCID - i.e. no customer, no MSISDN
    $params['iccid'] = '890126084210773932';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: no customer found", $decoded_response->errors);

    // subscriber without MSISDN
    $params['iccid'] = '890126096210379114';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: customer has no phone number", $decoded_response->errors);
    
    // MSISDN without ICCID
    $params['iccid'] = NULL;
    $params['msisdn'] = '2188655554';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: customer has no SIM card", $decoded_response->errors);

    // invalid sub state
    $params['msisdn'] = '9514007859';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: invalid customer state", $decoded_response->errors);

    // invalid MVNE
    $params['msisdn'] = '5189829378';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: invalid customer MVNE", $decoded_response->errors);

    // successful test
    $params['msisdn'] = '9732348582';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
    $this->assertEquals(0, count($decoded_response->errors));
    $this->assertEquals(0, count($decoded_response->warnings));
  }


  /**
   * test__mvneinternal_mvneinternal__AddData
   */
  public function test__mvneinternal__AddData()
  {
    $url = $this->base_url . '&command=' . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url , $this);
    $defaultTester->runDefaultTests();

    // test missing parameters
    $params = array();
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $missing = 'API_ERR_PARAMETER: Missing mvneinternal__AddData REST parameter';
    $this->assertContains("$missing data_option!", $decoded_response->errors);
    $this->assertContains("$missing reason!", $decoded_response->errors);

    // test missing ICCID and MSISDN
    $params['data_option'] = 'A-DATA-BLK|50';
    $params['reason'] = 'PHPUnit test';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN", $decoded_response->errors);

    // avialable ICCID - i.e. no customer, no MSISDN
    $params['iccid'] = '890126084210773932';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: no customer found", $decoded_response->errors);

    // subscriber without MSISDN
    $params['iccid'] = '890126096210379114';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: customer has no phone number", $decoded_response->errors);
    
    // MSISDN without ICCID
    $params['iccid'] = NULL;
    $params['msisdn'] = '2188655554';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: customer has no SIM card", $decoded_response->errors);

    // invalid sub state
    $params['msisdn'] = '9514007859';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: invalid customer state", $decoded_response->errors);

    // invalid MVNE
    $params['msisdn'] = '5189829378';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains("ERR_API_INVALID_ARGUMENTS: invalid customer MVNE", $decoded_response->errors);

    // successful test
    $params['msisdn'] = '9732348582';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
    $this->assertEquals(0, count($decoded_response->errors));
    $this->assertEquals(0, count($decoded_response->warnings));
  }


  public function test__mvneinternal__NetworkSuspend()
  {
    $url = $this->base_url . '&command=' . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url , $this);
    $defaultTester->runDefaultTests();

    teldata_change_db();

    $customer = get_customer_from_customer_id(18671);

    $params = array(
      'reason' => 'testing',
      'iccid'  => $customer->CURRENT_ICCID_FULL,
      'msisdn' => $customer->current_mobile_number
    );

    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);

    print_r($decoded_response);
  }
}

?>
