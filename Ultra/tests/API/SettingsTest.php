<?php

require_once 'PHPUnit/Autoload.php';
require_once((getenv('HTT_CONFIGROOT') . '/e.php'));
require_once('web.php');
require_once('test/api/test_api_include.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

/**
 * Tests for 'settings' partner APIs
 */
class PromotionalTest extends PHPUnit_Framework_TestCase
{
  /**
   * The setUp() and tearDown() template methods are run once for each test method (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/ultra_api.php?bath=rest&partner=settings&version=2&command=settings__';

    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';

    // options for our curl commands
    $this->curl_options = array(
        CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }

  /**
   *************************************
   * Test for settings__GetAllSettings *
   *************************************
   */
  public function test__settings__GetAllSettings ()
  {
    $url = $this->base_url . 'GetAllSettings';

    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    // test no username
    ///////////////////
    $params = array();

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue( ! $decoded_response->success );


    // test success
    ///////////////
    $params = array( 'username' => 'username' );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue( ! ! $decoded_response->success );
    $this->assertTrue( ! ! is_array($decoded_response->setting_fields) );
    $this->assertTrue( ! ! is_array($decoded_response->setting_values) );
    $this->assertTrue( ! ! is_array($decoded_response->setting_descriptions) );
    $this->assertTrue( ! ! count($decoded_response->setting_fields) );
    $this->assertTrue( ! ! count($decoded_response->setting_values) );
    $this->assertTrue( ! ! count($decoded_response->setting_descriptions) );
  }

  /**
   *************************************
   * Test for settings__TriggerSetting *
   *************************************
   */
  public function test__settings__TriggerSetting ()
  {
    $url = $this->base_url . 'TriggerSetting';

    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    // test required parameters
    ///////////////////////////
    $params = array(
      'username'      => 'username',
      'setting_field' => 'setting_field'
    );

    foreach ( $params as $param )
    {
      $test_params = $params;

      unset( $test_params[ $param ] );

      $json_result = curl_post($url,$test_params,$this->curl_options,NULL,240);

      echo "\n$json_result\n";

      $decoded_response = json_decode($json_result);

      $this->assertTrue(   ! $decoded_response->success );
    }


    // test non-existent token
    //////////////////////////
    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue(   ! $decoded_response->success );


    // test 'runner/monthly_charge/enabled'
    ///////////////////////////////////////
    $params = array(
      'username'      => 'username',
      'setting_field' => 'runner/monthly_charge/enabled',
      'setting_value' => 'FALSE'
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue(   $decoded_response->success );
    $this->assertEquals( $decoded_response->new_value , 'FALSE' );


    $params['setting_value'] = 'TRUE';

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertTrue(   $decoded_response->success );
    $this->assertEquals( $decoded_response->new_value , 'TRUE' );
  }
}

?>
