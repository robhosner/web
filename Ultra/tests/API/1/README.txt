PHPunit test suits for ULTRA APIs version 1

ApiPublicTest.php: class to test 'api_public' group of APIs
CelluphoneTest.php: class to test 'celluphone' group of APIs
CustomercareTest.php: class to test 'customercare' group of APIs
InternalTest.php: class to test 'internal' group of APIs
InteractiveCareTest.php: class to test 'interactivecare' group of APIs (IVR)
ProvisioningTest.php: class to test 'provisioning' group of APIs
ExternalPaymentsTest.php: class to test 'externalpayments' partner APIs
