<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');

/**
 * Tests for 'interactivecare' partner APIs v1
 */
class InteractiveCareTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = NULL;
  public $apache_username = NULL;
  public $apache_password = NULL;
  public $curl_options    = NULL;

  /**
   * setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class
   */
  public function setUp()
  {
    // all APIs tested with this class will have be reachable with this base URL
    $this->base_url = find_site_url().'/pr/interactivecare/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC);
  }

  /**
   * test__interactivecare__ApplyPIN
   */
  public function test__interactivecare__ApplyPIN()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    $pins = array(
      153617423604905390,
      178446202269035373,
      7854033738,
      7264592830

    );

    $msisdns = array(
      3474255162,
      6176521251,
      6176521249,
      3474255162
    );

    $params = array(
        'partner_tag' => 'internal',
    );

    for ($i = 0; $i < count($pins); $i++)
    {
      $params['pin_number'] = $pins[$i];
      $params['msisdn'] = $msisdns[$i];

      $result = curl_post($url, $params, $this->curl_options, NULL, 240);
      echo "\nURL: $url\nJSON: $result\n";
      $response = json_decode($result);

      $this->AssertFalse($response->success);
    }

    $params['pin_number'] = 125552666347162418;
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // test override language
    $params['language'] = 'ES';
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
  }

  /**
   * test__interactivecare__SetAutoRechargeByMSISDN
   */
  public function test__interactivecare__SetAutoRechargeByMSISDN()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // test missing params
    $params = array();
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
    $this->AssertContains('API_ERR_PARAMETER: Missing interactivecare__SetAutoRechargeByMSISDN REST parameter msisdn!', $response->errors);
    $params['msisdn'] = '1';
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
    $this->AssertContains('API_ERR_PARAMETER: Missing interactivecare__SetAutoRechargeByMSISDN REST parameter auto_recharge!', $response->errors);

    // test invalid params
    $params['msisdn'] = '12345';
    $params['auto_recharge'] = '3';
        $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);
    $this->AssertContains('ERR_API_INVALID_ARGUMENTS: msisdn value is shorter than min_strlen', $response->errors);
    $this->AssertContains('ERR_API_INVALID_ARGUMENTS: auto_recharge value has no matches', $response->errors);

    // test invalid customer
    $params['msisdn'] = TEST_MSISDN_INVALID;
    $params['auto_recharge'] = '1';
        $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // test Cancelled customer
    $params['msisdn'] = '2188655554';
    $params['auto_recharge'] = '1';
        $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // test customer without CC on file
    $params['msisdn'] = '9292354273';
    $params['auto_recharge'] = '1';
        $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // successful test
    $params['msisdn'] = '6094019808';
    $params['auto_recharge'] = 0;
        $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    print_r($response);
    $this->AssertTrue($response->success);

  }


  /**
   * test__interactivecare__ShowCustomerInfo":
   */
  public function test__interactivecare__ShowCustomerInfo()
  {
    // API setup
   $url = $this->base_url . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // test invalid MSISDN
    $params['msisdn'] = TEST_MSISDN_INVALID;
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // test Provisioned subscriber
    $params['msisdn'] = '5717334955';
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertFalse($response->success);

    // test Suspended subscriber
    $params = array('msisdn' => '9499031488');
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

    // test Active L19 subscriber: voice info
    $params = array('msisdn' => '3474146152');
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

    // test Active L19 subscriber: data info
    $params['get_data_usage'] = TRUE;
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

     // test Active L39 Spanish subscriber: voice info
    $params = array('msisdn' => '3109808394');
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

    // test Active L39 Spanish subscriber: data info
    $params['get_data_usage'] = TRUE;
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

    // test subscriber data info without any data purchases
    $params = array('msisdn' => '9292574692', 'get_data_usage' => FALSE);
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

    // test subscriber data info with 2 recent data purchases
    $params = array('msisdn' => '5732000324', 'get_data_usage' => TRUE);
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);

    // test language override
    $params = array('msisdn' => '5732000324', 'language' => 'ES');
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue($response->success);
  }


}

?>
