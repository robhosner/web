<?php

/*
 * constant values used in API tests
 */

define ('TEST_APACHE_USER', 'dougmeli');
define ('TEST_APACHE_PASS', 'Flora');

// ICCID
define ('TEST_ICCID_MVNE_2', '8111008000008000118'); // ICCID with MVNE=2
define ('TEST_ICCID_INVALID','9999999999999999999'); // invalid ICCID
define ('TEST_ICCID_VALID',  '1010101010101010010'); // always succeed ICCID

// CUSTOMER_ID
define ('TEST_CUSTOMER_ID_CANCELLED', '5'); // HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STATE = 'Cancelled'
define ('TEST_CUSTOMER_ID_INVALID', '999999'); // does not exist in CUSTOMERS.CUSTOMER_ID
define ('TEST_CUSTOMER_ID_VALID', '2509'); // Active customer with lots of BALANCE
define ('TEST_CUSTOMER_ID_MVNE_1', '2365'); // HTT_CUSTOMERS_OVERLAY_ULTRA.MVNE = 1
define ('TEST_CUSTOMER_ID_MVNE_2', '2844'); // HTT_CUSTOMERS_OVERLAY_ULTRA.MVNE = 2

// MSISDN
define ('TEST_MSISDN_MVNE_2', '3473206507'); // HTT_ULTRA_MSISDN.MVNE = 2
define ('TEST_MSISDN_MVNE_1', '9494687512'); // HTT_ULTRA_MSISDN.MVNE = 1
define ('TEST_MSISDN_INVALID', '5555555555'); // HTT_ULTRA_MSISDN.MVNE = 1
define ('TEST_MSISDN_VALID',   '1001001000'); // always succeed

// Ultra PINs, see http://wiki.hometowntelecom.com:8090/display/ULTRA/Test+Payment+and+SIM+Info
define ('TEST_VALID_PIN', '300000003000000275'); // active PIN
define('TEST_INACTIVE_PIN', '222690588372079863'); // inactive PIN
define('TEST_INALID_PIN', '222690583372079863'); // invalid PIN
define('TEST_USED_PIN', '400000004000000649'); // already used PIN

// inComm PINs
define ('INCOMM_ACTIVE_PIN', '7816423507');
define ('INCOMM_USED_PIN', '7854033738');
define ('INCOMM_INACTIVE_PIN', '7264592830');
define ('INCOMM_INVALID_PIN', '7264592833');
// special dev pin: 9999889999

?>
