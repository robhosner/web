<?php
  
/**
 * MvneTest: verify MVNE property
 */

class MvneTest
{

  private $url = NULL;
  private $mvne = NULL;
  private $caller = NULL;
  private $partner_tag = NULL;

  /*
   * constructor
   *
   * @param: url - API URL to POST
   * @param: caller - class that called us
   * @param: mvne - expected MVNE (1|2)
   *
   */
  public function __construct( $url, $caller )
  {
    $this->url = $url;
    $this->caller = $caller;
    $this->partner_tag = __CLASS__ . time();
  }

  /*
   * apiTest
   *
   * call API and check executuion without testing the result
   *
   * @param: API parameters array
   *
   */
  private function apiTest( $params )
  {
    $json_result = curl_post( $this->url, $params, $this->caller->curl_options, NULL, 240 );
    echo "\nURL: {$this->url}";
    echo "\nJSON: $json_result: \n";
    $decoded_response = json_decode( $json_result );
    $this->caller->assertNotEmpty( $json_result );
    $this->caller->assertNotEmpty( $decoded_response );
    return $decoded_response;
  }

  /*
   * validIccidTest
   *
   * test that given ICCID corresponds to MVNE
   *
   * @param: API parameters array
   *
   */
  public function validIccidTest( $params )
  {
    $response = $this->apiTest( $params );
    $this->caller->assertNotContains('ERR_API_INVALID_ARGUMENTS: cannot perform operation on given ICCID',
    	$response->errors);
  }

  /*
   * invalidIccidTest
   *
   * test that given ICCID does not corresponds to MVNE
   *
   * @param: API parameters array
   *
   */
  public function invalidIccidTest( $params )
  {
    $response = $this->apiTest( $params );
    $this->caller->assertTrue( ! $response->success );
    $this->caller->assertContains('ERR_API_INVALID_ARGUMENTS: cannot perform operation on given ICCID',
    	$response->errors);
  }
}

?>