<?php

require_once 'PHPUnit/Autoload.php';
require_once((getenv('HTT_CONFIGROOT') . '/e.php'));
require_once('web.php');
require_once('test/api/test_api_include.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

/**
 * Tests for 'promotional' partner APIs
 */
class PromotionalTest extends PHPUnit_Framework_TestCase
{
  /**
   * The setUp() and tearDown() template methods are run once for each test method (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/promotional/1/ultra/api/promotional__';
  
    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';
  
    // options for our curl commands
    $this->curl_options = array(
        CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }
  
  /**
   ***********************************************
   * Test for promotional__PreparePromoCustomers *
   ***********************************************
   */
  public function test__promotional__PreparePromoCustomers ()
  {
    $url = $this->base_url . 'PreparePromoCustomers';
    
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    
    $defaultTester->runDefaultTests();


    // test promotional plan not found
    //////////////////////////////////
    $params = array( 'promo_id' => 99999 );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue(   ! $decoded_response->success );
    $this->assertEquals(   $decoded_response->errors[0] , "ERR_API_INVALID_ARGUMENTS: promotional plan not found" );


    // test success
    ///////////////

/*
    $params = array(
      'promo_id'      => 3,
      'prepare_count' => 2
    );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

print_r($decoded_response);
*/

  }
  
  /**
   ******************************************************
   * Test for promotional__ListPromotionsForPreparation *
   ******************************************************
   */
  public function test__promotional__ListPromotionsForPreparation ()
  {
    $url = $this->base_url . 'ListPromotionsForPreparation';
    
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    
    $defaultTester->runDefaultTests();


    // test success : assume there are rows in [ULTRA].[PROMOTIONAL_PLANS]
    $params = array();

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! ! $decoded_response->success );
    $this->assertNotEmpty( $decoded_response->promotions );
    $this->assertTrue( ! ! is_array( $decoded_response->promotions ) );
    $this->assertTrue( ! ! count( $decoded_response->promotions ) );
  }
  
  /**
   *****************************************************
   * Test for promotional__ActivatePromotionalCustomer *
   *****************************************************
   */
  public function test__promotional__ActivatePromotionalCustomer ()
  {
    $url = $this->base_url . 'ActivatePromotionalCustomer';
    
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    
    $defaultTester->runDefaultTests();


    // test customer in wrong state
    ///////////////////////////////
    $params = array( 'customer_id' => 31 );

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_INVALID_ARGUMENTS: customer is not in state 'Promo Unused'" );
  }
}

?>
