<?php

require_once 'classes/PHPUnitBase.php';

/**
 * base class for Dealer Portal API PHPUnit testing
 * meant to be included in API PHPUnit files
 */

class DealerPortalPHPUnit extends PHPUnitBase
{

  /**
   * login user, assert success and return security token
   * @param String username
   * @param String password
   * @returns String security token or NULL on failure
   */
  protected function createSecurityToken($username, $password, $role = NULL)
  {
    $this->assertNotEmpty($username);
    $this->assertNotEmpty($password);
    $this->setOptions(array('api' => 'dealerportal__LoginUser', 'version' => 2, 'partner' => 'dealerportal'));
    $result = $this->callApi(array('username' => $username, 'password' => $password));
    $this->assertTrue($result->success);
    $this->assertNotEmpty($result->security_token);
    if ($role)
      $this->assertEquals($role, "{$result->business_type} {$result->access_role}");
    return $result->security_token;
  }


  /**
   * locate a user based on given role, login user, assert success and return security token
   * @param String role (composed of Organization and Access role: MVNO|Master|SubDistributor|Dealer + Owner|Manager|Employee)
   * @returns String security token or NULL on failure
   */
  protected function createRoleSecurityToken($role)
  {
    switch ($role)
    {
      case 'MVNO Owner':
        return $this->createSecurityToken('UVNVO', '123456', $role);
      case 'MVNO Manager':
        return $this->createSecurityToken('xelasfx', 'suffix87', $role);
      case 'MVNO Employee':
        return $this->createSecurityToken('rgonzales','ryan123', $role);
      case 'Master Owner':
        return $this->createSecurityToken('UVMaster','123456', $role);
      case 'Master Manager':
        return $this->createSecurityToken('rontraum','776cel2114', $role);
      case 'Master Employee':
        return $this->createSecurityToken('alexmaster','suffix78', $role);
      case 'SubDistributor Owner':
        return $this->createSecurityToken('UVSubDis','123456', $role);
      case 'SubDistributor Manager':
        return $this->createSecurityToken('DMagic','Magic2012', $role);
      case 'SubDistributor Employee':
        return $this->createSecurityToken('cellspa','cell1234', $role);
      case 'Dealer Owner':
        return $this->createSecurityToken('10FrKaf','qhwgrh4u', $role);
      case 'Dealer Manager':
        return $this->createSecurityToken('benli','window98', $role);
      case 'Dealer Employee':
        return $this->createSecurityToken('andrea106','amiyah', $role);
      default:
        $this->assertFalse(TRUE, "Unknown role $role");
    }
  }

} // of class
