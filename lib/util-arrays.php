<?php

/**
 * various utility functions for working with arrays
 */


/**
 * findObjectByProperty
 * search array of objects and return the first one that has one or more properties with specific values
 * @param Array of Objects
 * @param Array (property => value)
 * @returns Object or NULL if not found
 */
function findObjectByProperty($haystack, $needles)
{
  $count = count($needles);
  foreach ($haystack as $object)
  {
    $matched = 0;
    foreach ($needles as $property => $value)
      if (isset($object->$property) && $object->$property == $value)
        $matched++;

    if ($count == $matched)
      return $object;
  }

  return NULL;
}


/**
 * collectPropertyValues
 * collect all values of a specific property in a 3D array; missing properties will be set to NULL
 * e.g.:
 *   $data = array(array('name' => 'apple', 'cost' => 1), array('name' => 'orange', 'cost' => 0.5));
 *   collectPropertyValues($data, 'name') -> array('apple', 'orange')
 * @param Array of Objects or associative arrays
 * @param String property
 * @returns Array of collected values
 */
function collectPropertyValues($items, $property)
{
  $result = array();
  foreach ($items as $item)
    if (is_object($item))
      $result[] = isset($item->$property) ? $item->$property : NULL;
    elseif (is_array($item))
      $result[] = isset($item[$property]) ? $item[$property] : NULL;
    else
      logError('wrong type given: ' . gettype($item));

  return $result;
}

