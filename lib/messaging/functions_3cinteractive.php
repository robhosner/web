<?php


// module to interface with 3cinteractive APIs
// mostly obsolete


include_once('lib/util-common.php');
include_once('core.php');
include_once('web.php');


function get_three_cinteractive_trigger_id()
{
  #return '122900'; # obtained on 9/28/2012 for testing purposes
  #return '123806'; #final - RFK Oct 24
  # temporary testing short code: 35047
  return '125009'; # Jan 20 2013
}


function get_three_cinteractive_credentials()
{
  return array(
    'username'  => find_credential('sms/3ci/user'),
    'password'  => find_credential('sms/3ci/password'),
    'clientid'  => find_credential('sms/3ci/clientid'),
    'shortcode' => find_credential('sms/3ci/shortcode')
  );
}


// OBSOLETE MVNE1
function three_cinteractive_broadcast($params)
{
  # http://docs.3cinteractive.com/Broadcast_API

  $result = array( 'errors'=>array() );

  $url = 'https://platform.3cinteractive.com/api/broadcast.php';

  $credentials = get_three_cinteractive_credentials();

  $params = array(
    'username'       => $credentials['username'],
    'password'       => $credentials['password'],
    'broadcast_name' => $params['broadcast_name'],
    'short_code'     => $credentials['shortcode'],
    'country_code'   => $params['country_code'],
    'groups'         => $params['groups'],
    'message'        => $params['message']
  );

  $result = three_cinteractive_curl(
    array(
      'url'    => $url,
      'params' => $params
    )
  );

  return $result;
}


// OBSOLETE MVNE1
function three_cinteractive_send_message($params)
{
  # http://docs.3cinteractive.com/Message_API

  dlog('',"three_cinteractive_send_message params = ".json_encode($params));

  $result = array( 'errors'=>array() );

  $url = 'https://platform.3cinteractive.com/api/send_message.php';

  $credentials = get_three_cinteractive_credentials();

  $phone_number = '';

  if ( isset($params['msisdn']) )
  { $phone_number = $params['msisdn']; }

  if ( isset($params['phone_number']) )
  { $phone_number = $params['phone_number']; }

  $params = array(
    'username' => $credentials['username'],
    'password' => $credentials['password'],
    'message'  => $params['message'],
    'phone_number' => $phone_number,
    'trigger_id'   => get_three_cinteractive_trigger_id(),
    'client_tag'   => $params['client_tag'],
    'carrier_id'   => 28 # T-Mobile
  );

  dlog('',"three_cinteractive_send_message: Sending SMS ({$params['message']}) to $phone_number.");

  $result = three_cinteractive_curl(
    array(
      'url'    => $url,
      'params' => $params
    )
  );

  dlog('',"three_cinteractive_send_message: Finished SMS ({$params['message']}) to $phone_number.");

  dlog('',"three_cinteractive_send_message result = ".json_encode($result));

  return $result;
}


function three_cinteractive_curl($curl_params)
{
  $result = array( 'errors'=>array() );

  $urlConn = curl_init($curl_params['url']);

  $fields_string = '';

  foreach($curl_params['params'] as $key=>$value)
  {
    $fields_string .= $key.'='.$value.'&';
  }
  rtrim($fields_string, '&');

  curl_setopt($urlConn, CURLOPT_POST,           1);
  curl_setopt($urlConn, CURLOPT_HTTPHEADER,     array("Content-type", "application/x-www-form-urlencoded"));
  curl_setopt($urlConn, CURLOPT_RETURNTRANSFER, 1);

  #curl_setopt($urlConn, CURLOPT_SSL_VERIFYPEER, 0);

  curl_setopt($urlConn, CURLOPT_URL,  $curl_params['url']);
  curl_setopt($urlConn, CURLOPT_POST, count( $curl_params['params'] ));
  curl_setopt($urlConn, CURLOPT_POSTFIELDS, $fields_string);

  $curl_result = curl_exec($urlConn);

/*
<?xml version="1.0" encoding="ISO-8859-1"?>
<errorNotification>
	<message>Message too long</message>
</errorNotification>
*/

  if (empty($curl_result))
  {
    $result['errors'][] = curl_error($urlConn);
  }
  else
  {
    $result = parse_result_three_cinteractive_curl($curl_result);
  }

  return $result;
}


function parse_result_three_cinteractive_curl($curl_result)
{
  $result = array(
    'errors'  => array(),
    'result'  => $curl_result,
    'success' => FALSE
  );

  $result['xml_result_as_object'] = simplexml_load_string($curl_result);

  if ( $result['xml_result_as_object'] )
  {
    preg_match('/errorNotification/', $curl_result, $matches);

    if ( $matches )
    {
      foreach( $result['xml_result_as_object']->message as $error_message )
      {
        $result['errors'][] = (string)$error_message;
      }
    }
    else
    {
      preg_match('/successNotification/', $curl_result, $matches);

      if ( $matches )
      {
        $result['success'] = TRUE;
      }
      else
      {
        foreach( $result['xml_result_as_object']->message as $error_message )
        {
          $result['errors'][] = (string)$error_message;
        }
      }
    }
  }
  else
  {
    $result['errors'][] = 'Cannot parse API output';
  }

  return $result;
}

