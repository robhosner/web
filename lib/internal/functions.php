<?php


include_once("db.php");
include_once("db/htt_action_log.php");
include_once("session.php");
require_once('Ultra/Lib/Util/Redis.php');
require_once('Ultra/Lib/DB/ActionLog.php');


# synopsis

#func_create_temp_password(593);
#echo func_read_temp_password(593)."\n";
#func_unset_temp_password(593);
#echo func_read_temp_password(593)."\n";

/**
 * func_create_temp_password
 *
 * Stores temporary password for customer_id in redis
 * 
 * @param  integer $customer_id
 * @param  string  $token
 * @param  integer $ttl
 * @return string $password
 */
function func_create_temp_password($customer_id, $token='temp_password', $ttl=900)
{
  $password = func_generate_temp_password();

  $temp_password_key = $token.'/'.$customer_id;

  $redis = new \Ultra\Lib\Util\Redis;

  $redis->set( $temp_password_key , $password , $ttl );

  return $password;
}

/**
 * func_unset_temp_password
 *
 * Deletes temporary password for customer_id in redis
 * 
 * @param integer $customer_id
 * @param string $token
 */
function func_unset_temp_password($customer_id, $token='temp_password')
{
  $temp_password_key = $token.'/'.$customer_id;

  $redis = new \Ultra\Lib\Util\Redis;

  $redis->del($temp_password_key);
}

/**
 * func_read_temp_password
 *
 * Returns temporary password for customer_id from redis
 * 
 * @param  integer $customer_id
 * @param  string $token
 * @return string password
 */
function func_read_temp_password($customer_id, $token='temp_password')
{
  $temp_password_key = $token.'/'.$customer_id;

  $redis = new \Ultra\Lib\Util\Redis;

  return $redis->get( $temp_password_key );
}

/**
 * func_generate_temp_password
 *
 * returns a password [A-Z]{6}[0-9]{2}
 *
 * @return string $password
 */
function func_generate_temp_password()
{
  $password = '';

  $character_range = range(65,90);

  foreach(range(0,5) as $i)
  {
    $random_pick = mt_rand(1,count($character_range));
    $password .= chr($character_range[$random_pick-1]);
  }

  $character_range = range(48,57);

  foreach(range(0,1) as $i)
  {
    $random_pick = mt_rand(1,count($character_range));
    $password .= chr($character_range[$random_pick-1]);
  }

  return strtolower( $password );
}

/**
 * func_get_transition_failure_reason
 *
 * return a text string of the reason of a transition failure (MVNO-529)
 * 
 * @param  string $request_id
 * @return string $transition_failure_reason
 */
function func_get_transition_failure_reason( $transition_uuid )
{
  $transition_failure_reason = '';

  $query_result = \Ultra\Lib\DB\ActionLog\selectActionLog($transition_uuid, 'ABORTED');
  
  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    $transition_failure_reason = $query_result[0][0];
  }

  return $transition_failure_reason;
}

?>
