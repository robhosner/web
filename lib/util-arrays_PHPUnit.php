<?php

require_once 'lib/util-arrays.php';

class FindObjectByPropertyTest extends PHPUnit_Framework_TestCase
{
  function test_findObjectByProperty()
  {
    $haystack = array(
      (object)array('position' => 1, 'one' => 1, 'two' => 2),
      (object)array('position' => 2, 'one' => 1, 'two' => 3),
      (object)array('position' => 3, 'one' => 6, 'two' => 2));

    // find by single propery
    $result = findObjectByProperty($haystack, array('one' => 1));
    $this->assertNotEmpty($result);
    $this->assertEquals($result->position, 1);

    // find by single propery
    $result = findObjectByProperty($haystack, array('one' => 6));
    $this->assertNotEmpty($result);
    $this->assertEquals($result->position, 3);

    // property not found
    $result = findObjectByProperty($haystack, array('zero' => 1));
    $this->assertEmpty($result);

    // value not found
    $result = findObjectByProperty($haystack, array('one' => 4));
    $this->assertEmpty($result);

    // find by multiple properties
    $result = findObjectByProperty($haystack, array('one' => 1, 'two' => 3));
    $this->assertNotEmpty($result);
    $this->assertEquals($result->position, 2);

    // one property not found
    $result = findObjectByProperty($haystack, array('one' => 1, 'ten' => 3));
    $this->assertEmpty($result);

    // one value not found
    $result = findObjectByProperty($haystack, array('one' => 6, 'two' => 3));
    $this->assertEmpty($result);
  }

  function test_collectPropertyValues()
  {
    $data = array(
      array('name' => 'apple', 'retail' => 1.1, 'wholesale' => 0.5),
      array('name' => 'orange', 'retail' => 1.5, 'wholesale' => 1),
      array('name' => 'kiwi', 'retail' => 0.5, 'wholesale' => 0.2));

    $retail = collectPropertyValues($data, 'retail');
    print_r($retail);
    $this->assertEquals(count($retail), 3);
    $this->assertEquals(array_sum($retail), 3.1);
    $wholesale = collectPropertyValues($data, 'wholesale');
    print_r($wholesale);
    $this->assertEquals(count($wholesale), 3);
    $this->assertEquals(array_sum($wholesale), 1.7);
  }


}
