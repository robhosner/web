<?php

include_once('core.php');

include_once(__DIR__ . '/lib/util-common.php');

include_once('Ultra/Lib/Util/Redis.php');

use Ultra\Lib\Util\Redis as RedisUtils;

// we don't set this prefixed based upon $REQUEST HOSTNAME, because
// several hostnames could share a session.

#$mc_prefix = $e_config['mc/prefix'];
#$mc_server = mc_server();
#$mc_port   = $e_config['mc/port'];
$mc = NULL;

function mc_server()
{
  global $e_config;

  $mc_server = $e_config['mc/server'];

  if ( $mc_server == 'localhost' )
  {
    $mc_server = '127.0.0.1';
  }

  return $mc_server;
}

function mc_increment($key, $offset=1)
{
  global $mc;
  global $mc_prefix;
  global $mc_server;
  global $mc_port;

  if ($mc == NULL)
    $mc = memcache_connect($mc_server, $mc_port);

  $ret = memcache_increment($mc, $mc_prefix . $key, $offset);

  logged_mssql_query("MC_INCR '$mc_prefix$key' += $offset", TRUE);

  return $ret;
}

function mc_write($key, $val, $exp=0)
{
  global $mc;
  global $mc_prefix;
  global $mc_server;
  global $mc_port;

  mc_connect();

  $ret = memcache_set($mc, $mc_prefix . $key, $val, 0, $exp);

  logged_mssql_query("MC_WRITE '$mc_prefix$key' = '$val', exp=$exp", TRUE);

  return $ret;
}

function mc_delete($key)
{
  global $mc;
  global $mc_prefix;
  global $mc_server;
  global $mc_port;

  mc_connect();

  memcache_delete($mc, $mc_prefix . $key);

  logged_mssql_query("MC_DELETE '$mc_prefix$key'", TRUE);

  return TRUE;
}

function mc_read($key)
{
  global $mc;
  global $mc_prefix;
  global $mc_server;
  global $mc_port;

  mc_connect();

  $ret = memcache_get($mc, $mc_prefix . $key);

  logged_mssql_query("MC_READ '$mc_prefix$key' = '$ret'", TRUE);

  return $ret;
}

function mc_connect()
{
  global $mc;
  global $e_config;

  if ($mc == NULL)
    $mc = memcache_connect($e_config['mc/server'], $e_config['mc/port']);

}

/**
 * session_save
 *
 * zsessions are used by the front end API calls for services.ultra.me as user credentials
 * when someone logs in a zsession is made, correlating a UUID with a customer_id
 */
function session_save($customer, $session_id=NULL, $redis=NULL)
{
  global $out;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  # create UUID
  if (!$session_id) $session_id = substr(create_guid('PHP SESSION'), 1, -1);

  # this is not CUSTOMER_ID
  $id = $customer->CUSTOMER;

  dlog('session', 'CUSTOMER ID = %s', $id);

  $minutes = 30;

  # TTL
  $ttl = $minutes * 60; // redis expiration in seconds (from now)
  $exp = time() + $ttl; // cookie expiration is UNIX Epoch

  # associate UUID with $customer->CUSTOMER ; associate expiration as well
  $redis->set($session_id, $id, $ttl);
  $redis->set("$session_id expiration", $exp, $ttl);

  dlog('session', 'refreshed the zsession to %s, exp %s', $session_id, $exp);

  $session_value = $redis->get( $session_id );

  dlog('session', 'session id from redis : %s', $session_value);

  # sanity check
  if ( $session_value === $id )
    return $session_id;

  # something went wrong
  return NULL;
}

/**
 * session_read
 *
 * Returns the session data
 *
 * @return array
 */
function session_read($session_id, $redis=NULL)
{
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $bypass = 0;
  if (isset($_COOKIE['bypass_claim_account']) && $_COOKIE['bypass_claim_account']!='') $bypass = $_COOKIE['bypass_claim_account'];

  return array(
    $redis->get( $session_id ),
    $redis->get( "$session_id expiration" ),
    $session_id,
    $bypass
  );
}

/**
 * verify_session
 *
 * Verity session data ; returns NULL in case of failure
 *
 * @return array
 */
function verify_session($param_customer=NULL)
{
  global $out;

  if ( isset($param_customer) )
    $out['customer'] = $param_customer;

  if ( !isset($out['customer']) )
    $out['customer'] = NULL;

  dlog('session', '$out %s', $out);
  dlog('session', 'request %s', $_REQUEST);

  $zsession = array_key_exists("zsession", $_REQUEST) ? $_REQUEST['zsession'] : '';
  $c = array_key_exists("customer", $_REQUEST) ? $_REQUEST['customer'] : $out['customer'];

  dlog('session', '$c = %s', $c);

  # load customer data from DB
  $customer = find_customer(make_find_customer_query_anycosid(is_numeric($c) ? $c : -1,
                                                              is_numeric($c) ? NULL : $c));

  $check_session = session_read($zsession);

  dlog('session', 'check_session = %s', $check_session);
  dlog('session', 'customer      = %s', $customer);
  dlog('session', 'zsession      = %s', $zsession);

  # verify customer data with session data
  if ($customer &&
      is_array($check_session) &&
      $customer->CUSTOMER === $check_session[0] &&
      $zsession === $check_session[2])
  {
    return $check_session;
  }

  dlog('session', 'returning NULL :(');

  # something went wrong
  return NULL;
}

// close the Memcached connection
function mc_close()
{
  global $mc;
  if ($mc != NULL) memcache_close($mc);
}

// public domain from http://www.php.net/manual/en/function.setcookie.php
function setcookielive($name, $value='', $expire=0, $path='', $domain='', $secure=false, $httponly=false)
{
  //set a cookie as usual, but ALSO add it to $_COOKIE so the current page load has access
  $_COOKIE[$name] = $value;
  return setcookie($name,$value,$expire,$path,$domain,$secure,$httponly);
}

/**
 * @see MVNO-2288: moved the entire security secion (5 following functions) from partner-validate.php
 */
function verify_request_source()
{
  $ip = getip();
return TRUE;
  /* we don't use FALSE because (sigh) it's NULL */
  $deny = check_ip_rules('deny', -1, $ip, 'ip/deny');
  if (NULL != $deny) return FALSE;

  $allow = check_ip_rules('allow', TRUE, $ip, 'ip/allow');
  if (NULL != $allow) return $allow;

  return FALSE;
}

function getip()
{
  if (isset($_SERVER["HTTP_CLIENT_IP"]) && validip($_SERVER["HTTP_CLIENT_IP"]))
  {
    return $_SERVER["HTTP_CLIENT_IP"];
  }

  if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
  {
    foreach (explode(",",$_SERVER["HTTP_X_FORWARDED_FOR"]) as $ip)
    {
      if (validip(trim($ip)))
      {
        return $ip;
      }
    }
  }

  foreach (array("HTTP_X_FORWARDED", "HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_X_FORWARDED") as $header)
  {
    if (isset($_SERVER[$header]) && validip($_SERVER[$header])) return $_SERVER[$header];
  }

  return $_SERVER["REMOTE_ADDR"];
}

function validip($ip)
{
  $reserved_ips = array (
    array('0.0.0.0','2.255.255.255'),
    array('10.0.0.0','10.255.255.255'),
    array('127.0.0.0','127.255.255.255'),
    array('169.254.0.0','169.254.255.255'),
    array('172.16.0.0','172.31.255.255'),
    array('192.0.2.0','192.0.2.255'),
    array('192.168.0.0','192.168.255.255'),
    array('255.255.255.0','255.255.255.255')
  );

  if (!empty($ip) && ip2long($ip)!=-1)
  {
    foreach ($reserved_ips as $r)
    {
      $min = ip2long($r[0]);
      $max = ip2long($r[1]);
      if ((ip2long($ip) >= $min) && (ip2long($ip) <= $max)) return false;
    }

    return true;
  }
  else
  {
    return false;
  }
}

function check_ip_rules($mode, $onmatch, $ip, $rules)
{
  $sec = find_security($rules);

  #dlog('',
  #     "verify_request_source(): IP [$ip], $mode = %s",
  #     json_encode($sec));

  if ( ! is_null($sec) )
  {
    foreach ($sec as $rule)
    {
      #dlog('security',
      #     "verify_request_source(): IP [$ip], $mode rule = %s",
      #     $rule);

      if (preg_match("/\//", $rule))
      {
        if (ipCIDRCheck($ip, $rule))
        {
          #dlog('security',
          #     "verify_request_source(): IP [$ip] MATCHED NETMASK $mode rule = %s",
          #     $rule);

          return $onmatch;
        }
      }
      else
      {
        if (preg_match("/$rule/", $ip))
        {
          #dlog('security',
          #     "verify_request_source(): IP [$ip] MATCHED $mode rule = %s",
          #     $rule);

          return $onmatch;
        }
      }
    }
  }

  return NULL;
}

function ipCIDRCheck ($IP, $CIDR)
{
  list ($net, $mask) = explode('/', $CIDR);

  $ip_net = ip2long ($net);
  $ip_mask = ~((1 << (32 - $mask)) - 1);

  $ip_ip = ip2long ($IP);

  $ip_ip_net = $ip_ip & $ip_mask;

  return ($ip_ip_net == $ip_net);
}


/**
 * isIpAddressInArray
 * check if given IP address is in the given array
 * @param String IP address, typically obtained from Sesison:getClientIp()
 * @param Array list of individual IP addresses or ranges in CIDR notation, typically obtained from explode(' ', find_credential($key))
 * @return Boolean TRUE if found, FALSE otherwise
 */
function isIpAddressInArray($ip, $list)
{
  foreach ($list as $target)
    if ((strstr($target, '/') && ipCIDRCheck ($ip, $target)) || $ip == $target)
      return TRUE;

  return FALSE;
}

 /**
 * createCustomerSession
 *
 * login a customer via portal__Login API and return a newly created zsession
 * this function is for testing in development environment only since it uses dev authentication
 *
 * @param string username: CUSTOMERS.LOGIN_NAME
 * @param string password: CUSTOMERS.LOGIN_PASSWORD
 * @return string zsession or NULL on failure
 */
function createCustomerSession($username, $password)
{
  // credentials
  $curl_options = array(CURLOPT_USERPWD  => TEST_APACHE_USER . ':' . TEST_APACHE_PASS, CURLOPT_HTTPAUTH => CURLAUTH_BASIC);

  // create a zsession (v1 API) that we will need for later tests
  $params = array('account_login' => $username, 'account_password' => $password);
  $url = find_site_url() . '/pr/portal/1/ultra/api/portal__Login';
  $json_result = curl_post($url, $params, $curl_options, NULL, 240);
  $decoded_response = json_decode($json_result);
  if ( empty($decoded_response) || ! $decoded_response->success )
  {
    dlog('', 'ERROR: ' . print_r($json_result, TRUE));
    return NULL;
  }

  return $decoded_response->zsession;
}

/**
 * getCustomerFromSession
 *
 * verify zsession and return customer object corresponding to it
 * requires existing database connection
 *
 * @param string zsession
 * @param string method: name of the function to get customer object with
 * @return object customer or NULL on failure
 */
function getCustomerFromSession($zsession, $method = 'get_customer_from_customer')
{
  if (empty($zsession))
    return NULL;

  // check zsession
  $check = session_read($zsession);
  if (empty($check[0]))
  {
    dlog('', 'ERROR: session_read returned invalid data');
    return NULL;
  }

  // get customer object from CUSTOMERS.CUSTOMER
  if (is_callable($method))
    $customer = $method($check[0]);
  else
  {
    dlog('', "ERROR: function $method does not exist");
    return NULL;
  }
 
  if (! $customer)
  {
    dlog('', 'ERROR: failed to get customer from customer_id ' . $check[0]);
    return NULL;
  }

  dlog('', "INFO: zsession '$zsession' -> customer ID '{$customer->CUSTOMER_ID}'");
  return $customer;
}

/**
 * checkApiAbuseByIdentifier
 *
 * check if API has been called more than the given number per hour.
 *
 * @param String API name
 * @param string $id example: customer id, username, msisdn, ip
 * @param Number max count per hour
 * @param Object redis
 * @param Boolean increment: increment API usage if TRUE, do not increment if FALSE (useful for monitoring failed API calls only)
 * @param Boolean do not check but reset count instead
 * @return boolean TRUE if count has been exceeded or FALSE otherwise
 */
function checkApiAbuseByIdentifier($api, $id, $max, $redis = NULL, $increment = TRUE, $reset = FALSE, $skipInDev = true)
{
  // do not block in development
  if (\Ultra\UltraConfig\isDevelopmentDB() && $skipInDev)
  {
    return FALSE;
  }

  if ( ! $redis)
  {
    $redis = new \Ultra\Lib\Util\Redis;
  }

  $command = "$api/$id";
  if ($reset)
  {
    RedisUtils\redis_reset_command_count($redis, $command);
    return FALSE;
  }

  // increment existing count
  if ($increment)
  {
    RedisUtils\redis_increment_command_count_with_max($redis, $command, $max);
  }

  if (checkIfUserIsBlockedFromApi($api, $id, $max, $redis))
  {
    return true;
  }

  return FALSE;
}

/**
 * checkApiAbuse
 *
 * Check if API has been called more than the given number per hour
 * Useful for potentially dangerous APIs
 *
 * @param string API name
 * @param number max count per hour
 * @param object redis
 * @return boolean TRUE if count has been exceeded, FALSE otherwise
 */
function checkApiAbuse($api, $max, $redis=NULL, $errorMessage=NULL)
{
  // do not block APIs in development
  if (\Ultra\UltraConfig\isDevelopmentDB())
    return FALSE;

  if (! $redis)
    $redis = new \Ultra\Lib\Util\Redis;

  // increment existing count
  RedisUtils\redis_increment_command_count_by_hour($redis, $api);

  // check if current count has been exceeded
  $count = RedisUtils\redis_get_command_count_by_hour($redis, $api);

  if ($count > $max)
  {
    if ( empty($errorMessage) )
      $errorMessage = "WARNING: $api has been executed $count times - max is $max";

    dlog('', $errorMessage);

    return TRUE;
  }

  return FALSE;
}

/**
 * Check if the username is currently blocked.
 *
 * @param $api
 * @param $username
 * @param $max
 * @param Ultra\Lib\Util\Redis $redis
 * @return bool
 */
function checkIfUserIsBlockedFromApi($api, $username, $max, Ultra\Lib\Util\Redis $redis)
{
  $key = "command/count/$api/$username";

  $ttl = $redis->ttl($key);
  $count = $redis->get($key);

  if ($ttl > 0 && $count > $max)
  {
    dlog('', "WARNING: user: $username is blocked from api: $api for too many invalid attempts to login. User: $username will be unblocked in $ttl seconds.");
    return true;
  }

  return false;
}

