<?php

include_once("session.php");
include_once("db.php");

function sms_delivery_limit_reached( $msisdn )
{
  // maximum of 50 SMS per day

  $sms_delivery_limit_reached = FALSE;

  $max_sms_delivery_limit = 50;

  $todays_sms_count_key = "sms_count/".date("d.m.y")."/".$msisdn;

  // use memcached for the count.
  $count_sms_delivery = mc_read($todays_sms_count_key);

  if ( $count_sms_delivery >= $max_sms_delivery_limit )
  {
    $sms_delivery_limit_reached = TRUE;
    record_sms_delivery_limit_reached($count_sms_delivery);
  }

  return $sms_delivery_limit_reached;
}

function record_sms_delivery_limit_reached($count_sms_delivery)
{
  if ( should_audit_sms_count($count_sms_delivery) )
  {
    global $customer_full;

    // store in HTT_AUDITLOG only after count reaches a series of thresholds
    fraud_event($customer_full, 'sms', 'send', 'block',$data);
  }
}

function should_audit_sms_count($count_sms_delivery)
{
  # this series begins with the following values:
  # 5 10 20 40 80 160 320 640 1280 2560 5120 ...

  $threshold = 5;
  $found = FALSE;

  while(
    ( $count_sms_delivery >= $threshold )
    &&
    ( ! $found )
  )
  {
    if ( $count_sms_delivery == $threshold )
    {
      $found = TRUE;
    }
    $threshold *= 2;
  }

  return $found;
}

function send_sms($data)
{
  global $out;

  $sms_credentials = get_silverstreet_credentials();

  $todo = array();

  if (
    ( ! $data['homeboy'] )
    &&
    ( sms_delivery_limit_reached( $out['customer'] ) )
  )
  {
    $out['errors'][] = "You have reached your daily SMS limit, the service will be open for you again tomorrow.";
    $error_dump = array_merge(array(), $data);
    unset($error_dump['sql_log']); // we don't need to log this
    dlog("", "SMS DAILY DELIVERY LIMIT REACHED ".json_encode($error_dump));
    return array("--SMS DAILY DELIVERY LIMIT REACHED");
  }

  if (!array_key_exists("sms_text", $data) || !$data['sms_text'])
  {
    $out['errors'][] = "Enter SMS text, please.";
    return array("--SMS TEXT IS REQUIRED");
  }

  if (!array_key_exists("sms_dest", $data) || !$data['sms_dest'])
  {
    $out['errors'][] = "Enter a destination number, please.";
    return array("--SMS DEST IS REQUIRED");
  }

  if (strlen($data['sms_text']) > 160)
  {
    $todo[] = "--SMS TEXT WAS TRIMMED TO 160 from " . strlen($data['sms_text']);
    $data['sms_text'] = substr($data['sms_text'], 0, 159);
  }

  // the destination should start with 91 and have 12 digits, OR be in the whitelist
  switch($data['sms_dest'])
  {
  case '13125506663':
    break;
  default:
    if (strlen($data['sms_dest']) == 10)
    {
      $todo[] = "--SMS DEST got 91 prepended";
      $data['sms_dest'] = "91$data[sms_dest]";
    }

    if (!preg_match('/^91\d{10}$/', $data['sms_dest']))
    {
      return array("--INVALID SMS DEST: must start with 91 and be 12 digits, but is [$data[sms_dest]]");
    }
    break;
  }

  $ret = curl_get($sms_credentials['url'],
                  array(
                    'USERNAME' => $sms_credentials['user'],
                    'PASSWORD' => $sms_credentials['password'],
                    'DESTINATION' => $data['sms_dest'],
                    'SENDER' => 'IndiaLD',
                    'BODY' => $data['sms_text'],
                    'SERVICE' => 'ILD SMS',
                    'DLR' => 0,
                    ));

  if (TRUE) // TODO: this is a placeholder for a check on what constitutes a success for the $ret value
  {
    record_successful_sms( $out['customer'] );
  }

  $todo[] = "--sms_back=$ret";
  $out['sms_back'] = $ret;

  return $todo;
}

function send_ultra_password_sms($data)
{
  global $out;

  $sms_credentials = get_silverstreet_credentials();

  $todo = array();

  if (
    ( ! $data['homeboy'] )
    &&
    ( sms_delivery_limit_reached( $out['customer'] ) )
  )
  {
    $out['errors'][] = "You have reached your daily SMS limit, the service will be open for you again tomorrow.";
    $error_dump = array_merge(array(), $data);
    unset($error_dump['sql_log']); // we don't need to log this
    dlog("", "SMS DAILY DELIVERY LIMIT REACHED ".json_encode($error_dump));
    return array("--SMS DAILY DELIVERY LIMIT REACHED");
  }

  if (!array_key_exists("sms_text", $data) || !$data['sms_text'])
  {
    $out['errors'][] = "Enter SMS text, please.";
    return array("--SMS TEXT IS REQUIRED");
  }

  if (!array_key_exists("sms_dest", $data) || !$data['sms_dest'])
  {
    $out['errors'][] = "Enter a destination number, please.";
    return array("--SMS DEST IS REQUIRED");
  }

  if (strlen($data['sms_text']) > 160)
  {
    $todo[] = "--SMS TEXT WAS TRIMMED TO 160 from " . strlen($data['sms_text']);
    $data['sms_text'] = substr($data['sms_text'], 0, 159);
  }

  $ret = curl_get($sms_credentials['url'],
                  array(
                    'USERNAME' => $sms_credentials['user'],
                    'PASSWORD' => $sms_credentials['password'],
                    'DESTINATION' => $data['sms_dest'],
                    'SENDER' => 'IndiaLD',
                    'BODY' => $data['sms_text'],
                    'SERVICE' => 'ILD SMS',
                    'DLR' => 0,
                    ));

  if (TRUE) // TODO: this is a placeholder for a check on what constitutes a success for the $ret value
  {
    record_successful_sms( $out['customer'] );
  }

  $todo[] = "--sms_back=$ret";
  $out['sms_back'] = $ret;

  $out['sms_text'] = '';
  $out['sms_dest'] = '';
  $out['msisdn'] = '';

return $todo;
}

function send_silverstreet_sms($params)
{
  $sms_dest = $params['sms_dest'];
  $sender   = $params['sender'];
  $sms_text = $params['sms_text'];
  $service  = $params['service'];
  $dlr      = $params['dlr'];

  $sms_credentials = get_silverstreet_credentials();

  $ret = curl_get(
           $sms_credentials['url'],
           array(
             'USERNAME'    => $sms_credentials['user'],
             'PASSWORD'    => $sms_credentials['password'],
             'DESTINATION' => $sms_dest,
             'SENDER'      => $sender,
             'BODY'        => $sms_text,
             'SERVICE'     => $service,
             'DLR'         => $dlr,
           )
  );

  return $ret;
}

function get_silverstreet_credentials()
{
  return array(
    'user'      => find_credential('sms/silverstreet/user'),
    'url'       => find_credential('sms/silverstreet/url'),
    'password'  => find_credential('sms/silverstreet/password')
  );
}

function record_successful_sms( $msisdn )
{
  // use memcached for counting successful sms daily deliveries

  $todays_sms_count_key = "sms_count/".date("d.m.y")."/".$msisdn;

  // mc_increment does not create an item if it doesn't already exist.

  $count_sms_delivery = mc_read($todays_sms_count_key);

  if ( $count_sms_delivery )
  {
    mc_increment($todays_sms_count_key);
  }
  else
  {
    // Possible but extremely rare race condition: first 2 sms sent at the exact same time at the beginning of a new day. Oh, well ...

    mc_write($todays_sms_count_key, 1);
  }
}

?>
