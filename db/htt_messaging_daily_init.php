<?php


# HTT_MESSAGING_DAILY_INIT 


/**
 * htt_messaging_last_init 
 * @return integer $last_init_datetime
 */
function htt_messaging_last_init()
{
  $last_init_datetime = NULL;

  $sql = "SELECT datediff(hh,LAST_INIT_DATETIME,getutcdate())+1 FROM HTT_MESSAGING_DAILY_INIT";

  dlog('',$sql);

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && ( count($query_result) == 1 ) )
  {
    $last_init_datetime = $query_result[0][0];
  }

  return $last_init_datetime;
}

/**
 * htt_messaging_daily_init_update 
 * @return boolean
 */
function htt_messaging_daily_init_update()
{
  $sql = "UPDATE HTT_MESSAGING_DAILY_INIT SET LAST_INIT_DATETIME = getutcdate()";

  dlog('',$sql);

  return run_sql_and_check($sql);
}

/**
 * htt_messaging_daily_init_insert 
 * @return boolean
 */
function htt_messaging_daily_init_insert()
{
  $sql = "INSERT INTO HTT_MESSAGING_DAILY_INIT (LAST_INIT_DATETIME) VALUES ( getutcdate() )";

  dlog('',$sql);

  return run_sql_and_check($sql);
}

?>
