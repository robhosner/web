<?php

/**
 * htt_inventory_simbatches_select_query
 * 
 * Query used to select rows from htt_inventory_simbatches
 * 
 * @param  Array $attribbutes to select
 * @param  Array $clauses to check against
 * @return String query
 */
function htt_inventory_simbatches_select_query($attributes, $clauses, $limit = NULL)
{
  $sql = \Ultra\Lib\DB\makeParameterizedSelectStatement(
    'HTT_INVENTORY_SIMBATCHES',
    $limit,
    $attributes,
    $clauses
  );

  return $sql;
}

/**
 * htt_inventory_simbatches_get_by_batch_id
 * 
 * used to select row from htt_inventory_simbatches
 * 
 * @param  String $batch_id ICCID_BATCH_ID
 * @param  Array  $attributes to select
 * @return Array  $row from HTT_INVENTORY_SIMBATCHES || NULL
 */
function htt_inventory_simbatches_get_by_batch_id($batch_id, $attributes = NULL)
{
  return find_first(htt_inventory_simbatches_select_query($attributes, array('ICCID_BATCH_ID' => $batch_id)));
}
