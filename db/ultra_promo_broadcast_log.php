<?php

// [ULTRA].[PROMO_BROADCAST_LOG]

/**
 * add_to_ultra_promo_broadcast_log
 * $params: promo_broadcast_campaign_id
 *          customer_id
 *          msg_status
 *          msg_delivery_date
 *
 * @param  array $params
 * @return object of class \Result
 */
function add_to_ultra_promo_broadcast_log( $params )
{
  if ( empty( $params['customer_id'] ) )
    return make_error_Result('customer_id missing');

  if ( empty( $params['msg_status'] ) )
    return make_error_Result('msg_status missing');

  if ( empty( $params['promo_broadcast_campaign_id'] ) )
    return make_error_Result('promo_broadcast_campaign_id missing');

  if ( empty( $params['msg_delivery_date'] ) || ! $params['msg_delivery_date'] )
    $params['msg_delivery_date'] = 'NULL';
  else
    $params['msg_delivery_date'] = 'GETUTCDATE()';

  $sql = sprintf("
INSERT INTO ULTRA.PROMO_BROADCAST_LOG
(
PROMO_BROADCAST_CAMPAIGN_ID,
CUSTOMER_ID,
MSG_STATUS,
MSG_DELIVERY_DATE,
PROMO_STATUS,
PROMO_CLAIMED_DATE
)
VALUES
(
%d,
%d,
%s,
%s,
'UNCLAIMED',
NULL
) ",
    $params['promo_broadcast_campaign_id'],
    $params['customer_id'],
    mssql_escape_with_zeroes($params['msg_status']),
    $params['msg_delivery_date']
  );

  return run_sql_and_check_result($sql);
}

/**
 * get_ultra_promo_broadcast_log
 * $params: top
 *          customer_id
 *          promo_broadcast_campaign_id
 *          order_by
 *
 * @param  array $params
 * @return object of class \Result
 */
function get_ultra_promo_broadcast_log( $params )
{
  $where = array();

  $top = ( ( empty( $params['top'] ) || ! is_numeric( $params['top'] ) ) ? 1000 : $params['top'] );

  if ( ! empty( $params['customer_id'] ) )
    $where['CUSTOMER_ID']                 = $params['customer_id'];

  if ( ! empty( $params['promo_broadcast_campaign_id'] ) )
    $where['PROMO_BROADCAST_CAMPAIGN_ID'] = $params['promo_broadcast_campaign_id'];

  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'ULTRA.PROMO_BROADCAST_LOG',
    $top,
    NULL,
    $where,
    NULL,
    ( empty( $params['order_by'] ) ? NULL : $params['order_by'] )
  );

  return make_ok_Result( mssql_fetch_all_objects( logged_mssql_query( $sql ) ) );
}

/**
 * get_ultra_promo_broadcast_log_success_count
 *
 * @param  Integer promo_broadcast_campaign_id
 * @return Array [MSG_STATUS => count, ...]
 */
function get_ultra_promo_broadcast_log_success_count($promo_broadcast_campaign_id)
{
  $output = array();

  $sql = sprintf('SELECT MSG_STATUS, COUNT(*) as count FROM ULTRA.PROMO_BROADCAST_LOG with (nolock)
    WHERE promo_broadcast_campaign_id = %d
    GROUP BY MSG_STATUS',
    $promo_broadcast_campaign_id
  );

  $log = mssql_fetch_all_objects(logged_mssql_query($sql));

  foreach ($log as $entry)
    $output[$entry->MSG_STATUS] = $entry->count;

  return $output;
}

?>
