<?php

/**
 * get_plan_states 
 * @return object[]
 */
function get_plan_states()
{
  $sql = " SELECT * FROM DP_RBAC.PLAN_STATE ";

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_plan_states_by_activity 
 * @param  integer $activity_id
 * @param  string $activity_name
 * @param  integer $is_active
 * @return object[] $plan_states
 */
function get_plan_states_by_activity( $activity_id=NULL , $activity_name=NULL , $is_active=NULL )
{
  $plan_states = array();

  if ( !$activity_id && !$activity_name )
    return $plan_states;

  $where = array();

  if ( $activity_id )
    $where[] = sprintf(" a.ACTIVITY_ID   = %d ",$activity_id);

  if ( $activity_name )
    $where[] = sprintf(" a.ACTIVITY_NAME = %s ",mssql_escape_with_zeroes($activity_name));

  if ( $is_active )
    $where[] = sprintf(" pa.ACTIVE_FLAG  = %d ",$is_active);

  $sql = "
    SELECT
      a.ACTIVITY_ID,
      a.ACTIVITY_NAME,
      p.*
    FROM
      DP_RBAC.ACTIVITY a
    JOIN
      DP_RBAC.PLAN_STATE_ACTIVITY pa
    ON
      a.ACTIVITY_ID = pa.ACTIVITY_ID
    JOIN
      PLAN_STATE p
    ON
      p.PLAN_STATE_ID = pa.PLAN_STATE_ID
    WHERE
      ".implode(" AND ", $where);

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
    $plan_states = $data;

  return $plan_states;
}

?>
