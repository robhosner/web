<?php


include_once 'db.php';
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/Util/Redis/Action.php';


# php component to access DB table HTT_ACTION_LOG

# synopsis

#echo htt_action_log_insert_query(
#  array(
#    "action_uuid"     => '1',
#    "transition_uuid" => '123456abcd',
#    "status"          => 'OPEN',
#    "created"         => 'getutcdate()',
#    "pending_since"   => 'getutcdate()',
#    "closed"          => 'getutcdate()',
#    "action_seq"      => 1,
#    "action_type"     => 'test1',
#    "action_name"     => 'test2',
#    "action_result"   => 'test3'
#  )
#);

#echo htt_action_log_select_failure_reason(array('transition_uuid'=>'{2271F333-3547-E739-1674-76659DA36A9D}'))."\n\n";
#echo htt_action_log_select_failure_reason(array('action_uuid'    =>'{B3B97598-CE59-B9AA-A27D-7B220AE3538F}'))."\n\n";


/**
 * htt_action_log_select_failure_reason
 *
 * Composes a SQL query to retrieve the reason of an aborted action
 * $params: transition_uuid
 *          action_uuid
 *
 * @param  array $params
 * @return string
 */
function htt_action_log_select_failure_reason($params)
{
  if ( isset($params['transition_uuid']) && $params['transition_uuid'] )
  {
    $key_clause = sprintf(" a.TRANSITION_UUID = %s ",mssql_escape_with_zeroes($params['transition_uuid']) );
  }

  if ( isset($params['action_uuid']) && $params['action_uuid'] )
  {
    $key_clause = sprintf(" a.ACTION_UUID = %s ",mssql_escape_with_zeroes($params['action_uuid']) );
  }

  $query = "
    SELECT TOP(1) a.ACTION_RESULT
    FROM   HTT_ACTION_LOG a
    WHERE  $key_clause
    AND    a.STATUS = 'ABORTED'
    AND    a.ACTION_RESULT IS NOT NULL
    AND    a.ACTION_SEQ =
      (
      SELECT MIN(aa.ACTION_SEQ)
      FROM   HTT_ACTION_LOG aa
      WHERE  aa.TRANSITION_UUID = a.TRANSITION_UUID
      AND    aa.STATUS = 'ABORTED'
      )";

  return $query;
}

/**
 * get_hanging_actions
 *
 * Returns actions that are running for too long
 * $params: max_minutes_pending
 *
 * @param  array $params
 * @return array
 */
function get_hanging_actions($params)
{
  // Example: STATUS: RUNNING {EF0D86D0-BC0F-6245-3D99-755F3DBB0DB3} 1368015273

  $hanging_actions = NULL;

  $max_minutes_pending = ( isset($params['max_minutes_pending']) ? $params['max_minutes_pending'] : 15 );

  $query = "
    SELECT a.CREATED,
           a.PENDING_SINCE,
           a.ACTION_NAME,
           a.ACTION_UUID,
           a.TRANSITION_UUID,
           t.CUSTOMER_ID,
           DATEDIFF(mi, a.PENDING_SINCE, GETUTCDATE()) minutes_pending
    FROM   HTT_ACTION_LOG     a
    JOIN   HTT_TRANSITION_LOG t
    ON     a.TRANSITION_UUID = t.TRANSITION_UUID
    WHERE  a.STATUS LIKE 'RUNNING %'
    AND    DATEDIFF(mi, a.PENDING_SINCE, GETUTCDATE()) > $max_minutes_pending ";

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/**
 * reopen_action
 *
 * Resets a HTT_ACTION_LOG STATUS to 'OPEN'
 * @param  string $action_uuid
 * @return boolean
 */
function reopen_action($action_uuid)
{
  $query = sprintf("
    UPDATE HTT_ACTION_LOG
    SET    STATUS = 'OPEN'
    WHERE  ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );

  return run_sql_and_check($query);
}

/**
 * close_action
 *
 * Sets a HTT_ACTION_LOG STATUS to $status and updates ACTION_RESULT
 *
 * @param  string $action_uuid
 * @param  string $status
 * @param  string $action_result
 * @return boolean
 */
function close_action($action_uuid, $status, $action_result=NULL)
{
  // this is pre-pended to ACTION_RESULT
  if ( is_null($action_result) )
    $action_result = 'close_action';

  $query = sprintf("
    UPDATE HTT_ACTION_LOG
    SET    STATUS        = %s,
           CLOSED        = GETUTCDATE(),
           ACTION_RESULT = ( CASE
                             WHEN ( ACTION_RESULT IS NULL OR ACTION_RESULT = '' )
                             THEN '%s'
                             ELSE '%s' + ';' + RTRIM(ACTION_RESULT)
                             END )
    WHERE  ACTION_UUID   = %s",
    mssql_escape_with_zeroes($status),
    $action_result,
    $action_result,
    mssql_escape_with_zeroes($action_uuid)
  );

  $success = run_sql_and_check($query);

  if ( $success )
  {
    $a_uuid = preg_replace( '/[\{\}]/' , '' , $action_uuid );

    $redis = new \Ultra\Lib\Util\Redis;

    $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

    // update action in Redis
    $redis->hset( 'ultra/action/'.$a_uuid , 'status' , $status );

    // get $t_uuid from Redis
    $t_uuid = $redis->hget( 'ultra/action/'.$a_uuid , 'transition_uuid' );

    // remove action from Redis sorted set
    if ( $t_uuid )
      $redis->zrem( 'ultra/sortedset/transitions/actions/'.$t_uuid , $a_uuid );

    // destroy action in Redis
    $redis_action->deleteObject( $a_uuid );
  }

  return $success;
}

/**
 * abort_action
 *
 * Sets a HTT_ACTION_LOG STATUS to 'ABORTED'
 *
 * @param  string $action_uuid
 * @param  string $action_result
 * @return boolean
 */
function abort_action($action_uuid, $action_result=NULL)
{
  if ( is_null($action_result) )
    $action_result = 'abort_action';

  return close_action($action_uuid, 'ABORTED', $action_result);
}

/**
 * get_action_result
 *
 * Returns HTT_ACTION_LOG.ACTION_RESULT
 * $params: see htt_action_log_select_query
 *
 * @param  array $params
 * @return array
 */
function get_action_result($params)
{
  $action_result = array();

  $htt_action_log_select_query = htt_action_log_select_query( $params );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    $action_result = json_decode($query_result[0]->ACTION_RESULT);
  }

  return $action_result;
}

/**
 * get_actions
 *
 * Returns actions according to $params settings
 * $params: see htt_action_log_select_query
 *
 * @param  array $params
 * @return object[]
 */
function get_actions($params)
{
  $htt_action_log_select_query = htt_action_log_select_query( $params );

  return mssql_fetch_all_objects(logged_mssql_query($htt_action_log_select_query));
}

/**
 * htt_action_log_select_query
 *
 * Generic function to compose a HTT_ACTION_LOG SELECT query
 * $params: action_name
 *          transition_uuid
 *          action_uuid
 *          NOT#CLOSED
 *          NOT#ABORTED
 *          archive
 * 
 * @param  array $params
 * @return string SQL
 */
function htt_action_log_select_query($params)
{
  $where = array();

  if ( isset( $params["action_name"] ) )
  {
    $where[] = sprintf(" ACTION_NAME = %s " , mssql_escape($params["action_name"]) );
  }

  if ( isset( $params["transition_uuid"] ) )
  {
    $where[] = sprintf(" TRANSITION_UUID = %s " , mssql_escape($params["transition_uuid"]) );
  }

  if ( isset( $params["action_uuid"] ) )
  {
    $where[] = sprintf(" ACTION_UUID = %s " , mssql_escape($params["action_uuid"]) );
  }

  if ( ( isset( $params["NOT#CLOSED"] ) ) && $params["NOT#CLOSED"] )
  {
    $where[] = " STATUS != 'CLOSED' ";
  }

  if ( ( isset( $params["NOT#ABORTED"] ) ) && $params["NOT#ABORTED"] )
  {
    $where[] = " STATUS != 'ABORTED' ";
  }

  $table_name = ( isset( $params["archive"] ) && $params["archive"] )
                ?
                'HTT_ACTION_ARCHIVE'
                :
                'HTT_ACTION_LOG'
                ;

  $where_clause = ' WHERE '.implode(" AND ", $where);

  # Important:
  # CAST(ACTION_RESULT AS TEXT) is used to fix a bug for which we only get 255 characters out of varchar fields

  $query = "
    SELECT

ACTION_UUID,
TRANSITION_UUID,
STATUS,
CREATED,
CLOSED,
ACTION_SEQ,
ACTION_TYPE,
ACTION_NAME,
CAST(ACTION_RESULT AS TEXT) AS ACTION_RESULT,
PENDING_SINCE,
DATEDIFF ( minute , PENDING_SINCE , GETUTCDATE() ) PENDING_SINCE_MINUTES,
ACTION_SQL

    FROM $table_name
    $where_clause
    ORDER BY TRANSITION_UUID , ACTION_SEQ";

  return $query;
}

/**
 * htt_action_log_insert_query
 * $params: action_uuid
 *          transition_uuid
 *          status
 *          created
 *          pending_since
 *          closed
 *          action_seq
 *          action_type
 *          action_name
 *          action_result
 *
 * Generic function to compose a HTT_ACTION_LOG INSERT query
 * @param  array $params
 * @return string SQL
 */
function htt_action_log_insert_query($params)
{
  $query = sprintf(
  "INSERT INTO HTT_ACTION_LOG
(
  ACTION_UUID,
  TRANSITION_UUID,
  STATUS,
  CREATED,
  PENDING_SINCE,
  CLOSED,
  ACTION_SEQ,
  ACTION_TYPE,
  ACTION_NAME,
  ACTION_RESULT
)
VALUES
(
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %d,
  %s,
  %s,
  %s
)",
    mssql_escape($params["action_uuid"]),
    mssql_escape($params["transition_uuid"]),
    mssql_escape($params["status"]),
    $params["created"],
    $params["pending_since"],
    $params["closed"],
    $params["action_seq"],
    mssql_escape($params["action_type"]),
    mssql_escape($params["action_name"]),
    mssql_escape($params["action_result"])
  );

  return $query;
}

/**
 * open_actions_reaper
 *
 * Returns a query to clean up ancient actions
 *
 * @param  string $mode
 * @param  integer $max_age
 * @return string SQL
 */
function open_actions_reaper( $mode , $max_age )
{
  $clause = " ( HTT_ACTION_LOG.STATUS = 'OPEN' OR HTT_ACTION_LOG.STATUS = 'PENDING' )
         AND    DATEDIFF(ss, COALESCE(HTT_TRANSITION_LOG.CREATED, GETUTCDATE()), GETUTCDATE()) > $max_age
         AND    HTT_TRANSITION_LOG.STATUS = 'ABORTED'";

  $finder = "
      SELECT HTT_ACTION_LOG.ACTION_NAME, HTT_ACTION_LOG.ACTION_UUID, HTT_ACTION_LOG.CREATED
      FROM   DBO.HTT_TRANSITION_LOG WITH (NOLOCK)
      JOIN   DBO.HTT_ACTION_LOG WITH (NOLOCK)
      ON     HTT_TRANSITION_LOG.TRANSITION_UUID = HTT_ACTION_LOG.TRANSITION_UUID
      WHERE $clause";

  foreach (mssql_fetch_all_objects(logged_mssql_query($finder)) as $action)
  {
    dlog('',
           "$mode: Found OPEN or PENDING action %s %s to be aborted, created %s (%d seconds)",
           $action->ACTION_NAME,
           $action->ACTION_UUID,
           $action->CREATED,
           $max_age);
  }

  return "UPDATE HTT_ACTION_LOG
          SET    HTT_ACTION_LOG.STATUS = 'ABORTED',
                 HTT_ACTION_LOG.CLOSED = GETUTCDATE()
          FROM   HTT_ACTION_LOG
          JOIN   HTT_TRANSITION_LOG
          ON     HTT_ACTION_LOG.TRANSITION_UUID = HTT_TRANSITION_LOG.TRANSITION_UUID
          WHERE  $clause";
}

/**
 * pending_actions_reaper
 *
 * Returns a query to clean up stuck pending actions
 *
 * @param  string $mode
 * @param  integer $max_age
 * @return string SQL
 */
function pending_actions_reaper( $mode , $max_age )
{
  $clause = "status = 'PENDING' AND
ACTION_NAME NOT LIKE '%ewPortIn%' AND
ACTION_NAME NOT LIKE '%pdatePortIn%' AND
DATEDIFF(ss, COALESCE(PENDING_SINCE, GETUTCDATE()), GETUTCDATE()) > $max_age";

  $finder = "SELECT * from htt_action_log WHERE $clause";

  foreach (mssql_fetch_all_objects(logged_mssql_query($finder)) as $action)
  {
    dlog('',
           "$mode: Found PENDING action %s %s to be aborted, pending since %s (%d seconds)",
           $action->ACTION_NAME,
           $action->ACTION_UUID,
           $action->PENDING_SINCE,
           $max_age);
  }

  return "UPDATE htt_action_log SET status = 'ABORTED', closed = GETUTCDATE(), ACTION_RESULT = 'timeout' WHERE $clause";
}

/**
 * get_next_action
 *
 * (old code) retrieve the next action for $transition_uuid
 * 
 * @param  string $transition_uuid
 * @param  string $status
 * @return object table row || NULL
 */
function get_next_action($transition_uuid, $status)
{
  return get_by_column('htt_action_log',
                       array('transition_uuid' => $transition_uuid,
                             'status' => $status),
                       'ORDER BY action_seq');
}

/**
 * reserve_next_open_action
 *
 * Given a $transition_uuid , returns the next OPEN action with lower ACTION_SEQ value from the Redis sorted set.
 *
 * @param  string $transition_uuid
 * @param  object $redis Redis
 * @return object $next_open_action
 */
function reserve_next_open_action( $transition_uuid , $redis )
{
  dlog('', "transition_uuid = $transition_uuid");

  $next_open_action = NULL;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

  // get the next ACTION_UUID from the Redis sorted set
  $a_uuid = $redis->zhead(  'ultra/sortedset/transitions/actions/'.$t_uuid );
  $score  = $redis->zscore( 'ultra/sortedset/transitions/actions/'.$t_uuid , $a_uuid  );

  dlog('', "zhead returned $a_uuid , with score = $score");

  try
  {
    if ( ! $a_uuid )
      throw new Exception("No actions left in $t_uuid sorted set");

    // get action from DB
    $action = get_actions(
      array(
        "action_uuid" => '{'.$a_uuid.'}'
      )
    );

    if ( $action && is_array( $action ) && count( $action ) )
      $next_open_action = $action[0];

    if ( ! $next_open_action )
      throw new Exception("Action $a_uuid not found or DB read error");

    dlog('',"action = %s",$action);

    if ( $next_open_action->STATUS != 'OPEN' )
    {
      if ( $next_open_action->STATUS == 'CLOSED' )
      {
        // remove action from Redis sorted set
        $redis->zrem( 'ultra/sortedset/transitions/actions/'.$t_uuid , $a_uuid );

        $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

        // destroy action in Redis
        $redis_action->deleteObject( $a_uuid );
      }

      throw new Exception("Action $a_uuid status is not OPEN ( ".$next_open_action->STATUS." ), function reserve_next_open_action cannot continue");
    }

    if ($status = $redis->hget( 'ultra/action/'.$a_uuid , 'status' ))
      if ( $status != 'OPEN' )
        throw new Exception("Action $a_uuid status is not OPEN ( $status ), function reserve_next_open_action cannot continue");

    // reserve action in Redis
    $redis->hset( 'ultra/action/'.$a_uuid , 'status' , 'RUNNING '.getmypid() ); # TODO: use UUID?

    usleep(1000);

    $status = $redis->hget( 'ultra/action/'.$a_uuid , 'status' );

    if ( $status != 'RUNNING '.getmypid() )
      throw new Exception("Action $a_uuid status is $status, attempt to reserve failed");

    // the ACTION_UUID has been reserved successfully in Redis

    // reserve on DB, but do not double check
    $success = update_htt_action_log_status( 'RUNNING '.getmypid() , '{'.$a_uuid.'}' );

    if ( ! $success )
    {
      // HTT_ACTION_LOG update failed, we have to un-reserve the action
      $redis->hset( 'ultra/action/'.$a_uuid , 'status' , 'OPEN' );

      throw new Exception("HTT_ACTION_LOG update failed");
    }

    // the ACTION_UUID has been reserved successfully on DB
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $next_open_action = NULL;
  }

  return $next_open_action;
}

/**
 * log_action
 *
 * Save an action to HTT_ACTION_LOG and Redis
 * action params :
 *   $aparams = array('type' => 'pfuncall', 'name' => $action['pfuncall'], 'seq' => $seq++)
 *
 * @param  string $transition_uuid
 * @param  string $action_uuid
 * @param  array $aparams
 * @param  array $params
 * @param  string $status
 * @param  string $parent_action_id
 * @param  object $redis Redis
 * @param  object $redis_action \Ultra\Lib\Util\Redis\Action
 * @return boolean
 */
function log_action($transition_uuid,
                    $action_uuid,
                    $aparams,
                    $params=NULL,
                    $status='OPEN',
                    $parent_action_id=NULL,
                    $redis=NULL,
                    $redis_action=NULL)
{
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  if ( is_null($redis_action) )
    $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

  $seq  = ( isset($aparams['seq'])  ) ? $aparams['seq'] : 0 ;
  $type = ( isset($aparams['type']) ) ? $aparams['type'] : 'TODO' ;
  $name = ( isset($aparams['name']) ) ? $aparams['name'] : 'TODO' ;

  $extra_columns = '';
  $extra_values = '';

  if ('PENDING' == $status)
  {
    $extra_columns = ', PENDING_SINCE';
    $extra_values = ', GETUTCDATE()';
  }

  if (NULL == $seq)
  {
    $seq = "COALESCE(1+(SELECT MAX(action_seq) FROM htt_action_log WHERE transition_uuid = '$transition_uuid'), 0)";
  }

  if ($type === 'sql')
  {
    $extra_columns = $extra_columns . ', ACTION_SQL';
    $extra_values = $extra_values . sprintf(', %s', mssql_escape_with_zeroes($name));
    $name = 'sql';
  }
  elseif(NULL != $parent_action_id)
  {
    $extra_columns = $extra_columns . ', ACTION_SQL';
    $extra_values = $extra_values . sprintf(", '%s'", $parent_action_id);
  }

  dlog('transitions',
       "INSERTing action %s (transition %s), aparams %s",
       $action_uuid,
       $transition_uuid,
       $aparams);

  $q = sprintf("INSERT INTO htt_action_log
( ACTION_UUID, TRANSITION_UUID, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, STATUS %s )
VALUES ( '%s', '%s', %s, '%s', '%s', '%s' %s );",
               $extra_columns,
               $action_uuid,
               $transition_uuid,
               $seq,
               $type,
               str_replace("'", "''", $name),
               $status,
               $extra_values);

  $check = run_sql_and_check($q);

  if ( $check )
  {
    // add Action Object to Redis
    $redis_action->addObject(
      $action_uuid,
      $transition_uuid,
      $type,
      str_replace("'", "''", $name),
      $status
    );
  }

  if (is_array($params))
  {
    foreach ($params as $k => $v)
    {
      $check2 = log_action_parameter($action_uuid, $k, $v);
      $check = $check && $check2;
    }
  }

  return $check;
}

/**
 * reset_running_actions
 * @param  string $transition_uuid
 * @return string string $error
 */
function reset_running_actions($transition_uuid)
{
  $error = '';

  $sql = " UPDATE HTT_ACTION_LOG SET STATUS = 'OPEN' WHERE STATUS LIKE 'RUNNING%' AND ".
    sprintf(
    " TRANSITION_UUID = %s ",
    mssql_escape_with_zeroes($transition_uuid)
  );

  if ( ! run_sql_and_check($sql) )
    $error = 'Cannot update HTT_ACTION_LOG';

  return $error;
}

/**
 * update_htt_action_log_status
 *
 * Sets the value of HTT_ACTION_LOG.STATUS
 *
 * @param  string $status
 * @param  string $action_uuid
 * @return boolean - TRUE if success ; FALSE in case of issues
 */
function update_htt_action_log_status($status,$action_uuid)
{
  $extra_set = '';

  if ( preg_match('/^RUNNING /', $status) )
  {
    $extra_set .= " , PENDING_SINCE = GETUTCDATE() ";
  }

  return run_sql_and_check(
    sprintf(
      "UPDATE HTT_ACTION_LOG
       SET    STATUS         = %s
       $extra_set
       WHERE  ACTION_UUID    = %s",
      mssql_escape_with_zeroes($status),
      mssql_escape_with_zeroes($action_uuid)
    )
  );
}

/**
 * set_htt_action_log_status
 *
 * (old code) Update HTT_ACTION_LOG.STATUS
 *
 * @param  string $status
 * @param  string $transition_uuid
 * @return boolean
 */
function set_htt_action_log_status($status,$transition_uuid)
{
  $pending = ( preg_match('/^RUNNING /', $status) )
             ?
             ' , PENDING_SINCE = getutcdate() '
             :
             ''
             ;

  return run_sql_and_check(sprintf("UPDATE htt_action_log
SET
STATUS        = '%s'
$pending
WHERE
ACTION_UUID   = (SELECT TOP 1 action_uuid FROM htt_action_log
                     WHERE transition_uuid = '%s' AND status = 'OPEN'
                     ORDER BY action_seq)",
                                     $status,
                                     $transition_uuid));
}

?>
