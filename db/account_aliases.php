<?php

include_once('db.php');

# php component to access DB table ACCOUNT_ALIASES

# synopsis

#echo account_aliases_insert_query(
#  array(
#    'msisdn'     => 12345,
#    'account_id' => 4321
#  )
#)."\n\n";

#echo account_aliases_update_query(
#  array(
#    'msisdn'     => 1234,
#    'account_id' => 4321
#  )
#)."\n\n";

#echo account_aliases_select_query(
#  array(
#    'customer_id' => 2665
#  )
#)."\n\n";


/**
 * verify_account_aliases
 *
 * verifies the content of ACCOUNT_ALIASES for a given customer
 *
 * @param  integer $customer_id
 * @param  string $current_mobile_number
 * @return boolean $verify_account_aliases
 */
function verify_account_aliases($customer_id,$current_mobile_number)
{
  $verify_account_aliases = FALSE;

  $account_aliases_select_query = account_aliases_select_query(
    array(
      'customer_id' => $customer_id
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($account_aliases_select_query));

  if ( $query_result && ( is_array($query_result) ) && count($query_result) > 0 )
  {
    foreach( $query_result as $id => $account_aliases_data )
    {
      if ( $account_aliases_data->ALIAS == $current_mobile_number )
      {
        $verify_account_aliases = TRUE;
      }
    }
  }

  return $verify_account_aliases;
}

/**
 * account_aliases_select_query 
 *
 * @param  array $params
 * @return string SQL
 */
function account_aliases_select_query($params)
{
  $query = sprintf(
    "SELECT aa.*
     FROM   accounts a, account_aliases aa
     WHERE  a.account_id  = aa.account_id
     AND    a.customer_id = %d
    ",
    $params['customer_id']
  );

  if ( ! empty($params['alias']) )
    $query .= sprintf( " AND ALIAS = %s " , mssql_escape_with_zeroes($params['alias']) );

  return $query;
}

/**
 * account_aliases_update_query 
 *
 * @param  array $params
 * @return string SQL
 */
function account_aliases_update_query($params)
{
  $query = sprintf(
    "UPDATE ACCOUNT_ALIASES
     SET    ALIAS      = %s
     WHERE  ACCOUNT_ID = %d",
    mssql_escape_with_zeroes($params['msisdn']),
    $params['account_id']
  );

  return $query;
}

/**
 * account_aliases_from_customer_id_insert_query
 *
 * SQL statement to add to ACCOUNT_ALIASES using CUSTOMER_ID and msisdn
 *
 * @param  array $params
 * @return string SQL
 */
function account_aliases_from_customer_id_insert_query($params)
{
  $dnis               = "'*'";
  $sip_user_id        = "'*'";

  return sprintf( "
INSERT INTO ACCOUNT_ALIASES
(
  DNIS,
  ALIAS,
  ACCOUNT_ID,
  ACCOUNT_ALIAS_TYPE,
  PASSWORD,
  SIP_USER_ID
)
SELECT
  %s,
  %s,
  a.ACCOUNT_ID,
  %d,
  %s,
  %s
FROM
  ACCOUNTS a
WHERE
  CUSTOMER_ID = %d",
    $dnis,
    mssql_escape_with_zeroes($params['msisdn']),
    $params['account_alias_type'],
    mssql_escape_with_zeroes($params['password']),
    $sip_user_id,
    $params['customer_id']
  );
}

/**
 * account_aliases_insert_query
 * $params: msisdn
 *          account_id
 *          
 * @param  array $params
 * @return string SQL
 */
function account_aliases_insert_query($params)
{
  $dnis               = "'*'";
  $password           = 'NULL';
  $account_alias_type = 1;
  $sip_user_id        = "'*'";

  $query = sprintf(
    "
IF NOT EXISTS (SELECT * FROM ACCOUNT_ALIASES where ALIAS = %s AND ACCOUNT_ID = %d )
BEGIN
INSERT INTO ACCOUNT_ALIASES
(
  DNIS,
  ALIAS,
  ACCOUNT_ID,
  ACCOUNT_ALIAS_TYPE,
  PASSWORD,
  SIP_USER_ID
)
VALUES
(
  %s,
  %s,
  %d,
  %d,
  %s,
  %s
)
END
",
  mssql_escape_with_zeroes($params['msisdn']),
  $params['account_id'],
  $dnis,
  mssql_escape_with_zeroes($params['msisdn']),
  $params['account_id'],
  $account_alias_type,
  $password,
  $sip_user_id
  );

  return $query;
}

/**
 * account_aliases_fixup_query 
 * @return string SQL
 */
function account_aliases_fixup_query()
{
  return "
INSERT INTO ACCOUNT_ALIASES
( DNIS, ALIAS, ACCOUNT_ID, ACCOUNT_ALIAS_TYPE, PASSWORD, SIP_USER_ID )
select '*',MSISDN,max(a.[account_id]),1,null,'*' from [HTT_ULTRA_MSISDN] m join [htt_customers_overlay_ultra] c on m.[MSISDN]=c.[current_mobile_number]
join accounts a on c.[customer_id]=a.[CUSTOMER_ID] where len(msisdn)=10 and MSISDN_ACTIVATED =1 
and [MSISDN] not in (select ALIAS from [ACCOUNT_ALIASES]) 
group by MSISDN;";
}

/**
 * account_aliases_delete_by_alias_query 
 * @return string SQL
 */
function account_aliases_delete_by_alias_query($alias)
{
  return sprintf(
    'DELETE FROM ACCOUNT_ALIASES WHERE ALIAS = %s',
    mssql_escape_with_zeroes($alias)
  );
}

?>
