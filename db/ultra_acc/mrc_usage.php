<?php

// DB lauer for ULTRA_ACC..MRC_USAGE

/**
 * mrcUsageExists
 * check if usage records exist for a given billing cycle
 * @param Integer customer ID
 * @param Integer plan tracker ID
 * @returns Boolean
 */
function mrcUsageExists($customerId, $planTrackerId)
{
  $table = 'MRC_USAGE';

  $sql = sprintf('
    DECLARE @customer BIGINT = %d;
    DECLARE @tracker BIGINT = %d;
    SELECT MRC_USAGE_ID FROM %s WITH (NOLOCk) WHERE CUSTOMER_ID = @customer AND HTT_PLAN_TRACKER_ID = @tracker',
    $customerId, $planTrackerId, $table);
  $usage = mssql_fetch_all_objects(logged_mssql_query($sql));
  return count($usage) && ! empty($usage[0]->MRC_USAGE_ID) ? TRUE : FALSE;
}

/**
 * mrcUsageLookup
 * return all active lookup rows
 * @returns Array of Objects or NULL on error
 */
function mrcUsageLookup()
{
  $table = 'MRC_USAGE_LOOKUP';

  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery($table, NULL, NULL, array('EFFECTIVE_DATE' => 'past')))
  {
    \logError("failed to repare $table SELECT statement");
    return NULL;
  }

  $lookups = mssql_fetch_all_objects(logged_mssql_query($sql));
  if ( ! count($lookups))
  {
    \logError("failed to get $table values");
    return NULL;
  }
  
  return $lookups;
}

/**
 * mrcUsageInsert
 * create MRC_USAGE record
 * @returns Integer MRC_USAGE_ID on success or NULL on failure
 */
function mrcUsageInsert($customerId, $planTrackerId, $balanceSession, $statusSession, $wholesalePlan)
{
  // validate parameters
  foreach (func_get_args() as $argument)
    if (empty($argument))
    {
      \logError("missing required argument(s)");
      return NULL;
    }

  // prepare INSERT statement
  $table = 'MRC_USAGE';
  $sql = sprintf('
    DECLARE @customer BIGINT = %d;
    DECLARE @tracker BIGINT = %d;
    DECLARE @balance varchar(200) = %s;
    DECLARE @status varchar(200) = %s;
    DECLARE @wholesale INT = %d;
    INSERT INTO %s (CUSTOMER_ID, SAMPLE_DATE, HTT_PLAN_TRACKER_ID, SESSION_ID_CHECKBALANCE, SESSION_ID_QUERYSUBSCRIBER, WHOLESALEPLAN)
      VALUES (@customer, GETUTCDATE(), @tracker, @balance, @status, @wholesale)',
    $customerId, $planTrackerId, mssql_escape_with_zeroes($balanceSession), mssql_escape_with_zeroes($statusSession), $wholesalePlan, $table);

  // create MRC_USAGE record
  if ( ! run_sql_and_check($sql))
  {
    \logError("failed to create $table record");
    return NULL;
  }

  // return its ID
  if ( ! $id = get_scope_identity())
  {
    \logError("failed to get $table identity");
    return NULL;
  }

  return $id;
}

/**
 * mrcUsageValuesInsert
 * create MRC_USAGE_VALUES record
 * @params Array of values
 */
function mrcUsageValuesInsert($values)
{
  $rows = array();
  foreach ($values as $value)
    $rows[] = sprintf('(%d, %d, %s)', $value['mrc'], $value['lookup'], $value['value'] === NULL ? 'NULL' : sprintf('%d', $value['value']));

  $table = 'MRC_USAGE_VALUES';

  if (count($rows))
  {
    $sql = "INSERT INTO $table (MRC_USAGE_ID, LOOKUP_ID, VALUE) VALUES " . implode(', ', $rows);
    if ( ! run_sql_and_check($sql))
      \logError("failed to create $table record");
  }
  else
    \logError('no values given');
}

