<?php

require_once 'db.php';
require_once 'db/ultra_acc/mrc_usage.php';

class MrcUsageTest extends PHPUnit_Framework_TestCase
{
  function setUp()
  {
    \Ultra\Lib\DB\ultra_acc_connect();
  }

  function test_mrcUsageExists()
  {
    // positive result
    $customerId = 19430;
    $planTrackerId = 2106;
    $result = mrcUsageExists($customerId, $planTrackerId);
    $this->assertTrue($result);

    // negative result
    $customerId = 19407;
    $planTrackerId = 12095;
    $result = mrcUsageExists($customerId, $planTrackerId);
    $this->assertFalse($result);
  }

  
  function test_mrcUsageInsert()
  {
    
    $id = mrcUsageInsert(1000, 1001, 'VYT_TEST_1000', 'VYT_TEST_2000', 0);
    $this->assertEquals(NULL, $id);

    $id = mrcUsageInsert(1000, 1001, 'VYT_TEST_1000', 'VYT_TEST_2000', 111);
    $this->assertNotEmpty($id);
  }


  function test_mrcUsageValuesInsert()
  {
  
    mrcUsageValuesInsert(array());

    mrcUsageValuesInsert(array(
      array('mrc' => 83, 'lookup' => 1, 'value' => 8250),
      array('mrc' => 83, 'lookup' => 2, 'value' => 9000),
      array('mrc' => 83, 'lookup' => 3, 'value' => NULL)));

  }

}
