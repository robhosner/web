<?php

require_once 'db.php';
require_once 'db/ultra_bolton_tracker.php';

/**
 * tests for ULTRA.BOLTON_TRACKER layer functions
 */
class BoltonTrackerTest extends PHPUnit_Framework_TestCase
{
  /**
   * test_getCurrentTrackedBoltOns
   */
  public function test_getCurrentTrackedBoltOns()
  {
    teldata_change_db();

    $cid = 3530;
    $type = 'IMMEDIATE';
    $bolt_ons = getCurrentTrackedBoltOns($cid, $type);
    $this->assertGreaterThan(0, count($bolt_ons));
    print_r($bolt_ons);

    $cid = 3649;
    $type = 'MONTHLY';
    $bolt_ons = getCurrentTrackedBoltOns($cid, $type);
    $this->assertGreaterThan(0, count($bolt_ons));
    print_r($bolt_ons);

    $cid = 3649;
    $type = 'MONTHLY';
    $bolt_ons = getCurrentTrackedBoltOns($cid, $type, 'DATA');
    $this->assertGreaterThan(0, count($bolt_ons));
    print_r($bolt_ons);

    $cid = 3649;
    $bolt_ons = getCurrentTrackedBoltOns($cid);
    $this->assertGreaterThan(0, count($bolt_ons));
    print_r($bolt_ons);
  }
}



?>