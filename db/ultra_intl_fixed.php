<?php

/**
 * get_ultra_intl_fixed_customers
 * Get array of customer_id from ULTRA.INTL_FIXED
 * 
 * @param  integer $customer_id
 * @return array array of customer_id
 */
function get_ultra_intl_fixed_customers( $customer_id = NULL )
{
  $sql = 'select customer_id from [ULTRA].[INTL_FIXED]';

  if ( $customer_id )
    $sql .= sprintf(" where customer_id = %d",$customer_id);

  return mssql_fetch_all_rows(logged_mssql_query($sql));
}

/**
 * ultra_intl_fixed_customer_allowed
 * return if $customer_id in ULTRA.INTL_FIXED
 * 
 * @param  integer $customer_id
 * @return boolean
 */
function ultra_intl_fixed_customer_allowed( $customer_id )
{
  $data = get_ultra_intl_fixed_customers( $customer_id );

  return ( $data && is_array($data) && count($data) );
}

/**
 * add_msisdn_to_ultra_intl_fixed
 * INSERT customer_id with $msisdn row in ULTRA.INTL_FIXED
 * 
 * @param  string $msisdn
 * @return boolean result of SQL INSERT
 */
function add_msisdn_to_ultra_intl_fixed( $msisdn )
{
  $sql = sprintf(
    "insert into [ULTRA].[INTL_FIXED] select customer_id from htt_customers_overlay_ultra where current_mobile_number = %s",
    mssql_escape_with_zeroes(normalize_msisdn_10($msisdn))
  );

  return run_sql_and_check( $sql );
}

/**
 * add_customer_id_to_ultra_intl_fixed
 * INSERT customer_id row in ULTRA.INTL_FIXED
 * 
 * @param  integer $customer_id
 * @return boolean result of SQL INSERT
 */
function add_customer_id_to_ultra_intl_fixed( $customer_id )
{
  $sql = sprintf(
    "insert into [ULTRA].[INTL_FIXED] values ( %d )",
    $customer_id
  );

  return run_sql_and_check( $sql );
}

?>
