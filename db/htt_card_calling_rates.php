<?php

include_once('db.php');

# php component to access DB table htt_card_calling_rates #

# synopsis

#echo htt_card_calling_rates_select_query( array('card'=>'UVCARD','display'=>TRUE) )."\n";
#echo htt_card_calling_rates_select_query( array('card'=>'ULTRAMOBILE') )."\n";
#echo htt_card_calling_rates_select_query( array('card'=>'CALLINGCARD') )."\n";

/**
 * htt_card_calling_rates_select_query
 * $params: card
 *          display
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_card_calling_rates_select_query($params)
{
  $select_fields =
    "card_rate_ultramobile_19, ".
    "country COLLATE SQL_Latin1_General_CP1253_CI_AI     country, ".
    "destination COLLATE SQL_Latin1_General_CP1253_CI_AI destination, ".
    "card_rate_".strtolower($params['card']).", ".
    "card_rate_".strtolower($params['card'])."_expires ";

  $where = array();

  if ( isset($params['display']) && $params['display'] )
    $where[] = ' display = 1 ';

  $where_clause = "";

  if ( count($where) > 0 )
    $where_clause = ' WHERE '.implode(" AND ", $where);

  $query = " SELECT $select_fields FROM htt_card_calling_rates $where_clause ORDER BY COUNTRY, DESTINATION";

  return $query;
}

/**
 * htt_card_calling_rates_get_rates_data 
 * @param  string $product_name
 * @param  boolean $display
 * @return object[]
 */
function htt_card_calling_rates_get_rates_data($product_name, $display = TRUE)
{
  $params = array(
    'card'    => $product_name,
    'display' => TRUE
  );

  $query = htt_card_calling_rates_select_query($params);

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

?>
