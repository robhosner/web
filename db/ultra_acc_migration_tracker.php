<?php

/**
 * update_migration_tracker_to_complete
 * @param  integer $customer_id
 * @param  string $msisdn
 * @return boolean
 */
function update_migration_tracker_to_complete( $customer_id , $msisdn )
{
  $sql = sprintf("
UPDATE ULTRA.ACC_MIGRATION_TRACKER
SET    STATUS      = 'COMPLETE'
WHERE  CUSTOMER_ID = %d
AND    MSISDN      = %s",
    $customer_id,
    mssql_escape_with_zeroes( $msisdn )
  );

  return run_sql_and_check($sql);
}

/**
 * [update_migration_tracker description]
 * @param  array $migrating_customers [array(customer_id=>msisdn)]
 * @param  string $migration_date
 * @param  string $file_name
 * @return boolean
 */
function update_migration_tracker( $migrating_customers , $migration_date , $file_name )
{
  dlog('', "(%s)", func_get_args());

  $insert_values = array();

  $insert_size = 20;

  foreach( $migrating_customers as $customer_id => $msisdn )
  {
    $insert_values[] = sprintf("
(
  %d,
  %s,
  'MIGRATING',
  %s,
  '%s'
)",
      $customer_id,
      mssql_escape_with_zeroes( $msisdn ),
      mssql_escape_with_zeroes( $file_name ),
      $migration_date
    );

    if ( count($insert_values) >= $insert_size )
    {
      migration_tracker_insert_batch( $insert_values );

      $insert_values = array();
    }
  }

  if ( count($insert_values) )
    migration_tracker_insert_batch( $insert_values );

  return TRUE;
}

/**
 * get_ultra_acc_migration_tracker
 * $params: customer_id
 *          msisdn
 *          status
 *          file_name
 *          
 * @param  array $params
 * @return object[]
 */
function get_ultra_acc_migration_tracker( $params )
{
  $where = array();

  if ( isset( $params['customer_id']) )
    $where[] = ' CUSTOMER_ID = '.$params['customer_id'];

  if ( isset( $params['msisdn']     ) )
    $where[] = ' MSISDN      = '.mssql_escape_with_zeroes( $params['msisdn'] );

  if ( isset( $params['status']     ) )
    $where[] = ' STATUS      = '.mssql_escape_with_zeroes( $params['status'] );

  if ( isset( $params['file_name']  ) )
    $where[] = ' FILE_NAME   = '.mssql_escape_with_zeroes( $params['file_name'] );

  $sql = 'SELECT * FROM ULTRA.ACC_MIGRATION_TRACKER WHERE '.implode(' AND ',$where);

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * is_msisdn_migrating
 * @param  string  $msisdn
 * @return boolean
 */
function is_msisdn_migrating( $msisdn )
{
  $data = get_ultra_acc_migration_tracker(
    array(
      'msisdn' => $msisdn,
      'status' => 'MIGRATING'
    )
  );

  return ( $data && is_array($data) && count($data) );
}

/**
 * is_customer_id_migrating
 * @param  integer $customer_id
 * @return boolean
 */
function is_customer_id_migrating( $customer_id )
{
  $data = get_ultra_acc_migration_tracker(
    array(
      'customer_id' => $customer_id,
      'status'      => 'MIGRATING'
    )
  );

  return ( $data && is_array($data) && count($data) );
}

/**
 * migration_tracker_insert_batch
 * @param  array $insert_values (CUSTOMER_ID,MSISDN,STATUS,FILE_NAME,MIGRATION_DATE)
 * @return boolean
 */
function migration_tracker_insert_batch( $insert_values )
{
  $sql = "
INSERT INTO ULTRA.ACC_MIGRATION_TRACKER
(
 CUSTOMER_ID,
 MSISDN,
 STATUS,
 FILE_NAME,
 MIGRATION_DATE
)
VALUES ".implode(',',$insert_values);

  return run_sql_and_check_result($sql);
}

/**
 * add_to_ultra_acc_migration_tracker
 * $params: customer_id
 *          msisdn
 *          file_name
 *          migration_date
 *          status
 *          
 * @param  array $params
 * @return boolean
 */
function add_to_ultra_acc_migration_tracker( $params )
{
  if ( ! $params['customer_id'] )
    return make_error_Result('customer_id missing');

  if ( ! $params['msisdn'] )
    return make_error_Result('msisdn missing');

  if ( ! $params['file_name'] )
    return make_error_Result('file_name missing');

  if ( ! $params['migration_date'] )
    return make_error_Result('migration_date missing');

  if ( ! is_numeric($params['customer_id']) )
    return make_error_Result('customer_id invalid');

  if ( ! is_numeric($params['msisdn']) )
    return make_error_Result('msisdn invalid');

  if ( !isset( $params['status'] ) )
    $params['status'] = 'MIGRATING';

  $params['migration_date'] = "'".$params['migration_date']."'";

  $sql = sprintf("
INSERT INTO ULTRA.ACC_MIGRATION_TRACKER
(
 CUSTOMER_ID,
 MSISDN,
 STATUS,
 FILE_NAME,
 MIGRATION_DATE
)
VALUES
(
  %d,
  %s,
  %s,
  %s,
  %s
)",
  $params['customer_id'],
  mssql_escape_with_zeroes( $params['msisdn'] ),
  mssql_escape_with_zeroes( $params['status'] ),
  mssql_escape_with_zeroes( $params['file_name'] ),
  $params['migration_date']
  );

  return run_sql_and_check_result($sql);
}

?>
