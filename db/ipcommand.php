<?php

# php component to access DB table ipcommand #

# synopsis

#echo ipcommand_insert_query(
#  array(
#    'recipient' => 'CCV SERVER',
#    'sender'    => 'WEB INTERFACE',
#    'command'   => 'UPDATE_CCARD',
#    'value_1'   => 'test 1',
#    'value_2'   => 'test 2'
#  )
#)."\n";

/**
 * ipcommand_insert_query
 * $params: recipient
 *          sender
 *          command
 *          value_1
 *          value_2
 *          
 * @param  array $params
 * @return string SQL
 */
function ipcommand_insert_query($params)
{
  $recipient = $params['recipient'];
  $sender    = $params['sender'];
  $command   = $params['command'];
  $value_1   = $params['value_1'];
  $value_2   = $params['value_2'];

  $query = sprintf(
"INSERT INTO ipcommand
 (
  DATE,
  RECIPIENT,
  SENDER,
  COMMAND,
  VALUE1,
  VALUE2,
  VALUE3,
  VALUE4,
  VALUE5
 )
 VALUES
 (
  getUTCDate(),
  %s,
  %s,
  %s,
  %s,
  %s,
  '',
  '',
  ''
 )",
    mssql_escape($recipient),
    mssql_escape($sender),
    mssql_escape($command),
    mssql_escape($value_1),
    mssql_escape($value_2)
  );

  return $query;
}

?>
