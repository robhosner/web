#!/usr/bin/perl

use warnings;
use strict;


# we will found our Perl modules here
use lib "$ENV{DOCUMENT_ROOT}";


use Data::Dumper;
use XML::Compile::SOAP::Daemon::AnyDaemon;
use XML::Compile::SOAP11;
use XML::Compile::WSDL11;
use IO::Socket::SSL 'SSL_VERIFY_NONE';
use IO::Socket      'SOMAXCONN';
use File::Basename;
use JSON::XS;
use FindBin;
use Ultra::Lib::Amdocs::Notification;
use Ultra::Lib::MQ::EndPoint;
use Ultra::Lib::Util::Config;
use Ultra::Lib::Util::Miscellaneous;
use lib "$FindBin::Bin/";

use POSIX qw(strftime);

# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/var/log/htt/acc_notification_server.log', format => sub { sprintf('[%s] [%s] ', ( strftime "%m\/%e\/%y %H:%M:%S", localtime ), $$).$_[0] } ;

# forces a flush right away and after every write or print on the currently selected output channel.
$| = 1;


my $host        = shift;
my $environment = shift;
my $port        = shift; # [ QAT (mw-dev-01) ] 8799   ;   [ PROD (mw-02) ] 443


# objects which are instantiated once and for all
my $daemon      = XML::Compile::SOAP::Daemon::AnyDaemon->new();
my $json_coder  = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();
my $config      = Ultra::Lib::Util::Config->new();
my $mw_db_host  = $config->find_credential('mw/db/host');

# daemon definitions from WSDL
my $wsdl_file_name = get_wsdl_file_name( $config , $environment );
my $wsdl           = XML::Compile::WSDL11->new( $wsdl_file_name );


print STDERR "host           = $host\n";
print STDERR "port           = $port\n";
print STDERR "environment    = $environment\n";
print STDERR "wsdl_file_name = $wsdl_file_name\n";


if ( ! -f $wsdl_file_name )
{ print STDERR "File $wsdl_file_name does not exist\n"; }

if ( ! -r $wsdl_file_name )
{ print STDERR "File $wsdl_file_name is not readable\n"; }


# map callback operation
$daemon->operationsFromWSDL($wsdl,
                            default_callback =>
                            sub
                            {
                             my ($server, $in, $request) = @_;
                             return callback($server, $in, $request);
                            });

# see what is defined: this is useful for debugging/troubleshooting
$daemon->printIndex;


# set up the socket
my $socket = IO::Socket::SSL->new
 (   LocalHost  => $host,
   , LocalPort  => $port
   , Listen     => SOMAXCONN
   , Reuse      => 1
   , SSL_server => 1
   , SSL_verify_mode => SSL_VERIFY_NONE
   , SSL_key_file    => '/etc/httpd/certs/ultra.me.key'
   , SSL_cert_file   => '/etc/httpd/certs/ultra.me.crt'
 ) or error __x"cannot create socket at {interface}: {err}"
 , interface => "$host:$port"
 , err => IO::Socket::SSL::errstr();


# set up the daemon to run
$daemon->run
 ( name       => basename($0)
   , background => 1
   , max_childs => 44
   , socket     => $socket
   , client_reqbonus => 0
   , client_timeout  => 30
   , client_maxreq   => 1
 );


# this is never reached


# this is the code executed for each successful SOAP notification which hit this server
sub callback
{
  my ($server, $in, $request) = @_;

  # Operation is the API notification name.
  # Important note: not always 'soapaction' is passed, we should be able to retrieve this from the request content.
  my ( $operation ) = ( $request->header('soapaction') =~ /([^\/]+)$/ ) ;
  $operation =~ s/[\'\"]//g;

  my $mq_endpoint = Ultra::Lib::MQ::EndPoint->new(
    CONFIG     => $config,
    JSON_CODER => $json_coder 
  );

  # some feedback for our logs
  $mq_endpoint->log("callback start pid = $$ ; callback action = ".$request->header('soapaction')."callback operation = ".$operation);
  #$mq_endpoint->log("callback operation  = ".$operation);                               # this will be the header of the message we should send to the ACC MW MQ
  $mq_endpoint->log("callback parameters = ".$json_coder->encode( $in->{parameters} )); # those will be part of the message we should send to the ACC MW MQ
  $mq_endpoint->log("request = ".$request->content);

  my $action_uuid = $mq_endpoint->_getNewNotificationActionUUID();
  my $session_id  = undef;

  if ( ( defined $in->{parameters}->{ serviceTransactionId  } ) && $in->{parameters}->{ serviceTransactionId  } )
  {
    $session_id = $in->{parameters}->{ serviceTransactionId };
  }

  if ( ( defined $in->{parameters}->{ transactionID         } ) && $in->{parameters}->{ transactionID         } )
  {
    $session_id = $in->{parameters}->{ transactionID };
  }

  if ( ( defined $in->{parameters}->{ TransactionIDAnswered } ) && $in->{parameters}->{ TransactionIDAnswered } )
  {
    $session_id = $in->{parameters}->{ TransactionIDAnswered };
  }

  $mq_endpoint->log("action_uuid = $action_uuid");

  # connect to MW DB
  my $db = Ultra::Lib::DB::MSSQL->new(
    DB_NAME => 'ULTRA_ACC',
    DB_HOST => $mw_db_host,
    CONFIG  => $config
  );

  my $correlationID = ( ( defined $in->{parameters}->{ CorrelationID } ) ? $in->{parameters}->{ CorrelationID } : '' );
  my $msisdn        = ( ( defined $in->{parameters}->{ MSISDN        } ) ? $in->{parameters}->{ MSISDN        } : '' );

  my $amdocsNotificationObject = Ultra::Lib::Amdocs::Notification->new(
    DB          => $db,
    operation   => $operation,
    mq_endpoint => $mq_endpoint,
    COMMAND     => $operation,
    JSON_CODER  => $json_coder,
    CONFIG      => $mq_endpoint->{ CONFIG },
    ACTION_UUID => $action_uuid,
    SESSION_ID  => ( ( $session_id || ( length($msisdn) != 14 ) ) ? $session_id : $msisdn ),
    MSISDN      => $msisdn,
    ICCID       => ( ( $correlationID ) ? $mq_endpoint->_redis()->get( 'acc/correlationID/'.$correlationID ) : undef ),
    TAG         => $correlationID,
  );

  my $storeSOAPXMLParams = {
    data_xml         => $request->content,
    type_id          => $amdocsNotificationObject->TYPE_ID_NOTIFICATION_INBOUND
  };

  if ( ( defined $in->{parameters}->{ ProvisioningStatus } ) )
  {
    $storeSOAPXMLParams->{ ResultCode } = ( $in->{parameters}->{ ProvisioningStatus } ? 1 : 0 );
  }

  # store inbound SOAP XML into DB.
  my $success = $amdocsNotificationObject->storeSOAPXML( $storeSOAPXMLParams );

  if ( $amdocsNotificationObject->hasErrors() )
  {
    $mq_endpoint->log("storeSOAPXML errors : ".Dumper($amdocsNotificationObject->getErrors()));
  }

  my $return = { parameters => $amdocsNotificationObject->callbackOperation( $operation , $in->{parameters} ) };

  # unorthodox patch:
  # we fire the same exact notification to the DEV environment
# THIS CREATES A LOT OF PROBLEMS In PRODUCTION
  #propagate_to_dev_environment( $operation , $in->{parameters} , $json_coder , $mq_endpoint );

  $mq_endpoint->log("callback return = ".$json_coder->encode( $return ));

  store_notification_response( $amdocsNotificationObject , $return->{ parameters } , $operation , $json_coder , $mq_endpoint );

  undef $mq_endpoint;

  return $return;
}

sub propagate_to_dev_environment
{
  my ( $operation , $parameters , $json_coder , $mq_endpoint ) = @_;

  # Temporary URL: we will need a ``stable`` dev environment
  my $url = 'https://dougmeli:Flora@rgalli2-dev.uvnv.com/pr/internal/2/ultra/api/internal__ProxyNotification';

  my $data =
  {
    operation => $operation,
    arguments => $json_coder->encode( $parameters ),
  };

  my ($response,$timeout) = Ultra::Lib::Util::Miscellaneous::userAgentRequest($url,$data,undef,undef);

  if ( $timeout )
  {
    $mq_endpoint->log( "internal__ProxyNotification timed out" );
  }
  else
  {
    # $response is a HTTP::Response object
    if ( ref $response )
    {
      $mq_endpoint->log( "response = ".$response->decoded_content );
    }
    else
    {
      $mq_endpoint->log( "userAgentRequest did not return a HTTP::Response object : $response" );
    }
  }
}

sub store_notification_response
{
  my ( $amdocsNotificationObject , $parameters , $operation , $json_coder , $mq_endpoint ) = @_;

  if ( $operation eq 'PortOutRequest' )
  {
    $mq_endpoint->log("store_notification_response operation = $operation ; parameters = ".$json_coder->encode( $parameters ));

    my $xml = Ultra::Lib::Util::XML::xmlify( $operation . 'Response' , $parameters );

    $mq_endpoint->log( $xml );

    my $storeSOAPXMLParams = {
      data_xml         => $xml,
      type_id          => $amdocsNotificationObject->TYPE_ID_NOTIFICATION_OUTBOUND
    };

    # store outbound SOAP XML into DB.
    my $success = $amdocsNotificationObject->storeSOAPXML( $storeSOAPXMLParams );
  }
}

sub get_wsdl_file_name
{
  my ( $config , $environment ) = @_;

  my $wsdl_file = $config->find_credential('amdocs/notifications/soap/wsdl');

  if ( ( defined $environment ) && ( $environment eq 'mw-01' ) )
  {
    $wsdl_file =~ s/production/mw-01/;
    $wsdl_file =~ s/development/mw-01/;
  }

  if ( ( defined $environment ) && ( $environment eq 'development' ) )
  {
    $wsdl_file =~ s/production/development/;
  }

  return $wsdl_file;
}

__END__

This is the server for ACC notifications.
It can run in two modalities: development and production. The only difference between those two is the WSDL file used.
Incoming SOAP data (XML) is stored into ULTRA_ACC..SOAP_LOG ;
afterwards it is serialized into an inbound ACC Notification message and sent to the ACC Notification Message Queue.


To start:

DEV:
  sudo /etc/init.d/acc_notification_server-dev start &

PROD:
  sudo /etc/init.d/acc_notification_server-mw-01 start &

Starting acc_notification_server-dev:
info: switching to run mode 1, accept INFO-
info: switching to run mode 3, accept ALL
info: [Thu May 30 07:28:29 2013] [8648] wsdl = XML::Compile::WSDL11=HASH(0x24307e8)
info: added 5 operations from WSDL
SOAP11:
   PortOutDeactivation
   NotificationReceived
   PortOutRequest
   ThrottlingAlert
   ProvisioningStatus
info: [Thu May 30 07:28:29 2013] [8648] socket = IO::Socket::SSL=GLOB(0x26afa58


To kill:
  pkill -f -u rgalli acc_notification_server
  or
  sudo pkill -f -u apache acc_notification_server


TODO: I sometime see this error message in the log : ``info: connection ended with force; timeout``

Testing procedure:
- send a notification using /cgi_util/acc_mw_prober.cgi or runners/amdocs/acc_mw_prober.pl
- check the log /var/log/htt/acc_notification_server.log
- execute the runner command middleware__PollAccNotification
- execute the runner command middleware__PollUltraNotification

