#!/usr/bin/perl


use warnings;
use strict;


# the order of the following modules inclusions is VERY IMPORTANT, do not mix them
use Data::Dumper;
use Crypt::SSLeay;
use LWP::UserAgent 6;
use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;
use JSON::XS;
use Any::Daemon;
use File::Basename;
use Time::HiRes qw( time sleep );
use FindBin;
use Ultra::Lib::Amdocs::Base;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::MQ::ControlChannel;
use Ultra::Lib::Util::Config;
use Sys::Hostname;

use lib "$FindBin::Bin/";

use POSIX qw(strftime);

use Devel::Size qw(total_size);


# forces a flush right away and after every write or print on the currently selected output channel.
$| = 1;


my $foreground = shift;
my $dev        = shift;


my $maxChilds = $foreground ? 2 : ( $dev ? 18 : 55 ) ;


my $config = Ultra::Lib::Util::Config->new();


my $wsdl_file     = $config->find_credential('amdocs/control/soap/wsdl');
my $redis_host    = $config->find_credential('redis/host');
my $mw_db_host    = $config->find_credential('mw/db/host');
my $ssl_cert_file = $config->find_credential('amdocs/certificate/filename');
my $ssl_cert_pwd  = $config->find_credential('amdocs/certificate/password');
my $end_point     = ( $dev ? 'https://acc-prod.proxy.ultra.me:8084/acc/inbound.aspx' : 'https://acc.mvne2.t-mobile.com/acc/inbound.aspx' );

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = "0";
$ENV{HTTPS_DEBUG}                  = 1;
$ENV{HTTPS_CERT_FILE}              = $ssl_cert_file;
$ENV{HTTPS_KEY_FILE}               = $ssl_cert_file;

my $ssl_opts = {
    SSL_cert_file => $ssl_cert_file,
    SSL_key_file  => $ssl_cert_file,
    SSL_passwd_cb => sub { return $ssl_cert_pwd; },
    SSL_version   => 'SSLv3'
};

# it will be set to 1 in case we need to shut this down
my $shutdownRequested = 0;
my $init = 1;
my $compiledWSDL = {};
my $thisHostName = hostname;
$thisHostName =~ s/[\s\W]*//g;
my $log_file = '/var/log/htt/acc_client_'.$thisHostName.'.log';


use Log::Report mode => 'VERBOSE';


my $activeCommunication = '';
$SIG{USR1} = sub
{
  if ($activeCommunication)
  {
    info "Shutdown requested! Exiting when the current communication is done.";
    $shutdownRequested = 1;
  }
  else
  {
    info "Shutdown requested! Exiting promptly.";
    exit 0;
  }
};


# set up our logs
dispatcher 'FILE', 'log', mode => 'DEBUG', to => $log_file , format => sub { sprintf('[%s] [%s] ', ( strftime "%m\/%e\/%y %H:%M:%S", localtime ), $$).$_[0] } ;


my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();
my $pid_mgmt   = {};


print STDERR "wsdl_file     = $wsdl_file\n";
print STDERR "redis_host    = $redis_host\n";
print STDERR "ssl_cert_file = $ssl_cert_file\n";
print STDERR "end_point     = $end_point\n";
print STDERR "environment   = ".($dev?'DEV':'PROD')."\n";
print STDERR "log file      = $log_file\n";


# Load and compile all the WSDL files.
my $wsdl = XML::Compile::WSDL11->new($FindBin::Bin."/".$wsdl_file);

# obtained with  $wsdl->compileCalls(); $wsdl->printIndex;
my $acc_commands = [ qw/
  ActivateSubscriber
  CancelDeviceLocation
  CancelPortIn
  ChangeIMEI
  ChangeMSISDN
  ChangeSIM
  CheckBalance
  DeactivateSubscriber
  GetNGPList
  GetNetworkDetails
  ManageCap
  PortInEligibility
  QueryStatus
  QuerySubscriber
  ReactivateSubscriber
  RenewPlan
  ResendOTA
  ResetVoiceMailPassword
  RestoreSubscriber
  SendSMS
  SuspendSubscriber
  UpdateBalance
  UpdatePlanAndFeatures
  UpdatePortIn
  UpdateSubscriberAddress
  UpdateWholesalePlan/ ];

# compile every command separately
for my $acc_command( @$acc_commands )
{
  my $ua = LWP::UserAgent->new;

  $ua->default_headers->header( 'SOAPAction' => 'http://www.sigvalue.com/acc/'.$acc_command );

  $ua->ssl_opts( %$ssl_opts );

  my $trans = XML::Compile::Transport::SOAPHTTP->new( user_agent => $ua , address => $end_point );

  my $op = $wsdl->operation( $acc_command );

  $wsdl->compileCall( $op , transport => $trans );
}

my $daemon = Any::Daemon->new(
                              pid_file => '/var/run/acc_client.pid'
                              , user => 'apache'
                              , group => 'apache'
                             );

$daemon->run
(   background => ! $foreground,
  , max_childs => $maxChilds,
  , child_task => \&accClientTask
);


# main task of acc_client children processes
sub accClientTask
{
  my $mqControlChannelObject = Ultra::Lib::MQ::ControlChannel->new(
    JSON_CODER => $json_coder ,
    CONFIG     => $config
  );

  # distribute processes evenly. 8-)
  sleep ( $$ % $maxChilds ) if $init;

  $init = 0;

  while ( ! $shutdownRequested )
  {
    $activeCommunication = 1;

    # select an outbound request channel + message
    my $getNextOutboundMessageResult = $mqControlChannelObject->getNextOutboundMessage();

    if ( @$getNextOutboundMessageResult )
    {
      my ( $nextOutboundControlChannel , $message ) = @$getNextOutboundMessageResult;

      $mqControlChannelObject->log("out channel = $nextOutboundControlChannel");
      $mqControlChannelObject->log("out message = $message");

      my $decodedData = $mqControlChannelObject->extractFromMessage($message) || '';

      my $actionUUID = ( $decodedData && ( ref $decodedData ) && ( defined $decodedData->{'_actionUUID'} ) && $decodedData->{'_actionUUID'} )
                     ?
                     $decodedData->{'_actionUUID'}
                     :
                     'UNKNOWN_ACTION'
                     ;

      # MW message creation timestamp
      my $timestamp = ( $decodedData && ( ref $decodedData ) && ( defined $decodedData->{'_timestamp'} ) && $decodedData->{'_timestamp'} )
                     ?
                     $decodedData->{'_timestamp'}
                     :
                     int(time)
                     ;

      my $secondsDiff = int(time) - $timestamp ;

      my $timeout = 0;

      my $inboundResponseMessage;

      if ( $secondsDiff >= $mqControlChannelObject->getRequestReplyTimeoutSeconds() )
      {
        $mqControlChannelObject->log("Message expired after $secondsDiff seconds");

        $inboundResponseMessage = $mqControlChannelObject->buildMessage({
          'actionUUID' => $actionUUID,
          'header'     => 'error',
          'body'       => { 'errors' => [ 'MW Message timeout' ] },
        });
      }
      else
      {
        # here we send a command to Amdocs and we wait for the response
        my $amdocsOutboundCallResult = amdocsOutboundCall($mqControlChannelObject,$message,$actionUUID);

        if ( $amdocsOutboundCallResult->{ inboundResponseMessage } )
        {
          $inboundResponseMessage = $amdocsOutboundCallResult->{ inboundResponseMessage };
        }
        else
        {
          my $amdocsOutboundCallErrors = '';

          if ( @ { $amdocsOutboundCallResult->{'errors'} } )
          {
            $amdocsOutboundCallErrors = $json_coder->encode( $amdocsOutboundCallResult->{'errors'} );
          }
          else
          {
            $amdocsOutboundCallErrors = "No result from amdocsOutboundCall";
          }

          $inboundResponseMessage = $mqControlChannelObject->buildMessage({
            'actionUUID' => $actionUUID,
            'header'     => 'error',
            'body'       => { 'errors' => $amdocsOutboundCallErrors },
          });
        }
      }

      my $nextInboundControlChannel = $mqControlChannelObject->toInbound( $nextOutboundControlChannel );

      $mqControlChannelObject->log("in channel = $nextInboundControlChannel");
      $mqControlChannelObject->log("in message = $inboundResponseMessage");

      my $controlUUID = $mqControlChannelObject->sendToControlChannel( $nextInboundControlChannel , $inboundResponseMessage );

      $mqControlChannelObject->log("controlUUID = $controlUUID");

      $pid_mgmt->{ $$ }++;

      # check to avoid known memory leak
      if ( $pid_mgmt->{ $$ } > 64 )
      {
        delete $pid_mgmt->{ $$ };
        exit 0;
      }

    }
    else
    {
      #$mqControlChannelObject->log("got no message ($init)");
    }

    $activeCommunication = 0;

    sleep 1; # TODO: configure this together with $maxChilds in order to tune MQ poll frequency per minute.
  }

  exit 0;
}


sub amdocsOutboundCall
{
  my ($mqControlChannelObject,$message,$actionUUID) = @_;

  $mqControlChannelObject->log("[$actionUUID] amdocsOutboundCall invoked with message = $message");

  local $@;

  my $amdocsOutboundCallResult =
  {
    'success'                => 0,
    'errors'                 => [],
    'inboundResponseMessage' => '',
  };

  # Extract header (command) and body (data) from outbound message.
  my $decodedData = $mqControlChannelObject->extractFromMessage($message) || '';

  #$mqControlChannelObject->log( "decodedData = " . $json_coder->encode( $decodedData ) );

  if ( ! $decodedData )
  {
    $amdocsOutboundCallResult->{'errors'} = ["Could not extract any information from message $message"];
    return $amdocsOutboundCallResult;
  }

  if ( ref $decodedData ne 'HASH' )
  {
    $amdocsOutboundCallResult->{'errors'} = ["Malformed message $message"];
    return $amdocsOutboundCallResult;
  }

  if ( ! $decodedData->{ header } )
  {
    $amdocsOutboundCallResult->{'errors'} = ["Missing header in message $message"];
    return $amdocsOutboundCallResult;
  }

  if ( ! $decodedData->{ body } )
  {
    $amdocsOutboundCallResult->{'errors'} = ["Missing body in message $message"];
    return $amdocsOutboundCallResult;
  }

  if ( ref $decodedData->{ body } ne 'HASH' )
  {
    $amdocsOutboundCallResult->{'errors'} = ["Invalid body in message $message"];
    return $amdocsOutboundCallResult;
  }

  my $command = $decodedData->{ header };
  my $params  = $decodedData->{ body   };
  my $msisdn  = ( ( defined $decodedData->{ body }->{ MSISDN } ) ? $decodedData->{ body }->{ MSISDN } : '' );
  my $iccid   = ( ( defined $decodedData->{ body }->{ ICCID  } ) ? $decodedData->{ body }->{ ICCID  } : '' );
  my $correlationID = ( ( defined $decodedData->{ body }->{ CorrelationID } ) ? $decodedData->{ body }->{ CorrelationID } : undef );
  my $tag           = ( $correlationID ) ? $correlationID : $decodedData->{ _uuid } ;

  # paid event support
  my $makeitsoQueueId = defined $decodedData->{ makeitsoQueueId } ? $decodedData->{ makeitsoQueueId } : '';
  my $paidEvent       = defined $decodedData->{ paidEvent }       ? $decodedData->{ paidEvent }       : '';
  my $socName         = defined $decodedData->{ socName }         ? $decodedData->{ socName }         : '';

  # attempts to retrieve ICCID and MSISDN from $decodedData
  if ( !$iccid
    && ( defined $decodedData->{ body }->{ ResourceData } )
    && ( ref $decodedData->{ body }->{ ResourceData } eq 'HASH' )
    && ( defined $decodedData->{ body }->{ ResourceData }->{ Subscriber_ResourceData } )
    && ( ref $decodedData->{ body }->{ ResourceData }->{ Subscriber_ResourceData } eq 'ARRAY' )
    && ( @ { $decodedData->{ body }->{ ResourceData }->{ Subscriber_ResourceData } } )
  )
  {
    for my $resourceData( @ { $decodedData->{ body }->{ ResourceData }->{ Subscriber_ResourceData } } )
    {
      if ( ( ref $resourceData eq 'HASH' )
        && ( defined $resourceData->{ productCategory } )
      )
      {
        if ( ( $resourceData->{ productCategory } eq 'SIM' )
          && ( defined $resourceData->{ serialNumber } )
        )
        {
          $iccid = $resourceData->{ serialNumber };
        }
        elsif ( ( $resourceData->{ productCategory } eq 'MSISDN' )
          && ( defined $resourceData->{ serialNumber } )
          && !$msisdn
        )
        {
          # MSISDN from $decodedData in case of PortIn attempts
          $msisdn = $resourceData->{ serialNumber };
        }
      }
    }
  }

  # check memory allocation
  #$mqControlChannelObject->log('Memory report: $wsdl:'.total_size($wsdl).' mqControlChannelObject:'.total_size($mqControlChannelObject).' daemon:'.total_size($daemon));

  # Do the actual call.
  # $trace will let us log the request and the response.
  my ($answer,$trace,$start_time) = (undef,undef,scalar(time));

  eval
  {
    ($answer,$trace) = $wsdl->call( $command => { parameters => $params } );
  };

  $mqControlChannelObject->log('ACC Sync '.$command.' call duration = '.substr((scalar(time)-$start_time),0,5));

  if ( $@ ) # error originating from the last eval
  {
    if ( ref $@ eq 'Log::Report::Exception' )
    {
      push( @ { $amdocsOutboundCallResult->{'errors'} } , $@->toString() );
      $mqControlChannelObject->log("*ERROR*: ".$@->toString());
    }
    else
    {
      my $error = Dumper($@); # TODO: clean this string
      push( @ { $amdocsOutboundCallResult->{'errors'} } , $error );
      $mqControlChannelObject->log("*ERROR*: $error");
    }
  }
  elsif ( defined $trace ) # now $trace is a XML::Compile::SOAP::Trace
  {
    # connect to MW DB
    my $db = Ultra::Lib::DB::MSSQL->new(
      DB_NAME => 'ULTRA_ACC',
      DB_HOST => $mw_db_host,
      CONFIG  => $mqControlChannelObject->{ CONFIG }
    );

    my $amdocsBaseObject = Ultra::Lib::Amdocs::Base->new(
      JSON_CODER  => $json_coder,
      CONFIG      => $mqControlChannelObject->{ CONFIG },
      DB          => $db,
      COMMAND     => $command,
      ACTION_UUID => $actionUUID,
      MSISDN      => $msisdn,
      ICCID       => $iccid,
      TAG         => $tag,
      PAID_EVENT  => $paidEvent,
    );

    # store $iccid for the given CorrelationID
    if ( $iccid && $correlationID )
    {
      $mqControlChannelObject->setICCIDByCorrelationID($iccid,$correlationID);
    }

    my $request  = $trace->request;   # HTTP message which was sent
    my $response = $trace->response;  # HTTP message received

    # store SOAP XML request into DB.
    my $success = $amdocsBaseObject->storeSOAPXML({
      data_xml              => $request->content,
      type_id               => $amdocsBaseObject->TYPE_ID_CONTROL_OUTBOUND,
      soap_date_add_time_ms => - ( substr((scalar(time)-$start_time),0,5) * 1000 ),
    });

    if ( $amdocsBaseObject->hasErrors() )
    {
      $mqControlChannelObject->log("storeSOAPXML errors: ".$json_coder->encode( $amdocsBaseObject->getErrors() ));
    }

    my %duration = (total => sprintf("%.3f", 1000*$trace->elapse()));

    foreach (qw/encode stringify connect parse decode/)
    {
      $duration{$_} = sprintf("%.3f", 1000*$trace->elapse($_));
    }

    # verbose log for SOAP info
    $mqControlChannelObject->log("trace post     [] " . $request->as_string);
    $mqControlChannelObject->log("trace response [] " . $response->as_string);

    #$mqControlChannelObject->log("trace post     content [] " . $request->content);
    #$mqControlChannelObject->log("trace response content [] " . $response->content);

    $amdocsOutboundCallResult->{'success'} = $response->is_success;

    if ( $response->is_success )
    {
      my $responseData = $amdocsBaseObject->extractResponseData( $response->content );

      if ( $responseData )
      {
        # retrieve serviceTransactionId if present
        if ( ( defined $responseData->{ serviceTransactionId } ) && $responseData->{ serviceTransactionId } )
        {
          $amdocsBaseObject->{ SESSION_ID } = $responseData->{ serviceTransactionId };

          $mqControlChannelObject->log('ACC Sync '.$command.' call duration = '.substr((scalar(time)-$start_time),0,5).' for trans '.$responseData->{ serviceTransactionId });
        }

        my $storeSOAPXMLParams = {
          data_xml           => $response->content,
          type_id            => $amdocsBaseObject->TYPE_ID_CONTROL_INBOUND,
          makeitso_queue_id  => $makeitsoQueueId,
          soc_name           => $socName,
        };

        # retrieve ResultCode if present
        if ( ( defined $responseData->{ ResultCode } ) && $responseData->{ ResultCode } )
        {
          $storeSOAPXMLParams->{ ResultCode } = $responseData->{ ResultCode };
        }

        # retrieve MSISDN if present
        if ( ( ( ! exists $amdocsBaseObject->{ MSISDN } ) || ( ! $amdocsBaseObject->{ MSISDN } ) ) && ( exists $responseData->{ MSISDN } ) && ( $responseData->{ MSISDN } ) )
        {
          $amdocsBaseObject->{ MSISDN } = $responseData->{ MSISDN };
        }

        # store SOAP XML response into DB.
        my $success = $amdocsBaseObject->storeSOAPXML( $storeSOAPXMLParams );

        if ( $amdocsBaseObject->hasErrors() )
        {
          $mqControlChannelObject->log("storeSOAPXML errors: ".$json_coder->encode( $amdocsBaseObject->getErrors() ));
        }

        $mqControlChannelObject->log("content for body = ".$json_coder->encode( $responseData ));

        # prepare inbound response message
        $amdocsOutboundCallResult->{'inboundResponseMessage'} = $mqControlChannelObject->buildMessage({
          'header'     => $command,
          'body'       => $responseData,
          'actionUUID' => $actionUUID,
        });
      }
      else
      {
        $mqControlChannelObject->log("Error : ".Dumper( $amdocsBaseObject->getErrors() ));

        # prepare inbound response message
        $amdocsOutboundCallResult->{'inboundResponseMessage'} = $mqControlChannelObject->buildMessage({
          'header'     => $command,
          'body'       => { 'errors' => $json_coder->encode( $amdocsBaseObject->getErrors() ) },
          'actionUUID' => $actionUUID,
        });
      }
    }
    else
    {
      $mqControlChannelObject->log("trace response error");

      my $error = "amdocsOutboundCall request failed : ".$response->status_line;
      push( @ { $amdocsOutboundCallResult->{'errors'} } , $error );
      $mqControlChannelObject->log("*ERROR*: $error");
    }

    # force garbage collection
    undef $db;
    undef $amdocsBaseObject;
  }
  else
  {
    my $error = "no trace available after command $command";
    push( @ { $amdocsOutboundCallResult->{'errors'} } , $error );
    $mqControlChannelObject->log("*ERROR*: $error");
  }

  # final logging
  #$mqControlChannelObject->log("amdocsOutboundCall ".( $amdocsOutboundCallResult->{'success'} ? 'success' : 'failure' ));
  $mqControlChannelObject->log("amdocsOutboundCall inboundResponseMessage = ".$amdocsOutboundCallResult->{'inboundResponseMessage'});
  if ( @ { $amdocsOutboundCallResult->{'errors'} } )
  {
    $mqControlChannelObject->log("amdocsOutboundCall errors = ".join(" ; ", @ { $amdocsOutboundCallResult->{'errors'} } ));
  }

  return $amdocsOutboundCallResult;
}


__END__


This is the client for ACC Control Commands.
It polls Control messages from the ACC MW MQ and invokes SOAP requests to Amdocs.
This script and the modules it utilizes are not taking any decision about the data transmitted in the SOAP calls and returned in SOAP responses.
The sole purpose of this script is to:
- retrieve outbound messages from the ACC Control Message Queue
- compose appropriate SOAP requests to Amdocs
- enqueue inbound messages which include data obtained in SOAP responses
- store SOAP data (XML) into ULTRA_ACC..SOAP_LOG (both request and response)


How to start this script: (background)

DEV:
%> sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli_acc_dev HTT_CONFIGROOT=/home/ht/config/rgalli_acc_dev DOCUMENT_ROOT=/home/ht/www/rgalli_acc_dev/web ./runners/amdocs/acc_client.pl 0'
or
%> sudo /etc/init.d/acc_client_dev start

PROD:
%> sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=mw_acc_prod HTT_CONFIGROOT=/home/ht/config/mw_acc_prod DOCUMENT_ROOT=/home/ht/www/mw_acc_prod/web ./runners/amdocs/acc_client.pl 0'

or
%> sudo /etc/init.d/acc_client_prod start


How to shutdown gently:

%> sudo pkill -USR1 -f "acc_client"
or
%> sudo /etc/init.d/acc_client_dev stop
or
%> sudo /etc/init.d/acc_client_prod stop


Useful tests:

 my $command = 'QuerySubscriber';
 %> print $wsdl->explain($command, PERL => 'INPUT', recurse => 1);

 Messages can be enqueued with:
 %> perl runners/amdocs/acc_mw_prober.pl --acc_mq 'QuerySubscriber' --data 'MSISDN=6038920283&UserData__senderId=x&UserData__timeStamp=2006-10-06T00:23:02Z&UserData__channelId=ULTRA'

