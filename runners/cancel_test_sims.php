<?php

/*
 * a runner that cancels active test sims
 * on the network older than N days
 */

require_once 'db.php';
require_once 'classes/CancelTestSims.php';

// main
\logit('start execution');
$cancelTestSims = new CancelTestSims();
$cancelTestSims->main();
\logit('stop execution');
