<?php

/*
Identical to middleware__PollUltraControl

Purpose: Extracts an Outbound Control Message from the Ultra MW Message queue and process it.
*/

require_once 'db.php';
require_once 'Ultra/Lib/MQ/ControlChannel.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';

if ( ! \Ultra\UltraConfig\middleware_enabled_amdocs() )
{
  dlog('','middleware currently disabled');
}
else
{
  $communication = 'synchronous';

  if ( isset($argv[1]) )
    $communication = $argv[1];

  // default value
  if ( $communication != 'asynchronous' )
    $communication = 'synchronous';

  \logDebug("communication = $communication");

  $controlChannel = new \Ultra\Lib\MQ\ControlChannel;

  // select an outbound ( Synch or Asynch ) channel + message
  $getNextOutboundMessageResult = $controlChannel->getNextOutboundMessage( ( ! ! ( $communication == 'asynchronous' ) ) );

  if ( count($getNextOutboundMessageResult) )
  {
    dlog('',"%s",$getNextOutboundMessageResult);

    list( $nextOutboundControlChannel , $message ) = $getNextOutboundMessageResult;

    dlog('',"outbound message = $message");

    $extractedData = $controlChannel->extractFromMessage( $message );

    dlog('',"extractedData = %s",$extractedData);

    if ( !property_exists( $extractedData , 'header' ) || !$extractedData->header )
    {
      dlog('','error : No header in message');
      exit;
    }

    dlog('',$extractedData->_actionUUID." command = %s , parameters = %s",$extractedData->header,(array) $extractedData->body);

    // $message must be processed by the MW layer and should trigger one or more SOAP requests.

    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

    // execute the $extractedData->header command synchronously
    $controlCommandResult = $accMiddleware->processControlCommand(
      array(
        'command'    => $extractedData->header,
        'parameters' => (array) $extractedData->body,
        'actionUUID' => $extractedData->_actionUUID,
        'timestamp'  => $extractedData->_timestamp
      )
    );

    // asynchronous
    if ( $communication == 'asynchronous' )
    {
      dlog('',"asynchronous controlCommandResult = %s",$controlCommandResult);
      dlog('',"asynchronous controlCommandResult data = %s",$controlCommandResult->data_array);
      dlog('',"uuid = %s , errors = %s",$extractedData->_actionUUID,$controlCommandResult->get_errors());

      exit;
    }

    if ( $controlCommandResult )
    {
      dlog('',"controlCommandResult = %s",$controlCommandResult);

      if ( $controlCommandResult->is_success() )
        $inboundReply = $controlCommandResult->get_data_key('message');
      else
      {
        $body            = $controlCommandResult->data_array;
        $body['errors']  = $controlCommandResult->get_errors();
        $body['success'] = FALSE;

        $inboundReply = $controlChannel->buildMessage(
          array(
            'header'     => $extractedData->header,
            'body'       => $body,
            'actionUUID' => $extractedData->_actionUUID
          )
        );
      }
    }
    else
      $inboundReply = $controlChannel->buildMessage(
        array(
          'header'     => $extractedData->header,
          'body'       => array( 'errors' => array('processControlCommand did not return a Result') , 'success' => FALSE ),
          'actionUUID' => $extractedData->_actionUUID
        )
      );

    $nextInboundControlChannel = $controlChannel->toInbound( $nextOutboundControlChannel );

    dlog('',"inbound channel = $nextInboundControlChannel");
    dlog('',"inbound message = %s",$inboundReply);

    $controlUUID = $controlChannel->sendToControlChannel( $nextInboundControlChannel , $inboundReply );

    dlog('',"controlUUID = $controlUUID");
  }
}

