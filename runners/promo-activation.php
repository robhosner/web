<?php

// activate promo SIMS Batches

include_once('core.php');
include_once('web.php');
include_once('test/api/test_api_include.php');
require_once 'db.php';
include_once('Ultra/Lib/Util/Settings.php');

teldata_change_db();

/*
$settings = new \Ultra\Lib\Util\Settings;

$value = $settings->checkUltraSettings('runner/promo-activation/enabled');

dlog('',"runner/promo-activation/enabled => $value");

while ( ( $value == 'FALSE' ) || ( $value == '0' ) )
{
  dlog('',"Sleeping for 60 seconds since 'runner/promo-activation/enabled' is set to FALSE");
  sleep(60);
  $value = $settings->checkUltraSettings('runner/promo-activation/enabled');
}
*/

$hour = date('H') + 0;

# 25 for calls made before 04:00
$prepare_count = ($hour < 4 ) ? 45 : 24 ;

echo "hour = $hour ; prepare_count = $prepare_count\n";

$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
    CURLOPT_USERPWD  => "$apache_username:$apache_password",
    CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);

$url = find_site_url().'/pr/promotional/1/ultra/api/promotional__ListPromotionsForPreparation';

$params = array();

$json_result = curl_post($url,$params,$curl_options,NULL,240);

$result = json_decode($json_result);

print_r($result);

if ( !$result->success )
  die("Success is false");

if ( count($result->errors) )
  die("There are errors");

$promo_ids = array();

// loop through promotions
foreach( $result->promotions as $promotion )
{
  if ( $promotion->promo_status == 'PREPARATION' )
  {
    $promo_ids[] = $promotion->id;
  }
}

if ( ! count($promo_ids) )
  die("No Promotions in PREPARATION status");

// get one of the promos which is in PREPARATION status
$promo_id = $promo_ids[ array_rand ( $promo_ids ) ];

echo "ID:$promo_id\n";

$htt_transition_log_open_count = htt_transition_log_open_count();

echo "htt_transition_log_open_count = $htt_transition_log_open_count\n";

// transition sanity check
if ( $htt_transition_log_open_count < 50 )
{
  $url = find_site_url().'/pr/promotional/1/ultra/api/promotional__PreparePromoCustomers';

  $params = array(
  	'promo_id'      => $promo_id,
    'prepare_count' => $prepare_count 
  );
  
  $json_result = curl_post($url,$params,$curl_options,NULL,2400);
  
  $result = json_decode($json_result);
  
  print_r($result);
}

?>
