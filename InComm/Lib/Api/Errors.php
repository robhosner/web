<?php

namespace InComm\Lib\Api\Errors;

require_once 'Ultra/Lib/Api/Errors.php';

/**
 * decodeToUserError
 *
 * Decode error codes to user friendly errors
 *
 * @return string
 */
function decodeToUserError( $error_code , $language='EN' , $errorCodes=NULL , $redis=NULL )
{
  return \Ultra\Lib\Api\Errors\decodeToUserError( $error_code , $language , $errorCodes , $redis );
}

/**
 * errorCodes
 *
 * Map error codes to user friendly errors
 *
 * @return array or object
 */
function errorCodes($language='EN',$redis=NULL)
{
  return \Ultra\Lib\Api\Errors\errorCodes( $language , $redis );
}

