#!/usr/bin/perl


use strict;
use warnings;


use lib "$ENV{DOCUMENT_ROOT}";


use Data::Dumper;
use JSON::XS;
use Redis;
use Ultra::Lib::Amdocs::Base;
use Ultra::Lib::Amdocs::Control::Prober;
use Ultra::Lib::Amdocs::Notification;
use Ultra::Lib::Amdocs::Notification::Prober;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::WSDL11;


use CGI;


my $cgi = CGI->new;

if ( isDirectRequest($cgi) )
{
  print serveDirectRequest($cgi);
}
else
{
  print serveMainPage($cgi);
}

sub isDirectRequest
{
  my $cgi = shift;

  return $cgi->param('DirectRequest');
}

# serve request
sub serveDirectRequest
{
  my $cgi = shift;

  return
    $cgi->header().
    performRequest( $cgi )
  ;
}

# perform server-side request
sub performRequest
{
  my ($cgi) = @_;

  my ($type,$command) = ( $cgi->param('type') , $cgi->param('command') );

  print STDERR "performRequest ".$cgi->param('type')." , ".$cgi->param('command')."\n";

  my $requestResult = "";

  my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

  my $config = Ultra::Lib::Util::Config->new();

  if ( $type eq 'control')
  {
    print STDERR $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/control/soap/wsdl')."\n";

    my $controlProber = Ultra::Lib::Amdocs::Control::Prober->new(
      JSON_CODER  => $json_coder,
      LOG_ENABLED => 1,
      WSDL_FILE   => $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/control/soap/wsdl')
    );

    my $parameters = getCGIParametersForSOAPCall($cgi);

    $requestResult = $controlProber->probeControlCommand( $command , $parameters );
  }
  elsif ( $type eq 'notification')
  {
    print STDERR $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/notifications/soap/wsdl')."\n";

    my $notificationProber = Ultra::Lib::Amdocs::Notification::Prober->new(
      JSON_CODER  => $json_coder,
      LOG_ENABLED => 1,
      WSDL_FILE   => $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/notifications/soap/wsdl')
    );

    my $parameters = getCGIParametersForSOAPCall($cgi);

    $requestResult = $notificationProber->probeNotification( $command , $parameters );
  }
  else
  {
    $requestResult->{'soapRequest'} = 'Type not mapped';
  }

  if ( $requestResult && ( ref $requestResult ) )
  {
    $requestResult =
      $cgi->start_html( -title  => 'MVNE_prober.cgi' ).
      $cgi->h3( "$command Result:" ).q|
    <table>
      <tr>
        <td>
          Request:
        </td>
        <td>
          <textarea rows="20" cols="120" id="soap_synch_request" readonly>|.$requestResult->{'soapRequest'}.q|</textarea>
        </td>
      </tr>
      <tr>
        <td>
          Response:
        </td>
        <td>
          <textarea rows="20" cols="120" id="soap_synch_response" readonly>|.$requestResult->{'soapResponse'}.q|</textarea>
        </td>
      </tr>
      <tr>
        <td>
          Outcome:
        </td>
        <td>
          |.$requestResult->{'outcome'}.q|
        </td>
      </tr>
    </table>|.
    $cgi->end_html;
  }

  return $requestResult;
}

# main HTML form
sub htmlForm
{
  my $platform = shift;

  my $optionsByPlatform =
  {
    'MVNE2' => q|
              <option value="control"     >Control     </option>
              <option value="notification">Notification</option>
|,
  };

  return '
  <div id="main_form">
    <form name="" action="MVNE_prober.cgi">
      <table>
        <tr>
          <td>
            Type:
          </td>
          <td>
            <select name="type" id="type">
              <option value="" SELECTED   >-- choose --</option>'.$optionsByPlatform->{ $platform }.'
            </select>
          </td>
        </tr>
        <tr>
          <td>
            Command:
          </td>
          <td>
            <select name="command" id="command">
              <option value="">-- you have to choose a type --</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            WSDL:
          </td>
          <td>
            <div id="wsdl_file">
            </div>
          </td>
        <tr>
          <td>
            Data:
          </td>
          <td>
            <div id="command_data">
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div id="div_submit_button">
              <input type="button" value="Submit" id="submit">
            </div>
            <div id="div_loading">
              <img src="../images/load_20.gif" alt="" />
            </div>
          </td>
          <td>
          </td>
        </tr>
      </table>
    </form>
  </div>
  <div id="div_ajax">
  </div>
  ';
}

# display main page: a form which allows to test control commands and notifications
sub serveMainPage
{
  my $cgi = shift;

  my $platform = $cgi->param( 'platform' ) || 'MVNE2' ;

  my $html = htmlForm( $platform );

  my $javascriptCode = getJavascriptCode( $platform );

  return
    $cgi->header().
    $cgi->start_html(
      -title  => 'MVNE_prober.cgi',
      -script => [
                 { -type => 'text/javascript',
                   -src  => '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
                 },
                 { -type => 'text/javascript',
                   -code => $javascriptCode,
                 },
      ],
    ).
    $cgi->h1('MVNE prober').
    $html.
    $cgi->end_html;
}

# input HTML for the given field
sub inputByField
{
  my ($field,$inputConfiguration,$arrayIndex,$platform) = @_;

  my $inputHTML = '';
  my $inputName = $field;
  $inputName =~ s/[\s\"\']//g;
  $inputName =~ s/\:/_/g;

  $inputName =~ s/ARRAYOF/ARRAYOF$arrayIndex/g;

  if (
    ( defined $inputConfiguration->{ $field }->{ default } )
    &&
    ( ref $inputConfiguration->{ $field }->{ default } eq 'ARRAY' )
    )
  {
    $inputHTML = '<select class="SOAP_input" name="SOAP__'.$inputName.'" id="SOAP__'.$inputName.'">';

    for my $value( @ { $inputConfiguration->{ $field }->{ default } } )
    {
      $inputHTML .= '<option value="'.$value.'">'.$value.'</option>';
    }

    $inputHTML .= '</select>';
  }
  else
  {
    my $value =
      ( defined $inputConfiguration->{ $field }->{ default } )
      ?
      $inputConfiguration->{ $field }->{ default }
      :
        ( defined $inputConfiguration->{ $field }->{ default } )
        ?
        $inputConfiguration->{ $field }->{ default }
        :
        $inputConfiguration->{ $field }->{ example }
      ;

    if ( $value eq 'example' ) { $value = ''; }

    my $readonly = '';
    my $generate = '';

    if ( ( $platform eq 'MVNE1' ) && ( $inputName eq 'tag' ) )
    {
      $readonly = ' readonly';
      $generate = '<input type="button" value="Generate" id="generate_button">';
    }

    $inputHTML = '<input class="SOAP_input" type="text" name="SOAP__'.$inputName.'" id="SOAP__'.$inputName.'" value="'.$value.'"'.$readonly.'>'.$generate;
  }

  $field =~ s/ARRAYOF/ARRAYOF$arrayIndex/g;

  $inputConfiguration->{ $field }->{ type }     ||= '';
  $inputConfiguration->{ $field }->{ required } ||= '';

  return
    "<tr>".
    "<td>".$field."</td>".
    "<td>".$inputConfiguration->{ $field }->{ type }."</td>".
    "<td>".$inputConfiguration->{ $field }->{ required }."</td>".
    "<td>".$inputHTML."</td>".
    "<tr>";
}

# forms for all commands
sub formByCommand
{
  my ( $configuration , $platform ) = @_;

  my $formByCommand = {};

  my $alwaysHide = {
    'AmountData :: specificationValue',                                                 1,
    'AmountData :: specificationValueName',                                             1,
    'numberGroup',                                                                      1,
    'PlanData :: BAN',                                                                  1,
    'PortInData :: firstName',                                                          1,
    'PortInData :: fullName',                                                           1,
    'PortInData :: lastName',                                                           1,
    'PortInData :: PortInAddressList :: PortInAddressInfo :: ARRAYOF  :: City',         1,
    'PortInData :: PortInAddressList :: PortInAddressInfo :: ARRAYOF  :: Country',      1,
    'PortInData :: PortInAddressList :: PortInAddressInfo :: ARRAYOF  :: Zip',          1,
    'PortInData :: PortInAddressList :: PortInAddressInfo :: ARRAYOF  :: addressLine2', 1,
    'PortInData :: PortInAddressList :: PortInAddressInfo :: ARRAYOF  :: addressLine1', 1,
    'PortInData :: PortInAddressList :: PortInAddressInfo :: ARRAYOF  :: State',        1,
    'PortInData :: TAXID',                                                              1,
    'ContactData :: AddressList :: AddressInfo :: ARRAYOF  :: Address',                 1,
    'ContactData :: AddressList :: AddressInfo :: ARRAYOF  :: addressFormatType',       1,
    'ContactData :: AddressList :: AddressInfo :: ARRAYOF  :: cityName',                1,
    'ContactData :: AddressList :: AddressInfo :: ARRAYOF  :: stateCode',               1,
  };

  for my $command ( sort keys %$configuration )
  {
    my $inputConfiguration = $configuration->{ $command }->{ INPUT };

    for my $field( sort keys %$inputConfiguration )
    {
      if ( ! exists $alwaysHide->{ $field } )
      {
        $formByCommand->{ $command } .= inputByField($field,$inputConfiguration,0,$platform);

        if ( $field =~ /ARRAYOF/ )
        {
          # we provide 3 possible elements for SOAP arrays
          $formByCommand->{ $command } .= inputByField($field,$inputConfiguration,1,$platform);
          $formByCommand->{ $command } .= inputByField($field,$inputConfiguration,2,$platform);
        }
      }
    }
  }

  return $formByCommand;
}

# get MVNE2 info
sub getMVNE2Components
{
  my $platform = shift;

  return ('','','','') unless ( $platform eq 'MVNE2' );

  my $controlCommands      = '';
  my $notificationCommands = '';

  my $controlForms      = '';
  my $notificationForms = '';

  my $amdocsBase = Ultra::Lib::Amdocs::Base->new();

  my $config = Ultra::Lib::Util::Config->new();

  print STDERR $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/control/soap/wsdl')."\n";
  print STDERR $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/notifications/soap/wsdl')."\n";

  my $wsdlMVNE = XML::Compile::WSDL11->new( $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/control/soap/wsdl') );
  $wsdlMVNE->compileCalls;

  my $wsdlMVNO = XML::Compile::WSDL11->new( $ENV{DOCUMENT_ROOT}.'/runners/amdocs/'.$config->find_credential('amdocs/notifications/soap/wsdl') );
  $wsdlMVNO->compileCalls;

  my $controlCommandsConfig      = $amdocsBase->configuration( $wsdlMVNE );
  my $notificationCommandsConfig = $amdocsBase->configuration( $wsdlMVNO );

  for my $command ( sort keys %$controlCommandsConfig )
  {
    $controlCommands .= '"'.$command.'",
';
  }

  for my $command ( sort keys %$notificationCommandsConfig )
  {
    $notificationCommands .= '"'.$command.'",
';
  }

  chop $controlCommands;
  chop $controlCommands;

  chop $notificationCommands;
  chop $notificationCommands;

  my $formByCommandControl      = formByCommand($controlCommandsConfig , $platform);
  my $formByCommandNotification = formByCommand($notificationCommandsConfig , $platform);

  for my $command ( sort keys %$formByCommandControl )
  {
    $controlForms .= '
      if ( command == "'.$command.'" )
      {
        $("#command_data").html(\''.$formByCommandControl->{ $command }.'\');
      }';
  }

  for my $command ( sort keys %$formByCommandNotification )
  {
    $notificationForms .= '
      if ( command == "'.$command.'" )
      {
        $("#command_data").html(\''.$formByCommandNotification->{ $command }.'\');
      }';
  }

  return (
    $controlCommands,
    $notificationCommands,
    $controlForms,
    $notificationForms,
    $config->find_credential('amdocs/control/soap/wsdl'),
    $config->find_credential('amdocs/notifications/soap/wsdl')
  );
}

# JavaScript code to be included in the HTML page
sub getJavascriptCode
{
  my $platform = shift;

  my (
    $controlCommands,
    $notificationCommands,
    $controlForms,
    $notificationForms,
    $wsdl_control,
    $wsdl_notifications
  ) = getMVNE2Components( $platform );

  return qq|

\$( document ).ready(function() {

    function get_probe() {

      var html =
        '<textarea rows="120" cols="160" readonly>' +
        'Type     : '+ \$("#type").val()    + String.fromCharCode(13) +
        'Command  : '+ \$("#command").val() + String.fromCharCode(13) +
        'Request  :' + String.fromCharCode(13) + \$("#soap_synch_request").val()        + String.fromCharCode(13) +
        'Response :' + String.fromCharCode(13) + \$("#soap_synch_response").val()       + String.fromCharCode(13)
        '</textarea>'
      ;

      return html;

    }

    function export_probe() {

      // http://stackoverflow.com/questions/8197173/how-to-display-a-message-in-a-pop-up-window

      var html = get_probe();

      var win = window.open("about:blank", null, "width=1200,height=900");
      var doc = win.document;
      doc.open("text/html");
      doc.write( html );
      doc.close();

      return false;
    }

    function getControlCommands() {

      var controlCommands = [
        $controlCommands
      ];

      return controlCommands;
    }

    function getNotificationCommands() {

      var notificationCommands = [
        $notificationCommands
      ];

      return notificationCommands;
    }

  |.q|

  // User clicks the Submit button
  $("#submit").click(function() {

    if ( $("#type").val() == '' ) {
      alert("please select a Type");     return false;
    }

    if ( $("#command").val() == '' ) {
      alert("please select a Command");  return false;
    }

    $("#div_ajax").html( " " ) ;
    $("#div_loading").show();
    $("#div_submit_button").hide();

    var formData =
    {
      DirectRequest : 1,
      type          : $("#type").val(),
      command       : $("#command").val()
    };

    $('.SOAP_input').each(function() {

      formData[ this.id ] = $(this).val();

    });

    $.ajax({
      type:     'POST',
      url:      "MVNE_prober.cgi",
      data:     formData,
      error:    function error_MVNE_prober(html) {
                  $("#div_ajax").html( "CGI error" );
                  $("#div_loading").hide();
                  $("#div_submit_button").show();
                },
      success:  function callback_MVNE_prober(html) {
                  $("#div_ajax").html( html );
                  $("#div_loading").hide();
                  $("#div_submit_button").show();
                }
    });

    return false;

  });

  // User selected 'type'
  $("#type").change(function() {

    $("#command").empty();
    $("#div_ajax").empty();
    $("#command_data").html(' ');

    options = '<option value="">-- you have to choose a type --</option>';

    if ( $("#type").val() == 'control' ) {

      controlCommands = getControlCommands();

      options = '<option value="" SELECTED>--choose--</option>';

      for ( var i = 0; i < controlCommands.length ; i++ ) {
        options += '<option value="' + controlCommands[i] + '">' + controlCommands[i] + '</option>';
      }

      $("#wsdl_file").html('|.$wsdl_control.q|');
    }
    else if ( $("#type").val() == 'notification' ) {

      notificationCommands = getNotificationCommands();

      options = '<option value="" SELECTED>--choose--</option>';

      for ( var i = 0; i < notificationCommands.length ; i++ ) {
        options += '<option value="' + notificationCommands[i] + '">' + notificationCommands[i] + '</option>';
      }

      $("#wsdl_file").html('|.$wsdl_notifications.q|');
    }
    else {
      alert('error');
    }

    $("#command").append( options );

  });

  // User selected 'command'
  $("#command").change(function() {

    // prepare data form ( command_data )

    $("#div_ajax").empty();
    $("#command_data").html(' ');

    var command = $("#command").val();

    if ( $("#type").val() == 'control' ) {

      |.$controlForms.q|

    }
    else if ( $("#type").val() == 'notification' ) {

      |.$notificationForms.q|

    }

  });

  $("#div_loading").hide();

});

  |;
}

# get SOAP parameters from CGI object
sub getCGIParametersForSOAPCall
{
  my ($cgi) = @_;

  my $soapParameters = {};

  my @names = $cgi->param;

  for my $cgiParam( @names )
  {
    if ( ( defined $cgi->param( $cgiParam ) ) && ( $cgi->param( $cgiParam ) ne '' ) )
    {
      # verify that this is a parameter we should pass to the SOAP call
      if ( $cgiParam =~ /^SOAP__(\S+)$/ )
      {
        my $soapParam = $1;

        if ( $soapParam =~ /^(\S+)__(\S+)__ARRAYOF(\d+)__(\S+)$/ )
        {
          $soapParameters->{ $1 }->{ $2 }->[ $3 ]->{ $4 } = $cgi->param( $cgiParam );
        }
        elsif ( $soapParam =~ /^(\S+)__(\S+)$/ )
        {
          $soapParameters->{ $1 }->{ $2 } = $cgi->param( $cgiParam );
        }
        else
        {
          $soapParameters->{ $soapParam } = $cgi->param( $cgiParam );
        }
      }
    }
  }

  print STDERR Dumper($soapParameters);

  return $soapParameters;
}

__END__

CGI version of runners/amdocs/acc_mw_prober.pl

