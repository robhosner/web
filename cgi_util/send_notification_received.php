<?php

/*

[12/29/15 12:29:50] [2625] info: Ultra::Lib::DB::MSSQL executeWrite  INSERT INTO SOAP_LOG     ( DATA_XML,COMMAND,SESSION_ID,TYPE_ID,ENV,MSISDN     )     VALUES     ( ?,?,?,?,?,?     ) ["
    <NotificationReceived xmlns=\"http://www.sigvalue.com/acc\">
      <UserData>
        <senderId>MVNEACC</senderId>
        <timeStamp>2015-12-29T12:29:49.654</timeStamp>
      </UserData>
        <MSISDN>16572722505</MSISDN>
        <serviceGrade>SG801</serviceGrade>
        <shortCode>6700</shortCode>
        <messageType>ASCII</messageType>
        <smsText>UPDATA</smsText>
    </NotificationReceived>
","NotificationReceived",null,"3","mw_acc_prod","16572722505"]

*/

?>

<html>
<head>
<style>

p label { float: left; width: 128px; }
#notification { margin: 16px; }
#redButton {
  width: 128px;
  height: 128px;
  background-color: red;
  border: 5px solid #555;
  border-radius: 128px;
  color: white;
  cursor: pointer;
  font-size: 1.25em;
}

</style>

<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

var readyText   = 'SEND';
var workingText = 'WORKING';

var readyColor   = 'red';
var workingColor = '#888';

function internal__SendNotificationReceived()
{
  var c = confirm("Are you sure you wish to send this notification?");
  if (!c) return;

  $('#redButton').css('background-color', workingColor);
  $('#redButton').text(workingText);

  var formData = '';
  var fields = ['env','msisdn','service_grade','short_code','message_type','sms_text'];
  for (var i = 0; i < fields.length; i++)
  {
    if (i) formData += '&';
    formData += fields[i] + '=' + $('#' + fields[i]).val();
  }

  console.log(formData);

  $.ajax({
    method: 'GET',
    url: baseURL + '&command=internal__SendNotificationReceived',
    data: formData,
    success: function(data, status, settings) {
      var response = JSON.parse(data);

      if (response.user_errors && response.user_errors.length > 0)
      {
        alert(response.user_errors[0]);
      }
      else if (response.errors && response.errors.length > 0)
      {
        alert(response.errors[0]);
      }
      else if (response.success !== true)
      {
        alert('Error');
      }
      else
      {
        alert('Success');
      }

      $('#redButton').css('background-color', readyColor);
      $('#redButton').text(readyText);
    }
  });
}
</script>

</head>
<body>

<p>This tool will send a NotificationReceived message to the selected ENV</p>

<div id="notification">
  <p>
    <label>Which env?</label>
    <select name="env" id="env">
      <option value="DEV">DEV</option>
      <option value="QA">QA</option>
    </select>
  <p>
    <label>MSISDN</label>
    <input type="text" name="msisdn" id="msisdn" />
  </p>
  <p>
    <label>serviceGrade</label>
    <input type="text" name="service_grade" id="service_grade" placeholder="SG801" value="SG801" />
  </p>
  <p>
    <label>shortCode</label>
    <input type="text" name="short_code" id="short_code" placeholder="6700" value="6700" />
  </p>
  <p>
    <label>messageType</label>
    <input type="text" name="message_type" id="message_type" placeholder="ASCII" value="ASCII" disabled />
  </p>
  <p>
    <label>smsText</label>
    <input type="text" name="sms_text" id="sms_text" placeholder="UPDATA" />
  </p>

  <button id="redButton" onclick="internal__SendNotificationReceived();">SEND</button>
</div>

</body>
</html>