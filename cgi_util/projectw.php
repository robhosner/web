<?php
chdir('../');
require_once 'db.php';

define( 'FILE_PATH', '/var/tmp/Ultra/ProjectW/' );

// determine date to use
if ( time() < strtotime( 'today 8am' ) )
{
  // if it's before 10am, use yesterday's date
  $date = date( 'Y-m-d',  strtotime( 'yesterday' ) );
}
else
{
  $date = date( 'Y-m-d' );
}

// handle log download
if ( !empty( $_GET['log'] ) )
{
  // sanitize filename
  if ( preg_match( '/[^a-zA-Z0-9.\-]/', $_GET['log'] ) )
  {
    die();
  }

  $fullPath = FILE_PATH . $_GET['log'];
  if ( file_exists( $fullPath ) )
  {
    header( 'Content-Description: File Transfer' );
    header( 'Content-Type: application/octet-stream' );
    header( 'Content-Disposition: attachment; filename="' . basename( $fullPath ). '"' );
    header( 'Expires: 0' );
    header( 'Cache-Control: must-revalidate' );
    header( 'Pragma: public' );
    header( 'Content-Length: ' . filesize( $fullPath ) );
    readfile( $fullPath );
    exit;
  }
}

// handle file upload
if ( !empty( $_POST['filename'] ) && !empty( $_POST['data'] ) && !empty( $_POST['chunk_num'] ) && !empty( $_POST['num_chunks'] ) )
{
  // sanitize filename
  if ( preg_match( '/[^a-zA-Z0-9.\-]/', $_POST['filename'] ) )
  {
    response( false, 'Invalid filename' );
  }

  $fullPath = FILE_PATH . $_POST['filename'];

  if ( $_POST['chunk_num'] == 1 )
  {
    // check file already exists
    if ( file_exists( $fullPath ) )
    {
      response( false, $fullPath . ' already exists' );
    }
  }

  if ( $_POST['chunk_num'] == 1 )
  {
    logInfo( 'Chunk #' . $_POST['chunk_num'] . ' - creating file ' . $fullPath );
  }
  else
  {
    logInfo( 'Chunk #' . $_POST['chunk_num'] . ' - initial filesize of ' . $fullPath . ' = ' . filesize( $fullPath ) );
  }
  logInfo( 'Chunk #' . $_POST['chunk_num'] . ' - writing ' . strlen( $_POST['data'] ) . ' bytes to ' . $fullPath );

  // open file for writing (append mode)
  $file = fopen( FILE_PATH . $_POST['filename'], 'a' );
  if ( !$file )
  {
    response( false, FILE_PATH . $_POST['filename']);
  }

  // append to file
  fwrite( $file, $_POST['data'] );
  fclose( $file );

  logInfo( 'Chunk #' . $_POST['chunk_num'] . ' - final filesize of ' . $fullPath . ' = ' . filesize( $fullPath ) );

  response( true );
}

// handle file delete
if ( !empty( $_POST['filename'] ) && !empty( $_POST['delete'] ) )
{
  if ( !file_exists( FILE_PATH . $_POST['filename'] ) )
  {
    response( false, 'File does not exist' );
  }

  // rename
  $newFilename = str_replace( '.csv', '-REMOVED.csv', $_POST['filename'] );
  if ( rename( FILE_PATH . $_POST['filename'], FILE_PATH . $newFilename ) )
  {
    response( true );
  }
  else
  {
    response( false, 'Failed to rename file' );
  }
}

// check for existing files
$existingFiles = glob( FILE_PATH . 'ProjectW-' . $date . '*.csv' );
if ( count( $existingFiles ) )
{
  $fileNum = sprintf( '%02d', count( $existingFiles ) + 1 );
  $nextFilename = 'ProjectW-' . $date . '-' . $fileNum . '.csv';
}
else
{
  $nextFilename = 'ProjectW-' . $date . '-01' . '.csv';;
}

function response( $success, $message=null )
{
  echo json_encode(array(
    'success' => $success,
    'message' => $message
  ));
  die();
}

?>
<!DOCTYPE html>
<html lang='en'>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <style>
  #files-table, #export-cdr { border-collapse:collapse; width: 1300px; margin: 0px auto; }
  #files-table tr td { padding: 8px 16px 8px 16px; }
  #files-table .num-imported, #files-table .import, #files-table .delete, #files-table .import-log, #files-table .validate-log { text-align: center; }
  #files-table tbody tr:nth-child(odd) { background: #f1f1f1; }
  #files-table tbody tr td:not(:nth-child(1)) { text-align: center; }
  #cdr-date { width: 80px; }

  .stats-table-holder table tr td:first-child { text-align: right; padding-right: 8px !important; }
  .stats-table-holder table tr td { padding: 0 !important; background-color: white !important }
  </style>
</head>
<body>
    <script type="text/javascript">
    chunkSize = 8192;

    // handle file upload
    function uploadFile() {
      var fileElement = document.getElementById('file');
      var file = fileElement.files[0];

      var reader = new FileReader();
      reader.onload = function(progressEvent) {
        // determine chunks
        var filesize = this.result.length;
        var numChunks = Math.ceil(filesize/chunkSize);
        for (i=0; i < numChunks; i++) {
          $.ajax({
            type: 'POST',
            url: '/cgi_util/projectw.php',
            data: {
              filename: '<?php echo $nextFilename ?>',
              data: this.result.substr(i*chunkSize, chunkSize),
              chunk_num: i+1,
              num_chunks: numChunks
            },
            success: function(response) {
              var r = $.parseJSON(response);
              if (!r.success) {
                alert(r.message);
              }
            },
            async: false
          });
        }

        location.reload();
      };
      reader.readAsText(file);
    }

    // check imported files
    $(function() {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=publicprojw&command=publicprojw__GetTodaysFiles',
        success: function(response) {
          var r = $.parseJSON(response);
          if (r.success) {
            // update files table for imported files
            for (var filename in r.files) {
              var file = r.files[filename];
              var row = $('#files-table .filename:contains("' + filename + '")').parent();
              // set imported class
              row.attr('imported', 1);

              row.children('.status').html(file.IMPORT_FILE_STATUS);
              row.children('.date').html(file.IMPORT_FILE_DATE);
              row.children('.num-imported').html(file.NUM_IMPORTED);
              row.children('.unvalidated').html('<a href="#" onClick="validateFile(\'' + filename + '\'); return false;">Validate</a>');
              // export links
              row.children('.export-all').html('<a href="/ultra_api.php?bath=rest&version=2&partner=publicprojw&format=mime&mime=csv&command=publicprojw__ExportFile&filename=' + filename + '">Export All</a>');
              row.children('.export-errors').html('<a href="/ultra_api.php?bath=rest&version=2&partner=publicprojw&format=mime&mime=csv&command=publicprojw__ExportErrors&filename=' + filename + '">Export Errors</a>');
              // start/pause actions
              if (file.IMPORT_FILE_STATUS == 'ONGOING') {
                row.children('.validated').html('<a href="#" onClick="pauseFile(\'' + filename + '\'); return false;">Pause</a>&nbsp;&nbsp;&nbsp;&nbsp;');
                row.children('.validated').append('<a href="#" onClick="completeFile(\'' + filename + '\'); return false;">Complete</a>');
              } else if (file.IMPORT_FILE_STATUS == 'PAUSED') {
                row.children('.validated').html('<a href="#" onClick="startFile(\'' + filename + '\'); return false;">Resume</a>&nbsp;&nbsp;&nbsp;&nbsp;');
                row.children('.validated').append('<a href="#" onClick="completeFile(\'' + filename + '\'); return false;">Complete</a>');
              } else if (file.IMPORT_FILE_STATUS != 'COMPLETED') {
                row.children('.validated').html('<a href="#" onClick="startFile(\'' + filename + '\'); return false;">Start</a>');
              }
            }

            // update files table for non-imported files
            $('#files-table .file:not([imported])').each(function() {
              var filename = $(this).children('.filename').html();
              $(this).children('.status').html('Not Imported');
              $(this).children('.action').html('<a href="#" onClick="importFile(\'' + filename + '\'); return false;">Import</a>');
            });
          } else {
            alert('API call publicprojw__GetTodaysFiles failed');
          }
        },
        error: function() {
          alert('Error making API call publicprojw__GetTodaysFiles')
        }
      });
    });

    function importFile(filename) {
      var row = $('#files-table .filename:contains("' + filename + '")').parent();
      // show loading
      row.children('.action').html('<img src="/images/load_20.gif" />');

      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=projectw&command=projectw__ImportFile',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors importing ' + filename);
          }

          location.reload();
        }
      });
    }

    function validateFile(filename) {
      var row = $('#files-table .filename:contains("' + filename + '")').parent();
      // show loading
      row.children('.action').html('<img src="/images/load_20.gif" />');
      
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=projectw&command=projectw__ValidateFile',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors validating ' + filename);
          }

          location.reload();
        }
      });
    }

    function startFile(filename) {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=projectw&command=projectw__SetStatusOngoing',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors while starting file ' + filename);
          }

          location.reload();
        }
      });
    }

    function pauseFile(filename) {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=projectw&command=projectw__SetStatusPaused',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors while starting file ' + filename);
          }

          location.reload();
        }
      });
    }

    function completeFile(filename) {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=projectw&command=projectw__SetStatusCompleted',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors while completing file ' + filename);
          }

          location.reload();
        }
      });
    }

    function deleteFile(filename) {
      $.ajax({
        type: 'POST',
        url: '/cgi_util/projectw.php',
        data: { filename: filename, delete: 1 },
        success: function(response) {
          var r = $.parseJSON(response);
          if (r.success) {
            location.reload();
          } else {
            alert(r.message);
          }
        }
      });
    }

    function setStatsTable(filename, tableHolderElement)
    {
      $.ajax({
        type: 'POST',
        url: '/ultra_api.php?bath=rest&version=2&partner=publicprojw&command=publicprojw__GetFileStats',
        data: { filename: filename },
        success: function(response) {
          var r = $.parseJSON(response);
          if (!r.success) {
            alert('There were errors while retrieving stats for file ' + filename);
          } else {
            console.log(r);
            if (r.statuses.length == 0) {
              tableHolderElement.html('No data');
            } else {
              var str = '<table>';
              for (var i=0; i < r.statuses.length; i++) {
                var status = r.statuses[i];
                str += '<tr><td>' + status.STATUS + '</td><td>' + status.COUNT + '</td></tr>';
              }
              str += '</table>';

              tableHolderElement.html(str);
            }
          }
        }
      });
    }

    $(function() {
      // handle CDR date picker
      $('#cdr-date').datepicker();
      $('#view-cdr-btn').click(function() {
        if ($('#cdr-date').val() != '')
        {
          window.open('/ultra_api.php?format=mime&mime=csv&version=2&partner=publicprojw&command=publicprojw__ExportCDR&import_date=' + $('#cdr-date').val());
        }
        else
        {
          alert('Please select a date');
        }
      });

      // handle view stats link
      $('.view-stats-link').click(function() {
        $(this).html('Refresh');
        var filename = $(this).parent().parent().children('.filename').html();
        var tableHolderElement = $(this).siblings('.stats-table-holder');

        setStatsTable(filename, tableHolderElement);
      });
    });
    </script>
    
    <center>
    Upload a file: <input type="file" name="file" id="file"> <input type="submit" value="Upload" onclick="uploadFile();" />
    </center>
    <br /><br />

    <div id="export-cdr">
      Enter date for CDR log: <input type="text" id="cdr-date" value="<?php echo date('m/d/Y', strtotime($date)); ?>" /> <input id="view-cdr-btn" type="submit" value="View" />
      <br /><br />
    </div>

    <table id="files-table">
      <thead>
      <tr>
        <th>Filename</th>
        <th>Status</th>
        <th>Date</th>
        <th>Rows<br />Imported</th>
        <th>Import<br />Log</th>
        <th>Validation<br />Log</th>
        <th></th>
        <th></th>
        <th></th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach( $existingFiles as $file ) { ?>
        <?php $filename = str_replace( FILE_PATH, '', $file ); ?>
        <?php $isRemoved = strstr( $filename, 'REMOVED' ) !== false; ?>
        <?php
          // check for import log
          $importLogFilename = str_replace( '.csv', '.import.txt', $filename );
          $hasImportLog = file_exists( FILE_PATH . $importLogFilename );
          $importLogHasErrors = $hasImportLog && filesize( FILE_PATH . $importLogFilename ) != 0 ? true : false;

          // check for validation log
          $validateLogFilename = str_replace( '.csv', '.validation.txt', $filename );
          $hasValidateLog = file_exists( FILE_PATH . $validateLogFilename );
          $validateLogHasErrors = $hasValidateLog && filesize( FILE_PATH . $validateLogFilename ) != 0 ? true : false;

          // hide removed files
          if ( $isRemoved ) continue;
        ?>
        <tr class="file">
          <td class="filename"><?php echo $filename ?></td>
          <td class="status"></td>
          <td class="date"></td>
          <td class="num-imported"></td>
          <td class="import-log">
            <?php if ( $hasImportLog ) { ?>
              <?php if ( $importLogHasErrors ) { ?>
                <a href="/cgi_util/projectw.php?log=<?php echo $importLogFilename ?>" target="_blank">Errors</a>
              <?php } else { ?>
                No errors
              <?php } ?>
            <?php } ?>
          </td>
          <td class="validate-log">
            <?php if ( $hasValidateLog ) { ?>
              <?php if ( $validateLogHasErrors ) { ?>
                <a href="/cgi_util/projectw.php?log=<?php echo $validateLogFilename ?>" target="_blank">Errors</a>
              <?php } else { ?>
                No errors
              <?php } ?>
            <?php } ?>
          </td>
          <td class="view-stats">
            <?php if ( $hasImportLog ) { ?>
            <a href="#" class="view-stats-link">View Import Stats</a>
            <br />
            <div class="stats-table-holder"></div>
            <?php } ?>
          </td>
          <td class="export-all"></td>
          <td class="export-errors"></td>
          <td class="action <?php if ( $hasValidateLog ) echo " validated "; else echo " unvalidated " ?>"></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
</body>
</html>

