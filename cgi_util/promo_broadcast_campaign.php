<?php

/**
 * UI for SMS broadcast campaigns
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+Framework
 */

// this page has two modes of operation: campaign input and campaign progress
// if no campaign ID is given then we display input for a new campaign, otherwise we show the progress of the given campaign

// HTML HEAD is used by both pages
?>
  <!DOCTYPE html>
  <html lang='en'>
  <head>
    <title>SMS Broadcast</title>
    <!-- MVNO-2631: user friendly date picker -->
    <link rel="stylesheet" type="text/css" href="datepicker.css" />
    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script type="text/javascript" src="datepicker.js"></script>
    <script type="text/javascript" src="../js/utilities.js"></script>
    <style>
      body { font-family: Verdana, Arial, serif; }
      hr { border: none; height: 1px; background-color: Lightgrey; }
      table, td {border: 1px solid gray; border-collapse: collapse; }
      td { padding: 4px; }
      span.small { font: 12px Verdana, Arial, serif; color: grey; }
      div.progress { display: none; }
      img.centered { position: fixed; top: 50%; left: 50%; margin-top: -64px; margin-left: -64px; }

      #campaignList { margin-top: 24px; }
      #recentCampaignList table, #campaignList table { margin: 0px auto; width: 80%; }
      #recentCampaignList th, #campaignList th { text-align: left; }
      #recentCampaignList td, #campaignList td { text-align: left; }

      #message { margin: 16px 0; text-align: center; }
      .badMsg { color: red; }
      .goodMsg { color: green; }
    </style>
    <script type="text/javascript" src="../../js/jquery-2.0.3.js"></script>
    <script>
      // globals
      const CHUNK_SIZE = 4096;
      var theFile; // File object: CSV file to upload (properties are size, name, type)
      var theReader; // FileReader object
      var theChunkIndex; // current file chunk index number (starting from 0)
      var theChunkCount; // total number of chunks
      var theCampaign; // newly created campaign ID

      document.addEventListener('DOMContentLoaded', function() {
        if (document.getElementById('earliestDeliveryTime')) {
          buildDeliveryTimeOptions('earliestDeliveryTime', '0530', '1730', '0800');
          buildDeliveryTimeOptions('latestDeliveryTime', '0600', '2330', '2200');
        }

        document.getElementById('campaignStatusSelector').addEventListener('change', function() {
          showCampaignRowsWithStatus(document.getElementById('campaignStatusSelector').value);
        });
      });

      $(function() {
        $('#send_to_opt_out').change(function() {
          if ($(this).val() == 1)
          {
            if ( ! confirm("Confirm that the SMS message is related to service changes only and in no part is a marketing message."))
             $(this).val(0);
          }
        });
      })

      // load all existing campaigns
      function loadCampaignList(id)
      {
        // get existing campaign list
        try
        {
          // mandatory API parameters
          var formData = new FormData();
          formData.append('bath', 'rest');
          formData.append('partner', 'internal');
          formData.append('version', 2);
          formData.append('command', 'internal__ListPromoBroadcast')

          var request = new XMLHttpRequest();
          request.open('POST', '/ultra_api.php');
          request.onreadystatechange = function()
          {
            if (request.readyState == 4)
              processListResponse(request, id);
          }
          request.send(formData);
        }
        catch (error)
        {
          alert('Failed to retrieve campaign list: ' + error);
        }
      }

      // load details of a single campaign
      function loadCampaignDetails()
      {
        var campaign = window.location.search.split("campaign=")[1];
        document.getElementById('PROMO_BROADCAST_CAMPAIGN_ID').innerHTML = campaign;
        loadCampaignList(campaign);
      }

      function loadPromoBroadcastInfo(id)
      {
        try
        {
          // mandatory API parameters
          var formData = new FormData();
          formData.append('bath', 'rest');
          formData.append('partner', 'internal');
          formData.append('version', 2);
          formData.append('command', 'internal__GetPromoBroadcastInfo');
          formData.append('promo_broadcast_campaign_id', id);

          var request = new XMLHttpRequest();
          request.open('POST', '/ultra_api.php');
          request.onreadystatechange = function()
          {
            if (request.readyState == 4)
              processPromoBroadcastInfo(JSON.parse(request.response));
          }
          request.send(formData);
        }
        catch (error)
        {
          alert('Failed to retrieve campaign: ' + error);
        }
      }

      function processPromoBroadcastInfo(data)
      {
        var id = data.promo_broadcast_campaign_id;

        // fill in campaign details: returned campaign properties must match those on page by id
        for (var propertyName in data)
        {
          var element = document.getElementById(propertyName.toUpperCase());
          if (element)
          {

            if ((propertyName.toUpperCase() == 'MESSAGE_EN' 
              || propertyName.toUpperCase() == 'MESSAGE_ES'
              || propertyName.toUpperCase() == 'MESSAGE_ZH')
              && data[propertyName] != null)
            {
              element.innerHTML = convertUnicodeFromJavaEscape(data[propertyName]);
            }
            else if (propertyName.toUpperCase() == 'STARTED_DATE' 
              || propertyName.toUpperCase() == 'PROMO_EXPIRES_DATE' 
              || propertyName.toUpperCase() == 'CREATED_DATE')
                element.innerHTML = data[propertyName] + ' PT';
            else if (propertyName.toUpperCase() == 'NAME')
            {
              document.getElementById('edit_name_new_name').value = data[propertyName];
            }
            else
              element.innerHTML = data[propertyName];
          }
        }

        // create buttons appropriate for the campaign status
        var actions = '';
        var status = data.status;

        if (status == 'NEW')
          actions = '<input type="button" value="Initialize" onclick="updateCampaign(' + id + ', this.value)" />';
        else if (status == 'READY')
          actions = '<input type="button" value="Enable" onclick="updateCampaign(' + id + ', this.value)" />';
        else if (status == 'ONGOING')
          actions = '<input type="button" value="Pause" onclick="updateCampaign(' + id + ', this.value)" />';
        else if (status == 'PAUSED')
          actions = '<input type="button" value="Unpause" onclick="updateCampaign(' + id + ', this.value)" />';

        if (status != 'COMPLETED' && status != 'ABORTED')
          actions += ' <input type="button" value="Abort" onclick="updateCampaign(' + id + ', this.value)" />';
        document.getElementById('campaignActions').innerHTML = actions;
      }

      // callback for loadCampaignList
      function processListResponse(request, id)
      {
        try
        {
          var result = JSON.parse(request.responseText);     
          result.campaigns = sortCampaignList(result.campaigns);

          var newList = '';
          var recentList = '';
          for (var i = 0; i < result.count; i++)
          {
            // single campaign management
            if (id && result.campaigns[i].PROMO_BROADCAST_CAMPAIGN_ID == id)
            {
              loadPromoBroadcastInfo(id);
            }
            // all other campaigns are listed at the bottom
            else
            {   
              var row = '<tr><td>' + result.campaigns[i].PROMO_BROADCAST_CAMPAIGN_ID + '</td>'
                + '<td><a href="promo_broadcast_campaign.php?campaign=' + result.campaigns[i].PROMO_BROADCAST_CAMPAIGN_ID + '">' + result.campaigns[i].NAME + '</a></td>'
                + '<td>' + result.campaigns[i].STATUS + '</td>'
                + '<td>' + result.campaigns[i].CREATED_DATE + ' PT</td></tr>';

              if (result.campaigns[i].STARTED_DATE && newerThan(result.campaigns[i].STARTED_DATE, 3))
                recentList += row;
              else
                newList += row;

            }
          }

          document.getElementById('recentCampaignList').innerHTML = '<table>' 
            + '<th>ID</th><th>Name</th><th>Status</th><th>Created Date</th>'
            + recentList + '</table>';
          document.getElementById('campaignList').innerHTML = '<table>' + newList + '</table>';

          showCampaignRowsWithStatus('hiderows');
        }
        catch (error)
        {
          alert('Failed to retrieve campaign list: ' + error);
        }
      }

      function sortCampaignList(campaigns)
      {
        return campaigns.sort(function(a,b) {

          if (a.CREATED_DATE == null)
            return -1;

          var aTime = Date.parse(a.CREATED_DATE.substring(0, a.CREATED_DATE.length - 6) + ' ' + a.CREATED_DATE.substring(a.CREATED_DATE.length - 2, a.CREATED_DATE.length));
          var bTime = Date.parse(b.CREATED_DATE.substring(0, b.CREATED_DATE.length - 6) + ' ' + b.CREATED_DATE.substring(b.CREATED_DATE.length - 2, b.CREATED_DATE.length));

          if (aTime > bTime) return -1;
          if (aTime < bTime) return 1;
          if (aTime == bTime) return 0;
        });
      }

      // create new campaign in two steps: call internal__AddPromoBroadcast to create it, then call internal__UpdatePromoBroadcast to upload CSV file
      function newCampaign()
      {
        try
        {
          escapeMessageInputs();

          // validate campaign name
          var campaign = document.getElementById('campaignName').value;
          if (campaign.length < 4)
            throw 'broadcast name is missing or too short.';

          var english = document.getElementById('englishSMS').value;
          var spanish = document.getElementById('spanishSMS').value;
          var mandarin = document.getElementById('mandarinSMS').value;

          var sms_max_length = 160;

          if (english.length == 0)
          {
            if (spanish.length != 0)
              english = spanish;
            else if (mandarin.length != 0)
              english = mandarin;
            else
              throw 'All SMS fields cannot be empty.';
          }
          else
          {
            // validate English SMS text
            if (english.length < 12)
              throw 'English SMS is too short.';
            if ( ! validateText(english))
              throw 'English SMS contains invalid characters.';

            if (english.length > sms_max_length)
              alert('English message greater than ' + sms_max_length + ' and will be split.');
          }

          if (spanish.length == 0)
          {
            if (english.length != 0)
              spanish = english;
            else
              spanish = mandarin;
          }
          else
          {
            // validate optional Spanish SMS
            if (spanish.length < 12)
              throw 'Spanish SMS is too short.';
            if ( ! validateText(spanish))
              throw 'Spanish SMS contains invalid characters.';

            if (spanish.length > sms_max_length && english != spanish)
              alert('Spanish message greater than ' + sms_max_length + ' and will be split.');
          }

          if (mandarin.length == 0)
          {
            if (english.length != 0)
              mandarin = english;
            else
              mandarin = spanish;
          }
          else
          {
            // validate optional Mandarin SMS
            if (mandarin.length < 6)
              throw 'Mandarin SMS is too short.';
            if ( ! validateText(mandarin))
              throw 'Mandarin SMS contains invalid characters.';

            if (mandarin.length > sms_max_length && english != mandarin)
              alert('Mandarin message greater than ' + sms_max_length + ' and will be split.');
          }

          document.getElementById('englishSMS').value = english;
          document.getElementById('spanishSMS').value = spanish;
          document.getElementById('mandarinSMS').value = mandarin;

          var campaignForm = new FormData(document.getElementById("campaignForm"));

          // validate start date
          var start = document.getElementById('startDate').value;
          if (start.length < 6)
            throw 'missing or invalid start date.';
          campaignForm.append('started_date', validateAndConvertDate(start));

          // validate description
          var description = document.getElementById('campaignDescription').value;
          if (description.length < 8)
            throw 'description is missing or too short';
          if ( ! validateText(description))
            throw 'description contains invalid characters';

          // set promo_enabled for promotional campaigns
          if (document.getElementById('typePromo').checked)
          {
            campaignForm.append('promo_enabled', 1); 

            // validate keyword
            var keyword = document.getElementById('promoShortcode').value;
            if (keyword.length < 4)
              throw 'keyword is too short';
            if ( ! validateText(keyword))
              throw 'keyword contains invalid characters.';

            // validate and convert expiration date
            var expire = document.getElementById('expirationDate').value;
            if (expire.length < 6)
              throw 'expiration date is missing or invalid';
            campaignForm.append('promo_expires_date', validateAndConvertDate(expire));

            if (new Date(start) > new Date(expire))
              throw 'expiration date must be greater than start date';

            // verify promo_action and value
            var promo_value                 = document.getElementById('promo_value').value;
            var promo_value_mint_data_addon = document.getElementById('promo_value_mint_data_addon').value;

            if ( ( ! promo_value.length ) && ( ! promo_value_mint_data_addon.length ) )
              throw 'promo value is missing';
            if ( ( ! /^\d+$/.test(promo_value) ) && ( ! /^\d+$/.test(promo_value_mint_data_addon) ) )
              throw 'promo value must be positive a number.';
          }
          else
            campaignForm.append('promo_enabled', 0); 

          // check browser capabilities
          if ( ! (window.File && window.FileReader))
            throw 'your browser does not support file upload;\nplease use a recent version of Firefox, Chrome or Safari.';

          // validate and initialize CSV file
          var input = document.getElementById('dataFile');
          if ( ! input.files.length)
            throw 'you did not select a  file.';
          theFile = document.getElementById('dataFile').files[0];
          theChunkCount = Math.ceil(theFile.size / CHUNK_SIZE);
          theChunkIndex = 0;

          // passed all checks: confirm that user wants to create a new campaign
          var response = confirm('Ready to create a new SMS campaign. Proceed?');
          if (response !== true)
            return false;

          // remove campaign entry and show creation progress
          document.getElementById('setup').style.display = 'none';
          document.getElementById('progress').style.display = 'inline';
          document.getElementById('progressError').style.display = 'none'; // but hide the error button

          // create new campaign via an API call
          var request = new XMLHttpRequest();
          request.open('POST', '/ultra_api.php');
          request.onreadystatechange = function()
          {
            if (request.readyState == 4)
              newCampaignCallback(request);
          }
          request.send(campaignForm);
          document.body.style.cursor = 'wait';
        }
        catch (error)
        {
          document.body.style.cursor = 'auto';
          alert('ERROR: ' + error);
        }

        unescapeMessageInputs();
      }

      function escapeMessageInputs()
      {
        document.getElementById('englishSMS').value = convertUnicodeToJavaEscape(document.getElementById('englishSMS').value);
        document.getElementById('spanishSMS').value = convertUnicodeToJavaEscape(document.getElementById('spanishSMS').value);
        document.getElementById('mandarinSMS').value = convertUnicodeToJavaEscape(document.getElementById('mandarinSMS').value);
      }

      function unescapeMessageInputs()
      {
        document.getElementById('englishSMS').value = convertUnicodeFromJavaEscape(document.getElementById('englishSMS').value);
        document.getElementById('spanishSMS').value = convertUnicodeFromJavaEscape(document.getElementById('spanishSMS').value);
        document.getElementById('mandarinSMS').value = convertUnicodeFromJavaEscape(document.getElementById('mandarinSMS').value);
      }

      function newCampaignCallback(request)
      {
        try
        {
          // check HTTP response
          var message; // user friendly error message
          if (request.status != 200)
            throw (message = 'HTTP Server Error ' + request.status);

          // JSON will fail to parse on invalid response
          message = 'invalid server response';
          var response = JSON.parse(request.responseText);

          // check for API errors
          if (response.errors.length)
            throw (message = response.errors[0]);

          // get newly created campaign id
          theCampaign = response.campaign_id;
          document.getElementById('progress1').innerHTML = 'success, campaign ID ' + theCampaign;

          // start file upload
          theReader = new FileReader();
          loadFileChunk();

//          window.location.href = window.location.href + '?campaign=' + theCampaign;

        }
        catch (error)
        {
          document.body.style.cursor = 'auto';
          document.getElementById('spinner').style.display = 'none';
          document.getElementById('progress1').innerHTML = message;
          document.getElementById('progress1').style.color = 'red';
          document.getElementById('progressError').style.display = 'inline';
        }
      }

      // load a chunk of CSV file into RAM
      function loadFileChunk()
      {
        var size;
        theReader.onloadend = uploadFileChunk;
        theReader.onerror = function(event)
        {
          document.body.style.cursor = 'auto';
          document.getElementById('spinner').style.display = 'none';
          document.getElementById('progress1').innerHTML = event.getMessage();
          document.getElementById('progress1').style.color = 'red';
          document.getElementById('progressError').style.display = 'inline';
        }
        var blob = theFile.slice(CHUNK_SIZE * theChunkIndex, CHUNK_SIZE * theChunkIndex + CHUNK_SIZE);
        theReader.readAsText(blob);
      }

      // POST a chunk of CSV file to API
      function uploadFileChunk(event)
      {
        if (event.target.readyState == FileReader.DONE)
        {
          // prepare AJAX request and POST it
          var url = '/ultra_api.php';

          var formData = new FormData();
          formData.append('bath', 'rest');
          formData.append('partner', 'internal');
          formData.append('version', 2);
          formData.append('command', 'internal__UpdatePromoBroadcast');
          formData.append('campaign_id', theCampaign);
          formData.append('chunk_size', event.target.result.length);
          formData.append('chunk_index', theChunkIndex);
          formData.append('data_chunk', event.target.result);

          ajax = new XMLHttpRequest();
          ajax.open('POST', url, true);
          ajax.onreadystatechange = function()
          {
            if (ajax.readyState == 4)
              checkFileUpload(ajax);
          }
          ajax.send(formData);
        }
      }

      // process API response on chunk POST and start data next chunk
      function checkFileUpload(ajax)
      {
        try
        {
          // check for server errors
          var message;
          if (ajax.status != 200)
            throw (message = 'HTTP Server Error ' + ajax.status);
          
          // JSON will fail to parse on invalid response
          message = 'invalid server response';
          var response = JSON.parse(ajax.responseText);

          // check for API errors
          if (response.errors.length)
            throw (message = response.errors[0]);

          // update progress and process next chunk
          if (++theChunkIndex < theChunkCount)
          {
            document.getElementById('progress2').innerHTML = Math.round(theChunkIndex / theChunkCount * 100) + '%';
            loadFileChunk();
          }
          else // finished all chunks: redirect to campaign management
            window.location.href = window.location.href + '?campaign=' + theCampaign;
        }
        catch (error)
        {
          document.body.style.cursor = 'auto';
          document.getElementById('spinner').style.display = 'none';
          document.getElementById('progress2').innerHTML = message;
          document.getElementById('progress2').style.color = 'red';
          document.getElementById('progressError').style.display = 'inline';
        }
      }

      function validateText(sms)
      {
        var allowed = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-+$ .!/%=~@#^*()_[]\\{};\':?'; // allowed SMS characters
        for (var i = 0; i < sms.length; i++)
          if (allowed.indexOf(sms[i]) === -1)
            return false;
        return true;
      }

      function updateCampaign(id, action)
      {
        // translate action into status
        var status = null;
        if (action == 'Initialize')
          status = 'READY';
        else if (action == 'Enable' || action == 'Unpause')
          status = 'ONGOING';
        else if (action == 'Pause')
          status = 'PAUSED';
        else if (action == 'Abort')
          status = 'ABORTED';

        // replace campaign actions with spinner
        document.getElementById('campaignActions').innerHTML = "<img src='miso.gif'/>";

        // update campaign
        try
        {
          // mandatory API parameters
          
          var formData = new FormData();
          formData.append('bath', 'rest');
          formData.append('partner', 'internal');
          formData.append('version', 2);
          formData.append('command', 'internal__UpdatePromoBroadcast');
          formData.append('campaign_id', id);
          formData.append('status', status);

          var request = new XMLHttpRequest();
          request.open('POST', '/ultra_api.php');
          request.onreadystatechange = function()
          {
            if (request.readyState == 4)
              processUpdateResponse(request);
          }
          request.send(formData);
        }
        catch (error)
        {
          alert('Failed to update campaign: ' + error);
        }
      }

      function processUpdateResponse(request)
      {
        try
        {
          var result = JSON.parse(request.responseText);
          if (result.errors.length)
            throw result.errors[0];
        }
        catch (error)
        {
          alert('Failed to update campaign: ' + error);
        }
        location.reload();
      }

      function labelPromoValue(event)
      {
        var promoValue   = document.getElementById('promoValue');
        var promoAction  = document.getElementById('promo_action').value;
        var brandValue   = document.getElementById('brand').value;

        document.getElementById('promo_value_mint_data_addon').style.display = 'none';
        document.getElementById('promo_value_mint_data_addon').value = '';
        document.getElementById('promo_value').style.display = 'inline';

        if (promoAction == 'ADD4GDATA' || promoAction == 'AUTORECHARGEDATA' || promoAction == 'CCREACTIVATEDATA')
        {
          promoValue.innerHTML = ' MB';

          if (brandValue == 'MINT')
          {
            // show drop down list for Mint Data Add On
            document.getElementById('promo_value_mint_data_addon').style.display = 'inline';
            document.getElementById('promo_value').style.display = 'none';
            document.getElementById('promo_value').value = '';
          }
        }
        else
          promoValue.innerHTML = ' cents';
      }

      // validate and convert date from US format MM/DD/YYYY local time to Unix Epoch (as accepted by the API)
      function validateAndConvertDate(value)
      {
        if ( ! value.match(/^\d{1,2}\/\d{1,2}\/\d{2,4}$/))
          throw 'invalid date format.';
        var parsedDate = new Date(value);
        return parsedDate.getTime() / 1000;
      }

      function showCampaignRowsWithStatus(status)
      {
        var rows = document.getElementById('campaignList').getElementsByTagName('table')[0].rows;
        for (var i = 0; i < rows.length; i++)
        {
          if (rows[i].cells[2].innerHTML == status)
            rows[i].style.display = 'table-row';
          else
            rows[i].style.display = 'none';
        }
      }

      function newerThan(date, daysAgo)
      {
        return (Date.parse(date.substring(0, date.length - 6)) > (new Date).getTime() - 24*60*60*daysAgo*1000)
      }

      function buildDeliveryTimeOptions(id, start, end, defaultTime)
      {
        var cur = new Date();

        var hour = start.substring(0,2);
        var mins = start.substring(2,4);

        cur.setHours(hour);
        cur.setMinutes(mins);

        var opts = [];
        while (parseInt(cur.getHours() + '' + cur.getMinutes()) <= parseInt(end))
        {
          hour = cur.getHours() + '';
          mins = cur.getMinutes() + '';

          var nextHour = parseInt(hour);
          var nextMins = parseInt(mins);

          if (hour.length == 1)
            hour = '0' + hour;

          if (mins.length == 1)
            mins = '0' + mins;

          opts.push(hour + '' + mins);

          nextMins += 15;

          cur.setHours(nextHour);
          cur.setMinutes(nextMins);
        }

        var html = '<select>';
        for (var i = 0; i < opts.length; i++)
        {
          html += '<option';
          if (opts[i] == defaultTime)
            html += ' selected';
          html += ' value="' + opts[i] + '">' + opts[i] + '</option>';
        }
        html += '</select>';

        document.getElementById(id).innerHTML = html;
      }
      
      function showPromo()
      {
        document.getElementById('keywordRow').style.display = 'table-row';
        document.getElementById('expirationRow').style.display = 'table-row';
        document.getElementById('actionRow').style.display = 'table-row';
      }
      
      function hidePromo()
      {
        document.getElementById('keywordRow').style.display = 'none';
        document.getElementById('expirationRow').style.display = 'none';
        document.getElementById('actionRow').style.display = 'none';
      }

      function renameCampaign()
      {
        var campaign_id = document.getElementById('PROMO_BROADCAST_CAMPAIGN_ID').innerHTML;
        var new_name = document.getElementById('edit_name_new_name').value;

        var input = '&campaign_id=' + campaign_id + '&new_name=' + new_name;

        $.ajax({
          url: '/ultra_api.php',
          type: 'POST',
          data: 'bath=rest&partner=internal&version=2&command=internal__RenamePromoBroadcast' + input,
          success: function(res) {
            var data = JSON.parse(res);

            var msg;
            if (data.success == true)
            {
              msg = 'Name successfully updated!';
              document.getElementById('message').className = 'goodMsg';
            }
            else
            {
              msg = data.errors[0];
              document.getElementById('message').className = 'badMsg';
            }
            
            document.getElementById('message').innerHTML = msg;
          },
          error: function(res) {
            document.getElementById('message').innerHTML = 'Network error.';
          }
        });
      }

    </script>
  </head>
<?php

// new campaign creation page
if (empty($_GET['campaign']))
{
  ?>
  <body>
    <!-- new campaign entry -->
    <div align='center' id='setup'>
    <h3>Create a new SMS broadcast campaign:</h3>
    <table>
      <form id='campaignForm' method="POST">
      <input type='hidden' name='bath' value='rest' />
      <input type='hidden' name='partner' value='internal' />
      <input type='hidden' name='version' value='2' />
      <input type='hidden' name='command' value='internal__AddPromoBroadcast' />
      <tr>
        <td>Send only to subscribers of BRAND:</td>
        <td>
          <select id="brand" name="brand" onchange="labelPromoValue(event)">
            <option value="ULTRA">ULTRA</option>
            <option value="UNIVISION">UNIVISION</option>
            <option value="MINT">MINT</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Broadcast name:<br><span class='small'>campaign title</span></td>
        <td><input type='text' name='name' id='campaignName' maxlength=64 size=64></td>
      </tr>
      <tr>
        <td>English message:<br><span class='small'>limit 320 chars,<br>no commas <br>or ampersands allowed</span></td>
        <td><textarea name='message_en' rows="4" cols="100" maxlength="320" id="englishSMS" title="enter English SMS message"></textarea></td>
      </tr>
      <tr>
        <td>Spanish message:<br><span class='small'>optional,<br>limit 320 chars,<br>no commas <br>or ampersands allowed</span></td>
        <td><textarea name='message_es' rows="4" cols="100" maxlength="320" id="spanishSMS" title="enter Spanish SMS message"></textarea></td>
      </tr>
      <tr>
        <td>Mandarin message:<br><span class='small'>optional,<br>limit 320 chars,<br>no commas <br>or ampersands allowed</span></td>
        <td><textarea name='message_zh' rows="4" cols="100" maxlength="320" id="mandarinSMS" title="enter Mandarin SMS message"></textarea></td>
      </tr>
      <tr>
        <td>Start date:<br><span class='small'>format: MM/DD/YYYY</span></td>
        <td><input type='text' id='startDate' maxlength=10 size=10 class='datepicker'></td>
      </tr>
      <tr>
        <td>Campaign type:</td>
        <td>
          <input type="radio" name="promotional" value="broadcast" checked onclick="hidePromo()"> Broadcast &nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" name="promotional" value="promotion" id='typePromo' onclick="showPromo()"> Promotion
        </td>
      </tr>

      <tr>
        <td>Earliest delivery time:</td>
        <td><select id="earliestDeliveryTime" name="earliest_delivery_time"></select> local customer time</td>
      </tr>

      <tr>
        <td>Latest delivery time:</td>
        <td><select id="latestDeliveryTime" name="latest_delivery_time"></select> local customer time</td>
      </tr>

      <tr>
        <td>Messages per hour:<br /><span class='small'>Maximum SMS<br />messages per hour</span></td>
        <td><input type="number" id="messagesPerHour" name="messages_per_hour" size="10" value="0" min="0"></td>
      </tr>

      <tr id='keywordRow' style='display:none'>
        <td>Keyword:<br><span class='small'>aka 'shortcode'</span></td>
        <td><input type='text' name='promo_shortcode' id='promoShortcode' maxlength=16 size=16></td>
      </tr>
      <tr id='expirationRow' style='display:none'>
        <td>Expiration date:<br><span class='small'>greater than today</span></td>
        <td><input type='text' id='expirationDate' maxlength=10 size=10 class='datepicker'> @ 12:00 AM PT</td>
      </tr>
      <tr id='actionRow' style='display:none'>
        <td>Promotion:</td>
        <td>
          action:
          <select id='promo_action' name='promo_action' onchange="labelPromoValue(event)">
            <option value="ADDBALANCE" selected>Add Wallet Balance</option>
            <option value="ADDSTOREDVALUE">Add Stored Value</option>
            <option value="ADDPACKAGEBALANCE1">Add Call Anywhere Credit</option>
            <option value="ADD4GDATA">Add Data</option>
            <option value="ADDROAM">Add Roam</option>
            <option value="AUTORECHARGEWALLET" title='Enable monthly credit card auto recharge and optionally add funds to Wallet. Must have credit card on file.'>Enable Auto Renewal + Credit</option>
            <option value="AUTORECHARGEDATA" title='Enable monthly credit card auto recharge and optionally add cellular data. Must have credit card on file.'>Enable Auto Renewal + Data</option>
            <option value="CCREACTIVATEWALLET" title='Reactivate suspended subscriber and optionally add a value to Wallet. Must be suspended. Must have credit card on file.'>CC Reactivate + Credit</option>
            <option value="CCREACTIVATEDATA" title='Reactivate suspended subscriber and optionally add cellular data. Must be suspended. Must have credit card on file.'>CC Reactivate + Data</option>
          </select>&nbsp;&nbsp;&nbsp;&nbsp;
          value:
          <input type='text' name='promo_value' id='promo_value' maxlength=16 size=16><span id='promoValue'> cents</span>
          <select id="promo_value_mint_data_addon" name="promo_value_mint_data_addon" style='display:none'>
            <option value="" selected>-- choose --</option>
            <option value="1024">1024</option>
            <option value="3072">3072</option>
          </select>
        </td>
      </tr>
      <tr id='descriptionRow'>
        <td>Description:</td>
        <td><input type='text' name='promo_description' id='campaignDescription' maxlength=250 size=100></td>
      </tr>
      <tr>
        <td>Send to active?</td>
        <td>
          <select id="send_to_active" name="send_to_active">
            <option value="1" selected>YES</option>
            <option value="0">NO</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Send to suspended?</td>
        <td>
          <select id="send_to_suspended" name="send_to_suspended">
            <option value="1" selected>YES</option>
            <option value="0">NO</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Send to multi-month?</td>
        <td>
          <select id="send_to_multi_month" name="send_to_multi_month">
            <option value="0" selected>NO</option>
            <option value="1">YES</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Send to customers opted out of promotions?</td>
        <td>
          <select id="send_to_opt_out" name="send_to_opt_out">
            <option value="0" selected>NO</option>
            <option value="1">YES</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Send to customers configured for auto renewal?</td>
        <td>
          <select id="send_to_auto_renewal" name="send_to_auto_renewal">
            <option value="0">WITH AUTO RENEWAL OFF</option>
            <option value="1">WITH AUTO RENEWAL ON</option>
            <option value="2" selected>ON or OFF</option>
          </select>
        </td>
      </tr>

     </form>
     <!-- file upload -->
     <tr>
        <td>Subscriber CVS file:<br><span class='small'>customer_id, one per line</span></td>
        <td>
          <input type="hidden" name="MAX_FILE_SIZE" value="16000000" />
          <input id='dataFile' type="file" accept='.csv,.txt' /></td>
      </tr>
    </table>
    <p><input type="button" value="Add New Campaign" onclick='newCampaign()' /></p>
    <hr>
    
    
    </div>

    <!-- campaign creation progress -->
    <div align='center' class='progress' id='progress'>
      <h3>Progress:</h3>
      <p>Adding campaign: <span id=progress1>pending</span></p>
      <p>Uploading file: <span id=progress2>pending</span></p>
      <p><input type="button" value="Ok" id='progressError' onclick='window.location.reload();' /></p>
      <img id=spinner class=centered src='miso.gif'/>
    </div>

    <script>loadCampaignList();</script>

  <?php
}
else // campaign management page
{
  ?>
  <body>
    <h3 align='center'>Campaign Management:</h3>
    
    <div id="message"></div>

    <table align="center">
      <tr>
        <td>Campaign ID:</td>
        <td id='PROMO_BROADCAST_CAMPAIGN_ID'></td>
      </tr>
      <tr>
        <td>Name:</td>
        <td id='NAME'>
            <input type="text" id="edit_name_new_name" name="new_name" value="" size="36" />
            <button id="edit_name_update_button" onclick="renameCampaign();">Update Name</button>
        </td>
      </tr>
      <tr>
        <td>Description:</td>
        <td id='PROMO_DESCRIPTION'></td>
      </tr>
      <tr>
        <td>Status:</td>
        <td id='STATUS'></td>
      </tr>
      <tr>
        <td>English SMS:</td>
        <td id='MESSAGE_EN'></td>
      </tr>
      <tr>
        <td>Spanish SMS:</td>
        <td id='MESSAGE_ES'></td>
      </tr>
      <tr>
        <td>Mandarin SMS:</td>
        <td id='MESSAGE_ZH'></td>
      </tr>
      <tr>
        <td>Campaign created:</td>
        <td id='CREATED_DATE'></td>
      </tr>
      <tr>
        <td>Started date:</td>
        <td id='STARTED_DATE'></td>
      </tr>
      <tr>
        <td>Earliest Delivery Time:</td>
        <td id="EARLIEST_DELIVERY_TIME"></td>
      </tr>
      <tr>
        <td>Latest Delivery Time:</td>
        <td id="LATEST_DELIVERY_TIME"></td>
      </tr>
      <tr>
        <td>Messages per hour:</td>
        <td id="MESSAGES_PER_HOUR"></td>
      </tr>
      <tr>
        <td>VOLUME COUNT:</td>
        <td id="VOLUME_COUNT"></td>
      </tr>
      <tr>
        <td>SEND TO ACTIVE:</td>
        <td id="SEND_TO_ACTIVE"></td>
      </tr>
      <tr>
        <td>SEND TO SUSPENDED:</td>
        <td id="SEND_TO_SUSPENDED"></td>
      </tr>
      <tr>
        <td>SEND TO MULTI-MONTH:</td>
        <td id="SEND_TO_MULTI_MONTH"></td>
      </tr>
      <tr>
        <td>SEND TO OPTED OUT:</td>
        <td id="SEND_TO_OPT_OUT"></td>
      </tr>
      <tr>
        <td>SEND TO AUTO-RENEWAL:</td>
        <td id="SEND_TO_AUTO_RENEWAL"></td>
      </tr>
      <tr>
        <td>BRAND_ID:</td>
        <td id="BRAND_ID"></td>
      </tr>
      <tr>
        <td>Promo expires:</td>
        <td id='PROMO_EXPIRES_DATE'></td>
      </tr>
      <tr>
        <td>Promo shortcode:</td>
        <td id='PROMO_SHORTCODE'></td>
      </tr>
      <tr>
        <td>Promo enabled:</td>
        <td id='PROMO_ENABLED'></td>
      </tr>
      <tr>
        <td>Promo action:</td>
        <td id='PROMO_ACTION'></td>
      </tr>
      <tr>
        <td>Promo value:</td>
        <td id='PROMO_VALUE'></td>
      </tr>
      <tr>
        <td>Sent:</td>
        <td id="SENT"></td>
      </tr>
      <tr>
        <td>Sent Error:</td>
        <td id="SENT_ERROR"></td>
      </tr>
      <tr>
        <td>Redemption Success:</td>
        <td id="REDEMPTION_SUCCESS"></td>
      </tr>
      <tr>
        <td>Redemption Error:</td>
        <td id="REDEMPTION_ERROR"></td>
      </tr>
    </table>
    <p align='center' id='campaignActions'></p>

    <hr>
    <a href='promo_broadcast_campaign.php'><h3 align='center'>Create a new campaign</h3></a>
    <hr>

  <script>loadCampaignDetails();</script>

  <?php
}

?>

    <div style="text-align: center; margin-bottom: 32px;">
      <h3>Recent campaigns:</h3>

      <div id='recentCampaignList'></div>

      <h3>View existing campaigns:</h3>

      Select status type:&nbsp;
      <select id="campaignStatusSelector">
        <option value="" selected></option>
        <option value="NEW">NEW</option>
        <option value="READY">READY</option>
        <option value="ONGOING">ONGOING</option>
        <option value="PAUSED">PAUSED</option>
        <option value="ERROR">ERROR</option>
        <option value="ABORTED">ABORTED</option>
        <option value="COMPLETED">COMPLETED</option>
      </select>

      <div id='campaignList'></div>
    </div>

  </body>
  </html>
