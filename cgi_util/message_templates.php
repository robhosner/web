<?php ?>
<html>
<head>

<title>Message templates</title>
<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
$(function() {

});

var readyText   = 'EXECUTE';
var workingText = 'WORKING';

var readyColor   = 'red';
var workingColor = '#888';

var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

function internal__GetAllMessagingTemplates()
{
  var tableHeaders = "<tr><td><b>Template name</b></td><td><b>English</b></td><td><b>Spanish</b></td><td><b>Chinese</b></td></tr>";
  $('#templates').html(tableHeaders);

  var brand = $('#brand').val();
  $.ajax({
    method: 'GET',
    url: baseURL + '&command=internal__GetAllMessagingTemplates&brand=' + brand,
    success: function(data, status, settings) {
      var response = JSON.parse(data);

      if ((response.errors && response.errors.length > 0) || (response.success !== true))
      {
        alert(response.errors[0]);
      }
      else
      {
        var html = '';
        var templates    = response.templates;
        var lastTemplate = null;
        for (var key in templates)
        {
          var templateName = key.split('__')[0];
          if (templateName != lastTemplate)
          {
            if (lastTemplate !== null)
              html += '</tr>';
            html += '<tr><td>' + templateName + '</td>';

            lastTemplate = templateName;
          }
          html += '<td>' + templates[key] + '</td>';
        }

        $('#templates').append(html);
      }
    }
  });
}

$(function() {
  internal__GetAllMessagingTemplates();
});
</script>
<style>
body { font-family: sans-serif; }
td { padding: 8px; }
table { margin-top: 16px; }
table tr:nth-child(even) { background-color: #EEE; }
</style>

</head>
<body>

<div>
  <h2>Messaging Templates</h2>

  <p>
    <label>Brand: </label>
    <select id="brand">
      <option value="ULTRA">ULTRA</option>
      <option value="UNIVISION">UNIVISION</option>
      <option value="MINT">MINT</option>
    </select>
    <button onclick="internal__GetAllMessagingTemplates()">Reload</button>
  </p>

  <small>Template variables start with __params__</small>
  <table id="templates" border=1></table>
</div>

</body>
</html>
