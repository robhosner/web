<?php

// Provides all the useful details on a customer based upon a customer_id lookup.
function customercare__CustomerInfoByID($partner_tag, $customer_id, $mode)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $customer_info_array = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__CustomerInfoByID",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__CustomerInfoByID", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__CustomerInfoByID",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $results = get_customer_state( $customer );

    if ( count($results['errors']) == 0 )
    {
      $customer_address = $customer->ADDRESS1;

      if ( $customer->ADDRESS2 ) { $customer_address .= $customer->ADDRESS2; }

      $actcode = ( $customer->ACTIVATION_ICCID )
                 ?
                 get_act_code_from_iccid( $customer->ACTIVATION_ICCID )
                 :
                 ''
                 ;

      $plan_name = get_plan_from_cos_id( $customer->cos_id );
      $multi_month_options = multi_month_info($customer_id);

      $customer_info_array = array(
        'recharge_status'          => $results['recharge_status'],
        'customer_id'              => $customer->CUSTOMER_ID,
        'customer_plan'            => \Ultra\UltraConfig\getUltraPlanConfigurationItem($plan_name, 'name'),
        'customer_plan_start'      => $customer->plan_started_epoch,
        'customer_plan_expires'    => $customer->plan_expires_epoch,
        'paid_through'             => $multi_month_options ? strtotime("+{$multi_month_options['months_left']} month", $customer->plan_expires_epoch) : NULL,
        'duration'                 => $multi_month_options ? $multi_month_options['months_total'] : NULL,
        'preferred_language'       => $customer->preferred_language,
        'customer_stored_balance'  => $customer->stored_value,
        'customer_balance'         => $customer->BALANCE,
        'customer_minutes'         => $customer->minutes,
        'customer_status'          => $customer->plan_state,
        'customer_loginname'       => $customer->LOGIN_NAME,
        'current_mobile_number'    => $customer->current_mobile_number,
        'current_iccid'            => $customer->CURRENT_ICCID_FULL,
        'activation_iccid'         => $customer->ACTIVATION_ICCID,
        'brand'                    => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
        'first_name'               => $customer->FIRST_NAME,
        'last_name'                => $customer->LAST_NAME,
        'email'                    => $customer->E_MAIL,
        'address'                  => $customer_address,
        'city'                     => $customer->CITY,
        'state'                    => $customer->STATE_REGION,
        'activation_start_date'    => customer_activation_start_date( $customer->CUSTOMER_ID ),
        'actcode'                  => $actcode,
        'customer_has_credit_card' => customer_has_credit_card($customer),
        'voice_minutes'            => 0,
        'MVNE'                     => $customer->MVNE,
        'bolt_ons'                 => get_bolt_ons_info_from_customer_options($customer_id)
      );

      // DATAQ-112
      if ( $mode == 'OCS_BALANCE' && $plan_name == 'L34' )
        $customer_info_array['voice_minutes'] = mvneGetVoiceMinutes($customer->customer_id);

      $success = TRUE;
    }
    else
      $errors = $results['errors'];

  }
  else
    $errors = array("ERR_API_INVALID_ARGUMENTS: customer not found");

  $return_values = array_merge(
    $customer_info_array,
    array(
      "success" => $success,
      "errors"  => $errors
    )
  );

  return flexi_encode(fill_return($p,
                                  "customercare__CustomerInfoByID",
                                  func_get_args(),
                                  $return_values));
}

?>
