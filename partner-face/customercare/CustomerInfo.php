<?php

// Given a phone number, provides customer useful details.
function customercare__CustomerInfo($partner_tag, $phone_number, $mode, $cache_override)
{
  // $phone_number can be an ICCID or a MSISDN or a Actcode

  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $success = FALSE;

  $customer_info_array = array();
  $warnings            = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__CustomerInfo",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__CustomerInfo", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                    "customercare__CustomerInfo",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = NULL;

  if ( strlen($phone_number) == 11 )
    // $phone_number is an Actcode
    $customer = get_customer_from_actcode($phone_number);
  elseif ( strlen($phone_number) < 11 )
  {
    // $phone_number is a MSISDN
    if (! $customer = get_customer_from_msisdn($phone_number))
      $customer = get_customer_from_cancelled_msisdn($phone_number);
  }
  else
  {
    // $phone_number is a ICCID
    if ( strlen($phone_number) == 19 )
      $phone_number = substr($phone_number,0,-1);

    $customer = get_customer_from_iccid($phone_number);
  }

  if ( $customer )
  {
    $results = get_customer_state( $customer );
    
    if ( count($results['errors']) == 0 )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      $customer_address = $customer->ADDRESS1;

      if ( $customer->ADDRESS2 )
        $customer_address .= $customer->ADDRESS2;

      $originalsimproduct = get_product_type_from_iccid( $customer->CURRENT_ICCID_FULL );

      $originalsimproduct =
        ( strlen( $customer->CURRENT_ICCID_FULL ) == 19 )
        ?
        get_product_type_from_iccid( $customer->CURRENT_ICCID_FULL )
        :
        ''
        ;

      $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer->CUSTOMER_ID );
      $plan_name = get_plan_from_cos_id( $customer->cos_id );
      $multi_month_options = multi_month_info($customer->CUSTOMER_ID, $ultra_customer_options);

      // check for MVNE mismatch between customer, MSISDN and ICCID records
      $msisdn_mvne = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer->CUSTOMER_ID, 'MVNE', NULL, 'HTT_ULTRA_MSISDN');
      $mvne_sync = empty($msisdn_mvne) || $customer->MVNE == $msisdn_mvne ? NULL : 'msisdn';
      $iccid_mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $customer->CURRENT_ICCID_FULL, 'MVNE');
      $mvne_sync .= empty($iccid_mvne) || $customer->MVNE == $iccid_mvne ? ($mvne_sync ? NULL : 'none') : ($mvne_sync ? '+iccid' : 'iccid');

      // check for Brand mismatch between customer, MSISDN and ICCID records
      $brand_id = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customer->CUSTOMER_ID, 'BRAND_ID');
      $brand_sync = empty($brand_id) || $customer->BRAND_ID == $brand_id ? NULL : 'brand_id';
      $brand_iccid = \Ultra\Lib\DB\Getter\getScalar('ICCID', $customer->CURRENT_ICCID_FULL, 'BRAND_ID');
      $brand_sync .= empty($brand_iccid) || $customer->BRAND_ID == $brand_iccid ? ($brand_sync ? NULL : 'none') : ($brand_sync ? '+brand_id' : 'brand_id');

      $cc_info = get_cc_info_from_customer_id($customer->CUSTOMER_ID);

      if (\Ultra\Lib\Util\validateMintBrandId($customer->BRAND_ID))
      {
        $multi_month_overlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);
        $paid_through = strtotime($multi_month_overlay->CYCLE_EXPIRES);
        $duration     = $multi_month_overlay->TOTAL_MONTHS;
      }
      else
      {
        $multi_month_options = multi_month_info($customer->CUSTOMER_ID, $ultra_customer_options);
        $paid_through = $multi_month_options ? strtotime("+{$multi_month_options['months_left']} month", $customer->plan_expires_epoch) : NULL;
        $duration     = $multi_month_options ? $multi_month_options['months_total'] : NULL;
      }

      list($enabled_col_name) = array_map('trim', explode('=', account_enabled_assignment(TRUE)));

      $customer_info_array = [
        'recharge_status'          => $results['recharge_status'],
        'customer_id'              => $customer->CUSTOMER_ID,
        'customer_plan'            => \Ultra\UltraConfig\getUltraPlanConfigurationItem($plan_name, 'name'),
        'customer_plan_start'      => $customer->plan_started_epoch,
        'customer_plan_expires'    => $customer->plan_expires_epoch,
        'paid_through'             => $paid_through,
        'duration'                 => $duration,
        'preferred_language'       => $customer->preferred_language,
        'customer_stored_balance'  => sprintf("%.2f",$customer->stored_value),
        'customer_balance'         => sprintf("%.2f",$customer->BALANCE),
        'customer_minutes'         => sprintf("%.2f",( $customer->PACKAGED_BALANCE1 / 60 )),
        'customer_status'          => $customer->plan_state,
        'customer_loginname'       => $customer->LOGIN_NAME,
        'current_mobile_number'    => $customer->current_mobile_number,
        'current_iccid'            => $customer->CURRENT_ICCID_FULL,
        'activation_iccid'         => $customer->ACTIVATION_ICCID,
        'brand'                    => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
        'brand_sync_failures'      => $brand_sync,
        'first_name'               => $customer->FIRST_NAME,
        'last_name'                => $customer->LAST_NAME,
        'email'                    => $customer->E_MAIL,
        'address'                  => $customer_address,
        'city'                     => $customer->CITY,
        'state'                    => $customer->STATE_REGION,
        'activation_start_date'    => customer_activation_start_date( $customer->CUSTOMER_ID ),
        'customer_has_credit_card' => customer_has_credit_card($customer),
        'voice_minutes'            => 0,
        'originalsimproduct'       => $originalsimproduct,
        'MVNE'                     => $customer->MVNE,
        'mvne_sync_failures'       => $mvne_sync,
        'puk1'                     => '',
        'puk2'                     => '',
        'cc_bin'                   => empty($cc_info->BIN)       ? '' : $cc_info->BIN,
        'cc_last4'                 => empty($cc_info->LAST_FOUR) ? '' : $cc_info->LAST_FOUR,
        'temp_password'            => func_read_temp_password($customer->CUSTOMER_ID),
        '4g_lte_remaining'         => ( ( $mode == 'DATA_USAGE' ) ? $redis->get( 'ultra/data/4g_lte/remaining/'.$customer->CUSTOMER_ID ) : '' ),
        '4g_lte_usage'             => ( ( $mode == 'DATA_USAGE' ) ? $redis->get( 'ultra/data/4g_lte/usage/'    .$customer->CUSTOMER_ID ) : '' ),
        '4g_lte_last_applied'      => 0,
        '4g_lte_bolton_percent_used' => (($mode == 'DATA_USAGE') ? $redis->get('ultra/data/4g_lte/mint/addon/percent/used/' . $customer->CUSTOMER_ID) : ''),
        'ultra_customer_options'   => $ultra_customer_options,
        'zero_minutes'             => int_positive_or_zero(1000 - \Ultra\UltraConfig\zeroUsedMinutes($plan_name) - $customer->PERIOD_MINUTES_TO_DATE_BILLED),
        'bolt_ons'                 => get_bolt_ons_info_from_customer_options($customer->CUSTOMER_ID),
        'intl_dialing'             => ( $customer->$enabled_col_name == 8 ) ? 0 : 1,
        'wholesale_plan'           => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
        'bogo_free_month'          => count(array_intersect(\Ultra\UltraConfig\getValidBOGOTypes(), $ultra_customer_options)) ? 1 : 0,
        'account_number'           => $customer->CUSTOMER
      ];

      // retrieve PUK1 and PUK2 from HTT_INVENTORY_SIM
      $htt_inventory_sim_select_query = htt_inventory_sim_select_query(
        array(
          "customer_id" => $customer->CUSTOMER_ID
        )
      );

      $htt_inventory_sim_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

      if ( $htt_inventory_sim_result && is_array($htt_inventory_sim_result) && count($htt_inventory_sim_result) )
      {
        $customer_info_array['puk1'] = $htt_inventory_sim_result[0]->PUK1;
        $customer_info_array['puk2'] = $htt_inventory_sim_result[0]->PUK2;
        $customer_info_array['master_agent'] = $htt_inventory_sim_result[0]->INVENTORY_MASTERAGENT;
      }

      $success = TRUE;

      if ( $mode == 'DATA_USAGE' )
      {
        // get '4g_lte_last_applied' from HTT_DATA_EVENT_LOG

        $latest_data_event_log = get_latest_data_event_log( $customer->CUSTOMER_ID );

        if ( $latest_data_event_log )
          $customer_info_array['4g_lte_last_applied'] = $latest_data_event_log->event_date_epoch;
      }

      // invoke mwCheckBalance to check 4G LTE data usage for this customer
      if ( $customer->current_mobile_number
        && ( $mode == 'DATA_USAGE' )
        && ( ( !$customer_info_array['4g_lte_usage'] && !$customer_info_array['4g_lte_remaining'] ) || $cache_override ) 
      )
      {
        list($remaining, $usage, $mintAddOnRemaining, $mintAddOnUsage, $breakDown, $mvneError ) = mvneGet4gLTE($request_id, $customer->current_mobile_number, $customer->CUSTOMER_ID, $redis);

        $customer_info_array['4g_lte_remaining'] = $remaining;
        $customer_info_array['4g_lte_usage']     = $usage;

        if ( ! empty( $mintAddOnUsage )
          && ! empty( $mintAddOnUsage + $mintAddOnRemaining )
        )
        {
          $customer_info_array['4g_lte_bolton_percent_used'] = ceil( ( $mintAddOnUsage * 100 ) / ( $mintAddOnUsage + $mintAddOnRemaining ) );
        }
      }

      // If the result is under 1GB, then return as MB rounded up to the nearest MB i.e. 120MB
      // If the result is over 1GB, then show it as the GB number, the decimal, and then the MB amount out to three digits rounded up to the nearest MB i.e. 1.329GB

      if ( $customer_info_array['4g_lte_remaining'] )
        $customer_info_array['4g_lte_remaining'] = beautify_4g_lte_string( $customer_info_array['4g_lte_remaining'], FALSE );

      if ( $customer_info_array['4g_lte_usage'] )
        $customer_info_array['4g_lte_usage']     = beautify_4g_lte_string( $customer_info_array['4g_lte_usage'], TRUE );

      // DATAQ-112
      if ( $mode == 'OCS_BALANCE' && $plan_name == 'L34' )
        $customer_info_array['voice_minutes'] = mvneGetVoiceMinutes($customer->customer_id);
    }
    else
      $errors = $results['errors'];

  }
  else
    $errors = array("ERR_API_INVALID_ARGUMENTS: customer not found from input value = $phone_number");

  $return_values = array_merge(
    $customer_info_array,
    array(
      'success'  => $success,
      'errors'   => $errors,
      'warnings' => $warnings
    )
  );

  return flexi_encode(fill_return($p,
                                  "customercare__CustomerInfo",
                                  func_get_args(),
                                  $return_values)
  );
}

