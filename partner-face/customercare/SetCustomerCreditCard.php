<?php

// Updates account Credit Card to a user
function customercare__SetCustomerCreditCard($partner_tag, $customer_id, $account_cc_exp, $account_cc_cvv, $account_cc_number, $account_zipcode)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    'customercare__SetCustomerCreditCard',
                                    func_get_args(),
                                    array('success'  => TRUE,
                                          'errors'   => array(),
                                          'warnings' => array('ERR_API_INTERNAL: always_succeed'))));

  $errors = validate_params($p, 'customercare__SetCustomerCreditCard', func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  'customercare__SetCustomerCreditCard',
                                    func_get_args(),
                                    array('success'  => FALSE,
                                          'warnings' => array(),
                                          'errors'   => $errors)));

  teldata_change_db(); // connect to the DB

  $customer      = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID'));
  $customers_row = customers_get_customer_by_customer_id($customer_id, array('LAST_NAME'));

  if ( $customer && $customers_row )
  {
    // validate and store credit card info - this includes tokenization
    $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
      array(
        'customer_id'    => $customer_id,
        'cc_number'      => $account_cc_number,
        'cvv'            => $account_cc_cvv,
        'expires_date'   => $account_cc_exp,
        'cc_postal_code' => $account_zipcode
      ),
      $customers_row->LAST_NAME
    );

    if ( $result->is_failure() )
      $errors = $result->get_errors();
  }
  else
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: customer not found';

  return flexi_encode(fill_return($p,
                                  'customercare__SetCustomerCreditCard',
                                  func_get_args(),
                                  array('success'  => ( ! count( $errors ) ),
                                        'warnings' => array(),
                                        'errors'   => $errors)));
}

?>
