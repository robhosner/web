<?php


// Extracts an Inbound Notification Message from the ACC MW Message queue (Perl notification listener) and process it.
function middleware__PollAccNotification($partner_tag)
{
  global $p;
  global $mock;
  global $always_succeed;

  $uuid = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "middleware__PollAccNotification",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "middleware__PollAccNotification", func_get_args(), $mock);

  if ( !($errors && count($errors) > 0)
    && !\Ultra\UltraConfig\middleware_enabled_amdocs()
  )
    $errors[] = "ERR_API_INTERNAL: middleware currently disabled";

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "middleware__PollAccNotification",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));

  $endPoint = new \Ultra\Lib\MQ\EndPoint;

  // is there an inbound notification waiting in the ACC MW Notification Channel? (from the Perl notification listener)
  if ( $endPoint->peekNotificationChannelACCMW() )
  {
    // dequeue inbound ACC MW Notification Channel
    $message = $endPoint->dequeueNotificationChannelACCMW();

    if ( $message )
    {
      dlog('',"message = %s",$message);

      $data = $endPoint->extractFromMessage($message);

      if ( $data )
      {
        $data = (array) $data;

        dlog('',"data = %s",$data);

        $uuid = $data['_uuid'];

        $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Notification;

        $result = $accMiddleware->processNotification(
          array(
            'actionUUID' => $data['_actionUUID'],
            'uuid'       => $uuid,
            'command'    => $data['header'],
            'parameters' => $data['body']
          )
        );

        if ( $result->is_failure() )
          $errors = $result->get_errors();
      }
      else
      {
        $errors[] = "ERR_API_INTERNAL: Cannot extract data from message";
      }
    }
  }
  else
  {
    #dlog('',"No inbound message in ACC MW Notification Channel.");

    $p['log_skip'] = TRUE;
  }

  $success = ! (count($errors));

  return flexi_encode(fill_return($p,
                                  "middleware__PollAccNotification",
                                  func_get_args(),
                                  array("success" => $success,
                                        "uuid"    => $uuid,
                                        "errors"  => $errors)));
}

?>
