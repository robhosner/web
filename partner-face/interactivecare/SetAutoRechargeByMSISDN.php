<?php

/**
 * interactivecare__SetAutoRechargeByMSISDN
 *
 * enable or disable monthly auto recharge with credit card
 *
 * @see https://issues.hometowntelecom.com:8443/browse/MVNO-2424
 * @param string MSISDN
 * @param boolean auto_regarge
 * @return string JSON encoded result
 * @author VYT 14-05-29
 */
function interactivecare__SetAutoRechargeByMSISDN($partner_tag, $msisdn, $auto_recharge)
{
  global $p;
  global $mock;
  global $always_succeed;

  // init
  $success = FALSE;
  $errors = array();
  $warnings = array();

  try
  {
    // always succeed
    if ($always_succeed)
    {
      $success = TRUE;
      $warnings[] = 'ERR_API_INTERNAL: always_succeed';
      throw new Exception();
    }

    // check parameters
    $errors = validate_params($p, __FUNCTION__, func_get_args(), $mock);
    if ($errors && count($errors) > 0)
      throw new Exception(implode(', ', $errors));

    // get customer from msisdn
    teldata_change_db();
    $customer = get_customer_from_msisdn($msisdn);
    if (! $customer)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: no customer found');

    // check plan state
    $state = internal_func_get_state_from_customer_id($customer->customer_id);
    if ( ! in_array($state['state'], array('Active', 'Neutral', 'Port-In Requested', 'Pre-Funded', 'Provisioned')))
      throw new Exception("ERR_API_INVALID_ARGUMENTS: cannot set auto rechare on {$state['state']} customer");

    // check that CC is on file
    if (! customer_has_credit_card($customer))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: customer does not have credit card on file');

    // update auto renewal
    $params = array(
        'customer_id'        => $customer->CUSTOMER_ID,
        'monthly_cc_renewal' => $auto_recharge);

    // if auto_recharge is set to 0 than set easypay_activated to 0
    if ( ! $auto_recharge)
      $params['easypay_activated'] = $auto_recharge;
     
    $query = htt_customers_overlay_ultra_update_query($params);
    if (is_mssql_successful(logged_mssql_query($query)))
      $success = TRUE;
    else
      $errors[] = 'ERR_API_INTERNAL: failed to update customer auto recharge';
  }
  catch (Exception $e)
  {
    dlog('', 'ERROR: ' . $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return flexi_encode(fill_return(
    $p,
    __FUNCTION__,
    func_get_args(),
    array(
      'success' => $success,
      'warnings' => $warnings,
      'errors'  => $errors)));

}

?>
