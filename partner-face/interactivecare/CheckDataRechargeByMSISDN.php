<?php

// Return to the caller info about customer and his/her Voice Recharges.

function interactivecare__CheckDataRechargeByMSISDN($partner_tag,$msisdn)
{
  $retval = json_decode(interactivecare__doCheckDataRechargeByMSISDN($partner_tag,$msisdn),TRUE);

  $clean_error=count($retval['errors']);

  if (count($retval['errors']))
    $clean_error = preg_replace ('/ERR_API_[^:]+: /', '', $retval['errors'][0]);

  $retval['clean_error']=$clean_error;
  return json_encode($retval);
}


// Return to the caller info about customer and his/her Data Recharges.
function interactivecare__doCheckDataRechargeByMSISDN($partner_tag, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "interactivecare__CheckDataRechargeByMSISDN",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "interactivecare__CheckDataRechargeByMSISDN", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "interactivecare__CheckDataRechargeByMSISDN",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  $days_plan_expires = '';
  $data_recharge     = '';

  teldata_change_db();

  $customer = get_customer_from_msisdn($msisdn);

  if ( $customer )
  {
    if ( ! $customer->COS_ID )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer is invalid";
    else
    {
      $success = TRUE;

      $days_plan_expires = $customer->days_before_expire;

      // Format things so the JSON can be parsed by 3ci - from Kate
      $data_recharge     = array ();
      $plans             = get_data_recharge_by_plan( get_plan_from_cos_id($customer->COS_ID) );

      $planCount = 0;
      foreach ($plans as $plan)
      {
        $planCount++;
        $key = sprintf ('plan%d', $planCount);
        $data_recharge[$key] = array (
          'MB'          => $plan['MB'],
          'data_soc_id' => $plan['data_soc_id'],
          'cost'           => sprintf("%01.2f",$plan['cost'])
        );
      }
    }
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";

  return flexi_encode(fill_return($p,
                                  "interactivecare__CheckDataRechargeByMSISDN",
                                  func_get_args(),
                                  array("success"               => $success,
                                        "number_of_plans"       => $planCount, 
                                        "days_plan_expires"     => $days_plan_expires,
                                        "data_recharge"         => $data_recharge,
					"wallet_balance"    => sprintf("%01.2f",$customer->BALANCE),
                                        "customer_id"       => $customer->customer_id,  
                                        "errors"            => $errors)));
}

?>
