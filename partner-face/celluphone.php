<?php

include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_coverage_info.php');
include_once('fraud.php');
include_once('lib/inventory/functions.php');
include_once('lib/provisioning/functions.php');
include_once('lib/state_machine/functions.php');
include_once('lib/transitions.php');
include_once('partner-face/inventory-partners-include.php');
include_once('partner-face/provisioning-partners-include.php');
include_once('partner-face/provisioning-public-include.php');
include_once('partner-face/customercare/CustomerInfoByID.php');
include_once('partner-face/customercare/DealerLoginAsCustomer.php');
include_once('partner-face/customercare/ReplaceSIMCardByMSISDN.php');
include_once('partner-face/customercare/SearchCustomers.php');
include_once('partner-face/customercare/ValidateMSISDN.php');

?>
