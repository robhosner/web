<?php

// Allows you to check a customer's balance and status.
function externalpayments__CheckMobileBalance($partner_tag, $phone_number, $request_epoch, $store_zipcode, $store_zipcode_extra4, $store_id, $clerk_id, $terminal_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "externalpayments__CheckMobileBalance",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  // phone number validation is included in this call
  $errors = validate_params($p, "externalpayments__CheckMobileBalance", func_get_args(), $mock);

  $created_date = NULL;
  $customer_balance = NULL;
  $iccid = NULL;
  $plan_name = NULL;
  $plan_cost = NULL;
  $plan_state = NULL;
  $dealer_code = NULL;
  $brand = null;

  if ( ! ($errors && count($errors) > 0) )
  {
    teldata_change_db(); // connect to the DB
    $customer = get_customer_from_msisdn($phone_number);

    if ( $customer )
    {
      $plan_state = $customer->plan_state;

      // PROD-1664: setting default timezone to UTC changes log times
      $tz = date_default_timezone_get();
      date_default_timezone_set('UTC');
      $created_date = strtotime($customer->CREATION_DATE_TIME);
      date_default_timezone_set($tz);

      if (in_array($plan_state, array('Active', 'Suspended', 'Provisioned', 'Port-In Requested')))
      {
        if ($customer->COS_ID)
        {
          $short_plan_name = get_plan_from_cos_id($customer->COS_ID);
          $plan_name = get_plan_name_from_short_name($short_plan_name);
          $plan_cost = substr($short_plan_name, -2);
          $customer_balance = $customer->BALANCE;
          
          if ($customer->CURRENT_ICCID_FULL)
            $iccid = $customer->CURRENT_ICCID_FULL;
          elseif ($customer->current_iccid)
            $iccid = $customer->current_iccid;
        }
        else
          $errors = array("ERR_API_INVALID_ARGUMENTS: plan not found from MSISDN $phone_number");
      }

      // MVNO-2595: get activating dealer
      $dealer = get_dealer_from_customer_id($customer->customer_id);
      if ($dealer)
        $dealer_code = $dealer->Dealercd;
    }
    else
    {
      $errors = array("ERR_API_INVALID_ARGUMENTS: customer not found from MSISDN $phone_number");

      # fraud_event($customer, 'externalpayments', 'CheckMobileBalance', 'error', $fraud_data);

      $phone_number = '';
    }
    
  }

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "externalpayments__CheckMobileBalance",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  $output_array = fill_return($p,
                              "externalpayments__CheckMobileBalance",
                              func_get_args(),
                              array('success'           => TRUE,
                                    'errors'            => $errors,
                                    'warnings'          => array(),
                                    'phone_number'      => $phone_number,
                                    'customer_balance'  => floor($customer_balance * 100) / 100,
                                    'customer_active'   => $customer->plan_state == 'Active',
                                    'plan_state'        => $plan_state,
                                    'brand'             => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
                                    'created_date'      => $created_date,
                                    'plan_name'         => $plan_name,
                                    'plan_cost'         => $plan_cost,
                                    'iccid'             => $iccid,
                                    'dealer_code'       => $dealer_code));

  return flexi_encode($output_array);
}

?>
