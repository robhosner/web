<?php


include_once('db/customers.php');
include_once('lib/internal/functions.php');
include_once('lib/messaging/functions.php');


// Sends new login password via SMS, resets PASSWORD.  With TTL, the new password will expire in that many seconds.
function portal__ResetPasswordSMS($partner_tag, $login_string)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ResetPasswordSMS",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ResetPasswordSMS", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ResetPasswordSMS",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = FALSE;

  if ( is_numeric($login_string) )
  {
    $customer = find_first(\Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA',
      1, array('CUSTOMER_ID'), array('current_mobile_number' => $login_string), NULL, NULL, TRUE));
  }

  if ( ! $customer )
  {
    $customer = find_first(\Ultra\Lib\DB\makeSelectQuery('CUSTOMERS',
      1, array('CUSTOMER_ID'), array('LOGIN_NAME' => $login_string), NULL, NULL, TRUE));
  }

  if ( $customer )
  {

    # generate a temporary password ( token = 'reset_password' )

    $password = func_create_temp_password( $customer->CUSTOMER_ID , 'reset_password', 60*60*24);

    $result = funcSendExemptCustomerSMSTempPassword(
      array(
        'customer'      => $customer,
        'temp_password' => $password
      )
    );

    if ( count($result['errors']) == 0 )
    {

      # reset user password

      $check = reset_customer_password( $customer->CUSTOMER_ID );

      if ( $check )
      {
        $success = TRUE;
      }
      else
      { $errors[] = "ERR_API_INTERNAL: DB error"; }

    }
    else
    { $errors[] = "ERR_API_INTERNAL: internal error"; }

  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer or mobile number not found"; }

  return flexi_encode(fill_return($p,
                                  "portal__ResetPasswordSMS",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
