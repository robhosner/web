<?php

// Get a customer's email address and email them their username.
function portal__ForgotUsername($partner_tag, $email)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ForgotUsername",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ForgotUsername", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ForgotUsername",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  $customers = get_customers_from_email($email);

  if ( $customers && is_array($customers) && count($customers) )
  {
    if ( count($customers) == 1 )
    {
      if ( $customers[0]->LOGIN_NAME != '' )
      {
        $template_name = 'ultra-send-username';
        
        $postage_result = postage_post_template($template_name,
                                                array($customers[0]),
                                                FALSE,
                                                $customers[0]->LOGIN_NAME);

        dlog('',"postage_post_template output: %s",$postage_result);

        $success = TRUE;
      }
      else
      { $errors[] = "ERR_API_INTERNAL: user has no Username in DB."; }
    }
    else
    { $errors[] = "ERR_API_INTERNAL: duplicate email."; }
  }
  else
  { $errors[] = "ERR_API_INTERNAL: customer not found."; }

  return flexi_encode(fill_return($p,
                                  "portal__ForgotUsername",
                                  func_get_args(),
                                  array("success"    => $success,
                                        "errors"     => $errors)));
}

?>
