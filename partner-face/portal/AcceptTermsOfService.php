<?php

// Record when the customer accepts the Terms Of Service
function portal__AcceptTermsOfService($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__AcceptTermsOfService",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__AcceptTermsOfService", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__AcceptTermsOfService",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  $success = func_accept_terms_of_service($customer_id);

  if ( ! $success )
  {
    $errors[] = "ERR_API_INTERNAL: DB error";
  }

  return flexi_encode(fill_return($p,
                                  "portal__AcceptTermsOfService",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
