<?php

// Overwrites Marketing Settings for a logged in customer.
function portal__SetMarketingSettings($partner_tag, $zsession, $marketing_settings)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__SetMarketingSettings",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__SetMarketingSettings", func_get_args(), $mock);

  if ( ! ($errors && count($errors) > 0) )
  {
    $errors = validate_marketing_settings_array($marketing_settings);
  }

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__SetMarketingSettings",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) == 0 )
  {
    $customer = $data_zsession['customer'];

    $update_params = array(
      "customer_id" => $customer->CUSTOMER_ID
    );

    for( $i=0 ; $i<count($marketing_settings) ; $i+=2 )
    {
      if ($marketing_settings[$i] == 'marketing_voice_option')
      {
        $voice_option = $marketing_settings[$i + 1];

        $result = NULL;
        if ($voice_option)
          $result = opt_in_voice_preference($customer->CUSTOMER_ID);
        else
          $result = opt_out_voice_preference($customer->CUSTOMER_ID);

        if (!$result->is_success())
          throw new Exception('ERR_API_INTERNAL: Error writing to ULTRA.CUSTOMER_OPTIONS');
      }
      else
        $update_params[$marketing_settings[$i]] = $marketing_settings[$i+1];
    }

    $errors = set_marketing_settings( $update_params );

    if ( ! count($errors) )
    {
      $success = TRUE;
    }
  }
  else
  { $errors = $data_zsession['errors']; }

  return flexi_encode(fill_return($p,
                                  "portal__SetMarketingSettings",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

function validate_marketing_settings_array($marketing_settings)
{
  $errors = array();

  if ( count($marketing_settings) % 2 != 0 )
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: marketing_settings is invalid (length)";
  }
  else
  {
    $allowed_marketing_settings = array(
      'marketing_email_option',
      'marketing_sms_option',
      'marketing_voice_option'
    );

    foreach( $marketing_settings as $id => $string )
    {
      if ( ( $id % 2 ) && ( $string != '0' ) && ( $string != '1') )
      {
        $errors[] = "ERR_API_INVALID_ARGUMENTS: marketing_settings is invalid (values should be 0 or 1)";
      }
      else if ( ! ( $id % 2 ) && ! ( in_array( $string , $allowed_marketing_settings ) ) )
      {
        $errors[] = "ERR_API_INVALID_ARGUMENTS: marketing_settings is invalid (field mismatch)";
      }
    }
  }

  return $errors;
}

// curl -i 'https://rgalli3-dev.uvnv.com/pr/portal/1/ultra/api/portal__SetMarketingSettings.json' -u dougmeli:Flora -d 'zsession=1&marketing_settings[]=marketing_sms_option&marketing_settings[]=1&marketing_settings[]=marketing_email_option&marketing_settings[]=0'

?>
