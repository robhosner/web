<?php

// Try to activate a suspended customer.
function portal__ActivateSuspendedCustomer($partner_tag, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ActivateSuspendedCustomer",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__ActivateSuspendedCustomer", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__ActivateSuspendedCustomer",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $return_array['errors'] = $data_zsession['errors'];
  }
  else
  {
    $customer = $data_zsession['customer'];
  }

  if ( $customer )
  {
    /* *** get current state *** */
    $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

    if ( $state )
    {

      if ( $state['state'] == 'Suspended' )
      {
        $context = array(
          'customer_id' => $customer->CUSTOMER_ID
        );

        $result_status = reactivate_suspended_account($state,$customer,$context);

        if ( count($result_status['errors']) )
        {
          $errors = $result_status['errors'];
        }
        else
        {
          $success = TRUE;
        }

      }
      elseif ( $state['state'] != 'Active' )
      { # invalid state for this command
        $return_array['errors'][] = "ERR_API_INTERNAL: customer state not Suspended nor Active";
      }
      else
      {
        $success = TRUE;
      }

    }
    else # cannot determine customer state
    { $return_array['errors'][] = "ERR_API_INTERNAL: customer state could not be determined"; }

  }
  else # customer not found
  { $return_array['errors'][] = "ERR_API_INTERNAL: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "portal__ActivateSuspendedCustomer",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>
