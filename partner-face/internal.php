<?php


include_once('db.php');
include_once('db/htt_auditlog.php');
include_once('db/htt_customers_notes.php');
include_once('db/htt_soap_gui_log.php');
include_once('lib/messaging/functions.php');
include_once('lib/payments/functions.php');
include_once('lib/state_machine/aspider.php');
include_once('lib/state_machine/functions.php');
include_once('lib/transitions.php');
include_once('lib/crone-common.php');
include_once('partner-face/internal/CancelFraudCustomer.php');
include_once('partner-face/internal/ChangeState.php');
include_once('partner-face/internal/CheckAction.php');
include_once('partner-face/internal/CheckTransition.php');
include_once('partner-face/internal/GetActionsByTransition.php');
include_once('partner-face/internal/GetAllAbortedTransitions.php');
include_once('partner-face/internal/GetAllCustomerStateTransitions.php');
include_once('partner-face/internal/GetAllPendingTransitions.php');
include_once('partner-face/internal/GetCustomerData.php');
include_once('partner-face/internal/GetPendingTransitions.php');
include_once('partner-face/internal/GetState.php');
include_once('partner-face/internal/IsEligiblePortIn.php');
include_once('partner-face/internal/MonthlyServiceChargeStats.php');
include_once('partner-face/internal/RecoverFailedTransition.php');
include_once('partner-face/internal/QueryMSISDN.php');
include_once('partner-face/internal/QueryPortIn.php');
include_once('partner-face/internal/States.php');
include_once('partner-face/internal/TakeTransition.php');
include_once('classes/postageapp.inc.php');


date_default_timezone_set("America/Los_Angeles");

function internal__Test($customer_id, $test_string, $test_matches, $test_format, $test_date_format)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $errors = validate_params($p, "internal__Test", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__Test",
                                  func_get_args(),
                                  array("success" => FALSE,
                                        "errors" => $errors)));

  return flexi_encode(fill_return($p,
                                  "internal__Test",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

// Allows internal payment processors to add the given amount to the given customer account
function internal__SubmitChargeAmount($partner_tag, $customer_id, $amount, $reason, $reference, $subproduct_id)
{
  # $amount in cents

  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__SubmitChargeAmount",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__SubmitChargeAmount", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__SubmitChargeAmount",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db();

  // retrieve customer data

  $query = make_find_ultra_customer_query_from_customer_id($customer_id);

  $customer = find_customer( $query );

  if ( $customer )
  {
    # $reason is product id
    # For the ULTRA_PLAN_RECHARGE it goes to STORED_VALUE.
    # For the ULTRA_ADD_BALANCE   it goes to BALANCE.

    // see what plan the user has, if theyre are multi month set reason to ULTRA_ADD_BALANCE
    if ( multi_month_info($customer->CUSTOMER_ID))
      $reason = 'ULTRA_ADD_BALANCE';

    # retrieve tracking parameters from Redis

    $redis = new \Ultra\Lib\Util\Redis;

    $redis_epay_key_prepend = "externalpayments/".$reference.'/';

    $store_zipcode        = $redis->get($redis_epay_key_prepend.'store_zipcode');
    $store_zipcode_extra4 = $redis->get($redis_epay_key_prepend.'store_zipcode_extra4');
    $store_id             = $redis->get($redis_epay_key_prepend.'store_id');
    $clerk_id             = $redis->get($redis_epay_key_prepend.'clerk_id');
    $terminal_id          = $redis->get($redis_epay_key_prepend.'terminal_id');
    $source               = $redis->get($redis_epay_key_prepend.'source');
    $detail               = $redis->get($redis_epay_key_prepend.'detail'); // 'detail' is the name of the API that initiated this transaction

    $result = '';

    

    if ( $reason == 'ULTRA_PLAN_RECHARGE' )
    {
      dlog('',"internal__SubmitChargeAmount invokes func_add_stored_value");

      $result = func_add_stored_value(
        array(
          'customer'  => $customer,
          'amount'    => ($amount/100), # in $
          'reason'    => $detail ? $detail : __FUNCTION__,
          'source'    => $source,
          'reference' => $reference, # external payment Transaction ID
          'store_zipcode' => $store_zipcode.$store_zipcode_extra4,
          'store_id'      => $store_id,
          'clerk_id'      => $clerk_id,
          'terminal_id'   => $terminal_id,
          'reactivate_now' => FALSE,
          'pay_quicker'    => substr($subproduct_id, 0, 2) == 'PQ' ? $subproduct_id : FALSE));
    }
    else # $reason == 'ULTRA_ADD_BALANCE'
    {
      dlog('',"internal__SubmitChargeAmount invokes func_add_balance");

      $result = func_add_balance(
        array(
          'customer'  => $customer,
          'amount'    => ($amount/100), # in $
          'reason'    => $detail ? $detail : __FUNCTION__,
          'source'    => $source,
          'reference' => $reference, # external payment Transaction ID
          'store_zipcode' => $store_zipcode.$store_zipcode_extra4,
          'store_id'      => $store_id,
          'clerk_id'      => $clerk_id,
          'terminal_id'   => $terminal_id,
          'reactivate_now' => FALSE
        )
      );
    }

    // RTRIN-6: insert a corresponding row into INCOMM_TRANSACTIONS
    if ($source == 'INCOMM' && ! count($result['errors']))
    {
      if ( ! $transaction = $redis->get($redis_epay_key_prepend . 'transaction'))
        dlog('', "ERROR: failed to get inComm transaction $reference data from redis");
      elseif ( ! log_incomm_transaction($customer->CUSTOMER_ID, $reference, unserialize(gzuncompress($transaction))))
        dlog('', "ERROR: failed to record inComm transaction $reference");
    }

    if ( count($result['errors']) > 0 )
      $errors = $result['errors'];
    else
    {
      /* *** get current state *** */
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      /* *** send SMS to customer *** */

      $caption_amount = caption_amount($amount);

      if (
        $customer->plan_expires
        &&
        $state
        &&
        (
          ( $state['state'] != "Suspended" )
          ||
          ( $reason != 'ULTRA_PLAN_RECHARGE' )
        )
      )
      {
        funcSendExemptCustomerSMSBalanceAdd(
          array(
            'customer'      => $customer,
            'sms_template'  => ( $reason == 'ULTRA_PLAN_RECHARGE' ) ? 'balanceadd_recharge' : 'balanceadd_reload' ,
            'renewal_date'  => get_date_from_full_date( $customer->plan_expires ),
            'reload_amount' => '$'.$caption_amount,
            'gizmo_amount'  => '$'.$caption_amount
          )
        );
      }
      else
      {
        # SMS delivery skipped in initial activation phase
        dlog('',"$reason SMS delivery skipped since this is the initial activation phase");
      }

      if ( ! $state )
      {
        # non-fatal error
        dlog('',"We could not determine customer state in internal__SubmitChargeAmount");
      }

      $success = TRUE;
    }
  }
  else
    $errors = array("Customer id \"$customer_id\" not found");

  return flexi_encode(fill_return($p,
                                  "internal__SubmitChargeAmount",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

// cron runner, or needful errand
function internal__Crone($partner_tag, $mode)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__Crone",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__Crone", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__Crone",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));

  return flexi_encode(fill_return($p,
                                  "internal__Crone",
                                  func_get_args(),
                                  crone_run($mode)));
}

// Resolve all pending (is_closed_or_aborted is false) transition IDs
function internal__ResolvePendingTransitions($partner_tag, $customer_id, $environments)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;
  $transitions = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__ResolvePendingTransitions",
                                    func_get_args(),
                                    array("success"     => TRUE,
                                          "transitions" => $transitions,
                                          "warnings"    => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__ResolvePendingTransitions", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__ResolvePendingTransitions",
                                    func_get_args(),
                                    array("success"     => $success,
                                          "transitions" => $transitions,
                                          "errors"      => $errors)));

  teldata_change_db();

  $params = array();

  if ( isset($customer_id) )
    $params = array( 'customer_id' => $customer_id, 'environments' => $environments );
  else
    $params = array( 'customer_id' => NULL, 'environments' => $environments );

  return flexi_encode(fill_return($p,
                                  "internal__ResolvePendingTransitions",
                                  func_get_args(),
                                  internal_func_resolve_pending_transitions($params)));
}

// Examples:
// curl -i 'https://$DOMAIN/pr/internal/1/ultra/api/internal__SubmitChargeAmount' -d 'customer_id=25&amount=1000&reason=a&reference=b'

