<?php

// Check a transition
function internal__CheckTransition($partner_tag, $transition_id, $explain)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__CheckTransition",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__CheckTransition", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__CheckTransition",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db();

  if ($explain)
  {
    foreach (explain_transition($transition_id) as $e)
    {
      echo $e;
    }
    exit;
  }


  $transitions = internal_func_check_transition(array('transition_uuid' => $transition_id));

  if (NULL == $transitions)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__CheckTransition",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => array("ERR_API_INTERNAL: could not load transition!"))));
  }

  $transition = NULL;
  $actions = array();
  foreach ($transitions as $t)
  {
    $action_status = 'unknown';
    if (NULL != $t->ACTION_UUID && NULL != $t->ACTION_STATUS)
    {
      if ('CLOSED' === $t->ACTION_STATUS ||
          'ABORTED' === $t->ACTION_STATUS ||
          'PENDING' === $t->ACTION_STATUS ||
          'OPEN' === $t->ACTION_STATUS)
      {
        $action_status = strtolower($t->ACTION_STATUS);
      }

      $actions[$action_status] = $t->ACTION_UUID;
    }

    $transition = $t;
  }

  $ret = array("success" => TRUE,
               'customer_id' => $transition->CUSTOMER_ID,
               'status' => $transition->STATUS,
               'created' => $transition->created_epoch,
               'closed' => $transition->closed_epoch,
               'is_closed_or_aborted' => $transition->is_closed_or_aborted,
               "errors" => $errors);

  foreach ($actions as $status => $arr)
  {
    $ret["${status}_actions"] = $arr;
  }

  return flexi_encode(fill_return($p,
                                  "internal__CheckTransition",
                                  func_get_args(),
                                  $ret));
}

?>
