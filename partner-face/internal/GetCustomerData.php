<?php

// Get customer data for our UI tools.
function internal__GetCustomerData($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $customer_info_array = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__GetCustomerData",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__GetCustomerData", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__GetCustomerData",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  =>  $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $results = get_customer_state( $customer );

    if ( count($results['errors']) == 0 )
    {
      $customer_address = $customer->ADDRESS1;

      if ( $customer->ADDRESS2 ) { $customer_address .= $customer->ADDRESS2; }

      $plan_name = get_plan_from_cos_id( $customer->cos_id );

      $customer_info_array = array(
        'customer_plan'            => \Ultra\UltraConfig\getUltraPlanConfigurationItem($plan_name, 'name'),
        'customer_plan_start'      => $customer->plan_started_epoch,
        'customer_plan_expires'    => $customer->plan_expires_epoch,
        'preferred_language'       => $customer->preferred_language,
        'customer_stored_value'    => $customer->stored_value,
        'customer_minutes'         => $customer->minutes,
        'customer_balance'         => $customer->BALANCE,
        'packaged_balance1'        => $customer->PACKAGED_BALANCE1,
        'customer_status'          => $customer->plan_state,
        'customer_loginname'       => $customer->LOGIN_NAME,
        'customer_password'        => $customer->LOGIN_PASSWORD,
        'current_mobile_number'    => $customer->current_mobile_number,
        'recharge_status'          => $results['recharge_status'],
        'current_iccid'            => $customer->CURRENT_ICCID_FULL,
        'brand'                    => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
        'activation_iccid'         => $customer->ACTIVATION_ICCID,
        'first_name'               => $customer->FIRST_NAME,
        'last_name'                => $customer->LAST_NAME,
        'email'                    => $customer->E_MAIL,
        'address'                  => $customer_address,
        'city'                     => $customer->CITY,
        'state'                    => $customer->STATE_REGION,
        'country'                  => $customer->COUNTRY,
        'activation_start_date'    => customer_activation_start_date( $customer->CUSTOMER_ID ),
        'customer_has_credit_card' => customer_has_credit_card($customer)
      );

      $success = TRUE;
    }
    else
    { $errors = $results['errors']; }

  }
  else
  { $errors = array("ERR_API_INVALID_ARGUMENTS: customer not found"); }

  $return_values = array_merge(
    $customer_info_array,
    array(
      "success" => $success,
      "errors"  => $errors
    )
  );

  return flexi_encode(fill_return($p,
                                  "internal__GetCustomerData",
                                  func_get_args(),
                                  $return_values));
}

?>
