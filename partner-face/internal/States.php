<?php

// State Machine
function internal__States($partner_tag)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__States",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__States", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__States",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  return flexi_encode(fill_return($p,
                                  "internal__States",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "plans" => ultra_plans(),
                                        "states" => ultra_states(),
                                        "transitions" => ultra_transitions(),
                                        "errors" => $errors)));
}

?>
