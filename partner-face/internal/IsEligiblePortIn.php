<?php

// invokes PortInEligibility
function internal__IsEligiblePortIn($partner_tag, $customer_id, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__IsEligiblePortIn",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__IsEligiblePortIn", func_get_args(), $mock);

  if ( $customer_id != -1 )
  {
    if ( is_numeric( $customer_id ) )
    {
      $customer = find_customer( make_find_ultra_customer_query_from_customer_id($customer_id) );

      if ( ! $customer )
        $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id should be -1 or the identifier of an existing customer";
    }
    else
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer_id should be -1 or the numeric identifier of an existing customer";
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__IsEligiblePortIn",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $mwResult = $mwControl->mwPortInEligibility(
    array(
      'actionUUID' => $request_id,
      'msisdn'     => $msisdn),
    TRUE
  );

  $success  = ( $mwResult->is_success() && $mwResult->get_data_key('eligible') );
  $warnings = $mwResult->get_warnings();
  $errors   = $mwResult->get_errors();

  return flexi_encode(fill_return($p,
                                  "internal__IsEligiblePortIn",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "eligible" => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

?>
