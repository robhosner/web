<?php


include_once("db/htt_customers_overlay_ultra.php");
include_once("db/htt_portin.php");


// Used to resume an already failed port. Funding and plan already exists for the customer; the only information that is necessary is a resubmission of the phone number.
function provisioning__requestResubmitProvisionPortedCustomerAsync($partner_tag, $ICCID, $numberToPort, $portAccountNumber, $portAccountPassword, $portAccountZipcode)
{
  global $p;
  global $mock;
  global $always_succeed;

  # trim spaces
  $portAccountPassword = trim($portAccountPassword);
  $portAccountNumber   = trim($portAccountNumber);

  $request_id = NULL;
  $success    = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__requestResubmitProvisionPortedCustomerAsync",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__requestResubmitProvisionPortedCustomerAsync", func_get_args(), $mock);

  $result_status = array(
    'transitions' => NULL,
    'actions'     => NULL
  );

  /* Disabled verification because there's no reliable way to find out
   * if the MSISDN and ICCID are the same, and in theory a second
   * port-in with the same MSISDN should not be possible.  Also see
   * MVNO-945. */

  teldata_change_db(); // connect to the DB

  save_port_account( $numberToPort , $portAccountNumber , $portAccountPassword );

  # $numberToPort must match the first port request and is used to find the correct customer_id

  $customer = NULL;

    $portInQueue = new \PortInQueue();

    $loadByMsisdnResult = $portInQueue->loadByMsisdn( $numberToPort );

    teldata_change_db(); // connect back to default DB

    $errors = $loadByMsisdnResult->get_errors();

    if ( ! count($errors) )
      if ( !$portInQueue->customer_id )
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: previous porting attempt not found (2)';
      else
      {
        $query = make_find_ultra_customer_query_from_customer_id( $portInQueue->customer_id );
        $customer = find_customer( $query );

        if ( luhnenize($ICCID) != $customer->CURRENT_ICCID_FULL )
          $errors[] = "ERR_API_INVALID_ARGUMENTS: $ICCID is not associated with $numberToPort";
      }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "provisioning__requestResubmitProvisionPortedCustomerAsync",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  /* *** get current state *** */
  $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

  if ( $state )
  {

    if ( $state['state'] == 'Port-In Requested' )
    {

      ##==-- add action for mvneUpdatePortIn --==##

      $action_seq  = NULL;
      $action_type = 'funcall';
      $action_name = 'mvneUpdatePortIn';
      $params      = array(
        $customer->CUSTOMER_ID,
        $numberToPort,
        $ICCID,
        $portAccountNumber,
        $portAccountPassword,
        $portAccountZipcode,
        "__this_transition__",
        "__this_action__",
        "__this_action_seq__",
      );

      $context = array(
        'customer_id' => $customer->CUSTOMER_ID
      );

      $result_status = append_action($context,
                                     array('type' => $action_type,
                                           'name' => $action_name,
                                           'transaction' => NULL,
                                           'seq' => $action_seq
                                     ),
                                     $params,
                                     FALSE
      );

      if ( ! empty($result_status['transitions'][0]))
        unreserve_transition_uuid_by_pid($request_id = $result_status['transitions'][0]);

      if ( $result_status['success'] == 1 )
      {
        // memorize $request_id in Redis by customer_id
        $redis_port = new \Ultra\Lib\Util\Redis\Port();
        $redis_port->setRequestId( $customer->CUSTOMER_ID , $request_id );

        $success = TRUE;
      }
      else # append_action error
      { $errors[] = "ERR_API_INTERNAL: state transition error (0)"; }

    }
    else # customer not in correct state
    { $errors[] = "ERR_API_INTERNAL: customer state not valid"; }

  }
  else # cannot determine customer state
  { $errors[] = "ERR_API_INTERNAL: customer state could not be determined"; }

  return flexi_encode(fill_return($p,
                                  "provisioning__requestResubmitProvisionPortedCustomerAsync",
                                  func_get_args(),
                                  array("success"     => $success,
                                        'transitions' => $result_status['transitions'],
                                        'actions'     => $result_status['actions'],
                                        "request_id"  => $request_id,
                                        "errors"      => $errors)));
}

?>
