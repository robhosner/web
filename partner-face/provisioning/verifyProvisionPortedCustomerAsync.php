<?php

// This is the API to be invoked after requestProvisionPortedCustomerAsync in order to get the complete response
function provisioning__verifyProvisionPortedCustomerAsync($partner_tag, $request_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success      = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__verifyProvisionPortedCustomerAsync",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__verifyProvisionPortedCustomerAsync", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "provisioning__verifyProvisionPortedCustomerAsync",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  // check transition
  $transition_result = provision_check_transition($request_id);

  dlog('',"transition_result = %s",$transition_result);

  $transition_result['errors'] = append_transition_failure_reason($transition_result['errors'],$request_id);

  $r_codes = array();

  if ( isset($transition_result['customer_id']) && $transition_result['customer_id'] )
    // get r codes
    $r_codes = get_porting_r_codes( $transition_result['customer_id'] );

  $porting_status = 'unknown';
  $porting_success = FALSE;
  $porting_final = FALSE;

  if ( isset($transition_result['customer_id']) && $transition_result['customer_id'] )
  {
      // check PORTIN_QUEUE
      $portInQueue = new \PortInQueue();

      $loadByMsisdnResult = $portInQueue->loadByCustomerId( $transition_result['customer_id'] );

      if ( $loadByMsisdnResult->is_success() )
      {
        $transition_result['phone_number'] = $portInQueue->msisdn;
        $transition_result['pending']      = FALSE;
        $porting_status  = 'COMPLETE';
        $porting_final   = TRUE;
        $porting_success = TRUE;
        $r_codes         = '';

        if ( ( $loadByMsisdnResult->port_status != 'COMPLETED' ) && ( $loadByMsisdnResult->port_status != 'COMPLETE' ) )
        {
          list(
            $port_success,
            $port_pending,
            $port_status,
            $port_resolution
          ) = interpret_port_status( $portInQueue );

          $r_codes         = $port_resolution;
          $porting_success = $port_success;
          $porting_final   = !$port_success;
          $porting_status  = $port_status;

          $transition_result['pending'] = $port_pending;
        }
      }
      else
      {
        $transition_result['success'] = FALSE;
        $transition_result['errors']  = $loadByMsisdnResult->get_errors();
      }
  }

  return flexi_encode(fill_return($p,
                                  "provisioning__verifyProvisionPortedCustomerAsync",
                                  func_get_args(),
                                  array("success"      => $transition_result['success'],
                                        "phone_number" => $transition_result['phone_number'],
                                        "status"       => $transition_result['status'], # customer status
                                        "porting_final"  => $porting_final,
                                        "porting_status"  => $porting_status,
                                        "porting_success" => $porting_success,
                                        "resolution"          => $r_codes,
                                        /* 'transition_result' => $transition_result, */
                                        "pending"      => $transition_result['pending'],
                                        "warnings"     => $transition_result['warnings'],
                                        "errors"       => $transition_result['errors'])));
}

?>
