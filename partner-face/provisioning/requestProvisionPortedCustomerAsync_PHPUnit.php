<?php

require_once 'db.php';
require_once 'classes/PHPUnitBase.php';

class requestProvisionPortedCustomerAsyncTest extends PHPUnitBase
{
  public function setUp()
  {
    $partner = 'provisioning';
    $api = 'requestProvisionPortedCustomerAsync';

    // API setup
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'bath'      => 'rest',
      'version'   => 1,
      'partner'   => 'provisioning',
    ));

    parent::setUp();
  }
  public function test__main()
  {
    $params = array();
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // required
    $params['ICCID']        = '8901260962115472166';
    $params['loadPayment']  = 'CCARD';
    $params['targetPlan']   = 'UV30';
    $params['numberToPort'] = '7146753662';
    $params['customerZip']  = '99';
    $params['activation_masteragent'] = '1';
    $params['activation_agent']       = '1';
    $params['activation_userid']      = '1';

    $params['portAccountNumber']   = '12345';
    $params['portAccountPassword'] = '54321';
    $params['portAccountZipcode']  = 34743;

    // iccid is currently being used
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // OK at time of test
    $params['ICCID'] = '1010101010101010606';

    // iccid is ok, zipcode is invalid
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    $params['customerZip']  = '34743';

    // credit card validation errors
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // 
    $params['creditCard']         = '1234123412341234';
    $params['creditExpiry']       = '1122';
    $params['creditCVV']          = '111';
    $params['creditAutoRecharge'] = '0';
    $params['creditAmount']       = '3000';

    // less credit card validation errors
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    $params['customerAddressOne'] = '555 Address street';
    $params['customerAddressTwo'] = '';
    $params['customerCity']       = 'SomeCity';
    $params['customerState']      = 'CA';

    // port in denied due to customer state
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // change load payment type
    // port in denied due to customer state
    $params['loadPayment'] = 'NONE';
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // invalid pins
    // $params['loadPayment'] = 'PINS';
    // $params['pinsToApply'] = '5551115551,6661116661';
    // $result = $this->callApi($params);
    // $this->assertFalse($result->success);

    // intra port, test customer will be cloned and soft cancelled
    // $params['numberToPort'] = '6094019292';

    // not eligible for port in
    $params['numberToPort'] = '5555555555';
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // error verifying cc number
    $params['numberToPort'] = '7144959606';
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    //
    $params['customerFirstName'] = 'James';
    $params['customerLastName']  = 'Steinmetz';
    $params['customerEMail']     = '';

    $params['activation_distributor'] = '';
    $params['activation_store']       = '';
    
    $params['preferred_language']     = '';
    $params['dealer_code']            = '';
    $params['bolt_ons']               = '';

    $result = $this->callApi($params);
    $this->assertFalse($result->success);
  }
}
