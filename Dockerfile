FROM localhost:5000/web/basephp71:1.0.1

ENV HOME=/home/ht/www/api

# Adding the configuration file of the nginx
ADD DockerDependencies/nginx.conf /etc/nginx/nginx.conf

# Add api configuration dependencies
ADD DockerDependencies/api.ultra.me.conf /etc/nginx/sites-available/api.ultra.me.conf
ADD DockerDependencies/api.ultra.me.conf /etc/nginx/sites-enabled/api.ultra.me.conf
ADD DockerDependencies/supervisord.conf /etc/supervisord.conf

# Add php.ini
ADD DockerDependencies/php.ini /etc/opt/remi/php71/php.ini
ADD DockerDependencies/php-fpm.conf /etc/opt/remi/php71/php-fpm.conf

ADD DockerDependencies/logrotate.conf /etc/logrotate.conf
ADD DockerDependencies/htt /etc/logrotate.d/htt

RUN chmod -R 644 /etc/logrotate.d && chmod -R 644 /etc/logrotate.conf

WORKDIR $HOME/web

COPY . $HOME/web

RUN composer update && \
    cp -a -r vendor/apache/log4php/src/main/php/. $HOME/web/log4php && \
    cp -a -r vendor/phpseclib/phpseclib/phpseclib/Crypt $HOME/web/Crypt && \
    cp -a -r vendor/pear/validate/Validate.php $HOME/web/ && \
    cp -a -r vendor/pear/validate_finance_creditcard/Validate $HOME/web/Validate && \
    mkdir -p /opt/php/vendor/ && cp -a -r vendor/predis/predis/ /opt/php/vendor/ && \
    chmod +x dockerRunPhpunit.sh && chmod +x DockerRunApi.sh

RUN yum -y install php71-php-pecl-xdebug

CMD ["supervisord", "-n"]