var hotcoldViewModel;
function AppViewModel() {

    var self = this;

    //self.baseUrl = 'https://' + l_sApiBase;

    /////////////////////////////////
    //KNOCKOUT.JS OBSERVABLE BOOLEANS
    self.thinking0 = ko.observable(false);
    self.thinking1 = ko.observable(false);
    self.formError = ko.observable(false);
    self.validSearch = ko.observable(false);



    //KNOCKOUT.JS OBSERVABLE VALUES
    self.master = ko.observable('');
    self.iccidStart = ko.observable('');
    self.iccidEnd = ko.observable('');
    self.error0 = ko.observable('');
    self.error1 = ko.observable('');
    self.success1 = ko.observable('');
    self.activeNeutralCount = ko.observable(0);
    self.form_errors = ko.observable({});
    self.sims = ko.observableArray([]);

    //EO KNOCKOUT.JS OBSERVABLES
    ////////////////////////////


    ////////////////////////////////
    //NON-OBSERVABLE CLASS VARIABLES

    self.masters = ko.observableArray([
        {id: '32', name: "ABCMaster"},
        {id: '59', name: "Advanced Mobile"},
        {id: '200', name: "Alex Master Agent Test"},
        {id: '614', name: "American Telecommunications, Inc"},
        {id: '48', name: "Arrow Master"},
        {id: '46', name: "B-1 Master"},
        {id: '72', name: "Cellucom"},
        {id: '361', name: "Cellular Green"},
        {id: '38', name: "Celluphone California"},
        {id: '625', name: "Choice Products & Services, Inc."},
        {id: '199', name: "Chris 2000"},
        {id: '70', name: "CNCG"},
        {id: '60', name: "CNCG - Delete"},
        {id: '316', name: "Dial World Communications"},
        {id: '633', name: "Dynamic Wireless Corp"},
        {id: '484', name: "epay"},
        {id: '61', name: "Epic Wireless, Inc"},
        {id: '634', name: "Florida Auto Phone Inc"},
        {id: '532', name: "Global Mobile Konnections, LLC"},
        {id: '52', name: "Harry Wireless Inc."},
        {id: '53', name: "Harry Yed Wireless Inc"},
        {id: '635', name: "HK Enterprises Inc"},
        {id: '602', name: "Incomm"},
        {id: '636', name: "Key-Comm International Inc"},
        {id: '637', name: "Lindsay Export Inc"},
        {id: '364', name: "MegaTel Wireless"},
        {id: '620', name: "Midtown Choice"},
        {id: '363', name: "Mobilutions USA"},
        {id: '67', name: "Modern Wireless"},
        {id: '638', name: "N SPIRE Wireless Inc"},
        {id: '616', name: "Nova Ziff, Inc"},
        {id: '622', name: "PCC"},
        {id: '62', name: "Perfect Mobile Inc"},
        {id: '50', name: "Perpaid Master wireless"},
        {id: '639', name: "Pinnacle Wireless LLC"},
        {id: '562', name: "Portables Unlimited"},
        {id: '621', name: "QDI"},
        {id: '56', name: "RayzMobile Corp"},
        {id: '650', name: "Semtus Inc"},
        {id: '66', name: "SIMpro"},
        {id: '640', name: "Smart Choice Mobile Inc"},
        {id: '414', name: "Swadar Solutions Pvt Ltd"},
        {id: '57', name: "testmaster"},
        {id: '424', name: "The Preferred Prepaid"},
        {id: '582', name: "Ultra Giveaways"},
        {id: '69', name: "Ultra Mobile Master Agent"},
        {id: '54', name: "Ultra Online Sales"},
        {id: '492', name: "Ultra Promotions"},
        {id: '648', name: "Ultra TV Master Agent"},
        {id: '641', name: "United Digital & Tanning Inc"},
        {id: '642', name: "United Mobile Solutions LLC"},
        {id: '368', name: "URGE MOBILE"},
        {id: '39', name: "Verizon Wireless Santa Monica"},
        {id: '68', name: "Volante"},
        {id: '643', name: "Wireless Distributor Networks LLC"},
        {id: '63', name: "Wireless shop llc"}
    ]);





    self.bypassAPI = false;

    self.alertData = 0;

    self.requestId = '';
    self.zsession = '';


    self.fieldMap = {
        startICCID: 'iccidStart',
        endICCID: 'iccidEnd',
        masteragent: 'master'
    };


    self.errorMap = {
        "value is shorter than min_strlen": "The value entered is too short.",
        "value is longer than max_strlen": "The value entered is too long.",
        "is not a integer": "Please select a Master Agent",
        "value fails LUHN checksum": "You entered an invalid activation code.",
        "value fails Actcode format validation": "You entered an invalid activation code.",
        "value fails validation (letters, spaces, apostrophe)": "Contains invalid character - letters, spaces, apostrophe only",
        "value fails validation (letters, numbers, spaces, punctuation, #)": "Contains invalid character.",
        "value fails validation (alphanumeric or punctuation)": "Alphanumeric and punctuation only.",
        "value fails validation (numeric)": "Value must be numeric."


    };





    //EO NON-OBSERVABLE CLASS VARS
    //////////////////////////////


    ///////////////////////////
    //KNOCKOUT.JS SUBSCRIPTIONS
    var joinedFields = _.values(self.fieldMap);
    for (var i = 0; i < joinedFields.length; i++) {

        self[joinedFields[i]].subscribe(function() {

            self.form_errors()[this] = '';
            self.form_errors.valueHasMutated();
            if (_.values(self.form_errors()).join('')=='') self.formError(false);
        },joinedFields[i]);
    }
    //EO KNOCKOUT.JS SUBSCRIPTIONS
    /////////////////////////////


    /////////////////////
    //INITIALIZATION CODE

    //Configure datatables to work with observable self.sims() object
    //Create the DataTable
    //***NOTE*** -> dataTable is lowercase. Original example was uppercase, broke in ie7

    var dt = $('#example2').dataTable( {
        "aoColumns": [
            { "mData": "ICCID_FULL",
                "mRender": function ( data, type, row ) {
                    return '<a target="_blank" href="/dig.php?search=' + data + '">' + data + '</a>';
                }
            },
            { "mData": "CUSTOMER_ID",
                "mRender": function ( data, type, row ) {
                    return '<a target="_blank" href="/dig.php?search=' + data + '">' + data + '</a>';
                }
            },
            { "mData": "SIM_HOT",
                "mRender": function ( data, type, row ) {
                    return data==1 ? "hot" : "cold";
                }
            },
            { "mDataProp": "PLAN_STATE" }
    ]

    } );

    self.sims.subscribe(
        function () {
            //sims array won't be incremently modified, so just clear it and rebuild
            //dt.clear();
            dt.fnClearTable();

            for (var i = 0, j = self.sims().length; i < j; i++) {
                dt.fnAddData(self.sims()[i]);
            }

            //if (self.sims().length > 0) dt.fnAddData(self.sims());


            //for (var i = 0, j = self.sims().length; i < j; i++) {
              //  dt.row.add( self.sims()[i] ).draw();
            //}
        }
    );

    //EO INITIALIZATION CODE
    ////////////////////////








    ///////////////////////////
    //CLASS FUNCTIONS

    self.getSIMs = function() {
        self.sims([]);
        self.thinking0(true);
        self.validSearch(false);
        self.success1('');
        self.error0('');

        var params = {
            bath: 'rest', version: '1',
            callback: 'hotcoldViewModel.callback_getSIMs',
            startICCID: self.iccidStart,
            endICCID: self.iccidEnd,
            masteragent: self.master

        }
        if (!self.iccidEnd()) {
            params.endICCID = self.iccidStart;
        }
        $.ajax('/partner.php?format=jsonp&partner=inventory&command=inventory__GetSIMSByMasteragent',{
            dataType:'jsonp',
            data: params
        });

    };



    self.callback_getSIMs = function(data) {
        self.debug(data);

        self.thinking0(false);
        if (data.success) {
            self.validSearch(true);
            self.sims(data.masteragent_iccid_info);


            var temp = 0;
            for (var i=0; i<self.sims().length; i++) {
                if (self.sims()[i].SIM_ACTIVE) temp++;
            }
            self.activeNeutralCount(temp);



        } else {
            if (data.errors) {
                for (var i = 0; i < data.errors.length; i++) {
                    var apiError = data.errors[i];
                    if (apiError.indexOf("ERR_API_NO_ORANGE_SIM_FOR_MASTERAGENT: No Orange Sims found for Master Agent") >= 0) {
                        self.formError(true);
                        self.error0('No SIMs found for selected master and SIM range.');
                        self.sims([]);
                    } else {
                        //apiError = apiError.replace(/credit card is already/g, 'account_cc_number');
                        self.iterateFieldErrors(apiError);
                    }


                }
            }

        }
    };


    self.makeHot = function() {
        self.thinking1(true);
        self.error1('');

        var params = {
            bath: 'rest', version: '1',
            callback: 'hotcoldViewModel.callback_makeHot',
            username: 'ultra_user',
            startICCID: self.iccidStart,
            endICCID: self.iccidEnd,
            masteragent: self.master

        }
        if (!self.iccidEnd()) {
            params.endICCID = self.iccidStart;
        }
        $.ajax('/partner.php?format=jsonp&partner=inventory&command=inventory__AssignICCIDRangeToHot',{
            dataType:'jsonp',
            data: params
        });

    };
    self.callback_makeHot = function(data) {
        self.debug(data);

        self.thinking1(false);
        if (data.success) {
            self.getSIMs();
            self.success1('SIMs successfully set to Hot.');
        } else {
            self.error1(data.errors[[0]]);
        }

    }

    self.makeCold = function() {
        self.thinking1(true);
        self.error1('');

        var params = {
            bath: 'rest', version: '1',
            callback: 'hotcoldViewModel.callback_makeCold',
            username: 'ultra_user',
            startICCID: self.iccidStart,
            endICCID: self.iccidEnd,
            masteragent: self.master

        }
        if (!self.iccidEnd()) {
            params.endICCID = self.iccidStart;
        }
        $.ajax('/partner.php?format=jsonp&partner=inventory&command=inventory__AssignICCIDRangeToCold',{
            dataType:'jsonp',
            data: params
        });

    };


    self.getMasters = function() {

        var params = {
            bath: 'rest', version: '2',
            callback: 'hotcoldViewModel.callback_populateMasters'
        }
        $.ajax('/ultra_api.php?format=jsonp&partner=internal&command=internal__GetMasterAgents',{
            dataType:'jsonp',
            data: params
        });

    };


    self.callback_populateMasters = function(data) {
        self.masters(data.master_agents);
    };

    self.callback_makeCold = function(data) {
        self.debug(data);

        self.thinking1(false);
        if (data.success) {
            self.getSIMs();
            self.success1('SIMs successfully set to Cold.');
        } else {
            self.error1(data.errors[[0]]);
        }

    }


    self.iterateFieldErrors = function(apiError) {
        var apiField = _.find(
            _.keys(self.fieldMap),
            function(field){
                var containsField = apiError.indexOf(field) >= 0;
                return containsField;
            }
        );
        if (apiField) {
            var jsField = self.fieldMap[apiField];
            self.form_errors()[jsField] = self.mapError(apiError,apiField);
            self.formError(true);
            self.form_errors.valueHasMutated();
        }
    }




    self.debug = function(data) {
        if (self.alertData) printJson(data);
    }



    self.mapError = function(error,field,specialString) {

        var errorTemp = '';

        //If passed a field value, it will reduce the error to what falls after the field name, and then check the errorMap.
        if (field) {
            errorTemp = error.substr(error.indexOf(field) + field.length + 1);
            if (self.errorMap[errorTemp]) return self.errorMap[errorTemp];
        }
        //Special string does the same thing, but has its own param to reduce confusion. Used for instances like have a
        //  variable string within the error string (a specific zip, for example).
        else if (specialString) {
            errorTemp = error.substr(error.indexOf(specialString) + specialString.length + 1);
            if (self.errorMap[errorTemp]) return self.errorMap[errorTemp];
        }
        //Try matching the entire error.
        if (self.errorMap[error]) return self.errorMap[error];


        //Default to the full error, if not mapped.
        return error;
    }


    self.killSession = function() {
        var params = {
            bath: 'rest', version: '1',
            callback: 'hotcoldViewModel.doNothing',
            zsession: self.zsession
        }

        $.ajax(self.baseUrl + '/partner.php?format=jsonp&partner=portal&command=portal__KillSession',{
            dataType:'jsonp',
            data: params
        });
    }

    self.doNothing = function() {}

}











